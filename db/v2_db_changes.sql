#Student Manager
alter table sz_tx_student_tutor add `followup_comment` varchar(200) DEFAULT NULL;

#Tutor Manager
Alter table sz_tx_tutor_profile add `followup_comment` varchar(200) DEFAULT NULL;

#Exit Survey
CREATE TABLE `sz_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastupdatedate` datetime DEFAULT NULL,
  `student_id` int(20) DEFAULT NULL,
  `task_complete` varchar(15) NOT NULL DEFAULT 'Yes',
  `task_description` varchar(500) DEFAULT '',
  `task_reason` varchar(500) DEFAULT '',
  `task_recommend` int(11) DEFAULT '1',
  `task_locality` varchar(255) DEFAULT NULL,
  `page` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

#Tutor Analytics
CREATE TABLE `sz_tx_tutor_analytics` (
  `date` date NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `profile_view_count` int(200) DEFAULT '0',
  `message_click_count` int(200) DEFAULT '0',
  `phone_click_count` int(200) DEFAULT '0',
  `enroll_count` int(200) DEFAULT '0',
  `fb_click_count` int(200) DEFAULT '0',
  `twitter_click_count` int(200) DEFAULT '0',
  `course_count` int(200) DEFAULT '0',
  `videos_click_count` int(200) DEFAULT '0',
  `paymentstep2_count` int(200) DEFAULT '0',
  `paymentstep3_count` int(200) DEFAULT '0',
  PRIMARY KEY (`date`,`tutor_id`,`course_id`)
);

#Password Encryption
Alter table sz_tx_student_tutor add `std_v2_password` varchar(200) DEFAULT NULL;
Alter table sz_tx_tutor_profile add `tutor_v2_pwd` varchar(200) DEFAULT NULL;

#Batch Creation for course
CREATE TABLE `sz_tx_tutor_course_batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tutor_id` int(11) NOT NULL,
  `course_id` int(20) NOT NULL,
  `tutor_batch_name` varchar(200) DEFAULT NULL,
  `tutor_location` varchar(50) DEFAULT NULL,
  `tutor_lesson_location` varchar(20) DEFAULT NULL,
  `travel_radius` varchar(75) DEFAULT NULL,
  `tutor_class_dur_wks` varchar(50) DEFAULT NULL,
  `tutor_class_dur_dayspwk` varchar(50) DEFAULT NULL,
  `tutor_class_dur_hrspday` varchar(50) DEFAULT NULL,
  `tutor_batch_from_timing` varchar(50) DEFAULT NULL,
  `tutor_batch_to_timing` varchar(50) DEFAULT NULL,
  `tutor_batch_day` varchar(50) DEFAULT NULL,
  `batch_size` int(11) DEFAULT NULL,
  `seat_available` int(11) NOT NULL,
  `batch_date` datetime DEFAULT NULL,
  `lastupdatedate` datetime DEFAULT NULL,
  `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `locality` int(11) DEFAULT NULL,
  `pincode` int(11) DEFAULT '0',
  `landmark` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_format` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tutor_id` (`tutor_id`),
  KEY `course_id` (`tutor_id`)
) ENGINE=InnoDB;


#Day table
CREATE TABLE `sz_master_tutor_batch_day` (
  `day_id` int(11) NOT NULL AUTO_INCREMENT,
  `day_name` varchar(50) NOT NULL,
  PRIMARY KEY (`day_id`)
) ENGINE=InnoDB

#indexing for mysql database
alter table sz_tx_tutor_videos add KEY `tutor_id` (`tutor_id`);
alter table sz_batch_details add KEY `tutor_id` (`tutor_id`);
alter table sz_batch_details add KEY `tutor_course_id` (`tutor_course_id`);
alter table sz_branch_details add KEY `tutor_id` (`tutor_id`);
alter table sz_tx_tutor_albums add  KEY `tutor_id` (`tutor_id`);
alter table sz_tutor_profile_view add KEY `tutor_id` (`tutor_id`);
alter table sz_tx_tutor_skill_course add  KEY `tutor_pay_feetype` (`tutor_pay_feetype`);
alter table sz_tx_tutor_skill_course add  KEY `tutor_lesson_location` (`tutor_lesson_location`);


# add to table  `sz_master_tutor_batch_day`
INSERT INTO `sz_master_tutor_batch_day` VALUES (1,'Mon'),(2,'Tue'),(3,'Wed'),(4,'Thu'),(5,'Fri'),(6,'Sat'),(7,'Sun');

#changing datatype of batch_date in sz_tx_tutor_course_batch
alter table sz_tx_tutor_course_batch MODIFY column batch_date varchar(50) DEFAULT NULL; 

# For storing bank details
alter table sz_tx_tutor_profile add bank_name varchar(200) default null;
alter table sz_tx_tutor_profile add ifsc_code varchar(200) default null;
alter table sz_tx_tutor_profile add acc_no varchar(200) default null;

#Age min max columns
alter table sz_tx_tutor_profile add min varchar(200) default null; 
alter table sz_tx_tutor_profile add max varchar(200) default null;


#Order table

CREATE TABLE `sz_order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `std_id` int(11) NOT NULL,   student id
  `tutor_book_id` int(11) NOT NULL,    
  `tutor_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_of_booking` datetime NOT NULL,
  `class_booked` varchar(100) DEFAULT NULL,
  `no_of_students` int(11) NOT NULL,
  `tutor_fee_per_unit` int(11) NOT NULL,  
  `total_price` int(11) NOT NULL,  
  `commision_price` int(11) NOT NULL,
  `tutor_price` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

Alter table sz_billing_details add `course_id` int(11) DEFAULT NULL;
Alter table sz_billing_details add `batch_id` int(11) DEFAULT NULL;

#Order table
CREATE TABLE `sz_commission_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_fee` int(20) NOT NULL,
  `max_fee` int(20) NOT NULL,
  `min_enroll` int(20) NOT NULL,
  `max_enroll` int(11) NOT NULL,
  `commission` double(5,3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

#insert query for commision model
insert into sz_commission_details values("1","0","1999","0","5","0.15"),("2","0","1999","6","7","0.10"),("3","0","1999","8","999","0.075"),("4","2000","4999","0","3","0.15"),("5","2000","4999","4","5","0.10"),("6","2000","4999","6","999","0.075"),("7","5000","10000","0","1","0.15"),("8","5000","10000","2","3","0.10"),("9","5000","10000","4","999","0.075");

alter table sz_order_details modify  `commision_price` double(10,2) NOT NULL;
alter table sz_order_details modify `tutor_fee_per_unit` double(10,2) NOT NULL;
alter table sz_order_details modify `total_price` double(10,2) NOT NULL;
alter table sz_order_details modify `tutor_price` double(10,2) NOT NULL;
alter table sz_billing_details add `grand_total` double(10,2) not null;  
alter table sz_billing_details modify `price` double(10,2) not null;
alter table sz_order_details add `grand_total` double(10,2) not null;  

#Final batch summary Column in sz_tx_tutor_course_batch
alter table sz_tx_tutor_course_batch add column batch_summary_for_course varchar(200) default null;

#modify order table phone number
alter table sz_order_details  modify `phone_number` varchar(11) NOT NULL;

#Age min max columns for each course - Search Filter
alter table sz_tx_tutor_skill_course add min varchar(200) default null; 
alter table sz_tx_tutor_skill_course add max varchar(200) default null;

#Search Functionality - Batch Timing table
CREATE TABLE `sz_batch_timing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_timing_type` varchar(20) NOT NULL,
  `batch_from_timing` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB 

INSERT INTO `sz_batch_timing` (`id`, `batch_timing_type`, `batch_from_timing`) VALUES
(1, 'Any time', '00:00:00'),
(2, 'Early Morning', '04:00:00'),
(3, 'Late Morning', '09:00:00'),
(4, 'Afternoon', '12:00:00'),
(5, 'Evening', '17:00:00'),
(6, 'Night', '20:00:00'),
(7, 'Weekend', '00:00:00');


#Search Functionality - student_age table
CREATE TABLE `sz_student_age` (
  `student_age_id` int(11) NOT NULL,
  `student_age_type` varchar(20) NOT NULL,
  PRIMARY KEY (`student_age_id`)
) ENGINE=InnoDB

INSERT INTO `sz_student_age` (`student_age_id`, `student_age_type`) VALUES
(1, 'Everyone'),
(2, 'Kids');

#Search Functionality - Timing table
CREATE TABLE IF NOT EXISTS `sz_master_timing` (
`id` int(11) NOT NULL,
  `timing` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

INSERT INTO `sz_master_timing` (`id`, `timing`) VALUES
(1, '5:00 a.m'),
(2, '5:30 a.m'),
(3, '6:00 a.m'),
(4, '6:30 a.m'),
(5, '7:00 a.m'),
(6, '7:30 a.m'),
(7, '8:00 a.m'),
(8, '8:30 a.m'),
(9, '9:00 a.m'),
(10, '9:30 a.m'),
(11, '10:00 a.m'),
(12, '10:30 a.m'),
(13, '11:00 a.m'),
(14, '11:30 a.m'),
(15, '12:00 p.m'),
(16, '12:30 p.m'),
(17, '1:00 p.m'),
(18, '1:30 p.m'),
(19, '2:00 p.m'),
(20, '2:30 p.m'),
(21, '3:00 p.m'),
(22, '3:30 p.m'),
(23, '4:00 p.m'),
(24, '4:30 p.m'),
(25, '5:00 p.m'),
(26, '5:30 p.m'),
(27, '6:00 p.m'),
(28, '6:30 p.m'),
(29, '7:00 p.m'),
(30, '7:30 p.m'),
(31, '8:00 p.m'),
(32, '8:30 p.m'),
(33, '9:00 p.m'),
(34, '9:30 p.m'),
(35, '10:00 p.m'),
(36, '10:30 p.m'),
(37, '11:00 p.m'),
(38, '11:30 p.m');

CREATE TABLE IF NOT EXISTS `sz_master_batch_timing` (
`id` int(11) NOT NULL,
  `timing` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

INSERT INTO `sz_master_batch_timing` (`id`, `timing`) VALUES
(1, '12:00am'),
(2, '12:30am'),
(3, '1:00am'),
(4, '1:30am'),
(5, '2:00am'),
(6, '2:30am'),
(7, '3:00am'),
(8, '3:30am'),
(9, '4:00am'),
(10, '4:30am'),
(11, '5:00am'),
(12, '5:30am'),
(13, '6:00am'),
(14, '6:30am'),
(15, '7:00am'),
(16, '7:30am'),
(17, '8:00am'),
(18, '8:30am'),
(19, '9:00am'),
(20, '9:30am'),
(21, '10:00am'),
(22, '10:30am'),
(23, '11:00am'),
(24, '11:30am'),
(25, '12:00pm'),
(26, '12:30pm'),
(27, '1:00pm'),
(28, '1:30pm'),
(29, '2:00pm'),
(30, '2:30pm'),
(31, '3:00pm'),
(32, '3:30pm'),
(33, '4:00pm'),
(34, '4:30pm'),
(35, '5:00pm'),
(36, '5:30pm'),
(37, '6:00pm'),
(38, '6:30pm'),
(39, '7:00pm'),
(40, '7:30pm'),
(41, '8:00pm'),
(42, '8:30pm'),
(43, '9:00pm'),
(44, '9:30pm'),
(45, '10:00pm'),
(46, '10:30pm'),
(47, '11:00pm'),
(48, '11:30pm'),
(67, '11:30pm');

#mapping keyword
CREATE TABLE `sz_skill_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_entered` varchar(25) NOT NULL,
  `skill_name` varchar(25) NOT NULL,
  `skill_uniq` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8

#Batch Timing Update query
ALTER TABLE sz_batch_timing MODIFY batch_timing_type varchar(80) NOT NULL;

update `sz_batch_timing` set `batch_timing_type`='Early Morning (5:00am to 9:00am)' where `id`=2;
update `sz_batch_timing` set `batch_timing_type`='Late Morning (9:00am to 12:00pm)' where `id`=3;
update `sz_batch_timing` set `batch_timing_type`='Afternoon (12:00pm to 5:00pm)' where `id`=4;
update `sz_batch_timing` set `batch_timing_type`='Evening (5:00pm to 9:00pm)' where `id`=5;
update `sz_batch_timing` set `batch_timing_type`='Night (9:00pm to 12:00am)' where `id`=6;
update `sz_master_batch_timing` set `timing`='12:00pm' where `id`=67; 


#Task Survey
ALTER TABLE sz_task modify column student_id int(20);
alter table sz_task add page varchar(50);

# mapping database queries
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values('dnace','dance','Dance'),('eance','dance','Dance'),('sance','dance','Dance'),('cance','dance','Dance'),('fance','dance','Dance'),('dsnce','dance','Dance'),('dznce','dance','Dance'),('dqnce','dance','Dance'),('damce','dance','Dance'),('dahce','dance','Dance'),('dabce','dance','Dance'),('dande','dance','Dance'),('danve','dance','Dance'),('danxe','dance','Dance'),('dance','dance','Dance'),('dence','dance','Dance');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values('music','music','Music'),('nusic','music','Music'),('kusic','music','Music'),('jusic','music','Music'),('misic','music','Music'),('mysic','music','Music'),('mjsic','music','Music'),('mueic','music','Music'),('muaic','music','Music'),('mudic','music','Music'),('muwic','music','Music'),('musoc','music','Music'),('musuc','music','Music'),('muskc','music','Music'),('musid','music','Music'),('musiv','music','Music'),('musix','music','Music');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq)  values ('spiritual','spiritual','Spiritual'),('epiritual','spiritual','Spiritual'),('apiritual','spiritual','Spiritual'),('dpiritual','spiritual','Spiritual'),('wpiritual','spiritual','Spiritual'),('soiritual','spiritual','Spiritual'),('sliritual','spiritual','Spiritual'),('sporitual','spiritual','Spiritual'),('spuritual','spiritual','Spiritual'),('spkritual','spiritual','Spiritual');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq)  values('fitnses','fitness','Fitness'),('fitnes','fitness','Fitness'),('ritness','fitness','Fitness'),('titness','fitness','Fitness'),('citness','fitness','Fitness'),('ditness','fitness','Fitness'),('fotness','fitness','Fitness'),('futness','fitness','Fitness'),('fktness','fitness','Fitness'),('firness','fitness','Fitness'),('fitness','fitness','Fitness');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('violin','violin','Violin'),('violins','violin','Violin'), ('violni','violin','Violin'),('voilin','violin','Violin'),('ciolin','violin','Violin'),('biolin','violin','Violin'),('fiolin','violin','Violin'),('voolin','violin','Violin'),('vuolin','violin','Violin'),('vkolin','violin','Violin'),('viilin','violin','Violin'),('villin','violin','Violin'); 
insert into sz_skill_map (skill_entered,skill_name,skill_uniq)  values ('djing','djing','DJing'),('djnig','djing','DJing'),('ejing','djing','DJing'),('sjing','djing','DJing'),('cjing','djing','DJing'),('fjing','djing','DJing'),('diing','djing','DJing'),('dning','djing','DJing'),('duing','djing','DJing'),('dming','djing','DJing'),('djong','djing','DJing');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq)  values ('guitar','guitar','Guitar'),('guitra','guitar','Guitar'),('ruitar','guitar','Guitar'),('nuitar','guitar','Guitar'),('tuitar','guitar','Guitar'),('huitar','guitar','Guitar'),('giitar','guitar','Guitar'),('gyitar','guitar','Guitar'),('gjitar','guitar','Guitar'),('guotar','guitar','Guitar');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq)  values ('singing','singing','Singing'),('sniging','singing','Singing'),('einging','singing','Singing'),('ainging','singing','Singing'),('dinging','singing','Singing'),('winging','singing','Singing'),('songing','singing','Singing'),('sunging','singing','Singing'),('sknging','singing','Singing'),('simging','singing','Singing'),('sihging','singing','Singing'),('sibging','singing','Singing');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq)  values ('acting','acting','Acting'),('actnig','acting','Acting'),('acitng','acting','Acting'),('adting','acting','Acting'),('avting','acting','Acting'),('axting','acting','Acting'),('acring','acting','Acting'),('acging','acting','Acting'),('acying','acting','Acting'),('actong','acting','Acting'),('actung','acting','Acting'),('actkng','acting','Acting');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('parkour','parkour','Parkour'),('parkours','parkour','Parkour'), ('parkuor','parkour','Parkour'),('prakour','parkour','Parkour'),('oarkour','parkour','Parkour'),('larkour','parkour','Parkour'),('psrkour','parkour','Parkour'),('paekour','parkour','Parkour'),('patkour','parkour','Parkour'),('pagkour','parkour','Parkour'),('pafkour','parkour','Parkour'),('pariour','parkour','Parkour');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('ballet','ballet','Ballet'),('ballets','ballet','Ballet'), ('balelt','ballet','Ballet'),('balet','ballet','Ballet'),('bsllet','ballet','Ballet'),('bzllet','ballet','Ballet'),('nallet','ballet','Ballet'),('gallet','ballet','Ballet'),('balpet','ballet','Ballet'),('balmet','ballet','Ballet'),('balleg','ballet','Ballet'),('balley','ballet','Ballet');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('bboying','bboying','B-boying'),('bboyings','bboying','B-boying'),('nboying','bboying','B-boying'),('gboying','bboying','B-boying'),('bnoying','bboying'),('bgoying','bboying','B-boying'),('bvoying','bboying','B-boying'),('bblying','bboying','B-boying'),('bbpying','bboying','B-boying'),('bboting','bboying'),('bbouing','bboying','B-boying'),('bboyimg','bboying','B-boying');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('bollywood','bollywood','Bollywood'),('bollywoods','bollywood','Bollywood'), ('bolywood','bollywood','Bollywood'),('bollywod','bollywood','Bollywood'),('nollywood','bollywood','Bollywood'),('gollywood','bollywood','Bollywood'),('vollywood','bollywood','Bollywood'),('billywood','bollywood','Bollywood'),('blllywood','bollywood','Bollywood'),('bpllywood','bollywood','Bollywood'),('boplywood','bollywood','Bollywood'),('bomlywood','bollywood','Bollywood');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('tango','tango','Tango'),('tangos','tango','Tango'), ('tsngo','tango','Tango'),('tzngo','tango','Tango'),('tqngo','tango','Tango'),('tamgo','tango','Tango'),('tahgo','tango','Tango'),('tabgo','tango','Tango'),('tanro','tango','Tango'),('tanno','tango','Tango'),('tanto','tango','Tango'),('tanho','tango','Tango');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('piano','piano','Piano'),('pianos','piano','Piano'), ('pinao','piano','Piano'),('oiano','piano','Piano'),('liano','piano','Piano'),('poano','piano','Piano'),('puano','piano','Piano'),('pkano','piano','Piano'),('pisno','piano','Piano'),('pizno','piano','Piano'),('piqno','piano','Piano'),('piamo','piano','Piano');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('tabla','tabla','Tabla'),('tablas','tabla','Tabla'), ('rabla','tabla','Tabla'),('gabla','tabla','Tabla'),('yabla','tabla','Tabla'),('tsbla','tabla','Tabla'),('tzbla','tabla','Tabla'),('tqbla','tabla','Tabla'),('tanla','tabla','Tabla'),('tagla','tabla','Tabla'),('tavla','tabla','Tabla'),('tabpa','tabla','Tabla');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('flute','flute','Flute'),('flutes','flute','Flute'), ('rlute','flute','Flute'),('tlute','flute','Flute'),('clute','flute','Flute'),('dlute','flute','Flute'),('fpute','flute','Flute'),('fmute','flute','Flute'),('fkute','flute','Flute'),('flite','flute','Flute'),('flyte','flute','Flute'),('fljte','flute','Flute');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('waltz','waltz','Waltz'),('waltzs','waltz','Waltz'), ('ealtz','waltz','Waltz'),('saltz','waltz','Waltz'),('qaltz','waltz','Waltz'),('wsltz','waltz','Waltz'),('wzltz','waltz','Waltz'),('wqltz','waltz','Waltz'),('waptz','waltz','Waltz'),('wamtz','waltz','Waltz'),('walrz','waltz','Waltz'),('walgz','waltz','Waltz');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('ballroom','ballroom','Ballroom'),('ballrooms','ballroom','Ballroom'), ('balroom','ballroom','Ballroom'),('ballrom','ballroom','Ballroom'),('nallroom','ballroom','Ballroom'),('gallroom','ballroom','Ballroom'),('vallroom','ballroom','Ballroom'),('bsllroom','ballroom','Ballroom'),('bzllroom','ballroom','Ballroom'),('bqllroom','ballroom','Ballroom'),('baplroom','ballroom','Ballroom'),('bamlroom','ballroom','Ballroom');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('sitar','sitar','Sitar'),('sitars','sitar','Sitar'), ('sitra','sitar','Sitar'),('eitar','sitar','Sitar'),('aitar','sitar','Sitar'),('ditar','sitar','Sitar'),('witar','sitar','Sitar'),('sotar','sitar','Sitar'),('sutar','sitar','Sitar'),('sktar','sitar','Sitar'),('sirar','sitar','Sitar'),('sigar','sitar','Sitar');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('karate','karate','Karate'),('karates','karate','Karate'), ('kartae','karate','Karate'),('kraate','karate','Karate'),('iarate','karate','Karate'),('oarate','karate','Karate'),('larate','karate','Karate'),('marate','karate','Karate'),('ksrate','karate','Karate'),('kzrate','karate','Karate'),('kqrate','karate','Karate'),('kaeate','karate','Karate');
insert into sz_skill_map (skill_entered,skill_name,skill_uniq) values ('bartending','bartending','Bartending'),('bartendings','bartending','Bartending'), ('bartedning','bartending','Bartending'),('bartneding','bartending','Bartending'),('bratending','bartending','Bartending'),('batrending','bartending','Bartending'),('nartending','bartending','Bartending'),('gartending','bartending','Bartending'),('vartending','bartending','Bartending'),('bsrtending','bartending','Bartending'),('bzrtending','bartending','Bartending'),('bartendnig','bartending','Bartending');


#updated age
update `sz_student_age` set `student_age_type`='Adults (> 14)' where `student_age_id`=1; 
update `sz_student_age` set `student_age_type`='Kids (14 and below)' where `student_age_id`=2; 

#batch shortlisted 
CREATE TABLE `sz_shortlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` varchar(20) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `tutor_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;


CREATE TABLE `sz_master_serpa_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serpa_uname` varchar(20) NOT NULL,
  `serpa_pwd` varchar(50) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

insert into sz_master_serpa_login (serpa_uname,serpa_pwd,is_active) values('admin',md5('serpa@123'),'1');

CREATE TABLE `sz_job_table` ( 
  `job_id` int(11) NOT NULL AUTO_INCREMENT, 
  `state` enum('NEW', 'OPEN', 'CLOSED') DEFAULT 'NEW',
  `reason` varchar(50) DEFAULT NULL, 
  `date_new` datetime DEFAULT NULL, 
  `date_first_followup` datetime DEFAULT NULL,
  `date_last_followup` datetime DEFAULT NULL,
  `date_close` datetime DEFAULT NULL, 
  `date_next_followup` datetime DEFAULT NULL, 
  `type` enum('STUDENT_LEAD', 'PROFILE_CHANGE','CUSTOM') DEFAULT NULL, 
  `tutor_id` int(11) NOT NULL, `student_id` int(11) NOT NULL, 
  PRIMARY KEY (`job_id`) 
  ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;



CREATE TABLE `sz_checklist` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`job_id` int(11) NOT NULL,
`task` enum('TUTOR_RECO', 'ERP_STEP_1', 'ERP_STEP_2' , 'ERP_STEP_3', 'ERP_STEP_4','PROFILE_VERIFICATION','FEEDBACK_REQUEST','TUTOR_REPORT','CUSTOM') DEFAULT NULL,
`date_due` datetime DEFAULT NULL,
`date_created` datetime DEFAULT NULL,
`date_done` datetime DEFAULT NULL,
`done_flag` int(5) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `sz_log` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`job_id` int(11) NOT NULL,
`date_entry` datetime NOT NULL,
`entry` varchar(50) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


# modify checklist table column

Alter table sz_checklist modify `task` enum('REGISTER_STUDENT','TUTOR_FEEDBACK','TUTOR_RECO', 'ERP_STEP_1', 'ERP_STEP_2' , 'ERP_STEP_3', 'ERP_STEP_4','PROFILE_VERIFICATION','FEEDBACK_REQUEST','TUTOR_REPORT','CUSTOM') DEFAULT NULL;
Alter table sz_job_table modify `type` enum('STUDENT_LEAD', 'PROFILE_CHANGE','CUSTOM','PHONE_LEAD') DEFAULT NULL;
Alter table sz_log add `t_color`  varchar(20) COLLATE utf8_unicode_ci NOT NULL;
Alter table sz_log modify `entry` varchar(100) DEFAULT NULL;

# Modify Page (Task survey)
Alter table sz_task modify `page` varchar(100) DEFAULT NULL;


# Exotel table

CREATE TABLE `sz_exotel_extension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `calls_id` varchar(100) NOT NULL,
  `from_number` varchar(20) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `landline_number` varchar(20) NOT NULL,
  `digit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

# Extension column add tutor table

Alter table sz_tx_tutor_profile add `extension` int(11) NOT NULL;


# Task survey changes
alter table sz_task modify `task_complete` varchar(500) not null;
Alter table sz_log modify `entry` varchar(500) NOT NULL;


Alter table sz_task add `refund` varchar(10) NOT NUll;
Alter table sz_task modify `task_complete` varchar(100) NOT NUll;
Alter table sz_task modify `task_description` varchar(20) NOT NUll;
Alter table sz_task modify `task_reason` varchar(30) NOT NUll;
Alter table sz_task modify `task_recommend` varchar(30) NOT NUll;
Alter table sz_task modify `task_locality` varchar(500) NOT NUll;


# After 19oct live


#Trust-meter in profile page with listing
Alter table sz_tx_tutor_profile add `trustmeter` int(10) DEFAULT '0';
Alter table sz_tx_tutor_profile add `adhar_license` varchar(30) DEFAULT NULL;
Alter table sz_tx_tutor_profile add `virtual_tour` varchar(100) DEFAULT NULL;


#Batch Variations for timings in different slots
Alter table sz_tx_tutor_course_batch add `tutor_batch_from_timing1` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_to_timing1` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_day1` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_from_timing2` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_to_timing2` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_day2` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_from_timing3` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_to_timing3` varchar(50) DEFAULT NULL;
Alter table sz_tx_tutor_course_batch add `tutor_batch_day3` varchar(50) DEFAULT NULL;


#slot book in order details
Alter table sz_order_details add `slot_book` varchar(100) NOT NULL after `class_booked`;
Alter table sz_order_details add `batch_location` varchar(100) NOT NULL after `class_booked`;


#student email varification 
Alter table sz_tx_student_tutor add `status` enum('0','1') default '0';
Alter table sz_tx_student_tutor add `hash` varchar(100) NOT NULL;
update sz_tx_student_tutor set status='1';

