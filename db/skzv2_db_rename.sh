#!/bin/bash
# Rommel Dongre, 2015

if [ "$#" -ne 4 ]
then
    echo "Usage: '$0' [user] [password] [old_db] [new_db]"
    exit 1
fi

#dump old db to file
mysqldump -u$1 -p$2 $3 > $3.sql

# Create new database
cmd="create database $4"
echo $cmd
mysql -u$1 -p$2 -e "$cmd"
if [ "$?" -eq 0 ]
then
    echo "Created Database '$4' successfully"
else 
    exit $?
fi

# Import database
mysql -u$1 -p$2 $4 < $3.sql
if [ "$?" -eq 0 ]
then
    echo "Imported Database '$4' successfully"
    rm $3.sql
else 
    exit $?
fi
 
# Drop old database
cmd="drop database $3"
echo $cmd
mysql -u$1 -p$2 -e "$cmd"
if [ "$?" -eq 0 ]
then
    echo "Dropping Database '$4' successfully"
else 
    exit $?
fi

