#!/bin/bash
# Rommel Dongre, 2015

if [ "$#" -ne 4 ]
then
    echo "Usage: '$0' [user] [password] [database] [file]"
    exit 1
fi

# Create database
cmd="create database $3"
echo $cmd
mysql -u$1 -p$2 -e "$cmd"
if [ "$?" -eq 0 ]
then
    echo "Created Database '$3' successfully"
else 
    exit $?
fi

# Import database
cmd="source $4"
mysql -u$1 -p$2 -D $3 -e "$cmd"
if [ "$?" -eq 0 ]
then
    echo "Imported Database '$3' successfully"
else 
    exit $?
fi
 
