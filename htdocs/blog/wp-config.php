<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'skillzot_blog');

/** MySQL database username */
//define('DB_USER', 'skillzot_blog');
define('DB_USER', 'root');
/** MySQL database password */
//define('DB_PASSWORD', 'Skillzot2012');
define('DB_PASSWORD', 'root');
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-].,z=,_|H=[C>-|XCyq|TCU,-*o-C4*S/`wsW pVv|%.A5O0-_o=x{OlQ>sFGbs');
define('SECURE_AUTH_KEY',  'x6U|P!jF _&bRTJ4(W+_YqcRvR,nA+PwH,hF&St,x)ZA4fwWD;>XfapdH.rDhiJ`');
define('LOGGED_IN_KEY',    'ct(g{%J[U9/PyuykOjLy2-b4HLjp&MY|#erUd!sf~!9Ae_oN*+-Z`Fu^hR`XB)t&');
define('NONCE_KEY',        ' [QspG@:o<vtnyvh3gqNyVEB&Z=LUG^*B8y5NL,=L4JJf7veoof^B`#9NxeevPHw');
define('AUTH_SALT',        'r};H?P1CDhm;1bzLn(8Og:t3M2*/>K@VLX.l>EozLGL4&q!S9@xK:wbF?TWay*9^');
define('SECURE_AUTH_SALT', 'i|gh9XGtBG=#M6[A#%LAY0:OPJ1hxpoOAafLa)GnWSV9)3506t1lp-+ZdDF1x0J/');
define('LOGGED_IN_SALT',   '%)hflTc(0aD`@/`MF%rqJIlZmY1[k)a6~(5Py,6{a7wyYQtO6U-`xE77.xjfnUh$');
define('NONCE_SALT',       ';Hkhxg]l1G|[THKDC@(N5`%$rd2@Y+z4C6LFwAco-nb_c$UM>^YeK^:SFr(7u<Qn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');