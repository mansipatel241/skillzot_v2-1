<script>
$(function(){
	$.fn.hoverIntent = function(f,g) {
		// default configuration options
		var cfg = {
			sensitivity: 7,
			interval: 0,
			timeout: 0
		};
		// override configuration options with user supplied object
		cfg = $.extend(cfg, g ? { over: f, out: g } : f );

		// instantiate variables
		// cX, cY = current X and Y position of mouse, updated by mousemove event
		// pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
		var cX, cY, pX, pY;

		// A private function for getting mouse position
		var track = function(ev) {
			cX = ev.pageX;
			cY = ev.pageY;
		};

		// A private function for comparing current and previous mouse position
		var compare = function(ev,ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			// compare mouse positions to see if they've crossed the threshold
			if ( ( Math.abs(pX-cX) + Math.abs(pY-cY) ) < cfg.sensitivity ) {
				$(ob).unbind("mousemove",track);
				// set hoverIntent state to true (so mouseOut can be called)
				ob.hoverIntent_s = 1;
				return cfg.over.apply(ob,[ev]);
			} else {
				// set previous coordinates for next time
				pX = cX; pY = cY;
				// use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
				ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
			}
		};

		// A private function for delaying the mouseOut function
		var delay = function(ev,ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			ob.hoverIntent_s = 0;
			return cfg.out.apply(ob,[ev]);
		};

		// A private function for handling mouse 'hovering'
		var handleHover = function(e) {
			// next three lines copied from jQuery.hover, ignore children onMouseOver/onMouseOut
			var p = (e.type == "mouseover" ? e.fromElement : e.toElement) || e.relatedTarget;
			while ( p && p != this ) { try { p = p.parentNode; } catch(e) { p = this; } }
			if ( p == this ) { return false; }

			// copy objects to be passed into t (required for event object to be passed in IE)
			var ev = jQuery.extend({},e);
			var ob = this;

			// cancel hoverIntent timer if it exists
			if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

			// else e.type == "onmouseover"
			if (e.type == "mouseover") {
				// set "previous" X and Y position based on initial entry point
				pX = ev.pageX; pY = ev.pageY;
				// update "current" X and Y position based on mousemove
				$(ob).bind("mousemove",track);
				// start polling interval (self-calling timeout) to compare mouse coordinates over time
				if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

			// else e.type == "onmouseout"
			} else {
				// unbind expensive mousemove event
				$(ob).unbind("mousemove",track);
				// if hoverIntent state is true, then call the mouseOut function after the specified delay
				if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
			}
		};

		// bind the function to the two event listeners
		return this.mouseover(handleHover).mouseout(handleHover);
	};
    var config = {    
         sensitivity: 3, // number = sensitivity threshold (must be 1 or higher)    
         interval: 0,  // number = milliseconds for onMouseOver polling interval    
         over: doOpen,   // function = onMouseOver callback (REQUIRED)    
         timeout: 0,   // number = milliseconds delay before onMouseOut    
         out: doClose    // function = onMouseOut callback (REQUIRED)    
    };
    
    function doOpen() {
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
    }
 
    function doClose() {
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    }

    $("ul.dropdown li").hoverIntent(config);
    
    $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");

});

</script>


<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

$mydb = new wpdb('skillzot_staging','secret123','skillzot_test','localhost');
$rows = $mydb->get_results("select * from sz_tx_tutor_profile");

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div class="left" id="logo">
				<a href="/index/index"><img src="http://skillzot.com/testdata/blog/wp-content/uploads/2012/12/skillzot_logonew.jpg"></a>
			</div>
			
			<div class="right" id="header_right">
				<div style="float:right;" id="loginsignup">
					<a id="tutorsignup" class="right" href="/tutor/register">SIGN UP AS A TUTOR</a>
						<div style="float:right;" id="login" class="left">
							<a href="javascript:void(0);" style="font-weight:bold; margin-right: 9px;text-decoration: none;font-size:14px;" class="loginstyledefault openskillbox" id="headerloginidforloginprocess" rel="/login/index">LOG IN</a>
						</div>
				</div>
				<div class="clear">&nbsp;</div>
				<div class="right" id="headercontactus">
					<span rel="/managecms/contactus" class="contacttext openskillbox">Need Help  : </span>
					<span rel="/managecms/contactus" class="contactusno openskillbox">09820921404</span>
				<div class="clear">&nbsp;</div>
				<span style="color:#666666;display:inline-block;;font-size:12px;font-weight:normal;">Mon-Fri, 10am to 6pm</span>
				</div>
				<div class="clear">&nbsp;</div>	
			</div>	
		</div><!-- #masthead -->
		
		
		
		<div id="social">
				<ul>
			    	<li><a href="#" onclick="javascript:call_fb();" class="fb"></a></li>
			        <li><a href="#" onclick="javascript:call_twitter();" class="tweet"></a></li>
				  <li><a class="subscribe" href="#"></a></li>
			        <!-- <li><a class="youtube" href="#"></a></li> brgv 25-04-->
			    </ul>
		</div>
		
			
		<div id="menu">
		<div id="page-wrap">
		<ul class="dropdown"><?php //echo $fetchSkill_id;exit;?>
        		<li><a href="http://skillzot.com/testdata/index/index">Home</a></li>
				<?php $rows=$mydb->get_results("SELECT `s`.* FROM `sz_master_skills_for_diplay_listing` AS `s` WHERE 	(is_enabled = 1 && is_skill = 1 && parent_skill_id = 0 && skill_id!=0) ORDER BY `order_id` asc");
			
				if(isset($rows) &&  sizeof($rows)>0)
				{
				     foreach($rows as $maincategory)
				     {
				     ?>
				    <li id="dynamicselected<?php echo $maincategory->skill_id;?>"><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $maincategory->skill_id;?>" ><?php echo $maincategory->skill_name;?></a>
				   
					<?php
					$subcategory = $maincategory->skill_id;
					$subskillRows = $mydb->get_results("SELECT `s`.* FROM `sz_master_skills_for_diplay_listing` AS `s` WHERE (is_enabled = 1 && is_skill = 1 && parent_skill_id = '$subcategory' && skill_id!=0) ORDER BY `skill_name` asc");
					//-------------------------						  
					if(isset($subskillRows) && sizeof($subskillRows)>0)
					{
							$sizeofsubmain = sizeof($subskillRows);
							$sizeofsubmain1 = sizeof($subskillRows);
							//echo ($recordSize % 10);
							if ($sizeofsubmain % 10 == 0)
							{
									$widthCountforsubmain = ($sizeofsubmain / 10);
							}else
							{
									$widthCountforsubmain = ($sizeofsubmain / 10) + 1;
							}
							$roundedWidthcountforsubmain = floor($widthCountforsubmain);
							if (isset($sizeofsubmain) && $sizeofsubmain > 20 )
							{
									$roundedWidthcountforsubmain = $roundedWidthcountforsubmain-1;
							}
							$subdiv = -1;	
					}					  
					//--------------------------
												  
										  
					if(isset($subskillRows) && sizeof($subskillRows)>0)
					{?>
							<ul class="sub_menu" style="width:<?php echo ($roundedWidthcountforsubmain * 150);?>px;">
						
					        		<?php if(isset($subskillRows) && sizeof($subskillRows) > 10 )
									{ 
									if ($sizeofsubmain1 % 2 == 0)
									{
										$widthCountforsubmain1 = ($sizeofsubmain1 / 2);
									}else
									{
										$widthCountforsubmain1 = ($sizeofsubmain1 / 2) + 1;
									}
									$roundedWidthcountforsubmain1 = floor($widthCountforsubmain1);
									?>
									
									<div style="width:150px;float:left;display:inline-block;">
									<?php $j=0;?>
									<?php foreach($subskillRows as $subskillRow)
									{  
												if($j < $roundedWidthcountforsubmain1)
												{?>
													<li><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $subskillRow->skill_id;?>" ><?php echo $subskillRow->skill_name;?></a></li>
												<?php } ?>	
									<?php $j++;}?>
									</div>
									<div style="width:150px;float:left;display:inline-block;">
											<?php $j=0;?>
											<?php foreach($subskillRows as $subskillRow)
											{  
												if($j >= $roundedWidthcountforsubmain1 && $i < $sizeofsubmain1)
												{?>
													<li><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $subskillRow->skill_id;?>"><?php echo $subskillRow->skill_name;?></a></li>
												<?php } ?>
			
										  <?php $j++;}?>
									</div>
									<?php }else
									{ ?>
											<?php $i=0; foreach($subskillRows as $subcategory){ ?>	
											<li><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $subcategory->skill_id;?>" ><?php echo $subcategory->skill_name;?></a>
											<?php $i++; }?>
									<?php } ?>
									<?php
									$skillcategory = $subcategory->skill_id;
									$skillcategoryRows =$mydb->get_results("SELECT `s`.* FROM `sz_master_skills_for_diplay_listing` AS `s` WHERE (is_enabled = 1 && is_skill = 1 && parent_skill_id = '$skillcategory'  && skill_id!=0) ORDER BY `skill_name` asc");			

										  
									if(isset($skillcategoryRows) && sizeof($skillcategoryRows)>0)
									{
											$recordSize = sizeof($skillcategoryRows);
											$recordSize1 = sizeof($skillcategoryRows);
											//echo ($recordSize % 10);
											if ($recordSize % 10 == 0)
											{
												$widthCount = ($recordSize / 10);
											}else
											{
												$widthCount = ($recordSize / 10) + 1;
											}
											$roundedWidthcount = floor($widthCount);
											if (isset($recordSize) && $recordSize > 20 )
											{
												$roundedWidthcount = $roundedWidthcount-1;
											}
											$subdiv = -1;
											?>
							
							
											<ul style="width:<?php echo ($roundedWidthcount * 150);?>px;">
													<?php if(isset($skillcategoryRows) && sizeof($skillcategoryRows) > 10 )
													{ 
				
															if ($recordSize1 % 2 == 0)
															{
																	$widthCount1 = ($recordSize1 / 2);
															}else
															{
																	$widthCount1 = ($recordSize1 / 2) + 1;
															}
															$roundedWidthcount1 = floor($widthCount1);
															?>
															
															<div style="width:150px;float:left;display:inline-block;">
																<?php $j=0;?>
																<?php foreach($skillcategoryRows as $skillcategoryRow)
																{  
																		if($j < $roundedWidthcount1)
																		{?>
																			<li><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $skillcategoryRow->skill_id;?>"><?php echo $skillcategoryRow->skill_name;?></a></li>		
																		<?php } ?>
																		
																<?php $j++;}?>
															</div>
																													
															<div style="width:150px;float:left;display:inline-block;">
															<?php $j=0;?>
															<?php foreach($subskillRows as $subskillRow)
															{  
																	if($j >= $roundedWidthcount1 && $i < $recordSize1)
																	{ ?>
																		<li><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $subskillRow->skill_id;?>"><?php echo $subskillRow->skill_name;?></a></li>
																	<?php } ?>
					
															<?php $j++;}?>
															</div>
															
													
													<?php }
													else{ ?>
											
															<div style="width:150px;float:left;display:inline-block;">
															<?php foreach($skillcategoryRows as $skillcategory)
															{?>
																	<li><a href="http://skillzot.com/testdata/search/index/sessionunsetvar/1/skillid/<?php echo $skillcategory->skill_id;?>" ><?php  echo $skillcategory->skill_name;?></a></li>
															<?php }?>
															</div>
													<?php }?>
											</ul>
								<?php } ?>
							</li>
								
								
							</ul>
							<?php }	?>
	        	
				</li>
				<?php }?>
	      <?php }?>
		</ul>
		</div>  
 		</div>
		
</div><!-- #header -->
<div id="main">