var slideshow2_noFading = false;
var slideshow2_timeBetweenSlides =8000;	// Amount of time between each image(1000 = 1 second)
var slideshow2_fadingSpeed = 20;	// Speed of fading	(Lower value = faster)
var slideshow2_stats = new Array();
var slideshow2_slideIndex = new Array();	// Index of current image shown
var slideshow2_slideIndexNext = new Array();	// Index of next image shown
var slideshow2_imageDivs = new Array();	// Array of image divs(Created dynamically)
var slideshow2_currentOpacity = new Array();	// Initial opacity
var slideshow2_imagesInGallery = new Array();	// Number of images in gallery
var Opera = navigator.userAgent.indexOf('Opera')>=0?true:false;
function createParentDivs(imageIndex,divId)
{
	if(imageIndex==slideshow2_imagesInGallery[divId]){	
		showGallery(divId);
	}else{
		var imgObj = document.getElementById(divId + '_' + imageIndex);	
		if(Opera)imgObj.style.position = 'static';
		if(!slideshow2_imageDivs[divId])slideshow2_imageDivs[divId] = new Array();
		slideshow2_imageDivs[divId][slideshow2_imageDivs[divId].length] =  imgObj;

		imgObj.style.visibility = 'hidden';	
		imageIndex++;
		createParentDivs(imageIndex,divId);	
	}		
}
function showGallery(divId)
{
	if(slideshow2_slideIndex[divId]==-1)slideshow2_slideIndex[divId]=0; else slideshow2_slideIndex[divId]++;	// Index of next image to show
	if(slideshow2_slideIndex[divId]==slideshow2_imageDivs[divId].length)slideshow2_slideIndex[divId]=0;
	slideshow2_slideIndexNext[divId] = slideshow2_slideIndex[divId]+1;	// Index of the next next image
	if(slideshow2_slideIndexNext[divId]==slideshow2_imageDivs[divId].length)slideshow2_slideIndexNext[divId] = 0;

	
	slideshow2_currentOpacity[divId]=100;	// Reset current opacity

	// Displaying image divs
	slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.visibility = 'visible';
	if(Opera)slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.display = 'inline';
	if(navigator.userAgent.indexOf('Opera')<0){
		slideshow2_imageDivs[divId][slideshow2_slideIndexNext[divId]].style.visibility = 'visible';
	}
	
	if(document.all){	// IE rules
		slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.filter = 'alpha(opacity=100)';
		slideshow2_imageDivs[divId][slideshow2_slideIndexNext[divId]].style.filter = 'alpha(opacity=1)';
	}else{
		slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.opacity = 0.99;	// Can't use 1 and 0 because of screen flickering in FF
		slideshow2_imageDivs[divId][slideshow2_slideIndexNext[divId]].style.opacity = 0.01;
	}		
	

	tt=setTimeout('revealImage("' + divId + '")',slideshow2_timeBetweenSlides);		
}
function revealImage(divId)
{ 
    
	if(slideshow2_noFading){
		slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.visibility = 'hidden';
		if(Opera)slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.display = 'none';
		showGallery(divId);
		return;
	}
	slideshow2_currentOpacity[divId]--;
	if(document.all){
		slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.filter = 'alpha(opacity='+slideshow2_currentOpacity[divId]+')';
		slideshow2_imageDivs[divId][slideshow2_slideIndexNext[divId]].style.filter = 'alpha(opacity='+(100-slideshow2_currentOpacity[divId])+')';
	}else{
		slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.opacity = Math.max(0.01,slideshow2_currentOpacity[divId]/100);	
		// Can't use 1 and 0 because of screen flickering in FF
		slideshow2_imageDivs[divId][slideshow2_slideIndexNext[divId]].style.opacity = Math.min(0.99,(1 - (slideshow2_currentOpacity[divId]/100)));
	}
	if(slideshow2_currentOpacity[divId]>0){
		t=setTimeout('revealImage("' + divId + '")',slideshow2_fadingSpeed);
	}else{
		slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.visibility = 'hidden';	
		if(Opera)slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.display = 'none';		
		
		document.getElementById("content0").style.color='#818181';
		document.getElementById("content1").style.color='#818181';
		document.getElementById("content2").style.color='#818181';
//		document.getElementById("content3").style.color='#818181';
//		document.getElementById("content4").style.color='#818181';
//		document.getElementById("content5").style.color='#818181';
		//document.getElementById("content3").style.color='#3399cc';
		if (slideshow2_slideIndex[divId]==0)
		{
			document.getElementById("topban").href="#";
		}
			else if (slideshow2_slideIndex[divId]==1)
		{  
			document.getElementById("topban").href="#";
		}
			
		document.getElementById("content"+slideshow2_slideIndex[divId]).style.color='#000000';
		showGallery(divId);
		
	}
}

function initImageGallery(divId)
{
	var slideshow2_galleryContainer = document.getElementById(divId);
	
	
	slideshow2_slideIndex[divId] = -1;
	slideshow2_slideIndexNext[divId] = false;
	
	var galleryImgArray = slideshow2_galleryContainer.getElementsByTagName('IMG');
	for(var no=0;no<galleryImgArray.length;no++){
		galleryImgArray[no].id = divId + '_' + no;
	}
	
	slideshow2_imagesInGallery[divId] = galleryImgArray.length;
	createParentDivs(0,divId);		
	changeslide("0","imageSlideshowHolder");
	
}

function changeslide(id,divId)
{	clearTimeout(tt);clearTimeout(t);
slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.visibility = 'hidden';	
		if(Opera)slideshow2_imageDivs[divId][slideshow2_slideIndex[divId]].style.display = 'none'; slideshow2_slideIndex[divId]=id;
slideshow2_imageDivs[divId][id].style.visibility = 'hidden';	
		if(Opera)slideshow2_imageDivs[divId][id].style.display = 'block';	
	
		document.getElementById("content0").style.color='#818181';
		document.getElementById("content1").style.color='#818181';
		document.getElementById("content2").style.color='#818181';
//		document.getElementById("content3").style.color='#818181';
//		document.getElementById("content4").style.color='#818181';
//		document.getElementById("content5").style.color='#818181';
		//document.getElementById("content3").style.color='#3399cc';
		
		if (slideshow2_slideIndex[divId]==0)
		{
			document.getElementById("topban").href="#";
		}
			else if (slideshow2_slideIndex[divId]==1)
		{  
			document.getElementById("topban").href="#";
		}
			else if (slideshow2_slideIndex[divId]==1)
		{  
			document.getElementById("topban").href="#";
		}
			
		
		document.getElementById("content"+slideshow2_slideIndex[divId]).style.color='#000000';

	
showGallery(divId);
		
		
}
var tt,t;

function tabcpp(id,total){
//img=id.substring(1,2,3);
img=id.substring(1,2);
var tabnames = ['tab-product','tab-channel'];
for(i=1; i<=total; i++){ 
document.getElementById('re'+i).style.display = 'none';
document.getElementById('_'+i+tabnames[i-1]).src="images/"+tabnames[i-1]+"-normal.gif";
document.getElementById('_'+i+tabnames[i-1]).parentNode.style.cursor="pointer";
}
document.getElementById('re'+img).style.display = 'block';
document.getElementById(id).src="images/"+tabnames[img-1]+"-over.gif";
document.getElementById(id).parentNode.style.cursor="default";	

}

function preloadimages()
{
document.write('	<div style="display:none;"><img src="images/tab-faq-normal.gif" border="0" alt="" title=""  /> <img src="images/tab-faq-over.gif" border="0" alt="" title=""  /> <img src="images/tab-key-ben-normal.gif" border="0" alt="" title=""  /><img src="images/tab-key-ben-over.gif" border="0" alt="" title=""  /><img src="images/tab-plan-details-normal.gif" border="0" alt="" title=""  /><img src="images/tab-plan-details-over.gif" border="0" alt="" title=""  /></div>')
}

function showhide(src,img){
	var imgopen = 'images/minus.gif';
	var imgclose = 'images/plus.gif';

		if(document.getElementById(src).style.display == 'none'){
			document.getElementById(src).style.display = 'block';
			document.getElementById(img).src = imgopen;

		}
		else 
		{
			document.getElementById(src).style.display = 'none';
			document.getElementById(img).src = imgclose;
		}
}

function footer()
{
	document.write("<h5>    <a href=\"#\">Careers</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Group</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Foundation</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Lombard General Insurance</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Securities</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Prudential Life Insurance</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Prudential AMC</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Venture</a>&nbsp;|<br />&nbsp;<a href=\"#\">ICICI Direct</a>&nbsp;|&nbsp;<a href=\"#\">DISHA Financial Counselling</a>&nbsp;|&nbsp;<a href=\"#\">ICICI Home Finance</a>&nbsp;|&nbsp;<a href=\"#\">Erstwhile The Bank of Rajasthan</a>&nbsp;|    </h5>    <p>    &nbsp;<a href=\"#\">Terms &amp; Conditions</a>&nbsp;|&nbsp;<a href=\"#\">Interest Rates</a>&nbsp;|&nbsp;<a href=\"#\">Service Charges &amp; Fees</a>&nbsp;|&nbsp;<a href=\"#\">Form Center</a>&nbsp;|&nbsp;<a href=\"#\">Do Not Call Registry</a>&nbsp;|&nbsp;<a href=\"#\">Disclaimer</a>&nbsp;|&nbsp;<a href=\"#\">Code of Commitment</a>&nbsp;|&nbsp;<a href=\"#\">Group Code of Business Conduct and Ethics</a>&nbsp;|&nbsp;<a href=\"#\">Basel II Disclosures</a>&nbsp;|&nbsp;<br /><a href=\"#\">Notice Board</a>&nbsp;|&nbsp;<a href=\"#\">Use of unparliamentary language by customers</a>&nbsp;|&nbsp;<a href=\"#\">E-Brochures</a>&nbsp;|&nbsp;<a href=\"#\">Privacy</a>&nbsp;|&nbsp;<a href=\"#\">USA Patriot Act Certification</a>&nbsp;|&nbsp;<a href=\"#\">Safe Banking</a>&nbsp;|&nbsp;<a href=\"#\">Go Green</a>&nbsp;|&nbsp;<a href=\"#\">Gallery </a>&nbsp;|        </p>    <div class=\"yellowstrip\">    Best viewed with Internet Explorer Ver 5.5 and Ver 6 or Firefox Ver 1.5 with a resolution of 1024x768.    </div>");
	
	}
 
 function ClickHereToPrint(){
 try{
  var oIframe = document.getElementById('ifrmPrint');
  var oContent = document.getElementById('TableContentDiv').innerHTML;
  var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
 if (oDoc.document) oDoc = oDoc.document;
  oDoc.write("<head><title>title</title>");
  oDoc.write("</head><body onload='this.focus(); this.print();'>");
  oDoc.write(oContent + "</body>");
  oDoc.close();
 }
 catch(e){
 self.print();
 }
}