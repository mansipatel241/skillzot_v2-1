function clearit(elem) {
	if(elem.value == getAttribute(elem.attributes,"szprompt")) {
		elem.value = "";
	}
}

function fixit(elem) {
	if(elem.value == "") {
		elem.value = getAttribute(elem.attributes,"szprompt");
	}
	
}


function getAttribute(elem,attr) {
	var data;
	var cnt = elem.length;
	for(var i=0; i<cnt;i++) {
		if(elem[i].name == attr) {
			data = elem[i].value;
			break; 
		}
	}
	
	return data;
}

function addRow(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount - 1);

    var colCount = table.rows[0].cells.length;

    for(var i=0; i<colCount; i++) {

        var newcell = row.insertCell(i);

        newcell.innerHTML = table.rows[0].cells[i].innerHTML;
        //alert(newcell.childNodes);
        switch(newcell.childNodes[0].type) {
            case "text":
                    newcell.childNodes[0].value = "";
                    break;
            case "checkbox":
                    newcell.childNodes[0].checked = false;
                    break;
            case "select-one":
                    newcell.childNodes[0].selectedIndex = 0;
                    break;
        }
    }
}

function deleteRow(tableID) {
    try {
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;

    for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];
        if(null != chkbox && true == chkbox.checked) {
            if(rowCount <= 1) {
                alert("Cannot delete all the rows.");
                break;
            }
            table.deleteRow(i);
            rowCount--;
            i--;
        }

    }
    }catch(e) {
        alert(e);
    }
}

function verifyStep1() {
	
	var isError = false;	
	
	var firstname = document.forms["register_step1"]["firstname"].value;
	document.getElementById('firstname_label').style.visibility = "hidden";
	if(!isValidTextInput(firstname,"Firstname")) {
		//firstname is invalid
		document.getElementById('firstname_label').style.visibility = "visible";
		isError = true;
	}
	
	document.getElementById('lastname_label').style.visibility = "hidden";
	var lastname = document.forms["register_step1"]["lastname"].value;
	if(!isValidTextInput(lastname,"Lastname")) {
		//lastname is invalid
		document.getElementById('lastname_label').style.visibility = "visible";
		isError = true;
	}

	document.getElementById('email_label').style.visibility = "hidden";
	var email = document.forms["register_step1"]["email"].value;
	if(!isValidEmailFormat(email)) {
		//email is invalid
		document.getElementById('email_label').style.visibility = "visible";
		isError = true;	
	}

	document.getElementById('password_label').style.visibility = "hidden";
	var password = document.forms["register_step1"]["password"].value;
	var confirm_password = document.forms["register_step1"]["confirm_password"].value;

	if(!isValidPassword(password,confirm_password)) {
		//password is invalid
		document.getElementById('password_label').style.visibility = "visible";
		isError = true;
	}

	document.getElementById('address_label').style.visibility = "hidden";
	var address = document.forms["register_step1"]["address"].value;
	if(!isValidTextInput(address,"")) {
		//address is invalid
		document.getElementById('address_label').style.visibility = "visible";
		isError = true;
	}

	var locality = document.forms["register_step1"]["locality"].value;
	var city = document.forms["register_step1"]["city"].value;

	document.getElementById('pincode_label').style.visibility = "hidden";
	var pincode = document.forms["register_step1"]["pincode"].value;
	if(!isValidTextInput(pincode,"")) {
		//pincode is invalid
		document.getElementById('pincode_label').style.visibility = "visible";
		isError = true;
	}

	var mobile = document.forms["register_step1"]["mobile"].value;

	var landline = document.forms["register_step1"]["landline"].value;

	if(!isError) {
		document.register_step1.submit();
	}
}

function isValidTextInput(value,default_value) {
	if(value == default_value) {
		return false;
	}
	return true;
}


function isValidPassword(password,confirm_password) {
	if(!isValidTextInput(password,"")){
		return false;
	}
	if(password != confirm_password) {
		return false;
	}
	
	return true;
}



function isValidEmailFormat(email) {
	
	var atpos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
	  return false;
	}
	return true;
}