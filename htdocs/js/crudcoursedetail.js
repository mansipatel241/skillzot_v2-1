function saveForm(url,form_id,container_id,params,button_id){
	
	// validation of input arguments
	if(url==null || form_id==null || container_id==null)jAlert("Invalid or Insufficient Arguments");
	if(params==null)params="";
	if(button_id==null)button_id="";
	var containerObject = document.getElementById(container_id);
	var elementList = containerObject.getElementsByTagName('*');
	var postData = "";
	
	// loop for concatenating all the html elements in the postData string
	for(var i=0;i<elementList.length; i++){
		
		if(elementList[i].id!=null && elementList[i].id!="" && elementList[i].value!=null){
			
			if(elementList[i].type=="checkbox"){
				if(elementList[i].checked){
					if(postData=="")postData = elementList[i].id+"="+elementList[i].value;
					else postData += "&"+elementList[i].id+"="+elementList[i].value;
				}
			}else if(elementList[i].getAttribute("texttype")=="ckeditor_desc"){
				if(postData=="")postData = elementList[i].id+"="+CKEDITOR.instances.PageDescription.getData();
				else postData += "&"+elementList[i].id+"="+CKEDITOR.instances.PageDescription.getData();
			}else if(elementList[i].getAttribute("texttype")=="ckeditor_termscond"){
				if(postData=="")postData = elementList[i].id+"="+CKEDITOR.instances.TermsConditions.getData();
				else postData += "&"+elementList[i].id+"="+CKEDITOR.instances.TermsConditions.getData();
			}else if(elementList[i].getAttribute("texttype")=="ckeditor_longdesc"){
				if(postData=="")postData = elementList[i].id+"="+CKEDITOR.instances.LongDescription.getData();
				else postData += "&"+elementList[i].id+"="+CKEDITOR.instances.LongDescription.getData();
			}else if(elementList[i].getAttribute("texttype")=="ckeditor_termspecial"){
				if(postData=="")postData = elementList[i].id+"="+CKEDITOR.instances.TermsConditions.getData();
				else postData += "&"+elementList[i].id+"="+CKEDITOR.instances.TermsConditions.getData();
			}else if(elementList[i].getAttribute("texttype")=="ckeditor_longspecial"){
				if(postData=="")postData = elementList[i].id+"="+CKEDITOR.instances.LongDescription.getData();
				else postData += "&"+elementList[i].id+"="+CKEDITOR.instances.LongDescription.getData();
			}else if(elementList[i].getAttribute("texttype")=="ckeditor_overviewdesc"){
				if(postData=="")postData = elementList[i].id+"="+CKEDITOR.instances.OverviewDescription.getData();
				else postData += "&"+elementList[i].id+"="+CKEDITOR.instances.OverviewDescription.getData();
			}
			else{
				if(postData=="")postData = elementList[i].id+"="+elementList[i].value;
				else postData += "&"+elementList[i].id+"="+elementList[i].value;
			}
		}
	}
	
	// loop for concatenating any additional parameters in postData string
	if(params instanceof Array){
		
		for(key in params){
			if(postData=="")postData = key+"="+params[key];
			else postData += "&"+key+"="+params[key];
		}
	}
	
	if(button_id!=""){
		document.getElementById(button_id).disabled = true;
		document.getElementById(button_id).value = "Please Wait";
	}
	
	if(params!=""){
		document.getElementById(params).style.display = "block";
	}
	
	// ajax call for form validation
	$.ajax({
		url: url,
		type: "post",
		data: postData,
		success: function(response){
			
			if(button_id!=""){
				document.getElementById(button_id).disabled = false;
				document.getElementById(button_id).value = "Save";
			}
			
			if(params!=""){
				document.getElementById(params).style.display = "none";
			}
			
			var jsonObject = eval("(" + response + ")");
			if(jsonObject!=null && jsonObject!=""){
			
			if(jsonObject['returnvalue']=="success"){
				document.getElementById(form_id).submit();
				
			}
			else if(jsonObject['returnvalue']=="validation"){
				
				var validationData = jsonObject['data'];
				
				for(key in validationData){

					var currentObject = document.getElementById(key);
					if(currentObject!=null){
						
						var currentErrorObject = document.getElementById(key+"_error");
						if(validationData[key]=="null") {currentErrorObject.innerHTML = "Please enter your " + currentObject.getAttribute("errortag"); } 
						else if(validationData[key]=="ratenull"){ 
							if(document.getElementById(key).value=="Fees or fee range")
								{
									currentErrorObject.innerHTML = "Please enter your fees";
								}else{
									currentErrorObject.innerHTML = "Please select your fees type";
								}
						document.getElementById("rate").focus();
						}
						else if(validationData[key]=="descriptionnull"){ currentErrorObject.innerHTML = "Please enter your " + currentObject.getAttribute("errortag");
						//document.getElementById(key).focus();
						}
						else if(validationData[key]=="selectatleast")currentErrorObject.innerHTML = "Please select at least one " + currentObject.getAttribute("errortag");
						
						else if(validationData[key]=="allfield")currentErrorObject.innerHTML = " All the above fields need to be entered and cannot be left blank.";
						else if(validationData[key]=="coursenamenull"){ currentErrorObject.innerHTML = "Please enter " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						}
						else if(validationData[key]=="agegroupnull"){ currentErrorObject.innerHTML = "Please enter " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						}
						else if(validationData[key]=="formatnull"){ currentErrorObject.innerHTML = "Please enter " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						}
						else if(validationData[key]=="daysofweeknull"){ currentErrorObject.innerHTML = "Please enter your " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						}
						else if(validationData[key]=="shedulenull"){ currentErrorObject.innerHTML = "Please enter your " + currentObject.getAttribute("errortag");
						document.getElementById(key).focus();
						}
						
						else if(validationData[key]=="tavelradiusnull"){currentErrorObject.innerHTML = "Please enter your " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						}
						
						else if(validationData[key]=="paymentnull"){currentErrorObject.innerHTML = "Please select your " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						}
						
						
						else if(validationData[key]=="onlyenternull")currentErrorObject.innerHTML = "Please enter " + currentObject.getAttribute("errortag");
						else if(validationData[key]=="imageuploaderror")currentErrorObject.innerHTML = "Please upload an image";
						else if(validationData[key]=="wrongpass")currentErrorObject.innerHTML = "You have type wrong password";
						else if(validationData[key]=="statuserror")currentErrorObject.innerHTML ="Need to select at least one check box.";
						else if(validationData[key]=="onlyselectnull"){ currentErrorObject.innerHTML = "Please select " + currentObject.getAttribute("errortag");
						
						document.getElementById(key).focus();
						
						}
						else if(validationData[key]=="selectnull")currentErrorObject.innerHTML = "Please select your " + currentObject.getAttribute("errortag");
						else if(validationData[key]=="durationnumeric") { currentErrorObject.innerHTML = "Please enter only numbers, e.g. 14, 5, 1.5.";
						
						if(isNaN(document.getElementById("no_of_wks").value) != false)
						{
							document.getElementById("no_of_wks").focus();
						}else if(isNaN(document.getElementById("no_of_days_per_wks").value) != false){
							document.getElementById("no_of_days_per_wks").focus();
						}else if(isNaN(document.getElementById("no_of_hrs_per_day").value) != false){
							document.getElementById("no_of_hrs_per_day").focus();
						}else{}
					
						}
						else if(validationData[key]=="durationnull"){ currentErrorObject.innerHTML = "All the 3 above fields need to be selected please";
						
							if(document.getElementById("no_of_wks").value =="No. of weeks")
							{
								document.getElementById("no_of_wks").focus();
							}else if(document.getElementById("no_of_days_per_wks").value =="No. of days/wk"){
								document.getElementById("no_of_days_per_wks").focus();
							}else if(document.getElementById("no_of_hrs_per_day").value =="No. of hours/day"){
								document.getElementById("no_of_hrs_per_day").focus();
							}else{}
						
						}
						
						else if(validationData[key]=="selectskill")
						{ currentErrorObject.innerHTML = "Please select at least one " + currentObject.getAttribute("errortag");
						
						//document.getElementById("hiddenVal2").focus();
						
						}
						else if(validationData[key]=="selectagegroup")
						{ currentErrorObject.innerHTML = "Please select at least one " + currentObject.getAttribute("errortag");
												
						//document.getElementById("hiddenVal3").focus();						
												
						}
												
						else if(validationData[key]=="selectlanguage"){ currentErrorObject.innerHTML = "Please select at least one " + currentObject.getAttribute("errortag");
						
						//document.getElementById("travel_radius").focus();
						
						}
						
						else if(validationData[key]=="charstrength")currentErrorObject.innerHTML = currentObject.getAttribute("errortag") + " must be at least 6 characters long";
						else if(validationData[key]=="allfield")currentErrorObject.innerHTML ="All the above fields need to be entered and cannot be left blank.";
						else if(validationData[key]=="mobilelandlineinvalid")currentErrorObject.innerHTML ="Oops! Seems you've typed less than 10 digits. Please re-enter number.OR, please enter a landline number.";
						else if(validationData[key]=="yearerror")currentErrorObject.innerHTML ="Year 'From' cannot be greater than year 'To'.";
						else if(validationData[key]=="unmatch")currentErrorObject.innerHTML = currentObject.getAttribute("errortag") + " does not match with password";
						else if(validationData[key]=="uncheck")currentErrorObject.innerHTML = "Please accept the " + currentObject.getAttribute("errortag");
						else if(validationData[key]=="emailinvalid")currentErrorObject.innerHTML = "Oops! That seems to be an invalid email. Please re-enter email.";
						else if(validationData[key]=="conformpassnull")currentErrorObject.innerHTML = "Please confirm your password";
						else if(validationData[key]=="emailduplicate")currentErrorObject.innerHTML = "This email already exists. Enter another email or try to log in.";
						else if(validationData[key]=="passlength")currentErrorObject.innerHTML = "Password needs to be at least 5 characters long.";
						else if(validationData[key]=="passnotmatch")currentErrorObject.innerHTML = "Oops! The passwords don't seem to match";
						else if(validationData[key]=="pincodeinvalid")currentErrorObject.innerHTML = "Your Pin code seems to be invalid. Please re-enter Pin code.";
						else if(validationData[key]=="mobileinvalid")currentErrorObject.innerHTML = "Please enter a valid phone number (Numbers only..no letters, spaces or special characters)";
						else if(validationData[key]=="mobilelength")currentErrorObject.innerHTML = "Oops! Seems you've typed less than 10 digits. Please re-enter number.";
						else if(validationData[key]=="invalid")currentErrorObject.innerHTML = "Invalid value for " + currentObject.getAttribute("errortag");
						else if(validationData[key]=="notmatch")currentErrorObject.innerHTML = currentObject.getAttribute("errortag") + "entered not matching";
						else if(validationData[key]=="wrong")currentErrorObject.innerHTML =  "Wrong " + currentObject.getAttribute("errortag");
						else if(validationData[key]=="duplicate")currentErrorObject.innerHTML = "This " + currentObject.getAttribute("errortag") + " is already registered";
						else if(validationData[key]=="either")currentErrorObject.innerHTML = "Either enter value for " + currentObject.getAttribute("errortag") + " or Special Offer Banner Content";
						else if(validationData[key]=="duplicate_combination")currentErrorObject.innerHTML = "This " + currentObject.getAttribute("errortag2") + " is already registered";
						else if(validationData[key]=="notcombination")currentErrorObject.innerHTML = "This " + currentObject.getAttribute("errortag2") + " is not registered";
						else if(validationData[key]=="valid")currentErrorObject.innerHTML = "&nbsp;";
						else{
							currentValidationData = jsonObject['data'][key];
							if(currentValidationData['code']!=null){
								if(currentValidationData['code']=="nomatch")currentErrorObject.innerHTML = currentObject.getAttribute("errortag") + " does not match the " + document.getElementById(currentValidationData['field']).getAttribute("errortag") + " field";
								if(currentValidationData['code']=="match")currentErrorObject.innerHTML = currentObject.getAttribute("errortag") + " cannot be same as " + document.getElementById(currentValidationData['field']).getAttribute("errortag") + " field";
							}
							//else currentErrorObject.innerHTML = "unknown response code";
						}
					}
				}
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			jAlert("This operation could not be completed, Please try again. Contact system admin if problem persists");
		}	
	});
}

function deleteRecord(url,id_list,params){
	
	// validation of input arguments
	if(params==null)params="";
	if(url==null||id_list==""||id_list==null)jAlert("Invalid or Insufficient Arguments");
	var postData = "";
	
	$.alerts.okButton = "&nbsp;&nbsp;yes&nbsp;&nbsp;";
	$.alerts.cancelButton = "&nbsp;&nbsp;no&nbsp;&nbsp;";
	
	jConfirm("Are you sure you want to delete this record?","Confirmation",function(r){
		
		$.alerts.okButton = "&nbsp;OK&nbsp;";
		$.alerts.cancelButton = "&nbsp;Cancel&nbsp;";
		
		if(r){
			id_string="";
			if(id_list instanceof Array ){			
				for(var i=0;i<id_list.length;i++){
					if(id_string=="")
						id_string+=id_list[i];
					else
						id_string+=","+id_list[i];					
				}
			}
			else id_string=id_list;
			
			postData="id="+id_string;

			// loop for concatenating any additional parameters in postData string
			if(params instanceof Array){
				
				for(key in params){
					if(postData=="")postData = key+"="+params[key];
					else postData += "&"+key+"="+params[key];
				}
			}
			
			$.ajax({
				
				url: url,
				data: postData,
				type: "post",
				async: 'false',
				success: function(response){
				
					if(response!="" && response!=null){
						
						var jsonObject = eval('('+response+')');
						
						if(jsonObject !="" && jsonObject!=null){	
							if( jsonObject.returnvalue=="error"){
								jAlert(jsonObject.errormsg);
							}
							else if(jsonObject.returnvalue=="success"){
								jAlert("Value deleted successfully");
								window.location.reload();
							}
							else
								jAlert("This operation could not be completed, Please try again. Contact system admin if problem persists");
						}
					}
					else{
						window.location.reload();
					}	
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					jAlert("This operation could not be completed, Please try again. Contact system admin if problem persists");
				}
			});
		}
	});
}