<?php

class AdminnewController extends Zend_Controller_Action{

public function init(){
	
	$this->_helper->layout()->setLayout("adminnew");
}

public function indexAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(isset($authUserNamespace->user_id) || $authUserNamespace->user_id!="")$this->_redirect("/adminnew/home");
	
	$this->_helper->layout()->setLayout("loginnew");
	
	if($this->_request->isPost()){
	
		$username = $this->_request->getParam('username');
		$password = $this->_request->getParam('password');
		//mysql_connect("localhost", "root", "root"); // main site
		//mysql_connect("localhost", "pronto_skiilzot", "prontoskiilZOT"); // pronto
		//mysql_connect("localhost", "pronto_skiilzot", "Skillzot2012");// slash skillzot
		$usernameescape = $username;
		$passwordescape = md5($password);
		$adminlogin = new Skillzot_Model_DbTable_Adminlogin();
		$user_row = $adminlogin->fetchRow($adminlogin->select()
										 ->where("admin_uname='$usernameescape' && admin_pwd='$passwordescape' && is_active=1"));

		if($user_row!="" && sizeof($user_row)>0){
		
			$authUserNamespace->user_id = $user_row->id;
			$authUserNamespace->user_name = $user_row->admin_uname;
			
			$this->view->msg = "";
			
			$this->_redirect('/adminnew/home');
		
		}else{
			$this->view->msg = "inactive";
		}
	}
}

public function homeAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Home";
}

public function cmslistAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	
}

public function categoryAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->setLayout("adminnew");
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage UI Category";
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
	$records_per_page = $this->_request->getParam('shown');
	$this->view->records_per_page = $records_per_page;
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	$this->view->search_val = stripcslashes ($searchText);
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	
	if($searchText!="")
	{
		$skillrowResult =$skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"))
						->where("s.parent_skill_id='0' && s.skill_id!='0' && skill_depth='1' && s.skill_name like '%".$searchText."%'")
						->order(array("order_id asc")));
											
	}
	else
	{
		//echo "sdf";exit;
		$skillrowResult = $skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"))
						->where("s.parent_skill_id='0' && s.skill_id!='0' && skill_depth='1'")
						->order(array("order_id asc")));
	
	}

		/*pagination code*/
	if (isset($skillrowResult) && $skillrowResult!="")
	{
		$this->view->skillrowResult = $skillrowResult;
	}
	

	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($skillrowResult);
	$paginator = Zend_Paginator::factory($skillrowResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->skillrowResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
											   
}

public function subcategoryAction(){	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->setLayout("adminnew");
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage UI Category";
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($searchText!="")
	{
		$skillrowResult =$skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"))
						->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='2' && s.skill_name like '%".$searchText."%'")
						->order(array("order_id asc")));
											
	}
	else
	{
		//echo "sdf";exit;
		$skillrowResult = $skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"))
						->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='2'")
						 ->order(array("order_id asc")));
	
	}
	if (isset($skillrowResult) && $skillrowResult!="")
	{
		$this->view->skillrowResult = $skillrowResult;
	}
		/*pagination code*/
		
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($skillrowResult);
	$paginator = Zend_Paginator::factory($skillrowResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->skillrowResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
}
	
public function skillsAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->setLayout("adminnew");
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage UI Skill";
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	
	if($searchText!="")
	{
		$skillrowResult =$skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"))
						->where("s.parent_skill_id!='0' && s.skill_id!='0' && s.skill_depth!='0' && skill_depth!='1' && s.skill_name like '%".$searchText."%'")
						->order(array("order_id asc")));
											
	}
	else
	{
		$skillrowResult = $skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"))
						->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth!='0' && skill_depth!='1'")
						->order(array("order_id asc")));

	}

	if(isset($skillrowResult) && $skillrowResult!="")
	{
		$this->view->skillrowResult = $skillrowResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($skillrowResult);
	$paginator = Zend_Paginator::factory($skillrowResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->skillrowResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
											   
}

public function addfinalskillsAction()
{
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->disableLayout();
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
		if($this->_request->isPost()){
		
			if($this->_request->isXmlHttpRequest()){
			$category_id = $this->_request->getParam("category_id");
			$category_rows = $skillObj->fetchAll($skillObj->select()
														  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
														  ->where("s.parent_skill_id!='0' && s.skill_id!='0' && parent_skill_id='$category_id' && skill_depth='2'"));
			$i=0;
			$tags = "";
			foreach($category_rows as $e){
				$tags[$i]['skill_name'] = $e->skill_name;
				$tags[$i]['skill_id'] = $e->skill_id;
				//$arry[]=$tags;
				$i++;
			}
			echo json_encode($tags);exit;
			}
  		}
}

public function addskillsAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightbox");
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
	$skillrowResult = $skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
						->where("s.parent_skill_id='0' && s.skill_id!='0'"));
	$this->view->maincategory = $skillrowResult;
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	
	if(isset($id) && $id > 0)
	{
		$fetch_data = $skillObj->fetchRow("skill_id='$id'");
		$this->view->category =$fetch_data ;
	}

	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Skill";
	}else{
	    $authUserNamespace->admin_page_title = "Add Skill";
	}

	if($this->_request->isPost()){

		$category_id = $this->_request->getParam("Category_name");
		$subcategory_name = $this->_request->getParam("categories_sub");
		$skill_name = $this->_request->getParam("Skill_name");
		$skill_description = $this->_request->getParam("SkillDescription");
		$enable = $this->_request->getParam("Enable");
		$menuable = $this->_request->getParam("menuable");
		$ordernunber = $this->_request->getParam("OrderNumber");
		$hiddenid= $this->_request->getParam("hiddenvar");
		
		$urlName= $this->_request->getParam("urlName");
		$pageTitle= $this->_request->getParam("skillPagetitle");


		if($this->_request->isXmlHttpRequest()){

			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();

			if($category_id == "")$response["data"]["Category_name"] = "null";
			else $response["data"]["Category_name"] = "valid";
			
			if($skill_name == "")$response["data"]["Skill_name"] = "null";
			else $response["data"]["Skill_name"] = "valid";
			
//				if($subcategory_name == "")$response["data"]["categories_sub"] = "null";
//				else $response["data"]["categories_sub"] = "valid";
			
			if($skill_description == "")$response["data"]["SkillDescription"] = "null";
			else $response["data"]["SkillDescription"] = "valid";
			
			if($urlName == "")$response["data"]["urlName"] = "null";
			else $response["data"]["urlName"] = "valid";
			
			if($pageTitle == "")$response["data"]["skillPagetitle"] = "null";
			else $response["data"]["skillPagetitle"] = "valid";

//				if (isset($subcategory_name) && $subcategory_name!="")
//				{
				$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name' && skill_id!=0");
				if (isset($SkillNameData) && sizeof($SkillNameData) > 0)
				{
					$parent_id = $SkillNameData->skill_id;
				}else{
					$parent_id = $category_id;
				
				}
					if(isset($id) && $id !="")
					{
						$order_exist = $skillObj->fetchAll("skill_id!='$hiddenid' && order_id='$ordernunber' && parent_skill_id!='0' && parent_skill_id=$parent_id");
					}else{
						$order_exist = $skillObj->fetchAll("order_id='$ordernunber' && parent_skill_id!='0' && parent_skill_id='$parent_id'");
					}
//				}
			if($ordernunber == "")$response["data"]["OrderNumber"] = "null";
			elseif(isset($order_exist) && sizeof($order_exist) > 0)$response["data"]["OrderNumber"] = "duplicate";
			else $response["data"]["OrderNumber"] = "valid";

			if($enable == "")$response["data"]["Enable"] = "null";
			else $response["data"]["Enable"] = "valid";
			
			if($menuable == "")$response["data"]["menuable"] = "null";
			else $response["data"]["menuable"] = "valid";

			if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";

			echo json_encode($response);

		}else{
			$SkillData= $skillObj->fetchRow("skill_id='$category_id'");
			$final_category_name =  $SkillData->skill_name;
			if (isset($subcategory_name) && $subcategory_name!="")
			{
				$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
				$parent_id = $SkillNameData->skill_id;
			}else{
				$parent_id = $category_id;
			}
			if (isset($subcategory_name) && $subcategory_name=="" && isset($category_id) && $category_id!="")
			{
				$data = array("skill_name"=>$skill_name,"skill_description"=>$skill_description,"parent_skill_id"=>$parent_id,"skill_depth"=>'2',"is_enabled"=>$enable,"order_id"=>$ordernunber,"is_skill"=>$menuable);
			}else{
				$data = array("skill_name"=>$skill_name,"skill_description"=>$skill_description,"parent_skill_id"=>$parent_id,"skill_depth"=>'3',"is_enabled"=>$enable,"order_id"=>$ordernunber,"is_skill"=>$menuable);
			}
			//print_r($data);exit;
			if(isset($id) && $id > 0){
				$skillObj->update($data,"skill_id='$id'");
				
				if ((isset($menuable) && $menuable=="0"))
				{
					if (isset($subcategory_name) && $subcategory_name!="")
					{
						$data2 = array("is_skill"=>'0');
						$skillObj->update($data2,"parent_skill_id='$subcategory_name'");
						
						$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
						$parent_id = $SkillNameData->skill_id;
						$skillObj->update($data2,"parent_skill_id='$parent_id'");
						
					}else{
						$data2 = array("is_skill"=>'0');
						$skillObj->update($data2,"parent_skill_id='$id'");
						
					}
				}else{
					if (isset($subcategory_name) && $subcategory_name!="")
					{
						//echo "in";
						$data2 = array("is_skill"=>'1');
						$skillObj->update($data2,"parent_skill_id='$subcategory_name'");
						
						$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
						$parent_id = $SkillNameData->skill_id;
						$skillObj->update($data2,"parent_skill_id='$parent_id'");
						
					}else{
						$data2 = array("is_skill"=>'1');
						$skillObj->update($data2,"parent_skill_id='$id'");
					}
				}
				if(isset($enable) && $enable=="0")
				{
					if (isset($subcategory_name) && $subcategory_name!="")
					{
						//echo "out";
						$data3 = array("is_enabled"=>'0');
						$skillObj->update($data3,"skill_id='$subcategory_name'");
						
						$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
						$parent_id = $SkillNameData->skill_id;
						$skillObj->update($data3,"skill_id='$parent_id'");
					}else{
						$data3 = array("is_enabled"=>'0');
						$skillObj->update($data3,"skill_id='$id'");
						
					}
				}else{
					if (isset($subcategory_name) && $subcategory_name!="")
					{
						//echo "out";
						$data3 = array("is_enabled"=>'1');
						$skillObj->update($data3,"skill_id='$subcategory_name'");
						
						$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
						$parent_id = $SkillNameData->skill_id;
						$skillObj->update($data3,"skill_id='$parent_id'");
					}else{
						$data3 = array("is_enabled"=>'1');
						$skillObj->update($data3,"skill_id='$id'");
					}
				}
				$authUserNamespace->status_message = "Skills has been updated successfully";
			}else{
				$skillObj->insert($data);
				$authUserNamespace->status_message = "Skills has been added successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/adminnew/skills';</script>";
		}
	}

}
public function addcategoryAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightbox");
	
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
	if(isset($id) && $id > 0)
	{
		$this->view->category = $skillObj->fetchRow("skill_id='$id'");
	}
	
	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Category";
	}else{
	    $authUserNamespace->admin_page_title = "Add Category";
	}
	
	if($this->_request->isPost()){
	
		$category_name = $this->_request->getParam("CategoryName");
		$category_description = $this->_request->getParam("CategoryDescription");
		$enable = $this->_request->getParam("Enable");
		$menuable = $this->_request->getParam("menuable");
		$ordernunber = $this->_request->getParam("OrderNumber");
		$hiddenid= $this->_request->getParam("hiddenvar");
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			$data = array("skill_name"=>$category_name);
			if(isset($id) && $id !="")
			{
				$category_exist = $skillObj->fetchAll("skill_name = '$category_name' && skill_id!='$hiddenid' && parent_skill_id='0'");
			}else {
				$category_exist = $skillObj->fetchAll("skill_name = '$category_name' && parent_skill_id='0'");
			}
			
			if($data["skill_name"] == "")$response["data"]["CategoryName"] = "null";
			elseif(!preg_match("/^[a-zA-Z0-9 ]*$/", $data['skill_name']))$response['data']['CategoryName'] = "invalid";
			elseif(isset($category_exist) && sizeof($category_exist) > 0)$response["data"]["CategoryName"] = "duplicate";
			else $response["data"]["CategoryName"] = "valid";
			
			if($category_description == "")$response["data"]["CategoryDescription"] = "null";
			else $response["data"]["CategoryDescription"] = "valid";
			if(isset($id) && $id !="")
			{
				$order_exist = $skillObj->fetchAll("skill_id!='$hiddenid' && order_id='$ordernunber' && parent_skill_id='0'");
			}else {
				$order_exist = $skillObj->fetchAll("order_id='$ordernunber' && parent_skill_id='0'");
			}
							
			if($ordernunber == "")$response["data"]["OrderNumber"] = "null";
			elseif(isset($order_exist) && sizeof($order_exist) > 0)$response["data"]["OrderNumber"] = "duplicate";
			else $response["data"]["OrderNumber"] = "valid";
			
			if($enable == "")$response["data"]["Enable"] = "null";
			else $response["data"]["Enable"] = "valid";
			
			if($menuable == "")$response["data"]["menuable"] = "null";
			else $response["data"]["menuable"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
		}else{
			$data = array("skill_name"=>$category_name,"skill_description"=>$category_description,"parent_skill_id"=>'0',"skill_depth"=>'1',"is_enabled"=>$enable,"is_skill"=>$menuable,"order_id"=>$ordernunber);
			if(isset($id) && $id > 0){
				$skillObj->update($data,"skill_id='$id'");
				if ((isset($menuable) && $menuable=="0"))
				{
						$data2 = array("is_skill"=>'0');
						$skillObj->update($data2,"parent_skill_id='$hiddenid'");
						
						
						$categoryskillRows = $skillObj->fetchAll($skillObj->select()
									  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									  ->where("parent_skill_id = '$hiddenid'"));

						if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
						{
							foreach($categoryskillRows as $idsname)
							{
								$skillObj->update($data2,"parent_skill_id='$idsname->skill_id'");
							}
						}
				
				}else{
						$data2 = array("is_skill"=>'1');
						$skillObj->update($data2,"parent_skill_id='$hiddenid'");
						
						
						$categoryskillRows = $skillObj->fetchAll($skillObj->select()
									  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									  ->where("parent_skill_id = '$hiddenid'"));

						if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
						{
							foreach($categoryskillRows as $idsname)
							{
								$skillObj->update($data2,"parent_skill_id='$idsname->skill_id'");
							}
						}
				}
				
				if(isset($enable) && $enable=="0")
				{
						$data3 = array("is_enabled"=>'0');
						$skillObj->update($data3,"parent_skill_id='$hiddenid'");
						
						
						$categoryskillRows = $skillObj->fetchAll($skillObj->select()
									  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									  ->where("parent_skill_id = '$hiddenid'"));

						if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
						{
							foreach($categoryskillRows as $idsname)
							{
								$skillObj->update($data3,"parent_skill_id='$idsname->skill_id'");
							}
						}
				}else{
						$data3 = array("is_enabled"=>'1');
						$skillObj->update($data3,"parent_skill_id='$hiddenid'");
						
						
						$categoryskillRows = $skillObj->fetchAll($skillObj->select()
									  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									  ->where("parent_skill_id = '$hiddenid'"));

						if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
						{
							foreach($categoryskillRows as $idsname)
							{
								$skillObj->update($data3,"parent_skill_id='$idsname->skill_id'");
							}
						}
				}
				
				$authUserNamespace->status_message = "Category has been updated successfully";
			}else{
				$skillObj->insert($data);
				$authUserNamespace->status_message = "Category has been added successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/adminnew/category';</script>";
		}
	}
}

public function addsubcategoryAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightbox");
	$skillObj = new Skillzot_Model_DbTable_Skills();
	$skillrowResult = $skillObj->fetchAll($skillObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
						->where("s.parent_skill_id='0' && s.skill_id!='0'"));
						$this->view->maincategory = $skillrowResult;
	
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	if(isset($id) && $id > 0)
	{
		$this->view->category = $skillObj->fetchRow("skill_id='$id'");
	}
	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Sub Category";
	}else{
	    $authUserNamespace->admin_page_title = "Add Sub Category";
	}
	
	if($this->_request->isPost()){
	
		$category_name = $this->_request->getParam("subcategory_name");
		$category_id =  $this->_request->getParam("Category_name");
		$category_description = $this->_request->getParam("CategoryDescription");
		$enable = $this->_request->getParam("Enable");
		$menuable = $this->_request->getParam("menuable");
		$ordernunber = $this->_request->getParam("OrderNumber");
		$hiddenid= $this->_request->getParam("hiddenvar");
		
		
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			$data = array("skill_name"=>$category_name);
		
			if(isset($id) && $id !="")
			{
				$category_exist = $skillObj->fetchAll("skill_name = '$category_name' && skill_id!='$hiddenid' && parent_skill_id!='0'");
			}else
			{
				$category_exist = $skillObj->fetchAll("skill_name = '$category_name'  && parent_skill_id!='0'");
		
			}
			if($data["skill_name"] == "")$response["data"]["subcategory_name"] = "null";
			//elseif(!preg_match("/^[a-zA-Z0-9-/ ]*$/", $data['skill_name']))$response['data']['subcategory_name'] = "invalid";
			elseif(isset($category_exist) && sizeof($category_exist) > 0)$response["data"]["subcategory_name"] = "duplicate";
			else $response["data"]["subcategory_name"] = "valid";
			
			if($category_id == "")$response["data"]["Category_name"] = "null";
			else $response["data"]["Category_name"] = "valid";
			
//				if($category_name == "")$response["data"]["subcategory_name"] = "null";
//				else $response["data"]["subcategory_name"] = "valid";
			
			if($category_description == "")$response["data"]["CategoryDescription"] = "null";
			else $response["data"]["CategoryDescription"] = "valid";
			
			if(isset($id) && $id !="")
			{
				$order_exist = $skillObj->fetchAll("skill_id!='$hiddenid' && order_id='$ordernunber' && parent_skill_id!='0' && parent_skill_id=$category_id");
			}else
			{
				$order_exist = $skillObj->fetchAll("order_id='$ordernunber' && parent_skill_id!='0'&& parent_skill_id=$category_id");
			//print_r($category_exist );exit;
			}
			
							
			if($ordernunber == "")$response["data"]["OrderNumber"] = "null";
			elseif(isset($order_exist) && sizeof($order_exist) > 0)$response["data"]["OrderNumber"] = "duplicate";
			else $response["data"]["OrderNumber"] = "valid";
			
			if($enable == "")$response["data"]["Enable"] = "null";
			else $response["data"]["Enable"] = "valid";
			
			if($menuable == "")$response["data"]["menuable"] = "null";
			else $response["data"]["menuable"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
							
		}else{
		
			$data = array("skill_name"=>$category_name,"skill_description"=>$category_description,"parent_skill_id"=>$category_id,"skill_depth"=>'2',"is_enabled"=>$enable,"order_id"=>$ordernunber,"is_skill"=>$menuable);
			if(isset($id) && $id > 0){
				$skillObj->update($data,"skill_id='$id'");
				$authUserNamespace->status_message = "Sub Category has been updated successfully";
			}else{
				$skillObj->insert($data);
				$authUserNamespace->status_message = "Sub Category has been added successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/adminnew/subcategory';</script>";
		}
	}
}



public function deletedataAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->disableLayout();;
	$this->_helper->viewRenderer->setNoRender(true);
	$skillObj = new Skillzot_Model_DbTable_Skills();
	$category_id=$this->_request->getParam("category_id");
	$skill_id=$this->_request->getParam("skill_id");
	$subcategory_id=$this->_request->getParam("subcategory_id");
	if(isset($skill_id) && $skill_id!="")
	{
		$skillObj->delete("skill_id='$skill_id'");
		$authUserNamespace->status_message = "Skill has been deleted successfully";
		$this->_redirect('/adminnew/skills');
	}
	if(isset($category_id) && $category_id!="")
	{
		$SkillData= $skillObj->fetchRow("parent_skill_id='$category_id'");
		if(sizeof($SkillData) > 0)
		{
			$authUserNamespace->status_message = "Category can not be deleted";
		}else
		{
			$skillObj->delete("skill_id='$category_id'");
			$authUserNamespace->status_message = "Category has been deleted successfully";
		}
		
		$this->_redirect('/adminnew/category');
	}
	
	if(isset($subcategory_id) && $subcategory_id!="")
	{
		$SkillData= $skillObj->fetchRow("parent_skill_id='$subcategory_id'");
		if(sizeof($SkillData) > 0)
		{
			$authUserNamespace->status_message = "Sub category can not be deleted";
		}else{
			$skillObj->delete("skill_id='$subcategory_id'");
			$authUserNamespace->status_message = "Sub category has been deleted successfully";
		}
		$this->_redirect('/adminnew/subcategory');
		
	}
}




public function changepasswordAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	
	$authUserNamespace->admin_page_title = "Change Password";
	
	if($this->_request->isPost()) {
	
	$passwordObj = new Skillzot_Model_DbTable_Adminlogin();
	$user_id = $authUserNamespace->user_id;
	$current_password = $this->_request->getParam('CurrentPassword');
	$new_password = $this->_request->getParam('NewPassword');
	$confirm_password = $this->_request->getParam('ConfirmPassword');
	
	$response = array();
	if($this->_request->isXmlHttpRequest()){
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$password_row = $passwordObj->fetchRow("admin_pwd='$current_password' and id='$user_id'");
		
		if($current_password=="")$response["data"]["CurrentPassword"] = "null";
		elseif(!sizeof($password_row) > 0)$response["data"]["CurrentPassword"] = "invalid";
		else $response["data"]["CurrentPassword"] = "valid";
		
		if($new_password == "")$response["data"]["NewPassword"] = "null";
		else $response["data"]["NewPassword"] = "valid";
		
		if($confirm_password == "")$response["data"]["ConfirmPassword"] = "null";
  		elseif($confirm_password != $new_password)$response["data"]["ConfirmPassword"] = "notmatch";
		else $response["data"]["ConfirmPassword"] = "valid";
		
		if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
		else $response['returnvalue'] = "validation";

		echo json_encode($response);

		}else{
		
			$Current_pass_md5 = md5($current_password);
			$insert_new_pass = md5($new_password);
			$data['admin_pwd'] = $insert_new_pass;
			//print_r($data);exit;
			$passwordObj->update($data,"id='$user_id'");
			$authUserNamespace->status_message = "Password updated successfully.";
			$this->_redirect("/adminnew/home");
		}
	}
}

public function logoutAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	
	$this->_helper->layout()->disableLayout();
	$this->_helper->viewRenderer->setNoRender(true);
	
	Zend_Session::destroy(true,true);
	
	$this->_redirect('adminnew/index?msg=logout');
}

public function commingsoonAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage CommingSoon";
	$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
//		$searchText = "surat";
//		print $commingsoonObj->select()
//							->from(array('m'=>DATABASE_PREFIX."master_commingsoon"))
//							->where("m.city like '%".$searchText."%' || m.signin_as like '%".$searchText."%'" )
//							->order(array("order_id asc"));exit;
	if($searchText!="")
	{
		$commingsoonResult =$commingsoonObj->fetchAll($commingsoonObj->select()
						->from(array('m'=>DATABASE_PREFIX."master_commingsoon"))
						->where("m.city like '%".$searchText."%' || m.signin_as like '%".$searchText."%' || m.skill_interest like '%".$searchText."%' || m.email like '%".$searchText."%'" )
						->order(array("id asc")));
						
											
	}
	else
	{
		$commingsoonResult = $commingsoonObj->fetchAll($commingsoonObj->select()
						->from(array('m'=>DATABASE_PREFIX."master_commingsoon"))
						//->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='3'")
						->order(array("id asc")));

	}

	if(isset($commingsoonResult) && $commingsoonResult!="")
	{
		$this->view->commingsoonResult = $commingsoonResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($commingsoonResult);
	$paginator = Zend_Paginator::factory($commingsoonResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->commingsoonResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
public function deletecommingsoonAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->disableLayout();;
	$this->_helper->viewRenderer->setNoRender(true);
	$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
	$id=$this->_request->getParam("id");
	
	if(isset($id) && $id!="")
	{
		
			$commingsoonObj->delete("id='$id'");
			$authUserNamespace->status_message = "Detail has been deleted successfully";
			$this->_redirect('/adminnew/commingsoon');
	}
}
	public function exportexitsurveyAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
		
		$taskObj = new Skillzot_Model_DbTable_Task();

	    $exporttaskObj_rows = $taskObj->fetchAll($taskObj->select()
								->setIntegrityCheck(false)
								->from(array('c'=>DATABASE_PREFIX."task"),array('c.id as id','date(c.lastupdatedate) as date','time(c.lastupdatedate) as time','c.student_id as student_id','c.task_complete as task_complete','c.task_description as task_description','c.task_reason as task_reason','c.task_recommend as task_recommend','c.task_locality as task_locality','c.page as page','c.refund as refund'))
								->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.student_id=m.id',array('m.first_name as firstname','m.last_name as lastname','m.std_email as email','m.std_phone as phone'))
								->where("1=1")
								->order(array("c.id desc")));
			
		/** PHPExcel **/
		require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
		
		//Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		//Set properties
		$objPHPExcel->getProperties()->setCreator("Skillzot")
									 ->setLastModifiedBy("Skillzot")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 		 ->setKeywords("office 2007 openxml php")
							 		 ->setCategory("Test result file");
		
		//Add some data
		$i = 1;
		
		$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
		$sheet_0->setCellValue("A$i", 'Id')
				->setCellValue("B$i", 'Date')
				->setCellValue("C$i", 'Time')
            	->setCellValue("D$i", 'Student First Name')
            	->setCellValue("E$i", 'Student Last Name')
            	->setCellValue("F$i", 'Student Email')
            	->setCellValue("G$i", 'Student Phone')
            	->setCellValue("H$i", 'Task Complete')
            	->setCellValue("I$i", 'Task Description')
            	->setCellValue("J$i", 'Task Reason')
            	->setCellValue("K$i", 'Recommend')
            	->setCellValue("L$i", 'Student Locality')
            	->setCellValue("M$i", 'Safe and secure')
            	->setCellValue("N$i", 'Page');
            		
		$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);
		$i = 3;
		
		foreach ($exporttaskObj_rows as $t){
			$sheet_0->setCellValue("A$i",  $t->id)
				->setCellValue("B$i",  $t->date)
				->setCellValue("C$i",  $t->time)
            	->setCellValue("D$i",  $t->firstname)
            	->setCellValue("E$i",  $t->lastname)
            	->setCellValue("F$i",  $t->email)
            	->setCellValue("G$i",  $t->phone)
            	->setCellValue("H$i",  $t->task_complete)
            	->setCellValue("I$i",  $t->task_description)
            	->setCellValue("J$i",  $t->task_reason)
            	->setCellValue("K$i",  $t->task_recommend)
            	->setCellValue("L$i",  $t->task_locality)
            	->setCellValue("M$i",  $t->refund)
            	->setCellValue("N$i",  $t->page);

			$i++;
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Exit Survey Report');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="exit_survey_report.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
}
public function analyticsAction(){
	
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Tutor Analytics";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	$searchSkill = addslashes($this->_request->getParam('searchskill'));
	if (isset($searchSkill) && $searchSkill!=""){
		$this->view->search_skill = stripcslashes ($searchSkill);
	}
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	$searchLocality = addslashes($this->_request->getParam('searchlocality'));
	if (isset($searchLocality) && $searchLocality!=""){
		$this->view->search_locality = stripcslashes ($searchLocality);
	}
	$searchStatus = addslashes($this->_request->getParam('searchstatus'));
	if (isset($searchStatus) && $searchStatus!=""){
		$this->view->search_status = stripcslashes ($searchStatus);
	}
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	$skillobj = new Skillzot_Model_DbTable_Skills();
	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	$tutorclassnameObj = new Skillzot_Model_DbTable_Tutorclasstype();
	$tutoradressObj = new Skillzot_Model_DbTable_Branchdetails();
	$tutoradressObj1 = new Skillzot_Model_DbTable_Address();	
	$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
	
	
							
	if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="")
	{
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
							->where("n.tutor_first_name = '$searchText' || n.tutor_last_name ='$searchText' || n.company_name ='$searchText'")
							->order(array('c.date desc')));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="")
	{
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->where("m.skill_name='$searchSkill'")
							->order(array('c.date desc')));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="")
	{
		//echo "status".$searchStatus;
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
							->where("date(c.date) between '$searchSdate' and '$searchEdate'")
							->order(array('c.date desc')));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="")
	{
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->where("n.tutor_first_name='$searchText'|| n.company_name ='$searchText' and m.skill_name='$searchSkill'")
							->order(array('c.date desc')));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="")
	{
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->where("date(c.date) between '$searchSdate' and '$searchEdate' and n.tutor_first_name='$searchText' || n.company_name ='$searchText' and m.skill_name='$searchSkill'")
							->order(array('c.date desc')));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="")
	{
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->where("date(c.date) between '$searchSdate' and '$searchEdate'  and m.skill_name='$searchSkill'")
							->order(array('c.date desc')));
	}
	else if($searchText!="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="")
	{
	$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
							->where("date(c.date) between '$searchSdate' and '$searchEdate' and n.tutor_first_name='$searchText' || n.company_name ='$searchText'")
							->order(array('c.date desc')));
	}
	else
	{

		$tutorsignupResult = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name',
							'n.tutor_last_name as tutor_last_name','n.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
							->where("n.id=c.tutor_id && o.id=c.course_id")
							->order(array('c.date desc')));
	}
	if(isset($tutorsignupResult) && $tutorsignupResult!="")
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
	
public function reportsAction(){
	
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("adminnew");
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		$authUserNamespace->admin_page_title = "Reports";										   
	}

public function exportcommingsoonAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	
	$this->_helper->viewRenderer->setNoRender(true);
	$this->_helper->layout()->disableLayout();
	
	$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
	$commingsoon_rows = $commingsoonObj->fetchAll('1=1','id asc');
	
	/** PHPExcel **/
	require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
	
	//Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	//Set properties
	$objPHPExcel->getProperties()->setCreator("Pronto Infotech")
								 ->setLastModifiedBy("Pronto Infotech")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
						 		 ->setKeywords("office 2007 openxml php")
						 		 ->setCategory("Test result file");
	
	//Add some data
	$i = 1;
	
	$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
	$sheet_0->setCellValue("A$i", 'Index')
			->setCellValue("B$i", 'City')
        	->setCellValue("C$i", 'Email')
        	->setCellValue("D$i", 'Signing As')
        	->setCellValue("E$i", 'Skill Interested');
//            	->setCellValue("E$i", 'Zip Code')
//            	->setCellValue("F$i", 'Mobile Number');
        	
    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($commingsoon_rows as $t){
		
		$sheet_0->setCellValue("A$i", $t->id )
				->setCellValue("B$i", $t->city)
				->setCellValue("C$i", $t->email)
				->setCellValue("D$i", $t->signin_as)
				->setCellValue("E$i", $t->skill_interest);
//					->setCellValue("E$i", $t->zipcode)
//            		->setCellValue("F$i", $t->mobile);
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('CommingSoon Details');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="commingsoondata.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
public function exportstudentAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	
	$this->_helper->viewRenderer->setNoRender(true);
	$this->_helper->layout()->disableLayout();
	
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	
	
	$exportstudentObj = new Skillzot_Model_DbTable_Studentsignup();
	//$exportstudentObj_rows = $exportstudentObj->fetchAll('1=1','id desc');

	if($searchSdate!="" && $searchEdate!="")
	{
	$exportstudentObj_rows = $exportstudentObj->fetchAll($exportstudentObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."tx_student_tutor"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.std_locality=m.address_id',array('m.address_value as locality'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'c.std_city=l.address_id',array('l.address_value as city'))
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate'")
							 ->order(array("c.id desc")));
	}else
	{
	$exportstudentObj_rows = $exportstudentObj->fetchAll($exportstudentObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."tx_student_tutor"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.std_locality=m.address_id',array('m.address_value as locality'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'c.std_city=l.address_id',array('l.address_value as city'))
							->where("1=1")
							 ->order(array("c.id desc")));	
	}	
	/** PHPExcel **/
	require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
	
	//Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	//Set properties
	$objPHPExcel->getProperties()->setCreator("Skillzot")
								 ->setLastModifiedBy("Skillzot")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
						 		 ->setKeywords("office 2007 openxml php")
						 		 ->setCategory("Test result file");
	
	//Add some data
	$i = 1;
	
	$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
	$sheet_0->setCellValue("A$i", 'Student Id')
			->setCellValue("B$i", 'SignUp Date')
        	->setCellValue("C$i", 'First Name')
        	->setCellValue("D$i", 'Last Name')
        	->setCellValue("E$i", 'Email')
        	->setCellValue("F$i", 'Phone')
        	->setCellValue("G$i", 'Locality')
        	->setCellValue("H$i", 'Pincode')
        	->setCellValue("I$i", 'Skills')
        	->setCellValue("J$i", 'Course Name')
        	->setCellValue("K$i", 'Start date')
        	->setCellValue("L$i", 'End date')
        	->setCellValue("M$i", 'City')
        	->setCellValue("N$i", 'SignUp From')
        	->setCellValue("O$i", 'SignUp Url')
        	->setCellValue("P$i", 'Tutor Enrolled')
        	->setCellValue("Q$i", 'FollowUp Comment');
        	
    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($exportstudentObj_rows as $t){
		$sheet_0->setCellValue("A$i",  $t->id)
			->setCellValue("B$i",  $t->lastupdatedate)
        	->setCellValue("C$i",  $t->first_name)
        	->setCellValue("D$i",  $t->last_name)
        	->setCellValue("E$i",  $t->std_email)
        	->setCellValue("F$i",  $t->std_phone)
        	->setCellValue("G$i",  $t->locality)
        	->setCellValue("H$i",  $t->std_pincode)
        	->setCellValue("I$i",  $t->skill_interested)
        	->setCellValue("J$i",  "Null")
        	->setCellValue("K$i",  "Null")
        	->setCellValue("L$i",  "Null")
        	->setCellValue("M$i",  $t->city)
        	->setCellValue("N$i",  $t->login_from)
        	->setCellValue("O$i",  urldecode($t->signup_url))
        	->setCellValue("P$i",  "NUll")
        	->setCellValue("Q$i",  $t->followup_comment);
		
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Student Manager Report');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="student_manager_report.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
public function studentsignupAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Student Manager";
	$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	$searchSkill = addslashes($this->_request->getParam('searchskill'));
	if (isset($searchSkill) && $searchSkill!=""){
		$this->view->search_skill = stripcslashes ($searchSkill);
	}
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	/*if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!="" && isset($searchSkill) && $searchSkill!="" ){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
		$this->view->search_skill = stripcslashes ($searchSkill);
	}*/
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="")
	{
		//echo "text";
		 $studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						//->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%' || m.std_email like '%".$searchText."%' ||  m.login_from like '%".$searchText."%' || m.std_city like '%".$searchText."%'  || m.std_locality like '%".$searchText."%' || m.tutor_for like '%".$searchText."%' || m.std_phone like '%".$searchText."%' || m.lastupdatedate like '%".$searchText."%'" )
						->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%'" )
						->order(array("id desc")));
		 //print_r($studentsignupResult);
						
											
	}
	elseif($searchSkill!="" && $searchSdate =="" && $searchEdate =="" && $searchText =="")
	{
		//echo "skill";
		$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						//->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%' || m.std_email like '%".$searchText."%' ||  m.login_from like '%".$searchText."%' || m.std_city like '%".$searchText."%'  || m.std_locality like '%".$searchText."%' || m.tutor_for like '%".$searchText."%' || m.std_phone like '%".$searchText."%' || m.lastupdatedate like '%".$searchText."%'" )
						->where("m.skill_interested like '%".$searchSkill."%'" )
						->order(array("id desc")));
	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill =="" && $searchText =="" )
	{
		//echo "......dateseach...........".$searchSdate."endate".$searchEdate;
		$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						->where("date(m.lastupdatedate) between '$searchSdate' and '$searchEdate'" )
						->order(array("id desc")));

	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText =="")
	{
		//echo "......date...........".$searchSdate."endate".$searchEdate;
		$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						->where("date(m.lastupdatedate) between '$searchSdate' and '$searchEdate' and m.skill_interested like '%".$searchSkill."%'" )
						->order(array("id desc")));

	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill=="" && $searchText !="")
	{
		//echo "......date...........".$searchSdate."endate".$searchEdate;
		$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						->where("date(m.lastupdatedate) between '$searchSdate' and '$searchEdate' and m.first_name like '%".$searchText."%'" )
						->order(array("id desc")));

	}
	elseif($searchSdate=="" && $searchEdate=="" && $searchSkill!="" && $searchText !="")
	{
		//echo "......date...........".$searchSdate."endate".$searchEdate;
		$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						->where("m.first_name like '%".$searchText."%' and m.skill_interested like '%".$searchSkill."%'" )
						->order(array("id desc")));

	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText !="")
	{
		//echo "......date...........".$searchSdate."endate".$searchEdate;
		$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						->where("date(m.lastupdatedate) between '$searchSdate' and '$searchEdate' and m.first_name like '%".$searchText."%' and m.skill_interested like '%".$searchSkill."%'" )
						->order(array("id desc")));

	}
	else
	{
		
		$studentsignupResult = $studentSignupObj->fetchAll($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						//->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='3'")
						->order(array("id desc")));

	}

	if(isset($studentsignupResult) && $studentsignupResult!="")
	{
		$this->view->studentsignupResult = $studentsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($studentsignupResult);
	$paginator = Zend_Paginator::factory($studentsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->studentsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
public function addstudentAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightboxnew");
	$selectId = '1';
	$this->view->selectid = $selectId;
	$adressObj = new Skillzot_Model_DbTable_Address();
	$cityResult = $adressObj->fetchAll($adressObj->select()
							->from(array('a'=>DATABASE_PREFIX."master_address"))
							->where("a.address_type_id='1' && a.address_id!='0'"));
	$this->view->studentsignupadd = $cityResult;

	$addstudentObj = new Skillzot_Model_DbTable_Studentsignup();

	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	$fetch_data = $addstudentObj->fetchRow("id='$id'");		
	if(isset($id) && $id > 0)
	{
		//$fetch_data = $addstudentObj->fetchRow("id='$id'");
		//$this->view->student =$fetch_data ;
		$this->view->studentsignup = $addstudentObj->fetchRow("id='$id'");
		$this->view->studentsignupadd = $adressObj->fetchRow("address_id='$fetch_data->std_city'");

	}
	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Student";
	}else{
	    $authUserNamespace->admin_page_title = "Add Student";
	}
	
	if($this->_request->isPost()){
	
		$first_name = $this->_request->getParam("FirstName");
		$last_name = $this->_request->getParam("LastName");
		$email = $this->_request->getParam("Email");
		$password = $this->_request->getParam("Password");
		$phone = $this->_request->getParam("Phone");
		$city = $this->_request->getParam("City");
		$locality = $this->_request->getParam("Locality");
		$skills = $this->_request->getParam("Skills");
		$tutor_enrolled = $this->_request->getParam("TutorEnrolled");
		$course_name = $this->_request->getParam("CourseName");
		$start_date = $this->_request->getParam("Startdate");
		$end_date = $this->_request->getParam("Enddate");
		$followup_comment = $this->_request->getParam("FollowUpComment");
		$signup_from = $this->_request->getParam("SignUpFrom");
		$signup_url = $this->_request->getParam("SignUpUrl");
		$hiddenid= $this->_request->getParam("hiddenvar");

		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			if($first_name == "")$response["data"]["FirstName"] = "null";
			else $response["data"]["FirstName"] = "valid";
	

			if($password == "")$response["data"]["Password"] = "null";
			else $response["data"]["Password"] = "valid";
			

			if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";

			echo json_encode($response);
		}else{
			
			if(isset($id) && $id > 0){
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,"std_email"=>$email,"std_phone"=>$phone,"std_password"=>$password,"std_locality"=>$locality,"skill_interested"=>$skills,"followup_comment"=>$followup_comment,"login_from"=>$signup_from,"signup_url"=>$signup_url,"editdate"=>date('Y-m-d h:i:s'));
				$addstudentObj->update($data,"id='$id'");
				$authUserNamespace->status_message = "studentsignup has been updated successfully";
			}else{
				//echo "locality".$locality."city".$city;exit;
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,"std_email"=>$email,"std_password"=>$password,"std_phone"=>$phone,"std_city"=>$city,"std_locality"=>$locality,"skill_interested"=>$skills,"followup_comment"=>$followup_comment,"login_from"=>"Manual","signup_url"=>$signup_url,"lastupdatedate"=>date('Y-m-d h:i:s') );
				$addstudentObj->insert($data);
				$authUserNamespace->status_message = "studentsignup has been added successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/adminnew/studentsignup';</script>";
		}
	}
}
public function addtutorAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightboxnew");
	
}
public function tutorsignupAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Tutor Manager";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	$searchSkill = addslashes($this->_request->getParam('searchskill'));
	if (isset($searchSkill) && $searchSkill!=""){
		$this->view->search_skill = stripcslashes ($searchSkill);
	}
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	$searchLocality = addslashes($this->_request->getParam('searchlocality'));
	if (isset($searchLocality) && $searchLocality!=""){
		$this->view->search_locality = stripcslashes ($searchLocality);
	}
	$searchStatus = addslashes($this->_request->getParam('searchstatus'));
	if (isset($searchStatus) && $searchStatus!=""){
		$this->view->search_status = stripcslashes ($searchStatus);
	}
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	$skillobj = new Skillzot_Model_DbTable_Skills();
	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	$tutorclassnameObj = new Skillzot_Model_DbTable_Tutorclasstype();
	$tutoradressObj = new Skillzot_Model_DbTable_Branchdetails();
	$tutoradressObj1 = new Skillzot_Model_DbTable_Address();	

	if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="" && $searchLocality=="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("c.tutor_first_name = '$searchText' || c.tutor_last_name ='$searchText' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="" && $searchLocality=="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("m.skill_name='$searchSkill'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("l.address_value like '%".$searchLocality."%'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="" && $searchLocality=="" && $searchStatus!="")
	{
		//echo "status".$searchStatus;
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("c.is_active='$searchStatus'")
							//->group(array("n.course_id"))
							->order(array("id desc")));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus=="")
	{
		//echo "status".$searchStatus;
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate'")
							//->group(array("n.course_id"))

							->order(array("id desc")));
	}
	else if($searchText!="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus=="")
	{
		//echo "status".$searchStatus;
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and c.tutor_first_name='$searchText'")
							//->group(array("n.course_id"))

							->order(array("id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="" && $searchLocality=="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("c.tutor_first_name='$searchText' and m.skill_name='$searchSkill' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and c.tutor_first_name='$searchText' and m.skill_name='$searchSkill'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText' and m.skill_name='$searchSkill'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText' and m.skill_name='$searchSkill' and c.is_active='$searchStatus'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and c.tutor_first_name='$searchText' and m.skill_name='$searchSkill' and c.is_active='$searchStatus' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText' and c.is_active='$searchStatus' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%' and n.skill_name='$searchSkill' and c.is_active='$searchStatus'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where(" l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText' and m.skill_name='$searchSkill' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill=="" && $searchSdate =="" && $searchEdate =="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where(" l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("l.address_value like '%".$searchLocality."%'  and m.skill_name='$searchSkill' ")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus=="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate'  and m.skill_name='$searchSkill'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate'  and c.is_active='$searchStatus'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and c.tutor_first_name='$searchText' and c.is_active='$searchStatus' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill =="" && $searchSdate !="" && $searchEdate !="" && $searchLocality!="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.address_value like '%".$searchLocality."%' and c.is_active='$searchStatus'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText =="" && $searchSkill !="" && $searchSdate !="" && $searchEdate !="" && $searchLocality=="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
		                    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and  and m.skill_name='$searchSkill' and c.is_active='$searchStatus'")

		                    //->group(array("n.course_id"))

		                    ->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="" && $searchLocality=="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
		                    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where(" c.tutor_first_name='$searchText' and m.skill_name='$searchSkill' and c.is_active='$searchStatus' || c.company_name ='$searchText'")
		                    //->group(array("n.course_id"))
		                    ->order(array("c.id desc")));
	}
	else if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="" && $searchLocality!="" && $searchStatus!="")
	{
	$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where(" l.address_value like '%".$searchLocality."%' and c.tutor_first_name='$searchText'  and c.is_active='$searchStatus' || c.company_name ='$searchText'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else if($searchText=="" && $searchSkill !="" && $searchSdate =="" && $searchEdate =="" && $searchLocality=="" && $searchStatus!="")
	{
		$tutorsignupResult = $tutorProfileobj->fetchAll($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where(" m.skill_name='$searchSkill' and c.is_active='$searchStatus'")
							//->group(array("n.course_id"))
							->order(array("c.id desc")));
	}
	else
	{
		//echo "hello";
	$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->order(array("c.id desc")));

	}
	if(isset($tutorsignupResult) && $tutorsignupResult!="")
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
public function tutornewsignupAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage Tutor New Signup";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	if($searchText!="")
	{
		$tutorsignupResult =$tutorProfile->fetchAll($tutorProfile->select()
						->setIntegrityCheck(false)
						->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"))
						->joinLeft(array('a'=>DATABASE_PREFIX."master_address"),'m.tutor_add_city=a.address_id',array('a.address_value'))
						->where("(a.address_value like '%".$searchText."%' || m.tutor_first_name like '%".$searchText."%' || m.tutor_last_name like '%".$searchText."%' || m.tutor_email like '%".$searchText."%') && m.is_active!='0' && m.is_active!='1'" )
						->order(array("id desc")));
						
											
	}
	else
	{
		$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"))
						//->where("m.is_active!='0'")
						->order(array("id desc")));

	}
	if(isset($tutorsignupResult) && $tutorsignupResult!="")
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
public function tutordeleteAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->disableLayout();;
	$this->_helper->viewRenderer->setNoRender(true);
	$tutorProfileObj = new Skillzot_Model_DbTable_Tutorprofile();
	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
	$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
	$id=$this->_request->getParam("id");
	$newid=$this->_request->getParam("newid");
	
	
	if(isset($id) && $id!="")
	{
		
			$tutorProfileObj->delete("id='$id'");
			$tutorCourseobj->delete("tutor_id='$id'");
			$travelradiousObj->delete("tutor_id='$id'");
			$branchdetailObj->delete("tutor_id='$id'");
			$authUserNamespace->status_message = "Detail has been deleted successfully";
			$this->_redirect('/adminnew/tutorsignup');
	}
	if(isset($newid) && $newid!="")
	{
		
			$tutorProfileObj->delete("id='$newid'");
			$tutorCourseobj->delete("tutor_id='$newid'");
			$travelradiousObj->delete("tutor_id='$newid'");
			$branchdetailObj->delete("tutor_id='$newid'");
			$authUserNamespace->status_message = "Detail has been deleted successfully";
			$this->_redirect('/adminnew/tutornewsignup');
	}
	
	
}
public function studentdeleteAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->disableLayout();;
	$this->_helper->viewRenderer->setNoRender(true);
	$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
	$id=$this->_request->getParam("id");
	
	if(isset($id) && $id!="")
	{
		
			$studentSignupObj->delete("id='$id'");
			$authUserNamespace->status_message = "Detail has been deleted successfully";
			$this->_redirect('/adminnew/studentsignup');
	}
	
	
}
public function editstatusAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightboxnew");
	
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	if(isset($id) && $id > 0)
	{
		$this->view->category = $tutorProfile->fetchRow("id='$id'");
	}
	
	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Status";
	}
	
	if($this->_request->isPost()){
	
		$status_value = $this->_request->getParam("statusflag");
		
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			
			if($status_value == "")$response["data"]["statusflag"] = "onlyselectnull";
			else $response["data"]["statusflag"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
		}else{
			if ($status_value=="0"){
				$cDate=date('Y-m-d H:i:s');
				$data = array("is_active"=>"1","editdate"=>$cDate);
				$tutorProfile->update($data,"id='$id'");
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorsignup';</script>";
			}else{
				$cDate=date('Y-m-d H:i:s');
				$data = array("is_active"=>"0","editdate"=>$cDate);
				$tutorProfile->update($data,"id='$id'");
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorsignup';</script>";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			
		}
	}
}
public function getordernumberAction()
{
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->disableLayout();
	$skillObj = new Skillzot_Model_DbTable_Skills();
	
		if($this->_request->isPost()){
		
			if($this->_request->isXmlHttpRequest()){
			$category_id = $this->_request->getParam("category_id");
			$subcat_id = $this->_request->getParam("subcategory_id");
			
			if (isset($subcat_id) && $subcat_id!="")
			{
				//echo "in";
				$temp_id =  $subcat_id;
			}else{
				$temp_id =  $category_id;
			}
			//echo "---temp--".$temp_id;exit;
			$category_rows = $skillObj->fetchAll($skillObj->select()
														  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.order_id'))
														  ->where("s.parent_skill_id=$temp_id")
														  ->order(array("order_id desc")));
														//  print_r($category_rows);
			
			$tags = "";
			$tags['orderid'] = $category_rows[0]['order_id'] + 1;
			
			echo json_encode($tags);exit;
			}
  		}
}
public function reviewAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		$authUserNamespace->admin_page_title = "Review Display";
		
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$tutorProfileObj = new Skillzot_Model_DbTable_Tutorprofile();
		
		$records_per_page = $this->_request->getParam('shown');
		
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		$searchSkill = addslashes($this->_request->getParam('searchskill'));
		if (isset($searchSkill) && $searchSkill!=""){
			$this->view->search_skill = stripcslashes ($searchSkill);
		}
		$searchSdate = addslashes($this->_request->getParam('searchSdate'));
		$searchEdate = addslashes($this->_request->getParam('searchEdate'));
		if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
			$this->view->search_sdate = stripcslashes ($searchSdate);
			$this->view->search_edate = stripcslashes ($searchEdate);
		}
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
	
	    if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="")
		{
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
								 ->setIntegrityCheck(false)
		                         ->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('*'))
		                         ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
		                         ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array(''))
		                         ->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%'")
		                         ->order(array("c.id desc")));											
		}
		elseif($searchSkill!="" && $searchSdate =="" && $searchEdate =="" && $searchText =="")
		{	
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
		                         ->setIntegrityCheck(false)
		                         ->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('*'))
		                         ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
		                         ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array(''))
		                         ->where("l.tutor_first_name like '%".$searchSkill."%' || l.tutor_last_name like '%".$searchSkill."%'")
		                         ->order(array("c.id desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill =="" && $searchText =="" )
		{
		
			$tutorreviewResult =$tutorreviewobj->fetchAll($tutorreviewobj->select()
							    ->from(array('m'=>DATABASE_PREFIX."tutor_review"))
							    ->where("date(m.lastupdatedate) between '$searchSdate' and '$searchEdate'" )
							    ->order(array("id desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText =="")
		{
		    $tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
								 ->setIntegrityCheck(false)
							     ->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('*'))
							     ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							     ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array(''))
							     ->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and l.tutor_first_name like  '%".$searchSkill."%' || l.tutor_last_name like '%".$searchSkill."%'")
							     ->order(array("c.id desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill=="" && $searchText !="")
		{
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
							     ->setIntegrityCheck(false)
							     ->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('*'))
							     ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							     ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array(''))
							     ->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%'")
							     ->order(array("c.id desc")));
		}
		elseif($searchSdate=="" && $searchEdate=="" && $searchSkill!="" && $searchText !="")
		{
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
							     ->setIntegrityCheck(false)
							     ->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('*'))
							     ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							     ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array(''))
							     ->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%' and l.tutor_first_name like '%".$searchSkill."%' || l.tutor_last_name like '%".$searchSkill."%'")
							     ->order(array("c.id desc")));

		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText !="")
		{
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
							     ->setIntegrityCheck(false)
							     ->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('*'))
							     ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							     ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array(''))
						     	 ->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate' and m.first_name like '%".$searchText."%' and l.tutor_first_name like '%".$searchSkill."%'")
							     ->order(array("c.id desc")));
		}
		else
		{
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
							     ->from(array('m'=>DATABASE_PREFIX."tutor_review"))
							     ->order(array("id desc")));
		}
		if(isset($tutorreviewResult) && $tutorreviewResult!="")
		{
			$this->view->tutorreviewResult = $tutorreviewResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutorreviewResult);
		$paginator = Zend_Paginator::factory($tutorreviewResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->tutorreviewResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
	}
	public function enrollmentAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Enrollment Manager";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		$searchSkill = addslashes($this->_request->getParam('searchskill'));
		if (isset($searchSkill) && $searchSkill!=""){
			$this->view->search_skill = stripcslashes ($searchSkill);
		}
		$searchSdate = addslashes($this->_request->getParam('searchSdate'));
		$searchEdate = addslashes($this->_request->getParam('searchEdate'));
		if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
			$this->view->search_sdate = stripcslashes ($searchSdate);
			$this->view->search_edate = stripcslashes ($searchEdate);
		}
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorclassnameObj = new Skillzot_Model_DbTable_Tutorclasstype();
		$tutoradressObj = new Skillzot_Model_DbTable_Branchdetails();
		$tutoradressObj1 = new Skillzot_Model_DbTable_Address();	
	
	    if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="")
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))	
						//    ->joinLeft(array('d'=>DATABASE_PREFIX."billing_details"),'c.batch_id=d.batch_id',array('d.grand_total as grand_total'))		  						  
							->where("c.name like '%".$searchText."%' || c.name like '%".$searchText."%'")
		                    ->order(array("c.date_of_booking desc")));											
		}
		elseif($searchSkill!="" && $searchSdate =="" && $searchEdate =="" && $searchText =="")
		{	
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						     ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))		  						  
							  ->where("t.tutor_first_name like '%".$searchSkill."%' || t.tutor_last_name like '%".$searchSkill."%'")
		                         ->order(array("c.date_of_booking desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill =="" && $searchText =="" )
		{
		
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))			  						  
							->where("date(c.date_of_booking) between '$searchSdate' and '$searchEdate'" )
							->order(array("c.date_of_booking desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText =="")
		{
		   $tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))		  						  
							->where("date(c.date_of_booking) between '$searchSdate' and '$searchEdate' and t.tutor_first_name like  '%".$searchSkill."%' || t.tutor_last_name like '%".$searchSkill."%'")
							->order(array("c.date_of_booking desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill=="" && $searchText !="")
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))			  						  
							->where("date(c.date_of_booking) between '$searchSdate' and '$searchEdate' and c.name like '%".$searchText."%' || c.name like '%".$searchText."%'")
							->order(array("c.date_of_booking desc")));
		}
		elseif($searchSdate=="" && $searchEdate=="" && $searchSkill!="" && $searchText !="")
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))	  						  
							->where("c.name like '%".$searchText."%' || c.name like '%".$searchText."%' and t.tutor_first_name like '%".$searchSkill."%' || t.tutor_last_name like '%".$searchSkill."%'")
							->order(array("c.date_of_booking desc")));

		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText !="")
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))			  						  
							->where("date(c.date_of_booking) between '$searchSdate' and '$searchEdate' and c.name like '%".$searchText."%' and t.tutor_first_name like '%".$searchSkill."%'")
							->order(array("c.date_of_booking desc")));
		}
		else
		{
		$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.slot_book as slot_book','c.batch_location as batch_location','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number','c.grand_total as grand_total'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						     ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))		  						  
							->order(array("c.date_of_booking desc")));
		}
	if(isset($tutorsignupResult) && $tutorsignupResult!="")
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	}
	public function batchdetailsAction(){
		
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Tutor Manager";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
	}
	$searchSkill = addslashes($this->_request->getParam('searchskill'));
	if (isset($searchSkill) && $searchSkill!=""){
			$this->view->search_skill = stripcslashes ($searchSkill);
	}
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
			$this->view->search_sdate = stripcslashes ($searchSdate);
			$this->view->search_edate = stripcslashes ($searchEdate);
	}
	if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
	}
	
	$skillobj = new Skillzot_Model_DbTable_Skills();
	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	$tutorclassnameObj = new Skillzot_Model_DbTable_Tutorclasstype();
	$tutoradressObj = new Skillzot_Model_DbTable_Branchdetails();
	$tutoradressObj1 = new Skillzot_Model_DbTable_Address();	

	if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="")
	{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','sum(c.batch_size) as batch_size',
							 'sum(c.seat_available) as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','sum(d.tutor_price) as tutor_price','sum(d.commision_price) as commision_price','sum(d.tutor_fee_per_unit) as tutor_fee_per_unit','sum(d.no_of_students) as no_of_students','sum(d.total_price) as total_price','sum(d.grand_total) as grand_total','sum(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as null','sum(o.tutor_rate) as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))  						  
							->where("t.tutor_first_name like '%".$searchText."%' || t.tutor_last_name like '%".$searchText."%'")
							->group(array("c.tutor_id"))
		                    ->order(array("d.date_of_booking desc")));											
	}
	elseif($searchSkill!="" && $searchSdate =="" && $searchEdate =="" && $searchText =="")
	{	
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))	  						  
							  ->where("t.tutor_first_name like '%".$searchSkill."%' || t.tutor_last_name like '%".$searchSkill."%'")
		                         ->order(array("d.date_of_booking desc")));
	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill =="" && $searchText =="" )
	{
		
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))  						  
							->where("date(d.date_of_booking) between '$searchSdate' and '$searchEdate'" )
							->order(array("d.date_of_booking desc")));
	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText =="")
	{
		   $tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))  						  
							->where("date(d.date_of_booking) between '$searchSdate' and '$searchEdate' and t.tutor_first_name like  '%".$searchSkill."%' || t.tutor_last_name like '%".$searchSkill."%'")
							->order(array("d.date_of_booking desc")));
	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill=="" && $searchText !="")
	{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','sum(c.batch_size) as batch_size',
							 'sum(c.seat_available) as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','sum(d.tutor_price) as tutor_price','sum(d.commision_price) as commision_price','sum(d.tutor_fee_per_unit) as tutor_fee_per_unit','sum(d.no_of_students) as no_of_students','sum(d.total_price) as total_price','sum(d.grand_total) as grand_total','sum(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as null','sum(o.tutor_rate) as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))
							->where("date(d.date_of_booking) between '$searchSdate' and '$searchEdate' and t.tutor_first_name like '%".$searchText."%' || t.tutor_last_name like '%".$searchText."%'")
							->group(array("c.tutor_id"))
							->order(array("d.date_of_booking desc")));
	}
	elseif($searchSdate=="" && $searchEdate=="" && $searchSkill!="" && $searchText !="")
	{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))				  
							->where("c.name like '%".$searchText."%' || t.tutor_first_name like '%".$searchText."%' and t.tutor_first_name like '%".$searchSkill."%' || t.tutor_last_name like '%".$searchSkill."%'")
							->order(array("d.date_of_booking desc")));

	}
	elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText !="")
	{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))		  						  
							->where("date(d.date_of_booking) between '$searchSdate' and '$searchEdate' and t.tutor_first_name like '%".$searchText."%' and t.tutor_first_name like '%".$searchSkill."%'")
							->order(array("d.date_of_booking desc")));
	}
	else
	{
		$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))
							->order(array("d.date_of_booking desc")));
		
	}
	if(isset($tutorsignupResult) && $tutorsignupResult!="")
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
		}
public function exportmessageAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
		
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();	
		$searchSdate = addslashes($this->_request->getParam('searchSdate'));
		$searchEdate = addslashes($this->_request->getParam('searchEdate'));
		if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
			$this->view->search_sdate = stripcslashes ($searchSdate);
			$this->view->search_edate = stripcslashes ($searchEdate);
		}
		if($searchSdate!="" && $searchEdate!="")
		{	
	    $exportmessageObj_rows = $mailMessagesObj->fetchAll($mailMessagesObj->select()
								->setIntegrityCheck(false)
								->from(array('c'=>DATABASE_PREFIX."message"),array('c.id as id','date(c.mail_date) as date','time(c.mail_date) as time','c.mail_content as mail_content'))
								->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array('m.first_name as firstname','m.last_name as lastname','m.std_email as email','m.std_phone as phone'))
								->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array('l.tutor_first_name as tFname','l.tutor_last_name as tLname','l.tutor_email as tEmail','l.tutor_mobile as tMobile'))
								->where("date(c.mail_date) between '$searchSdate' and '$searchEdate'")
								->order(array("c.id desc")));
		}else
		{
		 $exportmessageObj_rows = $mailMessagesObj->fetchAll($mailMessagesObj->select()
								->setIntegrityCheck(false)
								->from(array('c'=>DATABASE_PREFIX."message"),array('c.id as id','date(c.mail_date) as date','time(c.mail_date) as time','c.mail_content as mail_content'))
								->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array('m.first_name as firstname','m.last_name as lastname','m.std_email as email','m.std_phone as phone'))
								->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array('l.tutor_first_name as tFname','l.tutor_last_name as tLname','l.tutor_email as tEmail','l.tutor_mobile as tMobile'))
								->where("1=1")
								->order(array("c.id desc")));	
		}	
		/** PHPExcel **/
		require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
		
		//Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		//Set properties
		$objPHPExcel->getProperties()->setCreator("Skillzot")
									 ->setLastModifiedBy("Skillzot")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 		 ->setKeywords("office 2007 openxml php")
							 		 ->setCategory("Test result file");
		
		//Add some data
		$i = 1;
		
		$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
		$sheet_0->setCellValue("A$i", 'Id')
				->setCellValue("B$i", 'Date')
				->setCellValue("C$i", 'Time')
            	->setCellValue("D$i", 'Student First Name')
            	->setCellValue("E$i", 'Student Last Name')
            	->setCellValue("F$i", 'Student Email')
            	->setCellValue("G$i", 'Student Phone')
            	->setCellValue("H$i", 'Tutor First Name')
            	->setCellValue("I$i", 'Tutor Last Name')
            	->setCellValue("J$i", 'Tutor Email')
            	->setCellValue("K$i", 'Tutor Phone')
            	->setCellValue("L$i", 'Message');
            		
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
		$i = 3;
		
		foreach ($exportmessageObj_rows as $t){
			$sheet_0->setCellValue("A$i",  $t->id)
				->setCellValue("B$i",  $t->date)
				->setCellValue("C$i",  $t->time)
            	->setCellValue("D$i",  $t->firstname)
            	->setCellValue("E$i",  $t->lastname)
            	->setCellValue("F$i",  $t->email)
            	->setCellValue("G$i",  $t->phone)
            	->setCellValue("H$i",  $t->tFname)
            	->setCellValue("I$i",  $t->tLname)
            	->setCellValue("J$i",  $t->tEmail)
            	->setCellValue("K$i",  $t->tMobile)
            	->setCellValue("L$i",  $t->mail_content);

			$i++;
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Student Message Report');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="student_message_report.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
public function messagedeleteAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$studentSignupObj = new Skillzot_Model_DbTable_Mailmessages();
		$id=$this->_request->getParam("id");
		if(isset($id) && $id!="")
		{
			
				$studentSignupObj->delete("id='$id'");
				$authUserNamespace->status_message = "Detail has been deleted successfully";
				$this->_redirect('/adminnew/messagedetail');
		}	
	}
	public function messagedetailAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		$authUserNamespace->admin_page_title = "View Messages";
		
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$tutorProfileObj = new Skillzot_Model_DbTable_Tutorprofile();
		
		$records_per_page = $this->_request->getParam('shown');	
		
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		$searchSkill = addslashes($this->_request->getParam('searchskill'));
		if (isset($searchSkill) && $searchSkill!=""){
			$this->view->search_skill = stripcslashes ($searchSkill);
		}
		$searchSdate = addslashes($this->_request->getParam('searchSdate'));
		$searchEdate = addslashes($this->_request->getParam('searchEdate'));
		if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
			$this->view->search_sdate = stripcslashes ($searchSdate);
			$this->view->search_edate = stripcslashes ($searchEdate);
		}
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		
	    if($searchText!="" && $searchSkill =="" && $searchSdate =="" && $searchEdate =="")
		{
			$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
		                     ->setIntegrityCheck(false)
		                     ->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
	                         ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
		                     ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
		                     ->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%'")
		                     ->order(array("c.id desc")));										
		}
		elseif($searchSkill!="" && $searchSdate =="" && $searchEdate =="" && $searchText =="")
		{
			
			$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
		                     ->setIntegrityCheck(false)
		                     ->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
		                     ->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
		                     ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
		                     ->where("l.tutor_first_name like '%".$searchSkill."%' || l.tutor_last_name like '%".$searchSkill."%'")
		                     ->order(array("c.id desc")));	
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill =="" && $searchText =="" )
		{
		
			$messageResult =$mailMessagesObj->fetchAll($mailMessagesObj->select()
							->from(array('m'=>DATABASE_PREFIX."message"))
							->where("date(m.mail_date) between '$searchSdate' and '$searchEdate'" )
					        ->order(array("id desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText =="")
		{
            $messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
						    ->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
							->where("date(c.mail_date) between '$searchSdate' and '$searchEdate' and l.tutor_first_name like '%".$searchSkill."%'")
						    ->order(array("c.id desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill=="" && $searchText !="")
		{
			$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
							->where("date(c.mail_date) between '$searchSdate' and '$searchEdate' and m.first_name like '%".$searchText."%'")
							->order(array("c.id desc")));


		}
		elseif($searchSdate=="" && $searchEdate=="" && $searchSkill!="" && $searchText !="")
		{
			$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
							->where("m.first_name like '%".$searchText."%' and l.tutor_first_name like '%".$searchSkill."%'")
							->order(array("c.id desc")));
		}
		elseif($searchSdate!="" && $searchEdate!="" && $searchSkill!="" && $searchText !="")
		{
		$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
							->where("date(c.mail_date) between '$searchSdate' and '$searchEdate' and m.first_name like '%".$searchText."%' and l.tutor_first_name like '%".$searchSkill."%'")
							->order(array("c.id desc")));
		}
		else
		{
		$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."message"),array('*'))
							->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array(''))
							->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.to_id=l.id',array(''))
							->order(array("c.id desc")));	
			
			
			/*$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->from(array('m'=>DATABASE_PREFIX."message"))
							->order(array("id desc")));*/
		}
		if(isset($messageResult) && $messageResult!="")
		{
			$this->view->messageResult = $messageResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($messageResult);
		$paginator = Zend_Paginator::factory($messageResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->messageResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
	}
public function exportbatchdetailsAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->viewRenderer->setNoRender(true);
	$this->_helper->layout()->disableLayout();
	
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$addresstable = new Skillzot_Model_DbTable_Localitysuburbs();
	$daytable = new Skillzot_Model_DbTable_Batchday();
	$tutoraddrtable = new Skillzot_Model_DbTable_Address();
	if($searchSdate!="" && $searchEdate!="")
	{
	$exporttutorObj_rows = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))
						    ->where("date(d.date_of_booking) between '$searchSdate' and '$searchEdate'")	
						    ->order(array("d.date_of_booking desc")));									  
	}else
	{
	$exporttutorObj_rows = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.id as id','c.tutor_id as tutor_id','c.lastupdatedate as lastupdatedate','c.course_id as course_id',
							'c.tutor_batch_name as tutor_batch_name','(CASE WHEN c.tutor_location="1," THEN "At Students" WHEN c.tutor_location=",2" THEN "At Teachers" WHEN c.tutor_location="1,2" THEN "At Student, At Teachers" ELSE ""  END) AS tutor_location','c.tutor_lesson_location as tutor_lesson_location',	
							'c.travel_radius as travel_radius','c.tutor_class_dur_wks as tutor_class_dur_wks','c.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','c.tutor_class_dur_hrspday as tutor_class_dur_hrspday',
							 'c.tutor_batch_from_timing as tutor_batch_from_timing','c.tutor_batch_to_timing as tutor_batch_to_timing','c.tutor_batch_day as tutor_batch_day','c.batch_size as batch_size',
							 'c.seat_available as seat_available','c.batch_date as batch_date','c.address as address','c.city as city','c.locality as locality','c.pincode as pincode','c.landmark as landmark','c.batch_summary_for_course as batch_summary_for_course'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('d'=>DATABASE_PREFIX."order_details"),'c.id=d.batch_id',array('d.batch_id as batch_id','d.tutor_price as tutor_price','d.commision_price as commision_price','d.tutor_fee_per_unit as tutor_fee_per_unit','d.no_of_students as no_of_students','d.total_price as total_price','d.grand_total as grand_total','(d.grand_total-d.total_price) as internet_charge','d.date_of_booking as date_of_booking'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
						    ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'c.locality=b.address_id',array('b.address_value as address1_value'))
						   ->order(array("d.date_of_booking desc")));						
	}
	/** PHPExcel **/
	require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
	
	//Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	//Set properties
	$objPHPExcel->getProperties()->setCreator("Skillzot")
								 ->setLastModifiedBy("Skillzot")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
						 		 ->setKeywords("office 2007 openxml php")
						 		 ->setCategory("Test result file");
	
	//Add some data
	$i = 1;
	
	$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
	$sheet_0->setCellValue("A$i", 'Id')
			->setCellValue("B$i", 'Tutor First Name')
        	->setCellValue("C$i", 'Tutor Last Name')
        	->setCellValue("D$i", 'Tutor Company Name')
        	->setCellValue("E$i", 'Tutor Course Name')
        	->setCellValue("F$i", 'Tutor Batch Name')
        	->setCellValue("G$i", 'No. of seats in batch')
        	->setCellValue("H$i", 'No. of seats booked')
        	->setCellValue("I$i", 'Seats Available')
        	->setCellValue("J$i", 'Fees')
        	->setCellValue("K$i", 'Teacher Earning')
        	->setCellValue("L$i", 'Skillzot Commission')
        	->setCellValue("M$i", 'Total Price')
        	->setCellValue("N$i", 'Total Fees Paid') 	
        	->setCellValue("O$i", 'Internet handling charge');
        
          	    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($exporttutorObj_rows as $t){
		
		set_time_limit(300);
	$sheet_0->setCellValue("A$i",  $t->id)
			->setCellValue("B$i",  $t->tutor_first_name)
	    	->setCellValue("C$i",  $t->tutor_last_name)
	    	->setCellValue("D$i",  $t->company_name)
	    	->setCellValue("E$i",  $t->tutor_course_name)
	    	->setCellValue("F$i",  $t->batch_summary_for_course)
	    	->setCellValue("G$i",  $t->batch_size)
	    	->setCellValue("H$i",  $t->no_of_students)
	    	->setCellValue("I$i",  $t->seat_available)
	    	->setCellValue("J$i",  $t->tutor_rate)
	    	->setCellValue("K$i",  $t->tutor_price)
	    	->setCellValue("L$i",  $t->commision_price)
	    	->setCellValue("M$i",  $t->total_price)
	    	->setCellValue("N$i",  $t->grand_total)
	    	->setCellValue("O$i", $t->internet_charge);
      
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

	//$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setAutoSize(true);
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Inventory Manager Report');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	$report_name="inventory_manager_report_".date("Y-m-d").".xls";
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$report_name.'"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
public function exportenrollmentAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->viewRenderer->setNoRender(true);
	$this->_helper->layout()->disableLayout();
	
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$addresstable = new Skillzot_Model_DbTable_Localitysuburbs();
	$daytable = new Skillzot_Model_DbTable_Batchday();
	$tutoraddrtable = new Skillzot_Model_DbTable_Address();
	if($searchSdate!="" && $searchEdate!="")
	{
	$exporttutorObj_rows = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.id as id','c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						     ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))	
						     ->joinLeft(array('d'=>DATABASE_PREFIX."billing_details"),'c.batch_id=d.batch_id',array('d.grand_total as grand_total'))	
						     ->where("date(c.date_of_booking) between '$searchSdate' and '$searchEdate'")			
						     ->order(array("c.date_of_booking desc")));
	}else
	{
	$exporttutorObj_rows = $tutorProfile->fetchAll($tutorProfile->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."order_details"),array('c.id as id','c.std_id as std_id','c.tutor_book_id as tutor_book_id','c.tutor_id as tutor_id',
							'c.course_id as course_id','c.batch_id as batch_id',	
							'c.order_id as order_id','c.date_of_booking as date_of_booking','c.class_booked as class_booked','c.no_of_students as no_of_students',
							 'c.tutor_fee_per_unit as tutor_fee_per_unit','c.total_price as total_price','c.name as name','c.phone_number as phone_number'))
						    ->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_book_id=t.id',array('t.tutor_first_name as tutor_first_name','t.tutor_last_name as tutor_last_name','t.company_name as company_name'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name'))
						     ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.batch_id=b.id',array('b.batch_summary_for_course as batch_summary_for_course'))	
						     ->joinLeft(array('d'=>DATABASE_PREFIX."billing_details"),'c.batch_id=d.batch_id',array('d.grand_total as grand_total'))			
						     ->order(array("c.date_of_booking desc")));
	}					
	/** PHPExcel **/
	require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
	
	//Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	//Set properties
	$objPHPExcel->getProperties()->setCreator("Skillzot")
								 ->setLastModifiedBy("Skillzot")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
						 		 ->setKeywords("office 2007 openxml php")
						 		 ->setCategory("Test result file");
	
	//Add some data
	$i = 1;
	
	$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
	$sheet_0->setCellValue("A$i", 'Id')
			->setCellValue("B$i", 'Tutor First Name')
        	->setCellValue("C$i", 'Tutor Last Name')
        	->setCellValue("D$i", 'Tutor Company Name')
        	->setCellValue("E$i", 'Tutor Course Name')
        	->setCellValue("F$i", 'Tutor Batch Summary')
        	->setCellValue("G$i", 'Name of Student Enrolled')
        	->setCellValue("H$i", 'Phone No.')
        	->setCellValue("I$i", 'Date of Enrolling/Paying')
        	->setCellValue("J$i", 'Fees')
        	->setCellValue("K$i", 'Quantity')
        	->setCellValue("L$i", 'Total Fees Paid');
          	    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($exporttutorObj_rows as $t){
		set_time_limit(300);
	$sheet_0->setCellValue("A$i",  $t->id)
			->setCellValue("B$i",  $t->tutor_first_name)
	    	->setCellValue("C$i",  $t->tutor_last_name)
	    	->setCellValue("D$i",  $t->company_name)
	    	->setCellValue("E$i",  $t->tutor_course_name)
	    	->setCellValue("F$i",  $t->batch_summary_for_course)
	    	->setCellValue("G$i",  $t->name)
	    	->setCellValue("H$i",  $t->phone_number)
	    	->setCellValue("I$i",  $t->date_of_booking)
	    	->setCellValue("J$i",  $t->tutor_fee_per_unit)
	    	->setCellValue("K$i",  $t->no_of_students)
	    	->setCellValue("L$i",  $t->grand_total);  
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Enrollment Manager Report');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="enrollment_manager_report.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
public function exportanalyticsAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	
	$this->_helper->viewRenderer->setNoRender(true);
	$this->_helper->layout()->disableLayout();
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
				
	if($searchSdate!="" && $searchEdate!="")
	{
	$exporttutorObj_rows = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name','n.tutor_last_name as tutor_last_name','n.count_video as count_video','n.company_name as company_name','if(n.tutor_fb_link!="","Yes","No") as tutor_fb_link','if(n.tutor_tw_link!="","Yes","No") as tutor_tw_link','if(n.website_link!="","Yes","No") as website_link','if(n.blog_link!="","Yes","No") as blog_link'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate')) 
							->joinLeft(array('s'=>DATABASE_PREFIX."master_skills"),'s.skill_id=o.tutor_skill_id',array('s.skill_name as skill_name'))
							->where("date(c.date) between '$searchSdate' and '$searchEdate'")
							->order(array('c.date desc','c.tutor_id desc')));
	}else
	{
	$exporttutorObj_rows = $tutorAnalyticsobj->fetchAll($tutorAnalyticsobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"),array('c.tutor_id as tutor_id','c.date as date','c.profile_view_count as profile_view_count','c.message_click_count as message_click_count','c.phone_click_count as phone_click_count','c.enroll_count as enroll_count','c.fb_click_count as fb_click_count','c.twitter_click_count as twitter_click_count','c.course_count as course_count','c.videos_click_count as videos_click_count','c.paymentstep2_count as paymentstep2_count','c.paymentstep3_count as paymentstep3_count'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_profile"),'n.id=c.tutor_id',array('n.tutor_first_name as tutor_first_name','n.tutor_last_name as tutor_last_name','n.count_video as count_video','n.company_name as company_name','if(n.tutor_fb_link!="","Yes","No") as tutor_fb_link','if(n.tutor_tw_link!="","Yes","No") as tutor_tw_link','if(n.website_link!="","Yes","No") as website_link','if(n.blog_link!="","Yes","No") as blog_link'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate')) 
							->joinLeft(array('s'=>DATABASE_PREFIX."master_skills"),'s.skill_id=o.tutor_skill_id',array('s.skill_name as skill_name'))
							->where("1=1")
							->order(array('c.date desc','c.tutor_id desc')));	
	}
	
	/** PHPExcel **/
	require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
	
	//Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	//Set properties
	$objPHPExcel->getProperties()->setCreator("Skillzot")
								 ->setLastModifiedBy("Skillzot")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
						 		 ->setKeywords("office 2007 openxml php")
						 		 ->setCategory("Test result file");
	
	//Add some data
	$i = 1;
	
	$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
	$sheet_0->setCellValue("A$i", 'tutor Id')
			->setCellValue("B$i", 'Date')
        	->setCellValue("C$i", 'First Name')
        	->setCellValue("D$i", 'Last Name')
        	->setCellValue("E$i", 'Company Name')
        	->setCellValue("F$i", 'Course Name')
        	->setCellValue("G$i", 'Skills Taught')
        	->setCellValue("H$i", 'Rate (rupees)')
        	->setCellValue("I$i", 'Video Count')
        	->setCellValue("J$i", 'Facebook Link(Y/N)')
        	->setCellValue("K$i", 'Twitter Links(Y/N)')
        	->setCellValue("L$i", 'Website Link(Y/N)')
        	->setCellValue("M$i", 'Blogs(Y/N)')
        	
        	->setCellValue("N$i", 'Profile View Count')
        	->setCellValue("O$i", 'Message Click Count')
        	->setCellValue("P$i", 'Phone Click Count')   	
        	->setCellValue("Q$i", 'Enroll Count')
        	->setCellValue("R$i", 'FB Click Count')        	     	
        	->setCellValue("S$i", 'Twitter Click Count')
        	->setCellValue("T$i", 'Course Click Count')
        	->setCellValue("U$i", 'PaymentStep2 Count')
        	->setCellValue("V$i", 'PaymentStep3 Count')
        	->setCellValue("W$i", 'Video Click Count');
        	
      
       	    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:W1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($exporttutorObj_rows as $t){
		set_time_limit(300);
	$sheet_0->setCellValue("A$i",  $t->tutor_id)
			->setCellValue("B$i",  $t->date)
	    	->setCellValue("C$i",  $t->tutor_first_name)
	    	->setCellValue("D$i",  $t->tutor_last_name)
	    	->setCellValue("E$i",  $t->company_name)
	    	->setCellValue("F$i",  $t->tutor_course_name)
	    	->setCellValue("G$i",  $t->skill_name)
	    	->setCellValue("H$i",  $t->tutor_rate)
	    	->setCellValue("I$i",  $t->count_video)
	    	
	    	->setCellValue("J$i",  $t->tutor_fb_link)
	    	->setCellValue("K$i",  $t->tutor_tw_link)
	    	->setCellValue("L$i",  $t->website_link)
	    	->setCellValue("M$i",  $t->blog_link)
	    	->setCellValue("N$i",  $t->profile_view_count)        	     	
	    	->setCellValue("O$i",  $t->message_click_count)
	    	->setCellValue("P$i",  $t->phone_click_count)
	    	->setCellValue("Q$i",  $t->enroll_count)
	    	->setCellValue("R$i",  $t->fb_click_count)
	    	->setCellValue("S$i",  $t->twitter_click_count)
	    	->setCellValue("T$i",  $t->course_count)
	    	->setCellValue("U$i",  $t->paymentstep2_count)
	    	->setCellValue("V$i",  $t->paymentstep3_count)
	    	->setCellValue("W$i",  $t->videos_click_count); 
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);

	
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Tutor Analytics Report');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="tutor_analytics_report.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
public function exportreviewAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
		$searchSdate = addslashes($this->_request->getParam('searchSdate'));
		$searchEdate = addslashes($this->_request->getParam('searchEdate'));
		if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
			$this->view->search_sdate = stripcslashes ($searchSdate);
			$this->view->search_edate = stripcslashes ($searchEdate);
		}
	
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		
		if($searchSdate!="" && $searchEdate!="")
		{
	    $exportreviewObj_rows = $tutorreviewobj->fetchAll($tutorreviewobj->select()
								->setIntegrityCheck(false)
								->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('c.id as id','date(c.lastupdatedate) as date','time(c.lastupdatedate) as time','c.comment as comment','c.skillgrade as skillgrade','c.recommend as recommend'))
								->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array('m.first_name as firstname','m.last_name as lastname','m.std_email as email','m.std_phone as phone'))
								->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array('l.tutor_first_name as tFname','l.tutor_last_name as tLname'))
								->joinLeft(array('s'=>DATABASE_PREFIX."master_skills"),'c.skill_id=s.skill_id',array('s.skill_name as skill'))
								->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate'")
								->order(array("c.id desc")));
		}else
		{
		$exportreviewObj_rows = $tutorreviewobj->fetchAll($tutorreviewobj->select()
								->setIntegrityCheck(false)
								->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('c.id as id','date(c.lastupdatedate) as date','time(c.lastupdatedate) as time','c.comment as comment','c.skillgrade as skillgrade','c.recommend as recommend'))
								->joinLeft(array('m'=>DATABASE_PREFIX."tx_student_tutor"),'c.from_id=m.id',array('m.first_name as firstname','m.last_name as lastname','m.std_email as email','m.std_phone as phone'))
								->joinLeft(array('l'=>DATABASE_PREFIX."tx_tutor_profile"),'c.tutor_id=l.id',array('l.tutor_first_name as tFname','l.tutor_last_name as tLname'))
								->joinLeft(array('s'=>DATABASE_PREFIX."master_skills"),'c.skill_id=s.skill_id',array('s.skill_name as skill'))
								->where("1=1")
								->order(array("c.id desc")));	
		}				 
		/** PHPExcel **/
		require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
		
		//Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		//Set properties
		$objPHPExcel->getProperties()->setCreator("Skillzot")
									 ->setLastModifiedBy("Skillzot")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 		 ->setKeywords("office 2007 openxml php")
							 		 ->setCategory("Test result file");
		
		//Add some data
		$i = 1;
		
		$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
		$sheet_0->setCellValue("A$i", 'Id')
				->setCellValue("B$i", 'Date')
				->setCellValue("C$i", 'Time')
            	->setCellValue("D$i", 'Tutor First Name')
            	->setCellValue("E$i", 'Tutor Last Name')
            	->setCellValue("F$i", 'Student First Name')
            	->setCellValue("G$i", 'Student Last Name')
            	->setCellValue("H$i", 'Skill')
            	->setCellValue("I$i", 'Testimonial')
            	->setCellValue("J$i", 'Skill Grade')
            	->setCellValue("K$i", 'Recommend')
            	->setCellValue("L$i", 'Student Email')
            	->setCellValue("M$i", 'Student Phone');
            	   		    		
		$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true);
		$i = 3;
		
		foreach ($exportreviewObj_rows as $t){
			$sheet_0->setCellValue("A$i",  $t->id)
				->setCellValue("B$i",  $t->date)
				->setCellValue("C$i",  $t->time)
            	->setCellValue("D$i",  $t->tFname)
            	->setCellValue("E$i",  $t->tLname)
            	->setCellValue("F$i",  $t->firstname)
            	->setCellValue("G$i",  $t->lastname)
            	->setCellValue("H$i",  $t->skill)
            	->setCellValue("I$i",  $t->comment)
            	->setCellValue("J$i",  $t->skillgrade)
            	->setCellValue("K$i",  $t->recommend)
            	->setCellValue("L$i",  $t->email)
            	->setCellValue("M$i",  $t->phone);
            		
			$i++;
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Tutor Review Report');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="tutor_review_report.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
public function reviewdeleteAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")
			$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$studentmsgObj = new Skillzot_Model_DbTable_Review();
		$id=$this->_request->getParam("id");
		if(isset($id) && $id!="")
		{	
				$studentmsgObj->delete("id='$id'");
				$authUserNamespace->status_message = "Detail has been deleted successfully";
				$this->_redirect('/adminnew/review');
		}
	}
public function exporttutorAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->viewRenderer->setNoRender(true);
	$this->_helper->layout()->disableLayout();
	
	$searchSdate = addslashes($this->_request->getParam('searchSdate'));
	$searchEdate = addslashes($this->_request->getParam('searchEdate'));
	if (isset($searchSdate) && $searchSdate!="" && isset($searchEdate) && $searchEdate!=""){
		$this->view->search_sdate = stripcslashes ($searchSdate);
		$this->view->search_edate = stripcslashes ($searchEdate);
	}
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	if($searchSdate!="" && $searchEdate!="")
	{
	$exporttutorObj_rows = $tutorProfile->fetchAll($tutorProfile->select()
						->setIntegrityCheck(false)
						->distinct()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name',
						'c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email',	
						'c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.tutor_brief_desc!="","Yes","No") as tutor_brief_desc','if(c.tutor_image_type!="","Yes","No") as tutor_image_type','if(o.tutor_class_image_type!="","Yes","No") as tutor_class_image_type','if(c.tutor_fb_link!="","Yes","No") as tutor_fb_link','if(c.tutor_tw_link!="","Yes","No") as tutor_tw_link','if(c.website_link!="","Yes","No") as website_link','if(c.blog_link!="","Yes","No") as blog_link','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE "Null"  END) AS class_type_name'))
						->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.tutor_course_name as tutor_course_name',
							'o.student_age_group as student_age_group','o.tutor_format as tutor_format',
							'o.tutor_certificate_granted as tutor_certificate_granted','o.tutor_class_batch_schedule as tutor_class_batch_schedule',
							'o.travel_radius as travel_radius','o.tutor_rate as tutor_rate','o.rate_comments as rate_comments','o.tutor_canc_policy as tutor_canc_policy','if(o.tutor_course_desc!="","Yes","No") as tutor_course_desc','o.tutor_class_dur_wks as tutor_class_dur_wks',
							'o.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','o.tutor_class_dur_hrspday as tutor_class_dur_hrspday','(CASE WHEN tutor_student_profile = "1,2,3," THEN "Beginner,Intermediate,Advanced"  WHEN tutor_student_profile = "1,2,"  THEN "Beginner,Intermediate" WHEN tutor_student_profile = "1," THEN "Beginner" WHEN tutor_student_profile = "1,3," THEN "Beginner,Advanced" WHEN tutor_student_profile = "2,3," THEN "Intermediate,Advanced" WHEN tutor_student_profile = "3," THEN "Advanced" WHEN tutor_student_profile = "2," THEN "Intermediate" ELSE "Null"  END)  AS tutor_student_profile'))
						->joinLeft(array('p'=>DATABASE_PREFIX."master_tutor_pay_cycle"),'p.pay_cycle_id=o.tutor_pay_feetype',array('p.pay_cycle_name as pay_cycle_name'))
						->joinLeft(array('q'=>DATABASE_PREFIX."master_tutor_lesson_location"),'o.tutor_lesson_location=q.location_id',array('q.location_name as location_name'))
						->joinLeft(array('r'=>DATABASE_PREFIX."master_tutor_fee_type"),'r.fee_type_id=o.tutor_pay_feetype',array('r.fee_type_name as fee_type_name'))
						->joinLeft(array('m'=>DATABASE_PREFIX."master_tutor_class_types"),'m.class_type_id=c.tutor_class_type',array('m.class_type_name as classType'))
						->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
						->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))
						->joinLeft(array('k'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=k.skill_id',array('k.skill_name as skill_name'))
						->where("date(c.lastupdatedate) between '$searchSdate' and '$searchEdate'")
						->order(array("c.id desc")));
	}
	else
	{
	$exporttutorObj_rows = $tutorProfile->fetchAll($tutorProfile->select()
						->setIntegrityCheck(false)
						->distinct()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name',
						'c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email',	
						'c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.tutor_brief_desc!="","Yes","No") as tutor_brief_desc','if(c.tutor_image_type!="","Yes","No") as tutor_image_type','if(o.tutor_class_image_type!="","Yes","No") as tutor_class_image_type','if(c.tutor_fb_link!="","Yes","No") as tutor_fb_link','if(c.tutor_tw_link!="","Yes","No") as tutor_tw_link','if(c.website_link!="","Yes","No") as website_link','if(c.blog_link!="","Yes","No") as blog_link','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE "Null"  END) AS class_type_name'))
						->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.tutor_course_name as tutor_course_name',
							'o.student_age_group as student_age_group','o.tutor_format as tutor_format',
							'o.tutor_certificate_granted as tutor_certificate_granted','o.tutor_class_batch_schedule as tutor_class_batch_schedule',
							'o.travel_radius as travel_radius','o.tutor_rate as tutor_rate','o.rate_comments as rate_comments','o.tutor_canc_policy as tutor_canc_policy','if(o.tutor_course_desc!="","Yes","No") as tutor_course_desc','o.tutor_class_dur_wks as tutor_class_dur_wks',
							'o.tutor_class_dur_dayspwk as tutor_class_dur_dayspwk','o.tutor_class_dur_hrspday as tutor_class_dur_hrspday','(CASE WHEN tutor_student_profile = "1,2,3," THEN "Beginner,Intermediate,Advanced"  WHEN tutor_student_profile = "1,2,"  THEN "Beginner,Intermediate" WHEN tutor_student_profile = "1," THEN "Beginner" WHEN tutor_student_profile = "1,3," THEN "Beginner,Advanced" WHEN tutor_student_profile = "2,3," THEN "Intermediate,Advanced" WHEN tutor_student_profile = "3," THEN "Advanced" WHEN tutor_student_profile = "2," THEN "Intermediate" ELSE "Null"  END)  AS tutor_student_profile'))
						->joinLeft(array('p'=>DATABASE_PREFIX."master_tutor_pay_cycle"),'p.pay_cycle_id=o.tutor_pay_feetype',array('p.pay_cycle_name as pay_cycle_name'))
						->joinLeft(array('q'=>DATABASE_PREFIX."master_tutor_lesson_location"),'o.tutor_lesson_location=q.location_id',array('q.location_name as location_name'))
						->joinLeft(array('r'=>DATABASE_PREFIX."master_tutor_fee_type"),'r.fee_type_id=o.tutor_pay_feetype',array('r.fee_type_name as fee_type_name'))
						->joinLeft(array('m'=>DATABASE_PREFIX."master_tutor_class_types"),'m.class_type_id=c.tutor_class_type',array('m.class_type_name as classType'))
						->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
						->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))
						->joinLeft(array('k'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=k.skill_id',array('k.skill_name as skill_name'))
						->order(array("c.id desc")));	

	}					
    
	/** PHPExcel **/
	require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
	
	//Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	//Set properties
	$objPHPExcel->getProperties()->setCreator("Skillzot")
								 ->setLastModifiedBy("Skillzot")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
						 		 ->setKeywords("office 2007 openxml php")
						 		 ->setCategory("Test result file");
	
	//Add some data
	$i = 1;
	
	$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
	$sheet_0->setCellValue("A$i", 'tutor Id')
			->setCellValue("B$i", 'SignUp Date')
        	->setCellValue("C$i", 'First Name')
        	->setCellValue("D$i", 'Last Name')
        	->setCellValue("E$i", 'Email')
        	->setCellValue("F$i", 'Mobile')
        	->setCellValue("G$i", 'Alt. Phone Number')
        	->setCellValue("H$i", 'Company Name')
        	->setCellValue("I$i", 'live date')
        	->setCellValue("J$i", 'Live Status')
        	->setCellValue("K$i", 'Types Of Classes')
        	->setCellValue("L$i", 'Skills Taught')
        	->setCellValue("M$i", 'Locality')
        	
        	->setCellValue("N$i", 'Course Name')
        	->setCellValue("O$i", 'Level Of Learning')        	     	
        	->setCellValue("P$i", 'Age Group')
        	->setCellValue("Q$i", 'Tutor Format')
        	->setCellValue("R$i", 'Certificates granted')
        	->setCellValue("S$i", 'Rate (rupees)')
        	->setCellValue("T$i", 'Fee Comments')
        	->setCellValue("U$i", 'Fee Type Name')
        	->setCellValue("V$i", 'Payment policy')
        	->setCellValue("W$i", 'Cancellation policy')
        	->setCellValue("X$i", 'Schedule of Batch start dates')
        	->setCellValue("Y$i", 'No. of Weeks')
        	->setCellValue("Z$i", 'No. of Days/Weeks')
        	->setCellValue("AA$i", 'No. of Hours/Days')
        
        	->setCellValue("AB$i", 'Lesson location')
        	->setCellValue("AC$i", 'Travel radius')
        	
        	->setCellValue("AD$i", 'Tutor Brief self-description')
        	->setCellValue("AE$i", 'Profile Pic')
        	->setCellValue("AF$i", 'Course Description')
        	->setCellValue("AG$i", 'Class Pics(Y/N)')
        	
        	->setCellValue("AH$i", 'Facebook Link(Y/N)')
        	->setCellValue("AI$i", 'Twitter Links(Y/N)')
        	->setCellValue("AJ$i", 'Website Link(Y/N)')
        	->setCellValue("AK$i", 'Blogs(Y/N)')
        	->setCellValue("AL$i", 'Follow Up Comments');
   	    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:AL1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($exporttutorObj_rows as $t){
		set_time_limit(300);
	$sheet_0->setCellValue("A$i",  $t->id)
			->setCellValue("B$i",  $t->lastupdatedate)
	    	->setCellValue("C$i",  $t->tutor_first_name)
	    	->setCellValue("D$i",  $t->tutor_last_name)
	    	->setCellValue("E$i",  $t->tutor_email)
	    	->setCellValue("F$i",  $t->tutor_mobile)
	    	->setCellValue("G$i",  $t->tutor_landline)
	    	->setCellValue("H$i",  $t->company_name)
	    	->setCellValue("I$i",  $t->editdate)
	    	->setCellValue("J$i",  $t->active)
	    	->setCellValue("K$i",  $t->class_type_name)
	    	->setCellValue("L$i",  $t->skill_name)
	    	->setCellValue("M$i",  $t->address.", Mumbai")
	    	
	    	->setCellValue("N$i", $t->tutor_course_name)
	    	->setCellValue("O$i", $t->tutor_student_profile)        	     	
	    	->setCellValue("P$i", $t->student_age_group)
	    	->setCellValue("Q$i", $t->tutor_format)
	    	->setCellValue("R$i", $t->tutor_certificate_granted)
	    	->setCellValue("S$i", $t->tutor_rate)
	    	->setCellValue("T$i", $t->rate_comments)
	    	->setCellValue("U$i", $t->fee_type_name)
	    	->setCellValue("V$i", $t->pay_cycle_name)
	    	->setCellValue("W$i", $t->tutor_canc_policy)
	    	->setCellValue("X$i", $t->tutor_class_batch_schedule)
	    	->setCellValue("Y$i", $t->tutor_class_dur_wks)
	    	->setCellValue("Z$i", $t->tutor_class_dur_dayspwk)
	    	->setCellValue("AA$i", $t->tutor_class_dur_hrspday)

	    	->setCellValue("AB$i", $t->location_name)
	    	->setCellValue("AC$i", $t->travel_radius)
	    	
	    	->setCellValue("AD$i", $t->tutor_brief_desc)
	    	->setCellValue("AE$i", $t->tutor_image_type)
	    	->setCellValue("AF$i", $t->tutor_course_desc)
	    	->setCellValue("AG$i", $t->tutor_class_image_type)
	    	
	    	->setCellValue("AH$i", $t->tutor_fb_link)
	    	->setCellValue("AI$i", $t->tutor_tw_link)
	    	->setCellValue("AJ$i", $t->website_link)
	    	->setCellValue("AK$i", $t->blog_link)
	    	->setCellValue("AL$i", $t->followup_comment);
       
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);

	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Tutor Manager Report');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	$report_name="Tutor_manager_report_".date("Y-m-d").".xls";
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$report_name.'"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
public function exotelreportAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Exotel Reports";

	$records_per_page = $this->_request->getParam('shown');	
		
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}

	$exotelextensionObj = new Skillzot_Model_DbTable_Exotelextension();
	$extensionObj = $exotelextensionObj->fetchAll($exotelextensionObj->select()
					->setIntegrityCheck(false)
					->from(array('c'=>DATABASE_PREFIX."exotel_extension"),array('c.digit as digit','c.mobile_number as mobile_number','c.landline_number as landline_number',
						'c.from_number as from_number','c.create_date as create_date','count(*) as count'))
					->joinLeft(array('k'=>DATABASE_PREFIX."tx_tutor_profile"),'k.extension=c.digit',array('k.tutor_first_name as tutor_first_name','k.userid_new as userid_new','k.userid as userid'))
					->where("1=1")
					->group(array("c.from_number","c.mobile_number"))
                    ->order(array("c.create_date desc")));

	if(isset($extensionObj) && $extensionObj!="")
	{
		$this->view->extensionresult = $extensionObj;
	}
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($extensionObj);
	$paginator = Zend_Paginator::factory($extensionObj);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->extensionresult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);

}
public function exportexotelreportsAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
		
	
		$exotelextensionObj = new Skillzot_Model_DbTable_Exotelextension();
		$extensionObj = $exotelextensionObj->fetchAll($exotelextensionObj->select()
					->setIntegrityCheck(false)
					->from(array('c'=>DATABASE_PREFIX."exotel_extension"),array('c.digit as digit','c.mobile_number as mobile_number','c.landline_number as landline_number',
						'c.from_number as from_number','c.create_date as create_date','count(*) as count'))
					->joinLeft(array('k'=>DATABASE_PREFIX."tx_tutor_profile"),'k.extension=c.digit',array('k.tutor_first_name as tutor_first_name','k.userid as userid','k.userid_new as userid_new'))
					->where("1=1")
					->group(array("c.from_number","c.mobile_number"))
                    ->order(array("c.create_date desc")));	 
		/** PHPExcel **/
		require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
		
		//Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		//Set properties
		$objPHPExcel->getProperties()->setCreator("Skillzot")
									 ->setLastModifiedBy("Skillzot")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 		 ->setKeywords("office 2007 openxml php")
							 		 ->setCategory("Test result file");
		
		//Add some data
		$i = 1;
		
		$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
		$sheet_0->setCellValue("A$i", 'Create Date')
				->setCellValue("B$i", 'From Number(Incoming Calls)')
				->setCellValue("C$i", 'Tutor mobile Number')
            	->setCellValue("D$i", 'Tutor Alternative Number')
            	->setCellValue("E$i", 'Count Calls')
            	->setCellValue("F$i", 'Extenson')
            	->setCellValue("G$i", 'Tutor Name')
            	->setCellValue("H$i", 'URL')
            	->setCellValue("I$i", 'New URL');
            	   		    		
		$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
		$i = 3;
		$url = "http://www.skillzot.com/";
		foreach ($extensionObj as $t){
			$sheet_0->setCellValue("A$i",  $t->create_date)
				->setCellValue("B$i",  $t->from_number)
				->setCellValue("C$i",  $t->mobile_number)
            	->setCellValue("D$i",  $t->landline_number)
            	->setCellValue("E$i",  $t->count)
            	->setCellValue("F$i",  $t->digit)
            	->setCellValue("G$i",  $t->tutor_first_name)
            	->setCellValue("H$i",  $url.$t->userid)
            	->setCellValue("H$i",  $url.$t->userid_new);   		
			$i++;
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

		

		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Exotel call Report');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client's web browser (Excel5)
		$report_name="Exotel_call_report_".date("Y-m-d").".xls";
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$report_name.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
	public function exotelrecorddeleteAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
		$exotelextensionObj = new Skillzot_Model_DbTable_Exotelextension();
		$exotelextensionObj->delete();
		$authUserNamespace->status_message = "Detail has been deleted successfully";
		$this->_redirect('/adminnew/exotelreport');

	}
	public function travelradiousAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Travel radious";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	if($searchText!="")
	{
		$tutorsignupResult =$travelradiousObj->fetchAll($travelradiousObj->select()
						->setIntegrityCheck(false)
						->from(array('m'=>DATABASE_PREFIX."travel_radious"))
						->joinLeft(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),'m.tutor_id=a.id',array('a.tutor_first_name','a.tutor_last_name'))
						->where("(a.tutor_first_name like '%".$searchText."%' || a.tutor_last_name like '%".$searchText."%')" )
						->order(array("lastupdatedate desc")));
											
	}
	else
	{
		$tutorsignupResult = $travelradiousObj->fetchAll($travelradiousObj->select()
						->from(array('m'=>DATABASE_PREFIX."travel_radious"))
						//->where("m.suburbs_id!='0'")
						->order(array("lastupdatedate desc")));

	}
	if(isset($tutorsignupResult) && $tutorsignupResult!="")
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	//echo "in";exit;
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
public function edittravelradiousAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightbox");
	
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
	if(isset($id) && $id > 0)
	{
		$this->view->category = $tutorProfile->fetchRow("id='$id'");
	}
	$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
	$suburbsData = $localitysuburbsObj->fetchAll($localitysuburbsObj->select()
														  ->from(array('s'=>DATABASE_PREFIX."locality_suburbs"))
														  //->where("s.is_active='1'")
														  ->order(array("s.suburbs_name asc")));
	if (isset($suburbsData) && sizeof($suburbsData)>0)
	{
		$this->view->suburbsdata = $suburbsData;
	}	
	
	if($this->_request->isPost()){
	
		$suburbsID = $this->_request->getParam("multile_sub_id");
		
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			
			if($suburbsID=="" )$response["data"]["suburbs_name"] = "selectatleast";
			else $response["data"]["suburbs_name"] = "valid";
//				
			if(!in_array('selectatleast',$response['data']) && !in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
		}else{
				$suburbsID = substr($suburbsID, 1);
				$os = explode(",",$suburbsID);
				if (in_array("1", $os) || in_array("5", $os) || in_array("9", $os)) 
				{
						 $suburbsID=$suburbsID.","."13";
				}
				if(in_array("7", $os) || in_array("8", $os))
				{
					 $suburbsID=$suburbsID.","."14";
				}		
				$data = array("suburbs_id"=>$suburbsID);
				$travelradiousObj->update($data,"id='$id'");
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/travelradious';</script>";
			//echo "<script>parent.Mediabox.close();</script>";
			
		}
	}
}

public function skillfriendlyAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->setLayout("admin");
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage Skill";
	$skillObj = new Skillzot_Model_DbTable_Skills();
	$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	
	if($searchText!="")
	{
		$skillrowResult =$skilldisplayObj->fetchAll($skilldisplayObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"))
						->where("s.skill_id!='0' && s.skill_depth!='0' && (s.skill_name like '%".$searchText."%' || s.skill_uniq like '%".$searchText."%' || s.page_title like '%".$searchText."%')")
						->order(array("s.skill_depth asc")));
											
	}
	else
	{
		$skillrowResult = $skilldisplayObj->fetchAll($skilldisplayObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"))
						->where("s.skill_id!='0' && skill_depth!='0'")
						->order(array("s.skill_depth asc")));

	}

	if(isset($skillrowResult) && $skillrowResult!="")
	{
		$this->view->skillrowResult = $skillrowResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 20;
	$record_count = sizeof($skillrowResult);
	$paginator = Zend_Paginator::factory($skillrowResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->skillrowResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
											   
}
public function editfridlyskillAction(){
$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightbox");
	$skillObj = new Skillzot_Model_DbTable_Skills();
	$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
	
	$skillrowResult = $skilldisplayObj->fetchAll($skilldisplayObj->select()
						->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id'))
						->where("s.parent_skill_id='0' && s.skill_id!='0'"));
	$this->view->maincategory = $skillrowResult;
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	
	//echo $id;exit;
	if(isset($id) && $id > 0)
	{
		$fetch_data = $skilldisplayObj->fetchRow("skill_id='$id'");
		$this->view->category =$fetch_data ;
	}

	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Skill";
	}else{
	    $authUserNamespace->admin_page_title = "Add Skill";
	}

	if($this->_request->isPost()){

		$category_id = $this->_request->getParam("Category_name");
		$subcategory_name = $this->_request->getParam("categories_sub");
		$skill_name = $this->_request->getParam("Skill_name");
		$skill_description = $this->_request->getParam("SkillDescription");
		$enable = $this->_request->getParam("Enable");
		$menuable = $this->_request->getParam("menuable");
		$ordernunber = $this->_request->getParam("OrderNumber");
		$hiddenid= $this->_request->getParam("hiddenvar");
		
		$urlName= $this->_request->getParam("urlName");
		$pageTitle= $this->_request->getParam("skillPagetitle");


		if($this->_request->isXmlHttpRequest()){

			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();

			if($skill_description == "")$response["data"]["SkillDescription"] = "null";
			else $response["data"]["SkillDescription"] = "valid";
			
			if($urlName == "")$response["data"]["urlName"] = "null";
			else $response["data"]["urlName"] = "valid";
			
			if($pageTitle == "")$response["data"]["skillPagetitle"] = "null";
			else $response["data"]["skillPagetitle"] = "valid";

			if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";

			echo json_encode($response);

		}else{
				$data = array("skill_description"=>$skill_description,"skill_uniq"=>$urlName,"page_title"=>$pageTitle);
				
				$skilldisplayObj->update($data,"skill_id='$id'");
				$authUserNamespace->status_message = "Skills has been updated successfully";
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/adminnew/skillfriendly';</script>";
		}
	}
}
public function tutorfriendlyurlAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$authUserNamespace->admin_page_title = "Manage Tutor Friendly Url";
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	
	$searchText = addslashes($this->_request->getParam('searchtext'));
	if (isset($searchText) && $searchText!=""){
		$this->view->search_val = stripcslashes ($searchText);
	}
	
	if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
	}
	
	if($searchText!="")
	{
		$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						->setIntegrityCheck(false)
						->from(array('m'=>DATABASE_PREFIX.'tx_tutor_profile'),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
						->where("(m.tutor_first_name like '%".$searchText."%' || m.tutor_last_name like '%".$searchText."%' || m.company_name like '%".$searchText."%' || m.tutor_url_name like '%".$searchText."%') && m.is_active='0'" ));
	}
	else
	{
		$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
						->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
						->where("m.is_active='0'")
						->order(array("id desc")));

	}
	//echo $searchText;exit;
	if(isset($tutorsignupResult) && sizeof($tutorsignupResult)>0)
	{
		$this->view->tutorsignupResult = $tutorsignupResult;
	}
		/*pagination code*/
	
	$page = $this->_request->getParam('page',1);
	//$this->view->page = $page;
	if($records_per_page=="")$records_per_page = 10;
	$record_count = sizeof($tutorsignupResult);
	$paginator = Zend_Paginator::factory($tutorsignupResult);
	$paginator->setItemCountPerPage($records_per_page);
	$paginator->setCurrentPageNumber($page);
	$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
	$this->view->tutorsignupResult = $paginator;
	$page_number  = $record_count / 1;
	$page_number_last =  floor($page_number);
	
}
public function edittutorfriendlyurlAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/adminnew");
	$this->_helper->layout()->setLayout("lightbox");
	
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	if(isset($id) && $id > 0)
	{
		$this->view->category = $tutorProfile->fetchRow("id='$id'");
	}
	
	if(isset($id) && $id > 0)
	{
		$authUserNamespace->admin_page_title = "Update Status";
	}
	
	if($this->_request->isPost()){
	
		$status_value = $this->_request->getParam("tutorurlnamefriendly");
		$usernameValue = $this->_request->getParam("tutorusername");
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			
			if($status_value == "")$response["data"]["tutorurlnamefriendly"] = "null";
			else $response["data"]["tutorurlnamefriendly"] = "valid";
			
			if($usernameValue == "")$response["data"]["tutorusername"] = "null";
			else $response["data"]["tutorusername"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
		}else{
				$data = array("tutor_url_name"=>$status_value,"userid"=>$usernameValue);
				$tutorProfile->update($data,"id='$id'");
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorfriendlyurl';</script>";
			
		}
	}
}
public function testtestAction(){

$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
$this->_helper->layout()->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);

$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
$companyaddressObj = new Skillzot_Model_DbTable_Companyaddresslist();
$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
$tutorMessageobj = new Skillzot_Model_DbTable_Messagetutor();

//$radiousData = $travelradiousObj->fetchAll();

	
	$fetchallturoriddata = $tutorProfile->fetchAll($tutorProfile->select()
									->from(array('p'=>DATABASE_PREFIX."tx_tutor_profile"),array('id','company_name','tutor_first_name','tutor_last_name','userid'))
									->order(array("p.id asc")));
									echo sizeof(fetchallturoriddata);
									foreach ($fetchallturoriddata as $tutorrow)
									{
										$userName = $tutorrow->tutor_first_name.$tutorrow->tutor_last_name;											
										$userNametopass= preg_replace("/[^A-Za-z0-9]/", "",$userName);
										echo "RedirectMatch 301". " "."http://www.skillzot.com/".strtolower($tutorrow->userid)." "."http://www.skillzot.com/".$tutorrow->userid;
										echo "<br/>";
									}	

				
 exit;
	foreach($radiousData as $tt)
		{
		
		$profile_rows1 = $tutorProfile->fetchRow($tutorProfile->select()
										->setIntegrityCheck(false)
										->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.id'))
										->where("s.id='$tt->tutor_id'"));
		
		if(isset($profile_rows1) && sizeof($profile_rows1)>0)
		{
			//echo "dd";
		}else{
		//$tutorMessageobj->delete("to_id='$tt->tutor_id'");
			//echo $tt->tutor_id;
		}
		echo "<br/>";
		}
//print_r($profile_rows1);exit;										

}
}
