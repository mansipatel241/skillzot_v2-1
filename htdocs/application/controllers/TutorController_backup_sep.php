<?php
class TutorController extends Zend_Controller_Action{
	
	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');

		if (isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && isset($_COOKIE['useridfriendly'] ) && $_COOKIE['logintype']=='1') {
			//echo "in";useridfriendly
			$authUserNamespace->maintutorid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
			$authUserNamespace->useridfriedly = $_COOKIE['useridfriendly'];
		}else if(isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='2'){
			//echo 
			$authUserNamespace->studentid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}
    		
	}
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout('layout2');

	}
	public function calAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(true);

	}
	public function viewcalAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(true);

	}
	public function viewcal1Action(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(true);

	}
	public function addbatchAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
		$getSelectedId = $this->_request->getParam("s_id");
		
		$batchTextValue = $this->_request->getParam("b_text_value");
		if (isset($batchTextValue) && $batchTextValue!="")
		{
			$this->view->batchtext = $batchTextValue;
		}
		$batchColor = $this->_request->getParam("b_color_value");
		if (isset($batchColor) && $batchColor!="")
		{
			$this->view->batchcolor = "#".strtoupper($batchColor);
		}
		if (isset($getSelectedId) && $getSelectedId!="")
		{
			$tempstartVararr = explode('_',$getSelectedId);
			$tempstartVar = $tempstartVararr[1];
			
			if (isset($tempstartVar) && $tempstartVar!="")
			{
				$this->view->starttime = $tempstartVar;
			}
			$tempendVar = $tempstartVar+1;
			if (isset($tempendVar) && $tempendVar!="")
			{
				$this->view->endtime = $tempendVar;
			}
			$tempdayVar = $tempstartVararr[2];
			if (isset($tempdayVar) && $tempdayVar!="")
			{
				$this->view->dayval = $tempdayVar;
			}
		}
//				$tutor_id = "2";
//				$color="#FFFFFF";
//				$row='38';
//				$colom ='7';
//				$desc="";
//				$endTime = '0';
//				$lastupdatedate = date("Y-m-d H:i:s");
//				for($i=1;$i<=$colom;$i++){
//					for($j=1;$j<=$row;$j++){
//					$data = array("tutor_id"=>$tutor_id,"b_description"=>$desc,"b_description"=>$desc,"b_color"=>$color,"b_color1"=>$color,
//					"b_day"=>$i,"b_start_timings"=>$j,"b_end_timings"=>$endTime,
//					"lastupdatedate"=>$lastupdatedate);
//					$batchdetailObj->insert($data);
//
//					}
//				}
//exit;
		
		if($this->_request->isPost()){
				
			if (isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!='')
			{
				$tutorId=$authUserNamespace->tutorsessionid;
			}else{
				$tutorId=$authUserNamespace->maintutorid;
			}
			
			
			$desc=$this->_request->getParam("batch_desc");
			$color=$this->_request->getParam("batch_color");
			if(isset($color) && $color==""){$color="#FFFFFF";}
			$day1=$this->_request->getParam("day_temp_id");
			$startTime=$this->_request->getParam("batch_start_time");
			$endTime=$this->_request->getParam("batch_end_time");
			$lastupdatedate = date("Y-m-d H:i:s");
			
				if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				$response=array();
				
				if($desc == "Location")$response["data"]["batch_desc"] = "locationerr";
				else $response["data"]["batch_desc"] = "valid";
				
				if(($day1=="" || $startTime > $endTime))$response["data"]["batch_start_time"] = "invalid";
				else $response["data"]["batch_start_time"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('locationerr',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
				
				}else{
					define('TUTORID',$tutorId);
					if (isset($day1) && $day1!=""){
						$dayFinal = substr($day1, 1);
						$arrayDay = explode(',',$dayFinal);
						
						if (isset($arrayDay) && sizeof($arrayDay)>1)
						{
						}else{
							$day = $dayFinal;
						}
					}
						$desc = ucfirst($desc);
						
					if (isset($arrayDay) && sizeof($arrayDay)>1)
					{
						//print_r($arrayDay);exit;
							for($j=0;$j<sizeof($arrayDay);$j++){
								for($i=$startTime;$i<$endTime;$i++){
									//echo "dd";exit;
									$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
									//print_r($data);exit;
									$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$arrayDay[$j]' && b_start_timings='$i'");
								}
							}
							//echo 'in';exit;
					}else{
						//echo 'out';exit;
					if (isset($day) && $day=='10')
							{
								$temp = 1;
								$tempend= 7;
								for($j=$temp;$j<=$tempend;$j++){
									for($i=$startTime;$i<$endTime;$i++){
										//echo "dd";exit;
										$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
										//print_r($data);exit;
										$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$j' && b_start_timings='$i'");
									}
								}
							}elseif(isset($day) && $day=='8'){
								$temp = 1;
									$tempend= 5;
									for($j=$temp;$j<=$tempend;$j++){
										for($i=$startTime;$i<$endTime;$i++){
											//echo "dd";exit;
											$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
											//print_r($data);exit;
											$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$j' && b_start_timings='$i'");
										}
									}
							}elseif(isset($day) && $day=='9'){
								$temp = 6;
									$tempend= 7;
									for($j=$temp;$j<=$tempend;$j++){
										for($i=$startTime;$i<$endTime;$i++){
											//echo "dd";exit;
											$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
											//print_r($data);exit;
											$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$j' && b_start_timings='$i'");
										}
									}
							}else{
								for($i=$startTime;$i<$endTime;$i++){
									$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
									//print_r($data);exit;
									$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$day' && b_start_timings='$i'");
								}
							}
					}
						echo "<script>parent.testfun(".TUTORID.");</script>";
						echo "<script>parent.Mediabox.close();</script>";
				}
		}
	}
	public function updatedetailsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
					$tutorId = $this->_request->getParam("tutor_id");
					$defaultCourseid = $this->_request->getParam("default_course_id");
					$courseId = $this->_request->getParam("course_id");
					if (isset($defaultCourseid) && $defaultCourseid!=''){
						$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
																  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
																  ->where("s.tutor_id='$tutorId' && s.tutor_course_id='0'"));
					}else{
					$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
																  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
																  ->where("s.tutor_id='$tutorId'"));
					}
					$i=0;
					$tags = "";
					$tags['totalsize'] = sizeof($batch_rows);
					if (isset($batch_rows) && sizeof($batch_rows)>0)
					{
						foreach($batch_rows as $a){
							$tags[$i]['colorcode'] = $a->b_color;
							$tags[$i]['description'] = $a->b_description;
							$i++;
						}
					}
					if (isset($tags) && sizeof($tags)>1)
					{
						echo json_encode($tags);
					}else{
						
						echo json_encode(null);
					}
				}
	  		}
	}
public function profileupdatedetailsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
					
				$tutorId = $this->_request->getParam("tutor_id");
				$courseId = $this->_request->getParam("course_id");
				
				$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
											  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
											  ->where("s.tutor_id='$tutorId' && s.tutor_course_id='$courseId'"));
				$i=0;
				$tags = "";
				$tags['totalsize'] = sizeof($batch_rows);
				
					foreach($batch_rows as $a){
						$tags[$i]['colorcode'] = $a->b_color;
						$tags[$i]['description'] = $a->b_description;
						$i++;
					}
					if (isset($tags) && sizeof($tags)>1)
					{
						echo json_encode($tags);
					}else{
						
						echo json_encode(null);
					}
				}
	  		}
	}
public function viewupdatedetailsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$tutorId = $this->_request->getParam("tutor_id");
				$batchType = $this->_request->getParam("batch_type");
				$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
															  ->where("s.tutor_id='$tutorId'"));
															  
				$batch_rows1 = $batchdetailObj->fetchAll($batchdetailObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
															  ->where("s.tutor_id='$tutorId' && s.b_description1='Available' && s.b_description1!=''"));
															  
															//  echo sizeof($batch_rows1);exit;
				$i=0;
				$tags = "";
				$tags['totalsize'] = sizeof($batch_rows);
				
				if (isset($batch_rows1) && sizeof($batch_rows1) > 0)
				{
					$tags['fully_book_display'] = "Available";
				}else{
					$tags['fully_book_display'] = "Fully booked";
				}
					foreach($batch_rows as $a){
						$tags[$i]['colorcode'] = $a->b_color1;
						if($a->b_description1=="Booked" || $a->b_description1=="Available"){
							$temp_val ="";
							$tags[$i]['description'] = $temp_val;
						}else{
							$tags[$i]['description'] = $a->b_description1;
						}
						//$tags[$i]['locality_id'] = $a->address_id;
						//$arry[]=$tags;
						$i++;
					}
					
//				foreach($batch_rows as $a){
//						$tags[$i]['colorcode'] = $a->b_color;
//						$tags[$i]['description'] = $a->b_description;
//						$i++;
//					}
				echo json_encode($tags);
				}
	  		}
	}
public function viewupdatedetails1Action()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$tutorId = $this->_request->getParam("tutor_id");
				$batchType = $this->_request->getParam("batch_type");
				$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
															  ->where("s.tutor_id='$tutorId'"));
															  
				$batch_rows1 = $batchdetailObj->fetchAll($batchdetailObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
															  ->where("s.tutor_id='$tutorId' && s.b_description!=''"));
															  
															//  echo sizeof($batch_rows1);exit;
				$i=0;
				$tags = "";
				$tags['totalsize'] = sizeof($batch_rows);
				
				if (isset($batch_rows1) && sizeof($batch_rows1) > 0)
				{
					$tags['fully_book_display'] = "Available";
				}else{
					$tags['fully_book_display'] = "Fully booked";
				}
//					foreach($batch_rows as $a){
//						$tags[$i]['colorcode'] = $a->b_color1;
//						$tags[$i]['description'] = $a->b_description1;
//						//$tags[$i]['locality_id'] = $a->address_id;
//						//$arry[]=$tags;
//						$i++;
//					}
					
				foreach($batch_rows as $a){
						$tags[$i]['colorcode'] = $a->b_color;
						if($a->b_description=="Booked" || $a->b_description=="Available"){
							$temp_val ="";
							$tags[$i]['description'] = $temp_val;
						}else{
							$tags[$i]['description'] = $a->b_description;
						}
						$i++;
					}
				echo json_encode($tags);
				}
	  		}
	}
public function deletedetailsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
		$tutorId = '1';
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				//$batchdetailObj->delete("tutor_id='$tutorId'");
				$color="#FFFFFF";
				$desc="";
				$data = array("b_description"=>$desc,"b_description1"=>$desc,"b_color"=>$color,"b_color1"=>$color);
				$batchdetailObj->update($data,"tutor_id=$tutorId");
				$tags = "success";
			
				echo json_encode($tags);
				}
	  		}
	}
	public function registerAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
//		 $request = $this->getRequest();
//		$year    = $request->getParam(2);
		$this->_helper->layout()->setLayout('layout2');
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
		$classtypeResult = $classtypeObj->fetchAll($classtypeObj->select()
										->from(array('s'=>DATABASE_PREFIX."master_tutor_class_types"))
										->order(array("class_type_id asc")));
		$this->view->classtype = $classtypeResult;
							
		$adressObj = new Skillzot_Model_DbTable_Address();
		$addressResult = $adressObj->fetchAll($adressObj->select()
								   ->from(array('a'=>DATABASE_PREFIX."master_address"))
								   ->where("a.address_type_id='2' && address_id!='0'"));
		$this->view->address = $addressResult;
		$cityResult = $adressObj->fetchAll($adressObj->select()
								->from(array('a'=>DATABASE_PREFIX."master_address"))
								->where("a.address_type_id='1' && a.address_id!='0'"));
		$this->view->city = $cityResult;
		//$tutorid = $this->_request->getParam("tutorid");
		if(isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!=""){
			$this->view->setradiodisable = 'disableradio';
			$tutorData = $tutorProfile->fetchRow("id ='$authUserNamespace->tutorsessionid'");
			$this->view->tutordata = $tutorData;
		}
		$authUserNamespace->classtype = "";
	
		if($this->_request->isPost()){
			
			$companyName1 = $this->_request->getParam("company_name");//when forth radio button select
			$companyName2 = $this->_request->getParam("indi_cmnyname");//when first three radio button select(individual)
			$firstnametxt = $this->_request->getParam("firstname");
			$lastnametxt = $this->_request->getParam("lastname");
			$emailtxt = $this->_request->getParam("email");
			$userIDforunique = $this->_request->getParam("userIDforuniq"); 
			if(isset($userIDforunique) && $userIDforunique!=null)
			{
					if(ord($userIDforunique[0])>=97 && ord($userIDforunique[0])<=122)
					{
						$userIDforunique[0]=chr( ord($userIDforunique[0])-32);
					}
			}		
			$userIDforunique = preg_replace('/[^A-Za-z0-9\-]/', '', $userIDforunique);
			//$userIDforunique= str_replace(" ","",ucfirst($userIDforunique));
			$passwordtxt = $this->_request->getParam("password");
			$confirm_passwordtxt = $this->_request->getParam("confirm_password");
//			$localitytxt = $this->_request->getParam("locality");
//			$citytxt = $this->_request->getParam("city");
//			$addresstxt = $this->_request->getParam("address");
//			$pincodetxt = $this->_request->getParam("pincode");
			$mobiletxt = $this->_request->getParam("mobile");
			$landlinetxt = $this->_request->getParam("landline");
			$radiotxt = $this->_request->getParam("radioval");
			
			$phoneVisible = $this->_request->getParam("ph_visible_val");
			
			if(isset($authUserNamespace->classtype) && $authUserNamespace->classtype!=""){
				
			}
			else{
				
				if($radiotxt==2)
				{
					$authUserNamespace->classtype = "2";
				}
				elseif($radiotxt==3)
				{
					$authUserNamespace->classtype = "3";
				}
				elseif($radiotxt==4)
				{
					$authUserNamespace->classtype = "4";
				}
				elseif($radiotxt==1)
				{
					$authUserNamespace->classtype = "1";
				}
				else{
					$authUserNamespace->classtype = "";
				}
			}
			if (isset($authUserNamespace->classtype) && $authUserNamespace->classtype=="4"){
				$companyName = $companyName1;
			}else{
				$companyName = $companyName2;
			}
			//echo $radiotxt;
			//echo $companyName;exit;
			//print "hr:".$authUserNamespace->classtype;
			
			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
	
				if($landlinetxt == "")$response["data"]["landline"] = "onlyenternull";
				elseif ($landlinetxt=="0" || strlen($landlinetxt)<10 || strlen($landlinetxt)>12)$response["data"]["landline"] = "mobilelength";
				else $response["data"]["landline"] = "valid";
				
				if($mobiletxt == "")$response["data"]["mobile"] = "onlyenternull";
				elseif((!is_numeric($mobiletxt) || $mobiletxt=="0"))$response["data"]["mobile"] = "mobileinvalid";
				elseif(($mobiletxt != "" && strlen($mobiletxt)!=10))$response["data"]["mobile"] = "mobilelength";
				else $response["data"]["mobile"] = "valid";
				
				if ($response["data"]["mobile"]=="valid")
				{
					$response["data"]["landline"] = "valid";
				}elseif($response["data"]["landline"]=="valid"){
					$response["data"]["mobile"]="valid";
				}
				
				if($passwordtxt == "")$response["data"]["password"] = "null";
				elseif(strlen($passwordtxt)<5)$response["data"]["password"] = "passlength";
				else $response["data"]["password"] = "valid";
				
				if($confirm_passwordtxt == "")$response["data"]["confirm_password"] = "conformpassnull";
				elseif($confirm_passwordtxt != $passwordtxt)$response["data"]["confirm_password"] = "passnotmatch";
				else $response["data"]["confirm_password"] = "valid";
				
				if (isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!="")
				{
					$tutorProfilerowforuserID = $tutorProfile->fetchRow($tutorProfile->select()
													 ->where("userid='$userIDforunique' && id!='$authUserNamespace->tutorsessionid'"));
				}else{
					$tutorProfilerowforuserID = $tutorProfile->fetchRow($tutorProfile->select()
													 ->where("userid='$userIDforunique'"));
				}
				
					
				
				
				if($userIDforunique == "")$response["data"]["userIDforuniq"] = "null";
				elseif(isset($tutorProfilerowforuserID) && sizeof($tutorProfilerowforuserID) > 0)
				{
					$response['data']['userIDforuniq'] = "useridduplicate";
				}
				else $response["data"]["userIDforuniq"] = "valid";
				
				if($emailtxt == "")$response["data"]["email"] = "null";
				elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $emailtxt)) $response['data']['email'] = "emailinvalid";
				else $response["data"]["email"] = "valid";
				
				
			/*fo update time email not duplicate so if condition*/
				if(isset($authUserNamespace->tutoremail) && $authUserNamespace->tutoremail!=""){
					$sessionemail = $authUserNamespace->tutoremail;
					$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
													 ->where("tutor_email='$emailtxt' && id!='$authUserNamespace->tutorsessionid'"));
					if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
					{
						$response['data']['email'] = "emailduplicate";
					}
					$studentsignup_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 	 	  ->where("std_email='$emailtxt'"));
					if (isset($studentsignup_row) && sizeof($studentsignup_row) > 0)
					{
						$response['data']['email'] = "emailduplicate";
					}
					
				}else{
					$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
													 ->where("tutor_email='$emailtxt'"));
					if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
					{
						$response['data']['email'] = "emailduplicate";
					}
					$studentsignup_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 	 	  ->where("std_email='$emailtxt'"));
					if (isset($studentsignup_row) && sizeof($studentsignup_row) > 0)
					{
						$response['data']['email'] = "emailduplicate";
					}
				}
				
				if($lastnametxt == "")$response["data"]["lastname"] = "null";
				else $response["data"]["lastname"] = "valid";
				
				if($firstnametxt == "")$response["data"]["firstname"] = "null";
				else $response["data"]["firstname"] = "valid";

				
				if (isset($authUserNamespace->classtype) && $authUserNamespace->classtype=="4")
				{
					if($companyName == "")$response["data"]["company_name"] = "null";
					else $response["data"]["company_name"] = "valid";
				}
			

				
				
				

//				if($pincodetxt == "")$response["data"]["pincode"] = "null";
//				elseif((!is_numeric($pincodetxt) || $pincodetxt=="0" || strlen($pincodetxt)!=6))$response["data"]["pincode"] = "pincodeinvalid";
//				else $response["data"]["pincode"] = "valid";
//
//				if($localitytxt == "")$response["data"]["locality"] = "selectnull";
//				else $response["data"]["locality"] = "valid";
//
//				if($citytxt == "")$response["data"]["city"] = "selectnull";
//				else $response["data"]["city"] = "valid";

//				if($mobiletxt == "" && $landlinetxt == "")$response["data"]["mobile"] = "onlyenternull";
//			    elseif((($mobiletxt != "") && (!is_numeric($mobiletxt) || $mobiletxt=="0")) || ($landlinetxt=="0" || strlen($landlinetxt)<12))$response["data"]["mobile"] = "mobileinvalid";
//				elseif($mobiletxt != "" && strlen($mobiletxt)!=10 )$response["data"]["mobile"] = "mobilelength";
//				else $response["data"]["mobile"] = "valid";
					
				
				
				
				
				
//				if($addresstxt == "")$response["data"]["address"] = "null";
//				else $response["data"]["address"] = "valid";
				
				

				if(!in_array('null',$response['data'])&& !in_array('useridduplicate',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
				&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
				$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);
				
			}
			else {
				$lastupdatedate = date("Y-m-d H:i:s");
				if(isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!=""){
					$id = $authUserNamespace->tutorsessionid;
				}
				
					if (isset($companyName) && $companyName!="")
					{
						$userNameforsearchUrl= preg_replace("/[^A-Za-z0-9]/", "-",$companyName);
						if (strstr($userNameforsearchUrl, '--') != false) {
							$userNameforsearchUrl = str_replace("--", "-", $userNameforsearchUrl);
						}
						if (strstr($userNameforsearchUrl, '---') != false)
						{
							$userNameforsearchUrl = str_replace("---", "-", $userNameforsearchUrl);
							
						}
					}else{
						$userNameforsearchUrl=strtolower($firstnametxt)." ".strtolower($lastnametxt);//.$tutorrow->id;
						$userNameforsearchUrl= preg_replace("/[^A-Za-z0-9]/", "-",$userNameforsearchUrl);
							
						if (strstr($userNameforsearchUrl, '--') != false) {
							$userNameforsearchUrl = str_replace("--", "-", $userNameforsearchUrl);
						}
						if (strstr($userNameforsearchUrl, '---') != false)
						{
							$userNameforsearchUrl = str_replace("---", "-", $userNameforsearchUrl);
							
						}
					}
				
				
				if (isset($authUserNamespace->classtype) && $authUserNamespace->classtype!="")
				{
					$data = array("company_name"=>$companyName,"tutor_first_name"=>$firstnametxt,"tutor_last_name"=>$lastnametxt,"tutor_email"=>$emailtxt,"tutor_v2_pwd"=>sha1($passwordtxt),
												"userid"=>$userIDforunique,"tutor_url_name"=>$userNameforsearchUrl,
				 								"tutor_mobile"=>$mobiletxt,"tutor_landline"=>$landlinetxt,"phone_visible"=>$phoneVisible,
				 								"tutor_class_type"=>$authUserNamespace->classtype,"lastupdatedate"=>$lastupdatedate,"editdate"=>$lastupdatedate);
				}else{
					$data = array("company_name"=>$companyName,"tutor_first_name"=>$firstnametxt,"tutor_last_name"=>$lastnametxt,"tutor_email"=>$emailtxt,"tutor_v2_pwd"=>sha1($passwordtxt),
				 								"userid"=>$userIDforunique,"tutor_url_name"=>$userNameforsearchUrl,
												"tutor_mobile"=>$mobiletxt,"tutor_landline"=>$landlinetxt,"phone_visible"=>$phoneVisible,
				 								"tutor_class_type"=>$radiotxt,"lastupdatedate"=>$lastupdatedate,"editdate"=>$lastupdatedate);
				}
				if (isset($id) && $id!="")
				{
					$tutorProfile_row = $tutorProfile->fetchRow("id=$id");
					if($tutorProfile_row!="" && sizeof($tutorProfile_row)>0)
					{
						$tutorProfile->update($data,"id=$id");
					}
				}
				else
				{
					$tutorProfile->insert($data);
					$tutorSessionId = $tutorProfile->getAdapter()->lastInsertId("tutor_email='$emailtxt'");
					$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
					
					//---------------calender entry--------------------
					$tutor_id = $tutorSessionId;
					$color="#FFFFFF";
					$row='38';
					$colom ='7';
					$desc="";
					$endTime = '0';
					$lastupdatedate = date("Y-m-d H:i:s");
					for($i=1;$i<=$colom;$i++){
						for($j=1;$j<=$row;$j++){
						$data = array("tutor_id"=>$tutor_id,"b_description"=>$desc,"b_description1"=>$desc,"b_color"=>$color,"b_color1"=>$color,
						"b_day"=>$i,"b_start_timings"=>$j,"b_end_timings"=>$endTime,
						"lastupdatedate"=>$lastupdatedate);
						$batchdetailObj->insert($data);
							
						}
					}
					//---------------calender entry--------------------
					
					//-------------------------travel radious entryy code----------------------
					
					$lastupdatedate = date("Y-m-d H:i:s");
					$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();

					$data = array("tutor_id"=>$tutor_id,"suburbs_id"=>'0',"lastupdatedate"=>$lastupdatedate);
					$travelradiousObj->insert($data);
													
		
					//---------------------end of travel radious ------------------------------
					
					$authUserNamespace->tutorsessionid = $tutorSessionId;					
					$authUserNamespace->useridfriedly = $userIDforunique;										
					$authUserNamespace->formname1 = "registerpage";
					$authUserNamespace->tutoremail = $emailtxt;
					$this->_redirect('/teacher-signup-experience');
				}
				if (isset($authUserNamespace->formname1) && $authUserNamespace->formname1=="registerpage")
				{
					$this->_redirect('/teacher-signup-experience');
				}
			}
		}
	}
	public function experinceAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout('layout2');
		//echo "inn";exit;
		if((!isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid == NULL) && (!isset($authUserNamespace->formname1) &&  $authUserNamespace->formname1 != "registerpage"))
		{
			$this->_redirect("/tutor/register");
		}
		$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
		$tutorstatusObj = new Skillzot_Model_DbTable_Tutorstatus();
		$companylistObj = new Skillzot_Model_DbTable_Companylist();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		
		$tutorstatusResult = $tutorstatusObj->fetchAll();
		$this->view->tutorstatus = $tutorstatusResult;
		//--------------step 3 display coding-----------------
		$tutorgradeObj = new Skillzot_Model_DbTable_Tutorgradesteach();
		$tutorgradeResult = $tutorgradeObj->fetchAll();
		if (isset($tutorgradeResult) && sizeof($tutorgradeResult) > 0) $this->view->tutorgrade = $tutorgradeResult;
		
		
	$skillobj1 = new Skillzot_Model_DbTable_Skills();
		$skillRows = $skillobj1->fetchAll($skillobj1->select()
											  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
											  ->where("is_enabled = 1 && parent_skill_id = 0 && skill_id!=0")
											  ->order(array("order_id asc")));
		if (isset($skillRows) && sizeof($skillRows) > 0)
			{
				$this->view->mainSkill = $skillRows;
			}
		//----------end step 3 display----------------------
		if(isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!="" && isset($authUserNamespace->formname2) )
		{
			$tutorid = $authUserNamespace->tutorsessionid;
			$tutorexperienceData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
													->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
													->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='1'"));
			$this->view->tutorexperiencedata = $tutorexperienceData;
			
			foreach ($tutorexperienceData as $experienceID)
			{
				$tutorexperienceIDarray[] =  $experienceID->id;
			}
			$tutorqualificationData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
										->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='2'"));
			$this->view->tutorqualificationdata = $tutorqualificationData;
			
			foreach ($tutorqualificationData as $qulificationID)
			{
				$qulificationIDarray[] =  $qulificationID->id;
			}
			
			//-------education commet
			
//			$tutoreducationData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
//										->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
//										->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='3'"));
//			$this->view->tutoreducationdata = $tutoreducationData;
//
//			foreach ($tutoreducationData as $educationID)
//			{
//				$educationIDarray[] =  $educationID->id;
//			}
			
			//-------------------
			
//			if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="4" ||$authUserNamespace->classtype=="3"))
//			{
				//-----------------not needed now-----------------------
				$companyaddressData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
													->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
													->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='4'"));
				if (isset($companyaddressData) && sizeof($companyaddressData)>0)
				{
					$this->view->companyaddressdata = $companyaddressData;
				}
				foreach ($companyaddressData as $addressID)
				{
					$addressIDarray[] =  $addressID->id;
				}
				//----------------------------------------------------------------------
				
				//-------------year drop down display selected-----------------------------------------------
				$companyData = $companylistObj->fetchAll($companylistObj->select()
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_company"))
											->where("c.tutor_id='$tutorid'"));
				$this->view->companydata = $companyData;
				//----------------------------------------------------------------------------------------------------
			//}
			
			//------------------------step 3 coding----------------------------
			$tutorcourseData = $tutorCourseobj->fetchRow("id ='$authUserNamespace->tutorcoursesessionid'");
			if (isset($tutorcourseData) && sizeof($tutorcourseData) > 0)
			{
				$this->view->tutorcoursedata = $tutorcourseData;
			}
			//------------------------------------------------------------------
		}
		if($this->_request->isPost()){
					
			$avgexpvar = $this->_request->getParam("avgexp");
			$avgqulvar = $this->_request->getParam("avgqul");
			$avgeduvar = $this->_request->getParam("avgedu");
			$avgcompvar = $this->_request->getParam("avgcomp");
			
//			echo "a".$avgexpvar;
//			echo "b".$avgqulvar;
//			exit;
			
			$institutetxt = $this->_request->getParam("institute");
			$subjecttxt = $this->_request->getParam("subject");
			$startyeardrp = $this->_request->getParam("startyear");
			$endyeardrp = $this->_request->getParam("endyear");
			$degreetxt = $this->_request->getParam("degree");
			$specialitytxt = $this->_request->getParam("speciality");
			$universitytxt = $this->_request->getParam("university");
			$completion_yeardrp = $this->_request->getParam("completion_year");
			
			$skillIds = $this->_request->getParam("hiddenVal2");
			
//			$collegetxt = $this->_request->getParam("college");
//			$diplomatxt = $this->_request->getParam("diploma");
//			$fieldsofstudytxt = $this->_request->getParam("fieldsofstudy");
//			$edbackcompletionyeardrp = $this->_request->getParam("edbackcompletionyear");
			
			for($i=0;$i<sizeof($institutetxt);$i++)
				{
					$statusteachname = "statusteach".$i;
					$statusTeach[$i] = $this->_request->getParam($statusteachname,0);
					
					$statusprofessionname = "statusprofession".$i;
					$statusProfession[$i] = $this->_request->getParam($statusprofessionname,0);
					
					$statushobbyname = "statushobby".$i;
					$statushobby[$i] = $this->_request->getParam($statushobbyname,0);
				}
			$companyName = $this->_request->getParam("company_name");
			$companyStartyear =  $this->_request->getParam("companystartdate");
			
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				$response=array();

				for($i=0;$i<sizeof($institutetxt);$i++)
				{
					if($institutetxt[$i] == "School/Organization/Self-employed" || $subjecttxt[$i]=="Skills taught or practiced" || $startyeardrp[$i]=="" || $endyeardrp[$i]=="")$response["data"]["institute".$i] = "allfield";
					else if($startyeardrp[$i]>$endyeardrp[$i])$response["data"]["institute".$i] = "yearerror";
					else $response["data"]["institute".$i] = "valid";
				}
				
				if($skillIds == "")$response["data"]["hiddenVal2"] = "selectskill";
				else $response["data"]["hiddenVal2"] = "valid";
				
				for($i=0;$i<sizeof($institutetxt);$i++)
				{
					if($statusTeach[$i] == "0" && $statusProfession[$i]=="0" && $statushobby[$i]=="0")$response["data"]["statusteach".$i] = "statuserror";
					else $response["data"]["statusteach".$i] = "valid";
				}

				
				
//				if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="4" || $authUserNamespace->classtype=="3"))
//				{
//
//					if($companyStartyear == "" )$response["data"]["companystartdate"] = "onlyselectnull";
//					else $response["data"]["companystartdate"] = "valid";
//
//				}
				$response["data"]["companystartdate"] = "valid";
				if(!in_array('onlyselectnull',$response['data']) && !in_array('selectskill',$response['data']) && !in_array('yearerror',$response['data']) && !in_array('allfield',$response['data']) && !in_array('statuserror',$response['data']) && !in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
			}
			else {
			
				$lastupdatedate = date("Y-m-d H:i:s");
				if (isset($tutorid) && $tutorid!="")//--------update code
				{
					/*Start for institute */
					//echo "b".$authUserNamespace->avgexpva;
					$lenInstitute = sizeof($institutetxt);
					$insertremianInstitute = $lenInstitute-$authUserNamespace->avgexpvar;
					$prevsessionInstitute = $authUserNamespace->avgexpvar;
					$expercounter = -1;
					for($i=0;$i<=$prevsessionInstitute;$i++)
					{
						//echo "ouut";
						if(isset($authUserNamespace->notinserttest) && $authUserNamespace->notinserttest=="1"){
							if($institutetxt[$i]!="School/Organization/Self-employed" && $subjecttxt[$i]!="Skills taught or practiced" && $startyeardrp[$i]!="" && $endyeardrp[$i]!=""){
								$data = array("tutor_id"=>$tutorid,"tutor_expedu_type"=>'1',
			 								"tutor_expedu_kind"=>$institutetxt[$i],"tutor_expedu_place"=>"",
			   								"tutor_expedu_field"=>$subjecttxt[$i],
			   								"teach_flag"=>$statusTeach[$i],"profession_flag"=>$statusProfession[$i],"hobby_flag"=>$statushobby[$i],
			   								"tutor_expedu_start_year"=>$startyeardrp[$i],
			   								"tutor_expedu_end_year"=>$endyeardrp[$i]);
								$tutorexperienceObj->update($data,"tutor_id=$tutorid && tutor_expedu_type='1' && id='$tutorexperienceIDarray[$i]'");
								$expercounter++;
							}
						}else{
							if($institutetxt[$i]!="School/Organization/Self-employed" && $subjecttxt[$i]!="Skills taught or practiced" && $startyeardrp[$i]!="" && $endyeardrp[$i]!=""){
								$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'1',
											"tutor_expedu_kind"=>$institutetxt[$i],"tutor_expedu_place"=>"",
											"tutor_expedu_field"=>$subjecttxt[$i],
											"teach_flag"=>$statusTeach[$i],"profession_flag"=>$statusProfession[$i],"hobby_flag"=>$statushobby[$i],
											"comp_address"=>"","comp_contactno"=>"",
											"tutor_expedu_start_year"=>$startyeardrp[$i],"tutor_expedu_end_year"=>$endyeardrp[$i],"lastupdatedate"=>$lastupdatedate);
								$tutorexperienceObj->insert($data);
								$expercounter++;
								$authUserNamespace->notinserttest="1";
							}
						}
						
					}
					for($i=($prevsessionInstitute+1);$i<$lenInstitute;$i++)
					{
						//echo "in";
						if($institutetxt[$i]!="School/Organization/Self-employed" && $subjecttxt[$i]!="Skills taught or practiced" && $startyeardrp[$i]!="" && $endyeardrp[$i]!=""){
							$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'1',
										"tutor_expedu_kind"=>$institutetxt[$i],"tutor_expedu_place"=>"",
										"tutor_expedu_field"=>$subjecttxt[$i],
										"teach_flag"=>$statusTeach[$i],"profession_flag"=>$statusProfession[$i],"hobby_flag"=>$statushobby[$i],
										"comp_address"=>"","comp_contactno"=>"",
										"tutor_expedu_start_year"=>$startyeardrp[$i],"tutor_expedu_end_year"=>$endyeardrp[$i],"lastupdatedate"=>$lastupdatedate);
							$tutorexperienceObj->insert($data);
							$expercounter++;
						}
					}
				
					if($expercounter==-1){
						$expercounter=0;
					}
					//exit;
					/* End for institute */
					
					/*Start for degree */
					$lenDegree = sizeof($degreetxt);
					$insertremianDegree = $lenDegree-$authUserNamespace->avgqulvar;
					$prevsessionDegree = $authUserNamespace->avgqulvar;
					$degreecounter = -1;
					for($j=0;$j<=$prevsessionDegree;$j++)
					{
						if(isset($authUserNamespace->notinserttestdegree) && $authUserNamespace->notinserttestdegree=="1"){
							if($degreetxt[$j]!="Degree/Diploma/etc" && $universitytxt[$j]!="College/University/Institute" && $specialitytxt[$j]!="Field/Speciality" && $completion_yeardrp[$j]!=""){
								$data = array("tutor_id"=>$tutorid,"tutor_expedu_type"=>'2',
			   								"tutor_expedu_kind"=>"$degreetxt[$j]","tutor_expedu_place"=>$universitytxt[$j],
			   								"tutor_expedu_field"=>$specialitytxt[$j],
			   								"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
			   								"tutor_expedu_start_year"=>"",
			   								"tutor_expedu_end_year"=>"$completion_yeardrp[$j]");
								$tutorexperienceObj->update($data,"tutor_id=$tutorid && tutor_expedu_type='2' && id='$qulificationIDarray[$j]'");
								$degreecounter++;
							}
							
						}else{
							if($degreetxt[$j]!="Degree/Diploma/etc" && $universitytxt[$j]!="College/University/Institute" && $specialitytxt[$j]!="Field/Speciality" && $completion_yeardrp[$j]!=""){
								$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'2',
											"tutor_expedu_kind"=>"$degreetxt[$j]","tutor_expedu_place"=>$universitytxt[$j],
											"tutor_expedu_field"=>$specialitytxt[$j],
											"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
											"tutor_expedu_start_year"=>'',
											"comp_address"=>"","comp_contactno"=>"",
											"tutor_expedu_end_year"=>"$completion_yeardrp[$j]","lastupdatedate"=>$lastupdatedate);
								$tutorexperienceObj->insert($data);
								$degreecounter++;
								$authUserNamespace->notinserttestdegree="1";
							}
						}
					}
					
					
					for($j=($prevsessionDegree+1);$j<$lenDegree;$j++)
					{
						
						if($degreetxt[$j]!="Degree/Diploma/etc" && $universitytxt[$j]!="College/University/Institute" && $specialitytxt[$j]!="Field/Speciality" && $completion_yeardrp[$j]!=""){
							$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'2',
										"tutor_expedu_kind"=>"$degreetxt[$j]","tutor_expedu_place"=>$universitytxt[$j],
										"tutor_expedu_field"=>$specialitytxt[$j],
										"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
										"tutor_expedu_start_year"=>'',
										"comp_address"=>"","comp_contactno"=>"",
										"tutor_expedu_end_year"=>$completion_yeardrp[$j],"lastupdatedate"=>$lastupdatedate);
							$tutorexperienceObj->insert($data);
							$degreecounter++;
						}
					}
					if($degreecounter==-1){
						$degreecounter=0;
					}
					$collegecounter = -1;
					$companycounter = -1;
					/* End for degree */
					
//					if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="4" || $authUserNamespace->classtype=="3"))
//					{
					//---number of branch and compny name comment 10-7-12 ------
						$data1 = array("tutor_expedu_type"=>'4',"tutor_id"=>$tutorid,"company_start_year"=>$companyStartyear,
											"number_of_branch"=>"");
						$companylistObj->update($data1,"tutor_id=$tutorid");
					//}
					
					//--------------------------------------------------------------------------
					//-------------------update skills data-------------------
					$data = array("tutor_skill_id"=>$skillIds);
					$tutorCourseobj->update($data,"tutor_id=$authUserNamespace->tutorsessionid");
					//--------------------end of update skills data=------------
					/*Start session set for update div increment value */
					$authUserNamespace->avgexpvar = $expercounter;
					$authUserNamespace->avgqulvar = $degreecounter;
					$authUserNamespace->avgeduvar = $collegecounter;
					$authUserNamespace->avgcompvar = $companycounter;
					/*session set for update div increment value */
//					print $degreecounter;
//					exit;
					
				
				}
				else{//----------------------insert code
					$authUserNamespace->notinserttest= "0";
					$varExpcounter = -1;
					$kset="0";
					for($i=0;$i<sizeof($institutetxt);$i++)
					{
						if($institutetxt[$i]!="School/Organization/Self-employed" && $subjecttxt[$i]!="Skills taught or practiced" && $startyeardrp[$i]!="" && $endyeardrp[$i]!=""){
						$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'1',
										"tutor_expedu_kind"=>$institutetxt[$i],"tutor_expedu_place"=>"",
										"tutor_expedu_field"=>$subjecttxt[$i],
										"teach_flag"=>$statusTeach[$i],"profession_flag"=>$statusProfession[$i],"hobby_flag"=>$statushobby[$i],
										"comp_address"=>"","comp_contactno"=>"",
										"tutor_expedu_start_year"=>$startyeardrp[$i],"tutor_expedu_end_year"=>$endyeardrp[$i],"lastupdatedate"=>$lastupdatedate);
						$tutorexperienceObj->insert($data);
						$varExpcounter++;
						$kset = "1";
						$authUserNamespace->notinserttest = "1";
						}
						$flag1 = "1";
					}
					if($kset=="0"){
						$varExpcounter++;
					}
					$varDegreecounter = -1;
					$fset="0";
					for($j=0;$j<sizeof($degreetxt);$j++)
					{
						
						if($degreetxt[$j]!="Degree/Diploma/etc" && $universitytxt[$j]!="College/University/Institute" && $specialitytxt[$j]!="Field/Speciality" && $completion_yeardrp[$j]!=""){
							$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'2',
										"tutor_expedu_kind"=>"$degreetxt[$j]","tutor_expedu_place"=>$universitytxt[$j],
										"tutor_expedu_field"=>$specialitytxt[$j],
										"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
										"tutor_expedu_start_year"=>'',
										"comp_address"=>"","comp_contactno"=>"",
										"tutor_expedu_end_year"=>"$completion_yeardrp[$j]","lastupdatedate"=>$lastupdatedate);
							$tutorexperienceObj->insert($data);
							$fset = "1";
							$varDegreecounter++;
							
							$authUserNamespace->notinserttestdegree = "1";
						}
						$flag2 = "2";
					}
					if($fset=="0"){
						$varDegreecounter++;
					}
					$varCollegecounter = -1;
//					for($k=0;$k<sizeof($collegetxt);$k++)
//					{
//
//						if($collegetxt[$k]!="College/School" && $diplomatxt[$k]!="Degree/Diploma" && $fieldsofstudytxt[$k]!="Fields of Study" && $edbackcompletionyeardrp[$k]!=""){
//							$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'3',
//										"tutor_expedu_kind"=>"$diplomatxt[$k]",
//										"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
//										"tutor_expedu_place"=>$collegetxt[$k],
//										"tutor_expedu_field"=>$fieldsofstudytxt[$k],"tutor_expedu_start_year"=>'',
//										"comp_address"=>"","comp_contactno"=>"",
//										"tutor_expedu_end_year"=>"$edbackcompletionyeardrp[$k]");
//							$tutorexperienceObj->insert($data);
//							$varCollegecounter++;
//						}
//						$flag3 = "3";
//					}
					
					$varCompanycounter = -1;
//					if (isset($authUserNamespace->classtype) && $authUserNamespace->classtype=="4")
//					{
//						for($m=0;$m<sizeof($address);$m++)
//						{
//							if($address[$m]!="Address" && $contactNumber[$m]!="Contact Number"){
//							$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_expedu_type"=>'4',
//											"tutor_expedu_kind"=>"",
//											"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
//											"tutor_expedu_place"=>"",
//											"tutor_expedu_field"=>"","tutor_expedu_start_year"=>"",
//											"comp_address"=>$address[$m],"comp_contactno"=>$contactNumber[$m],
//											"tutor_expedu_end_year"=>"");
//							$tutorexperienceObj->insert($data);
//							$varCompanycounter++;
//							}
//							$flag3 = "4";
//						}
					
					
					//---number of branch and compny name comment 10-7-12 ------
					
					//if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="4" || $authUserNamespace->classtype=="3")){
						$data = array("tutor_expedu_type"=>'4',"tutor_id"=>$authUserNamespace->tutorsessionid,
										"company_start_year"=>$companyStartyear,"number_of_branch"=>'',"lastupdatedate"=>$lastupdatedate);
						$companylistObj->insert($data);
					//}
					//--------------------------------------------------------------
					
					//-----------------insert skills----------------------
					$data = array("tutor_skill_id"=>$skillIds,"tutor_id"=>$authUserNamespace->tutorsessionid);
					$tutorCourseobj->insert($data);
					//------------------end of skills insert---------
					if (isset($flag1) && $flag1="1" && isset($flag2) && $flag2="2")
					{
						$authUserNamespace->formname2 = "experiencepage";
					}
					$authUserNamespace->avgexpvar = $varExpcounter;
					$authUserNamespace->avgqulvar = $varDegreecounter;
					$authUserNamespace->avgeduvar = $varCollegecounter;
					$authUserNamespace->avgcompvar = $varCompanycounter;
				}
					//-----------------qualification count------------
					$tutorDataquali = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
													->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
													->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='2'"));
					if(isset($tutorDataquali) && sizeof($tutorDataquali)>0)
					{	
						$totalQualicount = sizeof($tutorDataquali);
						$data2 = array("qualification_count"=>$totalQualicount);
						$tutorexperienceObj->update($data2,"tutor_id='$tutorid'");
					}
					//-----------------end---------------------
					$this->_redirect('/teacher-signup-course-details');
				
			}
			
		}

	}
public function coursecalAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout('layout2');
		//echo "sd".$authUserNamespace->avgaddvar;exit;
		
}

public function addbatchcalenderAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			//if(!isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid==""){$this->_redirect('/');}
			$selectId = '3';
			$this->view->selectid = $selectId;
			$this->_helper->layout()->disableLayout();

			$batchObj= new Skillzot_Model_DbTable_Batch();

			$tutor_id =$authUserNamespace->tutorsessionid;
			/*$courseId = $this->_request->getParam("courseid");
			$corseId = $this->_request->getParam("corseid");*/
			//echo "course".$corseId;exit;
			
			$adressObj = new Skillzot_Model_DbTable_Address();
			$batchdayObj = new Skillzot_Model_DbTable_Batchday();
			$batchdayResult=$batchdayObj->fetchAll();
			$this->view->batchdayResult = $batchdayResult;

			//$batchid = $this->_request->getParam("batchid");
			$defaultcorseId = $this->_request->getParam("defaultcorseid");


			$batchResult_row=$batchObj->fetchAll($batchObj->select()
							->from(array('a'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
							->where("a.course_id='$defaultcorseId'"));
			

			//echo $defaultcorseId;exit;

			if(isset($defaultcorseId) && $defaultcorseId!="")
			{
				$this->view->defaultcorseId = $defaultcorseId;
				$batchResult=$batchObj->fetchAll();
				$this->view->batchResult = $batchResult;
				$addressbarResult = $adressObj->fetchAll($adressObj->select()
								->setIntegrityCheck(false)
								->distinct()
							   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('a.locality as locality','a.address as address','a.pincode as pincode','a.landmark as landmark'))
							   ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'a.locality=b.address_id',array('b.address_value as address_value'))
							   ->where("b.address_type_id='2' && a.locality!='' and a.course_id='$defaultcorseId'"));
				$this->view->addressbarResult = $addressbarResult;
			}


			
			$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
			$suburbsData = $localitysuburbsObj->fetchAll($localitysuburbsObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."locality_suburbs"))
															  ->where("s.is_active='1'")
															  ->order(array("s.order asc")));
			if (isset($suburbsData) && sizeof($suburbsData)>0)
			{
				$this->view->suburbsdata = $suburbsData;
			}

			
			$addressResult = $adressObj->fetchAll($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"))
									   ->where("a.address_type_id='2' && address_id!='0'"));
			if (isset($addressResult) && sizeof($addressResult) > 0) 
				$this->view->address = $addressResult;
			$cityResult = $adressObj->fetchAll($adressObj->select()
									->from(array('a'=>DATABASE_PREFIX."master_address"))
									->where("a.address_type_id='1' && a.address_id!='0'"));
			if (isset($cityResult) && sizeof($cityResult) > 0) 
				$this->view->city = $cityResult;

			
				
		if($this->_request->isPost()){
			$hiddenVal6 = $this->_request->getParam("hiddenVal6");
			if($hiddenVal6=="1")
			{
				$locality_stu = $this->_request->getParam("hiddenVal5");
				$travel_radius = $this->_request->getParam("travel_radius");
			}
			if($hiddenVal6=="2")
			{
				$address = $this->_request->getParam("address");
				//echo $address;exit;
				$city = $this->_request->getParam("city");
				//echo $city[0];exit;
				$locality_id = $this->_request->getParam("locality");
				//echo $locality_id[0];exit;
				$pincode = $this->_request->getParam("pincode");
				$landmark = $this->_request->getParam("landmark");
							
				$addressResultDisplay = $adressObj->fetchRow($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"),array('a.address_value as address_value'))
									   ->where("a.address_id='$locality_id[0]'"));	
			}
			//echo "student".$locality_stu;exit;
			$no_of_wks = $this->_request->getParam("no_of_wks");
			$no_of_days_per_wks = $this->_request->getParam("no_of_days_per_wks");
			$no_of_hrs_per_day = $this->_request->getParam("no_of_hrs_per_day");
			$from_timing = $this->_request->getParam("from_timing");
			$to_timing = $this->_request->getParam("to_timing");
			$batchday = $this->_request->getParam("hiddenVal2");
			$batch_size = $this->_request->getParam("batch_size");
			$batch_date1 = $this->_request->getParam("batch_date");
			$batchdatecheckbox = $this->_request->getParam("batchdatecheckbox");
			//echo "batch".$batch_date;exit;
			$tutor_batch_name = $this->_request->getParam("tutor_batch_name");
			if($batchdatecheckbox=="No fixed start date")
			{
				$batch_date="No fixed start date";
			}else
			{
				$batch_date=$batch_date1;
			}
			$tutor_loc=$hiddenVal6;
			
			if (isset($tutor_loc) && $tutor_loc=="1"){
				
			$tutor_loc_display="At Student's home : ";}
			
			elseif (isset($tutor_loc) && $tutor_loc=="2"){
						   		
			$tutor_loc_display=$addressResultDisplay->address_value." : ";}
			
			/*elseif (isset($tutor_loc) && $tutor_loc=="1,2"){	
			
			$tutor_loc_display="At Student's home and ".$addressResultDisplay->address_value." : ";}*/
			
			if(isset($from_timing) && $from_timing != ""){  $from_timing_display=$from_timing." to "; }
			if (isset($to_timing) && $to_timing!=""){$to_timing_display=$to_timing.", ";}
				
			$daytable = new Skillzot_Model_DbTable_Batchday();
			$days=explode(',',$batchday);
			sort($days);
		    $day_row1='';
			if(count($days))
			{
						for ($start=0; $start < count($days); $start++) {
											$day_row=$daytable->fetchRow($daytable->select()
																->from(array('c'=>DATABASE_PREFIX."master_tutor_batch_day"),array('day_name'))
																->where("day_id='$days[$start]'"));
											$day_row1.=$day_row->day_name.'-';	
						}
			}
			$day_list1=ltrim($day_row1,'-');
			$day_list=rtrim($day_list1,'-');
			if(isset($batchday) && $batchday != ""){
						if(count($days)=="8"){ $batchday_display="Everyday";}
						else { $batchday_display=$day_list; }
			}				
			$batch_summary_display=$tutor_loc_display.$from_timing_display.$to_timing_display.$batchday_display;			
			
			$fromtime1=explode('am',$from_timing);
			$totime1=explode('am',$to_timing);
			$fromtime2=explode('pm',$from_timing);
			$totime2=explode('pm',$to_timing);
			//print_r($fromtime1);
			//echo "form1".sizeof($fromtime1);exit;
			//echo "form".$fromtime1[0]."and to".$totime1[0];exit;
			if($fromtime1['0']!=$from_timing && $totime1['0']!=$to_timing)
			{

				$from=$fromtime1['0'];
				$from_type = "am";
				$to=$totime1['0'];
				$to_type = "am";
				//echo "1st";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			elseif($fromtime1['0']==$from_timing && $totime1['0']==$to_timing)
			{
				$from=$fromtime2['0'];
				$from_type = "pm";
				$to=$totime2['0'];
				$to_type = "pm";
				//echo "2nd";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			elseif($fromtime1['0']!=$from_timing && $totime1['0']==$to_timing)
			{
				$from=$fromtime1['0'];
				$from_type ="am";
				$to=$totime2['0'];
				$to_type = "pm";
				//echo "3rd";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			elseif($fromtime1['0']==$from_timing && $totime1['0']!=$to_timing)
			{
				$from=$fromtime2['0'];
				$from_type = "pm";
				$to=$totime1['0'];
				$to_type = "am";
				//echo "4th";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
				if($hiddenVal6 == "")
				{
					$response["data"]["hiddenVal6"] = "selectatleast";
					$response["data"]["hiddenVal5"] = "valid";
					$response["data"]["address0"] = "valid";
					$response["data"]["travel_radius"] = "valid";
				}
				if($hiddenVal6 == "1" && $hiddenVal6!="2")
				{

					$response["data"]["hiddenVal6"] = "valid";
					$response["data"]["address0"] = "valid";	
					if($locality_stu == "")
					{
						$response["data"]["hiddenVal5"] = "selectatleast";
						$response["data"]["travel_radius"] = "valid";
					}
					elseif($travel_radius == "Travel radius" || $travel_radius == "")
					{
						$response["data"]["travel_radius"] = "tavelradiusnull";
						$response["data"]["hiddenVal5"] = "valid";
					}
					
					else{
					$response["data"]["hiddenVal5"] = "valid";
					$response["data"]["travel_radius"] = "valid";

					}
				}
				
				if($hiddenVal6 != "1" && $hiddenVal6=="2")
				{
					
					$response["data"]["hiddenVal6"] = "valid";
					$response["data"]["hiddenVal5"] = "valid";
					$response["data"]["travel_radius"] = "valid";
					if($address == "" || $address == "Address" || $city[0]=="" || $locality_id[0]=="" || $pincode=="Pincode" || $pincode=="0" || !is_numeric($pincode) || $landmark =="" || $landmark =="landmark")
					{
						$response["data"]["address0"] = "allfield";		
					}
					else{
						$response["data"]["address0"] = "valid";	
					}
				}
			/*	if($studentlocation != "" && $teacherlocation!="")
				{
					$response["data"]["hiddenVal6"] = "selectatleast";
					$response["data"]["travel_radius"] = "valid";
					if($locality_stu == "")
					{
						$response["data"]["hiddenVal5"] = "selectatleast";
					}
					elseif($travel_radius == "Travel radius" || $travel_radius == "")
					{
						$response["data"]["travel_radius"] = "tavelradiusnull";
						$response["data"]["hiddenVal5"] = "valid";
					}
					elseif($address == "" || $address == "Address" || $city[0]=="" || $locality_id[0]=="" || $pincode=="Pincode" || $pincode=="0" || !is_numeric($pincode) || $landmark =="" || $landmark =="landmark")
					{
						$response["data"]["address0"] = "allfield";		
					}
					else{
						$response["data"]["hiddenVal5"] = "valid";
						$response["data"]["address0"] = "valid";
					}
				}*/
				if($no_of_wks == "No. of weeks" || $no_of_days_per_wks == "	No. of days/wk" || $no_of_hrs_per_day == "No. of hours/day")
				{
					$response["data"]["no_of_wks"] = "durationnull";
				}
				else
				{
					$response["data"]["no_of_wks"] = "valid";
				}
				if($batch_size == "Batch size" || $batch_size == "")
				{
					$response["data"]["batch_size"] = "null";
				}
				elseif((!is_numeric($batch_size) || $batch_size<="0"))
				{
					$response["data"]["batch_size"] = "numberinvalid";
				}
				else
				{
					$response["data"]["batch_size"] = "valid";
				}
				if($batch_date == "Batch date" || $batch_date == "")
				{
					$response["data"]["batch_date"] = "null";
				}
				else
				{
					$response["data"]["batch_date"] = "valid";
				}
				if($tutor_batch_name=="")
				{
					$response["data"]["tutor_batch_name"] = "null";
				}
				else
				{
					$response["data"]["tutor_batch_name"] = "valid";
				}

				if($to_timing == "" || $from_timing == "" || $batchday == "")
				{
					$response["data"]["from_timing"] = "allfield";
				}
				else
				{
					$response["data"]["from_timing"] = "valid";
				}

				if($to_timing != "" && $from_timing != "" && $batchday != "")
				{
					$response["data"]["hiddenVal2"] = "valid";
					$response["data"]["from_timing"] = "valid";
					//$response["data"]["to_timing"] = "valid";
					//echo "from_type=".$from_type."and to=".$to_type;exit;
					if($from_type == $to_type)
					{
						//echo "1st";exit;
						$timeFirst  = strtotime($from);
						$timeSecond = strtotime($to);
						if(($from >= "12:00" && $from <= "12:59"))
						{
							if($timeSecond < $timeFirst)
							{
								//echo "string";exit;
								$response["data"]["to_timing"] = "valid";
							}
							
							
						}
						elseif($timeSecond > $timeFirst)
							{
								//echo "1st";exit;
								$response["data"]["to_timing"] = "valid";
							}
						else
							{
								$response["data"]["to_timing"] = "null";
							}
					}
					if($from_type != $to_type)
					{
						//echo "2nd";exit;
						$timeFirst  = strtotime($from);
						$timeSecond = strtotime($to);
						if(($timeSecond <= $timeFirst) && $to_type="pm" && $from_type="am")
							{
								$response["data"]["to_timing"] = "valid";
							}
						elseif(($timeSecond >= $timeFirst) && $to_type="am" && $from_type="pm")
							{
								$response["data"]["to_timing"] = "valid";
							}
						else
							{
								$response["data"]["to_timing"] = "null";
							}

					}
					
					
				}
				if(!in_array('selectatleast',$response['data']) 
					&& !in_array('durationnull',$response['data']) 
					&&!in_array('tavelradiusnull',$response['data']) 
					&& !in_array('allfield',$response['data']) 
					&& !in_array('numberinvalid',$response['data'])
					&& !in_array('null',$response['data'])  
					&&  !in_array('invalid',$response['data']))
					$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);
			}
			else {
			
			$curr_date=date("Y-m-d h:i:s");	
			if(isset($batchResult_row) && sizeof($batchResult_row)>0){
					$data = array("tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date);
					$batchObj->update($data,"course_id='$defaultcorseId'");
					echo "<script>parent.Mediabox.close();</script>";
					//echo "<script>window.parent.location='". BASEPATH ."/teacher-signup-course-details#registration_form_2';</script>";
				}else{
					$data = array("tutor_id"=>$tutor_id,"course_id"=>$defaultcorseId,"tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date
								);
					$batchObj->insert($data);
					//$batch_msg="Batch details uploaded successfully...!";
					//$this->view->batch_msg = $batch_msg;
					echo "<script>parent.Mediabox.close();</script>";
					//echo "<script>window.parent.location='". BASEPATH ."/teacher-signup-course-details';</script>";
				}
				
				
			}
		}
	}

	public function shortlistAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$batchid = $this->_request->getParam("batchid");
		//echo "batch".$batchid;
		$studentid = $this->_request->getParam("student");
		//echo "studentid".$studentid;
		$tutorid = $this->_request->getParam("tutor");
		//echo "tutorid".$tutorid;exit;
		$batch_shortlistObj= new Skillzot_Model_DbTable_shortlist();
		if($batchid!="" && $studentid!="" && $tutorid =="")
		{
			$selectdata=$batch_shortlistObj->fetchRow($batch_shortlistObj->select()
						->from(array('c'=>DATABASE_PREFIX."shortlist"),array('*'))
						->where("c.batch_id ='$batchid' && c.student_id='$studentid'"));
			$data = array("batch_id"=>$batchid,"student_id"=>$studentid,"tutor_id"=>"0");
			if(isset($selectdata) && $selectdata!="")
			{
				$batch_shortlistObj->update($data,"batch_id ='$batchid' && student_id='$studentid'");
			}else{
				$batch_shortlistObj->insert($data);
			}	
		}elseif($batchid!="" && $studentid=="" && $tutorid ==""){
			$student_sessionid = $authUserNamespace->studentid;
			$tutor_sessionid=$authUserNamespace->maintutorid;

			if(isset($student_sessionid))
			{
				$selectdata=$batch_shortlistObj->fetchRow($batch_shortlistObj->select()
						->from(array('c'=>DATABASE_PREFIX."shortlist"),array('*'))
						->where("c.batch_id ='$batchid' && c.student_id='$student_sessionid'"));
				$data = array("batch_id"=>$batchid,"student_id"=>$student_sessionid,"tutor_id"=>"0");
				if(isset($selectdata) && $selectdata!="")
				{
					$batch_shortlistObj->update($data,"batch_id ='$batchid' && student_id='$student_sessionid'");
				}else{
					$batch_shortlistObj->insert($data);
				}
			}else{
				$selectdata=$batch_shortlistObj->fetchRow($batch_shortlistObj->select()
						->from(array('c'=>DATABASE_PREFIX."shortlist"),array('*'))
						->where("c.batch_id ='$batchid' && c.tutor_id='$tutor_sessionid'"));
				$data = array("batch_id"=>$batchid,"student_id"=>"0","tutor_id"=>$tutor_sessionid);
				if(isset($selectdata) && $selectdata!="")
				{
					$batch_shortlistObj->update($data,"batch_id ='$batchid' && tutor_id='$tutor_sessionid'");
				}else{
					$batch_shortlistObj->insert($data);
				}
			}
			echo "<script>window.parent.location.reload();</script>";
		}else{
			$selectdata=$batch_shortlistObj->fetchRow($batch_shortlistObj->select()
						->from(array('c'=>DATABASE_PREFIX."shortlist"),array('*'))
						->where("c.batch_id ='$batchid' && c.tutor_id='$tutorid'"));
			$data = array("batch_id"=>$batchid,"student_id"=>"0","tutor_id"=>$tutorid);
			if(isset($selectdata) && $selectdata!="")
			{
				$batch_shortlistObj->update($data,"c.batch_id ='$batchid' && c.tutor_id='$tutorid'");
			}else{
				$batch_shortlistObj->insert($data);
			}
		}exit;

	}
		public function batchsavedAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("searchinnerpage_savedbatch");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage_savedbatch");
		}
		$batchid = $this->_request->getParam("batchid");
		$this->view->batchid = $batchid;
		//echo "batch".$batchid;
		$studentid = $this->_request->getParam("student");
		$this->view->studentid = $studentid;
		//echo "studentid".$studentid;
		$tutorid = $this->_request->getParam("tutor");
		$this->view->tutorid = $tutorid;
		//echo "tutorid".$tutorid;exit;
		$batch_shortlistObj= new Skillzot_Model_DbTable_shortlist();
		$batchObj= new Skillzot_Model_DbTable_Batch();
		if($studentid!="" && $tutorid =="")
		{
			$selectdata=$batch_shortlistObj->fetchRow($batch_shortlistObj->select()
						->from(array('c'=>DATABASE_PREFIX."shortlist"),array('GROUP_CONCAT(c.batch_id) as batch_id'))
						->where("c.student_id='$studentid'"));
			if(isset($selectdata->batch_id) && $selectdata->batch_id !="")
			{
			$shortlist_data=$batchObj->fetchAll($batchObj->select()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
						->where("c.id in ($selectdata->batch_id)"));
			$this->view->shortlist_data = $shortlist_data;
			}else{
			$shortlist_data=$batchObj->fetchAll($batchObj->select()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
						->where("c.id in (0)"));
			$this->view->shortlist_data = $shortlist_data;
			}
		}else{
			$selectdata=$batch_shortlistObj->fetchRow($batch_shortlistObj->select()
						->from(array('c'=>DATABASE_PREFIX."shortlist"),array('GROUP_CONCAT(c.batch_id) as batch_id'))
						->where("c.tutor_id='$tutorid'"));
			if(isset($selectdata->batch_id) && $selectdata->batch_id !="")
			{
			$shortlist_data=$batchObj->fetchAll($batchObj->select()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
						->where("c.id in ($selectdata->batch_id)"));
			$this->view->shortlist_data = $shortlist_data;
			}else{
			$shortlist_data=$batchObj->fetchAll($batchObj->select()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
						->where("c.id in (0)"));
			$this->view->shortlist_data = $shortlist_data;
			}
		}

	}
		public function deletebatchAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$batch_shortlistObj = new Skillzot_Model_DbTable_shortlist();
			$batchdetailObj = new Skillzot_Model_DbTable_Batch();
			$batchid = $this->_request->getParam("batchid");
			$this->view->batchid = $batchid;
			$batchviewResult=$batchdetailObj->fetchRow($batchdetailObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.id ='$batchid'"));
			$this->view->batchviewResult = $batchviewResult;
			if($this->_request->isPost()){

				$status_value = $this->_request->getParam("delete");

				if($this->_request->isXmlHttpRequest()){

					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response = array();


					if($status_value == "1")$response["data"]["statusflag"] = "valid";
					else $response["data"]["statusflag"] = "valid";

					if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";

					echo json_encode($response);

				}else{
					if (isset($batchid) && $batchid!="")
					{
					$batch_shortlistObj->delete("batch_id='$batchid'");
					}
					echo "<script>window.parent.location.reload();</script>";
					//	echo "<script>parent.Mediabox.close();</script>";

				}
			}

		}
		public function viewbatchcalenderAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			//if(!isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid==""){$this->_redirect('/');}
			$selectId = '3';
			$this->view->selectid = $selectId;
			$this->_helper->layout()->disableLayout();
			$batchid = $this->_request->getParam("batchid");

			$batchObj= new Skillzot_Model_DbTable_Batch();
			$batchviewResult=$batchObj->fetchRow($batchObj->select()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
						->where("c.id ='$batchid'"));

			if (isset($batchviewResult) && sizeof($batchviewResult) > 0) 
			$this->view->batchviewResult = $batchviewResult;

			$adressObj = new Skillzot_Model_DbTable_Address();
			$addressResult = $adressObj->fetchRow($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"))
									   ->where("a.address_type_id='2' && address_id='$batchviewResult->locality'"));
			if (isset($addressResult) && sizeof($addressResult) > 0) 
				$this->view->address = $addressResult;

			$cityResult = $adressObj->fetchRow($adressObj->select()
										->from(array('a'=>DATABASE_PREFIX."master_address"))
										->where("a.address_type_id='1' && a.address_id='$batchviewResult->city'"));
			if (isset($cityResult) && sizeof($cityResult) > 0) 
				$this->view->city = $cityResult;

			$tutor_id =$batchviewResult['tutor_id'];

			//echo "tutorid".$tutor_id;exit;

			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$courseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"))
										->where("c.tutor_id='$tutor_id'"));
			
										
			if (isset($courseData) && sizeof($courseData)>0)
			{
				$this->view->coursedata = $courseData ;
				$i = 0;
				foreach ($courseData as $course){
				 	if ((isset($course->tutor_skill_id) && $course->tutor_skill_id!="")||(isset($course->tutor_course_name) && $course->tutor_course_name!="")||(isset($course->id) && $course->id!="")){
						$tutorskillIds[$i] = $course->tutor_skill_id;
						$courseName[$i] = $course->tutor_course_name;
						$tutorIds[$i] = $course->id;
						$tutorlesionloc[$i] =$course->tutor_lesson_location;
						$i++;
				 	}else{
					
					$tutorskillIds[] = '';
						$courseName[] = '';
						$tutorIds[] = '';
					}
				}
			//----temperary for one id--------------------
			
			//print_r($tutorIds);exit;
			$tMainId = $tutorIds[0];
			$coursesubData = $tutorCourseobj->fetchRow("id ='$tMainId'");

			$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
			$masteraddressobj = new Skillzot_Model_DbTable_Address();
			$lessonlocationResult = $lessonlocationObj->fetchRow("location_id ='$coursesubData->tutor_lesson_location'");
			if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
			{
				if ($lessonlocationResult->location_id=='3')
				{
					$this->view->tutorlessonlocation = "Teacher's location & open to traveling";
				}else{
					$this->view->tutorlessonlocation = $lessonlocationResult->location_name;
				}
				
			}
			

		}
	}
	public function coursedetailAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//echo "sd".$authUserNamespace->avgaddvar;exit;
		if((!isset($authUserNamespace->tutorsessionid) &&  $authUserNamespace->tutorsessionid == NULL) && (!isset($authUserNamespace->formname2) &&  $authUserNamespace->formname2 == NULL))
		{
			$this->_redirect("/tutor/register");
		}
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$this->_helper->layout()->setLayout('layout2');
		$tutorid = $authUserNamespace->tutorsessionid;
		//echo "tutor_id".$tutorid."--";
		$checkCousedatainserted = $tutorCourseobj->fetchRow("tutor_id ='$tutorid'");
		//print_r($checkCousedatainserted->id);exit;
		if (isset($checkCousedatainserted) && sizeof($checkCousedatainserted)>0)
		{
			$authUserNamespace->tutorcoursesessionid = $checkCousedatainserted->id;
		}
		//echo $checkCousedatainserted->id."-------";
	//echo $authUserNamespace->tutorcoursesessionid;exit;
		$tutorgradeObj = new Skillzot_Model_DbTable_Tutorgradesteach();
		$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
		$tutorgradeResult = $tutorgradeObj->fetchAll();
		if (isset($tutorgradeResult) && sizeof($tutorgradeResult) > 0) $this->view->tutorgrade = $tutorgradeResult;
							
		$languageObj = new Skillzot_Model_DbTable_Language();
		$languagueResult = $languageObj->fetchAll();
		 if (isset($languagueResult) && sizeof($languagueResult) > 0) $this->view->language = $languagueResult;
		
		$profileTypeObj = new Skillzot_Model_DbTable_Studentprofiletype();
		$profileTypeResult = $profileTypeObj->fetchAll();
		if (isset($profileTypeResult) && sizeof($profileTypeResult) > 0) $this->view->profiletype = $profileTypeResult;
		//print_r($profileTypeResult);exit;
		
		$feestypeObj = new Skillzot_Model_DbTable_Tutorfeetype();
		$feestypeResult = $feestypeObj->fetchAll();
		if (isset($feestypeResult) && sizeof($feestypeResult) > 0) $this->view->feestype = $feestypeResult;
							
		$paymentcycleObj = new Skillzot_Model_DbTable_Tutorpaycycle();
		$paymentcycleResult = $paymentcycleObj->fetchAll();
		if (isset($paymentcycleResult) && sizeof($paymentcycleResult) > 0) $this->view->paycycle = $paymentcycleResult;
							
		$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
		$lessonlocationResult = $lessonlocationObj->fetchAll();
		if (isset($lessonlocationResult) && sizeof($lessonlocationResult) > 0) $this->view->lessonlocation = $lessonlocationResult;
		
		$timingsObj = new Skillzot_Model_DbTable_Timings();
		$timingsResult = $timingsObj->fetchAll();
		if (isset($timingsResult) && sizeof($timingsResult) > 0) $this->view->timings= $timingsResult;
		
		$adressObj = new Skillzot_Model_DbTable_Address();
		$addressResult = $adressObj->fetchAll($adressObj->select()
								   ->from(array('a'=>DATABASE_PREFIX."master_address"))
								   ->where("a.address_type_id='2' && address_id!='0'"));
		if (isset($addressResult) && sizeof($addressResult) > 0) $this->view->address = $addressResult;
		$cityResult = $adressObj->fetchAll($adressObj->select()
								->from(array('a'=>DATABASE_PREFIX."master_address"))
								->where("a.address_type_id='1' && a.address_id!='0'"));
		if (isset($cityResult) && sizeof($cityResult) > 0) $this->view->city = $cityResult;
		
		
		$batchsizeObj = new Skillzot_Model_DbTable_Tutorbatchsize();
		//$batchsizeResult = $batchsizeObj->fetchAll();
		if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="4" || $authUserNamespace->classtype=="3"))
		{
			$batchsizeResult = $batchsizeObj->fetchAll();
		}else{
			$batchsizeResult = $batchsizeObj->fetchAll($batchsizeObj->select()
												  ->from(array('s'=>DATABASE_PREFIX.'master_tutor_batch_size'))
												  ->limit(3,0));
		}
		if (isset($batchsizeResult) && sizeof($batchsizeResult) > 0)
			{
				$this->view->batchsize = $batchsizeResult;
			}
		
		
		$skillobj1 = new Skillzot_Model_DbTable_Skills();
		$skillRows = $skillobj1->fetchAll($skillobj1->select()
											  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
											  ->where("is_enabled = 1 && parent_skill_id = 0 && skill_id!=0")
											  ->order(array("order_id asc")));
		if (isset($skillRows) && sizeof($skillRows) > 0)
			{
				$this->view->mainSkill = $skillRows;
			}
			//echo $authUserNamespace->tutorcoursesessionid;exit;
		if(isset($authUserNamespace->tutorcoursesessionid) && $authUserNamespace->tutorcoursesessionid!="")
		{
			$tutorcourseData = $tutorCourseobj->fetchRow("id ='$authUserNamespace->tutorcoursesessionid'");
			if (isset($tutorcourseData) && sizeof($tutorcourseData) > 0)
			{
				$this->view->tutorcoursedata = $tutorcourseData;
			}
			//-----------------------------display of branch listings-----------------------------------
//			if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="4" ||$authUserNamespace->classtype=="3"))
//			{
				
				$companyaddressData = $branchdetailObj->fetchAll($branchdetailObj->select()
													->from(array('c'=>DATABASE_PREFIX."branch_details"))
													->where("c.tutor_id='$tutorid'"));
				if (isset($companyaddressData) && sizeof($companyaddressData)>0)
				{
					$this->view->companyaddressdata = $companyaddressData;
				}
				foreach ($companyaddressData as $addressID)
				{
					$addressIDarray[] =  $addressID->id;
				}
				//print_r($addressIDarray);exit;
			//}
			//-----------------------------end of display of branch listings-----------------------------------
		}
		if($this->_request->isPost()){
				
			$avgaddvar = $this->_request->getParam("avgadd");
			$skillIds = $this->_request->getParam("hiddenVal2");
			$gradeTeach = $this->_request->getParam("hiddenVal3");
			$courseName = $this->_request->getParam("coursename");
			$aboutCourse = $this->_request->getParam("OverviewDescription");
			$studentProfile = $this->_request->getParam("hiddenVal5");
			$studentComments =  $this->_request->getParam("student_comment");
			if ($studentComments=="Suitable for...")$studentComments="";
			$ageGroup = $this->_request->getParam("agegroup");
			$age = explode(" ",$ageGroup);
			$min1="0";
			$max1="99";	
			$flag="0";
			for ($start=0; $start < count($age); $start++) {
					if(($age[$start]=="above" || $age[$start]=="onwards" || $age[$start]=="+" || $age[$start]=="more" || $age[$start]=="greater" || $age[$start]=="above.") && $flag=="0")
						{
							for ($start1=0; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
										$min1=$age[$start1];
										$max1="99";
										$flag="1";
								}
							}
						}	
			}
			for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="below" || $age[$start]=="less" || $age[$start]=="below.") && $flag=="0")
						{
							for ($start1=0; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
										$min1="0";
										$max1=$age[$start1];
										$flag="1";
								}
							}
						}			
			}
			for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="to" || $age[$start]=="-" || $age[$start]=="TO") && $flag=="0")
						{
							$end=$start;
							for ($start1=0; $start1 < $end; $start1++)
							{
								if(is_numeric($age[$start1]))
								{
									$min1=$age[$start1];
									$flag="1";
								}
							}
							for ($start1=$end; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
									$max1=$age[$start1];
									$flag="1";
								}
							}
						}			
			}
			for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="any" || $age[$start]=="all" || $age[$start]=="anyone") && $flag=="0")
						{
							$min1="0";
							$max1="99";		
							$flag="1";
						}			
			}

		$age2 = explode("+",$ageGroup);               //33+
		if(is_numeric($age2[0]) && $flag=="0")
		{
				$min1=$age2[0];
				$max1="99";
				$flag="1";
		}
		$age2 = explode("-",$ageGroup);                //22-66 
		if(is_numeric($age2[0]) && is_numeric($age2[1]) && $flag=="0")
		{
				$min1=$age2[0];
				$max1=$age2[1];
				$flag="1";
		}
		if(is_numeric($age2[0]) && !is_numeric($age2[1]) && $flag=="0")      //22-66 years
		{
				$min1=$age2[0];
				$agespace = explode(" ",$age2[1]);
				if(is_numeric($agespace[0]))
				{ $max1=$agespace[0];} 
				$flag="1";
		} 
	
			$format = $this->_request->getParam("format");
			
			$courseCertis = $this->_request->getParam("coursecertis");
			$certiGranted = $this->_request->getParam("certisgranted");
			$numberofWeeks = $this->_request->getParam("no_of_wks");
			$numberofDays = $this->_request->getParam("no_of_days_per_wks");
			$numberofHors = $this->_request->getParam("no_of_hrs_per_day");
			
			$daysofWeek = $this->_request->getParam("days_of_weeks");
			//$timings = $this->_request->getParam("timings");
			$timeFrom = $this->_request->getParam("time_from");
			$timeTo = $this->_request->getParam("time_to");
			$batchShedule = $this->_request->getParam("batch_schedule");
			$batchSize = $this->_request->getParam("batch_size");
			$lessionLocation = $this->_request->getParam("LessionLocationhiddenvalue");
			$travelRadius = $this->_request->getParam("travel_radius");
			//if($travelRadius == "Locations where you travel to teach"){$travelRadius="";}
			$language = $this->_request->getParam("hiddenVal4");
			
			$payfeetype = $this->_request->getParam("feestype");
			$paymentcycle = $this->_request->getParam("paymentcycle");
			$canPolicy = $this->_request->getParam("cancel_policy");
			$image = $this->_request->getParam("imgfile");
			$imagecheck = $this->_request->getParam("imgch");
			$rate =  $this->_request->getParam("rate");
			$rateComments =  $this->_request->getParam("rate_comment");
			
			$address = $this->_request->getParam("address");
//			print_r($address);exit;
			$city = $this->_request->getParam("city");
			$locality = $this->_request->getParam("locality");
//			print_r($locality);exit;
			$pincode = $this->_request->getParam("pincode");
			$landmark = $this->_request->getParam("landmark");
			$numberofBranch = $this->_request->getParam("number_of_branches");
			if (!isset($numberofBranch) && $numberofBranch=="")
			{
				$numberofBranch="";
			}
			
			//$b_Timings_id = $this->_request->getParam("batch_type_id");
			$b_Tutor_status = $this->_request->getParam("tutor_batch_status");
			
			
			//echo "ss";exit;
			if($this->_request->isXmlHttpRequest()){
				
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
					if($payfeetype == "" || $rate == "Fees or fee range" )$response["data"]["rate"] = "ratenull";
					else $response["data"]["rate"] = "valid";
					
					if($paymentcycle == "select")$response["data"]["paymentcycle"] = "paymentnull";
					else $response["data"]["paymentcycle"] = "valid";
				
//					if($rate == "Fees or fee range")$response["data"]["rate"] = "ratenull";
//					elseif(!is_numeric($rate) || $rate=="0")$response["data"]["rate"] = "invalid";
//					else $response["data"]["rate"] = "valid";

					/*for($i=0;$i<sizeof($address);$i++)
					{
						if($address[$i] == "Address" || $city[$i]=="" || $locality[$i]=="" )$response["data"]["address".$i] = "allfield";
						else if ($pincode[$i]!="Pincode" && $pincode[$i]!="0" && !is_numeric($pincode[$i]))$response["data"]["address".$i] = "pincodeinvalid";
						else $response["data"]["address".$i] = "valid";
						
					}*/
					
					if (isset($lessionLocation) && ($lessionLocation=="1" || $lessionLocation=="3"))
					{
						if($travelRadius == "Locations where you travel to teach")$response["data"]["travel_radius"] = "tavelradiusnull";
						else $response["data"]["travel_radius"] = "valid";
					}
					
					if($language == "")$response["data"]["hiddenVal4"] = "selectlanguage";
					else $response["data"]["hiddenVal4"] = "valid";
					
					if($batchShedule == "Start date" )$response["data"]["batch_schedule"] = "shedulenull";
					else $response["data"]["batch_schedule"] = "valid";
					
//					if($daysofWeek == "Days of the weeks" || $timeFrom=="" || $timeTo=="")$response["data"]["days_of_weeks"] = "daysofweeknull";
//					elseif(is_numeric($daysofWeek))$response["data"]["days_of_weeks"] = "invalid";
//					else $response["data"]["days_of_weeks"] = "valid";

					if($numberofWeeks == "No. of weeks" || $numberofDays == "No. of days/wk" || $numberofHors == "No. of hours/day")$response["data"]["no_of_wks"] = "durationnull";
//				elseif((!is_numeric($numberofWeeks) || $numberofWeeks=="0")|| (!is_numeric($numberofDays) || $numberofDays=="0") ||(!is_numeric($numberofHors) || $numberofHors=="0") )$response["data"]["no_of_wks"] = "durationnumeric";
					else $response["data"]["no_of_wks"] = "valid";
					
					if($format == "" )$response["data"]["format"] = "formatnull";
					else $response["data"]["format"] = "valid";
					
					if($ageGroup == "")$response["data"]["agegroup"] = "agegroupnull";
					else $response["data"]["agegroup"] = "valid";
					
					if($studentProfile == "")$response["data"]["hiddenVal5"] = "selectskill";
					else $response["data"]["hiddenVal5"] = "valid";
					
					
					$aboutCourse = trim(strip_tags($aboutCourse),"'");
					if($aboutCourse == "")$response["data"]["OverviewDescription"] = "descriptionnull";
					else $response["data"]["OverviewDescription"] = "valid";
					
					if($courseName == "" )$response["data"]["coursename"] = "coursenamenull";
					else $response["data"]["coursename"] = "valid";
					
//					if($gradeTeach == "")$response["data"]["hiddenVal3"] = "selectagegroup";
//					else $response["data"]["hiddenVal3"] = "valid";
//					if($skillIds == "")$response["data"]["hiddenVal2"] = "selectskill";
//					else $response["data"]["hiddenVal2"] = "valid";
					
					
					
					if(!in_array('onlyenternull',$response['data']) &&
					!in_array('allfield',$response['data']) &&
					!in_array('selectlanguage',$response['data']) &&
					!in_array('selectagegroup',$response['data']) &&
					!in_array('ratenull',$response['data']) &&
					!in_array('paymentnull',$response['data']) &&
					!in_array('selectskill',$response['data']) &&
					!in_array('coursenamenull',$response['data']) &&
					!in_array('tavelradiusnull',$response['data']) &&
					!in_array('shedulenull',$response['data']) &&
					!in_array('agegroupnull',$response['data']) &&
					!in_array('formatnull',$response['data']) &&
					!in_array('daysofweeknull',$response['data']) &&
					!in_array('descriptionnull',$response['data']) &&
					!in_array('onlyselectnull',$response['data']) && !in_array('selectatleast',$response['data']) && !in_array('durationnull',$response['data']) &&
					!in_array('durationnumeric',$response['data']) && !in_array('null',$response['data']) && !in_array('uncheck',$response['data']) &&  !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
				}
				else {
					$lastupdatedate = date("Y-m-d H:i:s");
					
					if (isset($authUserNamespace->uploaded_image_name1) && $authUserNamespace->uploaded_image_name1!="")
					{
						$image_typearr = explode(".",$authUserNamespace->uploaded_image_name1);
						$image_type = "image/".$image_typearr[1];
						$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_name1);
					}else{
						$image_type = "";
						$getCropimagecontent="";
					}
					if($travelRadius == "Locations where you travel to teach")
					{	
						$travelRadius="";
					}
					
				//	$data1 = array("min"=>$min1,"max"=>$max1);
				//	$tutorProfileobj->update($data1,"id=$tutorid");
					//if (isset($authUserNamespace->classtype) && ( $authUserNamespace->classtype=="2" || $authUserNamespace->classtype=="3" || $authUserNamespace->classtype=="4")){
						$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_course_name"=>$courseName,"tutor_course_desc"=>$aboutCourse,
									"tutor_student_profile"=>$studentProfile,"tutor_student_comments"=>$studentComments,"student_age_group"=>$ageGroup,"tutor_format"=>$format,
									"tutor_certificate_granted"=>$certiGranted,
									"tutor_class_dur_wks"=>$numberofWeeks,"tutor_class_dur_dayspwk"=>$numberofDays,
									"tutor_class_dur_hrspday"=>$numberofHors,"tutor_class_timing"=>'',
									"tutor_batch_week"=>$daysofWeek,"tutor_batch_from_timing"=>$timeFrom,"tutor_batch_to_timing"=>$timeTo,
									"tutor_batch_status"=>$b_Tutor_status,
									"batch_size"=>$batchSize,
									"tutor_class_batch_schedule"=>$batchShedule,"tutor_lesson_location"=>$lessionLocation,"travel_radius"=>$travelRadius,
									"tutor_class_image_type"=>$image_type,"tutor_class_image_content"=>$getCropimagecontent,
									"tutor_lesson_language"=>$language,"tutor_pay_feetype"=>$payfeetype,
									"tutor_rate"=>$rate,"rate_comments"=>$rateComments,"tutor_pay_cycle"=>$paymentcycle,
									"tutor_canc_policy"=>$canPolicy,"lastupdatedate"=>$lastupdatedate,"min"=>$min1,"max"=>$max1);
					//} 
					//-------------------------------same for all--------------------------------------------------------------
//					else{
//						$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_course_name"=>$courseName,"tutor_course_desc"=>$aboutCourse,
//									"tutor_student_profile"=>$studentProfile,"tutor_student_comments"=>$studentComments,"student_age_group"=>$ageGroup,"tutor_format"=>$format,
//									"tutor_certificate_granted"=>$certiGranted,
//									"tutor_class_dur_wks"=>$numberofWeeks,"tutor_class_dur_dayspwk"=>$numberofDays,
//									"tutor_class_dur_hrspday"=>$numberofHors,"tutor_class_timing"=>'',
//									"tutor_batch_week"=>$daysofWeek,"tutor_batch_from_timing"=>$timeFrom,"tutor_batch_to_timing"=>$timeTo,
//									"tutor_batch_status"=>$b_Tutor_status,
//									"tutor_class_batch_schedule"=>$batchShedule,"tutor_lesson_location"=>$lessionLocation,"travel_radius"=>$travelRadius,
//									"tutor_class_image_type"=>$image_type,"tutor_class_image_content"=>$getCropimagecontent,
//									"tutor_lesson_language"=>$language,"tutor_pay_feetype"=>$payfeetype,
//									"tutor_rate"=>$rate,"rate_comments"=>$rateComments,"tutor_pay_cycle"=>$paymentcycle,
//									"tutor_canc_policy"=>$canPolicy,"lastupdatedate"=>$lastupdatedate);
//					}
					//------------------------------end--------------------------------------------------------------
					if (isset($authUserNamespace->tutorcoursesessionid) && $authUserNamespace->tutorcoursesessionid!="")
					{
						$tutorcourseSessionId = $authUserNamespace->tutorcoursesessionid;
						$tutorCourseobj->update($data,"id=$authUserNamespace->tutorcoursesessionid");
						if (isset($tutorcourseSessionId) && $tutorcourseSessionId!='')
						{
							$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
							$data2 = array("tutor_course_id"=>$tutorcourseSessionId);
							$batchdetailObj->update($data2,"tutor_id='$authUserNamespace->tutorsessionid' && tutor_course_id='0'");
						}
						//-----------------------------update branches----------------------------------------------------------
//						if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="3" || $authUserNamespace->classtype=="4"))
//						{
						if (isset($addressIDarray) && sizeof($addressIDarray)>0)
						{
								$lenAddress = sizeof($address);
								$insertremianAddress = $lenAddress-$authUserNamespace->avgaddvar;
								$prevsessionAddress = $authUserNamespace->avgaddvar;
								
//								echo "avgaddvar::".$avgaddvar;
//								echo "len:::".$lenAddress;
//								echo "inser:::".$insertremianAddress;
//								echo "previouss:::".$prevsessionAddress;
//								exit;
								for($m=0;$m<=$prevsessionAddress;$m++)
								{
									$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"course_id"=>$tutorcourseSessionId,
														"address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
														"pincode"=>$pincode[$m],"landmark"=>$landmark[$m]);
									$branchdetailObj->update($data,"course_id='$authUserNamespace->tutorcoursesessionid' && id='$addressIDarray[$m]'");
								}
								for($m=($prevsessionAddress+1);$m<$lenAddress;$m++)
								{
									$data1 = array("tutor_id"=>$authUserNamespace->tutorsessionid,"course_id"=>$tutorcourseSessionId,
														"address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
														"pincode"=>$pincode[$m],"landmark"=>$landmark[$m],"lastupdatedate"=>$lastupdatedate);
									$branchdetailObj->insert($data1);
								}
						}else{
							//if (isset($avgaddvar) && $avgaddvar=="0")
						//	{
								for($m=0;$m<sizeof($address);$m++)
								{
										$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"course_id"=>$tutorcourseSessionId,
														"address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
														"pincode"=>$pincode[$m],"landmark"=>$landmark[$m],"lastupdatedate"=>$lastupdatedate);
										$branchdetailObj->insert($data);
								}
							//}
						}
							$authUserNamespace->avgaddvar = $avgaddvar;
					//	}
						//-----------------------------end of update branches----------------------------------------------------------
					}else{
						$tutorCourseobj->insert($data);
						$tutorcourseSessionId = $tutorCourseobj->getAdapter()->lastInsertId("tutor_course_name='$courseName'");
						if (isset($tutorcourseSessionId) && $tutorcourseSessionId!='')
						{
							$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
							$data2 = array("tutor_course_id"=>$tutorcourseSessionId);
							$batchdetailObj->update($data2,"tutor_id='$authUserNamespace->tutorsessionid' && course_id='0'");
						}
						//-----------------------------add branches----------------------------------------------------------			
//						if (isset($authUserNamespace->classtype) && ($authUserNamespace->classtype=="3" || $authUserNamespace->classtype=="4"))
//						{
							$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
							for($m=0;$m<sizeof($address);$m++)
							{
									$data = array("tutor_id"=>$authUserNamespace->tutorsessionid,"tutor_skill_course_id"=>$tutorcourseSessionId,
													"number_of_branch"=>$numberofBranch,"address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
													"phone_number1"=>$phone1[$m],"phone_number2"=>$phone2[$m],"lastupdatedate"=>$lastupdatedate);
									$branchdetailObj->insert($data);
							}
							$authUserNamespace->avgaddvar = $avgaddvar;
//						}
						//-----------------------------end of add branches----------------------------------------------------
						$authUserNamespace->tutorcoursesessionid = $tutorcourseSessionId;
						$authUserNamespace->formname3 = "coursedetail";
					}
//					if (isset($authUserNamespace->classtype) && $authUserNamespace->classtype=="4")
//					{
//						for($m=0;$m<sizeof($address);$m++)
//						{
//							if (isset($authUserNamespace->tutorcoursesessionid) && $authUserNamespace->tutorcoursesessionid!="")
//							{
//								//echo "sdaf";exit;
//								$data = array("tutor_skill_course_id"=>$authUserNamespace->tutorcoursesessionid,
//												"address"=>$address[$m],"contact_no"=>$contactNumber[$m],
//												);
//												echo "dsa";exit;
//								$companyaddressObj->update($data,"tutor_skill_course_id='$authUserNamespace->tutorcoursesessionid' && id='$addressIDarray[$m]'");
//							}else{
//								$data = array("tutor_skill_course_id"=>$tutorcourseSessionId,
//												"address"=>$address[$m],"contact_no"=>$contactNumber[$m],
//												);
//												echo "ss";exit;
//								$companyaddressObj->insert($data);
//							}
//						}
//					}
					$this->_redirect('/teacher-signup-brief-description');
				}
		}
	}
	
	public function aboutmeAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		unset($authUserNamespace->edit_profile_session);
		unset($authUserNamespace->edit_profile_session1);
		if((!isset($authUserNamespace->tutorsessionid) &&  $authUserNamespace->tutorsessionid == NULL) && (!isset($authUserNamespace->formname3) &&  $authUserNamespace->formname3 != "coursedetail"))
		{
			$this->_redirect("/tutor/register");
		}
		$this->_helper->layout()->setLayout('layout2');
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
		$tutorid = $this->_request->getParam("tutorid");
		if (isset($tutorid) && $tutorid!="")
		{
			//$authUserNamespace->tutorsessionid = $tutorid;
			$tutorData = $tutorProfileobj->fetchRow("id ='$tutorid'");
			$this->view->tutordata = $tutorData;
		}
		if($this->_request->isPost()){
	
			$selfDescription = $this->_request->getParam("OverviewDescription");
			$authUserNamespace->selfDescription = $selfDescription;
			$gender = $this->_request->getParam("gender");
			$fbLink = $this->_request->getParam("fb_link");
			$bankname = $this->_request->getParam("bank_name");
			$accno = $this->_request->getParam("acc_no");
			$ifsccode = $this->_request->getParam("ifsc_code");
			$twitterLink = $this->_request->getParam("twitter_link");
			$linkedinLink = "";
			$videoLinkdis = $this->_request->getParam("video_link");
			
			$blogLink = $this->_request->getParam("blog_link");
			$websiteLink = $this->_request->getParam("website_link");
			
			//$videoLink1 = $this->_request->getParam("video_link1");
			//$videoLink2 = $this->_request->getParam("video_link2");
			//$videoLink3 = $this->_request->getParam("video_link3");
			$day = $this->_request->getParam("date");
			$month = $this->_request->getParam("month");
			$year = $this->_request->getParam("year");
			if (isset($authUserNamespace->flow_pic_session1) && $authUserNamespace->flow_pic_session1!="")
			{
				$image = $authUserNamespace->flow_pic_session1;
			}else{
				$image = "";
			}
			$imagecheck = $this->_request->getParam("imgch");
			
			//----------------------fb twitter link correction with/without http-----------
			if (isset($fbLink) && $fbLink!="")
			{
				$fbVar1 = strstr($fbLink,'https://');
				$fbVar2 = strstr($fbLink,'http://');
				if (isset($fbVar1) && $fbVar1!="")
				{
					$tempVar2 = substr($fbVar1,8,strlen($fbVar1));
				}else if (isset($fbVar2) && $fbVar2!="")
				{
					$tempVar2 = substr($fbVar2,7,strlen($fbVar2));
				}
				else{
					$tempVar2 = $fbLink;
				}
			}
			if (isset($tempVar2) && $tempVar2!=""){
				$fbLink = "https://".$tempVar2;
			}else{
				$fbLink = "";
			}
			if (isset($twitterLink) && $twitterLink!="")
				{
					$twitVar1 = strstr($twitterLink,'https://');
					$twitVar2 = strstr($twitterLink,'http://');
					if (isset($twitVar1) && $twitVar1!="")
					{
						$twittempVar2 = substr($twitVar1,8,strlen($twitVar1));
					}else if (isset($twitVar2) && $twitVar2!="")
					{
						$twittempVar2 = substr($twitVar2,7,strlen($twitVar2));
					}
					else{
						$twittempVar2 = $twitterLink;
					}
				}
				
			if (isset($twittempVar2) && $twittempVar2!="" ){
				$twitterLink = "https://".$twittempVar2;
			}else{
				$twitterLink = "";
			}
		if (isset($blogLink) && $blogLink!="")
				{
					$blogVar1 = strstr($blogLink,'https://');
					$blogVar2 = strstr($blogLink,'http://');
					if (isset($blogVar1) && $blogVar1!="")
					{
						$blogtempVar2 = substr($blogVar1,8,strlen($blogVar1));
					}else if (isset($blogVar2) && $blogVar2!="")
					{
						$blogtempVar2 = substr($blogVar2,7,strlen($blogVar2));
					}
					else{
						$blogtempVar2 = $blogLink;
					}
				}
				
			if (isset($blogtempVar2) && $blogtempVar2!="" ){
				$blogLink = "https://".$blogtempVar2;
			}else{
				$blogLink = "";
			}
			
			if (isset($websiteLink) && $websiteLink!="")
				{
					$webVar1 = strstr($websiteLink,'https://');
					$webVar2 = strstr($websiteLink,'http://');
					if (isset($webVar1) && $webVar1!="")
					{
						$webtempVar2 = substr($webVar1,8,strlen($blogVar1));
					}else if (isset($webVar1) && $webVar1!="")
					{
						$webtempVar2 = substr($webVar1,7,strlen($webVar1));
					}
					else{
						$webtempVar2 = $websiteLink;
					}
				}
				
			if (isset($webtempVar2) && $webtempVar2!="" ){
				$websiteLink = "https://".$webtempVar2;
			}else{
				$websiteLink = "";
			}
			
			//-------------------------------------------------------------------------
			$v = 1;
			$videoLink = Array();
			for($i=1;$i<7;$i++){
				$videoLink[$i] = "";
			}
			for($i=0;$i<sizeof($videoLinkdis);$i++)
				{
					if($videoLinkdis[$i]!="")
					{
						//echo "f".$i."--";
						$videoLink[$v] = $videoLinkdis[$i];
						$v++;
					}
				}
				 
			 $v = $v - 1;
			 $v_count_val = $v;
		 	if($this->_request->isXmlHttpRequest())
			{
			        $this->_helper->layout()->disableLayout();
			        $this->_helper->viewRenderer->setNoRender(true);
			        $response = array();

					$selfDescription = trim(strip_tags($selfDescription),"'");
					if($selfDescription == "")$response["data"]["OverviewDescription"] = "onlyenternull";
					else $response["data"]["OverviewDescription"] = "valid";
					
			    	$filecheck = basename($image);
					$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
					$ext = strtolower($ext);
				 	$f=0;
					if($imagecheck == ""){
						$f=1;
						if($image == "")$response["data"]["imgch"] = "imageuploaderror";
						elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['imgch']="valid";
						else $response["data"]["imgch"] = "invalid";
					}
					
					if(($imagecheck!="" || $imagecheck!=null)&&($image!="" || $image!=null)){
                        	
						if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['imgfile']="valid";
						else {$response["data"]["imgfile"] = "invalid";	}
					}
					
					else{
						if($f!=1){
							$response["data"]["imgfile"]="valid";
						}
					}
					
					if(!in_array('onlyenternull',$response['data']) && !in_array('imageuploaderror',$response['data']) && !in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
    
		    }else{
//		    	$image_type = $_FILES["imgfile"]["type"];
//				$tmpname = $_FILES["imgfile"]["tmp_name"];
//				$image_size = $_FILES["imgfile"]["size"];
			//	echo $image_size;
				//exit;
				//
				//echo $authUserNamespace->flow_pic_session1;exit;
	  			if (isset($authUserNamespace->flow_pic_session1) && $authUserNamespace->flow_pic_session1!="")
				{
					$image_typearr = explode(".",$authUserNamespace->flow_pic_session1);
					$image_type = "image/".$image_typearr[1];
					$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1);
					
					$jpeg_quality = 90;
					$ThumbWidth =160;
					$img_r = imagecreatefromstring($getCropimagecontent);
					imagefilter($img_r, IMG_FILTER_GRAYSCALE);
				    list($width, $height) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1);
					//calculate the image ratio
					$imgratio=$width/$height;
					if ($imgratio>1){
						$newwidth = $ThumbWidth;
						$newheight = $ThumbWidth/$imgratio;
					}else{
						$newheight = $ThumbWidth;
						$newwidth = $ThumbWidth*$imgratio;
					}
					// create a new temporary image
					
					$tmp_img = imagecreatetruecolor( $newwidth, $newheight );
					imagecopyresized($tmp_img, $img_r, 0, 0, 0, 0, $newwidth,$newheight, $width, $height);
					$target_path1 = dirname(dirname(dirname(__FILE__)))."/docs/Userfc.jpeg";
					$tagetcrop = dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1;
					
					imagejpeg($tmp_img,$target_path1,$jpeg_quality);
					
					$filecheck = basename($target_path1);
					$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
					$ext = strtolower($ext);
					if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" ||$ext == "pjpeg" || $ext == "png"){
					 		$thumb_type="image/".$ext;
					}
					$imagethumbcontent = file_get_contents($target_path1);
					
					$imagew='240';
    				$imageh='245';
    				//$src=file_get_contents($target_path1);
	    	
					$img_r = imagecreatefromstring($getCropimagecontent);
					$dst_r = ImageCreateTrueColor($imagew,$imageh);
					list($width11, $height11) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1);
	    			//list($width11, $height11) = getimagesize($target_path1);
	    			$jpeg = '.jpeg';
	    			imagecopyresized($dst_r,$img_r, 0, 0, 0, 0, $imagew, $imageh, $width11, $height11);
					imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg,$jpeg_quality);
					$var1=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg);
				}else{
					$var1 = $tutor_image_content = $thumb_type = $image_type = "";
					$imagethumbcontent = $getCropimagecontent="";
				}
						$date = $day."-".$month."-".$year;
						$data = array("tutor_brief_desc"=>$selfDescription,"tutor_gender"=>$gender,"tutor_dob"=>$date,
										"tutor_fb_link"=>$fbLink,"tutor_tw_link"=>$twitterLink,
										"blog_link"=>$blogLink,"website_link"=>$websiteLink,
										"bank_name"=>$bankname,"acc_no"=>$accno,
										"ifsc_code"=>$ifsccode,
										"tutor_ld_link"=>$linkedinLink,"count_video"=>$v_count_val,									
										"tutor_image_type"=>$image_type,"tutor_image_content"=>$getCropimagecontent,
										"thumb_profile_content"=>$var1,"thumb_profile_type"=>$image_type,"crone_flag"=>'1',
										"tutor_thumb_type"=>$thumb_type,"tutor_thumb_content"=>$imagethumbcontent);
						if (isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!="")
						{

							$tutorId = $authUserNamespace->tutorsessionid;
				    		$tutorProfileobj->update($data,"id=$tutorId");
				    		//print_r($data);exit;

							for($j=1;$j<=$v_count_val;$j++)
				    		{
				    			$lastupdatedate = date("Y-m-d H:i:s");	
				    			$data1=array("tutor_id"=>$tutorId,"video_link"=>$videoLink[$j],"lastupdatedate"=>$lastupdatedate);				    			
				    			$tutorVideoObj->insert($data1);
				    		}
				    		$authUserNamespace->maintutorid = $tutorId;
							$authUserNamespace->logintype = "1";
							
							$fetch_data = $tutorProfileobj->fetchRow("id='$tutorId'");
							$tutorFullname = ucfirst($fetch_data->tutor_first_name)." ".ucfirst($fetch_data->tutor_last_name);
							$tutorMail = $fetch_data->tutor_email;
							$tutorPass = $fetch_data->tutor_pwd;
							$companyName = $fetch_data->company_name;
							
							if (isset($companyName) && $companyName!="")
							{
								$toName = $companyName." "."(".$tutorFullname.")";
							}else{
								$toName = $tutorFullname;
							}
							
							$message2 = "<div style='text-align:justify;'>";
							$message2 .= "<div>Hi ".$tutorFullname."...<br/></div><br/>";
							$message2 .= "<div>Thanx for registering with Skillzot, you've made our day.<br/></div><br/>";
							$message2 .= "<div>Within one working day we will validate your profile and ensure that all the fields have been populated correctly. Once we do so, we will immediately make your profile live on Skillzot.<br/></div><br/>";
							$message2 .= "<div>We will call you in case we have any clarifications or there is some information yet to be completed. The sooner we complete all necessary details, the faster you can go live with your webpage on Skillzot.<br/></div><br/>";
							$message2 .= "<div>Post which we can direct relevant students who you can help become the next coolest talent maybe.<br/></div><br/>";
							$message2 .= "<div>If you need any help feel free to reply to this email or give us a call on +91 9820921404.<br/></div><br/>";
							$message2 .= "<div>Looking forward to partnering with you to help people add new 'happy powers' (our lingo for skills, think its way cooler), so that they can learn what they love and add to their happiness quotient, after all, we are happiest when we do the things we love, right?<br/></div><br/>";
							$message2 .= "<div>Cheers,<br/></div>";
							$message2 .= "<div>Sandeep<br/></div><br/>";
							$message2 .= "<div>Co-founder @ Skillzot</div>";
							$message2 .= "</div>";
							$message2 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
						
							$message = $message2;
							$to = $tutorMail;
							$from = "zot@skillzot.com";
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,"Sandeep @ Skillzot");
							$mail->addTo($to,$toName);
							$mail->setSubject("Chosen wisely you have");
							$mail->send();
							
							// hitesh added
							$message = $message2;
							$to1 = "sandeep.burman@skillzot.com";
							//$to = "zot@skillzot.com,sandeep.burman@skillzot.com";
							$to = "zot@skillzot.com";
							$from = "zot@skillzot.com";
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,"Sandeep @ Skillzot");
							$mail->addTo($to,$toName);
							$mail->addTo($to1,$toName);
							$mail->setSubject("Hola! You've made our day!");
							$mail->send();
							
							// hitesh ended
							
							//------backup mail---------------
							$tempsubVal = "Teacher registration ".$toName;
							$from = $tutorMail;
							$to = "zot@skillzot.com";
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,$tutorMail);
							$mail->addTo($to,"Skillzot");
							$mail->setSubject($tempsubVal);
							$mail->send();
							//----------------------------------
				    		unset($authUserNamespace->tutorsessionid);
				    		unset($authUserNamespace->tutorcoursesessionid);
				    		unset($authUserNamespace->formname1);
				    		unset($authUserNamespace->formname2);
				    		unset($authUserNamespace->formname3);
				    		unset($authUserNamespace->classtype);
				    		unset($authUserNamespace->tutoremail);
				    		unset($authUserNamespace->uploaded_image_name);
							unset($authUserNamespace->uploaded_image_name1);
							unset($authUserNamespace->selfDescription);
							unset($authUserNamespace->imageerror);
							unset($authUserNamespace->skillidnew);
							unset($authUserNamespace->notinserttest);
							unset($authUserNamespace->notinserttestdegree);
							unset($authUserNamespace->flow_pic_session1);
							unset($authUserNamespace->flow_pic_session);
							unlink($target_path1);
				    		array_map("unlink",glob(dirname(dirname(dirname(__FILE__)))."/docs/*.*" ) );
				    		//unlink($tagetcrop);
							//echo $authUserNamespace->useridfriedly;exit;
							$this->_redirect('/tutor/profile/tutorid/'.$tutorId);
				}
			}
		}
	}
	public function profileAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		$feetypeObj = new Skillzot_Model_DbTable_Tutorfeetype();
		$paycycleObj = new Skillzot_Model_DbTable_Tutorpaycycle();
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		//echo $authUserNamespace->useridfriedly;exit;
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutorcourseObj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$batchObj = new Skillzot_Model_DbTable_Batch();
		
		
		//from skill main table to skill display table page title and discription
		
		
	//	$skillobjlisting = new Skillzot_Model_DbTable_Skillslisting();
	//		$uniqSkillname=array();
	//		$uniqSkillname1=array();
	//			$sub_category_id = $this->_request->getParam("skill_id");
	//			$category_rows = $skillobj->fetchAll($skillobj->select()
	//														  ->from(array('s'=>DATABASE_PREFIX."master_skills"))
	//														  ->order(array("s.skill_name asc")));
	////														  foreach ($category_rows as $catr)
	//														  {
	//														  	//$uniqSkillname[]= str_replace("-"," ",$catr->skill_name);
	//														  	$uniqTitle=$catr->page_title;
	//															$uniqmetaDescription=$catr->skill_description;
															  	
	//														  	$data = array("page_title"=>$uniqTitle,"skill_description"=>$uniqmetaDescription);
	//														  	$skillobjlisting->update($data,"skill_id='$catr->skill_id'");
	//														  }
		//print_r($uniqSkillname);
	//echo "inserted";exit;
		
		
		
		
		
//			$uniqSkillname=array();
//			$uniqSkillname1=array();
//				$sub_category_id = $this->_request->getParam("skill_id");
//				$category_rows = $skillobj->fetchAll($skillobj->select()
//															  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_description','s.skill_id'))
//															  ->order(array("s.skill_name asc")));
//															  foreach ($category_rows as $catr)
//															  {
//															  	//$uniqSkillname[]= str_replace("-"," ",$catr->skill_name);
//															  	$uniqSkillname= preg_replace("/[^A-Za-z0-9]/", "", $catr->skill_name);
//															  	
//															  	$data = array("skill_uniq"=>strtolower($uniqSkillname));
//															  	$skillobj->update($data,"skill_id='$catr->skill_id'");
//															  }
//		//print_r($uniqSkillname);
//	echo "inserted";exit;
		
		
		//--------------for skills other table
//		$skillobj = new Skillzot_Model_DbTable_Skillslisting();
//		$uniqSkillname=array();
//			$uniqSkillname1=array();
//				$sub_category_id = $this->_request->getParam("skill_id");
//				$category_rows = $skillobj->fetchAll($skillobj->select()
//															  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_description','s.skill_id'))
//															  );
//															  foreach ($category_rows as $catr)
//															  {
//															  	//$uniqSkillname[]= str_replace("-"," ",$catr->skill_name);
//															  	$uniqSkillname= preg_replace("/[^A-Za-z0-9]/", "", $catr->skill_description);
//															  	
//															  	$data = array("skill_uniq"=>strtolower($uniqSkillname));
//															  	$skillobj->update($data,"skill_id='$catr->skill_id'");
//															  }
//		exit;
		
		
		
		
		$username = $this->_request->getParam("username");
		
		$usernamefromSearch = $this->_request->getParam("usernamefromsearch");
		//$username= preg_replace("/[^A-Za-z0-9]/", "",$username);
		//echo $username;exit;
		if (isset($username) && $username!="")
		{
			$newusernameData = $tutorProfileobj->fetchRow("userid_new = '$username'");
			if (isset($newusernameData) && sizeof($newusernameData)>0)
			{
				$turorflagid="1";
				$tutorId = $newusernameData->id;
			}
			$usernameData = $tutorProfileobj->fetchRow("userid = '$username'");
			if (isset($usernameData) && sizeof($usernameData)>0)
			{
				$turorflagid="1";
				$tutorId = $usernameData->id;
			}
			if($turorflagid!=1){
				$this->_redirect('/page-not-found');
			}
		}else if(isset($usernamefromSearch) && $usernamefromSearch!=""){
			
		$usernameDatafromsearch = $tutorProfileobj->fetchRow("tutor_url_name = '$usernamefromSearch'");
			if (isset($usernameDatafromsearch) && sizeof($usernameDatafromsearch)>0)
			{
				$tutorId = $usernameDatafromsearch->id;
			}else{
				$this->_redirect('/page-not-found');
			}
		}
		else{
				$tutorId = $this->_request->getParam("tutorid");
				//$tutorId= 51;
		}
		
		//echo $tutorId;exit;
		
			

		


//-------------------------end of rewrite rule-------------------------------
		
/*		$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
if ($_SERVER["SERVER_PORT"] != "80")
{
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
} 
else 
{
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
}


$tempurl = parse_url($pageURL, PHP_URL_PATH);
$string = substr($tempurl, 9);
echo $string;
		exit; */
//$v = curl_version(); 
//print $v['ssl_version'];

//	function _is_curl_installed() {
//	if  (in_array  ('curl', get_loaded_extensions())) {
//		return true;
//	}
//	else {
//		return false;
//	}
//}
//
//if (_is_curl_installed()) {
//  echo "cURL is installed on this server";
//} else {
//  echo "cURL is NOT installed on this server";
//}
//
////		echo 'cURL is '.(function_exists('curl_init') ?: ' not').' enabled';
////
//exit;

		
		
		
		//----------mannuall entry code----
		/* $tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		
		$allbranch = $tutorProfileobj->fetchAll($tutorProfileobj->select()
											->from(array('r'=>DATABASE_PREFIX."tx_tutor_profile"),array('id','tutor_address','tutor_add_city','tutor_add_locality','tutor_add_pincode'))
											);
		$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
 if (isset($allbranch) && sizeof($allbranch)>0)
{
		foreach ($allbranch as $branch){
			$lastupdatedate = date("Y-m-d H:i:s");
			$coursedatadis = $tutorCourseobj->fetchAll($tutorCourseobj->select()
											->from(array('r'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('id','tutor_skill_id'))
											->where("r.tutor_id='$branch->id'")
											->order(array("id asc")));
			if (isset($coursedatadis) && sizeof($coursedatadis)>0)
			{
				foreach ($coursedatadis as $course)
				{
					$data1 = array("tutor_id"=>$branch->id,"course_id"=>$course->id,
														"address"=>$branch->tutor_address,"city"=>$branch->tutor_add_city,"locality"=>$branch->tutor_add_locality,
														"pincode"=>$branch->tutor_add_pincode,"landmark"=>"Landmark","lastupdatedate"=>$lastupdatedate);
					$branchdetailObj->insert($data1);
				}
			}
		}
}
		exit; */
		//--------------
		
		
		//-------------------------travel radious entryy code----------------------
//		$lastupdatedate = date("Y-m-d H:i:s");
//		$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
//		$fetchallturoriddata = $tutorProfileobj->fetchAll($tutorProfileobj->select()
//										->from(array('p'=>DATABASE_PREFIX."tx_tutor_profile"),array('id'))
//										->order(array("p.id asc")));
//										foreach ($fetchallturoriddata as $tutorrow)
//										{
//											$data = array("tutor_id"=>$tutorrow->id,"suburbs_id"=>'0',"lastupdatedate"=>$lastupdatedate);
//											$travelradiousObj->insert($data);
//										}
//print_r($fetchallturoriddata);exit;
		//---------------------end of travel radious ------------------------------
		
		
		
		//---------------sms integration code---------------------
		
//		$username='madonlinepltd';
//	    $password ='Onlinehttp';
//	    $senderid = 'SKLZOT';
//	    $udh = '0';
//	    $message = 'test message';
//	    $dlrMask = '19';
//	    $dlrUrl = 'http://skillzot1.com/tutor/profile';
//	    
//	    $encodeUrl = urlencode($dlrUrl);
//	    $mobileno ='9892689293';
//		$url = 'http://www.myvaluefirst.com/smpp/sendsms?username='.$username.'&password='.$password.'&to='.$mobileno.'&udh='.$udh.'&from='.$senderid.'&text='.$message.'&dlr-mask='.$dlrMask.'&dlr-url='.$encodeUrl;

		//http://www.myvaluefirst.com/smpp/sendsms?username=madonlinepltd&password=Onlinehttp&to=9920143226&from=SKLZOT&text=Hi Puneet, you've got mail! Raja wants to get in touch with you to make you his/her guru. Reply from skillzot.com or call on 9820305510 b4 she finds another tutor:)
		
//		$ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_HEADER, 0);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        $output = curl_exec($ch);
//        curl_close($ch);
		//---------------end of sms integration code---------------
		
        
		//----tutor signup session unset ----
		
		//echo "tutor".$authUserNamespace->maintutorid;exit;
		
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			unset($authUserNamespace->tutorsessionid);
			unset($authUserNamespace->tutorcoursesessionid);
			unset($authUserNamespace->formname1);
	    	unset($authUserNamespace->formname2);
	    	unset($authUserNamespace->formname3);
	    	unset($authUserNamespace->classtype);
	    	unset($authUserNamespace->tutoremail);
		}
    	//echo $authUserNamespace->maincategory;exit;
    
    	//----------------------------------------
    	$skillidForbreadcrums = $this->_request->getParam("skillid");
    	$skillNamefrdly = $this->_request->getParam("skillname");
    	
    	if (isset($skillidForbreadcrums) && $skillidForbreadcrums!='')
    	{
    		$fetchSkill_id = $skillidForbreadcrums;
    		$this->view->skillid_perticular = $fetchSkill_id;
    		//unset($authUserNamespace->skillidnew);
    	}elseif (isset($skillNamefrdly) && $skillNamefrdly!=""){
    		
    	$category_rows = $skillobj->fetchRow($skillobj->select()
						  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
						  ->where("s.skill_uniq='$skillNamefrdly' && s.skill_id!=0 && s.is_skill='1'"));
						 // echo "ddd";
						 if (isset($category_rows) && sizeof($category_rows)>0)
						 {
							$fetchSkill_id = $category_rows->skill_id;
    						$this->view->skillid_perticular = $fetchSkill_id;
						 }
    	}
    	else{
    		//--------------------redirection code logic-------------
		
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$courseDataforfrdlyurl = $tutorCourseobj->fetchRow($tutorCourseobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"))
										->where("c.tutor_id='$tutorId'"));
			//print_r($courseData);exit;
			$contMusic=0;
			$contDance=0;
			$contFitness=0;
			$contMorecool=0;
			if (isset($courseDataforfrdlyurl) && sizeof($courseDataforfrdlyurl)>0)
			{
				$skillIdsforurl=$courseDataforfrdlyurl->tutor_skill_id;
				$skillIdsforurl = substr($skillIdsforurl, 0, strlen($skillIdsforurl)-1);
				$skillIdsforurl = explode(",",$skillIdsforurl);
				if (isset($skillIdsforurl) && sizeof($skillIdsforurl)>0)
				{
					if (sizeof($skillIdsforurl)=='1')
					{
						$onlyOneskillfrdly = 1;//temp value 1
					}
					else{
						for($i=0;$i<sizeof($skillIdsforurl);$i++){
						if (isset($skillIdsforurl[$i]) && $skillIdsforurl[$i]!="")
				    	{
							$skillndataforfrdly = $skillobj->fetchRow("skill_id='$skillIdsforurl[$i]' && skill_id!=0");
							if (isset($skillndataforfrdly) && sizeof($skillndataforfrdly)>0)
							{
								$skillDepthforfrdly = $skillndataforfrdly->skill_depth;
								if(isset($skillDepthforfrdly) && $skillDepthforfrdly=='3')
								{
									$skillndata2frdly = $skillobj->fetchRow("skill_id='$skillndataforfrdly->parent_skill_id' && skill_id!=0");
									if (isset($skillndata2frdly) && sizeof($skillndata2frdly)>0)
									{
										if($skillndata2frdly->skill_id=='13')
										{
											//echo "in1";
											$contMusic++;
										}else if($skillndata2frdly->skill_id=='14'){
											//echo "in2";
											$contDance++;
										}else if($skillndata2frdly->skill_id=='4'){
											//echo "in3";
											$contFitness++;
										}else if($skillndata2frdly->skill_id=='6'){
											//echo "in4";
											$contMorecool++;
										}
									}
								}else if(isset($skillDepthforfrdly) && $skillDepthforfrdly=='2')
								{
										if($skillndataforfrdly->skill_id=='13')
										{
											//echo "in5";
											$contMusic++;
										}else if($skillndataforfrdly->skill_id=='14'){
											//echo "in6";
											$contDance++;
										}else if($skillndataforfrdly->skill_id=='4'){
											//echo "in7";
											$contFitness++;
										}else if($skillndataforfrdly->skill_id=='6'){
											//echo "in8";
											$contMorecool++;
										}
								}else{
									}
							}
				    	}
					}
					}
					
				}
			}
			//print_r($skillIdsforurl);exit;
			if (isset($onlyOneskillfrdly) && $onlyOneskillfrdly=='1')
			{
				$skillidForbreadcrums=$skillIdsforurl[0];
				$fetchSkill_id = $skillidForbreadcrums;
    			$this->view->skillid_perticular = $fetchSkill_id;
			}else{
			if ((isset($contMusic) && $contMusic!="")||(isset($contDance) && $contDance!="")||(isset($contFitness) && $contFitness!="")||(isset($contMorecool) && $contMorecool!=""))
			{
			$max = max($contMusic,$contDance,$contFitness,$contMorecool);
				foreach( array('contMusic','contDance','contFitness','contMorecool') as $v) {
				    if ($$v == $max) {
				        $finalSkillnamefromfour = "\$$v";
				        
					    if($finalSkillnamefromfour=='$contMusic')
						{
							$skillidForbreadcrums=13;
							$fetchSkill_id = $skillidForbreadcrums;
    						$this->view->skillid_perticular = $fetchSkill_id;
						}else if($finalSkillnamefromfour=='$contDance')
						{
							$skillidForbreadcrums=14;
							$fetchSkill_id = $skillidForbreadcrums;
    						$this->view->skillid_perticular = $fetchSkill_id;
						}else if($finalSkillnamefromfour=='$contFitness'){
							$skillidForbreadcrums=4;
							$fetchSkill_id = $skillidForbreadcrums;
    						$this->view->skillid_perticular = $fetchSkill_id;
						}else{
							$skillidForbreadcrums=6;
							$fetchSkill_id = $skillidForbreadcrums;
    						$this->view->skillid_perticular = $fetchSkill_id;
						}
				       // break;
				    }
				}
			}
		}
		//--------------------------------------------
    	}
    	//echo $fetchSkill_id;exit;
    	if (isset($fetchSkill_id) && $fetchSkill_id!="")
    	{
			$skillndata = $skilldisplayObj->fetchRow($skilldisplayObj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1'"));//21-8
    	}
			if (isset($skillndata) && sizeof($skillndata)>0 )
				{
					$skillCategory = $skillndata->skill_name;
					$this->view->skillcategory = $skillCategory;
					$this->view->skillid = $skillndata->skill_id;
					$this->view->skillcategoryUniq = $skillndata->skill_uniq;
					$parentId = $skillndata->parent_skill_id;
					$skillDepth = $skillndata->skill_depth;
				}
			if (isset($skillDepth) && $skillDepth > 1)
			{
				$parentdata = $skilldisplayObj->fetchRow("skill_id='$parentId' && skill_id!=0 && is_skill='1' && is_enabled='1'");
				if (isset($parentdata) && sizeof($parentdata)>0)
				{
					$submainCategory = $parentdata->skill_name;
					if (isset($submainCategory) && $submainCategory!="")
						{
							$this->view->mainsubmaincategory = $submainCategory;
							$this->view->subcategoryid = $parentdata->skill_id;
							$this->view->mainsubmaincategoryUniq = $parentdata->skill_uniq;
						}
					$seccondParentID = $parentdata->parent_skill_id;
					$secondskillDepth = $parentdata->skill_depth;
				}
				if (isset($secondskillDepth) && $secondskillDepth > 1)
				{
					$finaldata = $skilldisplayObj->fetchRow("skill_id='$seccondParentID' && skill_id!=0 && is_skill='1'");
					$mainCategory = $finaldata->skill_name;
					//$mainCategoryId = $finaldata->skill_id;
					if (isset($mainCategory) && $mainCategory!="")
					{
						$this->view->maincategory = $mainCategory;
						$this->view->maincategoryid = $finaldata->skill_id;
						$this->view->maincategoryUniq = $finaldata->skill_uniq;
						//$this->view->maincategoryid = $mainCategoryId;
					}
				}
			}
		//------------------
		
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		$pageValue = $this->_request->getParam("page");
		if (isset($pageValue) && $pageValue!="")
		{
			$pageObj = new Skillzot_Model_DbTable_Pagesession();
			$setval2 = $pageValue. time().mt_rand();
			$mdset5 =  md5($setval2);
			$data = array("sessionnumber"=>$mdset5,"pagevalue"=>$pageValue);
			$pageObj->insert($data);
			$authUserNamespace->pagevalue = $mdset5;
			//$authUserNamespace->profileloginsessiondifferenceflag = 1; 
			
		}
		//echo $tutorId;exit;

		if (isset($tutorId) && $tutorId!="")
		{
			$this->view->tutorid = $tutorId;
			$authUserNamespace->t_id_for_review = $tutorId;
			$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
			$batchdetails_data = $batchdetailObj->fetchRow("tutor_id ='$tutorId' && b_description!=''");
			if (isset($batchdetails_data) && sizeof($batchdetails_data)>0)
			{
				$this->view->batch_t_id = $tutorId;
			}
			/* recometer logic working */
			if(isset($skillndata) && $skillndata != "")
			{
					if($skillndata->skill_id == '13' || $skillndata->skill_id == '14' || $skillndata->skill_id == '4' || $skillndata->skill_id == '6')
					{
						$rateHigh = $tutorreviewobj->fetchAll($tutorreviewobj->select()
													->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
													->where("r.tutor_id='$tutorId' && r.parent_skill_id='$skillndata->skill_id' &&  (skillgrade='9' || skillgrade='10')"));
						$rateLow = $tutorreviewobj->fetchAll($tutorreviewobj->select()
													->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
													->where("r.tutor_id='$tutorId' && r.parent_skill_id='$skillndata->skill_id' &&  (skillgrade='1' || skillgrade='2' || skillgrade='3' || skillgrade='4' || skillgrade='5' || skillgrade='6')"));
						$high=sizeof($rateHigh);
					    $low=sizeof($rateLow);
					    $recofortutor=$high-$low; 
						$reco_totaltutor = $tutorreviewobj->fetchAll($tutorreviewobj->select()
													->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment','lastupdatedate'))
													->where("r.tutor_id='$tutorId' && r.parent_skill_id='$skillndata->skill_id'")
													->order(array("r.id desc")));
														
					}
					else 
					{
						$rateHigh = $tutorreviewobj->fetchAll($tutorreviewobj->select()
													->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
													->where("r.tutor_id='$tutorId' && r.skill_id='$skillndata->skill_id' &&  (skillgrade='9' || skillgrade='10')"));
					   
					    $rateLow = $tutorreviewobj->fetchAll($tutorreviewobj->select()
													->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
													->where("r.tutor_id='$tutorId' && r.skill_id='$skillndata->skill_id' &&  (skillgrade='1' || skillgrade='2' || skillgrade='3' || skillgrade='4' || skillgrade='5' || skillgrade='6') "));
						$high=sizeof($rateHigh);
					    $low=sizeof($rateLow);
					    $recofortutor=$high-$low; 
					    $reco_totaltutor = $tutorreviewobj->fetchAll($tutorreviewobj->select()
													->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment','lastupdatedate'))
													->where("r.tutor_id='$tutorId' && r.skill_id='$skillndata->skill_id'")
													->order(array("r.id desc")));
					}
				
			}	
			if(isset($recofortutor) && $recofortutor>0){
				$this->view->recofortutor = $recofortutor;
			}else{
				$this->view->recofortutor = 0;
			}
			//print_r($reco_totaltutor);exit;
			if(isset($reco_totaltutor) && sizeof($reco_totaltutor)>0){
				$this->view->reco_totaltutor = sizeof($reco_totaltutor);
				$this->view->reco_data_all = $reco_totaltutor;
			}
			
		}
		$tutorProfileData =$tutorProfileobj->fetchRow("id='$tutorId'");
		
		if (isset($tutorProfileData) && sizeof($tutorProfileData)>0)
		{
			$this->view->tutordata = $tutorProfileData;
			$authUserNamespace->userclasstype = $tutorProfileData->tutor_class_type;
			$masteraddressobj = new Skillzot_Model_DbTable_Address();
			if (isset($tutorProfileData->tutor_add_locality) && $tutorProfileData->tutor_add_locality!=""){
				$locationId = $tutorProfileData->tutor_add_locality;
				$cityId =$tutorProfileData->tutor_add_city;
				$location_data = $masteraddressobj->fetchRow("address_id ='$locationId'");
				$city_data = $masteraddressobj->fetchRow("address_id ='$cityId'");
				$this->view->citydata = $city_data;
				$this->view->location = $location_data;
			}
			$class_id = $tutorProfileData->tutor_class_type;
			$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
			$ClassData = $classtypeObj->fetchRow("class_type_id ='$class_id'");
			if(isset($ClassData) && sizeOf($ClassData)>0){
				$this->view->classtype_id = $ClassData->class_type_id;
				if($ClassData->class_type_id!="" && ($ClassData->class_type_id == 4 || $ClassData->class_type_id == 2)){
					$this->view->classtype = 'Group Classes';
				}elseif($ClassData->class_type_id!="" && $ClassData->class_type_id == 1){
					$this->view->classtype = 'Personal Lessons';
				}elseif($ClassData->class_type_id!="" && $ClassData->class_type_id == 3){
					$this->view->classtype = 'Personal & Group Classes';
				}
				elseif($ClassData->class_type_id!="" && $ClassData->class_type_id == 5){
					$this->view->classtype = 'Dance Classes';
				}
			}
			$this->view->phonevisible = $tutorProfileData->phone_visible;
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$courseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"))
										->where("c.tutor_id='$tutorId'"));
			/*$courseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										->setIntegrityCheck(false)
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"),array("*"))
										->joinLeft(array('m'=>DATABASE_PREFIX."tx_tutor_course_batch"),'c.id=m.course_id',array(""))
										->where("m.id != '' && c.tutor_id='$tutorId'"));*/
			//print_r($courseData);exit;
			if (isset($courseData) && sizeof($courseData)>0)
			{
					$this->view->coursedata = $courseData ;
					$i = 0;
					foreach ($courseData as $course){
					 	if ((isset($course->tutor_skill_id) && $course->tutor_skill_id!="")||(isset($course->tutor_course_name) && $course->tutor_course_name!="")||(isset($course->id) && $course->id!="")){
							$tutorskillIds[$i] = $course->tutor_skill_id;
							$courseName[$i] = $course->tutor_course_name;
							$tutorIds[$i] = $course->id;
							$tutorlesionloc[$i] =$course->tutor_lesson_location;
							$i++;
					 	}else{
						
						$tutorskillIds[] = '';
							$courseName[] = '';
							$tutorIds[] = '';
						}
					}
					//----temperary for one id--------------------
					
					//print_r($tutorIds);exit;
					$tMainId = $tutorIds[0];
					//echo "sd".$tMainId;exit;
					$batchObj= new Skillzot_Model_DbTable_Batch();
					$batchResult=$batchObj->fetchAll($batchObj->select()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
							->where("c.course_id ='$tMainId'"));
					$this->view->batchResult = $batchResult;
					$coursesubData = $tutorCourseobj->fetchRow("id ='$tMainId'");
					//print_r($coursesubData);exit;				
					if (isset($coursesubData) && sizeof($coursesubData)>0){
						$this->view->coursesubdata = $coursesubData;
						$paycycleId = $coursesubData->tutor_pay_cycle ;
					
						$paytypeData = $paycycleObj->fetchRow("pay_cycle_id ='$paycycleId'");
						if (isset($paytypeData) && sizeof($paytypeData)>0){
							$this->view->paydatatype = $paytypeData->pay_cycle_name;
						}
						
						$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
						$masteraddressobj = new Skillzot_Model_DbTable_Address();
											
						$batchResult1=$batchObj->fetchAll($batchObj->select()
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.tutor_location AS tutor_location'))
											->where("c.tutor_id ='$tutorId' && c.tutor_location='1'"));
											
						$batchResult2=$batchObj->fetchAll($batchObj->select()
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.tutor_location AS tutor_location'))
											->where("c.tutor_id ='$tutorId' && c.tutor_location='2'"));
											
						if(sizeof($batchResult1)>0 && sizeof($batchResult2)=="")
						{
							$this->view->tutorlessnloc = "At Student's home";
						}
						elseif(sizeof($batchResult2)>0 && sizeof($batchResult1)=="")
						{
							$this->view->tutorlessnloc = "At Teacher's location";
						}elseif(sizeof($batchResult1)>0 && sizeof($batchResult2)>0)
					    {
							$this->view->tutorlessnloc = "At Teacher's location & open to traveling";
						}
						
						$batch_travel_radius=$batchObj->fetchRow($batchObj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('c.travel_radius AS travel_radius'))
										->where("c.tutor_id ='$tutorId' && c.travel_radius!=''"));
						if (isset($batch_travel_radius) && sizeof($batch_travel_radius)>0)
						{
							$this->view->batch_travel_radius = $batch_travel_radius->travel_radius;
						}
						//------locality display-------
						$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
//	********					$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
//														->from(array('b'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
//														->where("b.course_id='$coursesubData->id'"));
					if(isset($tutorIds) && sizeof($tutorIds)>0)
					{
					
							$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
												->setIntegrityCheck(false)
												->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('distinct(locality)'))
												->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
												->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
												->where("c.tutor_id='$tutorId'")
												 ->order(array("m.address_value asc")));
							//echo "tutordata".$tutorIds[0];exit;
						$this->view->tutorcourse =$tutorIds[0];
						$this->view->tutorlesionlocation =$tutorlesionloc[0];
//						print $branchdetailObj->select()
//												->setIntegrityCheck(false)
//												->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)','pincode','landmark','address'))
//												->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array('address_value as locality'))
//												->joinLeft(array('n'=>DATABASE_PREFIX."master_address"),'c.city=n.address_id',array('address_value'))
//												->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
//												->where("c.tutor_id='$tutorId' && c.course_id='$tutorIds[0]'")
//												 ->order(array("m.address_value asc"));exit; 			
														
						$j=0;
						if (isset($branchData) && sizeof($branchData)>0)
						{	
							foreach ($branchData as $branchs)
							{
								$location_data = $masteraddressobj->fetchRow("address_id ='$branchs->locality;'");
//								echo $location_data->address_value;
								if (isset($location_data) && sizeof($location_data)>0)
								{
									if ($j==0)
									{
										$localitytmp = $location_data->address_value;
									}else{
										$localitytmp = $localitytmp.", ".$location_data->address_value;
									}
									$j++;
								}
							}
													
							if (isset($localitytmp) && $localitytmp!="")
							{
								$this->view->multiplelocality = $localitytmp;
							}
						}
					}
							
						//------------------------------
					
					}
					
					$tutorIdSize = sizeof($tutorskillIds);
					$fetchIdStr = "";
					for($k=0;$k<$tutorIdSize;$k++){
					
					if($fetchIdStr=="")
						{
							$fetchIdStr = $tutorskillIds[$k];
						}else{
							$fetchIdStr = $fetchIdStr.",".$tutorskillIds[$k];
						}
					}
					$skillIDs = explode(",",$fetchIdStr);
					$sizeofskillIDs = sizeof($skillIDs);
					$skillObj = new Skillzot_Model_DbTable_Skills();
					for($j=0;$j<$sizeofskillIDs;$j++)
					{
						$sskilId = $skillIDs[$j];
						$SkillData = $skillObj->fetchRow("skill_id ='$sskilId' && skill_id!='0'");
						if (isset($SkillData) && sizeof($SkillData) > 0){
							$finalSkillname[$j] = $SkillData->skill_name;
						}
					}
					if (isset($finalSkillname) && sizeof($finalSkillname) > 0){
						$finalSkillname = array_unique($finalSkillname);
						$this->view->skillname = $finalSkillname;
					}
				}
				
				$experienceObj = new Skillzot_Model_DbTable_Tutorexperience();
				$expeduObj = new Skillzot_Model_DbTable_Expedutype();
				$Resultrow = $experienceObj->fetchAll($experienceObj->select()
				 							->setIntegrityCheck(false)
											->from(array('a'=>DATABASE_PREFIX."tx_tutor_experience"))
											->join(array('e'=>DATABASE_PREFIX."master_expedu_type"),'a.tutor_expedu_type = e.expedu_id',array('e.expedu_name'))
										    ->where("a.tutor_id='$tutorId' && e.expedu_id!='4' && e.expedu_id!='3' ")
										    ->order(array("a.tutor_expedu_type","a.id")));
										    
										    
										    
				if (isset($Resultrow) && sizeof($Resultrow)>0){
					$this->view->expeduname = $Resultrow;
				}
		if(isset($tutorId) && $tutorId!="")
			{
			 $videoResultdata = $tutorVideoObj->fetchAll($tutorVideoObj->select()
	                                                 ->setIntegrityCheck(false)
	     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_videos"))
	     											 ->where("v.tutor_id = '$tutorId'")
	     											 ->order(array("lastupdatedate DESC")));
	     											 
	     		if (isset($videoResultdata) && sizeof($videoResultdata) > 0)
	     		{
	     			$this->view->videoresult = $videoResultdata;
	     			
	     		}
			 $albumResultdata = $tutorAlbumObj->fetchAll($tutorAlbumObj->select()
	                                                 ->setIntegrityCheck(false)
	     											 ->from(array('a'=>DATABASE_PREFIX."tx_tutor_albums"))
	     											 ->joinLeft(array('p'=>DATABASE_PREFIX."tx_tutor_photos"),"p.tutor_album_id = a.id && p.cover_flag = 'y'",array('p.thumb_photo_link'))	     											 
	     											 ->where("a.tutor_id = '$tutorId' ")
	     											 ->order(array("lastupdatedate DESC")));	
	     		$albumid=array();	
	     		$albumflag=array();	
	     		$albumpic[][]=array();
	     		$k=0;
	     		$commondivalbum=0;		
	     		foreach($albumResultdata as $albwithpic)
				{						
							$tutor_album_id= $albwithpic->id;									
							$tutoralbumphotosRow = $tutorphotosObj->fetchRow("tutor_album_id ='$tutor_album_id'");							
							if(isset($tutoralbumphotosRow) && sizeof($tutoralbumphotosRow) > 0)	
	     					{
	     							$albumflag=1;
	     							$albumid=$tutoralbumphotosRow->tutor_album_id;	     						
		     							$albumpic[$k][0]=$albumid;
		     							$albumpic[$k][1]=$albumflag;
		     							$k++;
		     						$commondivalbum=1;
	     					}	     					
				} 					
				
					$this->view->albumpic = $albumpic;
					$this->view->commondivalbum = $commondivalbum;
							     											 
	     		if (isset($albumResultdata) && sizeof($albumResultdata) > 0)
	     		{
	     			$this->view->albumresult = $albumResultdata;		     						
	     		}
	     		$commondivphoto=0;		
			 $photoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
	                                                 ->setIntegrityCheck(false)
	     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
	     											 ->where("v.tutor_id = '$tutorId' && v.tutor_album_id='0'")
	     											 ->order(array("lastupdatedate DESC")));
	     		if (isset($photoResultdata) && sizeof($photoResultdata) > 0)
	     		{
	     			$this->view->photoresult = $photoResultdata;
	     			$commondivphoto=1;
	     		}
	     		$this->view->commondivphoto = $commondivphoto;
			}	
		}
		$date1=date('Y-m-d');
		$tutorcourseObj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$fetch_data = $tutorcourseObj->fetchRow("tutor_id='$tutorId'");					
	 	$courseid=$fetch_data->id;
	 	$age = explode(" ",$fetch_data->student_age_group);
		$min1="0";
		$max1="99";	
		$flag="0";
		for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="above" || $age[$start]=="onwards" || $age[$start]=="+" || $age[$start]=="more" || $age[$start]=="greater" || $age[$start]=="above.") && $flag=="0")
						{
							for ($start1=0; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
										$min1=$age[$start1];
										$max1="99";
										$flag="1";
								}
							}
						}	
		}
		for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="below" || $age[$start]=="less" || $age[$start]=="below.") && $flag=="0")
						{
							for ($start1=0; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
										$min1="0";
										$max1=$age[$start1];
										$flag="1";
								}
							}
						}			
		}
		for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="to" || $age[$start]=="-" || $age[$start]=="TO") && $flag=="0")
						{
							$end=$start;
							for ($start1=0; $start1 < $end; $start1++)
							{
								if(is_numeric($age[$start1]))
								{
									$min1=$age[$start1];
									$flag="1";
								}
							}
							for ($start1=$end; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
									$max1=$age[$start1];
									$flag="1";
								}
							}
						}			
		}
		for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="any" || $age[$start]=="all" || $age[$start]=="anyone") && $flag=="0")
						{
							$min1="0";
							$max1="99";		
							$flag="1";
						}			
		}
		$age2 = explode("+",$fetch_data->student_age_group);               //33+
		if(is_numeric($age2[0]) && $flag=="0")
		{
				$min1=$age2[0];
				$max1="99";
				$flag="1";
		}
		$age2 = explode("-",$fetch_data->student_age_group);                //22-66
		if(is_numeric($age2[0]) && is_numeric($age2[1]) && $flag=="0")
		{
				$min1=$age2[0];
				$max1=$age2[1];
				$flag="1";
		}
		if(is_numeric($age2[0]) && !is_numeric($age2[1]) && $flag=="0")      //22-66 years
		{
				$min1=$age2[0];
				$agespace = explode(" ",$age2[1]);
				if(is_numeric($agespace[0]))
				{ $max1=$agespace[0];} 
				$flag="1";
		}
		
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		
		if($courseid!="" && $courseid!="NULL")
		{
				$data1 = array("min"=>$min1,"max"=>$max1);
				$tutorcourseObj->update($data1,"id=$courseid");
				$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))	
										->where("c.tutor_id='$tutorId' && c.date='$date1' && c.course_id='$courseid'"));
		if (isset($resultData) && ($resultData > 0) )
		{
			$count=$resultData->profile_view_count;
			$coursecount=$resultData->course_count;
			$data1 = array("profile_view_count"=>$count+1,"course_count"=>$coursecount+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$tutorId' && date='$date1' && course_id='$courseid'");
		}
		else
		{
			$count="1";
			$data = array("date"=>$date1,"tutor_id"=>$tutorId,"course_id"=>$courseid,"profile_view_count"=>$count,"course_count"=>$count);
			$tutorAnalyticsobj->insert($data);
		}
		}
	}
public function fbcounterAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorcourseObj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		$TutorId = $this->_request->getParam("tutorid");
		$date1=date('Y-m-d');	
		$fetch_data = $tutorcourseObj->fetchRow("tutor_id='$TutorId'");
	 	$courseid=$fetch_data->id;
		if($courseid!="" && $courseid!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
										->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
		if (isset($resultData) && ($resultData > 0))
		{
			$count=$resultData->fb_click_count;
			$data1 = array("fb_click_count"=>$count+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
		}
		else
		{
			$count="1";
			$data = array("date"=>$date1,"tutor_id"=>$TutorId,"course_id"=>$courseid,"fb_click_count"=>$count);
			$tutorAnalyticsobj->insert($data);
		}
		}
		$fetch_data = $tutorProfileobj->fetchRow("id='$TutorId'");
		if (isset($fetch_data->tutor_fb_link) && $fetch_data->tutor_fb_link!="")
		{
			$this->_redirect($fetch_data->tutor_fb_link);
		}
}

public function twittercounterAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorcourseObj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		$TutorId = $this->_request->getParam("tutorid");
		$date1=date('Y-m-d');
		$fetch_data = $tutorcourseObj->fetchRow("tutor_id='$TutorId'");
	 	$courseid=$fetch_data->id;
		if($courseid!="" && $courseid!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
										->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
		if (isset($resultData) && ($resultData > 0))
		{
			$count=$resultData->twitter_click_count;
			$data1 = array("twitter_click_count"=>$count+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
		}
		else
		{
			$count="1";
			$data = array("date"=>$date1,"tutor_id"=>$TutorId,"course_id"=>$courseid,"twitter_click_count"=>$count);
			$tutorAnalyticsobj->insert($data);
		}
		}
		$fetch_data = $tutorProfileobj->fetchRow("id='$TutorId'");
		if (isset($fetch_data->tutor_tw_link) && $fetch_data->tutor_tw_link!="")
		{
			$this->_redirect($fetch_data->tutor_tw_link);
		}
}
public function videocounterAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$TutorId = $this->_request->getParam("tutorid");
		$VideoId = $this->_request->getParam("video_link");
		$date1=date('Y-m-d');
		$fetch_data = $tutorcourseObj->fetchRow("tutor_id='$TutorId'");
	 	$courseid=$fetch_data->id;
		if($courseid!="" && $courseid!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
										->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
		if (isset($resultData) && ($resultData > 0))
		{
			$count=$resultData->videos_click_count;
			$data1 = array("videos_click_count"=>$count+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
		}
		else
		{
			$count="1";
			$data = array("date"=>$date1,"tutor_id"=>$TutorId,"course_id"=>$courseid,"videos_click_count"=>$count);
			$tutorAnalyticsobj->insert($data);
		}
		}
		$fetch_data = $tutorProfileobj->fetchRow("id='$TutorId'");
		$count=$fetch_data->videos_click_count;		
		$data = array("videos_click_count"=>$count+1);
		$tutorProfileobj->update($data,"id='$TutorId'");
		$this->_redirect($VideoId);	
}
public function viewalbumphotosAction(){
			//$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			//if(!isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid==""){$this->_redirect('/');}

			$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
			$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
			//$tutor_album_id = $this->_request->getParam('tutor_album_id');
			
//			if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
//			{
//				$tutor_id = $authUserNamespace->maintutorid;
//				
//			}
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$tutor_album_id = $this->_request->getParam('tutor_album_id');
				$tutor_id = $this->_request->getParam('tutor_id');
				$returnResponse= array();
				
				$albumphotoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
	                                                 ->setIntegrityCheck(false)
	     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
	     											 ->where("v.tutor_id = '$tutor_id' && v.tutor_album_id = $tutor_album_id")
	     											 ->order(array("lastupdatedate DESC")));
	     		$photo_link="";
				foreach($albumphotoResultdata as $albumarray)
				{
	     			$photo_link=$albumarray->photo_link;
	     			$title=$albumarray->title;
	     			if($title=="")
	     			{
	     				$title="Photo Title";
	     			}
	     			$returnResponse[]=$photo_link.",".$title;
	     		}
	     		
	     		echo json_encode($returnResponse);exit;
			}
//			if(isset($tutor_id) && $tutor_id!="")
//			{
//				 $albumphotoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
//	                                                 ->setIntegrityCheck(false)
//	     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
//	     											 ->where("v.tutor_id = '$tutor_id' && v.tutor_album_id = $tutor_album_id")
//	     											 ->order(array("lastupdatedate DESC")));
//	     		if (isset($albumphotoResultdata) && sizeof($albumphotoResultdata) > 0)
//	     		{
//	     			$this->view->albumphotoresult = $albumphotoResultdata;
//	     			
//	     		}
//			}
//			if(isset($tutor_album_id) && $tutor_album_id!="")
//			{
//				$tutorAlbumRow = $tutorAlbumObj->fetchAll("id='$tutor_album_id'");
//				if (isset($tutorAlbumRow) && sizeof($tutorAlbumRow)>0)
//					{
//						foreach($tutorAlbumRow as $tar)
//						{							
//							$albumtitle = $tar->title;
//							$albumid= $tar->id;
//							$this->view->albumtitle = $albumtitle;			
//							$this->view->albumid = $albumid;							
//						}
//					}
//			}
	}	
	public function getoldvideoAction()
	{	
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		if(!isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid==""){$this->_redirect('/');}
			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
		
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
				$tutor_id = $authUserNamespace->maintutorid;	
		}
		
			$tutorProfilevideo = $tutorProfileobj->fetchAll($tutorProfileobj->select()
		                                   ->setIntegrityCheck(false)
		     							   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile")));	

				foreach($tutorProfilevideo as $tvds)
				{
						if(isset($tvds->tutor_vd_link) && $tvds->tutor_vd_link!="")
						{
							$lastupdatedate = date("Y-m-d H:i:s");	
								$data = array("tutor_id"=>$tvds->id,"video_link"=>$tvds->tutor_vd_link,
											  "lastupdatedate"=>$lastupdatedate);							
								$tutorVideoObj->insert($data);													
						}
						$tvds->tutor_vd_link1;
						if(isset($tvds->tutor_vd_link1) && $tvds->tutor_vd_link1!="")
						{
							$lastupdatedate = date("Y-m-d H:i:s");	
								$data1 = array("tutor_id"=>$tvds->id,"video_link"=>$tvds->tutor_vd_link1,"lastupdatedate"=>$lastupdatedate);
								$tutorVideoObj->insert($data1);							
								
						}
						$tvds->tutor_vd_link2;
						if(isset($tvds->tutor_vd_link2) && $tvds->tutor_vd_link2!="")
						{
							$lastupdatedate = date("Y-m-d H:i:s");	
								$data2 = array("tutor_id"=>$tvds->id,"video_link"=>$tvds->tutor_vd_link2,
											  "lastupdatedate"=>$lastupdatedate);
								$tutorVideoObj->insert($data2);
						}
						$tvds->tutor_vd_link3;
						if(isset($tvds->tutor_vd_link3) && $tvds->tutor_vd_link3!="")
						{
							$lastupdatedate = date("Y-m-d H:i:s");	
								$data3 = array("tutor_id"=>$tvds->id,"video_link"=>$tvds->tutor_vd_link3,
											  "lastupdatedate"=>$lastupdatedate);
								$tutorVideoObj->insert($data3);
						}
						$tvds->tutor_vd_link4;
						if(isset($tvds->tutor_vd_link4) && $tvds->tutor_vd_link4!="")
						{
							$lastupdatedate = date("Y-m-d H:i:s");	
								$data4 = array("tutor_id"=>$tvds->id,"video_link"=>$tvds->tutor_vd_link4,
											  "lastupdatedate"=>$lastupdatedate);
								$tutorVideoObj->insert($data4);
						}
						$tvds->tutor_vd_link5;
						if(isset($tvds->tutor_vd_link5) && $tvds->tutor_vd_link5!="")
						{
							$lastupdatedate = date("Y-m-d H:i:s");	
								$data5 = array("tutor_id"=>$tvds->id,"video_link"=>$tvds->tutor_vd_link5,
											  "lastupdatedate"=>$lastupdatedate);
								$tutorVideoObj->insert($data5);
						}					
				}
		
		echo " transfer of all old videos done";exit;
		
	}
	
	
	public function getcoursedetailsAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$courseId = $this->_request->getParam("courseid");
		$tutorId = $this->_request->getParam("tutor_id");
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$feetypeObj = new Skillzot_Model_DbTable_Tutorfeetype();
		$paycycleObj = new Skillzot_Model_DbTable_Tutorpaycycle();
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		$courseData = $tutorCourseobj->fetchRow("id ='$courseId'");
		$addresstable = new Skillzot_Model_DbTable_Address();						
		$address_row=$addresstable->fetchRow($addresstable->select()
					->from(array('c'=>DATABASE_PREFIX."master_address"),array('c.address_value as address_value'))
					->where("c.address_id='$batchname->locality'"));


		$batchObj= new Skillzot_Model_DbTable_Batch();
		$batchResult1=$batchObj->fetchRow($batchObj->select()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('GROUP_CONCAT(c.id) as id','GROUP_CONCAT(batch_summary_for_course,"@") as batch_summary_for_course','GROUP_CONCAT(c.tutor_location) as tutor_location','GROUP_CONCAT(c.tutor_batch_name) as tutor_batch_name','GROUP_CONCAT(c.tutor_batch_from_timing) as tutor_batch_from_timing','GROUP_CONCAT(c.tutor_batch_to_timing) as tutor_batch_to_timing','GROUP_CONCAT(c.tutor_class_dur_wks) as tutor_class_dur_wks','GROUP_CONCAT(c.tutor_class_dur_dayspwk) as tutor_class_dur_dayspwk','GROUP_CONCAT(c.tutor_class_dur_hrspday) as tutor_class_dur_hrspday','GROUP_CONCAT(c.batch_date) as batch_date'))
							->where("c.course_id ='$courseId'"));
		//$this->view->batchResult1 = $batchResult1;
		//$gradeArray = $batchResult1;
		$course_batchid = $batchResult1->id;
		//echo "id=".$course_batchid;
		$sizeOfbatch= sizeof($batchResult1);
		if(isset($batchResult1) && $sizeOfbatch > 0 ){

			$dataArray['batch_id']=$batchResult1->id;
			$dataArray['batch_summary_for_course']=$batchResult1->batch_summary_for_course;
			$dataArray['tutor_location']=$batchResult1->tutor_location;
			$dataArray['tutor_batch_name']=$batchResult1->tutor_batch_name;
			$dataArray['tutor_batch_from_timing']=$batchResult1->tutor_batch_from_timing;
			$dataArray['tutor_batch_to_timing']=$batchResult1->tutor_batch_to_timing;
			$dataArray['tutor_class_dur_wks']=$batchResult1->tutor_class_dur_wks;		
			$dataArray['tutor_class_dur_dayspwk']=$batchResult1->tutor_class_dur_dayspwk;
			$dataArray['tutor_class_dur_hrspday']=$batchResult1->tutor_class_dur_hrspday;
			$dataArray['batch_date']=$batchResult1->batch_date;		
		}

		//echo json_encode($dataArray);
		$date1=date('Y-m-d');
		if($courseId!="" && $courseId!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
										->where("c.tutor_id='$tutorId' && c.date='$date1' && c.course_id='$courseId'"));
		if (isset($resultData) && (sizeof($resultData) > 0))
		{
			$count=$resultData->course_count;
			$data1 = array("course_count"=>$count+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$tutorId' && date='$date1' && course_id='$courseId'");
		}
		else
		{
			$count="1";
			$data = array("date"=>$date1,"tutor_id"=>$tutorId,"course_id"=>$courseId,"course_count"=>$count);
			$tutorAnalyticsobj->insert($data);
		}
		}
		$sizeOfcourse = sizeof($courseData);
			if(isset($courseData) && $sizeOfcourse > 0 ){
					//$this->view->tutorlesionlocation =$courseData->tutor_lesson_location;
					
					$dataArray['courseid']=$courseData->id;
					$dataArray['tutorid']=$tutorId;
	       	  	  	if(isset($courseData->tutor_course_name) &&  $courseData->tutor_course_name!="")
	       	  	  	{
	       	  	  		$dataArray['coursename']=$courseData->tutor_course_name;
	       	  	  	}else{
	       	  	  		$dataArray['coursename'] = "-";
	       	  	  	}
	       	  	  	if(isset($courseData->tutor_course_desc) &&  $courseData->tutor_course_desc!="")
	       	  	  	{
	       	  	  		$dataArray['description']=$courseData->tutor_course_desc;
	       	  	  	}else{
	       	  	  		$dataArray['description']="-";
	       	  	  	}
	       	  	  	if(isset($courseData->student_age_group) &&  $courseData->student_age_group!="")
	       	  	  	{
	       	  	  			$dataArray['agegroup']=$courseData->student_age_group;
	       	  	  	}else{
	       	  	  			$dataArray['agegroup']="-";
	       	  	  	}
	       	  	  	if(isset($courseData->tutor_format) &&  $courseData->tutor_format!="")
	       	  	  	{
	       	  	  		$dataArray['tutorformat']=$courseData->tutor_format;
	       	  	  	}else{
	       	  	  		$dataArray['tutorformat']="-";
	       	  	  	}
	       	  	  	if(isset($courseData->tutor_rate) &&  $courseData->tutor_rate!="")
	       	  	  	{
	       	  	  		
	       	  	  		if (isset($courseData->tutor_pay_feetype) && $courseData->tutor_pay_feetype)
						{
							$feestypeObj1 = new Skillzot_Model_DbTable_Tutorfeetype();
							$fees_id = $courseData->tutor_pay_feetype;
							$tcourseData = $feestypeObj1->fetchRow("fee_type_id = $fees_id");
							$jj = " ".strtolower($tcourseData->fee_type_name);

						}else{
							$jj = "";
						}
					$dataArray['tutorrate']=$courseData->tutor_rate.$jj;
	       	  	  	}else{
	       	  	  		$dataArray['tutorrate']="-";
	       	  	  	}
						       	  	  	
					if(isset($courseData->tutor_canc_policy) &&  $courseData->tutor_canc_policy!="")
	       	  	  	{
	       	  	  		$dataArray['cancellationpolicy']=$courseData->tutor_canc_policy;
	       	  	  	}else{
	       	  	  		$dataArray['cancellationpolicy']="None";
	       	  	  	}
					if(isset($courseData->tutor_certificate_granted) &&  $courseData->tutor_certificate_granted!="")
	       	  	  	{
	       	  	  		$dataArray['reward']=$courseData->tutor_certificate_granted;
	       	  	  	}else{
	       	  	  		$dataArray['reward']="None";
	       	  	  	}
					if(isset($courseData->tutor_student_profile) &&  $courseData->tutor_student_profile!="")
	       	  	  	{
	       	  	  		
	       	  	  			$withcmmaIds = $removeLaststprofileId = substr($courseData->tutor_student_profile, 0, strlen($courseData->tutor_student_profile)-1);
							$stprofileIds = explode(",",$withcmmaIds);
							$profileTypeObj = new Skillzot_Model_DbTable_Studentprofiletype();
							for($i=0;$i<sizeof($stprofileIds);$i++)
							{
								$studentType = $profileTypeObj->fetchRow("profile_id =$stprofileIds[$i]");
								$studentTypeName[$i] = $studentType->profile_type;
							}
							if (sizeof($stprofileIds)==2)
							{
								$dataArray['prerequisits']=  $studentTypeName[0]." "."and"." ".$studentTypeName[1];
							}else if(sizeof($stprofileIds)==3){
								$dataArray['prerequisits'] =  $studentTypeName[0].","." ".$studentTypeName[1]." "."and"." ".$studentTypeName[2];
							}else{
								$dataArray['prerequisits'] =  $studentTypeName[0];
							}
	       	  	  		//$dataArray['prerequisits']=$courseData->tutor_student_profile;
	       	  	  		
	       	  	  	}else{
	       	  	  		$dataArray['prerequisits']="-";
	       	  	  	}
					if(isset($courseData->id) &&  $courseData->id!="")
	       	  	  	{
	       	  	  		$dataArray['id']=$courseData->id;
	       	  	  	}else{
	       	  	  		$dataArray['id']="";
	       	  	  	}
					if(isset($courseData->tutor_class_timing) &&  $courseData->tutor_class_timing!="")
	       	  	  	{
	       	  	  		$dataArray['timing']=$courseData->tutor_class_timing;
	       	  	  	}else{
	       	  	  		$dataArray['timing']="-";
	       	  	  	}
					if(isset($courseData->tutor_class_batch_schedule) &&  $courseData->tutor_class_batch_schedule!="")
	       	  	  	{
	       	  	  		$dataArray['shedule']=$courseData->tutor_class_batch_schedule;
	       	  	  	}else{
	       	  	  		$dataArray['shedule']="-";
	       	  	  	}
					if(isset($courseData->tutor_class_dur_wks) &&  $courseData->tutor_class_dur_wks!="")
	       	  	  	{
	       	  	  		if ($courseData->tutor_class_dur_wks=="1")
	       	  	  		{
	       	  	  			$dataArray['weeks']=$courseData->tutor_class_dur_wks." "."week";
	       	  	  		}elseif($courseData->tutor_class_dur_wks=='0')
						{$dataArray['weeks']="Recurring monthly";}
						else{
	       	  	  			$dataArray['weeks']=$courseData->tutor_class_dur_wks." "."weeks";
	       	  	  		}
	       	  	  		
	       	  	  	}else{
	       	  	  		$dataArray['weeks']="-";
	       	  	  	}
					if(isset($courseData->tutor_class_dur_dayspwk) &&  $courseData->tutor_class_dur_dayspwk!="")
	       	  	  	{
	       	  	  		if ($courseData->tutor_class_dur_dayspwk=="1")
	       	  	  		{
	       	  	  			$dataArray['days']=$courseData->tutor_class_dur_dayspwk." "."day a week";
	       	  	  		}elseif($courseData->tutor_class_dur_dayspwk=='0')
						{$dataArray['days'] = "Days/Week - Flexible";}
						else{
	       	  	  			$dataArray['days']=$courseData->tutor_class_dur_dayspwk." "."days a week";
	       	  	  		}
	       	  	  		
	       	  	  	}else{
	       	  	  		$dataArray['days']="-";
	       	  	  	}
					if(isset($courseData->tutor_class_dur_hrspday) &&  $courseData->tutor_class_dur_hrspday!="")
	       	  	  	{
	       	  	  		if ($courseData->tutor_class_dur_hrspday=="1")
	       	  	  		{
	       	  	  			$dataArray['hours']=$courseData->tutor_class_dur_hrspday." "."hour a day";
	       	  	  		}elseif($courseData->tutor_class_dur_hrspday=='0')
							{$dataArray['days'] = "Hours/Day - Flexible";}
						else{
		       	  	  		$dataArray['hours']=$courseData->tutor_class_dur_hrspday." "."hours a day";
	       	  	  		}
	       	  	  	}else{
	       	  	  		$dataArray['hours']="-";
	       	  	  	}
	       	  	  	
					if(isset($courseData->tutor_batch_week) &&  $courseData->tutor_batch_week!="")
	       	  	  	{
	       	  	  		$dataArray['daysinweek']=$courseData->tutor_batch_week;
	       	  	  	}else{
	       	  	  		$dataArray['daysinweek']="-";
	       	  	  	}

					if(isset($courseData->tutor_batch_from_timing) &&  $courseData->tutor_batch_from_timing!="" && isset($courseData->tutor_batch_to_timing) &&  $courseData->tutor_batch_to_timing!="")
	       	  	  	{
	       	  	  		$dataArray['timings']=$courseData->tutor_batch_from_timing." "."to"." ".$courseData->tutor_batch_to_timing;
	       	  	  	}else{
	       	  	  		$dataArray['timings']="-";
	       	  	  	}
	       	  	  	
	       	  	  		if(isset($courseData->tutor_rate) &&  $courseData->tutor_rate!="")
	       	  	  		{
			       	  	   	$paycycleId = $courseData->tutor_pay_cycle ;
							$paytypeData = $paycycleObj->fetchRow("pay_cycle_id ='$paycycleId'");
							if (isset($paytypeData) && sizeof($paytypeData)>0){
									$dataArray['paymentpolicy']=$paytypeData->pay_cycle_name;
							}else{
								$dataArray['paymentpolicy']= "-";
							}
	       	  	  		}
	       	  	  		
	       	  	  		
					if(isset($courseData->id) &&  $courseData->id!="")
	       	  	  	{
	       	  	  		$dataArray['b_id']=$courseData->id;
	       	  	  	}else{
	       	  	  		$dataArray['b_id']="-";
	       	  	  	}
					if(isset($courseData->tutor_id) &&  $courseData->tutor_id!="")
	       	  	  	{
	       	  	  		$dataArray['tutor_id']=$courseData->tutor_id;
	       	  	  	}else{
	       	  	  		$dataArray['tutor_id']="-";
	       	  	  	}
	       	  	  	
					if(isset($courseData->tutor_batch_status) &&  $courseData->tutor_batch_status!="")
	       	  	  	{
	       	  	  		$dataArray['tutor_status']=$courseData->tutor_batch_status;
	       	  	  	}else{
	       	  	  		$dataArray['tutor_status']="-";
	       	  	  	}
	       	  	  	
					$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
					$batchdetails_data = $batchdetailObj->fetchRow("tutor_id ='$tutorId' && tutor_course_id='$courseId' && b_description!=''");
					if (isset($batchdetails_data) && sizeof($batchdetails_data)>0)
					{
						$dataArray['flag_to_display_cal']="1";
					}else{
						$dataArray['flag_to_display_cal']="0";
					}
						
					if(isset($courseData->tutor_lesson_location) &&  $courseData->tutor_lesson_location!="")
	       	  	  	{
		       	  	  	$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
						$lessonlocationResult = $lessonlocationObj->fetchRow("location_id ='$courseData->tutor_lesson_location'");
						if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
						{
							if ($lessonlocationResult->location_id=='3')
							{
								$dataArray['tutor_travel_radious']= "Teacher's location & open to traveling";
							}else{
								$dataArray['tutor_travel_radious']= $lessonlocationResult->location_name;
							}
							
						}
						if(isset($courseData->travel_radius) &&  $courseData->travel_radius!="")
	       	  	  		{
	       	  	  			if ($lessonlocationResult->location_id=='3')
							{
								$temp_v ="Teacher's location & open to traveling"."<br/>"."(".$courseData->travel_radius.")";
	       	  	  				$dataArray['tutor_travel_radious']= $temp_v;
							}else{
								$temp_v = $lessonlocationResult->location_name."<br/>"."(".$courseData->travel_radius.")";
	       	  	  				$dataArray['tutor_travel_radious']= $temp_v;
							}
	       	  	  			
	       	  	  		}
	       	  	  	}else{
	       	  	  		$dataArray['tutor_travel_radious']="None";
	       	  	  	}
	       	  	  	
	       	  	  	
	       	  	  	//------locality display-------
	       	  	  		$masteraddressobj = new Skillzot_Model_DbTable_Address();
						$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
//						$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
//														->from(array('b'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
//														->where("b.course_id='$courseData->id'"));
						$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
											->setIntegrityCheck(false)
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('distinct(locality)'))
											->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
											->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
											->where("c.tutor_id='$courseData->tutor_id' && c.course_id='$courseData->id'")
											 ->order(array("m.address_value asc")));
//											print 		$branchdetailObj->select()
//											->setIntegrityCheck(false)
//											->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
//											->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
//											->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
//											->where("c.tutor_id='$courseData->tutor_id' && c.course_id='$courseData->id'")
//											 ->order(array("m.address_value asc"));exit;	
						$j=0;
						if (isset($branchData) && sizeof($branchData)>0)
						{	
							foreach ($branchData as $branchs)
							{
								$location_data = $masteraddressobj->fetchRow("address_id ='$branchs->locality;'");
//								echo $location_data->address_value;
								if (isset($location_data) && sizeof($location_data)>0)
								{
									if ($j==0)
									{
										$localitytmp = $location_data->address_value;
									}else{
										$localitytmp = $localitytmp.", ".$location_data->address_value;
									}
									$j++;
								}
							}
						}
						if (isset($localitytmp) && $localitytmp!="")
							{
								$dataArray['localitymultiple']= $localitytmp;
							}else{
								$dataArray['localitymultiple']= "None";
							}
						//------------------------------	       	  	  	
	       	  	  	
					if(isset($courseData->rate_comments) &&  $courseData->rate_comments!="Comments if any...")
	       	  	  	{
	       	  	  		$dataArray['rate_comments']=$courseData->rate_comments;
	       	  	  	}else{
	       	  	  		$dataArray['rate_comments']="-";
	       	  	  	}
					if(isset($courseData->tutor_student_comments) &&  $courseData->tutor_student_comments!="")
	       	  	  	{
	       	  	  		$dataArray['student_comments']=$courseData->tutor_student_comments;
	       	  	  	}else{
	       	  	  		$dataArray['student_comments']="-";
	       	  	  	}
	       	  	  	
	       	  	  	
					if(isset($courseData->tutor_class_image_type) &&  $courseData->tutor_class_image_type!="")
	       	  	  	{
	       	  	  		$dataArray['classimage']=$courseData->tutor_class_image_type;
	       	  	  	}else{
	       	  	  		$dataArray['classimage']="-";
	       	  	  	}
	       	  	  if(isset($courseData->tutor_lesson_location) && $courseData->tutor_lesson_location!="")
	       	  	  {
	       	  	  	$dataArray['tutorlesionlocation']=$courseData->tutor_lesson_location;
	       	  	  }else
	       	  	  {
	       	  	  	$dataArray['tutorlesionlocation']="";
	       	  	  }
				}
				
		echo json_encode($dataArray);
		
	}
	
	public function viewimageAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$img_id=$this->_request->getParam("id");
	 	$view_img = $tutorCourseobj->fetchRow("id=$img_id");
	 	if (isset($view_img->tutor_class_image_type) && $view_img->tutor_class_image_type!="" ){
	 		print $content = $view_img->tutor_class_image_content;exit;
	 	}
	}

	public function getlocalityAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$adresstypeObj = new Skillzot_Model_DbTable_Addresstype();
		$adressObj = new Skillzot_Model_DbTable_Address();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$cityid = $this->_request->getParam("city_id");
				$address_rows = $adressObj->fetchAll($adressObj->select()
								  ->from(array('s'=>DATABASE_PREFIX."master_address"))
								  ->where("s.address_id!='0' && s.parent_address_id='$cityid'")
								  ->order(array("s.address_value asc")));
				//print_r($address_rows);exit;
				$i=0;
				$tags = "";
				foreach($address_rows as $a){
					$tags[$i]['address_name'] = $a->address_value;
					$tags[$i]['locality_id'] = $a->address_id;
					//$arry[]=$tags;
					$i++;
				}
				echo json_encode($tags);
				}
	  		}
	}
public function whysignupAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->layout()->setLayout("lightbox");
	}
public function videoplayAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->layout()->setLayout("lightbox");	
	}
public function writereviewAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->layout()->setLayout("lightbox");
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorid = $this->_request->getParam("tutorid");
		//$tutorId= 51;
		$skillIdfromprofilepage = $this->_request->getParam("skillid");
		$this->view->skillidfromprofilepage = $skillIdfromprofilepage;//for displaying selected
		$reviewObj = new Skillzot_Model_DbTable_Review();
		if (isset($authUserNamespace->t_id_for_review) && $authUserNamespace->t_id_for_review!="")
		{
			$t_id_review = $authUserNamespace->t_id_for_review;
			//unset($authUserNamespace->t_id_for_review);
			
			$tutoridsRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('t.tutor_first_name'))
																	->where("id='$t_id_review'"));
			$tutor_f_name = $tutoridsRows[0]['tutor_first_name'];
			if (isset($tutor_f_name) && $tutor_f_name!="")
			{
				//echo "sdf";exit;
				$this->view->tutorname = $tutor_f_name;
			}
			
		}
		if(isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!=""){
			$loginId = $authUserNamespace->maintutorid;
		}else if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
			$loginId = $authUserNamespace->studentid;
		}
//		if (isset($loginId) && $loginId!=""){
//			if(isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!=""){
//				$reviewResults = $reviewObj->fetchRow("skill_id='$skillIdfromprofilepage' && from_id='$loginId' && tutor_id='$tutorid' && person='t'");
//			}else if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
//				$reviewResults = $reviewObj->fetchRow("skill_id='$skillIdfromprofilepage' && from_id='$loginId' && tutor_id='$tutorid' && person='s'");
//			}
//			
//		}
//		if (isset($reviewResults) && sizeof($reviewResults)>0){
//			$this->view->reviewresult = $reviewResults;
//		}
		if($this->_request->isPost()){
			
				$tutorRating = $this->_request->getParam("tutor_rating");
				$recomendedName = $this->_request->getParam("recomendname_val");
				$comments = $this->_request->getParam("User_Comments");
				$updateId = $this->_request->getParam("updateID");
				$skillname = $this->_request->getParam("skillname");
				//echo $skillname;exit;
				$skillobj = new Skillzot_Model_DbTable_Skills();
				$skillid = $skillobj->fetchRow("skill_name = '$skillname' && is_enabled='1'");
			
				if (isset($skillid) && sizeof($skillid)>0)
				{
					if (isset($loginId) && $loginId!=""){
						if(isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!=""){
							$reviewResults1 = $reviewObj->fetchRow("skill_id='$skillid->skill_id' && from_id='$loginId' && tutor_id='$tutorid' && person='t'");
						}else if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
							$reviewResults1 = $reviewObj->fetchRow("skill_id='$skillid->skill_id' && from_id='$loginId' && tutor_id='$tutorid' && person='s'");
						}
						
					}
				}
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
						
						if($skillname == "")$response["data"]["skillname"] = "null";
						else $response["data"]["skillname"] = "valid";
						
						if($comments == "")$response["data"]["User_Comments"] = "null";
						else $response["data"]["User_Comments"] = "valid";
						
						if(!in_array('null',$response['data']) && !in_array('selectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						echo json_encode($response);
					}
					else {
						$lastupdatedate = date("Y-m-d H:i:s");
						
					if (isset($tutorid) && $tutorid!=""){
						if (isset($reviewResults1) && sizeof($reviewResults1)>0)
						{
								$updateId = $reviewResults1->id;
								if(isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
								{
									$person = "t";
									$data = array("person"=>$person,"skill_id"=>$skillid->skill_id,"parent_skill_id"=>$skillid->parent_skill_id,"skillgrade"=>$tutorRating,"comment"=>$comments);
									$reviewObj->update($data,"from_id='$authUserNamespace->maintutorid' && id='$updateId'");
								}else if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
									$person = "s";
									$data = array("person"=>$person,"skill_id"=>$skillid->skill_id,"parent_skill_id"=>$skillid->parent_skill_id,"skillgrade"=>$tutorRating,"comment"=>$comments);
									$reviewObj->update($data,"from_id='$authUserNamespace->studentid' && id='$updateId'");
								}
								//echo "<script>parent.Mediabox.close();</script>";
								
						}else{
						
							if(isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
							{
								$person = "t";
								$data = array("person"=>$person,"skill_id"=>$skillid->skill_id ,"parent_skill_id"=>$skillid->parent_skill_id,"from_id"=>$authUserNamespace->maintutorid,"tutor_id"=>$tutorid,"skillgrade"=>$tutorRating,"comment"=>$comments,"lastupdatedate"=>$lastupdatedate);
							}else if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
								$person = "s";
								$data = array("person"=>$person,"skill_id"=>$skillid->skill_id ,"parent_skill_id"=>$skillid->parent_skill_id,"from_id"=>$authUserNamespace->studentid,"tutor_id"=>$tutorid,"skillgrade"=>$tutorRating,"comment"=>$comments,"lastupdatedate"=>$lastupdatedate);
							}
							//print_r($data);exit;
							$reviewObj->insert($data);
							//echo "<script>parent.Mediabox.close();</script>";
						}
					}
						
					/* recometer logic*/
					
					if (isset($tutorid) && $tutorid!=""){
								
						$rateHigh = $reviewObj->fetchAll($reviewObj->select()
														->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skill_id','skillgrade','person','id','from_id','comment'))
														->where("r.tutor_id='$tutorid' && r.skill_id='$skillid->skill_id'  && (skillgrade='9' || skillgrade='10')"));
						$rateLow = $reviewObj->fetchAll($reviewObj->select()
														->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skill_id','skillgrade','person','id','from_id','comment'))
														->where("r.tutor_id='$tutorid' && r.skill_id='$skillid->skill_id'  && (skillgrade='1' || skillgrade='2' || skillgrade='3' || skillgrade='4' || skillgrade='5' || skillgrade='6')"));
						$high=sizeof($rateHigh);
					    $low=sizeof($rateLow);
					    $recofortutor=$high-$low;																
						$reco_totaltutor = $reviewObj->fetchAll($reviewObj->select()
														->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skill_id','skillgrade','person','id','from_id','comment'))
														->where("r.tutor_id='$tutorid' && r.skill_id='$skillid->skill_id'"));	
																		
						if(isset($recofortutor) && $recofortutor>0){
							$num_tutor_recomen = $recofortutor;

						}else{
							$num_tutor_recomen = 0;
						}
						if(isset($reco_totaltutor) && sizeof($reco_totaltutor)>0){
							$total_recomended = sizeof($reco_totaltutor);//total for particular skill
							$recoScore = $num_tutor_recomen/$total_recomended;
							$recoScore = round($recoScore, 2);
							$recoScore = $recoScore * 100;
							$data1 = array("recograde"=>$recoScore);
							//print_r($data1);
							$reviewObj->update($data1,"tutor_id='$tutorid' && skill_id='$skillid->skill_id'");
						}else{
							//echo "out";
							$recoScore="0.00";
							$data1 = array("recograde"=>$recoScore);
							$reviewObj->update($data1,"tutor_id='$tutorid' && skill_id='$skillid->skill_id'");
						}
						//exit;
						$reco_tutor_rows = $reviewObj->fetchAll($reviewObj->select()
														->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('average'=>'avg(r.skillgrade)'))
														->where("r.tutor_id='$tutorid' && r.skill_id='$skillid->skill_id'"));
														
						if (isset($reco_tutor_rows) && sizeof($reco_tutor_rows)>0){
							$avgValue=$reco_tutor_rows[0]["average"];
							$data2 = array("gradeavg"=>$avgValue);
							$reviewObj->update($data2,"tutor_id='$tutorid'");
						}
						 
														
					}
						echo "<script>parent.window.location.reload(); </script>";
							//echo "<script>window.parent.location='". BASEPATH ."/tutor/profile/tutorid/".$tutorid."/skillid/".$skillid->skill_id."'</script>";
					}
			}
		$feetypeObj = new Skillzot_Model_DbTable_Tutorfeetype();
		$paycycleObj = new Skillzot_Model_DbTable_Tutorpaycycle();
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		
		
		
		
        
		//----tutor signup session unset ----
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			unset($authUserNamespace->tutorsessionid);
			unset($authUserNamespace->tutorcoursesessionid);
			unset($authUserNamespace->formname1);
	    	unset($authUserNamespace->formname2);
	    	unset($authUserNamespace->formname3);
	    	unset($authUserNamespace->classtype);
	    	unset($authUserNamespace->tutoremail);
		}
    	//echo $authUserNamespace->maincategory;exit;
    	//----------------------------------------
    	if (isset($authUserNamespace->skillidnew) && $authUserNamespace->skillidnew!='')
    	{
    		$fetchSkill_id = $authUserNamespace->skillidnew;
    		//unset($authUserNamespace->skillidnew);
    	}
    	if (isset($fetchSkill_id) && $fetchSkill_id!="")
    	{
			$skillndata = $skillobj->fetchRow("skill_id='$fetchSkill_id' && skill_id!=0");
    	}
			if (isset($skillndata) && sizeof($skillndata)>0 )
				{
					$skillCategory = $skillndata->skill_name;
					$this->view->skillcategory = $skillCategory;
					$this->view->skillid = $skillndata->skill_id;
					$parentId = $skillndata->parent_skill_id;
					$skillDepth = $skillndata->skill_depth;
				}
		
			if (isset($skillDepth) && $skillDepth > 1)
			{
				$parentdata = $skillobj->fetchRow("skill_id='$parentId' && skill_id!=0");
				$submainCategory = $parentdata->skill_name;
				if (isset($submainCategory) && $submainCategory!="")
					{
						$this->view->mainsubmaincategory = $submainCategory;
						$this->view->subcategoryid = $parentdata->skill_id;
					}
				$seccondParentID = $parentdata->parent_skill_id;
				$secondskillDepth = $parentdata->skill_depth;
				if (isset($secondskillDepth) && $secondskillDepth > 1)
				{
					$finaldata = $skillobj->fetchRow("skill_id='$seccondParentID' && skill_id!=0");
					$mainCategory = $finaldata->skill_name;
					//$mainCategoryId = $finaldata->skill_id;
					if (isset($mainCategory) && $mainCategory!="")
					{
						$this->view->maincategory = $mainCategory;
						$this->view->maincategoryid = $finaldata->skill_id;
						//$this->view->maincategoryid = $mainCategoryId;
					}
				}
			}
		
		
		//------------------
		
		$tutorId = $this->_request->getParam("tutorid");
		$pageValue = $this->_request->getParam("page");
		if (isset($pageValue) && $pageValue!="")
		{
			$pageObj = new Skillzot_Model_DbTable_Pagesession();
			$setval2 = $pageValue. time().mt_rand();
			$mdset5 =  md5($setval2);
			$data = array("sessionnumber"=>$mdset5,"pagevalue"=>$pageValue);
			$pageObj->insert($data);
			$authUserNamespace->pagevalue = $mdset5;
			//$authUserNamespace->profileloginsessiondifferenceflag = 1; 
			
		}
		if (isset($tutorId) && $tutorId!="")
		{
			$this->view->tutorid = $tutorId;
			$authUserNamespace->t_id_for_review = $tutorId;
			$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
			$batchdetails_data = $batchdetailObj->fetchRow("tutor_id ='$tutorId' && b_description!=''");
			if (isset($batchdetails_data) && sizeof($batchdetails_data)>0)
			{
				$this->view->batch_t_id = $tutorId;
			}
			/* recometer logic*/
			$rateHigh = $tutorreviewobj->fetchAll($tutorreviewobj->select()
											->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
											->where("r.tutor_id='$tutorId' && (skillgrade='9' || skillgrade='10')"));
			$rateLow = $tutorreviewobj->fetchAll($tutorreviewobj->select()
											->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
											->where("r.tutor_id='$tutorId' && (skillgrade='1' || skillgrade='2' || skillgrade='3' || skillgrade='4' || skillgrade='5' || skillgrade='6')"));
			$high=sizeof($rateHigh);
			$low=sizeof($rateLow);
			$recofortutor=$high-$low;									
			$reco_totaltutor = $tutorreviewobj->fetchAll($tutorreviewobj->select()
											->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment','lastupdatedate'))
											->where("r.tutor_id='$tutorId'")
											->order(array("r.id desc")));			
			if(isset($recofortutor) && $recofortutor>0){
				$this->view->recofortutor = $recofortutor;
			}else{
				$this->view->recofortutor = 0;
			}
			if(isset($reco_totaltutor) && sizeof($reco_totaltutor)>0){
				$this->view->reco_totaltutor = sizeof($reco_totaltutor);//all review for particular tutor
				$this->view->reco_data_all = $reco_totaltutor;
			}
			
		}
		$tutorProfileData =$tutorProfileobj->fetchRow("id='$tutorId'");
		
		if (isset($tutorProfileData) && sizeof($tutorProfileData)>0)
		{
			$this->view->tutordata = $tutorProfileData;
			$authUserNamespace->userclasstype = $tutorProfileData->tutor_class_type;
			$masteraddressobj = new Skillzot_Model_DbTable_Address();
			if (isset($tutorProfileData->tutor_add_locality) && $tutorProfileData->tutor_add_locality!=""){
				$locationId = $tutorProfileData->tutor_add_locality;
				$cityId =$tutorProfileData->tutor_add_city;
				$location_data = $masteraddressobj->fetchRow("address_id ='$locationId'");
				$city_data = $masteraddressobj->fetchRow("address_id ='$cityId'");
				$this->view->citydata = $city_data;
				$this->view->location = $location_data;
			}
			$class_id = $tutorProfileData->tutor_class_type;
			$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
			$ClassData = $classtypeObj->fetchRow("class_type_id ='$class_id'");
			if(isset($ClassData) && sizeOf($ClassData)>0){
				$this->view->classtype_id = $ClassData->class_type_id;
				if($ClassData->class_type_id!="" && $ClassData->class_type_id == 4){
					$this->view->classtype = 'Group classes';
				}else{
					$this->view->classtype = $ClassData->class_type_name;
				}
			}
			$this->view->phonevisible = $tutorProfileData->phone_visible;
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$courseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"))
										->where("c.tutor_id='$tutorId'"));
			//print_r($courseData);exit;
			if (isset($courseData) && sizeof($courseData)>0)
			{
					$this->view->coursedata = $courseData ;
					$i = 0;
					foreach ($courseData as $course){
					 	if ((isset($course->tutor_skill_id) && $course->tutor_skill_id!="")||(isset($course->tutor_course_name) && $course->tutor_course_name!="")||(isset($course->id) && $course->id!="")){
							$tutorskillIds[$i] = $course->tutor_skill_id;
							$courseName[$i] = $course->tutor_course_name;
							$tutorIds[$i] = $course->id;
							$i++;
					 	}else{
						
						$tutorskillIds[] = '';
							$courseName[] = '';
							$tutorIds[] = '';
						}
					}
					//----temperary for one id--------------------
					
					//print_r($tutorIds);exit;
					$tMainId = $tutorIds[0];
					//echo "sd".$tMainId;exit;
					$coursesubData = $tutorCourseobj->fetchRow("id ='$tMainId'");
					//print_r($coursesubData);exit;
					
					
					
					
					if (isset($coursesubData) && sizeof($coursesubData)>0){
						$this->view->coursesubdata = $coursesubData;
						$paycycleId = $coursesubData->tutor_pay_cycle ;
					
						$paytypeData = $paycycleObj->fetchRow("pay_cycle_id ='$paycycleId'");
						if (isset($paytypeData) && sizeof($paytypeData)>0){
							$this->view->paydatatype = $paytypeData->pay_cycle_name;
						}
						
						$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
						$masteraddressobj = new Skillzot_Model_DbTable_Address();
						$lessonlocationResult = $lessonlocationObj->fetchRow("location_id ='$coursesubData->tutor_lesson_location'");
						if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
						{
							if ($lessonlocationResult->location_id=='3')
							{
								$this->view->tutorlessonlocation = "Teacher's location & open to traveling";
							}else{
								$this->view->tutorlessonlocation = $lessonlocationResult->location_name;
							}
							
						}
						//------locality display-------
						$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
//						$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
//														->from(array('b'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
//														->where("b.course_id='$coursesubData->id'"));
						
							$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
												->setIntegrityCheck(false)
												->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
												->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
												->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
												->where("c.tutor_id='$tutorId'")
												 ->order(array("m.address_value asc")));
														
														
						$j=0;
						if (isset($branchData) && sizeof($branchData)>0)
						{	
							foreach ($branchData as $branchs)
							{
								$location_data = $masteraddressobj->fetchRow("address_id ='$branchs->locality;'");
//								echo $location_data->address_value;
								if (isset($location_data) && sizeof($location_data)>0)
								{
									if ($j==0)
									{
										$localitytmp = $location_data->address_value;
									}else{
										$localitytmp = $localitytmp.", ".$location_data->address_value;
									}
									$j++;
								}
							}
													
							if (isset($localitytmp) && $localitytmp!="")
							{
								$this->view->multiplelocality = $localitytmp;
							}
						}
						//------------------------------
					
					}
					
					$tutorIdSize = sizeof($tutorskillIds);
					$fetchIdStr = "";
					for($k=0;$k<$tutorIdSize;$k++){
					
					if($fetchIdStr=="")
						{
							$fetchIdStr = $tutorskillIds[$k];
						}else{
							$fetchIdStr = $fetchIdStr.",".$tutorskillIds[$k];
						}
					}
					$skillIDs = explode(",",$fetchIdStr);
					$sizeofskillIDs = sizeof($skillIDs);
					$skillObj = new Skillzot_Model_DbTable_Skills();
					for($j=0;$j<$sizeofskillIDs;$j++)
					{
						$sskilId = $skillIDs[$j];
						$SkillData = $skillObj->fetchRow("skill_id ='$sskilId' && skill_id!='0'");
						if (isset($SkillData) && sizeof($SkillData) > 0){
							$finalSkillname[$j] = $SkillData->skill_name;
						}
					}
					if (isset($finalSkillname) && sizeof($finalSkillname) > 0){
						$finalSkillname = array_unique($finalSkillname);
						$this->view->skillname = $finalSkillname;
					}
				}
				
				$experienceObj = new Skillzot_Model_DbTable_Tutorexperience();
				$expeduObj = new Skillzot_Model_DbTable_Expedutype();
				$Resultrow = $experienceObj->fetchAll($experienceObj->select()
				 							->setIntegrityCheck(false)
											->from(array('a'=>DATABASE_PREFIX."tx_tutor_experience"))
											->join(array('e'=>DATABASE_PREFIX."master_expedu_type"),'a.tutor_expedu_type = e.expedu_id',array('e.expedu_name'))
										    ->where("a.tutor_id='$tutorId' && e.expedu_id!='4' && e.expedu_id!='3' ")
										    ->order(array("a.tutor_expedu_type","a.id")));
										    
										    
										    
				if (isset($Resultrow) && sizeof($Resultrow)>0){
					$this->view->expeduname = $Resultrow;
				}
		}
			
		
}
public function termsAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	//$this->_helper->layout()->setLayout('staticlayout');
	$this->_helper->layout()->setLayout('innerpage');
	$course_id = $this->_request->getParam("course_id");
	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	$courseData = $tutorCourseobj->fetchRow("id ='$course_id'");
	$this->view->courseData=$courseData;

}
public function paymentstep1Action()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");	
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		$feetypeObj = new Skillzot_Model_DbTable_Tutorfeetype();
		$paycycleObj = new Skillzot_Model_DbTable_Tutorpaycycle();
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
			
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			unset($authUserNamespace->tutorsessionid);
			unset($authUserNamespace->tutorcoursesessionid);
			unset($authUserNamespace->formname1);
	    	unset($authUserNamespace->formname2);
	    	unset($authUserNamespace->formname3);
	    	unset($authUserNamespace->classtype);
	    	unset($authUserNamespace->tutoremail);
		}
    	//echo $authUserNamespace->maincategory;exit;
    	//----------------------------------------
    	if (isset($authUserNamespace->skillidnew) && $authUserNamespace->skillidnew!='')
    	{
    		$fetchSkill_id = $authUserNamespace->skillidnew;
    		//unset($authUserNamespace->skillidnew);
    	}
    	if (isset($fetchSkill_id) && $fetchSkill_id!="")
    	{
			$skillndata = $skillobj->fetchRow("skill_id='$fetchSkill_id' && skill_id!=0");
    	}
			if (isset($skillndata) && sizeof($skillndata)>0 )
				{
					$skillCategory = $skillndata->skill_name;
					$this->view->skillcategory = $skillCategory;
					$this->view->skillid = $skillndata->skill_id;
					$parentId = $skillndata->parent_skill_id;
					$skillDepth = $skillndata->skill_depth;
				}
		
			if (isset($skillDepth) && $skillDepth > 1)
			{
				$parentdata = $skillobj->fetchRow("skill_id='$parentId' && skill_id!=0");
				$submainCategory = $parentdata->skill_name;
				if (isset($submainCategory) && $submainCategory!="")
				{
						$this->view->mainsubmaincategory = $submainCategory;
						$this->view->subcategoryid = $parentdata->skill_id;
				}
				$seccondParentID = $parentdata->parent_skill_id;
				$secondskillDepth = $parentdata->skill_depth;
				if (isset($secondskillDepth) && $secondskillDepth > 1)
				{
					$finaldata = $skillobj->fetchRow("skill_id='$seccondParentID' && skill_id!=0");
					$mainCategory = $finaldata->skill_name;
					//$mainCategoryId = $finaldata->skill_id;
					if (isset($mainCategory) && $mainCategory!="")
					{
						$this->view->maincategory = $mainCategory;
						$this->view->maincategoryid = $finaldata->skill_id;
						//$this->view->maincategoryid = $mainCategoryId;
					}
				}
			}
			
		//------------------
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		
		//$tutorId = $this->_request->getParam("tutorid");
		$batchId = $this->_request->getParam("batchid");
		$batchObj= new Skillzot_Model_DbTable_Batch();
		$batchviewResult=$batchObj->fetchRow($batchObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.id ='$batchId'"));

		if (isset($batchviewResult) && sizeof($batchviewResult) > 0) 
			$this->view->batchviewResult = $batchviewResult;
			
			
		if ($batchviewResult->tutor_location=='1,2')
			{
				$this->view->batchviewResult_loc = "Teacher's location & open to traveling";
			}elseif($batchviewResult->tutor_location=='1,'){
				$this->view->batchviewResult_loc = "";
			}else
			{
				$this->view->batchviewResult_loc = "At teacher's location";
			}
		$tutorId=$batchviewResult->tutor_id;
		$courseid= $this->_request->getParam("courseid");
		$pageValue = $this->_request->getParam("page");
		$authUserNamespace->pageValue=$pageValue;
		$authUserNamespace->ptutorid=$tutorId;
		$authUserNamespace->pbatchid=$batchId;
		$authUserNamespace->pcourseid=$courseid;
		$authUserNamespace->pcourse_id=$batchviewResult->course_id;
		
		if($this->_request->isPost())
		{
			$batch_available = $this->_request->getParam("batch_avail");
			$noofstudents = $this->_request->getParam("noofstudents");
			
			//echo "seat_available".$batch_available;exit;
			//$homelocation = $this->_request->getParam("homelocation");
			//$teacherlocation = $this->_request->getParam("teacherlocation");
			//$lessionid=$this->_request->getParam("lessionid");
	
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				$response = array();
				
				if($noofstudents == "")$response["data"]["noofstudents"] = "nostudent";
				elseif((!is_numeric($noofstudents) || $noofstudents <= "0"))$response["data"]["noofstudents"] = "numberinvalid";
				elseif($noofstudents>$batch_available)$response["data"]["batch_avail"] = "onlyenternull";
				else $response["data"]["noofstudents"] = "valid";
				
				
				if(!in_array('location',$response['data']) && !in_array('null',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('nostudent',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('numberinvalid',$response['data'])){
						$response['returnvalue'] = "success";
					}
				else{$response['returnvalue'] = "validation";}
				echo json_encode($response);
			}
			else 
			{
					$authUserNamespace->nostudents=$noofstudents;
					$this->_redirect('/tutor/paymentstep2/batchid/'.$batchId.'/courseid/'.$courseid);
			}
		}
		
		if(isset($courseid) && $courseid != "")
		{
				$courseData = $tutorCourseobj->fetchRow("id ='$courseid'");
				$tutorProfileData =$tutorProfileobj->fetchRow("id='$tutorId'");
				$date1=date('Y-m-d');
				$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
										->where("c.tutor_id='$tutorId' && c.date='$date1' && c.course_id='$courseid'"));
										
				if (isset($resultData) && ($resultData!=""))
				{
					$count=$resultData->enroll_count;
					$data1 = array("enroll_count"=>$count+1);
					$tutorAnalyticsobj->update($data1,"tutor_id='$tutorId' && date='$date1' && course_id='$courseid'");
				}
				
				$this->view->tutordata = $tutorProfileData;
				$this->view->coursesubdata = $courseData;
				$authUserNamespace->coursename=$courseData->tutor_course_name;
				$authUserNamespace->tutorrate=$courseData->tutor_rate;
				$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
				$masteraddressobj = new Skillzot_Model_DbTable_Address();
				$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
												->setIntegrityCheck(false)
												->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
												->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
												->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
												->where("c.tutor_id='$tutorId'")
												 ->order(array("m.address_value asc")));
														
														
						$j=0;
						if (isset($branchData) && sizeof($branchData)>0)
						{	
							foreach ($branchData as $branchs)
							{
								$location_data = $masteraddressobj->fetchRow("address_id ='$branchs->locality;'");
//								echo $location_data->address_value;
								if (isset($location_data) && sizeof($location_data)>0)
								{
									if ($j==0)
									{
										$localitytmp = $location_data->address_value;
										$localityid=  $location_data->address_id;	
									}else
									{
										$localitytmp = $localitytmp.", ".$location_data->address_value;
										$localityid= $localityid.", ".$location_data->address_id;
									}
									$j++;
								}
							}
													
							if (isset($localitytmp) && $localitytmp!="")
							{
								$this->view->multiplelocality = $localitytmp;
								$this->view->multiplelocalityid = $localityid;
							}
						}
		}
		else 
		{
		$fetch_data = $tutorCourseobj->fetchRow("tutor_id='$tutorId'");
		$courseid=$fetch_data->id;
		$date1=date('Y-m-d');
		if($courseid!="" && $courseid!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
									->where("c.tutor_id='$tutorId' && c.date='$date1' && c.course_id='$courseid'"));
									
		if (isset($resultData) && ($resultData!=""))
		{
			$count=$resultData->enroll_count;
			$data1 = array("enroll_count"=>$count+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$tutorId' && date='$date1' && course_id='$courseid'");
		}
	    }
		$pageValue = $this->_request->getParam("page");
		$authUserNamespace->pageValue=$pageValue;
		if (isset($pageValue) && $pageValue!="")
		{
			$pageObj = new Skillzot_Model_DbTable_Pagesession();
			$setval2 = $pageValue. time().mt_rand();
			$mdset5 =  md5($setval2);
			$data = array("sessionnumber"=>$mdset5,"pagevalue"=>$pageValue);
			$pageObj->insert($data);
			$authUserNamespace->pagevalue = $mdset5;
			//$authUserNamespace->profileloginsessiondifferenceflag = 1; 
			
		}
		if (isset($tutorId) && $tutorId!="")
		{
			$this->view->tutorid = $tutorId;
			$authUserNamespace->t_id_for_review = $tutorId;
			$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
			$batchdetails_data = $batchdetailObj->fetchRow("tutor_id ='$tutorId' && b_description!=''");
			if (isset($batchdetails_data) && sizeof($batchdetails_data)>0)
			{
				$this->view->batch_t_id = $tutorId;
			}
			/* recometer logic*/
			$rateHigh = $tutorreviewobj->fetchAll($tutorreviewobj->select()
											->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
											->where("r.tutor_id='$tutorId' && (skillgrade='9' || skillgrade='10')"));
			$rateLow = $tutorreviewobj->fetchAll($tutorreviewobj->select()
											->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment'))
											->where("r.tutor_id='$tutorId' && (skillgrade='1' || skillgrade='2' || skillgrade='3' || skillgrade='4' || skillgrade='5' || skillgrade='6')"));
			$high=sizeof($rateHigh);
			$low=sizeof($rateLow);
			$recofortutor=$high-$low;									
			$reco_totaltutor = $tutorreviewobj->fetchAll($tutorreviewobj->select()
											->from(array('r'=>DATABASE_PREFIX."tutor_review"),array('skillgrade','person','id','from_id','comment','lastupdatedate'))
											->where("r.tutor_id='$tutorId'")
											->order(array("r.id desc")));							
			if(isset($recofortutor) && $recofortutor>0){
				$this->view->recofortutor = $recofortutor;
			}else{
				$this->view->recofortutor = 0;
			}
			if(isset($reco_totaltutor) && sizeof($reco_totaltutor)>0){
				$this->view->reco_totaltutor = sizeof($reco_totaltutor);
				$this->view->reco_data_all = $reco_totaltutor;
			}
			
		}
		$tutorProfileData =$tutorProfileobj->fetchRow("id='$tutorId'");
		
		if (isset($tutorProfileData) && sizeof($tutorProfileData)>0)
		{
			$this->view->tutordata = $tutorProfileData;
			$authUserNamespace->userclasstype = $tutorProfileData->tutor_class_type;
			$masteraddressobj = new Skillzot_Model_DbTable_Address();
			if (isset($tutorProfileData->tutor_add_locality) && $tutorProfileData->tutor_add_locality!=""){
				$locationId = $tutorProfileData->tutor_add_locality;
				$cityId =$tutorProfileData->tutor_add_city;
				$location_data = $masteraddressobj->fetchRow("address_id ='$locationId'");
				$city_data = $masteraddressobj->fetchRow("address_id ='$cityId'");
				$this->view->citydata = $city_data;
				$this->view->location = $location_data;
			}
			$class_id = $tutorProfileData->tutor_class_type;
			$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
			$ClassData = $classtypeObj->fetchRow("class_type_id ='$class_id'");
			if(isset($ClassData) && sizeOf($ClassData)>0){
				$this->view->classtype_id = $ClassData->class_type_id;
				if($ClassData->class_type_id!="" && $ClassData->class_type_id == 4){
					$this->view->classtype = 'Group classes';
				}else{
					$this->view->classtype = $ClassData->class_type_name;
				}
			}
			$this->view->phonevisible = $tutorProfileData->phone_visible;
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$courseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"))
										->where("c.tutor_id='$tutorId'"));
			//print_r($courseData);exit;
			if (isset($courseData) && sizeof($courseData)>0)
			{
					$this->view->coursedata = $courseData ;
					$i = 0;
					foreach ($courseData as $course){
					 	if ((isset($course->tutor_skill_id) && $course->tutor_skill_id!="")||(isset($course->tutor_course_name) && $course->tutor_course_name!="")||(isset($course->id) && $course->id!="")){
							$tutorskillIds[$i] = $course->tutor_skill_id;
							$courseName[$i] = $course->tutor_course_name;
							$tutorIds[$i] = $course->id;
							$i++;
					 	}else{
						
						$tutorskillIds[] = '';
							$courseName[] = '';
							$tutorIds[] = '';
						}
					}
					//----temperary for one id--------------------
					
					//print_r($tutorIds);exit;
					$tMainId = $tutorIds[0];
					//echo "sd".$tMainId;exit;
					$coursesubData = $tutorCourseobj->fetchRow("id ='$tMainId'");
					//print_r($coursesubData);exit;
					if (isset($coursesubData) && sizeof($coursesubData)>0){
						$this->view->coursesubdata = $coursesubData;
						
						$authUserNamespace->coursename=$coursesubData->tutor_course_name;
						$authUserNamespace->tutorrate=$coursesubData->tutor_rate;
						$paycycleId = $coursesubData->tutor_pay_cycle ;
					
						$paytypeData = $paycycleObj->fetchRow("pay_cycle_id ='$paycycleId'");
						if (isset($paytypeData) && sizeof($paytypeData)>0){
							$this->view->paydatatype = $paytypeData->pay_cycle_name;
						}
						
						$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
						$masteraddressobj = new Skillzot_Model_DbTable_Address();
						$lessonlocationResult = $lessonlocationObj->fetchRow("location_id ='$coursesubData->tutor_lesson_location'");
						if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
						{
							if ($lessonlocationResult->location_id=='3')
							{
								$this->view->tutorlessonlocation = "Teacher's location & open to traveling";
							}else{
								$this->view->tutorlessonlocation = $lessonlocationResult->location_name;
							}
							
						}
						//------locality display-------
						$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
//						$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
//														->from(array('b'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
//														->where("b.course_id='$coursesubData->id'"));
						
							$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
												->setIntegrityCheck(false)
												->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
												->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
												->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
												->where("c.tutor_id='$tutorId'")
												 ->order(array("m.address_value asc")));
														
														
						$j=0;
						if (isset($branchData) && sizeof($branchData)>0)
						{	
							foreach ($branchData as $branchs)
							{
								$location_data = $masteraddressobj->fetchRow("address_id ='$branchs->locality;'");
//								echo $location_data->address_value;
								if (isset($location_data) && sizeof($location_data)>0)
								{
									if ($j==0)
									{
										$localitytmp = $location_data->address_value;
										$localityid=  $location_data->address_id;	
									}else
									{
										$localitytmp = $localitytmp.", ".$location_data->address_value;
										$localityid= $localityid.", ".$location_data->address_id;
									}
									$j++;
								}
							}
													
							if (isset($localitytmp) && $localitytmp!="")
							{
								$this->view->multiplelocality = $localitytmp;
								$this->view->multiplelocalityid = $localityid;
							}
						}
						//------------------------------
					
					}
					
					$tutorIdSize = sizeof($tutorskillIds);
					$fetchIdStr = "";
					for($k=0;$k<$tutorIdSize;$k++){
					
					if($fetchIdStr=="")
						{
							$fetchIdStr = $tutorskillIds[$k];
						}else{
							$fetchIdStr = $fetchIdStr.",".$tutorskillIds[$k];
						}
					}
					$skillIDs = explode(",",$fetchIdStr);
					$sizeofskillIDs = sizeof($skillIDs);
					$skillObj = new Skillzot_Model_DbTable_Skills();
					for($j=0;$j<$sizeofskillIDs;$j++)
					{
						$sskilId = $skillIDs[$j];
						$SkillData = $skillObj->fetchRow("skill_id ='$sskilId' && skill_id!='0'");
						if (isset($SkillData) && sizeof($SkillData) > 0){
							$finalSkillname[$j] = $SkillData->skill_name;
						}
					}
					if (isset($finalSkillname) && sizeof($finalSkillname) > 0){
						$finalSkillname = array_unique($finalSkillname);
						$this->view->skillname = $finalSkillname;
					}
				}
				
				$experienceObj = new Skillzot_Model_DbTable_Tutorexperience();
				$expeduObj = new Skillzot_Model_DbTable_Expedutype();
				$Resultrow = $experienceObj->fetchAll($experienceObj->select()
				 							->setIntegrityCheck(false)
											->from(array('a'=>DATABASE_PREFIX."tx_tutor_experience"))
											->join(array('e'=>DATABASE_PREFIX."master_expedu_type"),'a.tutor_expedu_type = e.expedu_id',array('e.expedu_name'))
										    ->where("a.tutor_id='$tutorId' && e.expedu_id!='4' && e.expedu_id!='3' ")
										    ->order(array("a.tutor_expedu_type","a.id")));
										    
										    
										    
				if (isset($Resultrow) && sizeof($Resultrow)>0){
					$this->view->expeduname = $Resultrow;
				}
		}
		}		
}

public function paymentstep2Action()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		if (($authUserNamespace->maintutorid == $authUserNamespace->ptutorid) && $authUserNamespace->maintutorid != "" && $authUserNamespace->ptutorid !="")
		{
			$this->_redirect('/tutor/profile/tutorid/'.$authUserNamespace->ptutorid);	
		}
		$urlValuefor = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		//echo "facebook".$urlValuefor;
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		$batchId = $this->_request->getParam("batchid");

		//$TutorId = $this->_request->getParam("tutorid");
		$batchObj= new Skillzot_Model_DbTable_Batch();
		$batchviewResult=$batchObj->fetchRow($batchObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.id ='$batchId'"));
		
		$TutorId=$batchviewResult['tutor_id'];
		$courseid = $this->_request->getParam("courseid");
		$price= $authUserNamespace->nostudents*$authUserNamespace->tutorrate;
		$authUserNamespace->price=$price;
		$date1=date('Y-m-d');	
		if($courseid==""){
			$fetch_data = $tutorCourseobj->fetchRow("tutor_id='$TutorId'");
			$courseid=$fetch_data->id;
		}
		if($courseid!="" && $courseid!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
										->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
		if (isset($resultData) && ($resultData > 0))
		{
			$count=$resultData->paymentstep3_count;
			$data1 = array("paymentstep3_count"=>$count+1);
			$tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
		}
		}
		if($this->_request->isPost())
		{
			$uname=$this->_request->getParam("uname");
			$uaddress=$this->_request->getParam("uaddress");
			$ucity=$this->_request->getParam("ucity");
			$ustate=$this->_request->getParam("ustate");
			$upostalcode=$this->_request->getParam("upostalcode");
			$utelephone=$this->_request->getParam("utelephone");
			//$paymenttype=$this->_request->getParam("paymenttype");
			$reference_no=$this->_request->getParam("reference_no");
			$today = date("Y-m-d H:i:s");
				
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($uname == "")$response["data"]["uname"] = "null";
				else $response["data"]["uname"] = "valid";	
				
				if($uaddress == "")$response["data"]["uaddress"] = "null";
				else $response["data"]["uaddress"] = "valid";
					
				if($ucity == "")$response["data"]["ucity"] = "null";
				else $response["data"]["ucity"] = "valid";
				
				if($ustate == "")$response["data"]["ustate"] = "null";
				else $response["data"]["ustate"] = "valid";
				
				if($upostalcode == "")$response["data"]["upostalcode"] = "null";
				elseif((!is_numeric($upostalcode) || $upostalcode=="0"))$response["data"]["upostalcode"] = "numberinvalid";
				elseif(($upostalcode != "" && strlen($upostalcode)!=6))$response["data"]["upostalcode"] = "postallength";
				else $response["data"]["upostalcode"] = "valid";
				
				if($utelephone == "")$response["data"]["utelephone"] = "null";
				elseif((!is_numeric($utelephone) || $utelephone=="0"))$response["data"]["utelephone"] = "numberinvalid";
				elseif(($utelephone != "" && strlen($utelephone)!=10))$response["data"]["utelephone"] = "mobilelength";
				else $response["data"]["utelephone"] = "valid";
				
				/*if($paymenttype == "")$response["data"]["paymenttype"] = "null";
				else $response["data"]["paymenttype"] = "valid";*/
							
				if(!in_array('mobilelength',$response['data']) && !in_array('postallength',$response['data']) && !in_array('location',$response['data']) && !in_array('null',$response['data'])&& !in_array('nostudent',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('numberinvalid',$response['data'])){
						$response['returnvalue'] = "success";
					}
				else{$response['returnvalue'] = "validation";}
				echo json_encode($response);
			
			if((isset($uname) && $uname !="") && (isset($uaddress) && $uaddress !="") && (isset($ucity) && $ucity !="") && (isset($ustate) && $ustate !="") && (isset($upostalcode) && $upostalcode !="") && (isset($utelephone) && $utelephone !=""))
			{
				 $authUserNamespace->paymentname=$uname;
				 $authUserNamespace->paymentaddress=$uaddress;
				 $authUserNamespace->paymentcity=$ucity;
				 $authUserNamespace->paymentstate=$ustate;
				 $authUserNamespace->paymentpostalcode=$upostalcode;
				 $authUserNamespace->paymentphone=$utelephone;
				 $Internet_handle = $authUserNamespace->price * 0.02;
				 $authUserNamespace->grandprice = $authUserNamespace->price + $Internet_handle;
				 $billdetailsobj= new Skillzot_Model_DbTable_Billdetails();
				 if((isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid !=""))
				 {
				 	//echo $authUserNamespace->maintutorid;exit;
				 		$data = array("batch_id"=>$batchId,"course_id"=>$courseid,"name"=>$uname,"address"=>$uaddress,"city"=>$ucity,"state"=>$ustate,"pincode"=>$upostalcode,
								"phone_no"=>$utelephone,"payment_opt"=>"","tutor_id"=>$authUserNamespace->maintutorid,
								"ref_no"=>$reference_no,"price"=>$authUserNamespace->price,"update_date"=>$today,"grand_total"=>$authUserNamespace->grandprice);
						$billdata=$billdetailsobj->fetchRow("tutor_id = '$authUserNamespace->maintutorid' && batch_id='$batchId'");
						if(sizeof($billdata) <= 0)
						{
							$billdetailsobj->insert($data);
						}
						else{
							$billdetailsobj->update($data,"tutor_id = '$authUserNamespace->maintutorid' && batch_id='$batchId'");
						}
						//$this->_redirect('/tutor/paymentstep3/batchid/'.$batchId.'/courseid/'.$courseid);
				 }
				 else 
				 {
				 		if((isset($authUserNamespace->studentid) && $authUserNamespace->studentid !=""))
				 		{
				 		$data = array("batch_id"=>$batchId,"course_id"=>$courseid,"name"=>$uname,"address"=>$uaddress,"city"=>$ucity,"state"=>$ustate,"pincode"=>$upostalcode,
								"phone_no"=>$utelephone,"payment_opt"=>"","std_id"=>$authUserNamespace->studentid,
								"ref_no"=>$reference_no,"price"=>$authUserNamespace->price,"update_date"=>$today,"grand_total"=>$authUserNamespace->grandprice);
				 		}
						$billdata=$billdetailsobj->fetchRow("std_id = '$authUserNamespace->studentid' && batch_id='$batchId'");
						if(sizeof($billdata) <= 0)
						{
							$billdetailsobj->insert($data);
						}
						else{
							$billdetailsobj->update($data,"std_id = '$authUserNamespace->studentid' && batch_id='$batchId'");
						}
					//$this->_redirect('/tutor/paymentstep3/batchid/'.$batchId.'/courseid/'.$courseid);	
				 }	 
			}
			//$this->_redirect('/tutor/paymentstep3/batchid/'.$batchId.'/courseid/'.$courseid);
			//echo "<script>window.parent.location='". BASEPATH ."/tutor/paymentstep3/batchid/".$batchId."/courseid/".$courseid."';</script>";
			}

			
		}
		
	
}
public function paymentstep3Action()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		$batchId = $this->_request->getParam("batchid");

		//$TutorId = $this->_request->getParam("tutorid");
		$batchObj= new Skillzot_Model_DbTable_Batch();
		$batchviewResult=$batchObj->fetchRow($batchObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.id ='$batchId'"));
		$this->view->batchviewResult = $batchviewResult;
		$TutorId=$batchviewResult->tutor_id;
		$tutorCourseobj_row=$tutorCourseobj->fetchRow($tutorCourseobj->select()
					->from(array('a'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('*'))
					->where("a.id ='$batchviewResult->course_id'"));
		$this->view->tutorCourseobj_row = $tutorCourseobj_row;
		//$TutorId = $this->_request->getParam("tutorid");
		/*$billingObj= new Skillzot_Model_DbTable_Billdetails;
		$billingDetailsResult=$billingObj->fetchRow($billingObj->select()
					->from(array('b'=>DATABASE_PREFIX."billing_details"),array('*'))
					->where("b.batch_id ='$batchId'"));
		$this->view->billingDetailsResult = $billingDetailsResult;*/
		$courseid = $this->_request->getParam("courseid");
		$date1=date('Y-m-d');
		if($courseid==""){
			$fetch_data = $tutorCourseobj->fetchRow("tutor_id='$TutorId'");
			$courseid=$fetch_data->id;
		}
		if($courseid!="" && $courseid!="NULL")
		{
		$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
									    ->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
								
	    if (isset($resultData) && ($resultData > 0))
		{
			 $count=$resultData->paymentstep2_count;
			 $data1 = array("paymentstep2_count"=>$count+1);
			 $tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
		 }
	     }
	     if($this->_request->isPost())
		{
			$terms=$this->_request->getParam("terms");
							
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();	
											
				if( !in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('numberinvalid',$response['data'])){
						$response['returnvalue'] = "success";
					}
				else{$response['returnvalue'] = "validation";}
				echo json_encode($response);	
			
			}
			//$this->_redirect('/tutor/paymentstep4');
			
		}

}

public function secureAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		if($this->_request->isPost())
		{
				
					$account_id=$this->_request->getParam("account_id");
					$this->view->account_id=$account_id;
					
					$return_url=$this->_request->getParam("return_url");
					$this->view->return_url=$return_url;
					
					$mode=$this->_request->getParam("mode");
					$this->view->mode=$mode;
					
					$reference_no=$this->_request->getParam("reference_no");
					$this->view->reference_no=$reference_no;
					
					$amount=$this->_request->getParam("amount");
					$this->view->amount=$amount;
					
					$description=$this->_request->getParam("description");
					$this->view->description=$description;
					
					$name=$this->_request->getParam("name");
					$this->view->name=$name;
					
					$address=$this->_request->getParam("address");
					$this->view->address=$address;
					
					$city=$this->_request->getParam("city");
					$this->view->city=$city;
					
					$state=$this->_request->getParam("state");
					$this->view->state=$state;
					
					$postal_code=$this->_request->getParam("postal_code");
					$this->view->postal_code=$postal_code;
					
					$country=$this->_request->getParam("country");
					$this->view->country=$country;
					
					$phone=$this->_request->getParam("phone");
					$this->view->phone=$phone;
					
					$email=$this->_request->getParam("email");
					$this->view->email=$email;
					
					$hash = "ebskey"."|".$_POST['account_id']."|".$_POST['amount']."|".$_POST['reference_no']."|".$_POST['return_url']."|".$_POST['mode'];
					//echo $hash;exit;
					$secure_hash = md5($hash);
					$this->view->secure_hash=$secure_hash;					
		}
}
public function responseAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$this->_helper->layout()->setLayout("tutorprofilepage");
		}else{
			$this->_helper->layout()->setLayout("searchinnerpage");
		}
		//echo "ebs".$authUserNamespace->ebs_version;
		if($authUserNamespace->ebs_version == "test")
		{
			$fetchdata=$this->_request->getParam("fetchdata");
			$secret_key = "ebskey";	

			$path =  dirname(dirname(dirname(__FILE__))).'/ebs/Rc43.php';
			include $path;
			$DR = preg_replace("/\s/","+",$fetchdata);
			$rc4 = new Crypt_RC4($secret_key);
			$QueryString = base64_decode($DR);
			$rc4->decrypt($QueryString);
			$QueryString = explode('&',$QueryString);
			
			$response=array();
			
			foreach($QueryString as $param)
			{
				$param = explode('=',$param);
				$response[$param[0]] = isset($param[1]) ? urldecode($param[1]) : null;
				//$response[$param[0]] = urldecode($param[1]);
				$this->view->$param[0]=$response[$param[0]];
				
			} 

			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			$tutorData = $tutorProfileobj->fetchRow("id ='$authUserNamespace->ptutorid'");

			$batchObj= new Skillzot_Model_DbTable_Batch();
			$batchviewResult=$batchObj->fetchRow($batchObj->select()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
							->where("c.id ='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id'"));
			$seat_available=$batchviewResult->seat_available;
			$update_seat_available=$seat_available-$authUserNamespace->nostudents;

			$orderdetailsobj= new Skillzot_Model_DbTable_Orderdetails();
			$commisionObj= new Skillzot_Model_DbTable_Commisiondetails();
			if((isset($authUserNamespace->studentid) && $authUserNamespace->studentid !=""))
			{
			$data = array("std_id"=>$authUserNamespace->studentid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$response['PaymentID'],"date_of_booking"=>$response['DateCreated'],
						    "class_booked"=>$authUserNamespace->combinename,"no_of_students"=>"0",
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"total_price"=>$authUserNamespace->price,
						    "commision_price"=>"0","tutor_price"=>"0","name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);

			$data_for_update = array("std_id"=>$authUserNamespace->studentid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$response['PaymentID'],"date_of_booking"=>$response['DateCreated'],
						    "class_booked"=>$authUserNamespace->combinename,
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);

			$data_batch_available=array("seat_available"=>$update_seat_available);
			$curr_date=date('Y-m');
			$orderdetailsobj_update=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('*'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid' && a.batch_id='$authUserNamespace->pbatchid' && a.course_id='$authUserNamespace->pcourse_id' && a.std_id='$authUserNamespace->studentid'"));
			if($orderdetailsobj_update!="" && sizeof($orderdetailsobj_update)>0)
			{
				$orderdetailsobj->update($data_for_update,"DATE_FORMAT(date_of_booking,'%Y-%m') ='$curr_date' && tutor_book_id='$authUserNamespace->ptutorid' && batch_id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id' && std_id='$authUserNamespace->studentid'");
			}
			else
			{
				$orderdetailsobj->insert($data);
			}
			
			$batchObj->update($data_batch_available,"id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id'");
				$curr_date=date('Y-m');
			$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
			if($orderdetailsobj_com->no_of_students==0 || $orderdetailsobj_com->no_of_students=="NULL" || $orderdetailsobj_com->no_of_students=="")
				{
				//echo "if";
				$student_enrolled=1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate1=$commisionResult->commission;
				$tmp1 = $authUserNamespace->tutorrate * $commisionResult_rate1;
				$orderdetailsobj->update(array("commision_price"=>$tmp1),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				$reversed = "0";
				$curr_date=date('Y-m');
				for($i = 1; $i <= $authUserNamespace->nostudents-1; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));

				$student_enrolled=$orderdetailsobj_com->no_of_students+1;


				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;

				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");

				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;	
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price','e.tutor_price as tutor_price','e.total_price as total_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$total_commision= $previous_commision->commision_price + $reversed;
				$previous_amount= $previous_commision->tutor_price;
				//$total_amount= $previous_commision->total_price + $authUserNamespace->price;
				$total_commision_price= $previous_commision->total_price - $total_commision;
				$tutor_price= $previous_amount + $total_commision_price;
				$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				
				}
			
				elseif($orderdetailsobj_com->no_of_students!="")
				{
				//echo "else";
				$reversed = "0";
				$tmp="0";
				$curr_date=date('Y-m');
				
				for($i = 1; $i <= $authUserNamespace->nostudents; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
								->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
				$student_enrolled=$orderdetailsobj_com->no_of_students+1;
				
				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
								->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
								->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;
				$student_enrolled_incre=$student_enrolled_incre+1;
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$previous_commision1=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('*'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && e.course_id='$authUserNamespace->pcourse_id'"));
				//echo "comm".$previous_commision1->commision_price;exit;
				if($previous_commision->commision_price!="" && $previous_commision->commision_price!="0.00")
				{
					//echo "sub-if";
					$total_commision= $previous_commision1->commision_price + $reversed;
					$total_amount= $previous_commision1->total_price + $authUserNamespace->price;
					$tutor_price= $total_amount - $total_commision;
					$no_of_students=$previous_commision1->no_of_students + $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price,"total_price"=>$total_amount),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");

				}
				else
				{
					//echo "sub-else";
					$total_commision1= $reversed;
					$total_amount1= $authUserNamespace->price;
					$tutor_price1= $total_amount1 - $total_commision1;
					$no_of_students1= $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("no_of_students"=>$no_of_students1,"commision_price"=>$total_commision1,"tutor_price"=>$tutor_price1,"total_price"=>$total_amount1),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				}
				}
			}
		
			if((isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid !="")) 
			{
			$data = array("tutor_id"=>$authUserNamespace->maintutorid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$response['PaymentID'],"date_of_booking"=>$response['DateCreated'],
						    "class_booked"=>$authUserNamespace->combinename,"no_of_students"=>"0",
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"total_price"=>$authUserNamespace->price,
						    "commision_price"=>"0","tutor_price"=>"0","name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);
			$data_for_update = array("tutor_id"=>$authUserNamespace->maintutorid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$response['PaymentID'],"date_of_booking"=>$response['DateCreated'],
						    "class_booked"=>$authUserNamespace->combinename,
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);

			$data_batch_available=array("seat_available"=>$update_seat_available);
			$curr_date=date('Y-m');
			$orderdetailsobj_update=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('*'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid' && a.batch_id='$authUserNamespace->pbatchid' && a.course_id='$authUserNamespace->pcourse_id' && a.tutor_id='$authUserNamespace->maintutorid'"));
			if($orderdetailsobj_update!="" && sizeof($orderdetailsobj_update)>0)
			{
				$orderdetailsobj->update($data_for_update,"DATE_FORMAT(date_of_booking,'%Y-%m') ='$curr_date' && tutor_book_id='$authUserNamespace->ptutorid' && batch_id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id' && tutor_id='$authUserNamespace->maintutorid'");
			}
			else
			{
				$orderdetailsobj->insert($data);
			}
			
			$batchObj->update($data_batch_available,"id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id'");
			$curr_date=date('Y-m');
			$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
				if($orderdetailsobj_com->no_of_students==0 || $orderdetailsobj_com->no_of_students=="NULL" || $orderdetailsobj_com->no_of_students=="")
				{
				//echo "if";
				$student_enrolled=1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate1=$commisionResult->commission;
				$tmp1 = $authUserNamespace->tutorrate * $commisionResult_rate1;
				$orderdetailsobj->update(array("commision_price"=>$tmp1),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				$reversed = "0";
				$curr_date=date('Y-m');
				for($i = 1; $i <= $authUserNamespace->nostudents-1; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));

				$student_enrolled=$orderdetailsobj_com->no_of_students+1;


				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;

				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");

				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;	
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price','e.tutor_price as tutor_price','e.total_price as total_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$total_commision= $previous_commision->commision_price + $reversed;
				$previous_amount= $previous_commision->tutor_price;
				//$total_amount= $previous_commision->total_price + $authUserNamespace->price;
				$total_commision_price= $previous_commision->total_price - $total_commision;
				$tutor_price= $previous_amount + $total_commision_price;
				$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				
				}
			
				elseif($orderdetailsobj_com->no_of_students!="")
				{
				//echo "else";
				$reversed = "0";
				$tmp="0";
				$curr_date=date('Y-m');
				
				for($i = 1; $i <= $authUserNamespace->nostudents; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
								->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
				$student_enrolled=$orderdetailsobj_com->no_of_students+1;
				
				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
								->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
								->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;
				$student_enrolled_incre=$student_enrolled_incre+1;
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$previous_commision1=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('*'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && e.course_id='$authUserNamespace->pcourse_id'"));
				//echo "comm".$previous_commision1->commision_price;exit;
				if($previous_commision->commision_price!="" && $previous_commision->commision_price!="0.00")
				{
					//echo "sub-if";
					$total_commision= $previous_commision1->commision_price + $reversed;
					$total_amount= $previous_commision1->total_price + $authUserNamespace->price;
					$tutor_price= $total_amount - $total_commision;
					$no_of_students=$previous_commision1->no_of_students + $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price,"total_price"=>$total_amount),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");

				}
				else
				{
					//echo "sub-else";
					$total_commision1= $reversed;
					$total_amount1= $authUserNamespace->price;
					$tutor_price1= $total_amount1 - $total_commision1;
					$no_of_students1= $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("no_of_students"=>$no_of_students1,"commision_price"=>$total_commision1,"tutor_price"=>$tutor_price1,"total_price"=>$total_amount1),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				}
				}


			}
			if(isset($tutorData->company_name) && $tutorData->company_name != "")
			{
				$name=$tutorData->company_name;
				$tutorcompany=$tutorData->company_name;
				$tutoremail=$tutorData->tutor_email;
				$tutorname=$tutorData->tutor_first_name." ".$tutorData->tutor_last_name;
				$tutorcontact=$tutorData->tutor_mobile;
			}	
			else
			{
				$name=ucfirst($tutorData->tutor_first_name)." ".ucfirst($tutorData->tutor_last_name);	
			}
			if((isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid !=""))
			{
					$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorprofiledata = $tutorProfileobj->fetchRow("id ='$authUserNamespace->maintutorid'");
					$email1=$tutorprofiledata->tutor_email;
					$name1=$tutorprofiledata->tutor_first_name." ".$tutorprofiledata->tutor_last_name;
					$contact1=$tutorprofiledata->tutor_mobile;
			}
			else 
			{
					if((isset($authUserNamespace->studentid) && $authUserNamespace->studentid !=""))
					{
						$stuProfileobj = new Skillzot_Model_DbTable_Studentsignup();
						$stuprofiledata = $stuProfileobj->fetchRow("id ='$authUserNamespace->studentid'");
						$email1=$stuprofiledata->std_email;
						$name1=$stuprofiledata->first_name." ".$stuprofiledata->last_name;
						$contact1=$stuprofiledata->std_phone;
					}
			}
			
			if(isset($batchviewResult) && $batchviewResult!= "") {
				$alllocationId = $batchviewResult->tutor_batch_name;
				$locationIds = explode("/",$alllocationId);
				$slot_location= trim(($locationIds[1]."/".$locationIds[2]),"-")."at".$locationIds[0];

			}
			if(isset($batchviewResult->tutor_class_dur_wks) && $batchviewResult->tutor_class_dur_wks=="0")
										  { $weekdisplay="On-going monthly";
								          }else
								          { $weekdisplay=$batchviewResult->tutor_class_dur_wks; }
								          if(isset($batchviewResult->tutor_class_dur_dayspwk) && $batchviewResult->tutor_class_dur_dayspwk=="0")
										  { $daydisplay="Flexible days";
								          }else
								          { $daydisplay=$batchviewResult->tutor_class_dur_dayspwk; }
								          if(isset($batchviewResult->tutor_class_dur_hrspday) && $batchviewResult->tutor_class_dur_hrspday=="0.5")
								          { $hrsdisplay="30 mins"; 
										  }elseif(isset($batchviewResult->tutor_class_dur_hrspday) && $batchviewResult->tutor_class_dur_hrspday=="0.75")
								          { $hrsdisplay="45 mins"; }
								          elseif(isset($batchviewResult->tutor_class_dur_hrspday) && $batchviewResult->tutor_class_dur_hrspday=="1")
								          { $hrsdisplay="1 hour"; }
								          else
								          { $hrsdisplay=$batchviewResult->tutor_class_dur_hrspday." hours"; }
									$duration= $weekdisplay." course,".$daydisplay." week,".$hrsdisplay; 
			//echo $slot_location;
			$course_pdf=explode("by",$authUserNamespace->combinename);
			$duration_pdf=explode(",",$duration);
			$slot_location_pdf=explode("at",$slot_location);
			$slot_location_pdf_time=explode("/",$slot_location_pdf[0]);
			if($authUserNamespace->nostudents=="1")
			{
				$no_of_stud= " ".$authUserNamespace->nostudents." student (Rs.)";
			}
			else
			{
				$no_of_stud= " ".$authUserNamespace->nostudents." students (Rs.)";
			}
			//print_r($slot_location_pdf);
			try {
	       // create PDF
	       $pdf = new Zend_Pdf();

	       // create A4 page
	       $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
	       

	  		// draw a line at the top of the page
			$page->setLineColor(new Zend_Pdf_Color_Rgb(0,0,0));
			$page->drawLine(10, 790, ($page->getWidth()-10), 790);

			// draw another line near the bottom of the page
			$page->drawLine(10, 25, ($page->getWidth()-10), 25);

			// define image resource
			$image = Zend_Pdf_Image::imageWithPath('invoice/skillzot-logo.jpg');
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 10);
			// write image to page
			$page->drawImage($image, 25, 800, ($image->getPixelWidth()+25), (800+$image->getPixelHeight()));
		
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 8);
			$page->drawText('Contact us: +91 9820921404', 455, 820);
			$page->drawText('zot@skillzot.com', 500, 810);

			 // define font resource
	       	$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 12);
			$page->drawText("Order id: ".$response['PaymentID'], 25, 750)
				 ->drawText('Billing address ', 180, 750)
				 ->drawText('Student details ', 300, 750)
				 ->drawText('Tutor details ', 440, 750);
		    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 10);
	        $page->drawText('Order date: '.date('d-m-Y',strtotime($response['DateCreated'])), 25, 735);
	             //->drawText('Service tax: ', 25, 720);
			$page->drawText($authUserNamespace->paymentname, 180, 735)
	             ->drawText($authUserNamespace->paymentaddress, 180, 720)
	             ->drawText($authUserNamespace->paymentcity, 180, 705)
	             ->drawText($authUserNamespace->paymentpostalcode.", ", 180, 690)
	             ->drawText($authUserNamespace->paymentstate, 220, 690)
	             ->drawText($authUserNamespace->paymentphone, 180, 675);

			$page->drawText($name1, 300, 735)
	             ->drawText($email1, 300, 720)
	             ->drawText($contact1, 300, 705);

	        $page->drawText($tutorname, 440, 735)
	             ->drawText($tutorcompany, 440, 720)
	             ->drawText($tutoremail, 440, 705)
	             ->drawText($tutorcontact, 440, 690);
	             	  
	        $page->drawLine(10, 665, ($page->getWidth()-10), 665);
	        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);
	      	$page->drawText("Course details", 10, 650)
				 ->drawText('Slot & location', 120, 650)
				 ->drawText('Duration', 220, 650)
				 ->drawText('Price of the course', 300, 650)
				 ->drawText('Quantity', 410, 650)
				 ->drawText('Price for'.$no_of_stud, 460, 650);
			$page->drawLine(10, 640, ($page->getWidth()-10), 640);
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

	/*$text1 = preg_replace('/[\r\n]+/', '\n', $course_pdf[1]);
	$text2 = wordwrap($text1,20, '<br/>', true);
	$text = nl2br($text1);*/
	$textChunk = wordwrap($course_pdf[1], 20, '\n');
	$line=600;
	foreach(explode('\n', $textChunk) as $textLine){
	  if ($textLine!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine)), 10, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	$textChunk1 = wordwrap($course_pdf[0], 20, '\n');
	$line=620;
	foreach(explode('\n', $textChunk1) as $textLine1){
	  if ($textLine1!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine1)), 10, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	//echo $slot_location_pdf_time[0];
	$textChunk2 = wordwrap($slot_location_pdf_time[0], 120, '\n');
	$line=620;
	foreach(explode('\n', $textChunk2) as $textLine2){
	  if ($textLine2!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine2)), 120, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	$textChunk3 = wordwrap($slot_location_pdf_time[1], 120, '\n');
	$line=610;
	foreach(explode('\n', $textChunk3) as $textLine2){
	  if ($textLine2!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine2)), 120, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	      	$page->setFont($font, 8);
			//$page->drawText($course_pdf[0], 10, 620)
	        //$page->drawText($slot_location_pdf[0], 120, 620)
	        $page->drawText($duration_pdf[0], 220, 620)
	             ->drawText($authUserNamespace->tutorrate, 320, 620)
	             ->drawText($authUserNamespace->nostudents, 420, 620)
	             ->drawText($authUserNamespace->price, 510, 620);
	        $page->drawText("by", 20, 610)
	             ->drawText("at", 125, 600)
	             ->drawText($duration_pdf[1], 220, 610);
	        //$page->drawText($course_pdf[1], 10, 600)
	        $page->drawText($slot_location_pdf[1], 120, 590)
	             ->drawText($duration_pdf[2], 220, 600);
	        $page->drawLine(10, 560, ($page->getWidth()-10), 560);
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);    
	        $page->drawText("Internet handling charges (Rs)", 335, 540)
	        	 ->drawText($authUserNamespace->internet, 520, 540);   
	        $page->drawLine(10, 530, ($page->getWidth()-10), 530);
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);
	      	$page->drawText("Grand Total (Rs)", 335, 510)
	        	 ->drawText($authUserNamespace->grandprice, 510, 510);
	       	$page->drawLine(10, 490, ($page->getWidth()-10), 490);
	       	$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_ITALIC );
	      	$page->setFont($font, 6);
	      	$page->drawText("This is a computer generated invoice. No signature required", 230, 480);
			// add footer text 
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);   
			$page->drawText('Copyright 2015 Skillzot. All rights reserved.', ($page->getWidth()/3), 10);
	        $page->drawText('Declaration ', 20, 440);
	        $page->drawText('This is a system generated invoice and does not reqiured signature ', 20, 400);
	        $page->drawText('For queries regarding this enrollment please contact  ', 20, 360);
	        $page->drawText('Cancellation Policy  ', 20, 320);
	        $page->drawText('Teacher Cancellation Policy after confirmation ', 20, 240);
	        


	        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 8);
			$page->drawText("we declare that this invoice shows actual price of the services desribed inclusive of taxes and all particulars are true and correct.", 20, 420);
	        $page->drawText("Thank you for enrolling through skillzot.com, we look forward to serve you.", 20, 380);
	        $page->drawText("Phone: +919820921404, Email ID: zot@skillzot.com", 20, 340);
	         
	        $page->drawText("You will get a full refund if you decide not join the course within 10 days.", 20, 300);
	        $page->drawText("We will follow up with you to co-ordinate the same once you put in your request by sending an e-mail on zot@skillzot.com.", 20, 290);
	        $page->drawText("If you decide not to join after 10 days, then you can still avail refund with a 2% deduction within 30 days. ", 20, 280);
	        $page->drawText("You will get the refund with 07 business days after you put in your request.", 20, 270);
	        $page->drawText("Once you enroll with the teacher and there is confirmation from both you and teacher, the cancellation policies of the teacher as mentioned below applies.", 20, 260); 
	        $page->drawText("Once you enroll with the teacher and there confirmation from both you and the teacher, then there is no refund applicable.", 20, 220);
	       // add page to document
	       $pdf->pages[] = $page;
	     

	       // save as file
	       $pdf->save("invoice/".$response['PaymentID']."-".date('d-m-Y').".pdf");
	       //$pdf->save("invoice/".$PaymentID."-".date('d-m-Y').".pdf");
	      // echo 'SUCCESS: Document saved!';
	   } catch (Zend_Pdf_Exception $e) {
	       die ('PDF error: ' . $e->getMessage());
	   } catch (Exception $e) {
	       die ('Application error: ' . $e->getMessage());
	   }
	   // sms send to person making payment
		$username='madonlinepltd';
		//ini_set("display_errors",1);
	    $password ='Onlinehttp';
	    $senderid = 'SKLZOT';
	    $udh = '0';
		$message = "This is a confirmation of your booking ID: ".$response['PaymentID'].", amounting to Rs. ".$authUserNamespace->grandprice." for your class with ".$name.". Thank you for choosing skillzot.com.";
		//$message = "This is a confirmation of your booking ID: ".$PaymentID.", amounting to Rs. ".$authUserNamespace->grandprice." for your class with ".$name.". Thank you for choosing skillzot.com.";
	   	//print_r($message);exit;
	    $dlrMask = '19';
		$mobileno =	$authUserNamespace->paymentphone;
		//$mobileno =	'8454822903';
		$url = 'http://www.myvaluefirst.com/smpp/sendsms';
		$fields = array(
					'username' => urlencode($username),
					'password' => urlencode($password),
					'to' => urlencode($mobileno),
					'udh' => urlencode($udh),
					'from' => urlencode($senderid),
					'text' => urlencode($message),
					'dlr-mask' => urlencode($dlrMask)
		);
		$fields_string="";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		$fields_string = rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		curl_close($ch);
		//-----------------------------------------------------------
		
		
		// mail send to person making payment
		$message1 = "<div>Hi ".$authUserNamespace->paymentname.",<br/></div><br/>";	
		$message1 .= "<div>This is a confirmation of your booking through skillzot.com. <br/></div><br/>"; 
		$message1 .= "<div>Here is the summary: <br/></div><br/>";
		$message1 .= "<div>Name : ".$authUserNamespace->paymentname."<br/></div>";
		//$message1 .= "<div>Your order id number : ".$response['PaymentID']."<br/></div>";
		//$message1 .= "<div>Date of booking : ".$response['DateCreated']."<br/></div>";
		$message1 .= "<div>Your order id number : ".$response['PaymentID']."<br/></div>";
		$message1 .= "<div>Date of booking : ".$DateCreated."<br/></div>";
		$message1 .= "<div>Class booked : " .$authUserNamespace->combinename."<br/></div>";
		$message1 .= "<div>No. of students :  ".$authUserNamespace->nostudents."<br/></div>";
		$message1 .= "<div>Price of course per student :  ".$authUserNamespace->tutorrate."<br/></div>";
		$message1 .= "<div>Total amount :  ".$authUserNamespace->grandprice."<br/></div><br/>";
		$message1 .= "<div>Please take a print of this confirmation or sms confirmation with a valid photo id at the time <br/>of your first class. In case of any clarifications feel free to call us on +91 9820921404 (Mon-<br/>Fri, 10am to 5:30pm) or email us on zot@skillzot.com<br/></div><br/>";
		$message1 .= "<div>Thank you for using skillzot.com and feel free to stalk us on facebook for fun skill related happenings and more.<br/></div><br/>";
		$message1 .= "<div>Hope you have an awesome time nurturing your talent.<br/></div><br/>";
		$message1 .= "<div>Cheers,<br/></div>";
		$message1 .= "<div>Sandeep<br/></div>";
		$message1 .= "<div>Co-founder @ Skillzot</div>";
		//print_r($message1);exit;
		$sbNameTemp = "Booking confirmation with ".$name;
		$message = $message1;
		$to = $email;
		$from = "zot@skillzot.com";
		$mail = new Zend_Mail();
		$mail->setBodyHtml($message);
		$mail->setFrom($from,"Sandeep @ Skillzot");
		$mail->addTo($to,$authUserNamespace->paymentname); //ask
		$mail->setSubject($sbNameTemp);//ask
		$content = file_get_contents("invoice/".$response['PaymentID']."-".date('d-m-Y').".pdf"); // e.g. ("attachment/abc.pdf")
		$attachment = new Zend_Mime_Part($content);
		$attachment->type = 'application/pdf';
		$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
		$attachment->encoding = Zend_Mime::ENCODING_BASE64;
		$attachment->filename = $response['PaymentID']."-".date('d-m-Y').".pdf"; // name of file
		$mail->addAttachment($attachment);
		$sent = true;
		try {
		$mail->send();
		} catch (Exception $e){
		$sent = false;
		}

		//-----------------------------------------------------------

				
		//-------------sms to teacher----------------------
		$message1 = "Hi, you've got a new student through Skillzot.com. Please check your email for details.";
	    $dlrMask = '19';
	   	$mobileno =	$tutorData->tutor_mobile;
		//$mobileno ='8454822903';
		$url = 'http://www.myvaluefirst.com/smpp/sendsms';
		$fields = array(
					'username' => urlencode($username),
					'password' => urlencode($password),
					'to' => urlencode($mobileno),
					'udh' => urlencode($udh),
					'from' => urlencode($senderid),
					'text' => urlencode($message1),
					'dlr-mask' => urlencode($dlrMask)
		);
		$fields_string="";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		$fields_string = rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		curl_close($ch);

		//-----------------------------------------------------------
		
		// mail send to teacher
		$message2 = "<div>Hi ".$name.",<br/></div><br/>";	
		$message2 .= "<div>We're happy to inform you that we've got a new student for you, someone who's eager to<br/> follow your footsteps.<br/></div><br/>"; 
		$message2 .= "<div>Here is the summary: <br/></div><br/>";
		$message2 .= "<div>Name : " .$authUserNamespace->paymentname."<br/></div>";
		$message2 .= "<div>Your order id number : " .$response['PaymentID']."<br/></div>";
		$message2 .= "<div>Date of booking : " .$response['DateCreated']."<br/></div>";
		$message2 .= "<div>Class booked : " .$authUserNamespace->combinename."<br/></div>";
		$message2 .= "<div>No. of students :  " .$authUserNamespace->nostudents."<br/></div><br/>";
		$message2 .= "<div>Here's to nurturing talent.<br/></div><br/>";
		$message2 .= "<div>Cheers,<br/></div>";
		$message2 .= "<div>Sandeep<br/></div>";
		$message2 .= "<div>Co-founder @ Skillzot</div>";
		$sbNameTemp = "You've got a new student";
		$message = $message2;
		//print_r($message2);exit;
		$to = $tutorData->tutor_email;
		$from = "zot@skillzot.com";
		$mail = new Zend_Mail();
		$mail->setBodyHtml($message);
		$mail->setFrom($from,"Sandeep @ Skillzot");
		$mail->addTo($to,$name); //ask
		$mail->setSubject($sbNameTemp);//ask
		$sent = true;
		try {
		$mail->send();
		} catch (Exception $e){
		$sent = false;
		}
	}else{

			$secret_key = "af36d3fadf10a6faab09bd893ce74a33";	

			##for version 3 use ####
			if(isset($_REQUEST)){
			$response = $_REQUEST;
			}
			$b=array_flip($response);
			// print_r($b);
			$PaymentID = array_search('PaymentID', $b);
			$this->view->PaymentID=$PaymentID;
			$DateCreated = array_search('DateCreated', $b);
			$this->view->DateCreated=$DateCreated;

			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			$tutorData = $tutorProfileobj->fetchRow("id ='$authUserNamespace->ptutorid'");

			$batchObj= new Skillzot_Model_DbTable_Batch();
			$batchviewResult=$batchObj->fetchRow($batchObj->select()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
							->where("c.id ='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id'"));
			$seat_available=$batchviewResult->seat_available;
			$update_seat_available=$seat_available-$authUserNamespace->nostudents;

			$orderdetailsobj= new Skillzot_Model_DbTable_Orderdetails();
			$commisionObj= new Skillzot_Model_DbTable_Commisiondetails();
			if((isset($authUserNamespace->studentid) && $authUserNamespace->studentid !=""))
			{
			$data = array("std_id"=>$authUserNamespace->studentid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$PaymentID,"date_of_booking"=>$DateCreated,
						    "class_booked"=>$authUserNamespace->combinename,"no_of_students"=>"0",
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"total_price"=>$authUserNamespace->price,
						    "commision_price"=>"0","tutor_price"=>"0","name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);

			$data_for_update = array("std_id"=>$authUserNamespace->studentid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$PaymentID,"date_of_booking"=>$DateCreated,
						    "class_booked"=>$authUserNamespace->combinename,
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);

			$data_batch_available=array("seat_available"=>$update_seat_available);
			$curr_date=date('Y-m');
			$orderdetailsobj_update=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('*'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid' && a.batch_id='$authUserNamespace->pbatchid' && a.course_id='$authUserNamespace->pcourse_id' && a.std_id='$authUserNamespace->studentid'"));
			if($orderdetailsobj_update!="" && sizeof($orderdetailsobj_update)>0)
			{
				$orderdetailsobj->update($data_for_update,"DATE_FORMAT(date_of_booking,'%Y-%m') ='$curr_date' && tutor_book_id='$authUserNamespace->ptutorid' && batch_id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id' && std_id='$authUserNamespace->studentid'");
			}
			else
			{
				$orderdetailsobj->insert($data);
			}
			
			$batchObj->update($data_batch_available,"id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id'");
				$curr_date=date('Y-m');
			$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
			if($orderdetailsobj_com->no_of_students==0 || $orderdetailsobj_com->no_of_students=="NULL" || $orderdetailsobj_com->no_of_students=="")
				{
				//echo "if";
				$student_enrolled=1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate1=$commisionResult->commission;
				$tmp1 = $authUserNamespace->tutorrate * $commisionResult_rate1;
				$orderdetailsobj->update(array("commision_price"=>$tmp1),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				$reversed = "0";
				$curr_date=date('Y-m');
				for($i = 1; $i <= $authUserNamespace->nostudents-1; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));

				$student_enrolled=$orderdetailsobj_com->no_of_students+1;


				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;

				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");

				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;	
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price','e.tutor_price as tutor_price','e.total_price as total_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$total_commision= $previous_commision->commision_price + $reversed;
				$previous_amount= $previous_commision->tutor_price;
				//$total_amount= $previous_commision->total_price + $authUserNamespace->price;
				$total_commision_price= $previous_commision->total_price - $total_commision;
				$tutor_price= $previous_amount + $total_commision_price;
				$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				
				}
			
				elseif($orderdetailsobj_com->no_of_students!="")
				{
				//echo "else";
				$reversed = "0";
				$tmp="0";
				$curr_date=date('Y-m');
				
				for($i = 1; $i <= $authUserNamespace->nostudents; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
								->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
				$student_enrolled=$orderdetailsobj_com->no_of_students+1;
				
				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
								->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
								->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;
				$student_enrolled_incre=$student_enrolled_incre+1;
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$previous_commision1=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('*'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && e.course_id='$authUserNamespace->pcourse_id'"));
				//echo "comm".$previous_commision1->commision_price;exit;
				if($previous_commision->commision_price!="" && $previous_commision->commision_price!="0.00")
				{
					//echo "sub-if";
					$total_commision= $previous_commision1->commision_price + $reversed;
					$total_amount= $previous_commision1->total_price + $authUserNamespace->price;
					$tutor_price= $total_amount - $total_commision;
					$no_of_students=$previous_commision1->no_of_students + $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price,"total_price"=>$total_amount),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");

				}
				else
				{
					//echo "sub-else";
					$total_commision1= $reversed;
					$total_amount1= $authUserNamespace->price;
					$tutor_price1= $total_amount1 - $total_commision1;
					$no_of_students1= $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("no_of_students"=>$no_of_students1,"commision_price"=>$total_commision1,"tutor_price"=>$tutor_price1,"total_price"=>$total_amount1),"batch_id='$authUserNamespace->pbatchid' && std_id='$authUserNamespace->studentid' && course_id='$authUserNamespace->pcourse_id'");
				}
				}
			}
		
			if((isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid !="")) 
			{
			$data = array("tutor_id"=>$authUserNamespace->maintutorid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$PaymentID,"date_of_booking"=>$DateCreated,
						    "class_booked"=>$authUserNamespace->combinename,"no_of_students"=>"0",
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"total_price"=>$authUserNamespace->price,
						    "commision_price"=>"0","tutor_price"=>"0","name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);
			$data_for_update = array("tutor_id"=>$authUserNamespace->maintutorid,"tutor_book_id"=>$authUserNamespace->ptutorid,
						    "batch_id"=>$authUserNamespace->pbatchid,"course_id"=>$authUserNamespace->pcourse_id,
						    "order_id"=>$PaymentID,"date_of_booking"=>$DateCreated,
						    "class_booked"=>$authUserNamespace->combinename,
						    "tutor_fee_per_unit"=>$authUserNamespace->tutorrate,"name"=>$authUserNamespace->paymentname,
						    "address"=>$authUserNamespace->paymentaddress,"city"=>$authUserNamespace->paymentcity,
						    "state"=>$authUserNamespace->paymentstate,"pincode"=>$authUserNamespace->paymentpostalcode,
						    "phone_number"=>$authUserNamespace->paymentphone,"grand_total"=>$authUserNamespace->grandprice);

			$data_batch_available=array("seat_available"=>$update_seat_available);
			$curr_date=date('Y-m');
			$orderdetailsobj_update=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('*'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid' && a.batch_id='$authUserNamespace->pbatchid' && a.course_id='$authUserNamespace->pcourse_id' && a.tutor_id='$authUserNamespace->maintutorid'"));
			if($orderdetailsobj_update!="" && sizeof($orderdetailsobj_update)>0)
			{
				$orderdetailsobj->update($data_for_update,"DATE_FORMAT(date_of_booking,'%Y-%m') ='$curr_date' && tutor_book_id='$authUserNamespace->ptutorid' && batch_id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id' && tutor_id='$authUserNamespace->maintutorid'");
			}
			else
			{
				$orderdetailsobj->insert($data);
			}
			
			$batchObj->update($data_batch_available,"id='$authUserNamespace->pbatchid' && course_id='$authUserNamespace->pcourse_id'");
			$curr_date=date('Y-m');
			$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
				if($orderdetailsobj_com->no_of_students==0 || $orderdetailsobj_com->no_of_students=="NULL" || $orderdetailsobj_com->no_of_students=="")
				{
				//echo "if";
				$student_enrolled=1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate1=$commisionResult->commission;
				$tmp1 = $authUserNamespace->tutorrate * $commisionResult_rate1;
				$orderdetailsobj->update(array("commision_price"=>$tmp1),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				$reversed = "0";
				$curr_date=date('Y-m');
				for($i = 1; $i <= $authUserNamespace->nostudents-1; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
							->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));

				$student_enrolled=$orderdetailsobj_com->no_of_students+1;


				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;

				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");

				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
							->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
							->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;	
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price','e.tutor_price as tutor_price','e.total_price as total_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$total_commision= $previous_commision->commision_price + $reversed;
				$previous_amount= $previous_commision->tutor_price;
				//$total_amount= $previous_commision->total_price + $authUserNamespace->price;
				$total_commision_price= $previous_commision->total_price - $total_commision;
				$tutor_price= $previous_amount + $total_commision_price;
				$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				
				}
			
				elseif($orderdetailsobj_com->no_of_students!="")
				{
				//echo "else";
				$reversed = "0";
				$tmp="0";
				$curr_date=date('Y-m');
				
				for($i = 1; $i <= $authUserNamespace->nostudents; $i++) {
				$orderdetailsobj_com=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('a'=>DATABASE_PREFIX."order_details"),array('SUM(a.no_of_students) as no_of_students'))
								->where("DATE_FORMAT(a.date_of_booking,'%Y-%m') ='$curr_date' && a.tutor_book_id='$authUserNamespace->ptutorid'"));
				$student_enrolled=$orderdetailsobj_com->no_of_students+1;
				
				$orderdetailsobj_no_of=$orderdetailsobj->fetchRow($orderdetailsobj->select()
							->from(array('d'=>DATABASE_PREFIX."order_details"),array('d.no_of_students as no_of_students'))
							->where("d.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && d.course_id='$authUserNamespace->pcourse_id'"));
				$student_enrolled_incre=$orderdetailsobj_no_of->no_of_students+1;
				$orderdetailsobj->update(array("no_of_students"=>$student_enrolled_incre),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				
				$commisionResult=$commisionObj->fetchRow($commisionObj->select()
								->from(array('b'=>DATABASE_PREFIX."commission_details"),array('*'))
								->where("b.max_fee >= '$authUserNamespace->tutorrate' && Max_enroll >= $student_enrolled"));

				$commisionResult_rate=$commisionResult->commission;
				$tmp = $authUserNamespace->tutorrate * $commisionResult_rate + $tmp;
				$student_enrolled=$student_enrolled+1;
				$student_enrolled_incre=$student_enrolled_incre+1;
				}
				$reversed = $reversed + $tmp;
				$previous_commision=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('e.commision_price as commision_price'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && e.course_id='$authUserNamespace->pcourse_id'"));
				$previous_commision1=$orderdetailsobj->fetchRow($orderdetailsobj->select()
								->from(array('e'=>DATABASE_PREFIX."order_details"),array('*'))
								->where("e.batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && e.course_id='$authUserNamespace->pcourse_id'"));
				//echo "comm".$previous_commision1->commision_price;exit;
				if($previous_commision->commision_price!="" && $previous_commision->commision_price!="0.00")
				{
					//echo "sub-if";
					$total_commision= $previous_commision1->commision_price + $reversed;
					$total_amount= $previous_commision1->total_price + $authUserNamespace->price;
					$tutor_price= $total_amount - $total_commision;
					$no_of_students=$previous_commision1->no_of_students + $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("commision_price"=>$total_commision,"tutor_price"=>$tutor_price,"total_price"=>$total_amount),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");

				}
				else
				{
					//echo "sub-else";
					$total_commision1= $reversed;
					$total_amount1= $authUserNamespace->price;
					$tutor_price1= $total_amount1 - $total_commision1;
					$no_of_students1= $authUserNamespace->nostudents;
					$orderdetailsobj->update(array("no_of_students"=>$no_of_students1,"commision_price"=>$total_commision1,"tutor_price"=>$tutor_price1,"total_price"=>$total_amount1),"batch_id='$authUserNamespace->pbatchid' && tutor_id='$authUserNamespace->maintutorid' && course_id='$authUserNamespace->pcourse_id'");
				}
				}


			}
			if(isset($tutorData->company_name) && $tutorData->company_name != "")
			{
				$name=$tutorData->company_name;
				$tutorcompany=$tutorData->company_name;
				$tutoremail=$tutorData->tutor_email;
				$tutorname=$tutorData->tutor_first_name." ".$tutorData->tutor_last_name;
				$tutorcontact=$tutorData->tutor_mobile;
			}	
			else
			{
				$name=ucfirst($tutorData->tutor_first_name)." ".ucfirst($tutorData->tutor_last_name);	
			}
			if((isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid !=""))
			{
					$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorprofiledata = $tutorProfileobj->fetchRow("id ='$authUserNamespace->maintutorid'");
					$email1=$tutorprofiledata->tutor_email;
					$name1=$tutorprofiledata->tutor_first_name." ".$tutorprofiledata->tutor_last_name;
					$contact1=$tutorprofiledata->tutor_mobile;
			}
			else 
			{
					if((isset($authUserNamespace->studentid) && $authUserNamespace->studentid !=""))
					{
						$stuProfileobj = new Skillzot_Model_DbTable_Studentsignup();
						$stuprofiledata = $stuProfileobj->fetchRow("id ='$authUserNamespace->studentid'");
						$email1=$stuprofiledata->std_email;
						$name1=$stuprofiledata->first_name." ".$stuprofiledata->last_name;
						$contact1=$stuprofiledata->std_phone;
					}
			}
			
			if(isset($batchviewResult) && $batchviewResult!= "") {
				$alllocationId = $batchviewResult->tutor_batch_name;
				$locationIds = explode("/",$alllocationId);
				$slot_location= trim(($locationIds[1]."/".$locationIds[2]),"-")."at".$locationIds[0];

			}
			if(isset($batchviewResult->tutor_class_dur_wks) && $batchviewResult->tutor_class_dur_wks=="0")
										  { $weekdisplay="On-going monthly";
								          }else
								          { $weekdisplay=$batchviewResult->tutor_class_dur_wks; }
								          if(isset($batchviewResult->tutor_class_dur_dayspwk) && $batchviewResult->tutor_class_dur_dayspwk=="0")
										  { $daydisplay="Flexible days";
								          }else
								          { $daydisplay=$batchviewResult->tutor_class_dur_dayspwk; }
								          if(isset($batchviewResult->tutor_class_dur_hrspday) && $batchviewResult->tutor_class_dur_hrspday=="0.5")
								          { $hrsdisplay="30 mins"; 
										  }elseif(isset($batchviewResult->tutor_class_dur_hrspday) && $batchviewResult->tutor_class_dur_hrspday=="0.75")
								          { $hrsdisplay="45 mins"; }
								          elseif(isset($batchviewResult->tutor_class_dur_hrspday) && $batchviewResult->tutor_class_dur_hrspday=="1")
								          { $hrsdisplay="1 hour"; }
								          else
								          { $hrsdisplay=$batchviewResult->tutor_class_dur_hrspday." hours"; }
									$duration= $weekdisplay." course,".$daydisplay." week,".$hrsdisplay; 
			//echo $slot_location;
			$course_pdf=explode("by",$authUserNamespace->combinename);
			$duration_pdf=explode(",",$duration);
			$slot_location_pdf=explode("at",$slot_location);
			$slot_location_pdf_time=explode("/",$slot_location_pdf[0]);
			if($authUserNamespace->nostudents=="1")
			{
				$no_of_stud= " ".$authUserNamespace->nostudents." student (Rs.)";
			}
			else
			{
				$no_of_stud= " ".$authUserNamespace->nostudents." students (Rs.)";
			}
			//print_r($slot_location_pdf);
			try {
	       // create PDF
	       $pdf = new Zend_Pdf();

	       // create A4 page
	       $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
	       

	  		// draw a line at the top of the page
			$page->setLineColor(new Zend_Pdf_Color_Rgb(0,0,0));
			$page->drawLine(10, 790, ($page->getWidth()-10), 790);

			// draw another line near the bottom of the page
			$page->drawLine(10, 25, ($page->getWidth()-10), 25);

			// define image resource
			$image = Zend_Pdf_Image::imageWithPath('invoice/skillzot-logo.jpg');
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 10);
			// write image to page
			$page->drawImage($image, 25, 800, ($image->getPixelWidth()+25), (800+$image->getPixelHeight()));
		
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 8);
			$page->drawText('Contact us: +91 9820921404', 455, 820);
			$page->drawText('zot@skillzot.com', 500, 810);

			 // define font resource
	       	$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 12);
			$page->drawText("Order id: ".$PaymentID, 25, 750)
				 ->drawText('Billing address ', 180, 750)
				 ->drawText('Student details ', 300, 750)
				 ->drawText('Tutor details ', 440, 750);
		    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 10);
	        $page->drawText('Order date: '.date('d-m-Y',strtotime($DateCreated)), 25, 735);
	             //->drawText('Service tax: ', 25, 720);
			$page->drawText($authUserNamespace->paymentname, 180, 735)
	             ->drawText($authUserNamespace->paymentaddress, 180, 720)
	             ->drawText($authUserNamespace->paymentcity, 180, 705)
	             ->drawText($authUserNamespace->paymentpostalcode.", ", 180, 690)
	             ->drawText($authUserNamespace->paymentstate, 220, 690)
	             ->drawText($authUserNamespace->paymentphone, 180, 675);

			$page->drawText($name1, 300, 735)
	             ->drawText($email1, 300, 720)
	             ->drawText($contact1, 300, 705);

	        $page->drawText($tutorname, 440, 735)
	             ->drawText($tutorcompany, 440, 720)
	             ->drawText($tutoremail, 440, 705)
	             ->drawText($tutorcontact, 440, 690);
	             	  
	        $page->drawLine(10, 665, ($page->getWidth()-10), 665);
	        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);
	      	$page->drawText("Course details", 10, 650)
				 ->drawText('Slot & location', 120, 650)
				 ->drawText('Duration', 220, 650)
				 ->drawText('Price of the course', 300, 650)
				 ->drawText('Quantity', 410, 650)
				 ->drawText('Price for'.$no_of_stud, 460, 650);
			$page->drawLine(10, 640, ($page->getWidth()-10), 640);
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

	/*$text1 = preg_replace('/[\r\n]+/', '\n', $course_pdf[1]);
	$text2 = wordwrap($text1,20, '<br/>', true);
	$text = nl2br($text1);*/
	$textChunk = wordwrap($course_pdf[1], 20, '\n');
	$line=600;
	foreach(explode('\n', $textChunk) as $textLine){
	  if ($textLine!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine)), 10, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	$textChunk1 = wordwrap($course_pdf[0], 20, '\n');
	$line=620;
	foreach(explode('\n', $textChunk1) as $textLine1){
	  if ($textLine1!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine1)), 10, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	$textChunk2 = wordwrap($slot_location_pdf[0], 120, '\n');
	$line=620;
	foreach(explode('\n', $textChunk2) as $textLine2){
	  if ($textLine2!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine2)), 120, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	$textChunk3 = wordwrap($slot_location_pdf_time[1], 120, '\n');
	$line=610;
	foreach(explode('\n', $textChunk3) as $textLine2){
	  if ($textLine2!=='') {
	  	$page->setFont($font, 8);
	    $page->drawText(strip_tags(ltrim($textLine2)), 120, $line, 'UTF-8');
	    $line -=14;
	  }
	}
	      	$page->setFont($font, 8);
			//$page->drawText($course_pdf[0], 10, 620)
	        //$page->drawText($slot_location_pdf[0], 120, 620)
	        $page->drawText($duration_pdf[0], 220, 620)
	             ->drawText($authUserNamespace->tutorrate, 320, 620)
	             ->drawText($authUserNamespace->nostudents, 420, 620)
	             ->drawText($authUserNamespace->price, 510, 620);
	        $page->drawText("by", 20, 610)
	             ->drawText("at", 125, 600)
	             ->drawText($duration_pdf[1], 220, 610);
	        //$page->drawText($course_pdf[1], 10, 600)
	        $page->drawText($slot_location_pdf[1], 120, 590)
	             ->drawText($duration_pdf[2], 220, 600);
	        $page->drawLine(10, 560, ($page->getWidth()-10), 560);
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);    
	        $page->drawText("Internet handling charges (Rs)", 335, 540)
	        	 ->drawText($authUserNamespace->internet, 520, 540);   
	        $page->drawLine(10, 530, ($page->getWidth()-10), 530);
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);
	      	$page->drawText("Grand Total (Rs)", 335, 510)
	        	 ->drawText($authUserNamespace->grandprice, 510, 510);
	       	$page->drawLine(10, 490, ($page->getWidth()-10), 490);
	       	$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_ITALIC );
	      	$page->setFont($font, 6);
	      	$page->drawText("This is a computer generated invoice. No signature required", 230, 480);
			// add footer text 
			$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	      	$page->setFont($font, 10);   
			$page->drawText('Copyright 2015 Skillzot. All rights reserved.', ($page->getWidth()/3), 10);
	        $page->drawText('Declaration ', 20, 440);
	        $page->drawText('This is a system generated invoice and does not reqiured signature ', 20, 400);
	        $page->drawText('For queries regarding this enrollment please contact  ', 20, 360);
	        $page->drawText('Cancellation Policy  ', 20, 320);
	        $page->drawText('Teacher Cancellation Policy after confirmation ', 20, 240);
	        


	        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	      	$page->setFont($font, 8);
			$page->drawText("we declare that this invoice shows actual price of the services desribed inclusive of taxes and all particulars are true and correct.", 20, 420);
	        $page->drawText("Thank you for enrolling through skillzot.com, we look forward to serve you.", 20, 380);
	        $page->drawText("Phone: +919820921404, Email ID: zot@skillzot.com", 20, 340);
	         
	        $page->drawText("You will get a full refund if you decide not join the course within 10 days.", 20, 300);
	        $page->drawText("We will follow up with you to co-ordinate the same once you put in your request by sending an e-mail on zot@skillzot.com.", 20, 290);
	        $page->drawText("If you decide not to join after 10 days, then you can still avail refund with a 2% deduction within 30 days. ", 20, 280);
	        $page->drawText("You will get the refund with 07 business days after you put in your request.", 20, 270);
	        $page->drawText("Once you enroll with the teacher and there is confirmation from both you and teacher, the cancellation policies of the teacher as mentioned below applies.", 20, 260); 
	        $page->drawText("Once you enroll with the teacher and there confirmation from both you and the teacher, then there is no refund applicable.", 20, 220);
	       // add page to document
	       $pdf->pages[] = $page;
	     

	       // save as file
	       
	       $pdf->save("invoice/".$PaymentID."-".date('d-m-Y').".pdf");
	      // echo 'SUCCESS: Document saved!';
	   } catch (Zend_Pdf_Exception $e) {
	       die ('PDF error: ' . $e->getMessage());
	   } catch (Exception $e) {
	       die ('Application error: ' . $e->getMessage());
	   }
	   // sms send to person making payment
		$username='madonlinepltd';
		//ini_set("display_errors",1);
	    $password ='Onlinehttp';
	    $senderid = 'SKLZOT';
	    $udh = '0';
		$message = "This is a confirmation of your booking ID: ".$PaymentID.", amounting to Rs. ".$authUserNamespace->grandprice." for your class with ".$name.". Thank you for choosing skillzot.com.";
		//$message = "This is a confirmation of your booking ID: ".$PaymentID.", amounting to Rs. ".$authUserNamespace->grandprice." for your class with ".$name.". Thank you for choosing skillzot.com.";
	   	//print_r($message);exit;
	    $dlrMask = '19';
		$mobileno =	$authUserNamespace->paymentphone;
		//$mobileno =	'8454822903';
		$url = 'http://www.myvaluefirst.com/smpp/sendsms';
		$fields = array(
					'username' => urlencode($username),
					'password' => urlencode($password),
					'to' => urlencode($mobileno),
					'udh' => urlencode($udh),
					'from' => urlencode($senderid),
					'text' => urlencode($message),
					'dlr-mask' => urlencode($dlrMask)
		);
		$fields_string="";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		$fields_string = rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		curl_close($ch);
		//-----------------------------------------------------------
		
		
		// mail send to person making payment
		$message1 = "<div>Hi ".$authUserNamespace->paymentname.",<br/></div><br/>";	
		$message1 .= "<div>This is a confirmation of your booking through skillzot.com. <br/></div><br/>"; 
		$message1 .= "<div>Here is the summary: <br/></div><br/>";
		$message1 .= "<div>Name : ".$authUserNamespace->paymentname."<br/></div>";
		//$message1 .= "<div>Your order id number : ".$response['PaymentID']."<br/></div>";
		//$message1 .= "<div>Date of booking : ".$response['DateCreated']."<br/></div>";
		$message1 .= "<div>Your order id number : ".$PaymentID."<br/></div>";
		$message1 .= "<div>Date of booking : ".$DateCreated."<br/></div>";
		$message1 .= "<div>Class booked : " .$authUserNamespace->combinename."<br/></div>";
		$message1 .= "<div>No. of students :  ".$authUserNamespace->nostudents."<br/></div>";
		$message1 .= "<div>Price of course per student :  ".$authUserNamespace->tutorrate."<br/></div>";
		$message1 .= "<div>Total amount :  ".$authUserNamespace->grandprice."<br/></div><br/>";
		$message1 .= "<div>Please take a print of this confirmation or sms confirmation with a valid photo id at the time <br/>of your first class. In case of any clarifications feel free to call us on +91 9820921404 (Mon-<br/>Fri, 10am to 5:30pm) or email us on zot@skillzot.com<br/></div><br/>";
		$message1 .= "<div>Thank you for using skillzot.com and feel free to stalk us on facebook for fun skill related happenings and more.<br/></div><br/>";
		$message1 .= "<div>Hope you have an awesome time nurturing your talent.<br/></div><br/>";
		$message1 .= "<div>Cheers,<br/></div>";
		$message1 .= "<div>Sandeep<br/></div>";
		$message1 .= "<div>Co-founder @ Skillzot</div>";
		//print_r($message1);exit;
		$sbNameTemp = "Booking confirmation with ".$name;
		$message = $message1;
		$to = $email;
		$from = "zot@skillzot.com";
		$mail = new Zend_Mail();
		$mail->setBodyHtml($message);
		$mail->setFrom($from,"Sandeep @ Skillzot");
		$mail->addTo($to,$authUserNamespace->paymentname); //ask
		$mail->setSubject($sbNameTemp);//ask
		$content = file_get_contents("invoice/".$PaymentID."-".date('d-m-Y').".pdf"); // e.g. ("attachment/abc.pdf")
		$attachment = new Zend_Mime_Part($content);
		$attachment->type = 'application/pdf';
		$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
		$attachment->encoding = Zend_Mime::ENCODING_BASE64;
		$attachment->filename = $PaymentID."-".date('d-m-Y').".pdf"; // name of file
		$mail->addAttachment($attachment);
		$sent = true;
		try {
		$mail->send();
		} catch (Exception $e){
		$sent = false;
		}

		//-----------------------------------------------------------

				
		//-------------sms to teacher----------------------
		$message1 = "Hi, you've got a new student through Skillzot.com. Please check your email for details.";
	    $dlrMask = '19';
	   	$mobileno =	$tutorData->tutor_mobile;
		//$mobileno ='8454822903';
		$url = 'http://www.myvaluefirst.com/smpp/sendsms';
		$fields = array(
					'username' => urlencode($username),
					'password' => urlencode($password),
					'to' => urlencode($mobileno),
					'udh' => urlencode($udh),
					'from' => urlencode($senderid),
					'text' => urlencode($message1),
					'dlr-mask' => urlencode($dlrMask)
		);
		$fields_string="";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		$fields_string = rtrim($fields_string, '&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		curl_close($ch);

		//-----------------------------------------------------------
		
		// mail send to teacher
		$message2 = "<div>Hi ".$name.",<br/></div><br/>";	
		$message2 .= "<div>We're happy to inform you that we've got a new student for you, someone who's eager to<br/> follow your footsteps.<br/></div><br/>"; 
		$message2 .= "<div>Here is the summary: <br/></div><br/>";
		$message2 .= "<div>Name : " .$authUserNamespace->paymentname."<br/></div>";
		$message2 .= "<div>Your order id number : " .$PaymentID."<br/></div>";
		$message2 .= "<div>Date of booking : " .$DateCreated."<br/></div>";
		/*$message1 .= "<div>Your order id number : ".$PaymentID."<br/></div>";
		$message1 .= "<div>Date of booking : ".$DateCreated."<br/></div>";*/
		$message2 .= "<div>Class booked : " .$authUserNamespace->combinename."<br/></div>";
		$message2 .= "<div>No. of students :  " .$authUserNamespace->nostudents."<br/></div><br/>";
		$message2 .= "<div>Here's to nurturing talent.<br/></div><br/>";
		$message2 .= "<div>Cheers,<br/></div>";
		$message2 .= "<div>Sandeep<br/></div>";
		$message2 .= "<div>Co-founder @ Skillzot</div>";
		$sbNameTemp = "You've got a new student";
		$message = $message2;
		//print_r($message2);exit;
		$to = $tutorData->tutor_email;
		$from = "zot@skillzot.com";
		$mail = new Zend_Mail();
		$mail->setBodyHtml($message);
		$mail->setFrom($from,"Sandeep @ Skillzot");
		$mail->addTo($to,$name); //ask
		$mail->setSubject($sbNameTemp);//ask
		$sent = true;
		try {
		$mail->send();
		} catch (Exception $e){
		$sent = false;
		}
	}			
}
public function googlemap1Action()
{	
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
		 	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		 	$tutorid=$this->_request->getParam("batchid");

		 	$branchData = $branchdetailObj->fetchRow($branchdetailObj->select()
											->setIntegrityCheck(false)
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('distinct(locality)','pincode','landmark','address','tutor_id'))
											->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array('address_value as localityname'))
											->joinLeft(array('n'=>DATABASE_PREFIX."master_address"),'c.city=n.address_id',array('address_value'))
											->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
											->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('tutor_course_name'))
											->where("c.id='$tutorid'")
											->order(array("m.address_value asc")));
											
			$profilecompany = $tutorProfileobj->fetchRow("id ='$branchData->tutor_id'");
			$fetchcompany_name="";
			 if (isset($profilecompany) && sizeof($profilecompany)>0)
			 {
					 $fetchcompany_name = $profilecompany->company_name;
					 if($fetchcompany_name=="" && empty($fetchcompany_name))
					 {
						 $fetchtutor_first_name = $profilecompany->tutor_first_name;
						 $fetchtutor_last_name = $profilecompany->tutor_last_name;
						 $fetchcompany_name = $fetchtutor_first_name." ".$fetchtutor_last_name;
					 }
					 /*if($fetchcompany_name=="" && empty($fetchcompany_name) && $fetchtutor_mobile=="" && empty($fetchtutor_mobile))
					 {
				     	$fetchcompany_name	 = $profilecompany->tutor_landline;						
					 }*/					
			 }
		 	$this->view->fetchcompany_name=$fetchcompany_name;								
											
			$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
											->setIntegrityCheck(false)
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('distinct(locality)','pincode','landmark','address','tutor_id'))
											->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array('address_value as localityname'))
											->joinLeft(array('n'=>DATABASE_PREFIX."master_address"),'c.city=n.address_id',array('address_value'))
											->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
											->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('tutor_course_name'))
											->where("c.id='$tutorid'")
											->order(array("m.address_value asc")));		
					$i=0;
					$lat=array();
					$long=array();
					$passaddress=array();
					foreach ($branchData as $branchDataaddress)
					{
							$address1[$i] = $branchDataaddress->address;	
							$localityname[$i] = $branchDataaddress->localityname;
							$pincode[$i] = $branchDataaddress->pincode;
							$City[$i] = $branchDataaddress->address_value;
							$Coursename[$i] = $branchDataaddress->tutor_course_name;	
							//echo '<br>'.$address1[$i].'<br>'.'<br>Lat: '.$localityname[$i].'<br>Long: '.$pincode[$i].'<br>'.$City[$i].'<br>'.$Coursename[$i];exit;			
							//echo 	$address[$i]."---".$localityname[$i]."--".$pincode[$i]."---".$address_value[$i]."<br>";exit;					
							if($pincode[$i]!="0" && $pincode[$i]!="")
							{
								$address = $address1[$i].",".$localityname[$i].",".$City[$i].",".$pincode[$i];														
								$address = urlencode($address);	
							}
							else
							{
								$address = $address1[$i].",".$localityname[$i].",".$City[$i];														
								$address = urlencode($address);	
							}												  
							/*echo $address;exit;
							 $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
							  $output= json_decode($geocode);*/		
							
							$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=true&region=India";
							//echo $url;exit;
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							$response = curl_exec($ch);
							curl_close($ch);
							$response_a = json_decode($response);
							/*echo $lat = $response_a->results[0]->geometry->location->lat;
							echo "<br />";
							echo $long = $response_a->results[0]->geometry->location->lng;exit;*/				 		
				 			if(isset($response_a->results[0]->geometry->location->lat) &&  $response_a->results[0]->geometry->location->lng!="")
				 			{				 			
					 			$passaddress[$i] =$address1[$i].",".$localityname[$i].",".$City[$i].",".$pincode[$i];
								$lat[$i] = $response_a->results[0]->geometry->location->lat;
								$long[$i] = $response_a->results[0]->geometry->location->lng;
								$course_name[$i]=$Coursename[$i];
				 			}
				 			else
				 			{
				 				 $lat[$i]=0;
				 				 $long[$i]= 0;
				 				 $passaddress[$i]=$address1[$i].",".$localityname[$i].",".$City[$i].",".$pincode[$i];
				 				 $course_name[$i]=$Coursename[$i];
				 			}
							//echo '<br>'.$address.'<br>'.'<br>Lat: '.$lat[$i].'<br>Long: '.$long[$i];exit;
						$i++;
					}
							$this->view->lat=$lat;						
							$this->view->long=$long;									
							$this->view->fulladdress=$passaddress;	
							$this->view->course_name=$course_name;				
		 	/*echo "hi".$locality;exit;
		 	  echo "<script>window.location.href=googlemap;</script>";		 	
		 	  googlemap.html?lati=37&longi=-122.1419;*/
}
public function googlemapAction()
{	
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
		 	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		 	$tutorid=$this->_request->getParam("tutorid");
		 	//$courseid=$this->_request->getParam("courseid");
		 	
//		 	PRINT $branchdetailObj->select()
//												->setIntegrityCheck(false)
//												->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)','pincode','landmark','address'))
//												->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array('address_value as locality'))
//												->joinLeft(array('n'=>DATABASE_PREFIX."master_address"),'c.city=n.address_id',array('address_value'))
//												->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
//												->where("c.tutor_id='$tutorid' && c.course_id='$courseid'")
//												 ->order(array("m.address_value asc"));EXIT;
			$profilecompany = $tutorProfileobj->fetchRow("id ='$tutorid'");
			$fetchcompany_name="";
			 if (isset($profilecompany) && sizeof($profilecompany)>0)
			 {
					 $fetchcompany_name = $profilecompany->company_name;
					 if($fetchcompany_name=="" && empty($fetchcompany_name))
					 {
						 $fetchtutor_first_name = $profilecompany->tutor_first_name;
						 $fetchtutor_last_name = $profilecompany->tutor_last_name;
						 $fetchcompany_name = $fetchtutor_first_name." ".$fetchtutor_last_name;
					 }
					 /*if($fetchcompany_name=="" && empty($fetchcompany_name) && $fetchtutor_mobile=="" && empty($fetchtutor_mobile))
					 {
				     	$fetchcompany_name	 = $profilecompany->tutor_landline;						
					 }*/					
			 }
		 	$this->view->fetchcompany_name=$fetchcompany_name;
//		 	print $branchdetailObj->select()
//											->setIntegrityCheck(false)
//											->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)','pincode','landmark','address'))
//											->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array('address_value as localityname'))
//											->joinLeft(array('n'=>DATABASE_PREFIX."master_address"),'c.city=n.address_id',array('address_value'))
//											->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
//											->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.tutor_course_id=o.id',array('tutor_course_name'))
//											->where("c.tutor_id='$tutorid' && c.course_id='$courseid'")
//											->order(array("m.address_value asc"));exit;
		 	$branchData = $branchdetailObj->fetchAll($branchdetailObj->select()
											->setIntegrityCheck(false)
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('distinct(locality)','pincode','landmark','address'))
											->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array('address_value as localityname'))
											->joinLeft(array('n'=>DATABASE_PREFIX."master_address"),'c.city=n.address_id',array('address_value'))
											->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
											->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.course_id=o.id',array('tutor_course_name'))
											->where("c.tutor_id='$tutorid'")
											->order(array("m.address_value asc")));
					
					$i=0;
					$lat=array();
					$long=array();
					$passaddress=array();
					foreach ($branchData as $branchDataaddress)
					{
							$address1[$i] = $branchDataaddress->address;
							$localityname[$i] = $branchDataaddress->localityname;
							$pincode[$i] = $branchDataaddress->pincode;
							$City[$i] = $branchDataaddress->address_value;
							$Coursename[$i] = $branchDataaddress->tutor_course_name;							
							//echo 	$address[$i]."---".$localityname[$i]."--".$pincode[$i]."---".$address_value[$i]."<br>";exit;					
							if($pincode[$i]!="0" && $pincode[$i]!="")
							{
								$address = $address1[$i].",".$localityname[$i].",".$City[$i].",".$pincode[$i];														
								$address = urlencode($address);	
							}
							else
							{
								$address = $address1[$i].",".$localityname[$i].",".$City[$i];														
								$address = urlencode($address);	
							}												  
							/*echo $address;exit;
							 $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
							  $output= json_decode($geocode);*/		
							  					
							$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=true&region=India";
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							$response = curl_exec($ch);
							curl_close($ch);
							$response_a = json_decode($response);
							/*echo $lat = $response_a->results[0]->geometry->location->lat;
							echo "<br />";
							echo $long = $response_a->results[0]->geometry->location->lng;exit;*/				 		
				 			if(isset($response_a->results[0]->geometry->location->lat) &&  $response_a->results[0]->geometry->location->lng!="")
				 			{				 			
					 			$passaddress[$i] =$address1[$i].",".$localityname[$i].",".$City[$i].",".$pincode[$i];
								$lat[$i] = $response_a->results[0]->geometry->location->lat;
								$long[$i] = $response_a->results[0]->geometry->location->lng;
								$course_name[$i]=$Coursename[$i];
				 			}
				 			else
				 			{
				 				 $lat[$i]=0;
				 				 $long[$i]= 0;
				 				 $passaddress[$i]=$address1[$i].",".$localityname[$i].",".$City[$i].",".$pincode[$i];
				 				 $course_name[$i]=$Coursename[$i];
				 			}
							//echo '<br>'.$address.'<br>'.'<br>Lat: '.$lat[$i].'<br>Long: '.$long[$i];exit;
						$i++;
					}
							$this->view->lat=$lat;						
							$this->view->long=$long;									
							$this->view->fulladdress=$passaddress;	
							$this->view->course_name=$course_name;				
		 	/*echo "hi".$locality;exit;
		 	  echo "<script>window.location.href=googlemap;</script>";		 	
		 	  googlemap.html?lati=37&longi=-122.1419;*/
}
}
?>
