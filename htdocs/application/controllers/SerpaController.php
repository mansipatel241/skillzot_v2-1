<?php

class SerpaController extends Zend_Controller_Action{

	public function init(){
		
		$this->_helper->layout()->setLayout("serpa");
	}

	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(isset($authUserNamespace->user_id) || $authUserNamespace->user_id!="")$this->_redirect("/serpa/home");
		
		$this->_helper->layout()->setLayout("loginserpa");
		
		if($this->_request->isPost()){
		
			$username = $this->_request->getParam('username');
			$password = $this->_request->getParam('password');
			$usernameescape = $username;
			$passwordescape = md5($password);
			$adminlogin = new Skillzot_Model_DbTable_Serpalogin();
			$user_row = $adminlogin->fetchRow($adminlogin->select()
											 ->where("serpa_uname='$usernameescape' && serpa_pwd='$passwordescape' && is_active=1"));

			if($user_row!="" && sizeof($user_row)>0){
			
				$authUserNamespace->user_id1 = $user_row->id;
				$authUserNamespace->serpa_uname1 = $user_row->serpa_uname;
				
				$this->view->msg = "";
				
				$this->_redirect('/serpa/erp');
			
			}else{
				$this->view->msg = "inactive";
			}
		}
	}

	public function homeAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$authUserNamespace->admin_page_title = "SERPA Home";
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$jobtableNew = $jobtableobj->fetchRow($jobtableobj->select()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('count(c.state) as count'))
						    ->where("c.state='NEW'"));
		$this->view->jobtableNew = $jobtableNew;
		$jobtableOpen = $jobtableobj->fetchRow($jobtableobj->select()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('count(c.state) as count'))
						    ->where("c.state='OPEN'"));
		$this->view->jobtableOpen = $jobtableOpen;
		$jobtableClosed = $jobtableobj->fetchRow($jobtableobj->select()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('count(c.state) as count'))
						    ->where("c.state='CLOSED'"));
		$this->view->jobtableClosed = $jobtableClosed;

		$checklistPending = $jobtableobj->fetchRow($jobtableobj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."job_table"),array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'c.job_id=n.job_id',array('count(n.task) as count'))
						    ->where("c.state='OPEN' and n.done_flag='0'"));
		$this->view->checklistPending = $checklistPending;

		$checklistDone = $jobtableobj->fetchRow($jobtableobj->select()
							->setIntegrityCheck(false)
							->from(array('c'=>DATABASE_PREFIX."job_table"),array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'c.job_id=n.job_id',array('count(n.task) as count'))
						    ->where("c.state='OPEN' and n.done_flag='1'"));
		$this->view->checklistDone = $checklistDone;
		$sevendaysbefore = date('Y-m-d', strtotime('-7 days'));
		$thirtydaysbefore = date('Y-m-d', strtotime('-30 days'));
		$jobtableBeforeSeven = $jobtableobj->fetchRow($jobtableobj->select()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('count(c.state) as count'))
						    ->where("c.state='OPEN' and date(c.date_next_followup) <= '$sevendaysbefore'"));
		$this->view->jobtableBeforeSeven = $jobtableBeforeSeven;

		$jobtableBeforeThirty = $jobtableobj->fetchRow($jobtableobj->select()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('count(c.state) as count'))
						    ->where("c.state='OPEN' and date(c.date_next_followup) <= '$thirtydaysbefore'"));
		$this->view->jobtableBeforeThirty = $jobtableBeforeThirty;

	}
	public function erpAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$authUserNamespace->admin_page_title = "ERP Manager";

		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		if($records_per_page==""){
		$records_per_page = $this->_request->getParam('getPageValue');
		$this->view->records_per_page = $records_per_page;
		}
		$filter_search = $this->_request->getParam('filter');
		$this->view->filter_search = $filter_search;
		//$jobtableResult = $jobtableobj->fetchAll($jobtableobj->select()
						//->from(array('m'=>DATABASE_PREFIX."job_table"))
						//->where("m.state='NEW'")
						//->order(array("date_new desc")));
		$date=date("Y-m-d");
		if(isset($filter_search) && $filter_search=="Done")
		{
			$jobtableResult = $jobtableobj->fetchAll($jobtableobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('*'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'n.job_id=c.job_id',array('group_concat(DISTINCT if(n.done_flag="0", "NO", "YES"),",",n.task,",",date(n.date_due) SEPARATOR "\n") as checklist'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."log"),'c.job_id=o.job_id',array('*'))
						    ->where("c.state='CLOSED'")
						    ->group(array("c.job_id"))
							->order(array('c.date_new desc')));
		}elseif (isset($filter_search) && $filter_search=="All") {
			$jobtableResult = $jobtableobj->fetchAll($jobtableobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('*'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'n.job_id=c.job_id',array('group_concat(DISTINCT if(n.done_flag="0", "NO", "YES"),",",n.task,",",date(n.date_due) SEPARATOR "\n") as checklist'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."log"),'c.job_id=o.job_id',array('*'))
						    ->where("1=1")
						    ->group(array("c.job_id"))
							->order(array('c.date_new desc')));
		}elseif (isset($filter_search) && $filter_search=="Upcoming") {
			$jobtableResult = $jobtableobj->fetchAll($jobtableobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('*'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'n.job_id=c.job_id',array('group_concat(DISTINCT if(n.done_flag="0", "NO", "YES"),",",n.task,",",date(n.date_due) SEPARATOR "\n") as checklist'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."log"),'c.job_id=o.job_id',array('*'))
						    ->where("(c.state='NEW' || c.state='OPEN') && date(c.date_next_followup) > '$date' ")
						    ->group(array("c.job_id"))
							->order(array('c.date_new desc')));
		}else{

		$jobtableResult = $jobtableobj->fetchAll($jobtableobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('*'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'n.job_id=c.job_id',array('group_concat(DISTINCT if(n.done_flag="0", "NO", "YES"),",",n.task,",",date(n.date_due) SEPARATOR "\n") as checklist'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."log"),'c.job_id=o.job_id',array('*'))
						    ->where("(c.state='NEW' || c.state='OPEN') && date(c.date_next_followup) <= '$date' ")
						    ->group(array("c.job_id"))
							->order(array('c.date_new desc')));
		}
		if(isset($jobtableResult) && $jobtableResult!="")
		{
			$this->view->jobtableResult = $jobtableResult;
		}

		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($jobtableResult);
		$paginator = Zend_Paginator::factory($jobtableResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->jobtableResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
	}
	public function tutordetailsAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorclassnameObj = new Skillzot_Model_DbTable_Tutorclasstype();
		$tutoradressObj = new Skillzot_Model_DbTable_Branchdetails();
		$tutoradressObj1 = new Skillzot_Model_DbTable_Address();

		$tutor_id = $this->_request->getParam("tutorid");

		if(isset($tutor_id) && $tutor_id!="")
		{
		$tutorsignupResult = $tutorProfileobj->fetchRow($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('c.id as id','c.userid as userid','c.userid_new as userid_new','c.lastupdatedate as lastupdatedate','c.tutor_first_name as tutor_first_name','c.tutor_last_name as tutor_last_name','c.tutor_mobile as tutor_mobile','c.tutor_email as tutor_email','c.tutor_landline as tutor_landline','c.company_name as company_name','c.editdate as editdate','c.followup_comment as followup_comment','if(c.is_active!="1","Yes","No") as active','(CASE WHEN tutor_class_type="1" THEN "Personal lessons" WHEN tutor_class_type="2" THEN "Group classes" WHEN tutor_class_type="3" THEN "Both" ELSE ""  END) AS class_type_name'))
							->joinLeft(array('o'=>DATABASE_PREFIX."tx_tutor_skill_course"),'c.id=o.tutor_id',array('o.id as courseid','o.tutor_course_name as tutor_course_name','o.tutor_rate as tutor_rate'))
							->joinLeft(array('m'=>DATABASE_PREFIX."master_skills"),'o.tutor_skill_id=m.skill_id',array(''))
							->joinLeft(array('n'=>DATABASE_PREFIX."tx_tutor_course_batch"),'n.course_id=o.id',array('n.address as address'))
							->joinLeft(array('l'=>DATABASE_PREFIX."master_address"),'l.address_id=n.locality',array('l.address_value as city'))  
							->where("c.id='$tutor_id'"));
		$this->view->tutorsignupResult = $tutorsignupResult;
		}
	}
	public function studentdetailsAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$student_id = $this->_request->getParam("studentid");

		if(isset($student_id) && $student_id !="")
		{
		$studentsignupResult = $studentSignupObj->fetchRow($studentSignupObj->select()
						->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
						->where("m.id='$student_id'"));
		$this->view->studentsignupResult = $studentsignupResult;
		}
	}
	public function addjobAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$authUserNamespace->admin_page_title = "Add Job";
		if($this->_request->isPost()){
			$DueDate = $this->_request->getParam("DueDate");
			if(isset($DueDate) && $DueDate!="")
			{
				$explode_date=explode("/",$DueDate);
				$date_final= $explode_date[2].'-'.$explode_date[0].'-'.$explode_date[1].' '.date('h:i:s', time());
				//echo "date".$date_final;
				$erp_step2_due_date = date('Y-m-d h:i:s', strtotime($date_final. '+3 day'));
				//echo "date....".$erp_step2_due_date;exit;
				$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($date_final. '+7 days'));
				$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($date_final. '+10 days'));
				$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($date_final. '+30 days'));
			}else{
				$date_final = date('Y-m-d h:i:s');
				$erp_step2_due_date = date('Y-m-d h:i:s', strtotime('+3 days'));
				$erp_step3_due_date = date('Y-m-d h:i:s', strtotime('+7 days'));
				$erp_step4_due_date = date('Y-m-d h:i:s', strtotime('+10 days'));
				$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime('+30 days'));
			}

			$Type= $this->_request->getParam("Type");
			$StudentId = $this->_request->getParam("StudentId");
			$TutorId= $this->_request->getParam("TutorId");
			$hiddenid= $this->_request->getParam("hiddenvar");

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($DueDate == "")$response["data"]["DueDate"] = "valid";
				else $response["data"]["DueDate"] = "valid";
				if($Type == "")$response["data"]["Type"] = "null";
				else $response["data"]["Type"] = "valid";
				if($StudentId == "")
				{
					if($Type == 'STUDENT_LEAD')
					{
						$response["data"]["StudentId"] = "null";
					}else{
						$response["data"]["StudentId"] = "valid";
					}
				}		
				else 
					{
						$response["data"]["StudentId"] = "valid";
					}
				if($TutorId == ""){

					if($Type == 'STUDENT_LEAD')
					{
						$response["data"]["TutorId"] = "null";
					}elseif($Type == 'PROFILE_CHANGE')
					{
						$response["data"]["TutorId"] = "null";
					}
					else{
						$response["data"]["TutorId"] = "valid";
					}
				}		
				else 
					{
						$response["data"]["TutorId"] = "valid";
					}

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
		}else{
			
			if(isset($Type)){
				if($Type == 'STUDENT_LEAD')
				{
					$data = array("state"=>"NEW","reason"=>"NULL","date_new"=>date('Y-m-d h:i:s'),"date_first_followup"=>"0000-00-00 00:00:00","date_last_followup"=>"0000-00-00 00:00:00","date_close"=>"0000-00-00 00:00:00",
						"date_next_followup"=>$date_final,"type"=>$Type,"tutor_id"=>$TutorId,"student_id"=>$StudentId);
				}elseif($Type == 'PROFILE_CHANGE')
				{
					$data = array("state"=>"NEW","reason"=>"NULL","date_new"=>date('Y-m-d h:i:s'),"date_first_followup"=>"0000-00-00 00:00:00","date_last_followup"=>"0000-00-00 00:00:00","date_close"=>"0000-00-00 00:00:00",
					"date_next_followup"=>$date_final,"type"=>$Type,"tutor_id"=>$TutorId,"student_id"=>"NULL");
				}else{
					$data = array("state"=>"NEW","reason"=>"NULL","date_new"=>date('Y-m-d h:i:s'),"date_first_followup"=>"0000-00-00 00:00:00","date_last_followup"=>"0000-00-00 00:00:00","date_close"=>"0000-00-00 00:00:00",
					"date_next_followup"=>$date_final,"type"=>$Type,"tutor_id"=>$TutorId,"student_id"=>$StudentId);
				}
				$jobtableobj->insert($data);
				$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();	
				$data = array("job_id"=>$jobSessionId,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>"Job Manually Created By Admin");
				$logobj->insert($data);
				if($Type == 'STUDENT_LEAD')
				{
					
					$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"TUTOR_RECO","date_due"=>$date_final,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist2= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_1","date_due"=>$date_final,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist3= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_2","date_due"=>$erp_step2_due_date,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist4= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_3","date_due"=>$erp_step3_due_date,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist5= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_4","date_due"=>$erp_step4_due_date,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0"); 
					$data_for_checklist6= array("job_id"=>$jobSessionId,"task"=>"TUTOR_FEEDBACK","date_due"=>$tutor_feedback_due_date,"date_created"=>$lastupdate,
					"date_done"=>"NULL","done_flag"=>"0");
					
					
					$checklistobj->insert($data_for_checklist1);
					$checklistobj->insert($data_for_checklist2);
					$checklistobj->insert($data_for_checklist3);
					$checklistobj->insert($data_for_checklist4);
					$checklistobj->insert($data_for_checklist5);
					$checklistobj->insert($data_for_checklist6);
				}
				if($Type == 'PHONE_LEAD')
				{
					$data_for_checklist7= array("job_id"=>$jobSessionId,"task"=>"REGISTER_STUDENT","date_due"=>date('Y-m-d h:i:s'),"date_created"=>$lastupdate,
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"TUTOR_RECO","date_due"=>$date_final,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist2= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_1","date_due"=>$date_final,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist3= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_2","date_due"=>$erp_step2_due_date,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist4= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_3","date_due"=>$erp_step3_due_date,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$data_for_checklist5= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_4","date_due"=>$erp_step4_due_date,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0"); 
					$data_for_checklist6= array("job_id"=>$jobSessionId,"task"=>"TUTOR_FEEDBACK","date_due"=>$tutor_feedback_due_date,"date_created"=>$lastupdate,
					"date_done"=>"NULL","done_flag"=>"0");
					
					$checklistobj->insert($data_for_checklist7);
					$checklistobj->insert($data_for_checklist1);
					$checklistobj->insert($data_for_checklist2);
					$checklistobj->insert($data_for_checklist3);
					$checklistobj->insert($data_for_checklist4);
					$checklistobj->insert($data_for_checklist5);
					$checklistobj->insert($data_for_checklist6);
				}
				if($Type == 'PROFILE_CHANGE')
				{
					$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$date_final,"date_created"=>date('Y-m-d h:i:s'),
					"date_done"=>"NULL","done_flag"=>"0");
					$checklistobj->insert($data_for_checklist1);
				}
				//$authUserNamespace->status_message = "Add Job has been inserted successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/serpa/erp';</script>";
		}
	}
	}
	public function editchecklistAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();

		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		if(isset($id) && $id > 0)
		{
			$checklistobj= $checklistobj->fetchAll("job_id='$id'");
			$this->view->checklistobj = $checklistobj;
		}
		if(isset($id) && $id > 0)
		{
		$authUserNamespace->admin_page_title = "Edit Checklist";
		}

	}
	public function editchecklistdataAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();

		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		$redirectchecklistobj= $checklistobj->fetchRow("id='$id'");
		$check= $redirectchecklistobj->job_id;
		$log_entry = $redirectchecklistobj->task." Checklist Edit Manually By Admin.";
		if(isset($id) && $id > 0)
		{
		$authUserNamespace->admin_page_title = "Edit Checklist";
		$checklistobj1= $checklistobj->fetchRow("id='$id'");
		$this->view->editchecklistobj = $checklistobj1;
		}
		if($this->_request->isPost()){
			$Done = $this->_request->getParam("Done");
			if(isset($Done) && $Done!='')
			{
				$explode_done=explode("/",$Done);
				$Done_final= $explode_done[2].'-'.$explode_done[0].'-'.$explode_done[1].' '.date('h:i:s', time());
			}else
			{
				$Done_final="00:00:00 00:00:00";
			}
			$Task = $this->_request->getParam("Task");
			//echo $Task;exit;
			$Due = $this->_request->getParam("Due");
			if(isset($Due) && $Due !='')
			{
				$explode_due=explode("/",$Due);
				$Due_final= $explode_due[2].'-'.$explode_due[0].'-'.$explode_due[1].' '.date('h:i:s', time());
			}else{
				$Due_final="00:00:00 00:00:00";
			}
			
			$DoneFlag = $this->_request->getParam("DoneFlag");
			$hiddenid= $this->_request->getParam("hiddenvar");

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($Done == "")$response["data"]["Done"] = "valid";
				else $response["data"]["Done"] = "valid";
				if($Task != "")
				{
					$checklistobj2= $checklistobj->fetchRow("task='$Task' and id='$id'");
					//echo $checklistobj2->task;exit;
					$checklistobj1= $checklistobj->fetchRow("task='$Task' and job_id='$check'");
					
					if($checklistobj2->task == "$Task")
					{
						$response["data"]["Task"] = "valid";
					}else{
						if(isset($checklistobj1->task) && $checklistobj1->task !="")
						{
							$response["data"]["Task"] = "duplicate";
						}else{
						$response["data"]["Task"] = "valid";
						}
					}
				}else{
					$response["data"]["Task"] = "null";
				}
				if($Due == "")$response["data"]["Due"] = "valid";
				else $response["data"]["Due"] = "valid";
				if($DoneFlag == "")$response["data"]["DoneFlag"] = "null";
				else $response["data"]["DoneFlag"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
		}else{
			
			if(isset($id) && $id > 0){
					if(isset($DoneFlag) && $DoneFlag=="1")
					{ 
						//echo "if";exit;
						$data= array("task"=>$Task,"date_due"=>$Due_final,"date_done"=>date('Y-m-d h:i:s'),"done_flag"=>$DoneFlag);
						$checklistobj->update($data,"id='$id'");
						//$authUserNamespace->status_message = "Edit Checklist has been inserted successfully";
						$date_for_next_followup =  array("state"=>"OPEN","date_first_followup"=>date('Y-m-d h:i:s'),"date_last_followup"=>date('Y-m-d h:i:s'));
						$jobtableobj->update($date_for_next_followup,"job_id='$check'");

						$data = array("job_id"=>$check,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>$log_entry);
						$logobj->insert($data);

						$jobtableobj1= $jobtableobj->fetchRow("job_id='$check'");	
						
						$checkduedate= $checklistobj->fetchRow("id='$id'");
						if($checkduedate->task == 'TUTOR_RECO'){
							
							$erp_step2_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+3 day'));
							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+7 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+10 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+30 days'));

							$data_for_due_erp1 = array("date_due"=>$Due_final);
							$data_for_due_erp2 = array("date_due"=>$erp_step2_due_date);
							$data_for_due_erp3 = array("date_due"=>$erp_step3_due_date);
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp1,"job_id='$check' and task = 'ERP_STEP_1'");
							$checklistobj->update($data_for_due_erp2,"job_id='$check' and task = 'ERP_STEP_2'");
							$checklistobj->update($data_for_due_erp3,"job_id='$check' and task = 'ERP_STEP_3'");
							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");

						}elseif($checkduedate->task == 'ERP_STEP_1'){
							$erp_step2_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+3 day'));
							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+7 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+10 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+30 days'));
							
							$data_for_due_erp2 = array("date_due"=>$erp_step2_due_date);
							$data_for_due_erp3 = array("date_due"=>$erp_step3_due_date);
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp2,"job_id='$check' and task = 'ERP_STEP_2'");
							$checklistobj->update($data_for_due_erp3,"job_id='$check' and task = 'ERP_STEP_3'");
							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}elseif($checkduedate->task == 'ERP_STEP_2'){

							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+4 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+7 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+27 days'));
							
							$data_for_due_erp3 = array("date_due"=>$erp_step3_due_date);
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp3,"job_id='$check' and task = 'ERP_STEP_3'");
							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}elseif($checkduedate->task == 'ERP_STEP_3'){

							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+3 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+23 days'));
							
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}else{
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+20 days'));
							
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);
							
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}
						//$checklistobj3= $checklistobj->fetchRow("job_id='$check' and date_due <='$jobtableobj1->date_next_followup'");
						//if($checklistobj3=="")
						//{
							$checklistobj4 = $checklistobj->fetchRow($checklistobj->select()
								->from(array('c'=>DATABASE_PREFIX."checklist"),array('min(c.date_due) as date_due'))
							    ->where("job_id='$check' and done_flag='0'"));
							if(isset($checklistobj4->date_due) && $checklistobj4->date_due!="")
							{
							$date_for_next_followup =  array("date_next_followup"=>$checklistobj4->date_due);
							$jobtableobj->update($date_for_next_followup,"job_id='$check'");
							}
						//}

					}else{
						//echo "else";exit;
						$data= array("task"=>$Task,"date_due"=>$Due_final,"date_done"=>$Done_final,"done_flag"=>$DoneFlag);
						$checklistobj->update($data,"id='$id'");
						//$authUserNamespace->status_message = "Edit Checklist has been inserted successfully";
						$date_for_next_followup =  array("state"=>"OPEN","date_first_followup"=>date('Y-m-d h:i:s'),"date_last_followup"=>date('Y-m-d h:i:s'));
						$jobtableobj->update($date_for_next_followup,"job_id='$check'");

						$data = array("job_id"=>$check,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>$log_entry);
						$logobj->insert($data);
						$jobtableobj1= $jobtableobj->fetchRow("job_id='$check'");	
						
						$checkduedate= $checklistobj->fetchRow("id='$id'");
						if($checkduedate->task == 'TUTOR_RECO'){
							
							$erp_step2_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+3 day'));
							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+7 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+10 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+30 days'));

							$data_for_due_erp1 = array("date_due"=>$Due_final);
							$data_for_due_erp2 = array("date_due"=>$erp_step2_due_date);
							$data_for_due_erp3 = array("date_due"=>$erp_step3_due_date);
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp1,"job_id='$check' and task = 'ERP_STEP_1'");
							$checklistobj->update($data_for_due_erp2,"job_id='$check' and task = 'ERP_STEP_2'");
							$checklistobj->update($data_for_due_erp3,"job_id='$check' and task = 'ERP_STEP_3'");
							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");

						}elseif($checkduedate->task == 'ERP_STEP_1'){
							$erp_step2_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+3 day'));
							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+7 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+10 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+30 days'));
							
							$data_for_due_erp2 = array("date_due"=>$erp_step2_due_date);
							$data_for_due_erp3 = array("date_due"=>$erp_step3_due_date);
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp2,"job_id='$check' and task = 'ERP_STEP_2'");
							$checklistobj->update($data_for_due_erp3,"job_id='$check' and task = 'ERP_STEP_3'");
							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}elseif($checkduedate->task == 'ERP_STEP_2'){

							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+4 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+7 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+27 days'));
							
							$data_for_due_erp3 = array("date_due"=>$erp_step3_due_date);
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp3,"job_id='$check' and task = 'ERP_STEP_3'");
							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}elseif($checkduedate->task == 'ERP_STEP_3'){

							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+3 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+23 days'));
							
							$data_for_due_erp4 = array("date_due"=>$erp_step4_due_date);
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_due_erp4,"job_id='$check' and task = 'ERP_STEP_4'");
							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}else{
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime($Due_final. '+20 days'));
							
							$data_for_tutor_feedback = array("date_due"=>$tutor_feedback_due_date);

							$checklistobj->update($data_for_tutor_feedback,"job_id='$check' and task = 'TUTOR_FEEDBACK'");
						}

						//$checklistobj3= $checklistobj->fetchRow("job_id='$check' and date_due <='$jobtableobj1->date_next_followup'");
						//if($checklistobj3=="")
						//{
							$checklistobj4 = $checklistobj->fetchRow($checklistobj->select()
								->from(array('c'=>DATABASE_PREFIX."checklist"),array('min(c.date_due) as date_due'))
							    ->where("job_id='$check' and done_flag='0'"));
							if(isset($checklistobj4->date_due) && $checklistobj4->date_due!="")
							{
							$date_for_next_followup =  array("date_next_followup"=>$checklistobj4->date_due);
							$jobtableobj->update($date_for_next_followup,"job_id='$check'");
							}
						//}
					}
					
				
			}
			echo "<script>parent.Mediabox.close();</script>";
			//echo "<script>window.parent.location='". BASEPATH ."/serpa/editchecklist/id/'".$check.";</script>";
			//echo $this->_redirect('serpa/editchecklist/id/'.$check);
		}
	}

	}
	public function emailtemplateAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$authUserNamespace->admin_page_title = "ERP Template";
		$this->_helper->layout()->setLayout("lightboxnew");
		$id = $this->_request->getParam("id");
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$checklist= $checklistobj->fetchRow("id='$id'");
		if($checklist->task == 'TUTOR_RECO'){
							
			$tutor_message = "1. Greeting for Inbound: Message lead<br>
			- Friendly greeting , smile in the voice<br>
			- May I know who I am speaking with?<br>
			- Ask how they are doing today?<br>
			- How may I help?<br>
			2. Greeting For Outbound: Phone Lead<br>
			- Friendly greeting , smile in the voice<br>
			- Introduce yourself and why you are calling, ask if it’s a good time to speak<br>
			- Why am I calling – Following up on your inquiry to learn <Skill> contact <Teacher>?<br>
			3. Probing for additional details:<br>
			- What skill do you wish to learn?<br>
			- Are you looking for yourself or other(Kids, friend) <br>
			- You would prefer classes in an around which area(Where do you stay/work)<br>
			- Do you have any specifications for batch timings? – Morning, evening, weekends, Male/female teacher. <br>
			- Are you looking for teacher’s who can come home? <br>
			4. Email and registration: Action to be taken<br>
			- We’ll share the Tutor recommendations by mail and message.<br>
			- May I have your email id, so I can share the detailed profile with you<br>
			- Would you like me to register you on Skillzot with this email id for future communication purposes? It will help us follow up in case you need help to join the class<br>
			- Reassure that NO unnecessary marketing emailers will be sent.<br>
			5. Inform and make aware:<br>
			- You can now book your classes online through skillzot. Would you like to know more?<br>
			- If yes – Inform of booking and cancellation policy<br>
			- If no – Thank you for your time, please check your mailbox for our Tutor recommendations.<br>
			<b>Action: Email sent with Minimum 3 recommendations.</b><br>
			
			Hi Student Name,<br>
				Thank you for enquiring for <Skill Name> classes through www.skillzot.com. Based on your preferences, here are a few options that are best suited for you.<br>
				1 Company Name 1 - Tutor Name 1<br>
				a Phone:  <Tutor1 Contact1><br>
				b Locality: <Tutor 1  Locality 1 ><br>
				c For Batch details and Enrolment :< Tutor 1 URL><br>

				2 Company Name 2 - Tutor Name 2<br>
				a Phone:  <Tutor2 Contact1><br>
				b Locality: <Tutor 2 Locality 1 ><br>
				c For Batch details and Enrolment : <Tutor 2 URL><br>

				3 Company Name 3 - Tutor Name 3<br>
				a Phone:  <Tutor3 Contact1><br>
				b Locality: <Tutor 3 Locality 1 ><br>
				c For Batch details and Enrolment : <Tutor 3 URL><br>

				We hope the Teacher’s profile link gives you all the information you need to make up your mind and sign up.  If not, feel free to call them and do mention that you got the details from www.skillzot.com. 
				Please don’t hesitate to call or email us back in case you need any further assistance.<br>
				Cheers,<br>
				<Administrator Name><br>
				Reachable @ +91 9820921404 (between 10am to 5:30pm, Mon-Fri)<br>
				(Skillzot is an online portal to help people connect with the best teachers & classes to learn any skill)";


			}elseif($checkduedate->task == 'ERP_STEP_1'){
				$tutor_message = "1. Greeting for Inbound: Message lead<br>
			- Friendly greeting , smile in the voice<br>
			- May I know who I am speaking with?<br>
			- Ask how they are doing today?<br>
			- How may I help?<br>
			2. Greeting For Outbound: Phone Lead<br>
			- Friendly greeting , smile in the voice<br>
			- Introduce yourself and why you are calling, ask if it’s a good time to speak<br>
			- Why am I calling – Following up on your inquiry to learn <Skill> contact <Teacher>?<br>
			3. Probing for additional details:<br>
			- What skill do you wish to learn?<br>
			- Are you looking for yourself or other(Kids, friend) <br>
			- You would prefer classes in an around which area(Where do you stay/work)<br>
			- Do you have any specifications for batch timings? – Morning, evening, weekends, Male/female teacher. <br>
			- Are you looking for teacher’s who can come home? <br>
			4. Email and registration: Action to be taken<br>
			- We’ll share the Tutor recommendations by mail and message.<br>
			- May I have your email id, so I can share the detailed profile with you<br>
			- Would you like me to register you on Skillzot with this email id for future communication purposes? It will help us follow up in case you need help to join the class<br>
			- Reassure that NO unnecessary marketing emailers will be sent.<br>
			5. Inform and make aware:<br>
			- You can now book your classes online through skillzot. Would you like to know more?<br>
			- If yes – Inform of booking and cancellation policy<br>
			- If no – Thank you for your time, please check your mailbox for our Tutor recommendations.<br>
			<b>Action: Email sent with Minimum 3 recommendations.</b><br>
			
			Hi Student Name,<br>
				Thank you for enquiring for <Skill Name> classes through www.skillzot.com. Based on your preferences, here are a few options that are best suited for you.<br>
				1 Company Name 1 - Tutor Name 1<br>
				a Phone:  <Tutor1 Contact1><br>
				b Locality: <Tutor 1  Locality 1 ><br>
				c For Batch details and Enrolment :< Tutor 1 URL><br>

				2 Company Name 2 - Tutor Name 2<br>
				a Phone:  <Tutor2 Contact1><br>
				b Locality: <Tutor 2 Locality 1 ><br>
				c For Batch details and Enrolment : <Tutor 2 URL><br>

				3 Company Name 3 - Tutor Name 3<br>
				a Phone:  <Tutor3 Contact1><br>
				b Locality: <Tutor 3 Locality 1 ><br>
				c For Batch details and Enrolment : <Tutor 3 URL><br>

				We hope the Teacher’s profile link gives you all the information you need to make up your mind and sign up.  If not, feel free to call them and do mention that you got the details from www.skillzot.com. 
				Please don’t hesitate to call or email us back in case you need any further assistance.<br>
				Cheers,<br>
				<Administrator Name><br>
				Reachable @ +91 9820921404 (between 10am to 5:30pm, Mon-Fri)<br>
				(Skillzot is an online portal to help people connect with the best teachers & classes to learn any skill)";
					
			}elseif($checkduedate->task == 'ERP_STEP_2'){
				$tutor_message = "1. Greeting For Outbound <br>
				- Friendly greeting , smile in the voice<br>
				- Introduce yourself and why you are calling, ask if it’s a good time to speak <br>
				- Why am I calling – Following up on your inquiry to learn <Skill> contact <Teacher>?<br>
				2. Probing for additional details:<br>
				- We have shared contacts for teachers as per your requirement, hope you received our mail/message.<br>
				- Have you been able to contact the teacher yet?<br>
				- If yes – Have you registered with any teacher? Who?<br>
				- If no- Please feel free to contact the teacher when you’re able to, get back to us in case of any further assistance.<br>
				3. Action to be taken: <br>
				Open - Share more tutor recommendations if need be. <br>
				Closed - If joined already, inform about the “Class feedback” option – Helps improve your teachers profile rating and helps other students in deciding. ";
					
			}elseif($checkduedate->task == 'ERP_STEP_3'){
				$tutor_message = "1. Greeting For Outbound <br>
				- Friendly greeting , smile in the voice<br>
				- Introduce yourself and why you are calling, ask if it’s a good time to speak <br>
				- Why am I calling – Following up on your inquiry to learn <Skill> contact <Teacher>?<br>
				2. Probing for additional details:<br>
				- We have shared contacts for teachers as per your requirement, hope you received our mail/message.<br>
				- Have you been able to contact the teacher yet? <br>
				- If yes – Have you registered with any teacher? Who?<br>
				- If no because they didn’t get time yet - Please feel free to contact the teacher when you’re able to, get back to us in case of any further assistance.<br>
				- If no because the teacher did not get back - We will check with the teacher on your behalf and follow up. <br>
				3. Action to be taken: <br>
				- Open - Share more tutor recommendations if need be. <br>
				- Ask if we can follow up in 10 days or as specified by them.<br> 
				- Follow up with teacher in case they have not responded.<br> 
				- Closed - If joined already, inform about the “Class feedback” option – Helps improve your teachers profile rating and helps other students in deciding. <br>
				";
			
			}elseif($checkduedate->task == 'ERP_STEP_4'){
				$tutor_message = "1. Greeting For Outbound <br>
				- Friendly greeting , smile in the voice<br>
				- Introduce yourself and why you are calling, ask if it’s a good time to speak <br>
				- Why am I calling – Following up on your inquiry to learn <Skill> contact <Teacher>?<br>
				2. Probing for additional details:<br>
				- We have shared contacts for teachers as per your requirement, hope you received our mail/message.<br>
				- Have you been able to contact the teacher yet? <br>
				- If yes – Have you registered with any teacher? Who?<br>
				- If no because they didn’t get time yet - Please feel free to contact the teacher when you’re able to, get back to us in case of any further assistance.<br>
				- If no because the teacher did not get back - We will check with the teacher on your behalf and follow up. <br>
				3. Action to be taken: <br>
				- Open - Share more tutor recommendations if need be. <br>
				- Ask if we can follow up in 10 days or as specified by them.<br> 
				- Follow up with teacher in case they have not responded.<br> 
				- Closed - If joined already, inform about the “Class feedback” option – Helps improve your teachers profile rating and helps other students in deciding. <br>
				";
			}else{
				$tutor_message = "1. How easily were you able to find the information pertaining to the class you enrolled for? [Easily-Quite Easily - Not Easily - Hard]<br> 
				2. Did you know that you could also enroll online directly for the class through our Secure, Safe, Online Enrollment option ? [Yes- No]<br> 
				If yes then – <br> 
				3a. Why did you choose to enroll offline? (First ask open ended, if they can’t give an answer then prompt with the options)<br> 
				- Don't trust online enrollment<br> 
				- Not comfortable in transacting online<br> 
				- Don’t have debit/credit card or Net banking<br> 
				- Other:<br> 
				If no then - <br> 
				3b. How likely would you be to use it to register for your next class? <br> 
				- Not likely <br> 
				- Maybe <br> 
				-  Likely <br> 
				- Most likely";
			}
		if(isset($tutor_message) && $tutor_message!="")
		{
			$this->view->tutor_message = $tutor_message;
		}

	}
	public function addchecklistAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();

		$job_id = $this->_request->getParam("job_id");
		$this->view->id = $job_id;
		if(isset($job_id) && $job_id > 0)
		{
		$authUserNamespace->admin_page_title = "Add Checklist";
		}
		if($this->_request->isPost()){
			
			$Task = $this->_request->getParam("Task");
			$Due = $this->_request->getParam("Due");
			if(isset($Due) && $Due!="")
			{
				$explode_due=explode("/",$Due);
				$Due_final= $explode_due[2].'-'.$explode_due[0].'-'.$explode_due[1].' '.date('h:i:s', time());
			}else{
				$Due_final = date('Y-m-d h:i:s');
			}
			
			$hiddenid= $this->_request->getParam("hiddenvar");

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($Done == "")$response["data"]["Done"] = "valid";
				else $response["data"]["Done"] = "valid";
				if($Task != "")
				{
					$checklistobj1= $checklistobj->fetchRow("task='$Task' and job_id='$job_id'");
					//echo $checklistobj1->task;exit;
					if($checklistobj1->task == "$Task")
					{
						$response["data"]["Task"] = "duplicate";
					}else{
						$response["data"]["Task"] = "valid";
					}
				}else{
					$response["data"]["Task"] = "null";
				}
				if($Due == "")$response["data"]["Due"] = "valid";
				else $response["data"]["Due"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
		}else{
			
			if(isset($job_id) && $job_id > 0){
					
					$log_entry = $Task." Checklist Add Manually By Admin.";
					$data= array("job_id"=>$job_id,"date_created"=>date('Y-m-d h:i:s'),"task"=>$Task,"date_due"=>$Due_final,"date_done"=>"NULL","done_flag"=>"0");
					$checklistobj->insert($data);
					//$authUserNamespace->status_message = "Add Checklist has been inserted successfully";	

					$date_for_next_followup1 =  array("state"=>"OPEN","date_first_followup"=>date('Y-m-d h:i:s'),"date_last_followup"=>date('Y-m-d h:i:s'));
					$jobtableobj->update($date_for_next_followup1,"job_id='$job_id'");

					$data = array("job_id"=>$job_id,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>$log_entry);
					$logobj->insert($data);

					$jobtableobj1= $jobtableobj->fetchRow("job_id='$job_id'");	
					$checklistobj3= $checklistobj->fetchRow("job_id='$job_id' and date_due <='$jobtableobj1->date_next_followup'");
					if($checklistobj3=="")
					{
						$checklistobj4 = $checklistobj->fetchRow($checklistobj->select()
							->from(array('c'=>DATABASE_PREFIX."checklist"),array('min(c.date_due) as date_due'))
						    ->where("job_id='$job_id'"));
						$date_for_next_followup =  array("date_next_followup"=>$checklistobj4->date_due);
						$jobtableobj->update($date_for_next_followup,"job_id='$job_id'");
					}
				
			}
			echo "<script>parent.Mediabox.close();</script>";
			//echo "<script>window.parent.location='". BASEPATH ."/serpa/editchecklist/id/'".$check.";</script>";
			//echo $this->_redirect('serpa/editchecklist/id/'.$check);
		}
	}

	}
	public function addlogAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		if(isset($id) && $id > 0)
		{
		$authUserNamespace->admin_page_title = "Add Log";
		}

		if($this->_request->isPost()){
			$DataEntry = $this->_request->getParam("DataEntry");
			$hiddenid= $this->_request->getParam("hiddenvar");

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($DataEntry == "")$response["data"]["DataEntry"] = "null";
				else $response["data"]["DataEntry"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
		}else{
			
			if(isset($id) && $id > 0){
				$data = array("job_id"=>$id,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>$DataEntry);
				$logobj->insert($data);
				//$authUserNamespace->status_message = "Add log has been inserted successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/serpa/erp';</script>";
			
		}
	}

	}
	public function viewlogAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		if(isset($id) && $id > 0)
		{
		$authUserNamespace->admin_page_title = "View Log";
		}
				
		if(isset($id) && $id > 0)
		{
			$logobjResult = $logobj->fetchAll($logobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."log"),array('*'))
						    ->where("c.job_id='$id'")
							->order(array('c.date_entry desc')));
			$this->view->logobjResult = $logobjResult;
		}
	}
		public function reopenAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		if(isset($id) && $id > 0)
		{
		$authUserNamespace->admin_page_title = "Reopen Status";
		//$fetch_data = $jobtableobj->fetchRow("job_id='$id'");
		}

		if($this->_request->isPost()){
			$Reopen = $this->_request->getParam("Reopen");
			$hiddenid= $this->_request->getParam("hiddenvar");

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($Reopen == "")$response["data"]["Reopen"] = "null";
				else $response["data"]["Reopen"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
		}else{
			
			if(isset($id) && $id > 0){
				$data = array("date_first_followup"=>date('Y-m-d h:i:s'),"date_last_followup"=>date('Y-m-d h:i:s'),"state"=>$Reopen,"date_close"=>"NULL");
				$jobtableobj->update($data,"job_id='$id'");
				$data = array("job_id"=>$id,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>"Job Open by Admin");
				$logobj->insert($data);
				//$authUserNamespace->status_message = "Job State has been updated successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/serpa/erp';</script>";
		}
	}

	}
		public function closeAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->setLayout("lightboxnew");
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		if(isset($id) && $id > 0)
		{
		$authUserNamespace->admin_page_title = "Close Status";
		//$fetch_data = $jobtableobj->fetchRow("job_id='$id'");
		}

		if($this->_request->isPost()){
			$Close = $this->_request->getParam("Close");
			$Reason = $this->_request->getParam("Reason");
			$hiddenid= $this->_request->getParam("hiddenvar");

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				if($Close == "")$response["data"]["Close"] = "null";
				else $response["data"]["Close"] = "valid";
				if($Reason == "")$response["data"]["Reason"] = "null";
				else $response["data"]["Reason"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);
		}else{
			
			if(isset($id) && $id > 0){

				$data = array("date_close"=>date('Y-m-d h:i:s'),"date_last_followup"=>date('Y-m-d h:i:s'),"state"=>$Close,"reason"=>$Reason);
				$jobtableobj->update($data,"job_id='$id'");
				$data = array("job_id"=>$id,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>"Job Close by Admin");
				$logobj->insert($data);
				//$authUserNamespace->status_message = "Job State has been updated successfully";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			echo "<script>window.parent.location='". BASEPATH ."/serpa/erp';</script>";
		}
	}

	}
	public function checklistdeleteAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$id=$this->_request->getParam("id");
		$checklistobj1= $checklistobj->fetchRow("id='$id'");
		$check= $checklistobj1->job_id;
		$checklistobj4 = $checklistobj->fetchRow($checklistobj->select()
						->from(array('c'=>DATABASE_PREFIX."checklist"),array('min(c.date_due) as date_due'))
					    ->where("job_id='$check'"));

		$delete_task = $checklistobj1->task." deleted Manually by Admin.";
		if(isset($id) && $id!="")
		{
			
				$checklistobj->delete("id='$id'");
				//$authUserNamespace->status_message = "Detail has been deleted successfully";
	
				$date_for_next_followup =  array("date_next_followup"=>$checklistobj4->date_due,"state"=>"OPEN","date_first_followup"=>date('Y-m-d h:i:s'),"date_last_followup"=>date('Y-m-d h:i:s'));
				$jobtableobj->update($date_for_next_followup,"job_id='$check'");
				$data = array("job_id"=>$check,"date_entry"=>date('Y-m-d h:i:s'),"t_color"=>"#808080","entry"=>$delete_task);
				$logobj->insert($data);
				$this->_redirect('serpa/editchecklist/id/'.$check);

				
		}
	}
	public function exporterpAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();

		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();

		$exporterpObj_rows = $jobtableobj->fetchAll($jobtableobj->select()
						    ->setIntegrityCheck(false)
							->distinct()
							->from(array('c'=>DATABASE_PREFIX."job_table"),array('*'))
						    ->joinLeft(array('n'=>DATABASE_PREFIX."checklist"),'n.job_id=c.job_id',array('group_concat(DISTINCT if(n.done_flag="0", "NO", "YES"),",",n.task,",",date(n.date_due) SEPARATOR "\n") as checklist'))
						    ->joinLeft(array('o'=>DATABASE_PREFIX."log"),'c.job_id=o.job_id',array('*'))
						    ->where("1=1")
						    ->group(array("c.job_id"))
							->order(array('c.date_new desc')));
	
		/** PHPExcel **/
		require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
		
		//Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		//Set properties
		$objPHPExcel->getProperties()->setCreator("Skillzot")
									 ->setLastModifiedBy("Skillzot")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 		 ->setKeywords("office 2007 openxml php")
							 		 ->setCategory("Test result file");
		
		//Add some data
		$i = 1;
		
		$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
		$sheet_0->setCellValue("A$i", 'Checklist (Done,Task,Due)')
				->setCellValue("B$i", 'Log')
	        	->setCellValue("C$i", 'Due date')
	        	->setCellValue("D$i", 'Type')
	        	->setCellValue("E$i", 'State')
	        	->setCellValue("F$i", 'Reason')
	        	->setCellValue("G$i", 'Job Id')
	        	->setCellValue("H$i", 'Date New')
	        	->setCellValue("I$i", 'Date First')
	        	->setCellValue("J$i", 'Date Last')
	        	->setCellValue("K$i", 'Date Close')
	        	->setCellValue("L$i", 'Student Id')
	        	->setCellValue("M$i", 'Tutor Id');
        	
    		    		
	$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true);
	$i = 3;
	
	foreach ($exporterpObj_rows as $t){
		$sheet_0->setCellValue("A$i",  $t->checklist)
			->setCellValue("B$i",  $t->entry)
        	->setCellValue("C$i",  $t->date_next_followup)
        	->setCellValue("D$i",  $t->type)
        	->setCellValue("E$i",  $t->state)
        	->setCellValue("F$i",  $t->reason)
        	->setCellValue("G$i",  $t->job_id)
        	->setCellValue("H$i",  $t->date_new)
        	->setCellValue("I$i",  $t->date_first_followup)
        	->setCellValue("J$i",  $t->date_last_followup)
        	->setCellValue("K$i",  $t->date_close)
        	->setCellValue("L$i",  $t->student_id)
        	->setCellValue("M$i",  $t->tutor_id);
		
		$i++;
	}
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	
	// Rename sheet
	$objPHPExcel->getActiveSheet()->setTitle('Skillzot ERP Automation Report');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// Redirect output to a client's web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Skillzot_ERP_Automation_Report.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	}
	public function logoutAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id1) || $authUserNamespace->user_id1=="")$this->_redirect("/serpa");
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		Zend_Session::destroy(true,true);
		
		$this->_redirect('serpa/index?msg=logout');
	}


}