<?php

class LoginController extends Zend_Controller_Action{

	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("innerpage");
	}
		public function indexAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			

		/*not used*/
		$urlValuefor = $this->_request->getParam("url");
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
			$pageValue = $this->_request->getParam("page");
			if (isset($pageValue) && $pageValue!="")
			{
				$pageObj = new Skillzot_Model_DbTable_Pagesession();
				$setval2 = $pageValue. time().mt_rand();
				$mdset5 =  md5($setval2);
				$data = array("sessionnumber"=>$mdset5,"pagevalue"=>$pageValue);
				$pageObj->insert($data);
				$authUserNamespace->pagevalue = $mdset5;
			}
			unset($authUserNamespace->tutoridforreviewandcontact);
			unset($authUserNamespace->reviewredirectid);
			unset($authUserNamespace->contacttypebuttonRedirect);
			$TutorId = $this->_request->getParam("tutorid");
			if (isset($TutorId) && $TutorId!="")
			{
				$authUserNamespace->tutoridforreviewandcontact = $TutorId;
				$this->view->tutorid = $TutorId;
			}
			$reviewforRedirect=$this->_request->getParam("reviewid");
			if (isset($reviewforRedirect) && $reviewforRedirect!="")
			{
				$authUserNamespace->reviewredirectid = $reviewforRedirect;
				$this->view->reviewredirectid = $reviewforRedirect;
			}
			$batchforRedirect=$this->_request->getParam("batchid");
			//echo "batchforRedirect".$batchforRedirect;exit;
			if (isset($batchforRedirect) && $batchforRedirect!="")
			{
				$authUserNamespace->batchforRedirect = $batchforRedirect;
				$this->view->batchforRedirect = $batchforRedirect;
			}
			
			$contactbuttonRedirect=$this->_request->getParam("typeid");

			if (isset($contactbuttonRedirect) && $contactbuttonRedirect!="")
			{
				$authUserNamespace->contactbuttonredirectid = $contactbuttonRedirect;
				$this->view->contactbuttonredirectid = $contactbuttonRedirect;
			}
			
			$contacttypebuttonRedirect=$this->_request->getParam("contact");
			if (isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect!="")
			{
				$authUserNamespace->contacttypebuttonRedirect = $contacttypebuttonRedirect;
				$this->view->contacttypebuttonRedirect = $contacttypebuttonRedirect;
			}
			
			if($this->_request->isPost()){
				
					$txtemail=$this->_request->getParam("UserEmail");
					$txtpassword=$this->_request->getParam("UserPassword");
					
					if($this->_request->isXmlHttpRequest()){
				
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
					if($txtemail == "")$response["data"]["UserEmail"] = "null";
					else $response["data"]["UserEmail"] = "valid";
					
					if($txtemail!=""){
						if(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $txtemail)) $response['data']['UserEmail'] = "emailinvalid";
						else $response['data']['UserEmail'] = "valid";
					}
					
					if($txtpassword == "")$response ["data"]["UserPassword"]= "null";
					else $response ["data"]["UserPassword"]= "valid";
					
					if(!in_array('null',$response['data']) && !in_array('emailinvalid',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
					}
					else {
						$batch_shortlistObj= new Skillzot_Model_DbTable_shortlist();
						$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
						$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("tutor_email='$txtemail' && tutor_pwd='$txtpassword'"));
						$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
						$studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("std_email='$txtemail' && std_password ='$txtpassword'"));
						
						if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
						{
							$userId =  $tutorProfile_row->id;
							$pass = $tutorProfile_row->tutor_pwd;
							$authUserNamespace->maintutorid = $userId;
							$authUserNamespace->logintype = "1";
							
							if (isset($reviewforRedirect) && $reviewforRedirect!="" && $TutorId!=$userId ){
								$redLink = "/tutor/writereview/tutorid/".$TutorId;
								$this->_redirect($redLink);
							}else{
								echo "<script>window.parent.location.reload();</script>";
								//echo "<script>window.parent.location='". BASEPATH ."/tutor/profile/tutorid/".$TutorId."'</script>";
							}
							
							//echo "<script>window.parent.location='". BASEPATH ."/tutor/profile/tutorid/".$userId."'</script>";
							
						}
						elseif (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
						{
							$authUserNamespace->studentid = $studentProfile_row->id;
							$authUserNamespace->logintype = "2";
							
							if (isset($contactbuttonRedirect) && $contactbuttonRedirect!=""){
								$redLink = "/search/tutorcontact/tutorid/".$TutorId;
								$this->_redirect($redLink);
							}else{
								echo "<script>window.parent.location.reload();</script>";
							}
							//echo "<script>window.parent.location='". BASEPATH ."/index';</script>";
						}
						else{
							$authUserNamespace->checkerror="unsucess";
							$this->_redirect('/login/index');
						}
					}
			}
		
		}
	public function loginAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	
		//echo $redirectVal;exit;
			unset($authUserNamespace->tutoridforreviewandcontact);
			unset($authUserNamespace->reviewredirectid);
			unset($authUserNamespace->contacttypebuttonRedirect);
			
		$urlValuefor = $this->_request->getParam("url");
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
		$this->_helper->layout()->disableLayout();
	}
	public function moreoptionAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	}
	public function login1Action(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	}	
	public function checkloginAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			if($this->_request->isPost()){
				
				if($this->_request->isXmlHttpRequest()){
					$txtemail=$this->_request->getParam("UserEmail");
					$txtpassword=$this->_request->getParam("UserPassword");
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
					if($txtemail == "")$response["data"]["UserEmail"] = "null";
					else $response["data"]["UserEmail"] = "valid";
					
					if($txtemail!=""){
						if(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $txtemail)) $response['data']['UserEmail'] = "emailinvalid";
						else $response['data']['UserEmail'] = "valid";
					}
					
					if($txtpassword == "")$response ["data"]["UserPassword"]= "null";
					else $response ["data"]["UserPassword"]= "valid";
					
					if(!in_array('null',$response['data']) && !in_array('emailinvalid',$response['data']) && !in_array('invalid',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
				}
			}
	}	
	public function performloginAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			if($this->_request->isPost()){
				
				if($this->_request->isXmlHttpRequest()){
					$txtemail=$this->_request->getParam("UserEmail");
					$txtpassword=$this->_request->getParam("UserPassword");
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
						$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
						$tutorProfile_email = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("tutor_email='$txtemail'"));
						$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
						$studentProfile_email = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("std_email='$txtemail' and status = '1'"));
												 
						if(isset($tutorProfile_email) && sizeof($tutorProfile_email) > 0)
						{
							 if(($tutorProfile_email->tutor_v2_pwd)!="")
							 {
								 $tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("tutor_email='$txtemail' && tutor_v2_pwd=sha1('$txtpassword')"));
							 }
							 else
							 {
								 $encryt=$tutorProfile_email->tutor_pwd;
								 $data = array("tutor_v2_pwd"=>sha1($encryt),"tutor_pwd"=>'');
								 $tutorProfile->update($data,"tutor_email='$txtemail'");
								 $tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("tutor_email='$txtemail' && tutor_v2_pwd=sha1('$txtpassword')"));
							 }
						}
						if(isset($studentProfile_email) && sizeof($studentProfile_email) > 0)
						{
							 if(($studentProfile_email->std_v2_password)!="")
							 {
								 $studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("std_email='$txtemail' && std_v2_password =sha1('$txtpassword')")); 
							 }
							 else
							 {
								 $encryt=$studentProfile_email->std_password;
								 $data = array("std_v2_password"=>sha1($encryt),"std_password"=>'');
								 $studentSignupObj->update($data,"std_email='$txtemail'");
								 $studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("std_email='$txtemail' && std_v2_password =sha1('$txtpassword')"));
							 }
						}
						if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
						{
							$userId =  $tutorProfile_row->id;
							$authUserNamespace->maintutorid = $userId;
							$authUserNamespace->logintype = "1";
							$response['returnvalue'] = "success";
							$response['tuserid'] = $userId;
							////===sumit anand changes 9-7-13====////
							if(isset($tutorProfile_row->userid_new) && $tutorProfile_row->userid_new!="")
							{
								$response['useridfriendly'] = $tutorProfile_row->userid_new;
								setcookie('useridfriendly', $tutorProfile_row->userid_new, time()+60*60*24*365, '/');
							}
							else 
							{
								$response['useridfriendly'] = $tutorProfile_row->userid;
								setcookie('useridfriendly', $tutorProfile_row->userid, time()+60*60*24*365, '/');
							}
							//====end===//							
							setcookie('username', $userId, time()+60*60*24*365, '/');
							setcookie('logintype', "1", time()+60*60*24*365, '/');
						}
						elseif (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
						{
							
							$authUserNamespace->studentid = $studentProfile_row->id;
							$authUserNamespace->logintype = "2";
							$response['returnvalue'] = "success";
							$response['tuserid'] = "";
							setcookie('username', $studentProfile_row->id, time()+60*60*24*365, '/');
							setcookie('logintype', "2", time()+60*60*24*365, '/');
						}
						else{
							$response['returnvalue'] = "unsucess";
						}
					echo json_encode($response);
				}
			}
	}	
	
	
		
	public function logoutAction()
	{
//		$pageURL = 'http';
//		 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//		 $pageURL .= "://";
//		 if ($_SERVER["SERVER_PORT"] != "80") {
//		  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
//		 } else {
//		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
//		 }
//		 echo $pageURL;exit;

			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			Zend_Session::destroy(true,true);
			setcookie('username', "", time()-3600, '/');
			setcookie('logintype', "", time()-3600, '/');
			setcookie('useridfriendly', "", time()-3600, '/');
			
			$response=array();
//			if($this->sessionExists())
//    		{
//    			$response['returnvalue'] = "unsucess";
//    		}
//			//$this->_redirect('/index');
//			else{
					$response['returnvalue'] = "success";
//				}
			echo json_encode($response);
			
	}
	public function signupAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
//$this->_helper->layout()->setLayout("lightboxlayout");
//		echo $TutorId = $this->_request->getParam("tutorid");exit;
		if (isset($authUserNamespace->tutoridforreviewandcontact) && $authUserNamespace->tutoridforreviewandcontact!=""){
	 		$this->view->tutorid = $authUserNamespace->tutoridforreviewandcontact;
	 		//unset($authUserNamespace->tutoridforreviewandcontact);
	 		//$authUserNamespace->t_id_for_review = $tutorId;
		}
	 	$reviewId = $this->_request->getParam("reviewid");
	 	if (isset($authUserNamespace->reviewredirectid) && $authUserNamespace->reviewredirectid!="" )
	 	{
	 		$reviewId = $authUserNamespace->reviewredirectid;
	 		//unset($authUserNamespace->reviewredirectid);
	 	}
	 	if (isset($reviewId) && $reviewId!=""){
	 		$this->view->reviewid = $reviewId;
	 	}
	 	if (isset($authUserNamespace->contacttypebuttonRedirect) && $authUserNamespace->contacttypebuttonRedirect!="" )
	 	{
	 		$contacttypebuttonRedirect = $authUserNamespace->contacttypebuttonRedirect;
	 	}
		$urlValuefor = $this->_request->getParam("url");
		//echo "urlValuefor".$urlValuefor;exit;
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
		$batchforRedirect=$this->_request->getParam("batchid");
		//echo "batchforRedirect".$batchforRedirect;exit;
		if (isset($batchforRedirect) && $batchforRedirect!="")
		{
			$this->view->batchforRedirect = $batchforRedirect;
		}
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		
		$adressObj = new Skillzot_Model_DbTable_Address();
		$cityResult = $adressObj->fetchAll($adressObj->select()
								->from(array('a'=>DATABASE_PREFIX."master_address"))
								->where("a.address_type_id='1' && address_id!='0'"));
		$this->view->city = $cityResult;
		
		if($this->_request->isPost()){
			
			
				$reviewforRedirect=$this->_request->getParam("review_id");
				$LookingFor=$this->_request->getParam("LookingFor");
				$UserFName=$this->_request->getParam("UserFName");
				$UserLName=$this->_request->getParam("UserLName");
				$UserSkillname=$this->_request->getParam("UserSkill");
				$UserEmail=$this->_request->getParam("UserEmail1");
				$UserMobile=$this->_request->getParam("UserMobile");
				$Pass = $this->_request->getParam("UserPassword1");
				$confpass = $this->_request->getParam("UserretypePassword");
				$City = $this->_request->getParam("UserCity");
				$Locality = $this->_request->getParam("UserLocality");
				$Pincode = $this->_request->getParam("UserPincode");
				$TutorId = $this->_request->getParam("tutorId");
				$signupUrl = $this->_request->getParam("signupFromurl");
				$signuphomeurl = $this->_request->getParam("signuphomeurl");
				//echo "url compare".urldecode($signupUrl)." ".urldecode($signuphomeurl);exit;
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
						
						if($UserFName == "")$response["data"]["UserFName"] = "null";
						else $response["data"]["UserFName"] = "valid";
						
						if($UserLName == "")$response["data"]["UserLName"] = "null";
						else $response["data"]["UserLName"] = "valid";
						
						if($UserSkillname == "What you'd love to learn")$response["data"]["UserSkill"] = "null";
						else $response["data"]["UserSkill"] = "valid";
										
						if($UserEmail == "")$response["data"]["UserEmail1"] = "null";
						elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $UserEmail)) $response['data']['UserEmail1'] = "invalid";
						else $response["data"]["UserEmail1"] = "valid";
						
						$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
													 	 ->where("tutor_email='$UserEmail'"));
						if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
						{
							$response['data']['UserEmail1'] = "duplicate";
						}
						$studentsignup_row = $studentSignupObj->fetchRow($studentSignupObj->select()
													 	 	  ->where("std_email='$UserEmail'"));
						if (isset($studentsignup_row) && sizeof($studentsignup_row) > 0)
						{
							$response['data']['UserEmail1'] = "duplicate";
						}

						if($UserMobile == "" )$response["data"]["UserMobile"] = "valid";
						elseif((!is_numeric($UserMobile) || $UserMobile=="0"))$response["data"]["UserMobile"] = "null";
						elseif(($UserMobile != "" && strlen($UserMobile)!=10))$response["data"]["UserMobile"] = "null";
						else $response["data"]["UserMobile"] = "valid";

						if($Pass == "")$response["data"]["UserPassword1"] = "null";
						elseif(strlen($Pass)<5)$response["data"]["UserPassword1"] = "passlength";
						else $response["data"]["UserPassword1"] = "valid";
						
						if($confpass == "")$response["data"]["UserretypePassword"] = "conformpassnull";
						elseif($confpass != $Pass)$response["data"]["UserretypePassword"] = "passnotmatch";
						else $response["data"]["UserretypePassword"] = "valid";
						
	//					if($City == "")$response["data"]["UserCity"] = "null";
	//					else $response["data"]["UserCity"] = "valid";
	//					
	//					if($Locality == "")$response["data"]["UserLocality"] = "null";
	//					else $response["data"]["UserLocality"] = "valid";
					
	//					if($Pincode == "")$response["data"]["UserPincode"] = "null";
	//					elseif((!is_numeric($Pincode) || $Pincode=="0" || strlen($Pincode)!=6))$response["data"]["UserPincode"] = "invalid";
	//					else $response["data"]["UserPincode"] = "valid";
						
						if(!in_array('conformpassnull',$response['data']) && !in_array('passnotmatch',$response['data']) && !in_array('passlength',$response['data']) && !in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						echo json_encode($response);
					}
					else {
						$lastupdatedate = date("Y-m-d H:i:s");
						$LookingFor="";
						$hash = md5(uniqid(rand(), true));
						$encrypt = md5($UserEmail);	
						$data = array("tutor_for"=>$LookingFor,"first_name"=>$UserFName,"last_name"=>$UserLName,
						"skill_interested"=>$UserSkillname,"std_email"=>$UserEmail,"std_v2_password"=>sha1($Pass),"std_city"=>$City,
						"std_locality"=>$Locality,"signup_url"=>$signupUrl,"std_pincode"=>$Pincode,"std_phone"=>$UserMobile,
						"lastupdatedate"=>$lastupdatedate,"editdate"=>$lastupdatedate,"status"=>'0',"hash"=>$hash);
						$studentSignupObj->insert($data);
						$studentId = $studentSignupObj->getAdapter()->lastInsertId("std_email='$UserEmail'");
						$studentFullname = $UserFName." ".$UserLName;
						$profile_rows1 = $tutorProfile->fetchAll($tutorProfile->select()
																	->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.tutor_first_name','s.tutor_last_name'))
																	->where("s.id='$TutorId'"));
						if (isset($profile_rows1) && sizeof($profile_rows1)> 0)
						{
							$fullname = $profile_rows1[0]['tutor_first_name']." ".$profile_rows1[0]['tutor_last_name'];
						}else{
							$fullname = "None";
						}
						if (isset($reviewforRedirect) && $reviewforRedirect!="")
						{
							$subjectName = "Student sign up (Tutor Feedback) - ".$fullname;
						}else{
							$subjectName = "Student sign up (Contact) - ".$fullname;
						}
						if ($UserSkillname=="" && $UserSkillname==NULL)
						{
							$UserSkillname="None";
						}
			//------------------point no. 1------------------------------------
						$message1 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
									<tr align='center' width=100%>
										<td  width=30%>Looking Out Teacher for:</td>
										<td  width=30%>".$LookingFor."</td>
									</tr>
									<tr align='center'>
										<td>First Name:</td>
										<td width=30%>".$UserFName."</td>
									</tr>
									<tr align='center'>
										<td>Last Name:</td>
										<td  width=30%>".$UserLName."</td>
									</tr>
									<tr align='center'>
										<td>Skill:</td>
										<td  width=30%>".$UserSkillname."</td>
									</tr>
									<tr align='center'>
										<td>Email:</td>
										<td  width=30%>".$UserEmail."</td>
									</tr>
									</table>";
									$message = $message1;
									$to = "zot@skillzot.com";
									$from = $UserEmail;
									$mail = new Zend_Mail();
									$mail->setBodyHtml($message);
									$mail->setFrom($from,$studentFullname);
									$mail->addTo($to,"Admin");
									$mail->setSubject($subjectName);
									$mail->send();
					//--------------------------end of point no. 1 ----------------------------------------------				
					//---------------------------backup mail-----------------------------------------------------
									$message3 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
									<tr align='center' width=100%>
										<td  width=30%>Looking Out Teacher for:</td>
										<td  width=30%>".$LookingFor."</td>
									</tr>
									<tr align='center'>
										<td>First Name:</td>
										<td width=30%>".$UserFName."</td>
									</tr>
									<tr align='center'>
										<td>Last Name:</td>
										<td  width=30%>".$UserLName."</td>
									</tr>
									<tr align='center'>
										<td>Skill:</td>
										<td  width=30%>".$UserSkillname."</td>
									</tr>
									<tr align='center'>
										<td>Email:</td>
										<td  width=30%>".$UserEmail."</td>
									</tr>
									</table>";
									$tempsubVal = "Student sign up - ".$studentFullname;
									$message = $message3;
									$from = $UserEmail;
									//$to = "sz.contact@skillzot.com";
									//$to1 ="sandeep.burman@skillzot.com";
									$to ="zot@skillzot.com";
                                    $to1 ="sandeep.burman@skillzot.com";
                                    $mail = new Zend_Mail();
									$mail->setBodyHtml($message);
									$mail->setFrom($from,$UserEmail);
									$mail->addTo($to,"Admin");
									$mail->addTo($to1,"Admin");
                                  //  $mail->addTo($to2,"Admin");
									$mail->setSubject($tempsubVal);
									$mail->send();
				//---------------------------end of backup mail-------------------------------------------

									
				//----------------------------------Point no.2------------------------------------------
										
						$message2 = "<div style='text-align:justify;'>";
						$message2 .= "<div>Hi ".$UserFName."...</div><br/>";
						$message2 .= "<div>Thanx for registering with Skillzot, you've made our day<br/></div><br/>";
						$message2 .= "<div>Our mission is to help you add new 'happy powers' (our lingo for skills, think its way cooler), so that you can learn what you love and add to your happiness quotient, after all, you're happiest when you do the things you love, right?<br/></div><br/>";
	         			$message2 .= "<div>Just click below link to activate your account. <br/></div><br/>";
						$message2 .= "<div><a href='http://test.skillzot.com/login/confirmation?pass=".$encrypt."&key=".$hash."'>CLICK TO ACTIVATE YOUR ACCOUNT</a><br/></div><br/>";
						$message2 .= "<div>If you need any help feel free give us a call on +91 9820921404 (Mon-Fri, 10am to 5:30pm) or to reply to this email.<br/></div><br/>";
						$message2 .= "<div>Else, till then, feel free to stalk us on <a href='https://twitter.com/Skillzot' style='color:#1AA7EF;'>twitter</a> and/or <a href='http://www.facebook.com/pages/Skillzot/393253524025786' style='color:#1AA7EF;'>facebook</a>. You'll get cool, fun info on different types of 'happy powers'.<br/></div><br/><br/>";
	                    $message2 .= "<div>Cheers,<br/></div>";
						$message2 .= "<div>Sandeep<br/></div><br/>";
						$message2 .= "<div>Co-founder @ Skillzot</div>";
						$message2 .= "</div>";
						$message2 .= "<div><a href='http://www.skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
					
						$message = $message2;
						$to = $UserEmail;
						$from = "zot@skillzot.com";
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,"Sandeep @ Skillzot");
						$mail->addTo($to,$studentFullname);
						$mail->setSubject("skillzot - Email Verifcation");
						$mail->send();
			//---------------------------------------------------------------------------------------------		
						//$authUserNamespace->studentid = $studentId;
						//$authUserNamespace->logintype = "2";
//						if (isset($reviewforRedirect) && $reviewforRedirect!=""){
//							$redLink = "/tutor/writereview/tutorid/".$TutorId;
//							echo "in";exit;
//							$this->_redirect($redLink);
//						}else if(isset($contactbuttonRedirect) && $contactbuttonRedirect!=""){
//							$redLink = "/search/tutorcontact/tutorid/".$TutorId;
//							echo "out";exit;
//							$this->_redirect($redLink);
//						}else{
//							
//						}
						
						if (isset($reviewforRedirect) && $reviewforRedirect!=""){
							$redLink = "/tutor/writereview/tutorid/".$TutorId;
							$this->_redirect($redLink);
							
						}else if(isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect=='email' ){
							$redLink = "/search/tutorcontact/contact/email/tutorid/".$TutorId;
							$this->_redirect($redLink);
						}else if(isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect=='phone' ){
							$redLink = "/search/tutorcontact/contact/phone/tutorid/".$TutorId;
							$this->_redirect($redLink);
						}else if(isset($batchforRedirect) && $batchforRedirect!='' ){
							$finalRedirect = urldecode($urlValuefor);
							echo "<script>window.parent.location='".$finalRedirect."'</script>";
						}else{
							//$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
							//unset($authUserNamespace->curreturlforfblogin);
							$finalRedirect = urldecode($signupUrl);
							$homeRedirect = urldecode($signuphomeurl);
							$homeRedirect_add = $homeRedirect."/";
							if(($finalRedirect == $homeRedirect) || ($finalRedirect == $homeRedirect_add))
							{
								//echo "<script>window.parent.location='".$homeRedirect."/student/studentregistration/'</script>";
								//echo "<script>window.parent.location='".$finalRedirect."'</script>";
								$this->_redirect('/login/verification');
							}else{
								//echo "<script>window.parent.location='".$finalRedirect."'</script>";
								$this->_redirect('/login/verification');
							}
							
						}
					//	echo "<script>parent.Mediabox.close();</script>";
					}
		}
		
	}
public function signup3Action()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$urlValuefor = $this->_request->getParam("url");
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
		$this->_helper->layout()->disableLayout();
//$this->_helper->layout()->setLayout("lightboxlayout");
//		echo $TutorId = $this->_request->getParam("tutorid");exit;
	}
public function signup2Action()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$TutorId = $this->_request->getParam("tutorid");
		if (isset($TutorId) && $TutorId!="")
		{
			
			$this->view->tutorid = $TutorId;
		}
		$reviewforRedirect=$this->_request->getParam("reviewid");
		if (isset($reviewforRedirect) && $reviewforRedirect!="")
		{
			
			$this->view->reviewredirectid = $reviewforRedirect;
		}
		$urlValuefor = $this->_request->getParam("url");
		//echo "urlValuefor".$urlValuefor;exit;
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
		$batchforRedirect=$this->_request->getParam("batchid");
		//echo "batchforRedirect".$batchforRedirect;exit;
		if (isset($batchforRedirect) && $batchforRedirect!="")
		{
			$this->view->batchforRedirect = $batchforRedirect;
		}
}	
public function signup1Action()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$urlValuefor = $this->_request->getParam("url");
		if (isset($urlValuefor) && $urlValuefor!="")
		{
			$this->view->urltoredirectforfb = $urlValuefor;
		}
			$this->_helper->layout()->disableLayout();
}	
public function resetpassAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$getaction=$_GET['action'];
		$encrypted_code=$_GET['encrypt'];
		if($this->_request->isPost()) {	
		$new_password = $this->_request->getParam('NewPassword');
		$confirm_password = $this->_request->getParam('ConfirmPassword');
		if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
				
					if($new_password == "")$response["data"]["NewPassword"] = "null";
					elseif(strlen($new_password)<5)$response["data"]["NewPassword"] = "passlength";
					else $response["data"]["NewPassword"] = "valid";
					
					if($confirm_password == "")$response["data"]["ConfirmPassword"] = "conformpassnull";
		      		elseif($confirm_password != $new_password)$response["data"]["ConfirmPassword"] = "passnotmatch";
					else $response["data"]["ConfirmPassword"] = "valid";
					
					if(!in_array('null',$response['data']) && !in_array('wrongpass',$response['data']) &&  !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
					&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
					$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
			
					echo json_encode($response);
			
		}else{				
					if($getaction=="reset")
					{
							$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
							$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("md5(tutor_email)='$encrypted_code'"));
												 
							$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
							$studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("md5(std_email)='$encrypted_code'"));
												 
							if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
							{		
								//echo "tutor";
								$data = array("tutor_v2_pwd"=>sha1($new_password));
								$insertTutorpass = $tutorProfile->update($data,"id='$tutorProfile_row->id'");			
							}	
							elseif (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
							{
								//echo "student";
								$data1 = array("std_v2_password"=>sha1($new_password));
								$insertStudentpass = $studentSignupObj->update($data1,"id='$studentProfile_row->id'");
								
							}			
				}
				if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
						{
							$userId =  $tutorProfile_row->id;
							$authUserNamespace->maintutorid = $userId;
							$authUserNamespace->logintype = "1";
							$response['returnvalue'] = "success";
							$response['tuserid'] = $userId;
							if(isset($tutorProfile_row->userid_new) && $tutorProfile_row->userid_new!="")
							{
								$response['useridfriendly'] = $tutorProfile_row->userid_new;
								setcookie('useridfriendly', $tutorProfile_row->userid_new, time()+60*60*24*365, '/');
							}
							else 
							{
								$response['useridfriendly'] = $tutorProfile_row->userid;
								setcookie('useridfriendly', $tutorProfile_row->userid, time()+60*60*24*365, '/');
							}						
							setcookie('username', $userId, time()+60*60*24*365, '/');
							setcookie('logintype', "1", time()+60*60*24*365, '/');				
							$this->_redirect('/'.$tutorProfile_row->userid);							
						}
						elseif (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
						{
							
							$authUserNamespace->studentid = $studentProfile_row->id;
							$authUserNamespace->logintype = "2";
							$response['returnvalue'] = "success";
							$response['tuserid'] = "";
							setcookie('username', $studentProfile_row->id, time()+60*60*24*365, '/');
							setcookie('logintype', "2", time()+60*60*24*365, '/');
							$this->_redirect('/');
						}
						else{
							$response['returnvalue'] = "unsucess";
						}
					echo json_encode($response);
					
		}
	}
}
public function confirmationAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$encrypted_code=$_GET['pass'];
		$key=$_GET['key'];
		$this->view->encrypted_code = $encrypted_code;

		if(isset($encrypted_code) && $encrypted_code!=""){
									 
				$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
				$studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
									 ->where("md5(std_email)='$encrypted_code' and hash='$key'"));
			
				if (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
				{
					//echo "student";
					$data = array("status"=>'1');
					$insertStudentpass = $studentSignupObj->update($data,"id='$studentProfile_row->id'");
					
				}	
		}
				
		if($this->_request->isPost()) {	
			//$hiddenval = $this->_request->getParam('confirm');
		if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
					
					if(!in_array('null',$response['data']) && !in_array('wrongpass',$response['data']) &&  !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
					&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
					$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
			
					echo json_encode($response);
			
		}else{				
					
				if (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
						{
							
							$authUserNamespace->studentid = $studentProfile_row->id;
							$authUserNamespace->logintype = "2";
							$response['returnvalue'] = "success";
							$response['tuserid'] = "";
							setcookie('username', $studentProfile_row->id, time()+60*60*24*365, '/');
							setcookie('logintype', "2", time()+60*60*24*365, '/');
							$this->_redirect('/student/studentregistration');
						}
						else{
							$response['returnvalue'] = "unsucess";
						}
					echo json_encode($response);
					
		}
	}
}
public function forgetpassAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
		
			if($this->_request->isPost()){
				
					$txtemail=$this->_request->getParam("UserEmail");
					if($this->_request->isXmlHttpRequest()){
				
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
					if($txtemail == "")$response["data"]["UserEmail"] = "null";
					else $response["data"]["UserEmail"] = "valid";
					
					if($txtemail!=""){
						if(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $txtemail)) $response['data']['UserEmail'] = "invalid";
						else $response['data']['UserEmail'] = "valid";
					}
					
					if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
					}
					else {
						$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
						$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("tutor_email='$txtemail'"));
						if (isset($tutorProfile_row) && sizeof($tutorProfile_row)>0){
							$tutorFullname = ucfirst($tutorProfile_row->tutor_first_name)." ".$tutorProfile_row->tutor_last_name;
						}
						$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
						$studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("std_email='$txtemail'"));
						if (isset($studentProfile_row) && sizeof($studentProfile_row)>0){
							$studentFullname = ucfirst($studentProfile_row->first_name)." ".$studentProfile_row->last_name;
						}
						//print_r($studentProfile_row);
						/*if((isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0) || (isset($studentProfile_row) && sizeof($studentProfile_row) > 0 ))
						{
								$length = "6";
								$result = "";
								$chars = "bcdfghjklmnprstvwxzaeiou1234567890";
		    					for ($p = 0; $p < $length; $p++)
							    {
							        $result .= ($p%2) ? $chars[mt_rand(19, 23)] : $chars[mt_rand(0, 18)];
							    }
						}*/
						if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
						{						
							//http://localhost/login/resetpass?encrypt=35936504a37d53e03abdfbc7318d9ec7&action=reset
							$encrypt = md5($txtemail);
							$UserFName=$tutorProfile_row->tutor_first_name;
							$message1 = "<div style='text-align:justify;'>";
							$message1 .= "<div>Hi ".$UserFName."...</div><br/>";
							$message1 .= "<div>Yup, we agree, remembering passwords can be difficult. But don't worry, change is always good.<br/></div><br/>";
							$message1 .= "<div>Just click the link below and you'll be redirected to a secure site from which you can set a new password. <br/></div><br/>";
							$message1 .= "<div><a href='http://test.skillzot.com/login/resetpass?encrypt=".$encrypt."&action=reset'><button type='button' style='background-color: #00a7e6;border: 2px solid #00a7e6;border-radius: 5px;color: #ffffff;cursor: pointer;'>Reset Password</button></a><br/></div><br/>";
							$message1 .= "<div>If you did not request a new password, please let us know immediately at zot@skillzot.com<br/></div><br/><br/>";
							$message1 .= "<div>Cheers,<br/></div>";
							$message1 .= "<div>Team Skillzot<br/></div><br/>";
							$message1 .= "</div>";
		
							$message = $message1;
							$to = $txtemail;
							$from = 'skillzot.com';
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,'The Skillzot Team');
							$mail->addTo($to,$tutorFullname);
							$mail->setSubject('Reset your Skillzot password!');
							$mail->send();
							//$authUserNamespace->forgetpass="Mail has been sent to your EmailId";
							$this->view->message='Mail has been sent to your E-mailid';
							$this->_redirect('/login/forgetpassfinal');
							echo "<script>parent.Mediabox.close();</script>";
						}
						elseif (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
						{
							//http://localhost/login/resetpass?encrypt=ac73001b1d44f4925449ce09d9f5d5ca&action=reset
							$encrypt = md5($txtemail);
							$UserFName=$studentProfile_row->first_name;
							$message1 = "<div style='text-align:justify;'>";
							$message1 .= "<div>Hi ".$UserFName."...</div><br/>";
							$message1 .= "<div>Yup, we agree, remembering passwords can be difficult. But don't worry, change is always good.<br/></div><br/>";
							$message1 .= "<div>Just click the link below and you'll be redirected to a secure site from which you can set a new password. <br/></div><br/>";
							$message1 .= "<div><a href='http://test.skillzot.com/login/resetpass?encrypt=".$encrypt."&action=reset'><button type='button' style='background-color: #00a7e6;border: 2px solid #00a7e6;border-radius: 5px;color: #ffffff;cursor: pointer;'>Reset Password</button></a><br/></div><br/>";
							$message1 .= "<div>If you did not request a new password, please let us know immediately at zot@skillzot.com<br/></div><br/><br/>";
							$message1 .= "<div>Cheers,<br/></div>";
							$message1 .= "<div>Team Skillzot<br/></div><br/>";
							$message1 .= "</div>";
							$message = $message1;
							$to = $txtemail;
							$from = 'skillzot.com';
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,'The Skillzot Team');
							$mail->addTo($to,$studentFullname);
							$mail->setSubject('Reset your Skillzot password!');
							
							$mail->send();
							$this->view->message='Mail has been sent to your E-mailid';
							$this->_redirect('/login/forgetpassfinal');
							echo "<script>parent.Mediabox.close();</script>";
						}
						else{
							$this->view->message='E-mail id not registered';
							//$this->_redirect('/login/forgetpass');
						}
					}
			}
		
		}

public function forgetpassfinalAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
}
public function verificationAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
}
public function ajaxloginAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$category_id = $this->_request->getParam("category_id");
				$category_rows = $skillObj->fetchAll($skillObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
															  ->where("s.parent_skill_id!='0' && s.skill_id!='0' && parent_skill_id='$category_id' && skill_depth='2' && is_skill='1'"));
				$i=0;
				$tags = "";
				foreach($category_rows as $e){
					$tags[$i]['skill_name'] = "";
					$tags[$i]['skill_id'] = $e->skill_id;
					//$arry[]=$tags;
					$i++;
				}
				echo json_encode("success");
				}
	  		}
	}
	
}
