<?php

class IndexController extends Zend_Controller_Action{
	
	public function init(){
		//$this->_redirect('/commingsoon');
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='1') {
			//echo "in";
			$authUserNamespace->maintutorid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
			$authUserNamespace->useridfriedly = $_COOKIE['useridfriendly'];
		}else if(isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='2'){
			//echo "out";
			$authUserNamespace->studentid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}
    		
	}
	
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout('innerpageslider');
		$this->view->showfoot = "yes";
		//echo "inn";exit;
		
				$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
				$skillObj = new Skillzot_Model_DbTable_Skills();
				$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
				$tutorprofileObj = new Skillzot_Model_DbTable_Tutorprofile();
				$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
				$addresstable = new Skillzot_Model_DbTable_Address();
				$batchObj= new Skillzot_Model_DbTable_Batch();
				$skillmapobj = new Skillzot_Model_DbTable_SkillMap();
				$classtypeResult = $classtypeObj->fetchAll($classtypeObj->select()
												->from(array('s'=>DATABASE_PREFIX."master_tutor_class_types"))
												->order(array("class_type_id asc")));
				$this->view->classtype = $classtypeResult;
							
				
		if($this->_request->isPost()){
				//echo "sss";exit;
				$keywords1 = $this->_request->getParam("keywords");
				$keywords=trim($keywords1);
				//$keywords=mysql_real_escape_string($keywords2);
				//$authUserNamespace->search_keyword=$keywords;
				//$skilltype = $this->_request->getParam("skilltype");
				//echo "sss".$keywords;exit;
				if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
				
				if($keywords == "")$response["data"]["keywords"] = "null";
				else $response["data"]["keywords"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);
				}
				else {

				$search_for_skill = $skilldisplayObj->fetchRow($skilldisplayObj->select()
					->setIntegrityCheck(false)
					->from(array("r"=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
					->where("skill_name LIKE '%".addslashes($keywords)."%' || skill_uniq LIKE '%".addslashes($keywords)."%' "));

				$search_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
					->setIntegrityCheck(false)
					->distinct()
					->from(array("a"=>DATABASE_PREFIX.'tx_tutor_profile'))
					->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".addslashes($keywords)."%') || a.tutor_first_name like '%".addslashes($keywords)."%' || a.tutor_last_name like '".addslashes($keywords)."%') && a.is_active='0'"));
				//echo $search_for_tutor->id;exit;
				$search_for_tutor1 = $tutorprofileObj->fetchRow($tutorprofileObj->select()
					->setIntegrityCheck(false)
					->distinct()
					->from(array("a"=>DATABASE_PREFIX.'tx_tutor_profile'))
					->where("a.company_name like '%".addslashes($keywords)."%' && a.is_active='0'"));

				//echo "if";exit;
				if($search_for_skill->skill_uniq!="" && $search_for_tutor->id=="" && $search_for_tutor1->id=="")
				{
					$final_skill_search=$search_for_skill->skill_uniq;
					$skill_map_result=$search_for_skill->skill_uniq;
				}elseif($search_for_skill->skill_uniq=="" && $search_for_tutor->id!="" && $search_for_tutor1->id=="")
				{
					$query_for_tutor = $tutorCourseobj->fetchRow($tutorCourseobj->select()
							   ->setIntegrityCheck(false)
							   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('distinct(GROUP_CONCAT(a.tutor_skill_id)) as tutor_skill_id'))
							   ->where("a.tutor_id='".$search_for_tutor->id."'"));
					$final_skill_id=$query_for_tutor->tutor_skill_id;
					$string = str_replace(',', ' ', $final_skill_id);
					$string_for_skill_id=trim($string);
					//echo "'".$string_for_skill_id."'";exit;
					//print_r(trim($final_skill_id, ','));exit;
					if(isset($string_for_skill_id) && $string_for_skill_id!="")
					{
					$skill_id_for_tutor = explode(" ",$string_for_skill_id);
					//echo sizeof($skill_id_for_tutor);
					//print_r($skill_id_for_tutor);exit;
					//tutor_skill
					$tutor_skill=$skilldisplayObj->fetchRow($skilldisplayObj->select()
							   ->where("skill_id='".$skill_id_for_tutor[0]."' && skill_id!='0' && is_skill='1'"));
						if($tutor_skill->skill_uniq!="")
						{
							$skill_finalname=$tutor_skill->skill_uniq;
						}else{
							for($i=0;$i<=sizeof($skill_id_for_tutor);$i++) {
								$tutor_skill1=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  	 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0' && is_skill='1'"));
								if($tutor_skill1->skill_uniq!="")
								{
									$skill_finalname=$tutor_skill1->skill_uniq;
									break;
								}else{
									$tutor_skill2=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  		 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0'"));
									if($tutor_skill2->skill_uniq!="")
									{
										$tutor_skill3=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  			 ->where("skill_id='".$tutor_skill2->parent_skill_id."' && skill_id!='0' && is_skill='1'"));
										$skill_finalname=$tutor_skill3->skill_uniq;
										break;
									}
									
								}
							}
						}
						
					}
					//echo $skill_finalname;exit;		
					$final_search_keyword1=$keywords;
					$skill_map_result="";		
				}elseif($search_for_skill->skill_uniq=="" && $search_for_tutor->id=="" && $search_for_tutor1->id!="")
				{
						$query_for_tutor = $tutorCourseobj->fetchRow($tutorCourseobj->select()
								   ->setIntegrityCheck(false)
								   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('distinct(GROUP_CONCAT(a.tutor_skill_id)) as tutor_skill_id'))
								   ->where("a.tutor_id='".$search_for_tutor1->id."'"));
						$final_skill_id=$query_for_tutor->tutor_skill_id;
						$string = str_replace(',', ' ', $final_skill_id);
						$string_for_skill_id=trim($string);
						//echo "'".$string_for_skill_id."'";exit;
						//print_r(trim($final_skill_id, ','));exit;
						if(isset($string_for_skill_id) && $string_for_skill_id!="")
						{
						$skill_id_for_tutor = explode(" ",$string_for_skill_id);
						//print_r($skill_id_for_tutor);exit;
						//tutor_skill
						$tutor_skill=$skilldisplayObj->fetchRow($skilldisplayObj->select()
							   ->where("skill_id='".$skill_id_for_tutor[0]."' && skill_id!='0' && is_skill='1'"));
						if($tutor_skill->skill_uniq!="")
						{
							$skill_finalname=$tutor_skill->skill_uniq;
						}else{
							for($i=0;$i<=sizeof($skill_id_for_tutor);$i++) {
								$tutor_skill1=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  	 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0' && is_skill='1'"));
								if($tutor_skill1->skill_uniq!="")
								{
									$skill_finalname=$tutor_skill1->skill_uniq;
									break;
								}else{
									$tutor_skill2=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  		 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0'"));
									if($tutor_skill2->skill_uniq!="")
									{
										$tutor_skill3=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  			 ->where("skill_id='".$tutor_skill2->parent_skill_id."' && skill_id!='0' && is_skill='1'"));
										$skill_finalname=$tutor_skill3->skill_uniq;
										break;
									}
									
								}
							}
						}
						}
						
						//echo $tutor_skill->skill_uniq;exit;		
						$final_search_keyword1=$keywords;
						$skill_map_result="";
				}else
				{
							//echo "else";exit;
							$keyword_unique = explode(" ", $keywords);
							//echo sizeof($keyword_unique);exit;
							$list = array("in","is","it","a","the","of","or","I","you","he","me","us","they","she","to","but","that","this","those","then","has","and");
							
							#.............skill search ..................#

							$list_arr = array();
							foreach($keyword_unique as $key){
								if (in_array($key, $list)) {
								$list_arr[] = $key;
								//print_r($list_arr);
								}
							}
							$result = array_diff($keyword_unique, $list_arr);
							$final_search_keyword=implode(" ",$result);
							$word_for_search = explode(" ", $final_search_keyword);
							//print_r($word_for_search);exit;
							$tmp_skill_name=" ";
							$tmp_skill_name1=" ";
							$tmp_skill_name_acq=" ";
							$tmp_skill_name_mat=" ";
							for($i=0;$i<sizeof($word_for_search); $i++)
							{
								$skillrowResult1 = $skilldisplayObj->fetchRow($skilldisplayObj->select()
										   ->where("skill_name LIKE '%".$word_for_search[$i]."%' || skill_uniq LIKE '%".$word_for_search[$i]."%' && skill_id!='0' "));
									
								//echo $skillrowResult1->skill_uniq;exit;
								if(isset($skillrowResult1) && sizeof($skillrowResult1)>0)
								{
									//echo "if";exit;
									$tmp_skill_name=$tmp_skill_name." ".$word_for_search[$i];
									$tmp_skill_name_acq=$tmp_skill_name_acq." ".$skillrowResult1->skill_uniq;
									$tmp_skill_name_mat=$tmp_skill_name_mat." ".$skillrowResult1->skill_name;
									$tmp_skill_serach=$tmp_skill_name1.$skillrowResult1->skill_uniq;
									
								}else{
									//echo "else";exit;
									$skillrowResult = $skillmapobj->fetchRow($skillmapobj->select()
										   ->where("skill_name like '%".$word_for_search[$i]."%' || skill_entered like '%".$word_for_search[$i]."%'"));
									if(isset($skillrowResult) && sizeof($skillrowResult)>0)
									{
										//echo "if";exit;
									$tmp_skill_name=$tmp_skill_name." ".$word_for_search[$i];
									$tmp_skill_name_acq=$tmp_skill_name_acq." ".$skillrowResult->skill_name;
									$tmp_skill_name_mat=$tmp_skill_name_mat." ".$skillrowResult1->skill_uniq;
									$tmp_skill_serach=$tmp_skill_name1.$skillrowResult->skill_name;
									}
								}	
							}
							//skill
							//$skill_search=$tmp_skill_serach;
							//$final_skill_search=trim($skill_search);
							$final_skill_name=trim($tmp_skill_name);
							//$final_skill_name_acq=$tmp_skill_name_acq;
							//echo "ist".$final_skill_name;exit;
							if(isset($final_skill_name) && $final_skill_name!="")
							{
								$skillrowResult1 = $skilldisplayObj->fetchRow($skilldisplayObj->select()
										   ->where("skill_name LIKE '%".$final_skill_name."%' || skill_uniq LIKE '%".$final_skill_name."%' && skill_id!='0' "));
								if(isset($skillrowResult1) && sizeof($skillrowResult1)>0)
								{
								$skill_search=$skillrowResult1->skill_uniq;
								$final_skill_search=trim($skill_search);
								$final_skill_name=$final_skill_name;
								$final_skill_name_mat=$skillrowResult1->skill_name;
								$final_skill_name_acq=$skillrowResult1->skill_uniq;
								//echo $final_skill_search."and".$final_skill_name."and".$final_skill_name_acq;exit;
								}else{
								$skill_search=$tmp_skill_serach;
							    $final_skill_search=trim($skill_search);
								$final_skill_name=trim($tmp_skill_name);
								$final_skill_name_mat=trim($tmp_skill_name_mat);
								$final_skill_name_acq=$tmp_skill_name_acq;
								//echo $final_skill_search."and".$final_skill_name."and".$final_skill_name_acq;exit;
								}
							}
							if(($final_skill_name==$final_skill_name_acq) || ($final_skill_name==$final_skill_name_mat))
							{
								//echo "if";exit;
								$skill_map_result=$final_skill_name;

							}else{
								$skill_map_result=$final_skill_name;
							}
							//echo $skill_map_result;exit;
							//echo $authUserNamespace->tmp_skill_serach;exit;
							//echo "final='".trim($skill_search)."'";exit;
							#.............end of skill search ..................#

							#.............locality search ..................#
							//echo $final_skill_name_acq;exit;
							//print_r($word_for_search);exit;
							if(isset($final_skill_name) && $final_skill_name!="")
							{
								$locality_diff= explode(" ",trim($final_skill_name));
							}else{
								$locality_diff= " ";
							}
							
							//print_r($locality_diff);exit;
							$list_arr2 = array();
							foreach($word_for_search as $key){
								if (in_array($key, $locality_diff)) {
								$list_arr2[] = $key;
								//print_r($list_arr1);
								}
							}
							$result2 = array_diff($word_for_search, $list_arr2);
							//print_r($result2);exit;
							$final_search_keyword2=implode(" ",$result2);
							$word_for_search2 = explode(" ",$final_search_keyword2);
							if(isset($result2) && sizeof($result2)>0)
							{
							$tmp_locality=" ";
							$tmp_locality1=" ";
							for($i=0;$i<sizeof($word_for_search2); $i++)
							{
														
								$address_row=$addresstable->fetchRow($addresstable->select()
											->setIntegrityCheck(false)
											->from(array('a'=>DATABASE_PREFIX."master_address"),array('GROUP_CONCAT(a.address_id) as address_id'))
											->where("address_value like '%".$word_for_search2[$i]."%'"));
								if(isset($address_row->address_id) && sizeof($address_row->address_id)>0)
								{
									//echo "if";exit;
									$tmp_locality=$tmp_locality.$address_row->address_id;
									$tmp_locality1=$tmp_locality1." ".$word_for_search2[$i];
								}	
							}
							$final_address=$tmp_locality;
							$final_address_id=trim($final_address);
							$final_address_value=$tmp_locality1;
							$string_for_final_address_id=trim($final_address_id);
							$string_for_final_address_value=trim($final_address_value);
							//echo "'".$final_address_id."'";exit;
							}



							#.............end of locality search ..................#

							#.............tutor search ..................#

							if(isset($string_for_final_address_id) && $string_for_final_address_id !="")
							{
								$result_for_skill_and_locality = $final_skill_name." ".$string_for_final_address_value;
							}else{
								$result_for_skill_and_locality = $final_skill_name;
							}
							if(isset($result_for_skill_and_locality) && $result_for_skill_and_locality!="")
							{
								$skill_diff= explode(" ",trim($result_for_skill_and_locality));
							}else{
								$skill_diff= " ";
							}
							

							$list_arr1 = array();
							foreach($word_for_search as $key){
								if (in_array($key, $skill_diff)) {
								$list_arr1[] = $key;
								//print_r($list_arr1);
								}
							}

							$result1 = array_diff($word_for_search, $list_arr1);
							//print_r($result1);exit;
							$final_search_keyword1=implode(" ",$result1);
							//echo $final_search_keyword1;exit;
							$tmp_skill_id=" ";
							if(isset($final_search_keyword1) && $final_search_keyword1!="")
							{
								$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
										   ->setIntegrityCheck(false)
										   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array(''))
										   ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_skill_course"),'a.id=b.tutor_id',array('distinct(GROUP_CONCAT(b.tutor_skill_id)) as tutor_skill_id'))
										   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$final_search_keyword1."%') || a.company_name like '%".$final_search_keyword1."%') && a.is_active='0'"));
								if(isset($query_for_tutor->tutor_skill_id) && sizeof($query_for_tutor->tutor_skill_id)>0)
								{
									//echo "if";exit;
									$tmp_skill_id=$tmp_skill_id.",".$query_for_tutor->tutor_skill_id;
								}
								else{
									//echo "else";exit;
								$word_for_search1 = explode(" ",$final_search_keyword1);
								for($i=0;$i<sizeof($word_for_search1); $i++)
								{
								$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
										   ->setIntegrityCheck(false)
										   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array(''))
										   ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_skill_course"),'a.id=b.tutor_id',array('distinct(GROUP_CONCAT(b.tutor_skill_id)) as tutor_skill_id'))
										   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$word_for_search1[$i]."%') || a.company_name like '%".$word_for_search1[$i]."%') && a.is_active='0'"));
								
									if(isset($query_for_tutor->tutor_skill_id) && sizeof($query_for_tutor->tutor_skill_id)>0)
									{
										$tmp_skill_id=$tmp_skill_id.",".$query_for_tutor->tutor_skill_id;
									}	
								}
							}
							}
							if(isset($tmp_skill_id) && $tmp_skill_id!="")
							{
								$final_skill_id=$tmp_skill_id;
								$string = str_replace(',', ' ', $final_skill_id);
								$string_for_skill_id=trim($string);
								//echo "'".$string_for_skill_id."'";exit;
								//print_r(trim($final_skill_id, ','));exit;
								if(isset($string_for_skill_id) && $string_for_skill_id!="")
								{
									$skill_id_for_tutor = explode(" ",$string_for_skill_id);
									//print_r($skill_id_for_tutor);exit;
									//tutor_skill
									$tutor_skill=$skilldisplayObj->fetchRow($skilldisplayObj->select()
							  			 ->where("skill_id='".$skill_id_for_tutor[0]."' && skill_id!='0' && is_skill='1'"));
									if($tutor_skill->skill_uniq!="")
									{
										$skill_finalname=$tutor_skill->skill_uniq;
									}else{
										for($i=0;$i<=sizeof($skill_id_for_tutor);$i++) {
											$tutor_skill1=$skilldisplayObj->fetchRow($skilldisplayObj->select()
											  	 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0' && is_skill='1'"));
											if($tutor_skill1->skill_uniq!="")
											{
												$skill_finalname=$tutor_skill1->skill_uniq;
												break;
											}else{
												$tutor_skill2=$skilldisplayObj->fetchRow($skilldisplayObj->select()
											  		 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0'"));
												if($tutor_skill2->skill_uniq!="")
												{
													$tutor_skill3=$skilldisplayObj->fetchRow($skilldisplayObj->select()
											  			 ->where("skill_id='".$tutor_skill2->parent_skill_id."' && skill_id!='0' && is_skill='1'"));
													$skill_finalname=$tutor_skill3->skill_uniq;
													break;
												}
												
											}
										}
									}
									
								}
							}
					}
					//echo $skill_id_for_tutor[0];exit;
					//echo $skill_id_for_tutor[0];exit;
					//echo $query_for_tutor->tutor_skill_id;exit;
				//	echo "tutor=".$final_search_keyword1." skill=".$final_skill_search." locality=".$string_for_final_address_value;exit;

					  if ($final_search_keyword1!="" && $final_skill_search!="" && $string_for_final_address_value!="")
					   {
					   		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?search=".$final_search_keyword1."&locality_value=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);	
					   }
				      elseif($final_search_keyword1!="" && $final_skill_search=="" && $string_for_final_address_value=="")
					    {
					    	if($tutor_skill->skill_uniq=="")
					    	{
					    		$tutor_skill->skill_uniq="music";
					    	}
					   		$tuturIdlink = "/search/".$skill_finalname."-classes-in-mumbai?search=".$final_search_keyword1."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							echo "link".$tuturIdlink;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1!="" && $final_skill_search!="" && $string_for_final_address_value=="")
					    {
					   		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?search=".$final_search_keyword1."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1!="" && $final_skill_search=="" && $string_for_final_address_value!="")
					    {
					   		$tuturIdlink = "/search/".$skill_finalname."-classes-in-mumbai?search=".$final_search_keyword1."&locality_value=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1=="" && $final_skill_search!="" && $string_for_final_address_value!="")
					    {
					   		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?locality_value=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1=="" && $final_skill_search=="" && $string_for_final_address_value!="")
					    {
					   		$tuturIdlink = "/search/music-classes-in-mumbai?locality_value=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  else{
					  	//echo "else";exit;
					  		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?skill=".$final_skill_search."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					  }
					
			}
		}
	

}
	
	public function aboutusAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout('staticlayout');
		$this->_helper->layout()->setLayout('innerpage');
	}
	public function teamhiringAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout('staticlayout');
		$this->_helper->layout()->setLayout('innerpage');
	}
	public function faqAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout('staticlayout');
		$this->_helper->layout()->setLayout('innerpage');
	}
	public function privacypolicyAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout('staticlayout');
		$this->_helper->layout()->setLayout('innerpage');
	}

	public function contactusAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout('staticlayout');
		$this->_helper->layout()->setLayout('innerpage');
	}
	public function facebookloginAction(){
		//-----------------fb code---------------------------------------
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			//session_start();
			header('P3P: CP="CAO PSA OUR"');
			
			$urlValuefor = $this->_request->getParam("url");
			//echo $urlValue;exit;
			if(!isset($authUserNamespace->curreturlforfblogin) && $authUserNamespace->curreturlforfblogin=="")
			{
				$authUserNamespace->curreturlforfblogin = $urlValuefor;
			}
			//$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
			//echo $actual_link;exit;
		   $app_id = "444557608950929";
		   $app_secret = "0c99b86e3656095ca4c85bec5d2cdd90";
		   $my_url = "http://www.skillzot.com/index/facebooklogin/";
		   error_reporting('code');
		   $code = $_REQUEST["code"];
		   if(empty($code)) {
		   $state = md5(uniqid(rand(), TRUE));
			 
			 $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=". $app_id . "&redirect_uri=" . urlencode($my_url). "&state=". $state."&scope=email";
			 //$authUserNamespace->tutoridforreviewandcontact = $TutorId;
			 echo("<script> top.location.href ='" . $dialog_url . "'</script>");
			 exit;
		   }
			if($code!="" && $code!=null)
			{
				$token_url = "https://graph.facebook.com/oauth/access_token?". "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) . "&client_secret=" . $app_secret . "&code=" . $code;
				 $response = @file_get_contents($token_url);
				parse_str($response);
				   
				 $graph_url = "https://graph.facebook.com/me?access_token=". $access_token;
					$fb_data = json_decode(file_get_contents($graph_url),true);
					$firstnamefb = $fb_data[first_name];
					$lastnamefb = $fb_data[last_name];
					$emailfb = $fb_data[email];
					$genderfb = $fb_data[gender];
					 $tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					/*	$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("tutor_email='$emailfb'")); */
						$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
						$studentProfile_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 ->where("std_email='$emailfb'"));
														
						
						
						if (isset($authUserNamespace->tutoridforreviewandcontact) && $authUserNamespace->tutoridforreviewandcontact!="")
						{
							$TutorId = $authUserNamespace->tutoridforreviewandcontact;
							//unset($authUserNamespace->tutoridforreviewandcontact);
						}
						if (isset($authUserNamespace->reviewredirectid) && $authUserNamespace->reviewredirectid!=""){
						//echo "inn";
							$reviewforRedirect = $authUserNamespace->reviewredirectid;
							//unset($authUserNamespace->reviewredirectid);
						}		
						 
						if (isset($authUserNamespace->contacttypebuttonRedirect) && $authUserNamespace->contacttypebuttonRedirect!=""){
						//echo "out";
							$contacttypebuttonRedirect = $authUserNamespace->contacttypebuttonRedirect;
							//unset($authUserNamespace->contacttypebuttonRedirect);
						}
						
						if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
						{
						//echo "inn";
							$userId =  $tutorProfile_row->id;
							$authUserNamespace->maintutorid = $userId;

							$authUserNamespace->logintype = "1";
							if (isset($reviewforRedirect) && $reviewforRedirect!="" && $TutorId!=$userId ){
								$redLink = "/tutor/writereview/tutorid/".$TutorId;
								$this->_redirect($redLink);
							}else if(isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect=='email' ){
								$redLink = "/search/tutorcontact/contact/email/tutorid/".$TutorId;
								$this->_redirect($redLink);
							}else if(isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect=='phone' ){
								$redLink = "/search/tutorcontact/contact/phone/tutorid/".$TutorId;
								$this->_redirect($redLink);
								
							}else{
								$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
								//$stringforfb = substr($tempsessionvalue, 8);
								//unset($authUserNamespace->curreturlforfblogin);
								echo "<script>window.parent.location='". BASEPATH .$tempsessionvalue."'</script>";
							}
						}
						elseif (isset($studentProfile_row) && sizeof($studentProfile_row) > 0)
						{
						//echo "s---".$reviewforRedirect;exit;
							$studentId = $studentProfile_row->id;
							$loginFrom = "Facebook";	
							$data = array("login_from"=>$loginFrom);
							$studentSignupObj->update($data,"id=$studentId");
							//$studentSignupObj->insert($data);
						//echo "ddd".$authUserNamespace->curreturlforfblogin;exit;
							$authUserNamespace->studentid = $studentProfile_row->id;
							$authUserNamespace->logintype = "2";
							if (isset($reviewforRedirect) && $reviewforRedirect!=""){
								$redLink = "/tutor/writereview/tutorid/".$TutorId;
								$authUserNamespace->afterfbloginopenlightbox = $redLink;
								//$this->_redirect($redLink);
								
								$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
								//$stringforfb = substr($tempsessionvalue, 8);
								//echo $stringforfb;exit;
								unset($authUserNamespace->curreturlforfblogin);
								echo "<script>window.parent.location='". BASEPATH .$tempsessionvalue."'</script>";
								
							}else if(isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect=='email' ){
								$redLink = "/search/tutorcontact/contact/email/tutorid/".$TutorId;
								$authUserNamespace->afterfbloginopenlightbox = $redLink;
								//$this->_redirect($redLink);
								
								$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
								//$stringforfb = substr($tempsessionvalue, 8);
								//echo $stringforfb;exit;
								unset($authUserNamespace->curreturlforfblogin);
								echo "<script>window.parent.location='". BASEPATH .$tempsessionvalue."'</script>";
							}else if(isset($contacttypebuttonRedirect) && $contacttypebuttonRedirect=='phone' ){
								$redLink = "/search/tutorcontact/contact/phone/tutorid/".$TutorId;
								$authUserNamespace->afterfbloginopenlightbox = $redLink;
								//$this->_redirect($redLink);
								
								
								$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
								//$stringforfb = substr($tempsessionvalue, 8);
								//echo $stringforfb;exit;
								unset($authUserNamespace->curreturlforfblogin);
								echo "<script>window.parent.location='". BASEPATH .$tempsessionvalue."'</script>";
								
							}else{
								$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
								//$stringforfb = substr($tempsessionvalue, 8);
								//echo $stringforfb;exit;
								unset($authUserNamespace->curreturlforfblogin);
								echo "<script>window.parent.location='". BASEPATH .$tempsessionvalue."'</script>";
							}
						}
						else if($emailfb!="" && $firstnamefb!=""){
							
							$length = "6";
							$result = "";
							$chars = "bcdfghjklmnprstvwxzaeiou1234567890";
	    					for ($p = 0; $p < $length; $p++)
						    {
						        $result .= ($p%2) ? $chars[mt_rand(19, 23)] : $chars[mt_rand(0, 18)];
						    }
							$LookingFor = "Myself";
							$loginFrom = "Facebook";
							$lastupdatedate = date("Y-m-d H:i:s");
							$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
							$hash = md5(uniqid(rand(), true));
							$data = array("tutor_for"=>$LookingFor,"first_name"=>$firstnamefb,"last_name"=>$lastnamefb,"std_email"=>$emailfb,"std_password"=>$result,"lastupdatedate"=>$lastupdatedate,"login_from"=>$loginFrom,"signup_url"=>urlencode($tempsessionvalue),"editdate"=>$lastupdatedate,"status"=>'1',"hash"=>$hash);
							
							//print_r($data);exit;
							$studentSignupObj->insert($data);
							
							$message2 = "<div style='text-align:justify;'>";
							$message2 .= "<div>Hi ".$firstnamefb."...</div><br/>";
							$message2 .= "<div>Thanx for registering with Skillzot, you've made our day<br/></div><br/>";
							$message2 .= "<div>Our mission is to help you add new 'happy powers' (our lingo for skills, think its way cooler), so that you can learn what you love and add to your happiness quotient, after all, you're happiest when you do the things you love, right?<br/></div><br/>";
							$message2 .= "<div>If you need any help feel free give us a call on +91 9820921404 (Mon-Fri, 10am to 5:30pm) or to reply to this email.<br/></div><br/>";
							$message2 .= "<div>Else, till then, feel free to stalk us on <a href='https://twitter.com/Skillzot' style='color:#1AA7EF;'>twitter</a> and/or <a href='http://www.facebook.com/pages/Skillzot/393253524025786' style='color:#1AA7EF;'>facebook</a>. You'll get cool, fun info on different types of 'happy powers'.<br/></div><br/><br/>";
						    $message2 .= "<div>Cheers,<br/></div>";
							$message2 .= "<div>Sandeep<br/></div><br/>";
							$message2 .= "<div>Co-founder @ Skillzot</div>";
							$message2 .= "</div>";
							$message2 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
						
							$message = $message2;
							$to = $emailfb;
							$from = "zot@skillzot.com";
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,"Sandeep @ Skillzot");
							$mail->addTo($to,$firstnamefb);
							$mail->setSubject("Hola! You've made our day!");
							$mail->send();
							
							
							$studentFullname = $firstnamefb." ".$lastnamefb;
							$message3 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
									
									<tr align='center'>
										<td>First Name:</td>
										<td width=30%>".$firstnamefb."</td>
									</tr>
									<tr align='center'>
										<td>Last Name:</td>
										<td  width=30%>".$lastnamefb."</td>
									</tr>
									<tr align='center'>
										<td>Email:</td>
										<td  width=30%>".$emailfb."</td>
									</tr>
									<tr align='center'>
										<td>Password:</td>
										<td  width=30%>".$result."</td>
									</tr>
									<tr align='center'>
										<td>Login From:</td>
										<td  width=30%>".$loginFrom."</td>
									</tr>
									</table>";
									$tempsubVal = "Student sign up - ".$studentFullname;
									$message = $message3;
									$from = $emailfb;
									$to = "zot@skillzot.com";
									$to2 ="sandeep.burman@skillzot.com";
									$to1 ="zot@skillzot.com";
									$mail = new Zend_Mail();
									$mail->setBodyHtml($message);
									$mail->setFrom($from,$emailfb);
									$mail->addTo($to,"Admin");
									$mail->addTo($to1,"Admin");
									$mail->addTo($to2,"Admin");
									$mail->setSubject($tempsubVal);
									$mail->send();
							
							$studentId = $studentSignupObj->getAdapter()->lastInsertId("std_email='$emailfb'");
							$authUserNamespace->studentid = $studentId;
							$authUserNamespace->logintype = "2";
							
							if (isset($reviewforRedirect) && $reviewforRedirect!="" && $TutorId!=$userId ){
								$redLink = "/tutor/writereview/tutorid/".$TutorId;
								$this->_redirect($redLink);
							}else if(isset($contactbuttonRedirect) && $contactbuttonRedirect!="" ){
								$redLink = "/search/tutorcontact/tutorid/".$TutorId;
								$this->_redirect($redLink);
							}else{
								$tempsessionvalue = $authUserNamespace->curreturlforfblogin;
								//$stringforfb = substr($tempsessionvalue, 9);
								//echo $tempsessionvalue;exit;
								unset($authUserNamespace->curreturlforfblogin);
								echo "<script>window.parent.location='". BASEPATH .$tempsessionvalue."'</script>";
							}
							
							//$this->_redirect('/index');
						}else{
						 if($authUserNamespace->maintutorid == $TutorId)
						 {
								echo "<script>window.parent.location='". BASEPATH ."/tutor/profile/tutorid/".$TutorId."';</script>";
							}
						}
						unset($authUserNamespace->tutoridforreviewandcontact);
						unset($authUserNamespace->reviewredirectid);
						unset($authUserNamespace->contacttypebuttonRedirect);
			}else{
			print "response error";
			}
			
		//---------------------------------------------------------------
	}
	
	public function homeAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->card_id) && $authUserNamespace->card_id=="")$this->_redirect('/index');
		$this->_helper->layout()->setLayout('layout3');
		$this->view->home_menu = "active";
		
		$card_id = $this->view->card_id = $authUserNamespace->card_id;
		$overviewObj = new Bankoffercms_Model_DbTable_OverviewCms();
		$overview_row = $overviewObj->fetchRow($overviewObj->select()
														   ->from(array('o'=>DATABASE_PREFIX.'overview_cms'),array('card_display_name'))
														   ->where("id='$card_id'"));
														   
		if(isset($overview_row) && sizeof($overview_row)>0){
			$this->view->card_display_name = $overview_row->card_display_name;
		}
		
		$merchantObject = new Bankoffercms_Model_DbTable_Merchants();
		$merchant_rows = $merchantObject->fetchAll($merchantObject->select()
																  ->from(array('m'=>DATABASE_PREFIX.'merchants'),array('id','homepagebannertext','homepagebanner_order'))
																  ->where("active='Y' && preferred_merchants='yes'" )
																  ->order("homepagebanner_order asc"));
		$this->view->merchant_rows = $merchant_rows;

		// code for search bonus partneres
		$merchantlocationObject = new Bankoffercms_Model_DbTable_MerchantLocation();
		$merch_loc_rows = $merchantlocationObject->fetchAll($merchantlocationObject->select()
													  							   ->from(array('c'=>DATABASE_PREFIX.'merchant_locations'),array('distinct(city_id)'))
													  							   ->where("location_active='Y'"));
		
		if(isset($merch_loc_rows) && sizeof($merch_loc_rows)!=0){
			
			$city_ids = "";
			foreach ($merch_loc_rows as $location){
				if($city_ids=="") $city_ids .= $location->city_id;
				else $city_ids .= ",".$location->city_id;
			}
			$cityObject = new Bankoffercms_Model_DbTable_Cities();;
			$city_rows = $cityObject->fetchAll($cityObject->select()
														  ->from(array("c"=>DATABASE_PREFIX.'cities'),array('id','city_name'))
														  ->where("id in ($city_ids)")
														  ->order("city_name"));
			
			$this->view->cities = $city_rows;
		}
	}
	
	public function getcategoryAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		$categoryObj = new Bankoffercms_Model_DbTable_Categories();
		
		if($this->_request->isPost()){
			
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				$cat_name = $this->_request->getParam("cat_name");
				$cat_rows = $categoryObj->fetchAll($categoryObj->select()
															   ->where("(category_name like '%$cat_name%')")
															   ->limit(10));
				$i=0;
				$tags = "";
				
				foreach($cat_rows as $e){
					$tags[$i] = ucfirst($e->category_name);
					$i++;
				}
				$tags_name = json_encode($tags);
				echo $tags_name;
			}
		}
	}
	
	public function catinfoAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		//$this->_helper->layout->setLayout('admin');
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$cname = $this->_request->getParam('cname');
		
		// displaying schools
		$catObj = new Bankoffercms_Model_DbTable_Categories();
		$cat_detail = $catObj->fetchRow("category_name='$cname'");
		
		$response = array();
		
		if(sizeof($cat_detail) > 0){
			$response["data"]["category_name"] = $cat_detail->category_name;
			$response["data"]["cid"] = $cat_detail->id;
			echo json_encode($response);
		}
	}
	
	public function getcityAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		$cityObject = new Bankoffercms_Model_DbTable_Cities();
		
		if($this->_request->isPost()){
			
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				$city_name = $this->_request->getParam("city_name");
				$city_rows = $cityObject->fetchAll($cityObject->select()
														  	  ->where("(city_name like '%$city_name%')")
														   	  ->limit(10));
				$i=0;
				$tags = "";
				
				foreach($city_rows as $e){
					$tags[$i] = ucfirst($e->city_name);
					$i++;
				}
				
				$tags_name = json_encode($tags);
				echo $tags_name;
			}
		}
	}
}
