<?php
class StudentController extends Zend_Controller_Action{
	
	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("studentprofilepage");
		if (isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='1') {
			//echo "in";
			$authUserNamespace->maintutorid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}else if(isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='2'){
			//echo "out";
			$authUserNamespace->studentid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}
    		
	}
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');

	}
	public function studentregistrationAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("searchinnerpage");
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$adressObj = new Skillzot_Model_DbTable_Address();
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$skillmapobj = new Skillzot_Model_DbTable_SkillMap();
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
			$studentData = $studentSignupObj->fetchRow("id ='$authUserNamespace->studentid'");
			$this->view->studentdata = $studentData;
		}
		$query_for_skill = $skilldisplayObj->fetchRow($skilldisplayObj->select()
					->setIntegrityCheck(false)
       				->from(array("r"=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
				 	->where("skill_name LIKE '%".addslashes($studentData->skill_interested)."%' || skill_uniq LIKE '%".addslashes($studentData->skill_interested)."%' && skill_id!='0' && is_skill='1'"));
		
		$skill_finalname=$query_for_skill->skill_uniq;

		if($query_for_skill == "" || $query_for_skill == "NULL")
		{
		$skillrowResult = $skillmapobj->fetchRow($skillmapobj->select()
								   ->where("skill_name like '%".addslashes($studentData->skill_interested)."%' || skill_entered like '%".addslashes($studentData->skill_interested)."%'"));
		$skill_finalname=$skillrowResult->skill_name;
		}
		if($skill_finalname=="")
			{
				$skill_finalname="music";
			}
		$this->view->skill_finalname = $skill_finalname;
		$skillndata = $skilldisplayObj->fetchRow($skilldisplayObj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
								  ->where("s.skill_uniq='$skill_finalname' && s.skill_id!=0 && s.is_skill='1'"));//21-8	
		
		$this->view->skillcategoryUniq = $skillndata->skill_uniq;	
		$parentId = $skillndata->parent_skill_id;		
		$parentdata = $skilldisplayObj->fetchRow("skill_id='$parentId' && skill_id!=0 && is_skill='1' && is_enabled='1'");//16-8
		$this->view->mainsubmaincategoryUniq = $parentdata->skill_uniq;	
		$seccondParentID = $parentdata->parent_skill_id;
		$finaldata = $skilldisplayObj->fetchRow("skill_id='$seccondParentID' && skill_id!=0 && is_skill='1'");//16-8
		$this->view->maincategoryUniq = $finaldata->skill_uniq;
		$categorySubRows = $skilldisplayObj->fetchRow($skilldisplayObj->select()
						  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id','s.skill_uniq'))
						  ->where("is_enabled = 1 && parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$skillndata->skill_id' && skill_depth='2' && is_skill='1'")
						  ->order(array("skill_name")));
						  			 
		if (isset($categorySubRows) && $categorySubRows!=""){
			$this->view->skill_id_for_tutor = $categorySubRows->skill_id;
		}else{
			$this->view->skill_id_for_tutor = $skillndata->skill_id;
		}
		
	}
	public function personalinfoAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$selectId = '1';
		$this->view->selectid = $selectId;
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$adressObj = new Skillzot_Model_DbTable_Address();
		
		$cityResult = $adressObj->fetchAll($adressObj->select()
								->from(array('a'=>DATABASE_PREFIX."master_address"))
								->where("a.address_type_id='1' && a.address_id!='0'"));
		$this->view->city = $cityResult;
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
			$studentData = $studentSignupObj->fetchRow("id ='$authUserNamespace->studentid'");
			$this->view->studentdata = $studentData;
		}
	if($this->_request->isPost()){
			$LookingFor=$this->_request->getParam("LookingFor");
			$UserFName=$this->_request->getParam("firstname");
			$UserLName=$this->_request->getParam("lastname");
			$UserSkillname=$this->_request->getParam("UserSkill");
			$City = $this->_request->getParam("city");
			$Locality = $this->_request->getParam("locality");
			$Pincode = $this->_request->getParam("pincode");
			$TutorId = $this->_request->getParam("tutorId");
			
			if($this->_request->isXmlHttpRequest()){
				
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
					if($UserFName == "")$response["data"]["firstname"] = "null";
					else $response["data"]["firstname"] = "valid";
					
					if($UserLName == "")$response["data"]["lastname"] = "null";
					else $response["data"]["lastname"] = "valid";
					
					if($UserSkillname == "What you'd love to learn")$response["data"]["UserSkill"] = "null";
					else $response["data"]["UserSkill"] = "valid";
									
					if($City == "")$response["data"]["city"] = "selectnull";
					else $response["data"]["city"] = "valid";
					
					if($Locality == "")$response["data"]["locality"] = "selectnull";
					else $response["data"]["locality"] = "valid";
				
//					if($Pincode == "")$response["data"]["UserPincode"] = "null";
//					elseif((!is_numeric($Pincode) || $Pincode=="0" || strlen($Pincode)!=6))$response["data"]["UserPincode"] = "invalid";
//					else $response["data"]["UserPincode"] = "valid";
					
					if(!in_array('conformpassnull',$response['data']) && !in_array('selectnull',$response['data']) && !in_array('passnotmatch',$response['data']) && !in_array('passlength',$response['data']) && !in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
				}
				else {
					$lastupdatedate = date("Y-m-d H:i:s");
					$data = array("tutor_for"=>$LookingFor,"first_name"=>$UserFName,"last_name"=>$UserLName,"skill_interested"=>$UserSkillname,
					"std_city"=>$City,"std_locality"=>$Locality,"std_pincode"=>$Pincode,"editdate"=>$lastupdatedate);
					//print_r($data);exit;
					if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
						$studentSignupObj->update($data,"id='$authUserNamespace->studentid'");
						$authUserNamespace->changessave = "Your changes have been saved";
						$this->_redirect("/student/personalinfo");
					}
				//	echo "<script>parent.Mediabox.close();</script>";
				}
			}
	}

	public function inboxmailAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$selectId = '2';
		$this->view->selectid = $selectId;
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		$tutormailResult =$mailMessagesObj->fetchAll($mailMessagesObj->select()
							->from(array('m'=>DATABASE_PREFIX."message"))
							->where("m.to_id='$authUserNamespace->studentid' && m.rec_flag='s' && m.inbox_delete_flag='0'")
							->order(array("id desc")));
		if (isset($tutormailResult) && sizeof($tutormailResult)>0)
		{
			$this->view->allmailresult = $tutormailResult;
			
			$page = $this->_request->getParam('page',1);
			//$this->view->page = $page;
			if($records_per_page=="")$records_per_page = 10;
			$record_count = sizeof($tutormailResult);
			$paginator = Zend_Paginator::factory($tutormailResult);
			$paginator->setItemCountPerPage($records_per_page);
			$paginator->setCurrentPageNumber($page);
			$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
			$this->view->allmailresult = $paginator;
			$page_number  = $record_count / 1;
			$page_number_last =  floor($page_number);
		}
		
	}

	public function sentmailAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$selectId = '3';
		$this->view->selectid = $selectId;
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		$tutormailResult =$mailMessagesObj->fetchAll($mailMessagesObj->select()
							->from(array('m'=>DATABASE_PREFIX."message"))
							->where("m.from_id='$authUserNamespace->studentid' && m.send_flag='s' && m.sent_delete_flag='0' ")
							->order(array("id desc")));
		if (isset($tutormailResult) && sizeof($tutormailResult)>0)
			{
				$this->view->allmailresult = $tutormailResult;
				
				$page = $this->_request->getParam('page',1);
				//$this->view->page = $page;
				if($records_per_page=="")$records_per_page = 10;
				$record_count = sizeof($tutormailResult);
				$paginator = Zend_Paginator::factory($tutormailResult);
				$paginator->setItemCountPerPage($records_per_page);
				$paginator->setCurrentPageNumber($page);
				$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
				$this->view->allmailresult = $paginator;
				$page_number  = $record_count / 1;
				$page_number_last =  floor($page_number);
			}
	}
	public function readsentmailAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$selectId = '3';
		$this->view->selectid = $selectId;
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$mailId = $this->_request->getParam('mail_id');
		if (isset($mailId) && $mailId!="")
		{
			$mailrowResult = $mailMessagesObj->fetchRow("id='$mailId'");
			$this->view->readsentmailresult = $mailrowResult;
		}
			
		
	}
	public function readmailAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$selectId = '2';
		$this->view->selectid = $selectId;
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$mailId = $this->_request->getParam('mail_id');
		if (isset($mailId) && $mailId!="")
		{
			$mailrowResult = $mailMessagesObj->fetchRow("id='$mailId'");
			$this->view->readmailresult = $mailrowResult;
		}
		if($this->_request->isPost()) {
				
				$message_content = $this->_request->getParam('messagetext');
				$response = array();
				if($this->_request->isXmlHttpRequest()){
					
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					
					if($message_content == "")$response["data"]["messagetext"] = "null";
					else $response["data"]["messagetext"] = "valid";
					
					if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
					&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
					$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
			
					echo json_encode($response);
			
					}else{
						$lastupdatedate = date("Y-m-d H:i:s");
						$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
						$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
						if (isset($mailId) && $mailId!="")
						{
							$mailrowResult = $mailMessagesObj->fetchRow("id='$mailId'");
							//$student_data = $studentSignupObj->fetchRow("id='$mailrowResult->to_id'");
							$to_id = $mailrowResult->from_id;
							$subject_name = $mailrowResult->subject;
							//echo $mailId;exit;
							$data1 = array("from_id"=>$authUserNamespace->studentid,"to_id"=>$to_id,"parent_id"=>$mailId,
							"send_flag"=>"s","rec_flag"=>"t","mail_content"=>$message_content,"subject"=>$subject_name,"mail_date"=>$lastupdatedate
							);
							//print_r($data1);exit;
							$mailMessagesObj->insert($data1);
							$this->_redirect("/student/inboxmail");
						}
						
					}
				}
		
		
	}
	public function deleteinboxmailAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		if($this->_request->isPost()){
			$check_id = $this->_request->getParam('checkboxid');	
			//echo $check_ids;exit;
			if($this->_request->isXmlHttpRequest()){
			$tags = "success";
			
			$data = array("inbox_delete_flag"=>"1");
			//$mailMessagesObj->delete("id='$check_id'");
			$mailMessagesObj->update($data,"id=$check_id");
			echo json_encode($tags);
			}
		 }
	}
	
	public function deletesentmailAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		if($this->_request->isPost()){
			$check_id = $this->_request->getParam('checkboxid');	
			//echo $check_ids;exit;
			if($this->_request->isXmlHttpRequest()){
			$tags = "success";
			
			$data = array("sent_delete_flag"=>"1");
			//$mailMessagesObj->delete("id='$check_id'");
			$mailMessagesObj->update($data,"id=$check_id");
			echo json_encode($tags);
			}
		 }
	}
	public function emailpassAction(){
				$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
				if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
				$selectId = '6';
				$this->view->selectid = $selectId;
				$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
				$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
			    if (isset($authUserNamespace->studentid) && $authUserNamespace->studentid!="")
			    {
			    	$studentData = $studentSignupObj->fetchRow("id ='$authUserNamespace->studentid'");
					$this->view->studentdata = $studentData;
			    }
				if($this->_request->isPost()){
							
							$emailtxt = $this->_request->getParam("email");
							if($this->_request->isXmlHttpRequest()){
							
								$this->_helper->layout()->disableLayout();
								$this->_helper->viewRenderer->setNoRender(true);
								$response=array();
				
								if($emailtxt == "")$response["data"]["email"] = "null";
								elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $emailtxt)) $response['data']['email'] = "emailinvalid";
								else $response["data"]["email"] = "valid";
				
								/*fo update time email not duplicate so if condition*/
								$tutorProfile_row = $tutorProfileobj->fetchRow($tutorProfileobj->select()
																 ->where("tutor_email='$emailtxt'"));
								if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
								{
									$response['data']['email'] = "emailduplicate";
								}
								$studentsignup_row = $studentSignupObj->fetchRow($studentSignupObj->select()
															 	 	  ->where("std_email='$emailtxt' && id!='$authUserNamespace->studentid'"));
								if(isset($studentsignup_row) && sizeof($studentsignup_row) > 0)
								{
									$response['data']['email'] = "emailduplicate";
								}
												
								if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
								&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
								$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
								else $response['returnvalue'] = "validation";
								echo json_encode($response);
							
							}
							else {
				
								$data = array("std_email"=>$emailtxt);
								$studentSignupObj->update($data,"id='$authUserNamespace->studentid'");
								$authUserNamespace->changesave = "Your changes have been saved";
								$this->_redirect("/student/emailpass");
								
							}
						}
			}
	public function changepassAction(){
				$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
				if(!isset($authUserNamespace->studentid) && $authUserNamespace->studentid==""){$this->_redirect('/');}
				$this->_helper->layout()->disableLayout();
			    $this->_helper->viewRenderer->setNoRender(true);
				$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
				$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
				if($this->_request->isPost()) {
				
				$current_password = $this->_request->getParam('CurrentPassword');
				$new_password = $this->_request->getParam('NewPassword');
				$confirm_password = $this->_request->getParam('ConfirmPassword');
				$student_id = $authUserNamespace->studentid;
				$response = array();
				if($this->_request->isXmlHttpRequest()){
					
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					
					$password_row = $studentSignupObj->fetchRow("std_password='$current_password' and id='$student_id'");
					
					if($current_password=="")$response["data"]["CurrentPassword"] = "null";
					elseif(!sizeof($password_row) > 0)$response["data"]["CurrentPassword"] = "wrongpass";
					elseif(strlen($current_password)<5)$response["data"]["CurrentPassword"] = "passlength";
					else $response["data"]["CurrentPassword"] = "valid";
					
					if($new_password == "")$response["data"]["NewPassword"] = "null";
					elseif(strlen($new_password)<5)$response["data"]["NewPassword"] = "passlength";
					else $response["data"]["NewPassword"] = "valid";
					
					if($confirm_password == "")$response["data"]["ConfirmPassword"] = "conformpassnull";
		      		elseif($confirm_password != $new_password)$response["data"]["ConfirmPassword"] = "passnotmatch";
					else $response["data"]["ConfirmPassword"] = "valid";
					
					if(!in_array('null',$response['data']) && !in_array('wrongpass',$response['data'])&& !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
					&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
					$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
			
					echo json_encode($response);
			
					}else{
					
						//$Current_pass_md5 = md5($current_password);
						//$insert_new_pass = md5($new_password);
						$data = array("std_password"=>$new_password);
						//print_r($data);exit;
						$studentSignupObj->update($data,"id='$student_id'");
						//$authUserNamespace->status_message = "Password updated successfully.";
						$authUserNamespace->changesave = "Your changes have been saved";
						$this->_redirect("/student/emailpass");
						
					}
				}
		}
	
	}
?>