<?php 
class TutorvideonewController extends Zend_Controller_Action
{
	public function init(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	$tutorId = $id;
	if (isset($tutorId)) {
		//echo "in";
		$authUserNamespace->admintutorid=$tutorId;
		$authUserNamespace->logintype = '1';
		
	}
    		
	}

public function addvideosAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
	 	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	 	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	 	$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
	 	
	 	if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
		{
			$tutor_id = $authUserNamespace->admintutorid;
		}
		else
		{
			$tutor_id = $this->_request->getParam("id");
		}	
	if($this->_request->isPost())
		{
			$videourl = $this->_request->getParam("videourl");
			
			if($this->_request->isXmlHttpRequest())
			{
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($videourl == "")$response["data"]["videourl"] = "null";
				else $response["data"]["videourl"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);			
			}
			else 
			{	
				$lastupdatedate = date("Y-m-d H:i:s");			
				$data = array("tutor_id"=>$tutor_id,"title"=>'',"video_link"=>$videourl,"lastupdatedate"=>$lastupdatedate);
				//print_r($data);exit;
				$tutorVideoObj->insert($data);	
				$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");		
				echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/videos'</script>";
			}
		}	
	}
	
public function editvideonameAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
		$video_id = $this->_request->getParam('id'); 
		
		$tutorvideoRow = $tutorVideoObj->fetchRow("id='$video_id'");
		if (isset($tutorvideoRow) && sizeof($tutorvideoRow)>0)
		{
			$videotitle = $tutorvideoRow->title;
			
			$videoid = $tutorvideoRow->id;	
			$this->view->videotitle = $videotitle;										
		}	
		if($this->_request->isPost())
		{
			$videotitle = $this->_request->getParam("videotitle");
			
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($videotitle == "")$response["data"]["videotitle"] = "null";
				else $response["data"]["albumtitle"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else 
			{
				$album_id = $this->_request->getParam('id'); 			
				//$lastupdatedate = date("Y-m-d H:i:s");	
				$data = array("title"=>$videotitle);				
  			    $tutorVideoObj->update($data,"id=$video_id");
  			    $fetch_data = $tutorVideoObj->fetchRow("id='$video_id'");
  			    $authUserNamespace->admintutorid=$fetch_data->tutor_id;		
  			    $videoResultdata = $tutorVideoObj->fetchAll($tutorVideoObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_videos"))
     											 ->where("v.tutor_id = '$authUserNamespace->admintutorid'")
     											 ->order(array("lastupdatedate DESC")));
     			$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");	
  			    echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/videos'</script>";						 
			}
		}
	}
public function tutorvideodeleteAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					
		$videotutorphotos = $this->_request->getParam('id'); 
		
			if(isset($videotutorphotos) && $videotutorphotos!="")
			{		
					$fetch_data = $tutorVideoObj->fetchRow("id='$videotutorphotos'");
  			    	$authUserNamespace->admintutorid=$fetch_data->tutor_id;						
					$tutorVideoObj->delete("id='$videotutorphotos'");
					$videoResultdata = $tutorVideoObj->fetchAll($tutorVideoObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_videos"))
     											 ->where("v.tutor_id = '$authUserNamespace->admintutorid'")
     											 ->order(array("lastupdatedate DESC")));
					$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
					$this->_redirect('/editprofilenew/videos');
			}
			
	}
	
	
	
}
?>