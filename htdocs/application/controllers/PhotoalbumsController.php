<?php
class PhotoalbumsController extends Zend_Controller_Action
{
	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='1') {
			//echo "in";
			$authUserNamespace->maintutorid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}else if(isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='2'){
			//echo "out";
			$authUserNamespace->studentid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}
    		
	}
public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
	 	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	 	
	 	if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$tutor_id = $authUserNamespace->maintutorid;
		}else{
			$tutor_id = $this->_request->getParam("id");
		}
	 	
		if($this->_request->isPost())
		{
			$uploadfromcourse = $this->_request->getParam("uploadfromcourse");
			//echo 'hi'.$addfromcomp;
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();

				$filecheck1 = basename($uploadfromcourse);
  				$ext = substr($filecheck1, strrpos($filecheck1, '.') + 1);
				$ext = strtolower($ext);
				
				if($uploadfromcourse == "")$response["data"]["uploadfromcourse"] = "imageuploaderror";
				elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['uploadfromcourse']="valid";
				else $response["data"]["uploadfromcourse"] = "invalid";
			
				if(!in_array('null',$response['data'])&& !in_array('imageuploaderror',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('duplicate',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else
			{

				  $uploadfromtypeary = $_FILES["uploadfromcourse"]["type"];
				  $tmpName1ary = $_FILES["uploadfromcourse"]["tmp_name"];				 
				  $fileNameary = $_FILES["uploadfromcourse"]["name"];
				  $image_sizeary = $_FILES["uploadfromcourse"]["size"];
				  $fileInfoary = $_FILES["uploadfromcourse"];
				
				  
				  $file_count = count($_FILES["uploadfromcourse"]["name"]);
				   for ($i=0; $i<$file_count; $i++) 
				     {
				     	$uploadfromtype=$fileNameary[$i];
				     	
				     	$filecheck2 = basename($uploadfromtype);
	  					$ext = substr($filecheck2, strrpos($filecheck2, '.') + 1);
						$ext1 = strtolower($ext);
						$flag='0';
						
				     	if($ext1 == 'jpg' || $ext1 == 'jpeg' || $ext1 == "gif" || $ext1 == "pjpeg" || $ext1 == "png")
				     	{		
							$flag='0';								
				     	}
				     	else 
				     	{
				     			$authUserNamespace->photossizeerror = "Invalid value for Image file type";
				     			$flag='1';		
								$this->_redirect('/photoalbums/index');
				     	}
				     }		
				 
				 	 for ($i=0; $i<$file_count; $i++) 
			     	 {
				     	$uploadfromtype=$uploadfromtypeary[$i];
						$tmpName1  =$tmpName1ary[$i];
				     	$image_size = $image_sizeary[$i];
				     	$fileName  =$fileNameary[$i];
				     	$image_size = $image_sizeary[$i];
			     
			     
				 		if(isset($image_size) && $image_size > 5242880 )
				 		 {
							$authUserNamespace->photossizeerror = "Image size is grater than 5 Mb";
							$this->_redirect('/photoalbums/index');
						 }
						 else
						 {	
							 $fp1 = fopen($tmpName1, 'r');
							 $addfromcomptypecontent = fread($fp1, filesize($tmpName1));
							 $content = fread($fp1, filesize($tmpName1));
							 fclose($fp1);
								
								$this->view->image_type=$uploadfromtype;
								$this->view->tmpName=$tmpName1;
					
							$fileInfo = $_FILES["uploadfromcourse"];
						
							list($imagewidth, $imageheight, $type, $attr) = getimagesize($tmpName1);
							
							list($newimagewidth, $newimageheight, $newtype, $newattr) = getimagesize($tmpName1);
		
							/* if($imagewidth > 800)
							 {
								$propimgw = 800/$imagewidth;
								$imagewidth = 800;
								$imageheight = $imageheight*$propimgw;
							}
			
							if($imageheight > 600)
							{
								$propimgh = 600/$imageheight;
								$imageheight = 600;
								$imagewidth = $imagewidth*$propimgh;
							} */
							$jpeg_quality = 90;
							$src = file_get_contents($tmpName1);
					    	$img_r = imagecreatefromstring($src);
					    	$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
					    	imagecopyresampled($dst_r,$img_r,0,0,0,0,$imagewidth,$imageheight,$newimagewidth,$newimageheight);
					
				    	
				    	
						if(sizeof($fileInfo) > 0)
						{
					
									$type = $fileInfo["type"][$i];
									$tmpName = $fileInfo["tmp_name"][$i];
							
							$tutorphotoData = $tutorphotosObj->fetchAll("tutor_id ='$tutor_id'");
							if (isset($tutorphotoData) && sizeof($tutorphotoData)>0)
							{
								$photoCounter = sizeof($tutorphotoData);
							}
							else
							{
								$photoCounter = 0;
							}
							$lastupdatedate1 = date("Y-m-d H:i:s");
							$datetime=md5($lastupdatedate1);
							
							$mainPhotoname = "tutor_".$tutor_id."_".$photoCounter."_".$datetime;
							$thumbPhotoname = "tutorthumb_".$tutor_id."_".$photoCounter."_".$datetime;
							
							//echo "sss".$mainPhotoname;exit;
							
							$imagefinaltype = explode("/",$type);
							
							$profileResult = $tutorProfile->fetchRow($tutorProfile->select()
											->setIntegrityCheck(false)
											->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.id','s.tutor_first_name','s.tutor_last_name','s.company_name','s.tutor_url_name'))
											->where("s.id='$tutor_id'"));
							if (isset($profileResult) && sizeof($profileResult)>0)
							{
								if(isset($profileResult->tutor_url_name) && $profileResult->tutor_url_name!="")
								{
									$move_path = dirname(dirname(dirname(__FILE__)))."/albums/";
									$finalpath = $move_path.$profileResult->tutor_url_name.$profileResult->id;
									 
									if (!file_exists($finalpath)) 
									{
									    mkdir($finalpath,509,true) ;
										if (!file_exists($finalpath."/thumb")) 
										{
										    mkdir($finalpath."/thumb",509,true) ;
										}
									}
								}
							
								imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/albums/".$profileResult->tutor_url_name.$profileResult->id."/".$mainPhotoname.".".$imagefinaltype[1],$jpeg_quality);
								
								$photoLink =  "/albums/".$profileResult->tutor_url_name.$profileResult->id."/".$mainPhotoname.".".$imagefinaltype[1];
								$thumbPhotolink =  "/albums/".$profileResult->tutor_url_name.$profileResult->id."/thumb/".$thumbPhotoname.".".$imagefinaltype[1];
								
								$imagewidth='157';
			    				$imageheight='129';
				    	
			    				$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
				    			imagecopyresized($dst_r,$img_r, 0, 0, 0, 0, $imagewidth, $imageheight, $newimagewidth, $newimageheight);
				    			
				    			
								imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/albums/".$profileResult->tutor_url_name.$profileResult->id."/thumb/".$thumbPhotoname.".".$imagefinaltype[1],$jpeg_quality);
								
								$lastupdatedate = date("Y-m-d H:i:s");
								$coverFlag='n';
								$data = array("tutor_id"=>$tutor_id,"tutor_album_id"=>'',"title"=>'',"photo_link"=>$photoLink,"thumb_photo_link"=>$thumbPhotolink,"cover_flag"=>$coverFlag,"lastupdatedate"=>$lastupdatedate);
								$tutorphotosObj->insert($data);
								$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->maintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->maintutorid");
								$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
								$checklistobj = new Skillzot_Model_DbTable_Checklist();
								$logobj = new Skillzot_Model_DbTable_Log();
								$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
										  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
										  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
								if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
								{
									$lastupdatedate = date("Y-m-d h:i:s");
									$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
										"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
									$jobtableobj->insert($data_for_job);
									$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
									//echo "job_id=".$jobSessionId;exit;
									$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
									"date_done"=>"NULL","done_flag"=>"0");
									$checklistobj->insert($data_for_checklist1);
									$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
									$logobj->insert($data_for_log);
								}
							}							
						}
					 }
			     }
					 echo "<script>window.parent.location='". BASEPATH ."/editprofile/photos'</script>";
					//$this->_redirect("'".BASEPATH ."/editprofile/photos'");				
			}
		}
	}
	
	public function uploadpicturestep2Action(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$img_tutor_id= $authUserNamespace->maintutorid;
			$this->view->id = $img_tutor_id;
		}else{
			$img_tutor_id=$this->_request->getParam("id");
			$this->view->id = $img_tutor_id;
		}
		if(isset($img_tutor_id) && $img_tutor_id!="")
		{
			 $photoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
     											 ->where("v.tutor_id = '$img_tutor_id'"));
     		if (isset($photoResultdata) && sizeof($photoResultdata) > 0)
     		{
     			$this->view->photoresult = $photoResultdata;
     		}
		}
		//echo "<script>parent.Mediabox.close();</script>";
	}
	
	public function showphotoAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			
			$tutorphotoData = $tutorphotosObj->fetchAll($tutorphotosObj->select()
	     											 ->where("tutor_id ='$authUserNamespace->maintutorid'")
	     											 ->order(array("lastupdatedate DESC")));	     											 												
			if (isset($tutorphotoData) && sizeof($tutorphotoData)>0)
			{
				$this->view->photoresultsall = $tutorphotoData;
			}
		}
		$photoId = $this->_request->getParam("id");
		
		if (isset($photoId) && $photoId!="")
		{
			$this->view->mainphotoid = $photoId;
			
			$tutorphotosRow = $tutorphotosObj->fetchRow("id='$photoId'");
			if (isset($tutorphotosRow) && sizeof($tutorphotosRow)>0)
			{
				$photoUrl = $tutorphotosRow->photo_link;
				if (isset($photoUrl) && $photoUrl!="")
				{
					$this->view->photourl = $photoUrl;
				}
			}
		}
	}
	
	public function saveimageAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout()->disableLayout();
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			if($this->_request->isPost()){
				
				if(isset($authUserNamespace->uploaded_image_name) && $authUserNamespace->uploaded_image_name!=""){
				
				$jpeg_quality = 90;
				$x=$this->_request->getParam("sizeX");
				$y=$this->_request->getParam("sizeY");
				$w=$this->_request->getParam("sizeW");
				$h=$this->_request->getParam("sizeH");
				
				$src=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_name);
	    		$img_r = imagecreatefromstring($src);
	    		$dst_r = ImageCreateTrueColor($w,$h);
	    		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
    			imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_name1,$jpeg_quality);
				
				//unset($authUserNamespace->uploaded_image_name);
				//unset($authUserNamespace->uploaded_image_name1);
						
   				exit;
				}elseif(isset($authUserNamespace->uploaded_image_edit_name) && $authUserNamespace->uploaded_image_edit_name!=""){
				
				$jpeg_quality = 90;
				$x=$this->_request->getParam("sizeX");
				$y=$this->_request->getParam("sizeY");
				$w=$this->_request->getParam("sizeW");
				$h=$this->_request->getParam("sizeH");
				
				$src=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name);
	    		$img_r = imagecreatefromstring($src);
	    		$dst_r = ImageCreateTrueColor($w,$h);
	    		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
    			imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name1,$jpeg_quality);
				
				//unset($authUserNamespace->uploaded_image_name);
				//unset($authUserNamespace->uploaded_image_name1);
				if (isset($authUserNamespace->courseIdforsave) && $authUserNamespace->courseIdforsave!="")
				{
					if (isset($authUserNamespace->uploaded_image_edit_name1) && $authUserNamespace->uploaded_image_edit_name1!="")
					{
						$image_typearr = explode(".",$authUserNamespace->uploaded_image_edit_name1);
						$image_type = "image/".$image_typearr[1];
						$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name1);
					}else{
						$image_type = "";
						$getCropimagecontent="";
					}
					$data = array("tutor_class_image_type"=>$image_type,"tutor_class_image_content"=>$getCropimagecontent);
					$tutorCourseobj->update($data,"id='$authUserNamespace->courseIdforsave'");
					$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->maintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->maintutorid");
					$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
						$checklistobj = new Skillzot_Model_DbTable_Checklist();
						$logobj = new Skillzot_Model_DbTable_Log();
						$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
								  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
						if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
						{
							$lastupdatedate = date("Y-m-d h:i:s");
							$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
							$jobtableobj->insert($data_for_job);
							$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
							//echo "job_id=".$jobSessionId;exit;
							$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
							"date_done"=>"NULL","done_flag"=>"0");
							$checklistobj->insert($data_for_checklist1);
							$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
							$logobj->insert($data_for_log);
						}
					unset($authUserNamespace->courseIdforsave);
				}		
   				exit;
				
				}
				
	    }
	
	}
	
public function editphotoAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		$photo_id = $this->_request->getParam('id'); 
		//echo $abc;exit;
		$tutorphotosRow = $tutorphotosObj->fetchRow("id='$photo_id'");
		if (isset($tutorphotosRow) && sizeof($tutorphotosRow)>0)
		{
			$phototitle = $tutorphotosRow->title;
			$photoid = $tutorphotosRow->id;	
			$this->view->phototitle = $phototitle;										
		}	
		if($this->_request->isPost())
		{
			$titlephoto = $this->_request->getParam("titlephoto");
			
			if($this->_request->isXmlHttpRequest())
			{
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($titlephoto == "")$response["data"]["titlephoto"] = "null";
				else $response["data"]["titlephoto"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			
			}
			else 
			{
				$photo_id = $this->_request->getParam('id'); 
				$tutor_album_id = $this->_request->getParam('tutor_album_id'); 					
				if(isset($tutor_album_id) && $tutor_album_id!="")
				{					
					$data = array("title"=>$titlephoto);				
  			    	$tutorphotosObj->update($data,"id=$photo_id && tutor_album_id=$tutor_album_id");
  			    	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->maintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->maintutorid");
  			    	$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
						$checklistobj = new Skillzot_Model_DbTable_Checklist();
						$logobj = new Skillzot_Model_DbTable_Log();
						$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
								  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
						if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
						{
							$lastupdatedate = date("Y-m-d h:i:s");
							$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
							$jobtableobj->insert($data_for_job);
							$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
							//echo "job_id=".$jobSessionId;exit;
							$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
							"date_done"=>"NULL","done_flag"=>"0");
							$checklistobj->insert($data_for_checklist1);
							$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
							$logobj->insert($data_for_log);
						} 			    			  				
  			    	echo "<script>window.parent.location='". BASEPATH ."/editprofile/showalbumsphotos/tutor_album_id/$tutor_album_id'</script>";	
				}		
				else 
				{					
					$data = array("title"=>$titlephoto);			
  			    	$tutorphotosObj->update($data,"id=$photo_id");	
  			    	$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
						$checklistobj = new Skillzot_Model_DbTable_Checklist();
						$logobj = new Skillzot_Model_DbTable_Log();
						$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
								  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
						if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
						{
							$lastupdatedate = date("Y-m-d h:i:s");
							$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
							$jobtableobj->insert($data_for_job);
							$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
							//echo "job_id=".$jobSessionId;exit;
							$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
							"date_done"=>"NULL","done_flag"=>"0");
							$checklistobj->insert($data_for_checklist1);
							$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
							$logobj->insert($data_for_log);
						}	
  			    	echo "<script>window.parent.location='". BASEPATH ."/editprofile/photos'</script>";
				}		 
			}
		}
	}
public function addalbumsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
	 	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	 	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	 	
	 	if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$tutor_id = $authUserNamespace->maintutorid;
		}
		else
		{
			$tutor_id = $this->_request->getParam("id");
		}	
	if($this->_request->isPost())
		{
			$titlealbum = $this->_request->getParam("titlealbum");
			
			if($this->_request->isXmlHttpRequest())
			{
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($titlealbum == "")$response["data"]["titlealbum"] = "null";
				else $response["data"]["titlealbum"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);			
			}
			else 
			{	
				$lastupdatedate = date("Y-m-d H:i:s");			
				$data = array("tutor_id"=>$tutor_id,"title"=>$titlealbum,"lastupdatedate"=>$lastupdatedate);
				//print_r($data);exit;
				$tutorAlbumObj->insert($data);
				$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->maintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->maintutorid");
				$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
						$checklistobj = new Skillzot_Model_DbTable_Checklist();
						$logobj = new Skillzot_Model_DbTable_Log();
						$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
								  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
						if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
						{
							$lastupdatedate = date("Y-m-d h:i:s");
							$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
							$jobtableobj->insert($data_for_job);
							$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
							//echo "job_id=".$jobSessionId;exit;
							$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
							"date_done"=>"NULL","done_flag"=>"0");
							$checklistobj->insert($data_for_checklist1);
							$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
							$logobj->insert($data_for_log);
						}				
				echo "<script>window.parent.location='". BASEPATH ."/editprofile/albums'</script>";
			}
		}	
	}
	
public function editalbumnameAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		$album_id = $this->_request->getParam('id'); 
		
		$tutoralbumsRow = $tutorAlbumObj->fetchRow("id='$album_id'");
		if (isset($tutoralbumsRow) && sizeof($tutoralbumsRow)>0)
		{
			$albumtitle = $tutoralbumsRow->title;
			
			$albumid = $tutoralbumsRow->id;	
			$this->view->albumtitle = $albumtitle;										
		}	
		if($this->_request->isPost())
		{
			$album_title = $this->_request->getParam("albumtitle");
			
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($album_title == "")$response["data"]["albumtitle"] = "null";
				else $response["data"]["albumtitle"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else 
			{
				$album_id = $this->_request->getParam('id'); 			
				$lastupdatedate = date("Y-m-d H:i:s");	
				$data = array("title"=>$album_title);				
  			    $tutorAlbumObj->update($data,"id=$album_id");	
  			    $tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->maintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->maintutorid");
  			    $jobtableobj = new Skillzot_Model_DbTable_Jobtable();
						$checklistobj = new Skillzot_Model_DbTable_Checklist();
						$logobj = new Skillzot_Model_DbTable_Log();
						$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
								  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
						if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
						{
							$lastupdatedate = date("Y-m-d h:i:s");
							$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
							$jobtableobj->insert($data_for_job);
							$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
							//echo "job_id=".$jobSessionId;exit;
							$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
							"date_done"=>"NULL","done_flag"=>"0");
							$checklistobj->insert($data_for_checklist1);
							$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
							$logobj->insert($data_for_log);
						}	
  			    echo "<script>window.parent.location='". BASEPATH ."/editprofile/albums'</script>";						 
			}
		}
	}
	
public function addalbumphotosAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();	 	
	 	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();	 	
	 	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	 	$tutor_album_id = $this->_request->getParam('tutor_album_id');
	 	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	 	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	 	
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$tutor_id = $authUserNamespace->maintutorid;
			$tutor_album_id = $this->_request->getParam('tutor_album_id');
		}
		else
		{
			$tutor_id = $this->_request->getParam("id");
			$tutor_album_id = $this->_request->getParam('tutor_album_id');
		}
		
	if($this->_request->isPost())
		{
			$filetoupload0 = $this->_request->getParam("filetoupload0");
			
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();	
				
				
				$filecheck1 = basename($filetoupload0);
  				$ext = substr($filecheck1, strrpos($filecheck1, '.') + 1);
				$ext = strtolower($ext);
				
				if($filetoupload0 == "")$response["data"]["filetoupload0"] = "imageuploaderror";
				elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['uploadfromcourse']="valid";
				else $response["data"]["filetoupload0"] = "invalid";
			
				if(!in_array('null',$response['data'])&& !in_array('imageuploaderror',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('duplicate',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else
			{
				 $uploadfromtypeary = $_FILES["filetoupload0"]["type"];
				 $tmpName1ary = $_FILES["filetoupload0"]["tmp_name"];				 
				 $fileNameary = $_FILES["filetoupload0"]["name"];
				 $image_sizeary = $_FILES["filetoupload0"]["size"];
				 $fileInfoary = $_FILES["filetoupload0"];
					
				 $file_count = count($_FILES["filetoupload0"]["name"]);
				 for ($i=0; $i<$file_count; $i++) 
			     {
			     	$uploadfromtype=$fileNameary[$i];
			     	$filecheck2 = basename($uploadfromtype);
  					$ext = substr($filecheck2, strrpos($filecheck2, '.') + 1);
					$ext1 = strtolower($ext);
					$flag='0';
			     	if($ext1 == 'jpg' || $ext1 == 'jpeg' || $ext1 == "gif" || $ext1 == "pjpeg" || $ext1 == "png")
			     	{		
						$flag='0';								
			     	}
			     	else 
			     	{
			     			$authUserNamespace->photossizeerror = "Invalid value for Image file type";
			     			$flag='1';		
							$this->_redirect('/photoalbums/addalbumphotos');
			     	}
			     }			     
				 for ($i=0; $i<$file_count; $i++) 
			     {
			     	$uploadfromtype=$uploadfromtypeary[$i];
					$tmpName1  =$tmpName1ary[$i];
			     	$image_size = $image_sizeary[$i];
			     	$fileName  =$fileNameary[$i];
			     	$image_size = $image_sizeary[$i];
			     
			     
			     
			     	
					     if(isset($image_size) && $image_size > 5242880 )
						 {
								$authUserNamespace->photossizeerror = "Image size is grater than 5 Mb";
								$this->_redirect('/photoalbums/addalbumphotos');
						 }
						 else
						 {	
							 $fp1 = fopen($tmpName1, 'r');
							 $addfromcomptypecontent = fread($fp1, filesize($tmpName1));
							 $content = fread($fp1, filesize($tmpName1));
							 fclose($fp1);
							
							 $this->view->image_type=$uploadfromtype;
							 $this->view->tmpName=$tmpName1;
							
							 $fileInfo = $_FILES["filetoupload0"];
							
							 list($imagewidth, $imageheight, $type, $attr) = getimagesize($tmpName1);								
							 list($newimagewidth, $newimageheight, $newtype, $newattr) = getimagesize($tmpName1);
			
							 /*if($imagewidth > 800)
							 {
									$propimgw = 800/$imagewidth;
									$imagewidth = 800;
									$imageheight = $imageheight*$propimgw;
							 }
					
							 if($imageheight > 600)
							 {
									$propimgh = 600/$imageheight;
									$imageheight = 600;
									$imagewidth = $imagewidth*$propimgh;
							 } */
								$jpeg_quality = 90;
								$src = file_get_contents($tmpName1);
						    	$img_r = imagecreatefromstring($src);
						    	$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
						    	imagecopyresampled($dst_r,$img_r,0,0,0,0,$imagewidth,$imageheight,$newimagewidth,$newimageheight);
						
						    	
						    	
								if(sizeof($fileInfo) > 0)
								{
							
									$type = $fileInfo["type"][$i];
									$tmpName = $fileInfo["tmp_name"][$i];
									
									$tutorphotoData = $tutorphotosObj->fetchAll("tutor_id ='$tutor_id'");
									if (isset($tutorphotoData) && sizeof($tutorphotoData)>0)
									{
										$photoCounter = sizeof($tutorphotoData);
									}
									else
									{
										$photoCounter = 0;
									}
									$lastupdatedate1 = date("Y-m-d H:i:s");
									$datetime=md5($lastupdatedate1);
									
									$mainPhotoname = "tutor_".$tutor_id."_".$photoCounter."_".$datetime;
									$thumbPhotoname = "tutorthumb_".$tutor_id."_".$photoCounter."_".$datetime;
									
									//echo "sss".$mainPhotoname;exit;
									
									
									$imagefinaltype = explode("/",$type);
									
									$profileResult = $tutorProfile->fetchRow($tutorProfile->select()
													->setIntegrityCheck(false)
													->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.id','s.tutor_first_name','s.tutor_last_name','s.company_name','s.tutor_url_name'))
													->where("s.id='$tutor_id'"));
									if (isset($profileResult) && sizeof($profileResult)>0)
									{
										if(isset($profileResult->tutor_url_name) && $profileResult->tutor_url_name!="")
										{
											$move_path = dirname(dirname(dirname(__FILE__)))."/albums/";
											$finalpath = $move_path.$profileResult->tutor_url_name.$profileResult->id;
											 
											if (!file_exists($finalpath)) 
											{
											    mkdir($finalpath,509,true) ;
												if (!file_exists($finalpath."/thumb")) 
												{
												    mkdir($finalpath."/thumb",509,true) ;
												}
											}
										 }
									
											imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/albums/".$profileResult->tutor_url_name.$profileResult->id."/".$mainPhotoname.".".$imagefinaltype[1],$jpeg_quality);
											
											$photoLink =  "/albums/".$profileResult->tutor_url_name.$profileResult->id."/".$mainPhotoname.".".$imagefinaltype[1];
											$thumbPhotolink =  "/albums/".$profileResult->tutor_url_name.$profileResult->id."/thumb/".$thumbPhotoname.".".$imagefinaltype[1];
											
											$imagewidth='157';
						    				$imageheight='129';
							    	
						    				$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
							    			imagecopyresized($dst_r,$img_r, 0, 0, 0, 0, $imagewidth, $imageheight, $newimagewidth, $newimageheight);
							    			
							    			
											imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/albums/".$profileResult->tutor_url_name.$profileResult->id."/thumb/".$thumbPhotoname.".".$imagefinaltype[1],$jpeg_quality);
											
											$lastupdatedate = date("Y-m-d H:i:s");
											
											/*if((isset($tutor_id) && $tutor_id!="") && (isset($tutor_album_id) && $tutor_album_id!=""))
											{												
									     		$photoResultdata = $tutorphotosObj->fetchRow("tutor_id = '$tutor_id' && tutor_album_id ='$tutor_album_id' && cover_flag = 'y'");
												if(isset($photoResultdata) && $photoResultdata!="")
												{
													$coverFlag='n';																		
												}
												else 
												{
													$coverFlag='y';																																				
												}									     		
											}*/	
											$coverFlag='n';																							
											$data = array("tutor_id"=>$tutor_id,"tutor_album_id"=>$tutor_album_id,"title"=>'',"photo_link"=>$photoLink,"thumb_photo_link"=>$thumbPhotolink,"cover_flag"=>$coverFlag,"lastupdatedate"=>$lastupdatedate);										
											$tutorphotosObj->insert($data);
											$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->maintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->maintutorid");
											$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
											$checklistobj = new Skillzot_Model_DbTable_Checklist();
											$logobj = new Skillzot_Model_DbTable_Log();
											$job_for_profilechange = $jobtableobj->fetchAll($jobtableobj->select()
													  ->from(array('s'=>DATABASE_PREFIX.'job_table'),array('*'))
													  ->where("s.state='NEW' && s.type = 'PROFILE_CHANGE' && s.tutor_id= '$authUserNamespace->maintutorid'"));
											if($job_for_profilechange=="" || sizeof($job_for_profilechange)==0)
											{
												$lastupdatedate = date("Y-m-d h:i:s");
												$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdatedate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
													"date_close"=>"NULL","date_next_followup"=>$lastupdatedate,"type"=>"PROFILE_CHANGE","tutor_id"=>$authUserNamespace->maintutorid,"student_id"=>"NULL");
												$jobtableobj->insert($data_for_job);
												$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
												//echo "job_id=".$jobSessionId;exit;
												$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"PROFILE_VERIFICATION","date_due"=>$lastupdatedate,"date_created"=>$lastupdatedate,
												"date_done"=>"NULL","done_flag"=>"0");
												$checklistobj->insert($data_for_checklist1);
												$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdatedate,"t_color"=>"#000000","entry"=>"New Job created by SERPA");
												$logobj->insert($data_for_log);
											}
								 }									
							}							
						}
				     }		     
				echo "<script>window.parent.location='". BASEPATH ."/editprofile/showalbumsphotos/tutor_album_id/$tutor_album_id'</script>";
			}
		}
		
	}
public function showalbumsAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutor_album_id = $this->_request->getParam('tutor_album_id');
		
		if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			
			$tutoralbumphotoData = $tutorphotosObj->fetchAll($tutorphotosObj->select()
	     											 ->where("tutor_id ='$authUserNamespace->maintutorid' && tutor_album_id ='$tutor_album_id'")
	     											 ->order(array("lastupdatedate DESC")));	     											 												
			if (isset($tutoralbumphotoData) && sizeof($tutoralbumphotoData)>0)
			{
				$this->view->albumphotoresultsall = $tutoralbumphotoData;
			}
		}
		$photoId = $this->_request->getParam("id");
		
		if (isset($photoId) && $photoId!="")
		{
			$this->view->mainphotoid = $photoId;
			
			$tutorphotosRow = $tutorphotosObj->fetchRow("id='$photoId'");
			if (isset($tutorphotosRow) && sizeof($tutorphotosRow)>0)
			{
				$photoUrl = $tutorphotosRow->photo_link;
				if (isset($photoUrl) && $photoUrl!="")
				{
					$this->view->photourl = $photoUrl;
				}
			}
		}
	}
}
?>