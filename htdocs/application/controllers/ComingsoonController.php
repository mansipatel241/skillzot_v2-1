<?php

class ComingsoonController extends Zend_Controller_Action{
	
	public function init(){
		$this->_helper->layout()->setLayout("layout4");
	}
	public function indexAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
	}
	public function comminglightAction()
	{
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			if($this->_request->isPost())
			{
				$CityName = $this->_request->getParam("CityName");
				$Email=$this->_request->getParam("Email");
			
				
						$authUserNamespace->commingcity = $CityName;
						$authUserNamespace->commingemail = $Email;
						
						$tags = "success";
						echo json_encode($tags);
						//echo "<script>parent.Mediabox.open('/commingsoon/almostfinish','','737 400');</script>";
						//$this->_redirect('/commingsoon/almostfinish');
					
			}
		
		
	}
	public function almostfinishAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
		
		if($this->_request->isPost()){
			
			$signType = $this->_request->getParam("SignType");
			$skillInterested = $this->_request->getParam("SkillInterested");
	
			if($this->_request->isXmlHttpRequest()){
				
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
														
					if($skillInterested == "Enter skills you want to learn or teach")$response["data"]["SkillInterested"] = "null";
					else $response["data"]["SkillInterested"] = "valid";
					

					if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
				}
				else {
					$lastupdatedate = date("Y-m-d H:i:s");
					$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
					$data = array("city"=>$authUserNamespace->commingcity,"email"=>$authUserNamespace->commingemail,"signin_as"=>$signType,"skill_interest"=>$skillInterested,"lastupdatedate"=>$lastupdatedate);
					$commingsoonObj->insert($data);
					$emailId = $authUserNamespace->commingemail;
					$message1 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
										<tr align='center' width=100%>
											<td  width=30%>City:</td>
											<td  width=30%>".$authUserNamespace->commingcity."</td>
										</tr>
										<tr align='center'>
											<td>Email:</td>
											<td width=30%>".$emailId."</td>
										</tr>
										<tr align='center'>
											<td>Sign Up as:</td>
											<td  width=30%>".$signType."</td>
										</tr>
										<tr align='center'>
											<td>Skill interested:</td>
											<td  width=30%>".$skillInterested."</td>
										</tr>
										</table>";
					$fromCotent = $authUserNamespace->commingcity.",".$skillInterested;
				
					$message = $message1;
					$to = "sz.registrations@skillzot.com";
					//$to = "jinu97@gmail.com";
					$from = $emailId;
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom($from,$fromCotent);
					$mail->addTo($to,"Admin");
					$mail->setSubject("Student sign up - coming soon");
					$mail->send();
					//----------------------backup email -----------------
					$message4 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
										<tr align='center' width=100%>
											<td  width=30%>City:</td>
											<td  width=30%>".$authUserNamespace->commingcity."</td>
										</tr>
										<tr align='center'>
											<td>Email:</td>
											<td width=30%>".$emailId."</td>
										</tr>
										<tr align='center'>
											<td>Sign Up as:</td>
											<td  width=30%>".$signType."</td>
										</tr>
										<tr align='center'>
											<td>Skill interested:</td>
											<td  width=30%>".$skillInterested."</td>
										</tr>
										</table>";
				
					$message = $message4;
					$to = "sz.contact@skillzot.com";
					//$to = "jinu97@gmail.com";
					$from = $emailId;
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom($from,$emailId);
					$mail->addTo($to,"Admin");
					$mail->setSubject("Coming soon sign up");
					$mail->send();
					//----------------------------------------------------
					//$authUserNamespace->forgetpass="Mail has been sent to your EmailId";
					//$this->view->message='Mail has been sent to your EmailId';
				
					$message2 = "<div style='text-align:justify;'>";
					$message2 .= "<div>Hi...<br/></div><br/>";
					$message2 .= "<div>Thanx for registering with Skillzot, you've made our day.<br/></div><br/>";
					$message2 .= "<div>Your details have been safely stored with us in an imaginary vault that even Tess Maar Khan can't crack. Rest assured, zero spam!.<br/></div><br/>";
					$message2 .= "<div>Our mission is to ensure that this <i>super-portal</i> is going to help you add new 'super powers' (our lingo for skills, think it's way cooler), so that you can <i>nurture your talent</i> and make a difference to yourself.<br/></div><br/>";
					$message2 .= "<div>We've just started services in Mumbai and hope to land in your city sooner rather than later. You will be amongst the 1st to be notified when we do so.<br/></div><br/>";
					$message2 .= "<div>Till then, feel free to stalk us on <a href='https://twitter.com/Skillzot' style='color:#1AA7EF;'>twitter</a> and/or <a href='http://www.facebook.com/pages/Skillzot/393253524025786' style='color:#1AA7EF;'>facebook</a>. It'll be fun.<br/></div><br/><br/>";
					$message2 .= "<div>Cheers,<br/></div>";
					$message2 .= "<div>Sandeep<br/></div><br/>";
					$message2 .= "<div>Co-founder @ Skillzot</div>";
					$message2 .= "</div>";
					$message2 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";

					$message = $message2;
					$to = $emailId;
					$from = "sz.registrations@skillzot.com";
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom($from,"Sandeep @ Skillzot");
					$mail->addTo($to,"User");
					$mail->setSubject("Hola! You've made our day");
					$mail->send();
					//$authUserNamespace->forgetpass="Mail has been sent to your EmailId";
					//$this->view->message='Mail has been sent to your EmailId';
				
					unset($authUserNamespace->commingcity);
					unset($authUserNamespace->commingemail);
					echo "<script>parent.Mediabox.close();</script>";
					//$this->_redirect('/comingsoon/thankyou');
				}
		}
		
	}
	public function thankyouAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout("lightbox");
	}
	public function aboutusAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout('layout11');
		if($this->_request->isPost()){
				$CityName = $this->_request->getParam("CityName");
				$Email=$this->_request->getParam("Email");
			
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
						
//						if($CityName == "")$response["data"]["CityName"] = "null";
//						else $response["data"]["CityName"] = "valid";
										
						if($Email == "Email" && $CityName == "City Name")$response["data"]["CityName"] = "null";
						elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $Email)) $response['data']['CityName'] = "invalid";
						else $response["data"]["CityName"] = "valid";

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						echo json_encode($response);
					}
					else {
						$authUserNamespace->commingcity = $CityName;
						$authUserNamespace->commingemail = $Email;
						$this->_redirect('/comingsoon/almostfinish');
					}
			}
	}
	public function wearehiringAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout('layout11');
		$this->view->werhireing = "yes";
		//$this->_helper->layout()->setLayout("lightbox");
	}
}
?>