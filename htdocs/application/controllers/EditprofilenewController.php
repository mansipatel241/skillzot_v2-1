<?php

class EditprofilenewController extends Zend_Controller_Action{

public function init(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->setLayout("editprofilepagenew");
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	$tutorId = $id;
	if (isset($tutorId)) {
		//echo "in";
		$authUserNamespace->admintutorid=$tutorId;
		$authUserNamespace->logintype = '1';
		//echo $authUserNamespace->admintutorid;
		$tutorPobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorD = $tutorPobj->fetchRow("id ='$tutorId'");
		$authUserNamespace->useridfriedly = $tutorD->company_name;
		//echo "in".$tutorD->company_name;
	}
		
}
public function indexAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}

}
public function editstatusAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) || $authUserNamespace->admintutorid=="")$this->_redirect("/adminnew/tutorsignup");
	
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	
	$this->view->category = $tutorProfile->fetchRow("id='$authUserNamespace->admintutorid'");
		
	if($this->_request->isPost()){
	
		$status_value = $this->_request->getParam("statusflag");
		
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			
			if($status_value == "")$response["data"]["statusflag"] = "onlyselectnull";
			else $response["data"]["statusflag"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
		}else{
			if ($status_value=="0"){
				$cDate=date('Y-m-d H:i:s');
				$data = array("is_active"=>"1","editdate"=>$cDate);
				$tutorProfile->update($data,"id='$authUserNamespace->admintutorid'");
				//$this->_redirect('/editprofilenew/editstatus');
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorsignup';</script>";
			}else{
				$cDate=date('Y-m-d H:i:s');
				$data = array("is_active"=>"0","editdate"=>$cDate);
				$tutorProfile->update($data,"id='$authUserNamespace->admintutorid'");
				//$this->_redirect('/editprofilenew/editstatus');
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorsignup';</script>";
			}
			//echo "<script>parent.Mediabox.close();</script>";
			
		}
	}
}
public function followupAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) || $authUserNamespace->admintutorid=="")$this->_redirect("/adminnew/tutorsignup");
	$selectId = '10';
	$this->view->selectid = $selectId;
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
							
	if(isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!=""){
		
	$tutorData = $tutorProfile->fetchRow("id='$authUserNamespace->admintutorid'");
	$this->view->tutorData = $tutorData;
	}
	if($this->_request->isPost()){
	
		$followup_comment = $this->_request->getParam("followup");
		
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();
			
			
			if($followup_comment == "")$response["data"]["followup"] = "onlyselectnull";
			else $response["data"]["followup"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
		}else{
			if ($followup_comment==""){
				$data = array("followup_comment"=>$followup_comment);
				$tutorProfile->update($data,"id='$authUserNamespace->admintutorid'");
				//$this->_redirect('/editprofilenew/followup');
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorsignup';</script>";
				
			}else{
				$data = array("followup_comment"=>$followup_comment);
				$tutorProfile->update($data,"id='$authUserNamespace->admintutorid'");
				//$this->_redirect('/editprofilenew/followup');
				echo "<script>window.parent.location='". BASEPATH ."/adminnew/tutorsignup';</script>";
				
			}
		
			
		}
	}
}
public function personalinfoAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$selectId = '2';
	$this->view->selectid = $selectId;
	
	$adressObj = new Skillzot_Model_DbTable_Address();
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$cityResult = $adressObj->fetchAll($adressObj->select()
							->from(array('a'=>DATABASE_PREFIX."master_address"))
							->where("a.address_type_id='1' && address_id!='0'"));
	if (isset($cityResult) && sizeof($cityResult)>0)
	{
		$this->view->city = $cityResult;
	}
	if(isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!=""){
			$tutorData = $tutorProfile->fetchRow("id ='$authUserNamespace->admintutorid'");
			$this->view->tutordata = $tutorData;
		}
if($this->_request->isPost()){
		//$companyName1 = $this->_request->getParam("company_name");//when forth radio button select
		$companyName2 = $this->_request->getParam("indi_cmnyname1");//when first three radio button select(individual)
		$firstnametxt = $this->_request->getParam("firstname");
		$lastnametxt = $this->_request->getParam("lastname");
		$userIDforunique = $this->_request->getParam("userIDforuniq");
		
		if (isset($tutorData->userid_new) && $tutorData->userid_new=="" && $tutorData->userid!=$userIDforunique)
		{
			$useridFlag = 1;
			$userIDforunique = $this->_request->getParam("userIDforuniq");	
			if(isset($userIDforunique) && $userIDforunique!=null)
			{
				if(ord($userIDforunique[0])>=97 && ord($userIDforunique[0])<=122)
				{
					$userIDforunique[0]=chr( ord($userIDforunique[0])-32);
				}
			}											
		}
		else if(isset($tutorData->userid_new) && $tutorData->userid_new!="")
		{
			
			$userIDforunique = $this->_request->getParam("userIDforuniq");
			if(isset($userIDforunique) && $userIDforunique!=null)
			{
				if(ord($userIDforunique[0])>=97 && ord($userIDforunique[0])<=122)
				{
					$userIDforunique[0]=chr( ord($userIDforunique[0])-32);
				}
			}					
		}	
		else
		{			
			$useridFlag = 0;
			$userIDforunique= $tutorData->userid;				
		}
//			echo $useridFlag;
//			echo $userIDforunique;exit;
//			$userIDforunique= str_replace(" ","",ucfirst(strtolower($userIDforunique)));
		$emailtxt = $this->_request->getParam("email");
		$passwordtxt = $this->_request->getParam("password");
		$confirm_passwordtxt = $this->_request->getParam("confirm_password");
//			$localitytxt = $this->_request->getParam("locality");
//			$citytxt = $this->_request->getParam("city");
//			$addresstxt = $this->_request->getParam("address");
//			$pincodetxt = $this->_request->getParam("pincode");
		$mobiletxt = $this->_request->getParam("mobile");
		$landlinetxt = $this->_request->getParam("landline");
		$selfDescription = $this->_request->getParam("OverviewDescription");
		$day = $this->_request->getParam("date");
		$month = $this->_request->getParam("month");
		$year = $this->_request->getParam("year");
		$date = $day."-".$month."-".$year;
		$fbLink = $this->_request->getParam("fb_link");
		$bankname = $this->_request->getParam("bank_name");
		$accno = $this->_request->getParam("acc_no");
		$ifsccode = $this->_request->getParam("ifsc_code");
		$twitterLink = $this->_request->getParam("twitter_link");
		//$linkedinLink = $this->_request->getParam("linkin_link");
		$videoLinkdis = $this->_request->getParam("video_link");
		$blogLink = $this->_request->getParam("blog_link");
		$websiteLink = $this->_request->getParam("website_link");
		$radiotxt = $this->_request->getParam("txtradio");
		$gender = $this->_request->getParam("gender");
		$adhar_license = $this->_request->getParam("adhar_license");
		$virtual_tour = $this->_request->getParam("virtual_tour");
		
		$phoneVisible = $this->_request->getParam("ph_visible_val");
		//echo $fbLink;exit;
		//----------------------fb twitter link correction with/without http-----------
		if (isset($fbLink) && $fbLink!="")
		{
			$fbVar1 = strstr($fbLink,'https://');
			$fbVar2 = strstr($fbLink,'http://');
			if (isset($fbVar1) && $fbVar1!="")
			{
				$tempVar2 = substr($fbVar1,8,strlen($fbVar1));
			}else if (isset($fbVar2) && $fbVar2!="")
			{
				$tempVar2 = substr($fbVar2,7,strlen($fbVar2));
			}
			else{
				$tempVar2 = $fbLink;
			}
		}
		if (isset($tempVar2) && $tempVar2!=""){
			$fbLink = "https://".$tempVar2;
		}else{
			$fbLink = "";
		}
		if (isset($twitterLink) && $twitterLink!="")
			{
				$twitVar1 = strstr($twitterLink,'https://');
				$twitVar2 = strstr($twitterLink,'http://');
				if (isset($twitVar1) && $twitVar1!="")
				{
					$twittempVar2 = substr($twitVar1,8,strlen($twitVar1));
				}else if (isset($twitVar2) && $twitVar2!="")
				{
					$twittempVar2 = substr($twitVar2,7,strlen($twitVar2));
				}
				else{
					$twittempVar2 = $twitterLink;
				}
			}
			
		if (isset($twittempVar2) && $twittempVar2!="" ){
			$twitterLink = "https://".$twittempVar2;
		}else{
			$twitterLink = "";
		}
		
		if (isset($blogLink) && $blogLink!="")
			{
				$blogVar1 = strstr($blogLink,'https://');
				$blogVar2 = strstr($blogLink,'http://');
				if (isset($blogVar1) && $blogVar1!="")
				{
					$blogtempVar2 = substr($blogVar1,8,strlen($blogVar1));
				}else if (isset($blogVar2) && $blogVar2!="")
				{
					$blogtempVar2 = substr($blogVar2,7,strlen($blogVar2));
				}
				else{
					$blogtempVar2 = $blogLink;
				}
			}
			
		if (isset($blogtempVar2) && $blogtempVar2!="" ){
			$blogLink = "http://".$blogtempVar2;
		}else{
			$blogLink = "";
		}
		
		if (isset($websiteLink) && $websiteLink!="")
			{
				$webVar1 = strstr($websiteLink,'https://');
				$webVar2 = strstr($websiteLink,'http://');
				if (isset($webVar1) && $webVar1!="")
				{
					$webtempVar2 = substr($webVar1,8,strlen($webVar1));
				}else if (isset($webVar2) && $webVar2!="")
				{
					$webtempVar2 = substr($webVar2,7,strlen($webVar2));
				}
				else{
					$webtempVar2 = $websiteLink;
				}
			}
		if (isset($webtempVar2) && $webtempVar2!="" ){
			$websiteLink = "http://".$webtempVar2;
		}else{
			$websiteLink = "";
		}
		//-------------------------------------------------------------------------	
		
		
		$v = 1;
		$videoLink = Array();
		for($i=0;$i<sizeof($videoLinkdis);$i++)
			{
				if($videoLinkdis[$i]!="")
				{
					//echo "f".$i."--";
					$videoLink[$v] = $videoLinkdis[$i];
					$v++;
				}
			}
		for($i=1+sizeof($videoLinkdis);$i<7;$i++){
			$videoLink[$i] = "";
		}
		 $v = $v - 1;
		 $v_count_val = $v;
		
			if($radiotxt==2)
			{
				$authUserNamespace->userclasstype = "2";
			}
			elseif($radiotxt==3)
			{
				$authUserNamespace->userclasstype = "3";
			}
			elseif($radiotxt==4)
			{
				$authUserNamespace->userclasstype = "4";
			}
			elseif($radiotxt==1)
			{
				$authUserNamespace->userclasstype = "4";
			}
			else{
				$authUserNamespace->userclasstype = "";
			}
		
		//if (isset($authUserNamespace->userclasstype) && $authUserNamespace->userclasstype=="4"){
		//	$companyName = $companyName1;
		//}else{
			$companyName = $companyName2;
		//}
		if($this->_request->isXmlHttpRequest()){
		
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response=array();

			if (isset($authUserNamespace->userclasstype) && $authUserNamespace->userclasstype=="4")
			{
				if($companyName == "")$response["data"]["indi_cmnyname1"] = "null";
				else $response["data"]["indi_cmnyname1"] = "valid";
			}
		
			if($firstnametxt == "")$response["data"]["firstname"] = "null";
			else $response["data"]["firstname"] = "valid";

			if($lastnametxt == "")$response["data"]["lastname"] = "null";
			else $response["data"]["lastname"] = "valid";
			
			if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
			{
				$userIDforunique= str_replace(" ","",$userIDforunique);
				$tutorProfilerowforuserID = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("userid='$userIDforunique' && id!='$authUserNamespace->admintutorid'"));
			}else{
				$userIDforunique= str_replace(" ","",$userIDforunique);
				$tutorProfilerowforuserID = $tutorProfile->fetchRow($tutorProfile->select()
												 ->where("userid='$userIDforunique'"));
			}
			
			if($userIDforunique == "")$response["data"]["userIDforuniq"] = "null";
			else if(isset($tutorProfilerowforuserID) && sizeof($tutorProfilerowforuserID) > 0)
			{
				$response['data']['userIDforuniq'] = "useridduplicate";
			}
			else if(preg_match("/[^a-zA-Z0-9]/i",$userIDforunique)){
				$response["data"]['userIDforuniq'] = "invalid";
			}
			else $response["data"]["userIDforuniq"] = "valid"; 
			/*fo update time email not duplicate so if condition*/

//				if($pincodetxt == "")$response["data"]["pincode"] = "null";
//				elseif((!is_numeric($pincodetxt) || $pincodetxt=="0" || strlen($pincodetxt)!=6))$response["data"]["pincode"] = "pincodeinvalid";
//				else $response["data"]["pincode"] = "valid";
//
//				if($localitytxt == "")$response["data"]["locality"] = "selectnull";
//				else $response["data"]["locality"] = "valid";
//
//				if($citytxt == "")$response["data"]["city"] = "selectnull";
//				else $response["data"]["city"] = "valid";
			
			if($mobiletxt == "")$response["data"]["mobile"] = "onlyenternull";
			elseif((!is_numeric($mobiletxt) || $mobiletxt=="0"))$response["data"]["mobile"] = "mobileinvalid";
			elseif(($mobiletxt != "" && strlen($mobiletxt)!=10))$response["data"]["mobile"] = "mobilelength";
			else $response["data"]["mobile"] = "valid";
			
			if($landlinetxt == "")$response["data"]["landline"] = "onlyenternull";
			elseif ($landlinetxt=="0" || strlen($landlinetxt)<10 || strlen($landlinetxt)>12)$response["data"]["landline"] = "mobilelength";
			else $response["data"]["landline"] = "valid";
			
			if ($response["data"]["mobile"]=="valid")
			{
				$response["data"]["landline"] = "valid";
			}elseif($response["data"]["landline"]=="valid"){
				$response["data"]["mobile"]="valid";
			}
			
				
//				if($addresstxt == "")$response["data"]["address"] = "null";
//				else $response["data"]["address"] = "valid";
			
			$selfDescription = trim(strip_tags($selfDescription),"'");
			if($selfDescription == "")$response["data"]["OverviewDescription"] = "onlyenternull";
			else $response["data"]["OverviewDescription"] = "valid";
				
			if(!in_array('null',$response['data'])&& !in_array('useridduplicate',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
			&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
			$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			echo json_encode($response);
			
		}
		else {
			$lastupdatedate = date("Y-m-d H:i:s");
			if(isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!=""){
				$id = $authUserNamespace->admintutorid;
			}
//				if (isset($authUserNamespace->userclasstype) && $authUserNamespace->userclasstype=="4")
//				{
		if (isset($useridFlag) && $useridFlag=='1')
		{
			$data = array("company_name"=>$companyName,"tutor_first_name"=>$firstnametxt,"tutor_last_name"=>$lastnametxt,"userid_new"=>$userIDforunique,
						 "tutor_brief_desc"=>$selfDescription,"tutor_dob"=>$date,
						 "tutor_fb_link"=>$fbLink,"tutor_tw_link"=>$twitterLink,"count_video"=>$v_count_val,
						 "bank_name"=>$bankname,"acc_no"=>$accno,"ifsc_code"=>$ifsccode,
						 "tutor_class_type"=>$radiotxt,"tutor_gender"=>$gender,
						 "tutor_vd_link"=>$videoLink[1],"tutor_vd_link1"=>$videoLink[2],"tutor_vd_link2"=>$videoLink[3],"tutor_vd_link3"=>$videoLink[4],"tutor_vd_link4"=>$videoLink[5],"tutor_vd_link5"=>$videoLink[6],
						 "blog_link"=>$blogLink,"website_link"=>$websiteLink,
			 			// "tutor_add_locality"=>$localitytxt,"tutor_add_city"=>$citytxt,
			 			// "tutor_add_pincode"=>$pincodetxt,
			 			 "tutor_mobile"=>$mobiletxt,"tutor_landline"=>$landlinetxt,"phone_visible"=>$phoneVisible,"editdate"=>$lastupdatedate,"adhar_license"=>$adhar_license,"virtual_tour"=>$virtual_tour
			 			);
		}else{
			$data = array("company_name"=>$companyName,"tutor_first_name"=>$firstnametxt,"tutor_last_name"=>$lastnametxt,
						 "tutor_brief_desc"=>$selfDescription,"tutor_dob"=>$date,
						 "tutor_fb_link"=>$fbLink,"tutor_tw_link"=>$twitterLink,"count_video"=>$v_count_val,
						 "bank_name"=>$bankname,"acc_no"=>$accno,"ifsc_code"=>$ifsccode,
						 "tutor_class_type"=>$radiotxt,"tutor_gender"=>$gender,
						 "tutor_vd_link"=>$videoLink[1],"tutor_vd_link1"=>$videoLink[2],"tutor_vd_link2"=>$videoLink[3],"tutor_vd_link3"=>$videoLink[4],"tutor_vd_link4"=>$videoLink[5],"tutor_vd_link5"=>$videoLink[6],
						 "blog_link"=>$blogLink,"website_link"=>$websiteLink,
			 			 //"tutor_add_locality"=>$localitytxt,"tutor_add_city"=>$citytxt,
			 			// "tutor_add_pincode"=>$pincodetxt,
			 			 "tutor_mobile"=>$mobiletxt,"tutor_landline"=>$landlinetxt,"phone_visible"=>$phoneVisible,"editdate"=>$lastupdatedate,"adhar_license"=>$adhar_license,"virtual_tour"=>$virtual_tour
			 			);
		}
//			print_r($data);exit;
				
			//}
//				else{
//					$data = array("tutor_first_name"=>$firstnametxt,"tutor_last_name"=>$lastnametxt,
//							 "tutor_brief_desc"=>$selfDescription,"tutor_dob"=>$date,
//							 "tutor_fb_link"=>$fbLink,"tutor_tw_link"=>$twitterLink,
//							 "tutor_ld_link"=>$linkedinLink,"tutor_vd_link"=>$videoLink,
//				 			 "tutor_address"=>$addresstxt,"tutor_add_locality"=>$localitytxt,"tutor_add_city"=>$citytxt,
//				 			 "tutor_add_pincode"=>$pincodetxt,"tutor_mobile"=>$mobiletxt,"tutor_landline"=>$landlinetxt
//				 			);
//				}
			
			if (isset($id) && $id!="")
			{
				
				$tutorProfile_row = $tutorProfile->fetchRow("id=$id");
				if($tutorProfile_row!="" && sizeof($tutorProfile_row)>0)
				{
					$tutorProfile->update($data,"id=$id");

					$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");

					if (isset($userIDforunique) && $userIDforunique!="")
					{ 
						$authUserNamespace->useridfriedly=$userIDforunique;					
						//echo 	$authUserNamespace->useridfriedly;exit;	
						setcookie("useridfriendly", $authUserNamespace->useridfriedly, time()+60*60*24*365);							
					}
					$authUserNamespace->changessave = "Your changes have been saved";
					$this->_redirect('/editprofilenew/personalinfo');
				}
			}
		}
	}
	
}
public function getlocalityAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$adresstypeObj = new Skillzot_Model_DbTable_Addresstype();
		$adressObj = new Skillzot_Model_DbTable_Address();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$cityid = $this->_request->getParam("city_id");
				$address_rows = $adressObj->fetchAll($adressObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_address"))
															  ->where("s.address_id!='0' && s.parent_address_id='$cityid'"));
				$i=0;
				$tags = "";
				foreach($address_rows as $a){
					$tags[$i]['address_name'] = $a->address_value;
					$tags[$i]['locality_id'] = $a->address_id;
					$i++;
				}
				echo json_encode($tags);
				}
	  		}
	}
	public function profilepicAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '1';
		$this->view->selectid = $selectId;
		//echo $authUserNamespace->admintutorid;exit;
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorId = $authUserNamespace->admintutorid;
		if (isset($tutorId) && $tutorId!=""){
			$tutorData = $tutorProfileobj->fetchRow("id ='$tutorId'");
			$authUserNamespace->userclasstype = $tutorData->tutor_class_type;
		}
		//echo $authUserNamespace->userclasstype;exit;
		if(isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!=""){
				$tutorData = $tutorProfileobj->fetchRow("id ='$authUserNamespace->admintutorid'");
				$this->view->tutordata = $tutorData;
		}
	if($this->_request->isPost()){

//			$image = $this->_request->getParam("imgfile");
		$imagecheck = $this->_request->getParam("imgch");
		if (isset($authUserNamespace->edit_profile_session1) && $authUserNamespace->edit_profile_session1!="")
		{
			$image = $authUserNamespace->edit_profile_session1;
		}else{
			$image = "";
		}
	 	if($this->_request->isXmlHttpRequest())
		{
		        $this->_helper->layout()->disableLayout();
		        $this->_helper->viewRenderer->setNoRender(true);
		        $response = array();
				
		    	$filecheck = basename($image);
				$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
				$ext = strtolower($ext);
			 	$f=0;
				if($image == ""){
					$f=1;
					if($image == "")$response["data"]["imgfile"] = "imageuploaderror";
					elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['imgfile']="valid";
					else $response["data"]["imgfile"] = "invalid";
				}
				
				if(($imagecheck!="" || $imagecheck!=null)&&($image!="" || $image!=null)){
                    	
					if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['imgfile']="valid";
					else {$response["data"]["imgfile"] = "invalid";	}
				}
				else{
					if($f!=1){
						$response["data"]["imgfile"]="valid";
					}
				}
				
				if(!in_array('null',$response['data']) && !in_array('imageuploaderror',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);

	    }else{
	    if (isset($authUserNamespace->edit_profile_session1) && $authUserNamespace->edit_profile_session1!="")
			{
				$image_typearr = explode(".",$authUserNamespace->edit_profile_session1);
				$image_type = "image/".$image_typearr[1];
				$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1);
				
				$jpeg_quality = 90;
				$ThumbWidth =160;
				$img_r = imagecreatefromstring($getCropimagecontent);
				imagefilter($img_r, IMG_FILTER_GRAYSCALE);
			    list($width, $height) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1);
				//calculate the image ratio
				$imgratio=$width/$height;
				if ($imgratio>1){
					$newwidth = $ThumbWidth;
					$newheight = $ThumbWidth/$imgratio;
				}else{
					$newheight = $ThumbWidth;
					$newwidth = $ThumbWidth*$imgratio;
				}
				// create a new temporary image
				
				$tmp_img = imagecreatetruecolor( $newwidth, $newheight );
				imagecopyresized($tmp_img, $img_r, 0, 0, 0, 0, $newwidth,$newheight, $width, $height);
				$target_path1 = dirname(dirname(dirname(__FILE__)))."/docs/Userfc.jpeg";
				//$tagetcrop = dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1;
				
				imagejpeg($tmp_img,$target_path1,$jpeg_quality);
				
				$filecheck = basename($target_path1);
				$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
				$ext = strtolower($ext);
				if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" ||$ext == "pjpeg" || $ext == "png"){
				 		$thumb_type="image/".$ext;
				}
				
				
				$imagethumbcontent = file_get_contents($target_path1);
				
				$imagew='100';
				$imageh='100';
				//$src=file_get_contents($target_path1);
    	
				$img_r = imagecreatefromstring($getCropimagecontent);
				$dst_r = ImageCreateTrueColor($imagew,$imageh);
				list($width11, $height11) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1);
    			//list($width11, $height11) = getimagesize($target_path1);
    			$jpeg = '.jpeg';
    			imagecopyresized($dst_r,$img_r, 0, 0, 0, 0, $imagew, $imageh, $width11, $height11);
				imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg,$jpeg_quality);
				$var1=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg);
				
				$data = array("tutor_image_type"=>$image_type,"tutor_image_content"=>$getCropimagecontent,
									"thumb_profile_content"=>$var1,"thumb_profile_type"=>$image_type,
									"tutor_thumb_type"=>$thumb_type,"tutor_thumb_content"=>$imagethumbcontent);
	    		$tutorProfileobj->update($data,"id=$authUserNamespace->admintutorid");
	    		$authUserNamespace->changesave = "Your changes have been saved";
	    		unset($authUserNamespace->edit_profile_session);
				unset($authUserNamespace->edit_profile_session1);
				array_map("unlink",glob(dirname(dirname(dirname(__FILE__)))."/docs/*.*" ) );
			//	$this->_redirect('/editprofile/profilepic');
			}
			//$this->_redirect('/editprofile/profilepic');
		}
	}
}

	public function experienceAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '3';
		$this->view->selectid = $selectId;
		
		$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
		$companylistObj = new Skillzot_Model_DbTable_Companylist();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorid = $authUserNamespace->admintutorid;
		
//			if (isset($authUserNamespace->userclasstype) && $authUserNamespace->userclasstype=="4")
//			{
		
			$companyaddressData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
												->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
												->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='4'"));
			$this->view->companyaddressdata = $companyaddressData;
			foreach ($companyaddressData as $addressID)
			{
				$addressIDarray[] =  $addressID->id;
			}
			$companyData = $companylistObj->fetchAll($companylistObj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_company"))
										->where("c.tutor_id='$tutorid'"));
										//echo 	sizeof($companyData);exit;
			$this->view->companydata = $companyData;
		//}
		if(isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
		{
			
			
			$tutorid = $authUserNamespace->admintutorid;
			
			//for page title--------
			$tutorsignupResult = $tutorProfileobj->fetchRow($tutorProfileobj->select()
						->setIntegrityCheck(false)
						->from(array('m'=>DATABASE_PREFIX.'tx_tutor_profile'),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
						->where("m.id='$tutorid'"));
			$this->view->tutordata = $tutorsignupResult;
			//-----------------
			$tutorexperienceData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
													->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
													->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='1'"));
			$this->view->tutorexperiencedata = $tutorexperienceData;
			foreach ($tutorexperienceData as $experienceID)
			{
				$tutorexperienceIDarray[] =  $experienceID->id;
			}
			$tutorqualificationData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
										->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='2'"));
			$this->view->tutorqualificationdata = $tutorqualificationData;
			
			foreach ($tutorqualificationData as $qulificationID)
			{
				$qulificationIDarray[] =  $qulificationID->id;
			}
			$tutoreducationData = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
									->where("c.tutor_id='$tutorid' && c.tutor_expedu_type='3'"));
			$this->view->tutoreducationdata = $tutoreducationData;
		
			foreach ($tutoreducationData as $educationID)
			{
				$educationIDarray[] =  $educationID->id;
			}
		}
		
		if($this->_request->isPost()){
				
		$institutetxt = $this->_request->getParam("institute");
		$subjecttxt = $this->_request->getParam("subject");
		$startyeardrp = $this->_request->getParam("startyear");
		$endyeardrp = $this->_request->getParam("endyear");
		
		$degreetxt = $this->_request->getParam("degree");
		$specialitytxt = $this->_request->getParam("speciality");
		$universitytxt = $this->_request->getParam("university");
		$completion_yeardrp = $this->_request->getParam("completion_year");
		
		$collegetxt = $this->_request->getParam("college");
		$diplomatxt = $this->_request->getParam("diploma");
		$fieldsofstudytxt = $this->_request->getParam("fieldsofstudy");
		$edbackcompletionyeardrp = $this->_request->getParam("edbackcompletionyear");
		
		for($i=0;$i<sizeof($institutetxt);$i++)
			{
				$statusteachname = "statusteach".$i;
				$statusTeach[$i] = $this->_request->getParam($statusteachname,0);
				
				$statusprofessionname = "statusprofession".$i;
				$statusProfession[$i] = $this->_request->getParam($statusprofessionname,0);
				
				$statushobbyname = "statushobby".$i;
				$statushobby[$i] = $this->_request->getParam($statushobbyname,0);
			}
		$companyName = $this->_request->getParam("company_name");
		$address = $this->_request->getParam("address");
		$contactNumber = $this->_request->getParam("contact");
		$companyStartyear =  $this->_request->getParam("companystartdate");
			
		if($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response=array();

			for($i=0;$i<sizeof($institutetxt);$i++)
			{
				if($institutetxt[$i] == "School/Organization/Self-employed" || $subjecttxt[$i]=="Skills taught or practiced" || $startyeardrp[$i]=="" || $endyeardrp[$i]=="")$response["data"]["institute".$i] = "allfield";
				else if($startyeardrp[$i]>$endyeardrp[$i])$response["data"]["institute".$i] = "yearerror";
				else $response["data"]["institute".$i] = "valid";
			}
			for($i=0;$i<sizeof($statusTeach);$i++)
			{
				if($statusTeach[$i] == "0" && $statusProfession[$i]=="0" && $statushobby[$i]=="0")$response["data"]["statusteach".$i] = "null";
				else $response["data"]["statusteach".$i] = "valid";
			}
			
			for($i=0;$i<sizeof($degreetxt);$i++)
			{
				if($degreetxt[$i] == "Degree/Diploma/etc" || $specialitytxt[$i]=="Speciality" || $universitytxt[$i]=="College/University/Institute" || $completion_yeardrp[$i]=="")$response["data"]["degree".$i] = "null";
				else $response["data"]["degree".$i] = "valid";
			}
			for($i=0;$i<sizeof($collegetxt);$i++)
			{
				if($collegetxt[$i] == "College/School" || $diplomatxt[$i]=="Degree/Diploma" || $fieldsofstudytxt[$i]=="Fields of Study" || $edbackcompletionyeardrp[$i]=="")$response["data"]["college".$i] = "null";
				else $response["data"]["college".$i] = "valid";
			}
			if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";

			echo json_encode($response);
		}
		else {
			$lastupdatedate = date("Y-m-d H:i:s");
			if (isset($tutorid) && $tutorid!="")
			{
				
				for($i=0;$i<sizeof($institutetxt);$i++)
				{
					$data = array("tutor_id"=>$tutorid,"tutor_expedu_type"=>'1',
	 								"tutor_expedu_kind"=>$institutetxt[$i],"tutor_expedu_place"=>"",
	   								"tutor_expedu_field"=>$subjecttxt[$i],
	   								"teach_flag"=>$statusTeach[$i],"profession_flag"=>$statusProfession[$i],"hobby_flag"=>$statushobby[$i],
	   								"tutor_expedu_start_year"=>$startyeardrp[$i],
	   								"tutor_expedu_end_year"=>$endyeardrp[$i],"lastupdatedate"=>$lastupdatedate);
					$tutorexperienceObj->update($data,"tutor_id=$tutorid && tutor_expedu_type='1' && id='$tutorexperienceIDarray[$i]'");
				}
				for($j=0;$j<sizeof($degreetxt);$j++)
				{
					$data = array("tutor_id"=>$tutorid,"tutor_expedu_type"=>'2',
	   								"tutor_expedu_kind"=>"$degreetxt[$j]","tutor_expedu_place"=>$universitytxt[$j],
	   								"tutor_expedu_field"=>$specialitytxt[$j],
	   								"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
	   								"tutor_expedu_start_year"=>"",
	   								"tutor_expedu_end_year"=>"$completion_yeardrp[$j]","lastupdatedate"=>$lastupdatedate);
					$tutorexperienceObj->update($data,"tutor_id=$tutorid && tutor_expedu_type='2' && id='$qulificationIDarray[$j]'");
				}
				for($k=0;$k<sizeof($collegetxt);$k++)
				{
					$data = array("tutor_id"=>$tutorid,"tutor_expedu_type"=>'3',
									"tutor_expedu_kind"=>"$diplomatxt[$k]","tutor_expedu_place"=>$collegetxt[$k],
									"tutor_expedu_field"=>$fieldsofstudytxt[$k],
									"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
									"tutor_expedu_start_year"=>'',
									"tutor_expedu_end_year"=>"$edbackcompletionyeardrp[$k]","lastupdatedate"=>$lastupdatedate);
					$tutorexperienceObj->update($data,"tutor_id=$tutorid && tutor_expedu_type='3' && id='$educationIDarray[$k]'");
				}
				//--------------------start year update------------------------------
				$companyforstartyearData = $companylistObj->fetchRow("tutor_id ='$tutorid'");
				if(isset($companyforstartyearData) && sizeof($companyforstartyearData)>0)
				{
					$data1 = array("tutor_expedu_type"=>'4',"tutor_id"=>$tutorid,"company_start_year"=>$companyStartyear,
									"lastupdatedate"=>$lastupdatedate);
					$companylistObj->update($data1,"tutor_id=$tutorid ");
				}else{
					$data1 = array("tutor_expedu_type"=>'4',"tutor_id"=>$tutorid,"company_start_year"=>$companyStartyear,
									"lastupdatedate"=>$lastupdatedate);
					$companylistObj->insert($data1);
				}
				//--------------------end of start year update------------------------------
				$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
				$authUserNamespace->changesave = "Your changes have been saved";
				//-----------------qualification count------------
				$tutorDataquali = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
												->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
												->where("c.tutor_id='$authUserNamespace->admintutorid' && c.tutor_expedu_type='2'"));
				if(isset($tutorDataquali) && sizeof($tutorDataquali)>0)
				{	
					$totalQualicount = sizeof($tutorDataquali);
					$data2 = array("qualification_count"=>$totalQualicount);
					$tutorexperienceObj->update($data2,"tutor_id=$authUserNamespace->admintutorid");
				}
				//-----------------end---------------------
				$this->_redirect('/editprofilenew/experience');
			
		}
		
	}
}
}
	public function addexperienceAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '3';
		$this->view->selectid = $selectId;
		$this->_helper->layout()->disableLayout();
		$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
			
	if($this->_request->isPost()){
				
		$institutetxt = $this->_request->getParam("institute");
		$subjecttxt = $this->_request->getParam("subject");
		$startyeardrp = $this->_request->getParam("startyear");
		$endyeardrp = $this->_request->getParam("endyear");
		
		for($i=0;$i<sizeof($institutetxt);$i++)
			{
				$statusteachname = "statusteach".$i;
				$statusTeach[$i] = $this->_request->getParam($statusteachname,0);
				
				$statusprofessionname = "statusprofession".$i;
				$statusProfession[$i] = $this->_request->getParam($statusprofessionname,0);
				
				$statushobbyname = "statushobby".$i;
				$statushobby[$i] = $this->_request->getParam($statushobbyname,0);
			}
		if($this->_request->isXmlHttpRequest())
		{
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response=array();

			for($i=0;$i<sizeof($institutetxt);$i++)
			{
				if($institutetxt[$i] == "School/Organization/Self-employed" || $subjecttxt[$i]=="Skills taught or practiced" || $startyeardrp[$i]=="" || $endyeardrp[$i]=="")$response["data"]["institute".$i] = "allfield";
				else if($startyeardrp[$i]>$endyeardrp[$i])$response["data"]["institute".$i] = "yearerror";
				else $response["data"]["institute".$i] = "valid";
			}
			for($i=0;$i<sizeof($statusTeach);$i++)
			{
				if($statusTeach[$i] == "0" && $statusProfession[$i]=="0" && $statushobby[$i]=="0")$response["data"]["statusteach".$i] = "statuserror";
				else $response["data"]["statusteach".$i] = "valid";
			}
			
			if(!in_array('yearerror',$response['data']) && !in_array('allfield',$response['data']) && !in_array('statuserror',$response['data']) && !in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";

			echo json_encode($response);
		}
		else {
			$lastupdatedate = date("Y-m-d H:i:s");	
			for($i=0;$i<sizeof($institutetxt);$i++)
				{
					$data = array("tutor_id"=>$authUserNamespace->admintutorid,"tutor_expedu_type"=>'1',
									"tutor_expedu_kind"=>$institutetxt[$i],"tutor_expedu_place"=>"",
									"tutor_expedu_field"=>$subjecttxt[$i],
									"teach_flag"=>$statusTeach[$i],"profession_flag"=>$statusProfession[$i],"hobby_flag"=>$statushobby[$i],
									"comp_address"=>"","comp_contactno"=>"",
									"tutor_expedu_start_year"=>$startyeardrp[$i],"tutor_expedu_end_year"=>$endyeardrp[$i],"lastupdatedate"=>$lastupdatedate);
					$tutorexperienceObj->insert($data);
				}
				echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/experience';</script>";
		}
		
	}
	}
	public function addqualificationAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$this->_helper->layout()->disableLayout();
		$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
		if($this->_request->isPost()){
						
				$degreetxt = $this->_request->getParam("degree");
				$specialitytxt = $this->_request->getParam("speciality");
				$universitytxt = $this->_request->getParam("university");
				$completion_yeardrp = $this->_request->getParam("completion_year");
				
				if($this->_request->isXmlHttpRequest())
				{
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
	
									
					for($i=0;$i<sizeof($degreetxt);$i++)
					{
						if($degreetxt[$i] == "Degree/Diploma/etc" || $specialitytxt[$i]=="Field/Speciality" || $universitytxt[$i]=="College/University/Institute" || $completion_yeardrp[$i]=="")$response["data"]["degree".$i] = "allfield";
						else $response["data"]["degree".$i] = "valid";
					}
					
					if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
	
					echo json_encode($response);
				}
				else {
						$lastupdatedate = date("Y-m-d H:i:s");
						for($j=0;$j<sizeof($degreetxt);$j++)
						{
							
							if($degreetxt[$j]!="Degree/Diploma/etc" && $universitytxt[$j]!="College/University/Institute" && $specialitytxt[$j]!="Field/Speciality" && $completion_yeardrp[$j]!=""){
								$data = array("tutor_id"=>$authUserNamespace->admintutorid,"tutor_expedu_type"=>'2',
											"tutor_expedu_kind"=>"$degreetxt[$j]","tutor_expedu_place"=>$universitytxt[$j],
											"tutor_expedu_field"=>$specialitytxt[$j],
											"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
											"tutor_expedu_start_year"=>'',
											"comp_address"=>"","comp_contactno"=>"",
											"tutor_expedu_end_year"=>"$completion_yeardrp[$j]","lastupdatedate"=>$lastupdatedate);
								$tutorexperienceObj->insert($data);
							}
						}
						//-----------------qualification count------------
						$tutorDataquali = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
														->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
														->where("c.tutor_id='$authUserNamespace->admintutorid' && c.tutor_expedu_type='2'"));
						if(isset($tutorDataquali) && sizeof($tutorDataquali)>0)
						{	
							$totalQualicount = sizeof($tutorDataquali);
							$data2 = array("qualification_count"=>$totalQualicount);
							$tutorexperienceObj->update($data2,"tutor_id=$authUserNamespace->admintutorid");
						}
						$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
						//-----------------end---------------------
						echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/experience';</script>";
				}
		}
	}
	public function addeducationAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$this->_helper->layout()->disableLayout();
		$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
		if($this->_request->isPost()){
					
			$collegetxt = $this->_request->getParam("college");
			$diplomatxt = $this->_request->getParam("diploma");
			$fieldsofstudytxt = $this->_request->getParam("fieldsofstudy");
			$edbackcompletionyeardrp = $this->_request->getParam("edbackcompletionyear");
			
			if($this->_request->isXmlHttpRequest())
			{
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
	
									
					for($i=0;$i<sizeof($collegetxt);$i++)
					{
						if($collegetxt[$i] == "College/School" || $diplomatxt[$i]=="Degree/Diploma" || $fieldsofstudytxt[$i]=="Fields of Study" || $edbackcompletionyeardrp[$i]=="")$response["data"]["college".$i] = "allfield";
						else $response["data"]["college".$i] = "valid";
					}
					if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
	
					echo json_encode($response);
			}
			else {
					$lastupdatedate = date("Y-m-d H:i:s");
					for($k=0;$k<sizeof($collegetxt);$k++)
					{
						
						if($collegetxt[$k]!="College/School" && $diplomatxt[$k]!="Degree/Diploma" && $fieldsofstudytxt[$k]!="Fields of Study" && $edbackcompletionyeardrp[$k]!=""){
							$data = array("tutor_id"=>$authUserNamespace->admintutorid,"tutor_expedu_type"=>'3',
										"tutor_expedu_kind"=>"$diplomatxt[$k]",
										"teach_flag"=>"","profession_flag"=>"","hobby_flag"=>"",
										"tutor_expedu_place"=>$collegetxt[$k],
										"tutor_expedu_field"=>$fieldsofstudytxt[$k],"tutor_expedu_start_year"=>'',
										"comp_address"=>"","comp_contactno"=>"",
										"tutor_expedu_end_year"=>"$edbackcompletionyeardrp[$k]","lastupdatedate"=>$lastupdatedate);
							$tutorexperienceObj->insert($data);
						}
					}
					echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/experience';</script>";
			}
			
	}
}
	public function skillsAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$selectId = '4';
			$this->view->selectid = $selectId;
			
			$skillobj1 = new Skillzot_Model_DbTable_Skills();
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			$skillRows = $skillobj1->fetchAll($skillobj1->select()
										  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
										  ->where("is_enabled = 1 && parent_skill_id = 0 && skill_id!=0")
										  ->order(array("order_id asc")));
			if (isset($skillRows) && sizeof($skillRows)>0)
			{
				$this->view->mainSkill = $skillRows;
			}
			$tutorgradeObj = new Skillzot_Model_DbTable_Tutorgradesteach();
			$tutorgradeResult = $tutorgradeObj->fetchAll();
			if (isset($tutorgradeResult) && sizeof($tutorgradeResult)>0)
			{
				$this->view->tutorgrade = $tutorgradeResult;
			}
			if(isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
			{
				$tutorcourseData = $tutorCourseobj->fetchRow("tutor_id ='$authUserNamespace->admintutorid'");
				$this->view->tutorcoursedata = $tutorcourseData;
				//for page title--------
				$tutorsignupResult = $tutorProfileobj->fetchRow($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX.'tx_tutor_profile'),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
							->where("m.id='$authUserNamespace->admintutorid'"));
				$this->view->tutordata = $tutorsignupResult;
			//-----------------
			}
			
	if($this->_request->isPost()){
			
		$skillIds = $this->_request->getParam("hiddenVal2");
		$gradeTeach = $this->_request->getParam("hiddenVal3");
		if (isset($gradeTeach) && $gradeTeach!="")
		{}else{$gradeTeach="";}
		if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
				
				if($skillIds == "")$response["data"]["hiddenVal2"] = "selectatleast";
				else $response["data"]["hiddenVal2"] = "valid";
				
//					if($gradeTeach == "")$response["data"]["hiddenVal3"] = "selectatleast";
//					else $response["data"]["hiddenVal2"] = "valid";
				
				if(!in_array('selectatleast',$response['data']) && !in_array('null',$response['data']) && !in_array('uncheck',$response['data']) &&  !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);
			}
			else {
				
				$findcourseid = $tutorCourseobj->fetchRow("tutor_id =$authUserNamespace->admintutorid");//find course id in course table
				$sizeofcorsedata = sizeof($findcourseid);
				if($sizeofcorsedata!='0' ){
					$data = array("tutor_skill_id"=>$skillIds,"tutor_grade_to_teach"=>$gradeTeach);
					$tutorCourseobj->update($data,"tutor_id=$authUserNamespace->admintutorid");
					$authUserNamespace->changesave = "Your changes have been saved";
				}else{
					$data = array("tutor_skill_id"=>$skillIds,"tutor_grade_to_teach"=>$gradeTeach,"tutor_id"=>$authUserNamespace->admintutorid);
					$tutorCourseobj->insert($data);
					$authUserNamespace->changesave = "Your changes have been saved";
				}
				$this->_redirect('/editprofilenew/skills');
				
			}
	}
		}
	public function coursedetailAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
//				echo $authUserNamespace->defultcourseid;
//				unset($authUserNamespace->defultcourseid);
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$selectId = '5';
			$this->view->selectid = $selectId;
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			$tutorid = $authUserNamespace->admintutorid;
			//for page title--------
				$tutorsignupResult = $tutorProfileobj->fetchRow($tutorProfileobj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX.'tx_tutor_profile'),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
							->where("m.id='$authUserNamespace->admintutorid'"));
				$this->view->tutordata = $tutorsignupResult;
			//--------------
			$tutorcourseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
												->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('c.id','c.tutor_course_name'))
												->where("c.tutor_id='$tutorid'"));
			$this->view->coursedata = $tutorcourseData;
			$i = 0;
				foreach ($tutorcourseData as $course){
				 	if (isset($course->tutor_skill_id) && $course->tutor_skill_id!="")
						$courseName[$i] = $course->tutor_course_name;
						$tutorIds[$i] = $course->id;
						$i++;
				}
			
		}
public function getaddressAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);

	 	$batchObj = new Skillzot_Model_DbTable_Batch();
		
		if($this->_request->isPost()){
		
			if($this->_request->isXmlHttpRequest()){
			$locality = $this->_request->getParam("locality");
			$courseId = $this->_request->getParam("courseid");
			//echo $locality;exit;
			$batchaddressResult=$batchObj->fetchAll($batchObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.course_id ='$courseId' && c.locality='$locality'"));
			//print_r($batchaddressResult);exit;
			$i=0;
			$tags = "";
			foreach($batchaddressResult as $a){
				$tags[$i]['address'] = $a->address;
				$tags[$i]['pincode'] = $a->pincode;
				$tags[$i]['landmark'] = $a->landmark;
				$i++;
			}
			echo json_encode($tags);
			}
	  	}
	}
	public function viewbatchcalenderAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$selectId = '3';
			$this->view->selectid = $selectId;
			$this->_helper->layout()->disableLayout();
			$batchid = $this->_request->getParam("batchid");

			$batchObj= new Skillzot_Model_DbTable_Batch();
			$batchviewResult=$batchObj->fetchRow($batchObj->select()
						->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
						->where("c.id ='$batchid'"));
			if (isset($batchviewResult) && sizeof($batchviewResult) > 0) 
			$this->view->batchviewResult = $batchviewResult;

			$adressObj = new Skillzot_Model_DbTable_Address();
			$addressResult = $adressObj->fetchRow($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"))
									   ->where("a.address_type_id='2' && address_id='$batchviewResult->locality'"));
			if (isset($addressResult) && sizeof($addressResult) > 0) 
				$this->view->address = $addressResult;

			$cityResult = $adressObj->fetchRow($adressObj->select()
										->from(array('a'=>DATABASE_PREFIX."master_address"))
										->where("a.address_type_id='1' && a.address_id='$batchviewResult->city'"));
			if (isset($cityResult) && sizeof($cityResult) > 0) 
				$this->view->city = $cityResult;

			$tutor_id =$authUserNamespace->maintutorid;

			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$courseData = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"))
										->where("c.tutor_id='$tutor_id'"));
			if (isset($courseData) && sizeof($courseData)>0)
			{
				$this->view->coursedata = $courseData ;
				$i = 0;
				foreach ($courseData as $course){
				 	if ((isset($course->tutor_skill_id) && $course->tutor_skill_id!="")||(isset($course->tutor_course_name) && $course->tutor_course_name!="")||(isset($course->id) && $course->id!="")){
						$tutorskillIds[$i] = $course->tutor_skill_id;
						$courseName[$i] = $course->tutor_course_name;
						$tutorIds[$i] = $course->id;
						$tutorlesionloc[$i] =$course->tutor_lesson_location;
						$i++;
				 	}else{
					
					$tutorskillIds[] = '';
						$courseName[] = '';
						$tutorIds[] = '';
					}
				}
			//----temperary for one id--------------------
			
			//print_r($tutorIds);exit;
			$tMainId = $tutorIds[0];
			$coursesubData = $tutorCourseobj->fetchRow("id ='$tMainId'");

			$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
			$masteraddressobj = new Skillzot_Model_DbTable_Address();
			$lessonlocationResult = $lessonlocationObj->fetchRow("location_id ='$coursesubData->tutor_lesson_location'");
			if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
			{
				if ($lessonlocationResult->location_id=='3')
				{
					$this->view->tutorlessonlocation = "Teacher's location & open to traveling";
				}else{
					$this->view->tutorlessonlocation = $lessonlocationResult->location_name;
				}
				
			}
			

		}
	}
		public function addbatchcalenderAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$selectId = '3';
			$this->view->selectid = $selectId;
			$this->_helper->layout()->disableLayout();

			$batchObj= new Skillzot_Model_DbTable_Batch();

			$tutor_id =$authUserNamespace->admintutorid;
			$courseId = $this->_request->getParam("courseid");
			$corseId = $this->_request->getParam("corseid");
			$batchid = $this->_request->getParam("batchid");
			$defaultcorseId = $this->_request->getParam("defaultcorseid");

			//echo "courseid='".$courseId."'";exit;
			//echo "course".$corseId;exit;
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			$adressObj = new Skillzot_Model_DbTable_Address();
			$batchdayObj = new Skillzot_Model_DbTable_Batchday();
			$batchdayResult=$batchdayObj->fetchAll();
			$this->view->batchdayResult = $batchdayResult;

			$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
			$suburbsData = $localitysuburbsObj->fetchAll($localitysuburbsObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."locality_suburbs"))
															  ->where("s.is_active='1'")
															  ->order(array("s.order asc")));
			if (isset($suburbsData) && sizeof($suburbsData)>0)
			{
				$this->view->suburbsdata = $suburbsData;
			}			
			$addressResult = $adressObj->fetchAll($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"))
									   ->where("a.address_type_id='2' && address_id!='0'"));
			if (isset($addressResult) && sizeof($addressResult) > 0) 
				$this->view->address = $addressResult;
			$cityResult = $adressObj->fetchAll($adressObj->select()
									->from(array('a'=>DATABASE_PREFIX."master_address"))
									->where("a.address_type_id='1' && a.address_id!='0'"));
			if (isset($cityResult) && sizeof($cityResult) > 0) 
				$this->view->city = $cityResult;
			$addressbarResult = $adressObj->fetchAll($adressObj->select()
										->setIntegrityCheck(false)
										->distinct()
									   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('a.locality as locality','a.address as address','a.pincode as pincode','a.landmark as landmark'))
									   ->joinLeft(array('b'=>DATABASE_PREFIX."master_address"),'a.locality=b.address_id',array('b.address_value as address_value'))
									   ->where("b.address_type_id='2' && a.locality!='' and a.tutor_id='$tutor_id'"));
				$this->view->addressbarResult = $addressbarResult;
			if(isset($defaultcorseId) && $defaultcorseId!="" && $batchid=="" && $courseId=="" && $corseId=="")
			{
			
				$defaultcorseId=$authUserNamespace->maxcourseId;
				$this->view->defaultcorseId = $defaultcorseId;
				$batchResult=$batchObj->fetchAll();
				$this->view->batchResult = $batchResult;
				
			}
			if($batchid=="" && $defaultcorseId=="" && $courseId=="" && $corseId=="")
			{

					$defaultcorseId=$authUserNamespace->maxcourseId;
					$this->view->defaultcorseId = $defaultcorseId;
					$batchResult=$batchObj->fetchAll();
					$this->view->batchResult = $batchResult;
					

				
			}
			if (isset($batchid) && $batchid!="" && $corseId!="" && $courseId=="" && $defaultcorseId=="")
			{
				$this->view->corseId = $corseId;
				//$this->view->courseId = $courseId;
				$this->view->batchid = $batchid;
				$batchResult = $batchObj->fetchRow("id ='$batchid'");
				$this->view->batchResult = $batchResult;
				
			}
			if(isset($batchid) && $batchid!="" && $courseId!="" && $corseId=="" && $defaultcorseId=="")
			{
				$this->view->courseId = $courseId;
				$this->view->batchid = $batchid;
				$batchResult = $batchObj->fetchRow("id ='$batchid'");
				$this->view->batchResult = $batchResult;
				
			}
			if($batchid=="" && $courseId!="" && $corseId=="" && $defaultcorseId=="")
			{
				$this->view->courseId = $courseId;
				$batchResult = $batchObj->fetchAll();
				$this->view->batchResult = $batchResult;
				
			}
				
		if($this->_request->isPost()){
			$hiddenVal6 = $this->_request->getParam("hiddenVal6");
			
			if($hiddenVal6=="1")
			{
				$locality_stu = $this->_request->getParam("hiddenVal5");
				$travel_radius = $this->_request->getParam("travel_radius");
			}
			if($hiddenVal6=="2")
			{
				$address = $this->_request->getParam("address");
				//echo $address;exit;
				$city = $this->_request->getParam("city");
				//echo $city[0];exit;
				$locality_id = $this->_request->getParam("locality");
				//echo $locality_id[0];exit;
				$pincode = $this->_request->getParam("pincode");
				$landmark = $this->_request->getParam("landmark");
							
				$addressResultDisplay = $adressObj->fetchRow($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"),array('a.address_value as address_value'))
									   ->where("a.address_id='$locality_id[0]'"));	
			}
			//echo "student".$locality_stu;exit;
			$no_of_wks = $this->_request->getParam("no_of_wks");
			$no_of_days_per_wks = $this->_request->getParam("no_of_days_per_wks");
			$no_of_hrs_per_day = $this->_request->getParam("no_of_hrs_per_day");
			$from_timing = $this->_request->getParam("from_timing");
			$to_timing = $this->_request->getParam("to_timing");
			$batchday = $this->_request->getParam("hiddenVal2");
			//echo "batchday".$batchday;exit;
			$batch_size = $this->_request->getParam("batch_size");
				$batch_date1 = $this->_request->getParam("batch_date");
			$batchdatecheckbox = $this->_request->getParam("batchdatecheckbox");
			//echo "batch".$batch_date;exit;
			$tutor_batch_name = $this->_request->getParam("tutor_batch_name");
			if($batchdatecheckbox=="No fixed start date")
			{
				$batch_date="No fixed start date";
			}else
			{
				$batch_date=$batch_date1;
			}
			$tutor_loc=$hiddenVal6;
			
			if (isset($tutor_loc) && $tutor_loc=="1"){
				
			$tutor_loc_display="At Student's home : ";}
			
			elseif (isset($tutor_loc) && $tutor_loc=="2"){
						   		
			$tutor_loc_display=$addressResultDisplay->address_value." : ";}
			
			/*elseif (isset($tutor_loc) && $tutor_loc=="1,2"){	
			
			$tutor_loc_display="At Student's home and ".$addressResultDisplay->address_value." : ";}*/
			
			if(isset($from_timing) && $from_timing != ""){  $from_timing_display=$from_timing." to "; }
			if (isset($to_timing) && $to_timing!=""){$to_timing_display=$to_timing.", ";}
				
			$daytable = new Skillzot_Model_DbTable_Batchday();
			$days=explode(',',$batchday);
			sort($days);
		    $day_row1='';
			if(count($days))
			{
						for ($start=0; $start < count($days); $start++) {
											$day_row=$daytable->fetchRow($daytable->select()
																->from(array('c'=>DATABASE_PREFIX."master_tutor_batch_day"),array('day_name'))
																->where("day_id='$days[$start]'"));
											$day_row1.=$day_row->day_name.'-';	
						}
			}
			$day_list1=ltrim($day_row1,'-');
			$day_list=rtrim($day_list1,'-');
			if(isset($batchday) && $batchday != ""){
						if(count($days)=="8"){ $batchday_display="Everyday";}
						else { $batchday_display=$day_list; }
			}					  
			$batch_summary_display=$tutor_loc_display.$from_timing_display.$to_timing_display.$batchday_display;			
			
			$fromtime1=explode('am',$from_timing);
			$totime1=explode('am',$to_timing);
			$fromtime2=explode('pm',$from_timing);
			$totime2=explode('pm',$to_timing);
			//print_r($fromtime1);
			//echo "form1".sizeof($fromtime1);exit;
			//echo "form".$fromtime1[0]."and to".$totime1[0];exit;
			if($fromtime1['0']!=$from_timing && $totime1['0']!=$to_timing)
			{

				$from=$fromtime1['0'];
				$from_type = "am";
				$to=$totime1['0'];
				$to_type = "am";
				//echo "1st";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			elseif($fromtime1['0']==$from_timing && $totime1['0']==$to_timing)
			{
				$from=$fromtime2['0'];
				$from_type = "pm";
				$to=$totime2['0'];
				$to_type = "pm";
				//echo "2nd";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			elseif($fromtime1['0']!=$from_timing && $totime1['0']==$to_timing)
			{
				$from=$fromtime1['0'];
				$from_type ="am";
				$to=$totime2['0'];
				$to_type = "pm";
				//echo "3rd";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			elseif($fromtime1['0']==$from_timing && $totime1['0']!=$to_timing)
			{
				$from=$fromtime2['0'];
				$from_type = "pm";
				$to=$totime1['0'];
				$to_type = "am";
				//echo "4th";
				//echo "form".$from."and to".$to."from_type=".$from_type."to_type=".$to_type;exit;
			}
			$hid11 = $this->_request->getParam("hid11");
			$hid12 = $this->_request->getParam("hid12");
			$hid13 = $this->_request->getParam("hid13");
			$from_timing_add1 = $this->_request->getParam("from_timing1");
			$to_timing_add1 = $this->_request->getParam("to_timing1");
			$from_timing_add2 = $this->_request->getParam("from_timing2");
			$to_timing_add2 = $this->_request->getParam("to_timing2");
			$from_timing_add3 = $this->_request->getParam("from_timing3");
			$to_timing_add3 = $this->_request->getParam("to_timing3");
			$day11 = $this->_request->getParam("day11");
			$day21 = $this->_request->getParam("day21");
			$day31 = $this->_request->getParam("day31");
			$day41 = $this->_request->getParam("day41");
			$day51 = $this->_request->getParam("day51");
			$day61 = $this->_request->getParam("day61");
			$day71 = $this->_request->getParam("day71");

			$day12 = $this->_request->getParam("day12");
			$day22 = $this->_request->getParam("day22");
			$day32 = $this->_request->getParam("day32");
			$day42 = $this->_request->getParam("day42");
			$day52 = $this->_request->getParam("day52");
			$day62 = $this->_request->getParam("day62");
			$day72 = $this->_request->getParam("day72");

			$day13 = $this->_request->getParam("day13");
			$day23 = $this->_request->getParam("day23");
			$day33 = $this->_request->getParam("day33");
			$day43 = $this->_request->getParam("day43");
			$day53 = $this->_request->getParam("day53");
			$day63 = $this->_request->getParam("day63");
			$day73 = $this->_request->getParam("day73");

			$batchday_add1 = $day11.",".$day21.",".$day31.",".$day41.",".$day51.",".$day61.",".$day71;
			$batchday_add2 = $day12.",".$day22.",".$day32.",".$day42.",".$day52.",".$day62.",".$day72;
			$batchday_add3 = $day13.",".$day23.",".$day33.",".$day43.",".$day53.",".$day63.",".$day73;

			if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
				if($hid11 == "1" && $hid12 == "" && $hid13 == ""){
					
					if($from_timing_add1 == "" || $to_timing_add1 == ""){

						$response["data"]["addmore"] = "allfield";
					}elseif($day11 =="" && $day21 =="" && $day31=="" && $day41 =="" && $day51=="" && $day61 =="" && $day71==""){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}elseif($hid11 == "" && $hid12 == "2" && $hid13 == ""){

					if($from_timing_add2 == "" || $to_timing_add2 == ""){

						$response["data"]["addmore"] = "allfield";
					}elseif($day12 =="" && $day22 =="" && $day32=="" && $day42 =="" && $day52=="" && $day62 =="" && $day72==""){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}elseif($hid11 == "" && $hid12 == "" && $hid13 == "3"){

					if($from_timing_add3 == "" || $to_timing_add3 == ""){

						$response["data"]["addmore"] = "allfield";
					}elseif($day13 =="" && $day23 =="" && $day33 =="" && $day43 =="" && $day53 =="" && $day63 =="" && $day73 ==""){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}elseif($hid11 == "1" && $hid12 == "2" && $hid13 == "3"){

					if($from_timing_add3 == "" || $to_timing_add3 == "" || $from_timing_add2 == "" || $to_timing_add2 == "" || $from_timing_add1 == "" || $to_timing_add1 == ""){

						$response["data"]["addmore"] = "allfield";
					}elseif(($day13 =="" && $day23 =="" && $day33 =="" && $day43 =="" && $day53 =="" && $day63 =="" && $day73 =="")
						|| ($day12 =="" && $day22 =="" && $day32=="" && $day42 =="" && $day52=="" && $day62 =="" && $day72=="" ) ||
						($day11 =="" && $day21 =="" && $day31=="" && $day41 =="" && $day51=="" && $day61 =="" && $day71=="")){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}elseif($hid11 == "1" && $hid12 == "2" && $hid13 == ""){

					if($from_timing_add2 == "" || $to_timing_add2 == "" || $from_timing_add1 == "" || $to_timing_add1 == ""){

						$response["data"]["addmore"] = "allfield";
					}elseif(($day12 =="" && $day22 =="" && $day32=="" && $day42 =="" && $day52=="" && $day62 =="" && $day72=="" ) ||
						($day11 =="" && $day21 =="" && $day31=="" && $day41 =="" && $day51=="" && $day61 =="" && $day71=="")){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}elseif($hid11 == "1" && $hid12 == "" && $hid13 == "3"){

					if($from_timing_add3 == "" || $to_timing_add3 == "" || $from_timing_add1 == "" || $to_timing_add1 == ""){

						$response["data"]["addmore"] = "allfield";
					}elseif(($day13 =="" && $day23 =="" && $day33 =="" && $day43 =="" && $day53 =="" && $day63 =="" && $day73 =="")
						|| ($day11 =="" && $day21 =="" && $day31=="" && $day41 =="" && $day51=="" && $day61 =="" && $day71=="")){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}elseif($hid11 == "" && $hid12 == "2" && $hid13 == "3"){

					if($from_timing_add3 == "" || $to_timing_add3 == "" || $from_timing_add2 == "" || $to_timing_add2 == "" ){

						$response["data"]["addmore"] = "allfield";
					}elseif(($day13 =="" && $day23 =="" && $day33 =="" && $day43 =="" && $day53 =="" && $day63 =="" && $day73 =="")
						|| ($day12 =="" && $day22 =="" && $day32=="" && $day42 =="" && $day52=="" && $day62 =="" && $day72=="" )){

						$response["data"]["addmore"] = "allfield";
					}else{
						$response["data"]["addmore"] = "valid";	
					}

				}else{
					$response["data"]["addmore"] = "valid";	
				}
				if($hiddenVal6 == "")
				{
					$response["data"]["hiddenVal6"] = "selectatleast";
					$response["data"]["hiddenVal5"] = "valid";
					$response["data"]["address0"] = "valid";
					$response["data"]["travel_radius"] = "valid";
				}
				if($hiddenVal6 == "1" && $hiddenVal6!="2")
				{

					$response["data"]["hiddenVal6"] = "valid";
					$response["data"]["address0"] = "valid";	
					if($locality_stu == "")
					{
						$response["data"]["hiddenVal5"] = "selectatleast";
						$response["data"]["travel_radius"] = "valid";
					}
					elseif($travel_radius == "Travel radius" || $travel_radius == "")
					{
						$response["data"]["travel_radius"] = "tavelradiusnull";
						$response["data"]["hiddenVal5"] = "valid";
					}
					
					else{
					$response["data"]["hiddenVal5"] = "valid";
					$response["data"]["travel_radius"] = "valid";

					}
				}
				
				if($hiddenVal6 != "1" && $hiddenVal6=="2")
				{
					
					$response["data"]["hiddenVal6"] = "valid";
					$response["data"]["hiddenVal5"] = "valid";
					$response["data"]["travel_radius"] = "valid";
					if($address == "" || $address == "Address" || $city[0]=="" || $locality_id[0]=="" || $pincode=="Pincode" || $pincode=="0" || !is_numeric($pincode) || $landmark =="" || $landmark =="landmark")
					{
						$response["data"]["address0"] = "allfield";		
					}
					else{
						$response["data"]["address0"] = "valid";	
					}
				}
	
				if($no_of_wks == "No. of weeks" || $no_of_days_per_wks == "	No. of days/wk" || $no_of_hrs_per_day == "No. of hours/day")
				{
					$response["data"]["no_of_wks"] = "durationnull";
				}
				else
				{
					$response["data"]["no_of_wks"] = "valid";
				}
				if($batch_size == "Batch size" || $batch_size == "")
				{
					$response["data"]["batch_size"] = "null";
				}
				elseif((!is_numeric($batch_size) || $batch_size <="0"))
				{
					$response["data"]["batch_size"] = "numberinvalid";
				}
				else
				{
					$response["data"]["batch_size"] = "valid";
				}
				if($batch_date == "Batch date" || $batch_date == "")
				{
					$response["data"]["batch_date"] = "null";
				}
				else
				{
					$response["data"]["batch_date"] = "valid";
				}
				if($tutor_batch_name=="")
				{
					$response["data"]["tutor_batch_name"] = "null";
				}
				else
				{
					$response["data"]["tutor_batch_name"] = "valid";
				}
				if($to_timing == "" || $from_timing == "" || $batchday == "")
				{
					$response["data"]["from_timing"] = "allfield";
				}
				else
				{
					$response["data"]["from_timing"] = "valid";
				}

				if($to_timing != "" && $from_timing != "" && $batchday != "")
				{
					$response["data"]["hiddenVal2"] = "valid";
					$response["data"]["from_timing"] = "valid";
					//$response["data"]["to_timing"] = "valid";
					//echo "from_type=".$from_type."and to=".$to_type;exit;
					if($from_type == $to_type)
					{
						//echo "1st";exit;
						$timeFirst  = strtotime($from);
						$timeSecond = strtotime($to);
						if(($from >= "12:00" && $from <= "12:59"))
						{
							if($timeSecond < $timeFirst)
							{
								//echo "string";exit;
								$response["data"]["to_timing"] = "valid";
							}
							
							
						}
						elseif($timeSecond > $timeFirst)
							{
								//echo "1st";exit;
								$response["data"]["to_timing"] = "valid";
							}
						else
							{
								$response["data"]["to_timing"] = "null";
							}
					}
					if($from_type != $to_type)
					{
						//echo "2nd";exit;
						$timeFirst  = strtotime($from);
						$timeSecond = strtotime($to);
						if(($timeSecond <= $timeFirst) && $to_type="pm" && $from_type="am")
							{
								$response["data"]["to_timing"] = "valid";
							}
						elseif(($timeSecond >= $timeFirst) && $to_type="am" && $from_type="pm")
							{
								$response["data"]["to_timing"] = "valid";
							}
						else
							{
								$response["data"]["to_timing"] = "null";
							}

					}
					
					
				}
				
				if(!in_array('selectatleast',$response['data']) 
					&& !in_array('durationnull',$response['data']) 
					&&!in_array('tavelradiusnull',$response['data']) 
					&& !in_array('allfield',$response['data'])
					&& !in_array('numberinvalid',$response['data']) 
					&& !in_array('null',$response['data'])  
					&&  !in_array('invalid',$response['data']))
					$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);
			}
			else {
			
			$curr_date=date("Y-m-d h:i:s");	
			if(isset($batchid) && $batchid!="" && $corseId!="" && $courseId=="" && $defaultcorseId==""){
					$data = array("tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date,"tutor_batch_from_timing1"=>$from_timing_add1,
								"tutor_batch_to_timing1"=>$to_timing_add1,"tutor_batch_from_timing2"=>$from_timing_add2,
								"tutor_batch_to_timing2"=>$to_timing_add2,"tutor_batch_from_timing3"=>$from_timing_add3,
								"tutor_batch_to_timing3"=>$to_timing_add3,"tutor_batch_day1"=>$batchday_add1,
								"tutor_batch_day2"=>$batchday_add2,"tutor_batch_day3"=>$batchday_add3);
					$batchObj->update($data,"id='$batchid'");
					echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/editcourse/courseid/".$corseId."';</script>";				
				}elseif(isset($batchid) && $batchid!="" && $courseId!="" && $corseId=="" && $defaultcorseId==""){
					//echo "courseId='".$courseId."'".$batchid;exit;
					$data = array("tutor_id"=>$tutor_id,"course_id"=>$courseId,"tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date,"tutor_batch_from_timing1"=>$from_timing_add1,
								"tutor_batch_to_timing1"=>$to_timing_add1,"tutor_batch_from_timing2"=>$from_timing_add2,
								"tutor_batch_to_timing2"=>$to_timing_add2,"tutor_batch_from_timing3"=>$from_timing_add3,
								"tutor_batch_to_timing3"=>$to_timing_add3,"tutor_batch_day1"=>$batchday_add1,
								"tutor_batch_day2"=>$batchday_add2,"tutor_batch_day3"=>$batchday_add3
								);
					$batchObj->insert($data);
					echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/editcourse/courseid/".$courseId."';</script>";
				}elseif($batchid=="" && $courseId!="" && $corseId=="" && $defaultcorseId==""){
					//echo "courseId='".$courseId."'";exit;
					$data = array("tutor_id"=>$tutor_id,"course_id"=>$courseId,"tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date,"tutor_batch_from_timing1"=>$from_timing_add1,
								"tutor_batch_to_timing1"=>$to_timing_add1,"tutor_batch_from_timing2"=>$from_timing_add2,
								"tutor_batch_to_timing2"=>$to_timing_add2,"tutor_batch_from_timing3"=>$from_timing_add3,
								"tutor_batch_to_timing3"=>$to_timing_add3,"tutor_batch_day1"=>$batchday_add1,
								"tutor_batch_day2"=>$batchday_add2,"tutor_batch_day3"=>$batchday_add3
								);
					$batchObj->insert($data);
					echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/editcourse/courseid/".$courseId."';</script>";
				}elseif(isset($defaultcorseId) && $defaultcorseId!="" && $batchid=="" && $courseId=="" && $corseId==""){
					//echo "defaultcorseId='".$defaultcorseId."'";exit;
					$data = array("tutor_id"=>$tutor_id,"course_id"=>$defaultcorseId,"tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date,"tutor_batch_from_timing1"=>$from_timing_add1,
								"tutor_batch_to_timing1"=>$to_timing_add1,"tutor_batch_from_timing2"=>$from_timing_add2,
								"tutor_batch_to_timing2"=>$to_timing_add2,"tutor_batch_from_timing3"=>$from_timing_add3,
								"tutor_batch_to_timing3"=>$to_timing_add3,"tutor_batch_day1"=>$batchday_add1,
								"tutor_batch_day2"=>$batchday_add2,"tutor_batch_day3"=>$batchday_add3
								);
					$batchObj->insert($data);
					echo "<script>parent.Mediabox.close();</script>";
					//echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/editcourse/defaultcorseid/".$defaultcorseId."';</script>";
				}else
				{
					//echo "else".$defaultcorseId;exit;
					$data = array("tutor_id"=>$tutor_id,"course_id"=>$defaultcorseId,"tutor_batch_name"=>$tutor_batch_name,"tutor_location"=>$hiddenVal6,
								"tutor_lesson_location"=>$locality_stu,"travel_radius"=>$travel_radius,
								"tutor_class_dur_wks"=>$no_of_wks,"tutor_class_dur_dayspwk"=>$no_of_days_per_wks,
								"tutor_class_dur_hrspday"=>$no_of_hrs_per_day,
								"tutor_batch_from_timing"=>$from_timing,"tutor_batch_to_timing"=>$to_timing,
								"batch_size"=>$batch_size,"seat_available"=>$batch_size,
								"batch_date"=>$batch_date,
								"address"=>$address,"city"=>$city[0],"locality"=>$locality_id[0],
								"pincode"=>$pincode,"landmark"=>$landmark,
								"tutor_batch_day"=>$batchday,
								"batch_summary_for_course"=>$batch_summary_display,
								"lastupdatedate"=>$curr_date,"tutor_batch_from_timing1"=>$from_timing_add1,
								"tutor_batch_to_timing1"=>$to_timing_add1,"tutor_batch_from_timing2"=>$from_timing_add2,
								"tutor_batch_to_timing2"=>$to_timing_add2,"tutor_batch_from_timing3"=>$from_timing_add3,
								"tutor_batch_to_timing3"=>$to_timing_add3,"tutor_batch_day1"=>$batchday_add1,
								"tutor_batch_day2"=>$batchday_add2,"tutor_batch_day3"=>$batchday_add3
								);
					$batchObj->insert($data);
					echo "<script>parent.Mediabox.close();</script>";
				}
				
				
			}
		}
	}
	public function editcourseAction(){
		//unset($authUserNamespace->defultcourseid);
		//echo $authUserNamespace->defultcourseid;exit;
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$selectId = '5';
			$this->view->selectid = $selectId;
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		    $tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		    $languageObj = new Skillzot_Model_DbTable_Language();
		    $languagueResult = $languageObj->fetchAll();
		    
		    $tutorid = $authUserNamespace->admintutorid;
			
			if (isset($tutorid) && $tutorid!="")
			{
				$this->view->tutorid = $tutorid;
				$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
				$batchdetails_data = $batchdetailObj->fetchRow("tutor_id ='$tutorid'");
				if (isset($batchdetails_data) && sizeof($batchdetails_data)>0)
				{
					$this->view->batch_t_id = $tutorid;
				}
			}
		    $courseId = $this->_request->getParam("courseid");
			$batchObj= new Skillzot_Model_DbTable_Batch();
			$batchResult=$batchObj->fetchAll($batchObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
									->where("c.course_id ='$courseId'"));
			$this->view->batchResult = $batchResult;
		    
			$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
			$lessonlocationResult = $lessonlocationObj->fetchAll();
			$this->view->lessonlocation = $lessonlocationResult;
	
			$batchsizeObj = new Skillzot_Model_DbTable_Tutorbatchsize();
			$batchsizeResult = $batchsizeObj->fetchAll();
			$this->view->batchsize = $batchsizeResult;
			
			$profileTypeObj = new Skillzot_Model_DbTable_Studentprofiletype();
			$profileTypeResult = $profileTypeObj->fetchAll();
			$this->view->profiletype = $profileTypeResult;
			
			$adressObj = new Skillzot_Model_DbTable_Address();
			$addressResult = $adressObj->fetchAll($adressObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."master_address"))
									   ->where("a.address_type_id='2' && address_id!='0'"));
			if (isset($addressResult) && sizeof($addressResult) > 0) $this->view->address = $addressResult;
			$cityResult = $adressObj->fetchAll($adressObj->select()
									->from(array('a'=>DATABASE_PREFIX."master_address"))
									->where("a.address_type_id='1' && a.address_id!='0'"));
			if (isset($cityResult) && sizeof($cityResult) > 0) $this->view->city = $cityResult;
	
			$defaultcorseId = $this->_request->getParam("defaultcorseid");
			
			$batchData1 = $batchdetailObj->fetchRow("tutor_id ='$tutorid'");
				if (isset($batchData1) && sizeof($batchData1) > 0){
					
				}else{
				$tutor_id = $tutorid;
				$color="#FFFFFF";
				$row='38';
				$colom ='7';
				$desc="";
				$endTime = '0';
				$lastupdatedate = date("Y-m-d H:i:s");
					for($i=1;$i<=$colom;$i++){
						for($j=1;$j<=$row;$j++){
						$data = array("tutor_id"=>$tutor_id,"b_description"=>$desc,"b_description1"=>$desc,"b_color"=>$color,"b_color1"=>$color,
						"b_day"=>$i,"b_start_timings"=>$j,"b_end_timings"=>$endTime,
						"lastupdatedate"=>$lastupdatedate);
						$batchdetailObj->insert($data);
							
						}
					}
				}
			
			
			if (isset($defaultcorseId) && $defaultcorseId!="")
			{
				$tempId = $defaultcorseId;
				$this->view->courseid = $defaultcorseId;
				$this->view->defaultcourseid = $defaultcorseId;
				$authUserNamespace->defultcourseid = $defaultcorseId ;
				
				$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
				$batchData = $batchdetailObj->fetchRow("tutor_id ='$tutorid' && tutor_course_id='0'");
				if (isset($batchData) && sizeof($batchData) > 0){
					
				}else{
				$tutor_id = $tutorid;
				$color="#FFFFFF";
				$row='38';
				$colom ='7';
				$desc="";
				$endTime = '0';
				$lastupdatedate = date("Y-m-d H:i:s");
					for($i=1;$i<=$colom;$i++){
						for($j=1;$j<=$row;$j++){
						$data = array("tutor_id"=>$tutor_id,"b_description"=>$desc,"b_description1"=>$desc,"b_color"=>$color,"b_color1"=>$color,
						"b_day"=>$i,"b_start_timings"=>$j,"b_end_timings"=>$endTime,
						"lastupdatedate"=>$lastupdatedate);
						$batchdetailObj->insert($data);
							
						}
					}
				}
			
				
			}else{
				unset($authUserNamespace->defultcourseid);
			}
			$courseId = $this->_request->getParam("courseid");
			if (isset($courseId) && $courseId!="")
			{
				$this->view->courseid = $courseId;
				$this->view->uniq_courseid = $courseId;
				$authUserNamespace->courseIdforsave = $courseId;
				$tempId = $courseId;
				
				$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
				$batchData = $batchdetailObj->fetchRow("tutor_id ='$tutorid' && tutor_course_id=$tempId");
				
				if (isset($batchData) && sizeof($batchData) > 0){
					
				}else{
				$tutor_id = $tutorid;
				$color="#FFFFFF";
				$row='38';
				$colom ='7';
				$desc="";
				$endTime = '0';
				$lastupdatedate = date("Y-m-d H:i:s");
					for($i=1;$i<=$colom;$i++){
						for($j=1;$j<=$row;$j++){
						$data = array("tutor_id"=>$tutor_id,"tutor_course_id"=>$tempId,"b_description"=>$desc,"b_description1"=>$desc,"b_color"=>$color,"b_color1"=>$color,
						"b_day"=>$i,"b_start_timings"=>$j,"b_end_timings"=>$endTime,
						"lastupdatedate"=>$lastupdatedate);
						$batchdetailObj->insert($data);
						}
					}
				}
				
			}
			//echo $tempId;exit;
			//----------------------------------------------------------------------------------------------------------------
			
			//if (isset($authUserNamespace->userclasstype) &&  ($authUserNamespace->userclasstype=="4" || $authUserNamespace->userclasstype=="3")){
				
				if (isset($tempId) && $tempId!=""){
				
				if (isset($defaultcorseId) && $defaultcorseId!="")//for add display only one
				{
					$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
					$companyaddressData = $branchdetailObj->fetchAll($branchdetailObj->select()
														->from(array('c'=>DATABASE_PREFIX."branch_details"))
														//->where("c.tutor_id='$tutorid'")
														->where("c.tutor_id='$tutorid' && c.course_id='$tempId'")
														->limit(10,0)
														);
					if (isset($companyaddressData) && sizeof($companyaddressData)>0)
					{
						$this->view->companyaddressdata = $companyaddressData;
					}
					foreach ($companyaddressData as $addressID)
					{
						$addressIDarray[] =  $addressID->id;
					}
					$authUserNamespace->avgaddvar=0;
				}else{//for edu display all
					//echo $tempId;exit;
					$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
					$companyaddressData = $branchdetailObj->fetchAll($branchdetailObj->select()
														->from(array('c'=>DATABASE_PREFIX."branch_details"))
														->where("c.tutor_id='$tutorid' && c.course_id='$tempId'"));
													//	->where("c.tutor_id='$tutorid'"));
					if (isset($companyaddressData) && sizeof($companyaddressData)>0)
					{
						$this->view->companyaddressdata = $companyaddressData;
					}
					foreach ($companyaddressData as $addressID)
					{
						$addressIDarray[] =  $addressID->id;
					}
					$authUserNamespace->avgaddvar=sizeof($companyaddressData)-1;
				}
				}
		//	}
			//----------------------------------------------------------------------------------------------------------------
		    if (isset($languagueResult) && sizeof($languagueResult)>0)
		    {
		    	$this->view->language = $languagueResult;
		    }
		    $feestypeObj = new Skillzot_Model_DbTable_Tutorfeetype();
			$feestypeResult = $feestypeObj->fetchAll();
			if (isset($feestypeResult) && sizeof($feestypeResult)>0)
		    {
				$this->view->feestype = $feestypeResult;
		    }
			$paymentcycleObj = new Skillzot_Model_DbTable_Tutorpaycycle();
			$paymentcycleResult = $paymentcycleObj->fetchAll();
			if (isset($paymentcycleResult) && sizeof($paymentcycleResult)>0)
		    {
				$this->view->paycycle = $paymentcycleResult;
		    }
			
			$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
			$lessonlocationResult = $lessonlocationObj->fetchAll();
			if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
			{
				$this->view->lessonlocation = $lessonlocationResult;
			}
		
		   
		    if (isset($courseId) && $courseId!="" )
		    {
		    	$tutorcourseData = $tutorCourseobj->fetchRow("id ='$courseId'");
				$this->view->tutorcoursedata = $tutorcourseData;
		    }
		    if(isset($defaultcorseId) && $defaultcorseId!="")
		    {
		    	$tutorcourseData = $tutorCourseobj->fetchRow("id ='$defaultcorseId'");
				$this->view->tutorcoursedata = $tutorcourseData;
		    }
		    $timingsObj = new Skillzot_Model_DbTable_Timings();
			$timingsResult = $timingsObj->fetchAll();
			if (isset($timingsResult) && sizeof($timingsResult)>0)
			{
				$this->view->timings= $timingsResult;
			}
			$tutorcourseCount = $tutorCourseobj->fetchRow($tutorCourseobj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('MAX(c.id) as count')));
			$maxcourseId=$tutorcourseCount['count']+1;
			$authUserNamespace->maxcourseId=$maxcourseId;
	if($this->_request->isPost()){
			
		$courseName = $this->_request->getParam("coursename");
		$aboutCourse = $this->_request->getParam("OverviewDescription");
		$studentProfile = $this->_request->getParam("hiddenVal5");
		$studentComments =  $this->_request->getParam("student_comment");
		if ($studentComments=="Suitable for...")$studentComments="";
		$ageGroup = $this->_request->getParam("agegroup");
		$format = $this->_request->getParam("format");
		
		$address = $this->_request->getParam("address");
		$city = $this->_request->getParam("city");
		
		$locality = $this->_request->getParam("locality");
		$pincode = $this->_request->getParam("pincode");
		$landmark = $this->_request->getParam("landmark");
		
		$numberofBranch = $this->_request->getParam("number_of_branches");
		$numberofBranch=0;
		$courseCertis = $this->_request->getParam("coursecertis");
		$certiGranted = $this->_request->getParam("certisgranted");
		$numberofWeeks = $this->_request->getParam("no_of_wks");
		$numberofDays = $this->_request->getParam("no_of_days_per_wks");
		$numberofHors = $this->_request->getParam("no_of_hrs_per_day");
		
		$daysofWeek = $this->_request->getParam("days_of_weeks");
		//$timings = $this->_request->getParam("timings");
		$timeFrom = $this->_request->getParam("time_from");
		$timeTo = $this->_request->getParam("time_to");
		$batchShedule = $this->_request->getParam("batch_schedule");
		$batchSize = $this->_request->getParam("batch_size");
		$lessionLocation = $this->_request->getParam("LessionLocationhiddenvalue");
		$travelRadius = $this->_request->getParam("travel_radius");
		//if($travelRadius == "Locations where you travel to teach"){$travelRadius="";}
		$language = $this->_request->getParam("hiddenVal4");
		
		$payfeetype = $this->_request->getParam("feestype");
		$paymentcycle = $this->_request->getParam("paymentcycle");
		$canPolicy = $this->_request->getParam("cancel_policy");
		$rate =  $this->_request->getParam("rate");
		$rateComments =  $this->_request->getParam("rate_comment");
		
		$address = $this->_request->getParam("address");
		$contactNumber = $this->_request->getParam("contact");
		$batchTiming = "";
		//$b_Timings_id = $this->_request->getParam("batch_type_id");
		$b_Tutor_status = $this->_request->getParam("tutor_batch_status");
		$age = explode(" ",$ageGroup);
			$min1="0";
			$max1="99";	
			$flag="0";
			for ($start=0; $start < count($age); $start++) {
					if(($age[$start]=="above" || $age[$start]=="onwards" || $age[$start]=="+" || $age[$start]=="more" || $age[$start]=="greater" || $age[$start]=="above.") && $flag=="0")
						{
							for ($start1=0; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
										$min1=$age[$start1];
										$max1="99";
										$flag="1";
								}
							}
						}	
			}
			for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="below" || $age[$start]=="less" || $age[$start]=="below.") && $flag=="0")
						{
							for ($start1=0; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
										$min1="0";
										$max1=$age[$start1];
										$flag="1";
								}
							}
						}			
			}
			for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="to" || $age[$start]=="-" || $age[$start]=="TO") && $flag=="0")
						{
							$end=$start;
							for ($start1=0; $start1 < $end; $start1++)
							{
								if(is_numeric($age[$start1]))
								{
									$min1=$age[$start1];
									$flag="1";
								}
							}
							for ($start1=$end; $start1 < count($age); $start1++)
							{
								if(is_numeric($age[$start1]))
								{
									$max1=$age[$start1];
									$flag="1";
								}
							}
						}			
			}
			for ($start=0; $start < count($age); $start++) {
						if(($age[$start]=="any" || $age[$start]=="all" || $age[$start]=="anyone") && $flag=="0")
						{
							$min1="0";
							$max1="99";		
							$flag="1";
						}			
			}

		$age2 = explode("+",$ageGroup);               //33+
		if(is_numeric($age2[0]) && $flag=="0")
		{
				$min1=$age2[0];
				$max1="99";
				$flag="1";
		}
		$age2 = explode("-",$ageGroup);                //22-66 
		if(is_numeric($age2[0]) && is_numeric($age2[1]) && $flag=="0")
		{
				$min1=$age2[0];
				$max1=$age2[1];
				$flag="1";
		}
		if(is_numeric($age2[0]) && !is_numeric($age2[1]) && $flag=="0")      //22-66 years
		{
				$min1=$age2[0];
				$agespace = explode(" ",$age2[1]);
				if(is_numeric($agespace[0]))
				{ $max1=$agespace[0];} 
				$flag="1";
		} 
		if($this->_request->isXmlHttpRequest()){
			
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response=array();
				
				if($payfeetype == "" )$response["data"]["feestype"] = "ratenull";
				else $response["data"]["feestype"] = "valid";
				
				if($paymentcycle == "select")$response["data"]["paymentcycle"] = "paymentnull";
				else $response["data"]["paymentcycle"] = "valid";
				
				for($i=0;$i<sizeof($address);$i++)
				{
					if($address[$i] == "" || $address[$i] == "Address" || $city[$i]=="" || $locality[$i]=="")$response["data"]["address".$i] = "allfield";
					else if ($pincode[$i]!="Pincode" && $pincode[$i]!="0" && !is_numeric($pincode[$i]))$response["data"]["address".$i] = "pincodeinvalid";
					else $response["data"]["address".$i] = "valid";
				}
				
				if (isset($lessionLocation) && ($lessionLocation=="1" || $lessionLocation=="3"))
				{
					if($travelRadius == "Locations where you travel to teach")$response["data"]["travel_radius"] = "tavelradiusnull";
					else $response["data"]["travel_radius"] = "valid";
				}
				
				if($language == "")$response["data"]["hiddenVal4"] = "selectlanguage";
				else $response["data"]["hiddenVal4"] = "valid";
				
				if($batchShedule == "Start date" )$response["data"]["batch_schedule"] = "shedulenull";
				else $response["data"]["batch_schedule"] = "valid";
				
//					if($daysofWeek == "Days of the weeks" || $timeFrom=="" || $timeTo=="")$response["data"]["days_of_weeks"] = "daysofweeknull";
//					elseif(is_numeric($daysofWeek))$response["data"]["days_of_weeks"] = "invalid";
//					else $response["data"]["days_of_weeks"] = "valid";
				
				if($numberofWeeks == "No. of weeks" || $numberofDays == "No. of days/wk" || $numberofHors == "No. of hours/day")$response["data"]["no_of_wks"] = "durationnull";
				//elseif((!is_numeric($numberofWeeks) || $numberofWeeks=="0")|| (!is_numeric($numberofDays) || $numberofDays=="0") ||(!is_numeric($numberofHors) || $numberofHors=="0") )$response["data"]["no_of_wks"] = "durationnumeric";
				else $response["data"]["no_of_wks"] = "valid";
				
				if($format == "" )$response["data"]["format"] = "formatnull";
				else $response["data"]["format"] = "valid";
				
				if($ageGroup == "")$response["data"]["agegroup"] = "agegroupnull";
				else $response["data"]["agegroup"] = "valid";
				
				if($studentProfile == "" )$response["data"]["hiddenVal5"] = "onlyselectnull";
				else $response["data"]["hiddenVal5"] = "valid";
				
				$aboutCourse = trim(strip_tags($aboutCourse),"'");
				if($aboutCourse == "")$response["data"]["OverviewDescription"] = "descriptionnull";
				else $response["data"]["OverviewDescription"] = "valid";
				
				if($courseName == "" )$response["data"]["coursename"] = "coursenamenull";
				else $response["data"]["coursename"] = "valid";
				
					if($rate == "Fees")$response["data"]["rate"] = "null";
					elseif(!is_numeric($rate) || $rate=="0")$response["data"]["rate"] = "invalid";
					else $response["data"]["rate"] = "valid";
				
				

				//-----------------------------------------------------------------------
				if(!in_array('onlyenternull',$response['data']) &&
				!in_array('selectlanguage',$response['data']) &&
				!in_array('allfield',$response['data']) &&
				!in_array('tavelradiusnull',$response['data']) &&
				!in_array('selectagegroup',$response['data']) &&
				!in_array('ratenull',$response['data']) &&
				!in_array('paymentnull',$response['data']) &&
				!in_array('selectskill',$response['data']) &&
				!in_array('coursenamenull',$response['data']) &&
				!in_array('shedulenull',$response['data']) &&
				!in_array('agegroupnull',$response['data']) &&
				!in_array('formatnull',$response['data']) &&
				!in_array('daysofweeknull',$response['data']) &&
				!in_array('descriptionnull',$response['data']) &&
				!in_array('onlyselectnull',$response['data']) && !in_array('selectatleast',$response['data']) && !in_array('durationnull',$response['data']) &&
				!in_array('durationnumeric',$response['data']) && !in_array('null',$response['data']) && !in_array('uncheck',$response['data']) &&  !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				echo json_encode($response);
				
			}
			else {
				
					$lastupdatedate = date("Y-m-d H:i:s");
					//print_r($authUserNamespace->uploaded_image_edit_name1);exit;
					if (isset($authUserNamespace->uploaded_image_edit_name1) && $authUserNamespace->uploaded_image_edit_name1!="")
					{
						$image_typearr = explode(".",$authUserNamespace->uploaded_image_edit_name1);
						$image_type = "image/".$image_typearr[1];
						$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name1);
					}else{
						//$image_type = "";
						//$getCropimagecontent="";
					}
					if($travelRadius == "Locations where you travel to teach")
					{	
						$travelRadius="";
					}
					$tutorid = $authUserNamespace->admintutorid;
					$coursedata = $tutorCourseobj->fetchRow("tutor_id ='$tutorid'");
					if (isset($coursedata) && sizeof($coursedata)>0){
						$skillIds = $coursedata->tutor_skill_id;
						$gradeTeach = $coursedata->tutor_grade_to_teach;
					}
					
				if (isset($authUserNamespace->uploaded_image_edit_name1) && $authUserNamespace->uploaded_image_edit_name1!="")
					{
						
					
					$data = array("tutor_skill_id"=>$skillIds,"tutor_grade_to_teach"=>$gradeTeach,"tutor_id"=>$tutorid,"tutor_course_name"=>$courseName,"tutor_course_desc"=>$aboutCourse,
								"tutor_student_profile"=>$studentProfile,"tutor_student_comments"=>$studentComments,"student_age_group"=>$ageGroup,"tutor_format"=>$format,
								"tutor_certificate_granted"=>$certiGranted,
								"tutor_class_dur_wks"=>$numberofWeeks,"tutor_class_dur_dayspwk"=>$numberofDays,
								"tutor_class_dur_hrspday"=>$numberofHors,"tutor_class_timing"=>$batchTiming,
								"tutor_batch_week"=>$daysofWeek,"tutor_batch_from_timing"=>$timeFrom,"tutor_batch_to_timing"=>$timeTo,
								"batch_size"=>$batchSize,
								"tutor_batch_status"=>$b_Tutor_status,
								"tutor_class_batch_schedule"=>$batchShedule,"tutor_lesson_location"=>$lessionLocation,"travel_radius"=>$travelRadius,
								"tutor_class_image_type"=>$image_type,"tutor_class_image_content"=>$getCropimagecontent,
								"tutor_lesson_language"=>$language,"tutor_pay_feetype"=>$payfeetype,
								"tutor_rate"=>$rate,"rate_comments"=>$rateComments,"tutor_pay_cycle"=>$paymentcycle,
								"tutor_canc_policy"=>$canPolicy,"lastupdatedate"=>$lastupdatedate,"min"=>$min1,"max"=>$max1);

					}else{
						//echo "else";exit;
						$data = array("tutor_skill_id"=>$skillIds,"tutor_grade_to_teach"=>$gradeTeach,"tutor_id"=>$tutorid,"tutor_course_name"=>$courseName,"tutor_course_desc"=>$aboutCourse,
								"tutor_student_profile"=>$studentProfile,"tutor_student_comments"=>$studentComments,"student_age_group"=>$ageGroup,"tutor_format"=>$format,
								"tutor_certificate_granted"=>$certiGranted,
								"tutor_class_dur_wks"=>$numberofWeeks,"tutor_class_dur_dayspwk"=>$numberofDays,
								"tutor_class_dur_hrspday"=>$numberofHors,"tutor_class_timing"=>$batchTiming,
								"tutor_batch_week"=>$daysofWeek,"tutor_batch_from_timing"=>$timeFrom,"tutor_batch_to_timing"=>$timeTo,
								"batch_size"=>$batchSize,
								"tutor_batch_status"=>$b_Tutor_status,
								"tutor_class_batch_schedule"=>$batchShedule,"tutor_lesson_location"=>$lessionLocation,"travel_radius"=>$travelRadius,
								"tutor_lesson_language"=>$language,"tutor_pay_feetype"=>$payfeetype,
								"tutor_rate"=>$rate,"rate_comments"=>$rateComments,"tutor_pay_cycle"=>$paymentcycle,
								"tutor_canc_policy"=>$canPolicy,"lastupdatedate"=>$lastupdatedate,"min"=>$min1,"max"=>$max1);
					}
					if (isset($courseId) && $courseId!="")
					{
						
						$tutorCourseobj->update($data,"id='$courseId'");
//							if (isset($authUserNamespace->userclasstype) && ( $authUserNamespace->userclasstype=="4" ||$authUserNamespace->userclasstype=="3"))
//							{

							$lenAddress = sizeof($address);
							$insertremianAddress = $lenAddress-$authUserNamespace->avgaddvar;
							$prevsessionAddress = $authUserNamespace->avgaddvar;
							
							for($m=0;$m<=$prevsessionAddress;$m++)
							{
								$data = array("address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
													"pincode"=>$pincode[$m],"landmark"=>$landmark[$m],"lastupdatedate"=>$lastupdatedate);
								$branchdetailObj->update($data,"id='$addressIDarray[$m]'");
							
							}
							for($m=($prevsessionAddress+1);$m<$lenAddress;$m++)
							{
								$data = array("tutor_id"=>$authUserNamespace->admintutorid,"course_id"=>$tempId,
													"address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
													"pincode"=>$pincode[$m],"landmark"=>$landmark[$m],"lastupdatedate"=>$lastupdatedate);
								$branchdetailObj->insert($data);
							}
							//echo "image";exit;	
						//}
					}else{
						
						$getCoursecontent = $tutorCourseobj->fetchRow("tutor_id ='$authUserNamespace->admintutorid'");
						$sizeofContent = sizeof($getCoursecontent);
						//echo $getCoursecontent->tutor_skill_id;
						if (isset($getCoursecontent) && $sizeofContent=='1' && $getCoursecontent->tutor_course_name=="" && $getCoursecontent->tutor_course_desc=="")
						{
							$tutorCourseobj->update($data,"tutor_id='$authUserNamespace->admintutorid'");
							$tutorcoursetmpId = $getCoursecontent->tutor_skill_id;
						}else{
							
						if($defaultcorseId!="" || $defaultcorseId=="")
							{
							
								$data1 = array("id"=>$authUserNamespace->maxcourseId,"tutor_skill_id"=>$skillIds,"tutor_grade_to_teach"=>$gradeTeach,"tutor_id"=>$tutorid,"tutor_course_name"=>$courseName,"tutor_course_desc"=>$aboutCourse,
									"tutor_student_profile"=>$studentProfile,"tutor_student_comments"=>$studentComments,"student_age_group"=>$ageGroup,"tutor_format"=>$format,
									"tutor_certificate_granted"=>$certiGranted,
									"tutor_class_dur_wks"=>$numberofWeeks,"tutor_class_dur_dayspwk"=>$numberofDays,
									"tutor_class_dur_hrspday"=>$numberofHors,"tutor_class_timing"=>$batchTiming,
									"tutor_batch_week"=>$daysofWeek,"tutor_batch_from_timing"=>$timeFrom,"tutor_batch_to_timing"=>$timeTo,
									"batch_size"=>$batchSize,
									"tutor_batch_status"=>$b_Tutor_status,
									"tutor_class_batch_schedule"=>$batchShedule,"tutor_lesson_location"=>$lessionLocation,"travel_radius"=>$travelRadius,
									"tutor_lesson_language"=>$language,"tutor_pay_feetype"=>$payfeetype,
									"tutor_rate"=>$rate,"rate_comments"=>$rateComments,"tutor_pay_cycle"=>$paymentcycle,
									"tutor_canc_policy"=>$canPolicy,"lastupdatedate"=>$lastupdatedate);
								$tutorCourseobj->insert($data1);
								unset($authUserNamespace->maxcourseId);
							}
						$tutorcoursetmpId = $tutorCourseobj->getAdapter()->lastInsertId("tutor_course_name='$courseName'");
							if (isset($tutorcoursetmpId) && $tutorcoursetmpId!='')
							{
								$branchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
								$data2 = array("tutor_course_id"=>$tutorcoursetmpId);
								$branchdetailObj->update($data2,"tutor_id='$authUserNamespace->admintutorid' && tutor_course_id='0'");
							}
						}
						//---------------insertion of branches-----------------
//							if (isset($authUserNamespace->userclasstype) && ($authUserNamespace->userclasstype=="4"||$authUserNamespace->userclasstype=="3"))
//							{
							
							$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
							for($m=0;$m<sizeof($address);$m++)
							{
									$data = array("tutor_id"=>$authUserNamespace->admintutorid,"course_id"=>$tutorcoursetmpId,
													"address"=>$address[$m],"city"=>$city[$m],"locality"=>$locality[$m],
													"pincode"=>$pincode[$m],"landmark"=>$landmark[$m],"lastupdatedate"=>$lastupdatedate);
									$branchdetailObj->insert($data);
							}
						//}
						//---------------end of insertion of branches-----------------
						
					}
					$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
					//echo "h";exit;
					array_map( "unlink", glob( dirname(dirname(dirname(__FILE__)))."/docs/*.jpg" ) );
					array_map( "unlink", glob( dirname(dirname(dirname(__FILE__)))."/docs/*.jpeg" ) );
					array_map( "unlink", glob( dirname(dirname(dirname(__FILE__)))."/docs/*.png" ) );
					unset($authUserNamespace->uploaded_image_edit_name1);
					unset($authUserNamespace->uploaded_image_edit_name);
					$authUserNamespace->changesave = "Your changes have been saved";
					$this->_redirect("/editprofilenew/coursedetail");
			}
	}
				
	}

	public function emailpassAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$selectId = '6';
			$this->view->selectid = $selectId;
			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		    if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
		    {
		    	$tutorData = $tutorProfileobj->fetchRow("id ='$authUserNamespace->admintutorid'");
				$this->view->tutordata = $tutorData;
		    }
			if($this->_request->isPost()){
						
						$emailtxt = $this->_request->getParam("email");
						if($this->_request->isXmlHttpRequest()){
						
							$this->_helper->layout()->disableLayout();
							$this->_helper->viewRenderer->setNoRender(true);
							$response=array();
			
							if($emailtxt == "")$response["data"]["email"] = "null";
							elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $emailtxt)) $response['data']['email'] = "emailinvalid";
							else $response["data"]["email"] = "valid";
			
							/*fo update time email not duplicate so if condition*/
							$tutorProfile_row = $tutorProfileobj->fetchRow($tutorProfileobj->select()
															 ->where("tutor_email='$emailtxt' && id!='$authUserNamespace->admintutorid'"));
							if(isset($tutorProfile_row) && sizeof($tutorProfile_row) > 0)
							{
								$response['data']['email'] = "emailduplicate";
							}
							$studentsignup_row = $studentSignupObj->fetchRow($studentSignupObj->select()
														 	 	  ->where("std_email='$emailtxt'"));
							if(isset($studentsignup_row) && sizeof($studentsignup_row) > 0)
							{
								$response['data']['email'] = "emailduplicate";
							}
											
							if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
							&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
							$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
							else $response['returnvalue'] = "validation";
							echo json_encode($response);
						
						}
						else {
							$data = array("tutor_email"=>$emailtxt);
							$tutorProfileobj->update($data,"id='$authUserNamespace->admintutorid'");
							
							$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
							$profile_rows = $tutorProfile->fetchAll($tutorProfile->select()
													->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.tutor_first_name','s.tutor_last_name','s.tutor_email','s.tutor_pwd'))
													->where("s.id='$authUserNamespace->admintutorid'"));
							if (isset($profile_rows) && sizeof($profile_rows) > 0)
							{
								$tutorFullname = ucfirst($profile_rows[0]['tutor_first_name'])." ".ucfirst($profile_rows[0]['tutor_last_name']);
								$tutorMail = $profile_rows[0]['tutor_email'];
								$tutorPass = $profile_rows[0]['tutor_pwd'];
							}
							$message2 = "<div style='text-align:justify;'>";
							$message2 .= "<div>Hi ".$tutorFullname."...<br/></div><br/>";
							$message2 .= "<div>We're just dropping a line to confirm that you've changed your email id and your new log in details will be as follows - <br/></div><br/>";
							$message2 .= "<div>Username- ".$tutorMail."<br/></div><br/>";
							$message2 .= "<div>Password- ".$tutorPass."<br/></div><br/>";
							$message2 .= "<div>Do feel free to get in touch with us in case of any clarifications please.<br/></div><br/>";
							$message2 .= "<div>Cheers,<br/></div>";
							$message2 .= "<div>Support Team @ Skillzot<br/></div><br/>";
							$message2 .= "<div>(We care, more...)</div>";
							$message2 .= "</div>";
						
							$message = $message2;
							//print_r($message2);exit;
							$to = $emailtxt;
							$from = "sz.contact@skillzot.com"; //done
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,"Skillzot Support Team");
							$mail->addTo($to,$tutorFullname);
							$mail->setSubject("Your email id has been changed");//need to ask
							$mail->send();
							
							$authUserNamespace->changesave = "Your changes have been saved";
							$this->_redirect("/editprofilenew/emailpass");
						}
					}
		}
	public function changemailAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(true);
		}
	public function changepassAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(true);
			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			
			if($this->_request->isPost()) {
			
			$current_password = $this->_request->getParam('CurrentPassword');
			$new_password = $this->_request->getParam('NewPassword');
			$confirm_password = $this->_request->getParam('ConfirmPassword');
			$tutor_id = $authUserNamespace->admintutorid;
			$response = array();
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				$password_row = $tutorProfileobj->fetchRow("tutor_pwd='$current_password' and id='$tutor_id'");
				
				if($current_password=="")$response["data"]["CurrentPassword"] = "null";
				elseif(!sizeof($password_row) > 0)$response["data"]["CurrentPassword"] = "wrongpass";
				//elseif(strlen($current_password)<5)$response["data"]["CurrentPassword"] = "passlength";
				else $response["data"]["CurrentPassword"] = "valid";
				
				if($new_password == "")$response["data"]["NewPassword"] = "null";
				elseif(strlen($new_password)<5)$response["data"]["NewPassword"] = "passlength";
				else $response["data"]["NewPassword"] = "valid";
				
				if($confirm_password == "")$response["data"]["ConfirmPassword"] = "conformpassnull";
	      		elseif($confirm_password != $new_password)$response["data"]["ConfirmPassword"] = "passnotmatch";
				else $response["data"]["ConfirmPassword"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('wrongpass',$response['data']) &&  !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
				&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
				$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
		
				echo json_encode($response);
		
				}else{
					//$Current_pass_md5 = md5($current_password);
					//$insert_new_pass = md5($new_password);
					$data = array("tutor_pwd"=>$new_password);
					$tutorProfileobj->update($data,"id='$tutor_id'");
						$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
						$profile_rows = $tutorProfile->fetchAll($tutorProfile->select()
												->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.tutor_first_name','s.tutor_last_name','s.tutor_email','s.tutor_pwd'))
												->where("s.id='$tutor_id'"));
						if (isset($profile_rows) && sizeof($profile_rows) > 0)
						{
							$tutorFullname = ucfirst($profile_rows[0]['tutor_first_name'])." ".ucfirst($profile_rows[0]['tutor_last_name']);
							$tutorMail = $profile_rows[0]['tutor_email'];
							$tutorPass = $profile_rows[0]['tutor_pwd'];
						}
						$message2 = "<div style='text-align:justify;'>";
						$message2 .= "<div>Hi ".$tutorFullname."...<br/></div><br/>";
						$message2 .= "<div>We're just dropping a line to confirm that you've changed your password and your new log in details will be as follows - <br/></div><br/>";
						$message2 .= "<div>Username- ".$tutorMail."<br/></div><br/>";
						$message2 .= "<div>Password- ".$tutorPass."<br/></div><br/>";
						$message2 .= "<div>Do feel free to get in touch with us in case of any clarifications please.<br/></div><br/>";
						$message2 .= "<div>Cheers,<br/></div>";
						$message2 .= "<div>Support Team @ Skillzot<br/></div><br/>";
						$message2 .= "<div>(We care, more...)</div>";
						$message2 .= "</div>";
					
						$message = $message2;
						//print_r($message2);exit;
						$to = $tutorMail;
						$from = "sz.contact@skillzot.com"; //done
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,"Skillzot Support Team");
						$mail->addTo($to,$tutorFullname);
						$mail->setSubject("Your password has been changed");//need to ask
						$mail->send();
					
					//$authUserNamespace->status_message = "Password updated successfully.";
					$authUserNamespace->changesave = "Your changes have been saved";
					$this->_redirect("/editprofilenew/emailpass");
					
				}
			}
	}
	public function deletecourseAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(true);
		    $tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		    $branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
		    $batchObj= new Skillzot_Model_DbTable_Batch();
			$courseId = $this->_request->getParam('cid');
			if (isset($courseId) && $courseId!="")
			{
				$branchdetailObj->delete("course_id='$courseId'");
				$tutorCourseobj->delete("id='$courseId'");
				$batchObj->delete("course_id='$courseId'");
			}
			$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
			$authUserNamespace->changesave = "Your changes have been saved";
			$this->_redirect("/editprofilenew/coursedetail");
	}
	public function deleteeducationAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(true);
		    $tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
			$educationId = $this->_request->getParam('cid');
			if (isset($educationId) && $educationId!="")
			{
				$tutorexperienceObj->delete("id='$educationId'");
			}
			$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
			$authUserNamespace->changesave = "Your changes have been saved";
			$this->_redirect("/editprofilenew/experience");
	}
public function inboxmailAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
	$records_per_page = $this->_request->getParam('shown');
	if (isset($records_per_page) && $records_per_page!=""){
		$this->view->records_per_page = $records_per_page;
	}
	$tutormailResult =$mailMessagesObj->fetchAll($mailMessagesObj->select()
						->from(array('m'=>DATABASE_PREFIX."message"))
						->where("m.to_id='$authUserNamespace->admintutorid' && m.rec_flag='t' && m.inbox_delete_flag='0'")
						->order(array("id desc")));
	if (isset($tutormailResult) && sizeof($tutormailResult)>0)
	{
		$this->view->allmailresult = $tutormailResult;
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutormailResult);
		$paginator = Zend_Paginator::factory($tutormailResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->allmailresult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
	}
	//print_r($tutormailResult);exit;
}
public function sentmailAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
	$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
	$tutormailResult =$mailMessagesObj->fetchAll($mailMessagesObj->select()
						->from(array('m'=>DATABASE_PREFIX."message"))
						->where("m.from_id='$authUserNamespace->admintutorid' && m.send_flag='t' && m.sent_delete_flag='0'")
						->order(array("id desc")));
//							print $mailMessagesObj->select()
//							->from(array('m'=>DATABASE_PREFIX."message"))
//							->where("m.from_id='$authUserNamespace->admintutorid' && m.send_flag='t' && m.sent_delete_flag='0'")
//							->order(array("id desc"));exit;
	if (isset($tutormailResult) && sizeof($tutormailResult)>0)
		{
			$this->view->allmailresult = $tutormailResult;
			
			$page = $this->_request->getParam('page',1);
			//$this->view->page = $page;
			if($records_per_page=="")$records_per_page = 10;
			$record_count = sizeof($tutormailResult);
			$paginator = Zend_Paginator::factory($tutormailResult);
			$paginator->setItemCountPerPage($records_per_page);
			$paginator->setCurrentPageNumber($page);
			$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
			$this->view->allmailresult = $paginator;
			$page_number  = $record_count / 1;
			$page_number_last =  floor($page_number);
		}
	
	
	
}
public function readsentmailAction(){
$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
	$mailId = $this->_request->getParam('mail_id');
	if (isset($mailId) && $mailId!="")
	{
		$mailrowResult = $mailMessagesObj->fetchRow("id='$mailId'");
		$this->view->readsentmailresult = $mailrowResult;
	}
	
	
}
public function readmailAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
	$mailId = $this->_request->getParam('mail_id');
	if (isset($mailId) && $mailId!="")
	{
		$mailrowResult = $mailMessagesObj->fetchRow("id='$mailId'");
		$this->view->readmailresult = $mailrowResult;
	}
	if($this->_request->isPost()) {
			
			$message_content = $this->_request->getParam('messagetext');
			$response = array();
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				
				if($message_content == "")$response["data"]["messagetext"] = "null";
				else $response["data"]["messagetext"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']) && !in_array('mobileinvalid',$response['data'])
				&& !in_array('selectnull',$response['data']) && !in_array('mobilelength',$response['data']) && !in_array('onlyenternull',$response['data']) && !in_array('pincodeinvalid',$response['data']) && !in_array('passnotmatch',
				$response['data']) && !in_array('conformpassnull',$response['data']) && !in_array('passlength',$response['data']) && !in_array('emailduplicate',$response['data']) && !in_array('emailinvalid',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
		
				echo json_encode($response);
		
				}else{
					$lastupdatedate = date("Y-m-d H:i:s");
					$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
					$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
					$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
					if (isset($mailId) && $mailId!="")
					{
						$mailrowResult = $mailMessagesObj->fetchRow("id='$mailId'");
						$subject_name = $mailrowResult->subject;
						if (isset($authUserNamespace->logintype) && $authUserNamespace->logintype=='1')
						{
							if($mailrowResult->send_flag=='t'){
								$tutor_data = $tutorProfileobj->fetchRow("id='$mailrowResult->from_id'");
								$to_id = $tutor_data->id;
								$data1 = array("from_id"=>$authUserNamespace->admintutorid,"to_id"=>$to_id,"parent_id"=>$mailId,
								"send_flag"=>"t","rec_flag"=>"t","mail_content"=>$message_content,"subject"=>$subject_name,"mail_date"=>$lastupdatedate
								);
							}else{
								$student_data = $studentSignupObj->fetchRow("id='$mailrowResult->from_id'");
								$to_id = $student_data->id;
								$data1 = array("from_id"=>$authUserNamespace->admintutorid,"to_id"=>$to_id,"parent_id"=>"$mailId",
								"send_flag"=>"t","rec_flag"=>"s","mail_content"=>$message_content,"subject"=>$subject_name,"mail_date"=>$lastupdatedate
								);
							}
						}
						
							if (isset($data1) && sizeof($data1) > 0)
							{
								if($mailrowResult->rec_flag=='t'){
									$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
									$fetch_data = $tutorProfileobj->fetchRow("id='$mailrowResult->to_id'");
									$tutorFullname = ucfirst($fetch_data->tutor_first_name)." ".ucfirst($fetch_data->tutor_last_name);
									$companyName = $fetch_data->company_name;
									
									if (isset($companyName) && $companyName!="")
									{
										$toName = $companyName." "."(".$tutorFullname.")";
									}else{
										$toName = $tutorFullname;
									}
								}
								if($mailrowResult->send_flag=='t'){
									$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
									$fetch_data = $tutorProfileobj->fetchRow("id='$mailrowResult->from_id'");
									$studentemailID = $fetch_data->tutor_email;
									$studentfullname = ucfirst($fetch_data->tutor_first_name)." ".ucfirst($fetch_data->tutor_last_name);
								}else{
									$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
									$student_data = $studentSignupObj->fetchRow("id='$mailrowResult->from_id'");
									$studentemailID = $student_data->std_email;
									$studentfullname = $student_data->first_name." ".$student_data->last_name;
								}
							
							//echo $tutorFullname;exit;
							$message1 = "<table width=60% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
										<tr align='center'>
											<td>Message :</td>
											<td width=50%>".$message_content."</td>
										</tr>
									
										</table><br/>";
							//$message1 .= "<div>$fromFullname is expecting to hear from you within 1 working day so do please follow up with your availability and take this forward.<br/></div><br/>";
							$message1 .= "<div>Cheers,<br/></div>";
							$message1 .= "<div>Sandeep<br/></div><br/>";
							$message1 .= "<div>Co-founder @ Skillzot</div>";
							$message1 .= "</div>";
							$message1 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
							
							$tempsubVal = "You've got mail from ".$toName;
							//print_r($message1);exit;
							$message = $message1;
							$to = $studentemailID;
							$from = "sz.enquiries@skillzot.com";
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,$tutorFullname);
							$mail->addTo($to,$studentfullname); //ask
							$mail->setSubject($tempsubVal);//ask
							//$mail->send();
							//----------------backup mail-----------------
							$subTemp =  "Msg from teacher (".$toName.") to student (".$studentfullname.")"; 
							//print_r($message1);exit;
							$message = $message1;
							$to = "sz.enquiries@skillzot.com";
							$to1 ="zot@skillzot.com";
							$to2 ="sandeep.burman@skillzot.com";
							//$to = "jinu_97@yahoo.co.in";
							$from = $studentemailID;
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom($from,$studentemailID);
							$mail->addTo($to,$toName); //ask
							$mail->addTo($to1,$toName);
							$mail->addTo($to2,$toName);
							$mail->setSubject($subTemp);//ask
							//$mail->send();
							
							
							
							//------------------------------------------
							$mailMessagesObj->insert($data1);
						}
						$this->_redirect("/editprofilenew/inboxmail");
					}
					
				}
			}
	
	
}
public function deleteinboxmailAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	$this->_helper->viewRenderer->setNoRender(true);
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
	if($this->_request->isPost()){
		$check_id = $this->_request->getParam('checkboxid');
		//echo $check_ids;exit;
		if($this->_request->isXmlHttpRequest()){
		$tags = "success";
		
		$data = array("inbox_delete_flag"=>"1");
		//$mailMessagesObj->delete("id='$check_id'");
		$mailMessagesObj->update($data,"id=$check_id");
		echo json_encode($tags);
		}
	 }
	
	
}
public function deletesentmailAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	$this->_helper->viewRenderer->setNoRender(true);
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
	if($this->_request->isPost()){
		$check_id = $this->_request->getParam('checkboxid');
		//echo $check_ids;exit;
		if($this->_request->isXmlHttpRequest()){
		$tags = "success";
		
		$data = array("sent_delete_flag"=>"1");
		//$mailMessagesObj->delete("id='$check_id'");
		$mailMessagesObj->update($data,"id=$check_id");
		echo json_encode($tags);
		}
	 }
}
public function updatedetailsAction()
{
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
 	$this->_helper->viewRenderer->setNoRender(true);
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
	
		if($this->_request->isPost()){
		
			if($this->_request->isXmlHttpRequest()){
			$tutorId = $this->_request->getParam("tutor_id");
			$defaultCourseid = $this->_request->getParam("default_course_id");
			$courseId = $this->_request->getParam("course_id");
			if (isset($defaultCourseid) && $defaultCourseid!=''){
				$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
														  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
														  ->where("s.tutor_id='$tutorId' && s.tutor_course_id='0'"));
			}else{
			$batch_rows = $batchdetailObj->fetchAll($batchdetailObj->select()
														  ->from(array('s'=>DATABASE_PREFIX."batch_details"))
														  ->where("s.tutor_id='$tutorId' && s.tutor_course_id='$courseId'"));
			}
			$i=0;
			$tags = "";
			$tags['totalsize'] = sizeof($batch_rows);
			
				foreach($batch_rows as $a){
					$tags[$i]['colorcode'] = $a->b_color;
					$tags[$i]['description'] = $a->b_description;
					$i++;
				}
			echo json_encode($tags);
			}
  		}
}
public function addbatchAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	$batchdetailObj = new Skillzot_Model_DbTable_Batchdetails();
	$getSelectedId = $this->_request->getParam("s_id");
	
	$batchTextValue = $this->_request->getParam("b_text_value");
	if (isset($batchTextValue) && $batchTextValue!="")
	{
		$this->view->batchtext = $batchTextValue;
	}
	$batchColor = $this->_request->getParam("b_color_value");
	if (isset($batchColor) && $batchColor!="")
	{
		$this->view->batchcolor = "#".strtoupper($batchColor);
	}
	//echo strtoupper($batchColor);exit;
	$courseId = $this->_request->getParam("b_course_id");
	if (isset($getSelectedId) && $getSelectedId!="")
	{
		$tempstartVararr = explode('_',$getSelectedId);
		$tempstartVar = $tempstartVararr[1];
		
		if (isset($tempstartVar) && $tempstartVar!="")
		{
			$this->view->starttime = $tempstartVar;
		}
		$tempendVar = $tempstartVar+1;
		if (isset($tempendVar) && $tempendVar!="")
		{
			$this->view->endtime = $tempendVar;
		}
		$tempdayVar = $tempstartVararr[2];
		if (isset($tempdayVar) && $tempdayVar!="")
		{
			$this->view->dayval = $tempdayVar;
		}
	}
//				$tutor_id = "2";
//				$color="#FFFFFF";
//				$row='38';
//				$colom ='7';
//				$desc="";
//				$endTime = '0';
//				$lastupdatedate = date("Y-m-d H:i:s");
//				for($i=1;$i<=$colom;$i++){
//					for($j=1;$j<=$row;$j++){
//					$data = array("tutor_id"=>$tutor_id,"b_description"=>$desc,"b_description"=>$desc,"b_color"=>$color,"b_color1"=>$color,
//					"b_day"=>$i,"b_start_timings"=>$j,"b_end_timings"=>$endTime,
//					"lastupdatedate"=>$lastupdatedate);
//					$batchdetailObj->insert($data);
//
//					}
//				}
//exit;
	
	if($this->_request->isPost()){
			
		if (isset($authUserNamespace->tutorsessionid) && $authUserNamespace->tutorsessionid!='')
		{
			$tutorId=$authUserNamespace->tutorsessionid;
		}else{
			$tutorId=$authUserNamespace->admintutorid;
		}
		
		$desc=$this->_request->getParam("batch_desc");
		$color=$this->_request->getParam("batch_color");
		if(isset($color) && $color==""){$color="#FFFFFF";}
		$day1=$this->_request->getParam("day_temp_id");
		$startTime=$this->_request->getParam("batch_start_time");
		$endTime=$this->_request->getParam("batch_end_time");
		$lastupdatedate = date("Y-m-d H:i:s");
		//echo $day1;exit;
			if($this->_request->isXmlHttpRequest()){
		
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			
			$response=array();
			
			if($desc == "Location")$response["data"]["batch_desc"] = "locationerr";
			else $response["data"]["batch_desc"] = "valid";
			
			if(($day1=="" || $startTime > $endTime))$response["data"]["batch_start_time"] = "invalid";
			else $response["data"]["batch_start_time"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('locationerr',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
			
			echo json_encode($response);
			
			}else{
				define('TUTORID',$tutorId);
				if (isset($day1) && $day1!=""){
					$dayFinal = substr($day1, 1);
					$arrayDay = explode(',',$dayFinal);
					
					if (isset($arrayDay) && sizeof($arrayDay)>1)
					{
					}else{
						$day = $dayFinal;
					}
				}
//					/echo sizeof($arrayDay);exit;
					$desc = ucfirst($desc);
				if (isset($arrayDay) && sizeof($arrayDay)>1)
				{
					//print_r($arrayDay);exit;
						for($j=0;$j<sizeof($arrayDay);$j++){
							for($i=$startTime;$i<$endTime;$i++){
								//echo "dd";exit;
								$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
								//print_r($data);exit;
								$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$arrayDay[$j]' && b_start_timings='$i' && tutor_course_id='$courseId'");
							}
						}
				}else{
					//echo 'out';exit;
				if (isset($day) && $day=='10')
						{
							$temp = 1;
							$tempend= 7;
							for($j=$temp;$j<=$tempend;$j++){
								for($i=$startTime;$i<$endTime;$i++){
									//echo "dd";exit;
									$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
									//print_r($data);exit;
									$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$j' && b_start_timings='$i' && tutor_course_id='$courseId'");
								}
							}
						}elseif(isset($day) && $day=='8'){
							$temp = 1;
								$tempend= 5;
								for($j=$temp;$j<=$tempend;$j++){
									for($i=$startTime;$i<$endTime;$i++){
										//echo "dd";exit;
										$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
										//print_r($data);exit;
										$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$j' && b_start_timings='$i' && tutor_course_id='$courseId'");
									}
								}
						}elseif(isset($day) && $day=='9'){
							$temp = 6;
								$tempend= 7;
								for($j=$temp;$j<=$tempend;$j++){
									for($i=$startTime;$i<$endTime;$i++){
										//echo "dd";exit;
										$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
										//print_r($data);exit;
										$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$j' && b_start_timings='$i' && tutor_course_id='$courseId'");
									}
								}
						}else{
							for($i=$startTime;$i<$endTime;$i++){
								$data = array("b_description"=>$desc,"b_color"=>$color,"lastupdatedate"=>$lastupdatedate);
								//print_r($data);exit;
								$batchdetailObj->update($data,"tutor_id='$tutorId' && b_day='$day' && b_start_timings='$i' && tutor_course_id='$courseId'");
							}
						}
				}
					echo "<script>parent.testfun(".TUTORID.");</script>";
					echo "<script>parent.Mediabox.close();</script>";
			}
	}
}
function removeclasspicAction()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
 	$this->_helper->viewRenderer->setNoRender(true);
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	
		if($this->_request->isPost()){
		
			if($this->_request->isXmlHttpRequest()){
			$courseId = $this->_request->getParam("courseid");
			if (isset($courseId) && $courseId!=''){
				$data = array("tutor_class_image_type"=>'',"tutor_class_image_content"=>'');
				$tutorCourseobj->update($data,"id=$courseId");
				
				$i=0;
				$tags = "";
				$tags = "success";
				echo json_encode($tags);
			}
		
			}
			
  		}
}
function upadatequalificationAction()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
 	$this->_helper->viewRenderer->setNoRender(true);
 	$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
 	
 	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
									$profile_rows1 = $tutorProfile->fetchAll($tutorProfile->select()
																->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.id'))
																);
																
	foreach ($profile_rows1 as $p)
	{
		//echo $p->id."<br/>";
		$tutorDataquali = $tutorexperienceObj->fetchAll($tutorexperienceObj->select()
													->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"))
													->where("c.tutor_id='$p->id' && c.tutor_expedu_type='2'"));
		if(isset($tutorDataquali) && sizeof($tutorDataquali)>0)
		{	
			$totalQualicount = sizeof($tutorDataquali);
			$data2 = array("qualification_count"=>$totalQualicount);
			//print_r($data2);echo "<br/>";
			$tutorexperienceObj->update($data2,"tutor_id=$p->id");
			$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
		}
	}
	
}
public function deletebranchAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(true);
		    $branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
			$branchId = $this->_request->getParam('branchid');
			$courseId = $this->_request->getParam('courseid');
			if (isset($branchId) && $branchId!="")
			{
				$branchdetailObj->delete("id='$branchId'");
			}
			$authUserNamespace->changesave = "Your changes have been saved";
			$link = "/editprofilenew/editcourse/courseid/".$courseId;
			$this->_redirect($link);
	}
public function deletebatchAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
			
			$batchdetailObj = new Skillzot_Model_DbTable_Batch();
			$batchId = $this->_request->getParam('batchid');
			$courseId = $this->_request->getParam('courseid');
			$batchviewResult=$batchdetailObj->fetchRow($batchdetailObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.id ='$batchId'"));
		//	if (isset($batchviewResult) && sizeof($batchviewResult) > 0) 
			
			$this->view->batchId = $batchId;
			$this->view->courseId = $courseId;
			$this->view->batchviewResult = $batchviewResult;
		if($this->_request->isPost()){

		$status_value = $this->_request->getParam("delete");

		if($this->_request->isXmlHttpRequest()){

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$response = array();


		if($status_value == "1")$response["data"]["statusflag"] = "valid";
		else $response["data"]["statusflag"] = "valid";

		if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
		else $response['returnvalue'] = "validation";

		echo json_encode($response);

		}else{
		if (isset($batchId) && $batchId!="")
			{
					$batchdetailObj->delete("id='$batchId'");
			}
		echo "<script>window.parent.location.reload();</script>";
	//	echo "<script>parent.Mediabox.close();</script>";

		}
	}
				
}	
public function deletebatchslotAction(){
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
			$this->_helper->layout()->disableLayout();
			
			$batchdetailObj = new Skillzot_Model_DbTable_Batch();
			$batchId = $this->_request->getParam('batchid');
			$tutor_batch_day = $this->_request->getParam('tutor_batch_day');

			$batchviewResult=$batchdetailObj->fetchRow($batchdetailObj->select()
					->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"),array('*'))
					->where("c.id ='$batchId'"));
			
			
			$this->view->batchId = $batchId;
			$this->view->tutor_batch_day = $tutor_batch_day;
			$this->view->batchviewResult = $batchviewResult;

			if($this->_request->isPost()){

			$status_value = $this->_request->getParam("delete");

			if($this->_request->isXmlHttpRequest()){

			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$response = array();


			if($status_value == "1")$response["data"]["statusflag"] = "valid";
			else $response["data"]["statusflag"] = "valid";

			if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";

			echo json_encode($response);

			}else{
			if (isset($batchId) && $batchId!="")
				{
						$batchfromtiming1 = $batchviewResult->tutor_batch_from_timing1;
						$batchtotiming1 = $batchviewResult->tutor_batch_to_timing1;
						$batchfromtiming2 = $batchviewResult->tutor_batch_from_timing2;
						$batchtotiming2 = $batchviewResult->tutor_batch_to_timing2;
						$batchfromtiming3 = $batchviewResult->tutor_batch_from_timing3;
						$batchtotiming3 = $batchviewResult->tutor_batch_to_timing3;
						$batchday1 = $batchviewResult->tutor_batch_day1;
						$batchday2 = $batchviewResult->tutor_batch_day2;
						$batchday3 = $batchviewResult->tutor_batch_day3;
					if($tutor_batch_day == "1"){

						$data = array("tutor_batch_from_timing1"=>$batchfromtiming2,"tutor_batch_to_timing1"=>$batchtotiming2,
							"tutor_batch_from_timing2"=>$batchfromtiming3,"tutor_batch_to_timing2"=>$batchtotiming3,
							"tutor_batch_from_timing3"=>"","tutor_batch_to_timing3"=>"","tutor_batch_day1"=>$batchday2,
							"tutor_batch_day2"=>$batchday3,"tutor_batch_day3"=>"");
					}elseif($tutor_batch_day == "2"){

						$data = array("tutor_batch_from_timing1"=>$batchfromtiming1,"tutor_batch_to_timing1"=>$batchtotiming1,
							"tutor_batch_from_timing2"=>$batchfromtiming3,"tutor_batch_to_timing2"=>$batchtotiming3,
							"tutor_batch_from_timing3"=>"","tutor_batch_to_timing3"=>"","tutor_batch_day1"=>$batchday1,
							"tutor_batch_day2"=>$batchday3,"tutor_batch_day3"=>"");
					}else{

						$data = array("tutor_batch_from_timing1"=>$batchfromtiming1,"tutor_batch_to_timing1"=>$batchtotiming1,
							"tutor_batch_from_timing2"=>$batchfromtiming2,"tutor_batch_to_timing2"=>$batchtotiming2,
							"tutor_batch_from_timing3"=>"","tutor_batch_to_timing3"=>"","tutor_batch_day1"=>$batchday1,
							"tutor_batch_day2"=>$batchday2,"tutor_batch_day3"=>"");
					}
					
					$batchdetailObj->update($data,"id='$batchId'");
				}
			echo "<script>window.parent.location.reload();</script>";
		//	echo "<script>parent.Mediabox.close();</script>";

			}
		}
				
}		
public function photosAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '7';
		$this->view->selectid = $selectId;
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutor_id = $authUserNamespace->admintutorid;
		
		if(isset($tutor_id) && $tutor_id!="")
		{
			 $photoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
     											 ->where("v.tutor_id = '$tutor_id' && v.tutor_album_id='0'")
     											 ->order(array("lastupdatedate DESC")));
     		if (isset($photoResultdata) && sizeof($photoResultdata) > 0)
     		{
     			$this->view->photoresult = $photoResultdata;
     			
     		}
		}
}


public function albumsAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '8';
		$this->view->selectid = $selectId;
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		
		$tutor_id = $authUserNamespace->admintutorid;				
		if(isset($tutor_id) && $tutor_id!="")
		{
     		 $albumResultdata = $tutorAlbumObj->fetchAll($tutorAlbumObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('a'=>DATABASE_PREFIX."tx_tutor_albums"))
     											 ->joinLeft(array('p'=>DATABASE_PREFIX."tx_tutor_photos"),"p.tutor_album_id = a.id && p.cover_flag = 'y'",array('p.thumb_photo_link'))	     											 
     											 ->where("a.tutor_id = '$tutor_id' ")
     											 ->order(array("lastupdatedate DESC")));	     											 
     		if (isset($albumResultdata) && sizeof($albumResultdata) > 0)
     		{
     			$this->view->albumresult = $albumResultdata;		     						
     		}
		}			
 }	
public function videosAction()
{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '9';
		$this->view->selectid = $selectId;
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();			
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
		
		$tutor_id = $authUserNamespace->admintutorid;			
		
		if(isset($tutor_id) && $tutor_id!="")
		{
		 $videoResultdata = $tutorVideoObj->fetchAll($tutorVideoObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_videos"))
     											 ->where("v.tutor_id = '$tutor_id'")
     											 ->order(array("lastupdatedate DESC")));
     		if (isset($videoResultdata) && sizeof($videoResultdata) > 0)
     		{
     			$this->view->videoresult = $videoResultdata;
     			
     		}
		}
		
 }

public function showalbumsphotosAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
		$selectId = '8';
		$this->view->selectid = $selectId;
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		$tutor_album_id = $this->_request->getParam('tutor_album_id');
		$this->view->tutor_album_id = $tutor_album_id;
		$tutor_id = $authUserNamespace->admintutorid;
		
		if(isset($tutor_id) && $tutor_id!="")
		{
			 $albumphotoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
     											 ->where("v.tutor_id = '$tutor_id' && v.tutor_album_id = $tutor_album_id")
     											 ->order(array("lastupdatedate DESC")));
     		if (isset($albumphotoResultdata) && sizeof($albumphotoResultdata) > 0)
     		{
     			$this->view->albumphotoresult = $albumphotoResultdata;
     			
     		}
		}
		if(isset($tutor_album_id) && $tutor_album_id!="")
		{
			$tutorAlbumRow = $tutorAlbumObj->fetchAll("id='$tutor_album_id'");
			if (isset($tutorAlbumRow) && sizeof($tutorAlbumRow)>0)
				{
					foreach($tutorAlbumRow as $tar)
					{							
						$albumtitle = $tar->title;
						$albumid= $tar->id;
						$this->view->albumtitle = $albumtitle;			
						$this->view->albumid = $albumid;							
					}
				}
		}
}	

public function tutorphotodeleteAction()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
				
	$deletetutorphotos = $this->_request->getParam('id'); 
	
		if(isset($deletetutorphotos) && $deletetutorphotos!="")
		{								
				$tutorphotosRow = $tutorphotosObj->fetchRow("id='$deletetutorphotos'");
				if (isset($tutorphotosRow) && sizeof($tutorphotosRow)>0)
				{
					$photoUrl = $tutorphotosRow->photo_link;
					$thumb_photo_link = $tutorphotosRow->thumb_photo_link;						
				}
				$fetch_data = $tutorphotosObj->fetchRow("id='$deletetutorphotos'");
		    	$authUserNamespace->admintutorid=$fetch_data->tutor_id;									
				//echo dirname(dirname(dirname(__FILE__))).$photoUrl;exit;
				$tutorphotosObj->delete("id='$deletetutorphotos'");	
				unlink(dirname(dirname(dirname(__FILE__))).$photoUrl);
				unlink(dirname(dirname(dirname(__FILE__))).$thumb_photo_link);	
				$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
		    	
				$photoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
								->setIntegrityCheck(false)
								->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
								->where("v.tutor_id = '$authUserNamespace->admintutorid' && v.tutor_album_id='0'")
								->order(array("lastupdatedate DESC")));
					//$authUserNamespace->admintutorid=$fetch_data->tutor_id;
				//echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/photos'</script>";					
				$this->_redirect('/editprofilenew/photos');
		}
//echo "hi".$Id;
}
public function tutoralbumphotodeleteAction()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
				
	$deletetutoralbumphotos = $this->_request->getParam('id'); 
	$deletetutoralbumid = $this->_request->getParam('albumid'); 
	
	if(isset($deletetutoralbumphotos) && isset($deletetutoralbumid))
		{								
				$tutoralbumphotosRow = $tutorphotosObj->fetchRow("id='$deletetutoralbumphotos' && tutor_album_id=$deletetutoralbumid");
				if (isset($tutoralbumphotosRow) && sizeof($tutoralbumphotosRow)>0)
				{
					$photoUrl = $tutoralbumphotosRow->photo_link;
					$thumb_photo_link = $tutoralbumphotosRow->thumb_photo_link;						
				}									
				$fetch_data = $tutorphotosObj->fetchRow("id='$deletetutoralbumphotos'");
  			    $authUserNamespace->admintutorid=$fetch_data->tutor_id;	
				$tutorphotosObj->delete("id='$deletetutoralbumphotos' && tutor_album_id=$deletetutoralbumid");	
				$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");
				unlink(dirname(dirname(dirname(__FILE__))).$photoUrl);
				
				unlink(dirname(dirname(dirname(__FILE__))).$thumb_photo_link);	
				$albumphotoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
     											 ->where("v.tutor_id = '$authUserNamespace->admintutorid' && v.tutor_album_id = $deletetutoralbumid")
     											 ->order(array("lastupdatedate DESC")));						
				$this->_redirect("/editprofilenew/showalbumsphotos/tutor_album_id/$deletetutoralbumid");
			//echo "<script>window.parent.location='". BASEPATH ."/editprofilenew/showalbumsphotos/tutor_album_id/$deletetutoralbumid'</script>";
		}
}
public function makecoverphotoAction()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	
	$makealbumcover = $this->_request->getParam('id'); 		
	$albumid = $this->_request->getParam('albumid'); 

	if(isset($makealbumcover) && isset($albumid))
		{
			$tutorphotosRow = $tutorphotosObj->fetchAll("tutor_album_id='$albumid'");
				
			if (isset($tutorphotosRow) && sizeof($tutorphotosRow)>0)
				{
					$i=0;
					foreach($tutorphotosRow as $tpr)
					{
						$photoid[$i] = $tpr->id;							
						$data = array("cover_flag"=>'n');	
						$tutorphotosObj->update($data,"id='$photoid[$i]'");

						
					}
				}
					$data = array("cover_flag"=>'y');				
	  			    $tutorphotosObj->update($data,"id=$makealbumcover");	
	  			    $fetch_data = $tutorphotosObj->fetchRow("id='$authUserNamespace->admintutorid'");
  			    	$authUserNamespace->admintutorid=$fetch_data->tutor_id;	
  			    	$photoResultdata = $tutorphotosObj->fetchAll($tutorphotosObj->select()
										->setIntegrityCheck(false)
										->from(array('v'=>DATABASE_PREFIX."tx_tutor_photos"))
										->where("v.tutor_id = '$authUserNamespace->admintutorid' && v.tutor_album_id='0'")
										->order(array("lastupdatedate DESC")));
  			    	$this->_redirect("/editprofilenew/albums");
	  			    		  			    			   
		}

}
public function tutoralbumsdeleteAction()
{
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	if(!isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid==""){$this->_redirect('/adminnew/tutorsignup');}
	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	
	$deletetutoralbum = $this->_request->getParam('id'); 
	
	if(isset($deletetutoralbum) && $deletetutoralbum!="")
	{
		$tutorphotosRow = $tutorphotosObj->fetchAll("tutor_album_id='$deletetutoralbum'");
		
				if (isset($tutorphotosRow) && sizeof($tutorphotosRow)>0)
				{
					$i=0;
					
					foreach($tutorphotosRow as $tpr)
					{
						$photoid[$i] = $tpr->id;
						$photoUrl[$i] = $tpr->photo_link;
						$thumb_photo_link[$i] = $tpr->thumb_photo_link;
						
						$tutorphotosObj->delete("id='$photoid[$i]'");	
						 unlink(dirname(dirname(dirname(__FILE__))).$photoUrl[$i]);					
						 unlink(dirname(dirname(dirname(__FILE__))).$thumb_photo_link[$i]);
						$i++;
					} 
				}
			$fetch_data = $tutorAlbumObj->fetchRow("id='$deletetutoralbum'");
  			$authUserNamespace->admintutorid=$fetch_data->tutor_id;
			$tutorAlbumObj->delete("id='$deletetutoralbum'");	
			$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorexperienceObj = new Skillzot_Model_DbTable_Tutorexperience();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					$tutorreviewobj = new Skillzot_Model_DbTable_Review();
					$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
					
					$trustid = $authUserNamespace->admintutorid;
					$certificate = $tutorexperienceObj->fetchRow($tutorexperienceObj->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_experience"),array('count(c.tutor_expedu_kind) as tutor_expedu_kind'))
									->where("c.tutor_id='".$trustid."' and c.tutor_expedu_type='2'"));

					$videos = $tutorVideoObj->fetchRow($tutorVideoObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_videos"),array('count(c.video_link) as video_link'))
								->where("c.tutor_id='".$trustid."'"));

					$photos = $tutorphotosObj->fetchRow($tutorphotosObj->select()
								->from(array('c'=>DATABASE_PREFIX."tx_tutor_photos"),array('count(c.photo_link) as photo_link'))
								->where("c.tutor_id='".$trustid."'"));
			
					$twitter = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_tw_link) as tutor_tw_link'))
									->where("c.id='".$trustid."' and c.tutor_tw_link!=''"));

					$facebook = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.tutor_fb_link) as tutor_fb_link'))
									->where("c.id='".$trustid."' and c.tutor_fb_link!=''"));

					$blog = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.blog_link) as blog_link'))
									->where("c.id='".$trustid."' and c.blog_link!=''"));

					$review = $tutorreviewobj->fetchRow($tutorreviewobj->select()
									->from(array('c'=>DATABASE_PREFIX."tutor_review"),array('count(*) as reviews'))
									->where("c.id='".$trustid."'"));

					$virtual = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.virtual_tour) as virtual_tour'))
									->where("c.id='".$trustid."' and c.virtual_tour!=''"));

					$adhar = $tutorProfile->fetchRow($tutorProfile->select()
									->from(array('c'=>DATABASE_PREFIX."tx_tutor_profile"),array('count(c.adhar_license) as adhar_license'))
									->where("c.id='".$trustid."' and c.adhar_license!=''"));

					if(isset($review->reviews) && $review->reviews >= "10"){$reviews = "10";}else{$reviews = $review->reviews;}
					if(isset($certificate->tutor_expedu_kind) && $certificate->tutor_expedu_kind!="0"){$certificates = "1";}else{$certificates = "0";}
					if(isset($photos->photo_link) && $photos->photo_link!="0"){$photo = "1";}else{$photo = "0";}
					if(isset($videos->video_link) && $videos->video_link!="0"){$video = "1";}else{$video = "0";}
					if(isset($virtual->virtual_tour) && $virtual->virtual_tour!="0"){$virtuals = "1";}else{$virtuals = "0";}
					if(isset($blog->blog_link) && $blog->blog_link!="0"){$blogs = "1";}else{$blogs = "0";}
					if(isset($facebook->tutor_fb_link) && $facebook->tutor_fb_link!="0"){$facebooks = "1";}else{$facebooks = "0";}
					if(isset($twitter->tutor_tw_link) && $twitter->tutor_tw_link!="0"){$twitters = "1";}else{$twitters = "0";}
					if(isset($adhar->adhar_license) && $adhar->adhar_license!="0"){$adhar = "1";}else{$adhar = "0";}

					//$total = $certificate->tutor_expedu_kind + $photos->photo_link + $videos->video_link + $twitter->tutor_tw_link + $facebook->tutor_fb_link + 
					//$blog->blog_link + $review->reviews + $virtual->virtual_tour + $adhar->adhar_license;

					$total = $reviews+$certificates+$photo+$video+$virtuals+$blogs+$facebooks+$twitters+$adhar;
					$data_for_trust = array("trustmeter" => $total);
					$tutorProfile->update($data_for_trust,"id=$authUserNamespace->admintutorid");		
	}
	$albumResultdata = $tutorAlbumObj->fetchAll($tutorAlbumObj->select()
                                                 ->setIntegrityCheck(false)
     											 ->from(array('a'=>DATABASE_PREFIX."tx_tutor_albums"))
     											 ->joinLeft(array('p'=>DATABASE_PREFIX."tx_tutor_photos"),"p.tutor_album_id = a.id && p.cover_flag = 'y'",array('p.thumb_photo_link'))	     											 
     											 ->where("a.tutor_id = '$authUserNamespace->admintutorid' ")
     											 ->order(array("lastupdatedate DESC")));
	$this->_redirect("/editprofilenew/albums");
}
}
?>
