<?php
class SearchController extends Zend_Controller_Action{
	
	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		//$this->_helper->layout()->setLayout("innerpage");
		
	}
	
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("searchinnerpage");
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$skillListingobj = new Skillzot_Model_DbTable_Skillslisting();
		$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$adressObj = new Skillzot_Model_DbTable_Address();
		$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
		$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
		$adressObj = new Skillzot_Model_DbTable_Address();

		$batchObj= new Skillzot_Model_DbTable_Batch();
		$addresstable = new Skillzot_Model_DbTable_Address();
		
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		$ageObj=new Skillzot_Model_DbTable_StudentAge();

	
		$localityId = $this->_request->getParam("locality");//locality search
		//echo "Locality=".$localityId;exit;
		if (isset($localityId) && $localityId!=""){
			
			if ($localityId=='southmumbai'){$localityId=13;}
			elseif ($localityId=='centralsuburb1'){$localityId=2;}
			elseif ($localityId=='centralsuburb2'){$localityId=3;}
			elseif ($localityId=='westernsuburb1'){$localityId=10;}
			elseif ($localityId=='westernsuburb2'){$localityId=11;}
			elseif ($localityId=='harboursuburb1'){$localityId=6;}
			elseif ($localityId='navimumbai'){$localityId=14;}
			
			$this->view->localityID = $localityId;
		}
		//------------------richa --------------
		$ageID=$this->_request->getParam("age");
		if (isset($ageID) && $ageID!=""){
			
			if ($ageID=='kids'){$ageID=2;}
			else{$ageID="";}
					
			$this->view->ageID = $ageID;
		}
		$batchID=$this->_request->getParam("batch");
		if (isset($batchID) && $batchID!=""){
			
			if ($batchID=='early'){$batchID=2;}
			else if($batchID=='late'){$batchID=3;}
			else if($batchID=='afternoon'){$batchID=4;}
			else if($batchID=='evening'){$batchID=5;}
			else if($batchID=='night'){$batchID=6;}
			else if($batchID=='weekend'){$batchID=7;}
			else {$batchID="";}
			//print_r($batchID);		
			$this->view->batchID = $batchID;
		}
		//------------richa-------------------
		$fetchSkill_type =  preg_replace("/[^A-Za-z0-9]/", "", $this->_request->getParam("lesson"));//lession type
		if (isset($fetchSkill_type) && $fetchSkill_type!=""){
			
			if($fetchSkill_type=='personal'){$fetchSkill_type=1;}
			elseif ($fetchSkill_type=='group'){$fetchSkill_type=2;}
			elseif($fetchSkill_type=='personalgroup'){$fetchSkill_type=3;}
			$this->view->lessonType = $fetchSkill_type;
		}
		//echo $fetchSkill_type;exit;
		$locationId = preg_replace("/[^A-Za-z0-9]/", "", $this->_request->getParam("location"));//class location search
		
		//echo $locationId;exit;
		if (isset($locationId) && $locationId!=""){
			
			if($locationId=='homelessons'){$locationId=1;}
			elseif ($locationId=='teacherslocation'){$locationId=2;}
			elseif ($locationId=='homelessonsteacherslocation'){$locationId=3;}
			
			$this->view->classlocationType = $locationId;
		}
		//echo "locality---".$localityId ;
		//echo "lession type---".$fetchSkill_type ;
//		echo "class location search---".$locationId ;
		//echo "<br/>";
		//echo $localityId;
		//exit;
		

//echo "locality---".$localityId ;exit;
//echo "<br/>";
//echo "lession type---".$fetchSkill_type ;
//echo "<br/>";
//echo "class location search---".$locationId ;
//echo "<br/>";
//exit;
		//echo $searchskillFrdly;exit;
		
		//---------------------narrow down search--------------------
		$categoryskillRows = $skilldisplayObj->fetchAll($skilldisplayObj->select()
											  ->from(array('s'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
											  ->where("parent_skill_id = 0 && skill_id!=0 && skill_id != 2 && is_skill='1'")
											  ->order(array("order_id asc")));
		if (isset($categoryskillRows) && sizeof($categoryskillRows)>0){
			$this->view->categoryvalues = $categoryskillRows;
		}
		// print_r($categoryskillRows);exit;
		$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
		$classtypeResult = $classtypeObj->fetchAll($classtypeObj->select()
										->from(array('s'=>DATABASE_PREFIX."master_tutor_class_types"))
										->order(array("class_type_id asc")));
		if (isset($classtypeResult) && sizeof($classtypeResult)>0){
			$this->view->classtype = $classtypeResult;
		}
		$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
		$suburbsData = $localitysuburbsObj->fetchAll($localitysuburbsObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."locality_suburbs"))
															  ->where("s.is_active='1'")
															  ->order(array("s.order asc")));
				if (isset($suburbsData) && sizeof($suburbsData)>0)
				{
					$this->view->suburbsdata = $suburbsData;
				}		
		$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
		$lessonlocationResult = $lessonlocationObj->fetchAll();
		if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0){
			$this->view->lessonlocation = $lessonlocationResult;
		}
		//--------------------------age ---------------------------
		$ageObj=new Skillzot_Model_DbTable_StudentAge();
			$agetypeResult=$ageObj->fetchAll($ageObj->select()
										->from(array('s'=>DATABASE_PREFIX."student_age"))
										->where("student_age_id!=0"));
		if(isset($agetypeResult) && sizeof($agetypeResult)>0){
			$this->view->age=$agetypeResult;
		}
		//----------------------------batch timing-------------------------------
		$batchObj=new Skillzot_Model_DbTable_BatchTiming();
		$batchTimingResult=$batchObj->fetchAll($batchObj->select()
										->from(array('s'=>DATABASE_PREFIX."batch_timing"))
										->where("id!=0"));
		if(isset($batchTimingResult) && sizeof($batchTimingResult)>0){
			$this->view->batches=$batchTimingResult;
		}
		//----------------------------------------------------------------------
		
		$pageValue = $this->_request->getParam("page");
		//echo $pageValue;exit;
		if (isset($pageValue) && $pageValue!="")
		{
		$this->view->pagevalue = $pageValue;
		}else{
			$this->view->pagevalue = $pageValue;
		}
		
		$fetchSkill_id_url = $this->_request->getParam("skillid");/* search skiil id*/
		$skillNamefrdly = $this->_request->getParam("skillname");


		//echo $skillNamefrdly;exit;
		/* Rakesh changes for home search */
		$search_tutor = $this->_request->getParam("search");
		$locality_tutor = $this->_request->getParam("locality_value");
		$search_keywords = $this->_request->getParam("search_keywords");
		$skill_keywords = $this->_request->getParam("skill_keywords");
		$this->view->search_keywords = $search_keywords;
		$this->view->skill_keywords = $skill_keywords;
		/*end of rakesh */
		//echo $skill_keywords;exit;
		$nameArrayfrdly = explode("-", $skillNamefrdly);
		/*if (isset($nameArrayfrdly) && sizeof($nameArrayfrdly)>0)
		{
			$skillNamefrdly = $nameArrayfrdly[0];
		}*/

		$skillNamefrdly = $nameArrayfrdly[0];

		$category_rows = $skilldisplayObj->fetchRow($skilldisplayObj->select()
						  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id'))
						  ->where("s.skill_uniq='$skillNamefrdly' && s.skill_id!=0 && s.is_skill='1'"));
						  if (isset($category_rows) && sizeof($category_rows)>0)
						  {
						  	$fetchSkill_id = $category_rows->skill_id;
						  }else{
						  	$fetchSkill_id=$fetchSkill_id_url;
						  }

			//--------------------skilldata for title and other--------------------------	
			//echo $fetchSkill_id;exit;
	if (isset($fetchSkill_id) && $fetchSkill_id!="")//main loop to avoid method post error
	{		
		$skillndata = $skilldisplayObj->fetchRow($skilldisplayObj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1'"));//21-8
								 // print_r($skillndata);exit;
			if (isset($skillndata) && sizeof($skillndata)>0 )
				{//echo "ff";exit;
					$skillCategory = $skillndata->skill_name;
					//echo "first";exit;
					$this->view->skillcategory = $skillCategory;
					$this->view->skillcategoryUniq = $skillndata->skill_uniq;
					$this->view->skillcategoryUniqPagetitle = $skillndata->page_title;
					$this->view->skillMetadescription = $skillndata->skill_description;
					$this->view->skillid = $skillndata->skill_id;//main catogory
					$this->view->idtoshowselected = $fetchSkill_id;
					$skillDepth = $skillndata->skill_depth;
					//$authUserNamespace->skillcategory = $skillCategory;
					$parentId = $skillndata->parent_skill_id;
					
					if (isset($skillDepth) && $skillDepth=='1')
					{
					
								$categorySubRows = $skilldisplayObj->fetchAll($skilldisplayObj->select()
																	  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id','s.skill_uniq'))
																	  ->where("is_enabled = 1 && parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$fetchSkill_id' && skill_depth='2' && is_skill='1'")
																	  ->order(array("skill_name asc")));
																	  
																	 
																	 
								if (isset($categorySubRows) && sizeof($categorySubRows)>0){
									$this->view->categorysubrows = $categorySubRows;
								}
					}else if(isset($skillDepth) && $skillDepth=='2'){
						//echo "ff";exit;
								$categorySubRows = $skilldisplayObj->fetchAll($skilldisplayObj->select()
																	  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id','s.skill_uniq'))
																	  ->where("is_enabled = 1 && parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$parentId' && skill_depth='2' && is_skill='1'")
																	   ->order(array("skill_name asc")));
								if (isset($categorySubRows) && sizeof($categorySubRows)>0){
									$this->view->categorysubrows = $categorySubRows;
								}
								//echo "ss";exit;
								
								$categorylastRows = $skilldisplayObj->fetchAll($skilldisplayObj->select()
																	  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id','s.skill_uniq'))
																	  ->where("parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$fetchSkill_id' && skill_depth='3' && is_skill='1' && is_enabled='1'")
																	  ->order(array("s.skill_name asc")));
																	  
//																	  print $skilldisplayObj->select()
//																	  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id'))
//																	  ->where("parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$fetchSkill_id' && skill_depth='3' && is_skill='1' && is_enabled='1'")
//																	  ->order(array("s.skill_name asc"));exit;
																//s.parent_skill_id!='0' && s.skill_id!='0' && parent_skill_id='$sub_category_id' && skill_depth='3' && is_skill='1' && is_enabled='1'
								
								if (isset($categorylastRows) && sizeof($categorylastRows)>0){
									$this->view->categorylastrows = $categorylastRows;
								}
								
					}
					else{
						
								$parentdata = $skilldisplayObj->fetchRow("skill_id='$parentId' && skill_id!=0 && is_skill='1' && is_enabled='1'");
								//print_r($parentdata);exit;
								if (isset($parentdata) && sizeof($parentdata)>0 )
								{
									$categorySubRows = $skilldisplayObj->fetchAll($skilldisplayObj->select()
																		  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id','s.skill_uniq'))
																		  ->where("is_enabled = 1 && parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$parentdata->parent_skill_id' && skill_depth='2' && is_skill='1'")
																		   ->order(array("skill_name asc")));
																		  
									if (isset($categorySubRows) && sizeof($categorySubRows)>0){
										$this->view->categorysubrows = $categorySubRows;
								}
								}
						
								$categorylastRows = $skilldisplayObj->fetchAll($skilldisplayObj->select()
																	  ->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id','s.skill_uniq'))
																	  ->where("parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$parentId' && skill_depth='3' && is_skill='1' && is_enabled='1'")
																	  ->order(array("s.skill_name asc")));
																		//s.parent_skill_id!='0' && s.skill_id!='0' && parent_skill_id='$sub_category_id' && skill_depth='3' && is_skill='1' && is_enabled='1'
								if (isset($categorylastRows) && sizeof($categorylastRows)>0){
									$this->view->categorylastrows = $categorylastRows;
								}
						
					}
						
					
				}
		//echo "locality---".$localityId ;exit;		
		if (isset($skillDepth) && $skillDepth > 1)
		{//echo "in";exit;
			$parentdata = $skilldisplayObj->fetchRow("skill_id='$parentId' && skill_id!=0 && is_skill='1' && is_enabled='1'");//16-8
			//echo "ddd";print_r($parentdata);exit;
			if (isset($parentdata) && sizeof($parentdata)>0 )
			{
				$submainCategory = $parentdata->skill_name;
				if (isset($submainCategory) && $submainCategory!="")
					{
						
						$this->view->mainsubmaincategory = $submainCategory;
						//echo $submainCategory;exit;
						$this->view->mainsubmaincategoryUniq = $parentdata->skill_uniq;
						//$this->view->mainsubmaincategoryPagetitle = $parentdata->page_title;
						$this->view->mainsubmainid = $parentdata->skill_id;//sub category
						$this->view->idtoshowselected = $parentdata->skill_id;
						//$authUserNamespace->submaincategory = $submainCategory;
					}
				$seccondParentID = $parentdata->parent_skill_id;
				$secondskillDepth = $parentdata->skill_depth;
				if (isset($secondskillDepth) && $secondskillDepth > 1)
				{
					$finaldata = $skilldisplayObj->fetchRow("skill_id='$seccondParentID' && skill_id!=0 && is_skill='1'");//16-8
					if (isset($finaldata) && sizeof($finaldata) > 0)
					{
						$mainCategory = $finaldata->skill_name;
						if (isset($mainCategory) && $mainCategory!="")
						{
							$this->view->maincategory = $mainCategory;
							$this->view->maincategoryUniq = $finaldata->skill_uniq;
							//$this->view->maincategoryPagetitle = $finaldata->page_title;
							$this->view->maincategoryid = $finaldata->skill_id;
							$this->view->idtoshowselected = $finaldata->skill_id;
							//$authUserNamespace->maincategory = $mainCategory;
						}
					}
				}
			}
		}
		//exit;
	//-------------------------------------------------------------------		
			
//----------------------search new logic--------------------------------------------------
					$skillndata = $skillobj->fetchRow($skillobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
								  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1' && d.listing_flag='1'"));//21-8
								  
					if (isset($skillndata) && sizeof($skillndata)>0)
					{
						$fetchChildIdS = $skillobj->fetchAll($skillobj->select()
												  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
												  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
												  ->where("s.parent_skill_id = '$fetchSkill_id' && d.listing_flag='1'"));//21-8
												 
						/*fetch sub categories if child exsist of main categories*/
						$final_fetchids = array();
						//echo sizeof($fetchChildIdS);exit;
						if(isset($fetchChildIdS) && sizeof($fetchChildIdS)>0){
							$j = 0;
							$final_ids = array();
							$final_ids1 = array();
							foreach($fetchChildIdS as $subids){
								$final_ids[$j] = $subids->skill_id;
								$j++;
							}
						}
					}
					$m=0;
					/*fetch skills categories  of sub categories*/
					if(isset($final_ids) && sizeof($final_ids)>0){
						for($k=0;$k<sizeof($final_ids);$k++){
							$fetchsubChildIdS = $skillobj->fetchAll($skillobj->select()
													     ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
													      ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
													     ->where("s.parent_skill_id = '$final_ids[$k]' && d.listing_flag='1'"));//21-8
													     
							if(isset($fetchsubChildIdS) && sizeof($fetchsubChildIdS)){
								foreach($fetchsubChildIdS as $skillsubids){
									$final_ids1[$m] = $skillsubids->skill_id;
									$m++;
									
								}
							}
						}
						$final_fetchids = array_merge($final_ids,$final_ids1); /*All id with sub cat & skill id*/
	//					print_r($final_fetchids);exit;
					}
					//echo "locality---".$localityId ;exit;
					//echo $search_tutor;exit;
					/* Rakesh changes for home search */
					//echo "locality---".$localityId ;exit;
					if($search_tutor!="" && $locality_tutor!="")
					{

						$tutorprofileObj = new Skillzot_Model_DbTable_Tutorprofile();
						$batchObj= new Skillzot_Model_DbTable_Batch();
						$addresstable = new Skillzot_Model_DbTable_Address();
						$address_row=$addresstable->fetchRow($addresstable->select()
									->setIntegrityCheck(false)
									->from(array('a'=>DATABASE_PREFIX."master_address"),array('GROUP_CONCAT(a.address_id) as address_id'))
									->where("address_value like '%".$locality_tutor."%'"));
						$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
							 		   ->setIntegrityCheck(false)
									   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array('GROUP_CONCAT(a.id) as id'))
									   ->joinLeft(array('m'=>DATABASE_PREFIX."tx_tutor_course_batch"),'a.id=m.tutor_id',array('locality'))
									   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$search_tutor."%') || a.company_name like '%".$search_tutor."%') && m.locality IN (".$address_row->address_id.") && a.is_active='0'"));
						
						if($query_for_tutor->id=="")
						{
							$tmp_tutor_id=" ";
							$word_for_search1 = explode(" ",$search_tutor);
							for($i=0;$i<sizeof($word_for_search1); $i++)
							{
								//echo "for";exit;
								$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
							 		   ->setIntegrityCheck(false)
									   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array('GROUP_CONCAT(a.id) as id'))
									   ->joinLeft(array('m'=>DATABASE_PREFIX."tx_tutor_course_batch"),'a.id=m.tutor_id',array('locality'))
									   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$word_for_search1[$i]."%') || a.company_name like '%".$word_for_search1[$i]."%') && m.locality IN (".$address_row->address_id.") && a.is_active='0'"));
					
								if(isset($query_for_tutor->id) && sizeof($query_for_tutor->id)>0)
								{
									$tmp_tutor_id=$tmp_tutor_id.",".$query_for_tutor->id;
									
								}
									if($query_for_tutor->id=="")
									{
										$this->view->skillname=$skillNamefrdly;
										$this->view->searchname=$search_tutor;
										
									}
							}
						}
						if(isset($tmp_tutor_id) && $tmp_tutor_id!="")
						{
							$final_tutor_id=$tmp_tutor_id;
							$string = str_replace(',', ' ', $final_tutor_id);
							$string_for_tutor_id=trim($string);
							$tutor_id_for_tutor = explode(" ",$string_for_tutor_id);
							$tutor_id_for_search_locality1=$tutor_id_for_tutor[0];
							//echo $tutor_id_for_search1;exit;
							 
						}else{
							$tutor_id_for_search_locality1=$query_for_tutor->id;
						}
						
						if($tutor_id_for_search_locality1!=""){
							$tutor_id_for_search_locality=$tutor_id_for_search_locality1;
						}else{
							$tutor_id_for_search_locality=0;
						}
						//echo $search_tutor;exit;
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
													  ->where("t.tutor_id in ($tutor_id_for_search_locality)"));

						/*$tutor_id_for_search_locality1=$query_for_tutor->id;
						if($tutor_id_for_search_locality1!=""){
							$tutor_id_for_search_locality=$tutor_id_for_search_locality1;
						}else{
							$tutor_id_for_search_locality=0;
						}
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
													  ->where("t.tutor_id in ($tutor_id_for_search_locality)"));*/
					}elseif($search_tutor!="" && $locality_tutor==""){
						$tutorprofileObj = new Skillzot_Model_DbTable_Tutorprofile();
						$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array('GROUP_CONCAT(a.id) as id'))
									   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$search_tutor."%') || a.company_name like '%".$search_tutor."%')  && a.is_active='0'"));
						//echo "id".$query_for_tutor->id;exit;
						if($query_for_tutor->id=="")
						{
							//echo "if";exit;
							$tmp_tutor_id=" ";
							$word_for_search1 = explode(" ",$search_tutor);
							for($i=0;$i<sizeof($word_for_search1); $i++)
							{
								//echo "for";exit;
								$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
									   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array('GROUP_CONCAT(a.id) as id'))
									   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$word_for_search1[$i]."%') || a.company_name like '%".$word_for_search1[$i]."%') && a.is_active='0'"));

								if(isset($query_for_tutor->id) && sizeof($query_for_tutor->id)>0)
								{
									$tmp_tutor_id=$tmp_tutor_id.",".$query_for_tutor->id;
									
								}
									if($query_for_tutor->id=="")
									{
										$this->view->skillname=$skillNamefrdly;
										$this->view->searchname=$search_tutor;
										
									}
							}
						}

						
						if(isset($tmp_tutor_id) && $tmp_tutor_id!="")
						{
							$final_tutor_id=$tmp_tutor_id;
							$string = str_replace(',', ' ', $final_tutor_id);
							$string_for_tutor_id=trim($string);
							$tutor_id_for_tutor = explode(" ",$string_for_tutor_id);
							$tutor_id_for_search1=$tutor_id_for_tutor[0];
							//echo $tutor_id_for_search1;exit;
							 
						}else{
							$tutor_id_for_search1=$query_for_tutor->id;
						}
						
						if($tutor_id_for_search1!=""){
							$tutor_id_for_search=$tutor_id_for_search1;
						}else{
							$tutor_id_for_search=0;
						}
						//echo $search_tutor;exit;
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
													  ->where("t.tutor_id in ($tutor_id_for_search)"));
					}elseif($search_tutor=="" && $locality_tutor!=""){
						//echo $locality_tutor;
						
						$tutorprofileObj = new Skillzot_Model_DbTable_Tutorprofile();
						$addresstable = new Skillzot_Model_DbTable_Address();
						$address_row=$addresstable->fetchRow($addresstable->select()
									->setIntegrityCheck(false)
									->from(array('a'=>DATABASE_PREFIX."master_address"),array('GROUP_CONCAT(a.address_id) as address_id'))
									->where("address_value like '%".$locality_tutor."%'"));
						//echo "tutor".$address_row->address_id;exit;
						$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
							 		   ->setIntegrityCheck(false)
									   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array('GROUP_CONCAT(a.id) as id'))
									   ->joinLeft(array('m'=>DATABASE_PREFIX."tx_tutor_course_batch"),'a.id=m.tutor_id',array('locality'))
									   ->where("m.locality IN (".$address_row->address_id.") && a.is_active='0'"));
						$tutor_id_for_locality1=$query_for_tutor->id;
						//echo $tutor_id_for_locality1;exit;
						if($tutor_id_for_locality1!=""){
							$tutor_id_for_locality=$tutor_id_for_locality1;
						}else{
							$tutor_id_for_locality=0;
						}
						//print_r($tutor_id_for_locality);exit;
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
													  ->where("t.tutor_id in ($tutor_id_for_locality)"));
						//print_r($tutor_id_for_locality);exit;
					}else{
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));
					}

					if(sizeof($tutorCourseRows)==0){
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));
						$this->view->skillname=$skillNamefrdly;
						$this->view->searchname=$search_tutor;
						$this->view->localitysearch=$locality_tutor;
						//$search_tutor = "";
						//$locality_tutor = "";
					}else{
						$this->view->skillname1="1";
					}
					if(isset($skill_keywords) && $skill_keywords!="")
					{
						$skillrowResult1 = $skillListingobj->fetchRow($skillListingobj->select()
								   ->where("skill_name LIKE '%".$skill_keywords."%' || skill_uniq LIKE '%".$skill_keywords."%' && skill_id!='0' && is_skill='1'"));
						if($skillrowResult1=="")
						{		
						$skillmapobj = new Skillzot_Model_DbTable_SkillMap();
						$tmp_skill_word=explode(" ", trim($skill_keywords));
						$tmp_skill=" ";
						for($i=0;$i<sizeof($tmp_skill_word); $i++)
						{
							
							$skillrowResult = $skillmapobj->fetchRow($skillmapobj->select()
									   ->where("skill_name ='".$tmp_skill_word[$i]."'"));
							if($skillrowResult=="")
							{
								//echo 'sendo if';exit;
								$this->view->skillname=$skillNamefrdly;
							}	
						}
						//echo "if";exit;
						}else{
							$this->view->skillname1="1";
							//echo "else";exit;
						}
					}
					/*$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));*/
					/* end of rakesh code*/
					$i = 0;
					$l = 0;
					$final_tutor_id = array();
					$final_tutor_id_final =array();

					/* convert arry to string for profile table*/
					foreach($tutorCourseRows as $tutorCourserow){
					
						$Skill_id = $tutorCourserow->tutor_skill_id;
						//echo $Skill_id;exit;
						$removeLastgradeId = substr($Skill_id, 0, strlen($Skill_id)-1);
						$explodedCategory = explode(",",$removeLastgradeId);
						//print_r($explodedCategory);exit;
						if(isset($final_fetchids) && sizeof($final_fetchids)>0){
							
							for($g=0;$g<sizeof($final_fetchids);$g++){
								if(in_array($final_fetchids[$g],$explodedCategory)){
									$final_tutor_id[$i] = $tutorCourserow->tutor_id;
									$i=$i+1;
								}
							}
						}else{
							if(in_array($fetchSkill_id,$explodedCategory)){
							$listing_rows = $skillListingobj->fetchRow("skill_id=$fetchSkill_id && listing_flag='1'");
								if (isset($listing_rows) && sizeof($listing_rows)>0)
								{
									$final_tutor_id[$l] = $tutorCourserow->tutor_id;
									$l=$l+1;
								}
							}
						}
						$tempfinal_tutor_id1 = array_unique($final_tutor_id);
					//print_r($tempfinal_tutor_id);exit;
					}
					if(isset($tempfinal_tutor_id1) && sizeof($tempfinal_tutor_id1)>0)
					{
						//echo "if";exit;
						$tempfinal_tutor_id=$tempfinal_tutor_id1;
					}
					else{
						$this->view->skillname=$skillNamefrdly;
						$this->view->searchname=$search_tutor;
						$this->view->localitysearch=$locality_tutor;
						$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));
						$i = 0;
						$l = 0;
						$final_tutor_id = array();
						$final_tutor_id_final =array();

						foreach($tutorCourseRows as $tutorCourserow){
						
							$Skill_id = $tutorCourserow->tutor_skill_id;

							$removeLastgradeId = substr($Skill_id, 0, strlen($Skill_id)-1);
							$explodedCategory = explode(",",$removeLastgradeId);

							if(isset($final_fetchids) && sizeof($final_fetchids)>0){
								
								for($g=0;$g<sizeof($final_fetchids);$g++){
									if(in_array($final_fetchids[$g],$explodedCategory)){
										$final_tutor_id[$i] = $tutorCourserow->tutor_id;
										$i=$i+1;
									}
								}
							}else{
								if(in_array($fetchSkill_id,$explodedCategory)){
								$listing_rows = $skillListingobj->fetchRow("skill_id=$fetchSkill_id && listing_flag='1'");
									if (isset($listing_rows) && sizeof($listing_rows)>0)
									{
										$final_tutor_id[$l] = $tutorCourserow->tutor_id;
										$l=$l+1;
									}
								}
							}
							$tempfinal_tutor_id = array_unique($final_tutor_id);
				
						}
					}
					$q=0;
					foreach($tempfinal_tutor_id as $varId){
						$final_tutor_id1[$q] = $varId;
						$q++;
					}
					if (isset($final_tutor_id1) && sizeof($final_tutor_id1) > 0){
						$final_tutor_id_final = $final_tutor_id1;
					}
					//print_r($final_tutor_id_final);
					//print_r($tutor_id_for_search);
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0 ){
						$reviewObj = new Skillzot_Model_DbTable_Review();
						$strofTutorids = implode(",",$final_tutor_id_final);
						if(isset($tempfinal_tutor_id1) && sizeof($tempfinal_tutor_id1)>0){
							//echo "if";exit;
						if($search_tutor!="" && $locality_tutor==""){
							if($tutor_id_for_search !="")
							{
								$condition = "t.id in ($tutor_id_for_search)";
								$reviewdata=$reviewObj->fetchAll("skill_id !=''");
							}else{
								$condition = "t.id in ($strofTutorids)";
								$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");
							}
							
						}elseif($search_tutor!="" && $locality_tutor!=""){
							if($tutor_id_for_search !="")
							{
								$condition = "t.id in ($tutor_id_for_search_locality)";
								$reviewdata=$reviewObj->fetchAll("skill_id !=''");
							}else{
								$condition = "t.id in ($strofTutorids)";
								$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");
							}

						}elseif($search_tutor=="" && $locality_tutor!=""){
							if($tutor_id_for_search !="")
							{
								$condition = "t.id in ($tutor_id_for_locality)";
								$reviewdata=$reviewObj->fetchAll("skill_id !=''");
							}else{
								$condition = "t.id in ($strofTutorids)";
								$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");
							}
						}else{
							$condition = "t.id in ($strofTutorids)";
							$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");
						}
						}else{
							//echo "else";exit;
							$condition = "t.id in ($strofTutorids)";
							$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");
						}				
						
									//print_r($strofTutorids);	
									//echo $skillndata->skill_id;exit;
									
									
					
//-------------------------------------------------age search--------------------------------------------------
						$final_tutor_id5 = array();
						if (isset($ageID)&& $ageID!="")
						{
								//					print_r($strofTutorids);

							if (isset($final_tutor_id_final) && isset($ageID) && $ageID!="")
							{
								$arrystr = "0";
								//print_r(sizeof($final_tutor_id_final));
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									//print_r($final_tutor_id_final[$j]);
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								//print_r($arrystr);
								$condition = "tutor_id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}
							
							//print_r($condition);
							if ($ageID==2)
							{
								$fetch_age_id = "min <=12 OR max<=12";
								
								
								//$fetch_age_id = "tutor_grade_to_teach LIKE '%2%' OR tutor_grade_to_teach LIKE '%3%' OR tutor_grade_to_teach LIKE '%1%'";
							}else if($ageID==1){
								$fetch_age_id = "1=1";
							}
							//echo $fetch_location_id;exit;
							$tutorAgeRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
																	->from(array('s'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
																	->where("($fetch_age_id) and $condition"));
																	
									
								if(isset($tutorAgeRows) && sizeOf($tutorAgeRows)>0){
									$t=0;
									foreach($tutorAgeRows as $tutorAgeRow){
										$final_tutor_id5[$t] = $tutorAgeRow->tutor_id;
										$t++;
									}
								}
								//$final_tutor_id_final = $final_tutor_id4;
								
								$tempfinal_tutor_id1 = array_unique($final_tutor_id5);
								$smart1 = array();
								if (isset($tempfinal_tutor_id1) && sizeof($tempfinal_tutor_id1)>0)
								{
									$q=0;
									foreach($tempfinal_tutor_id1 as $v){
										$smart1[$q] =  $v;
										$q++;
									}
								}
								$final_tutor_id_final = $smart1;
						}

//---------------------------------------------------end of age search----------------------------------------------------
//-------------------------------------------------------batch search----------------------------------------------------
					$final_tutor_id6 = array();
						if (isset($batchID)&& $batchID!="")
						{
												//	print_r($strofTutorids);

							if (isset($final_tutor_id_final) && isset($batchID) && $batchID!="")
							{
								$arrystr = "0";
								//print_r(sizeof($final_tutor_id_final));
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									//print_r($final_tutor_id_final[$j]);
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								//print_r($arrystr);
								$condition = "tutor_id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}		
							$tutorBatchobj=new Skillzot_Model_DbTable_Batch();

							if($batchID==7){
								//print_r($batchID);    weekend batch
								$fetch_batch="tutor_batch_day like '%6%' or tutor_batch_day like '%7%'";
								$tutorBatchRows = $tutorBatchobj->fetchAll($tutorBatchobj->select()
																	->from(array('s'=>DATABASE_PREFIX.'tx_tutor_course_batch'))
																	->where("($fetch_batch) and $condition"));
							}
							else{
							if ($batchID==2)
							{//main condition : from_timing <=zone_end and to_timing>=zone_start
								$fetch_batch = "m.id>=11 and t.id<=19";//ZONE end 9:00am =19 and zone start 12:00 am
								//print_r($fetch_batch);
							}else if($batchID==3){
								$fetch_batch = "m.id>=19 and t.id<=25";
							}
							else if($batchID==4){
								$fetch_batch = "m.id>=25 and t.id<=35";
							}
							else if($batchID==5){
								$fetch_batch = "m.id>=35 and t.id<=43";
							}
							else if($batchID==6){
								$fetch_batch = "m.id>=43 and t.id<=67";
							}
							
							//echo $fetch_location_id;exit;
							$tutorBatchRows = $tutorBatchobj->fetchAll($tutorBatchobj->select()
																	->from(array('s'=>DATABASE_PREFIX.'tx_tutor_course_batch'))
																	->join(array('m'=>DATABASE_PREFIX.'master_batch_timing'),"s.tutor_batch_from_timing=m.timing",array(''))
																	->join(array('t'=>DATABASE_PREFIX.'master_batch_timing'),"s.tutor_batch_to_timing=t.timing",array(''))
																	->where("$condition and ($fetch_batch)"));
																	
							}
								if(isset($tutorBatchRows) && sizeOf($tutorBatchRows)>0){
									$t=0;
									foreach($tutorBatchRows as $tutorBatchRow){
										$final_tutor_id6[$t] = $tutorBatchRow->tutor_id;
										$t++;
									}
								}
								//$final_tutor_id_final = $final_tutor_id4;
								
								$tempfinal_tutor_id1 = array_unique($final_tutor_id6);
								$smart1 = array();
								if (isset($tempfinal_tutor_id1) && sizeof($tempfinal_tutor_id1)>0)
								{
									$q=0;
									foreach($tempfinal_tutor_id1 as $v){
										$smart1[$q] =  $v;
										$q++;
									}
								}
								$final_tutor_id_final = $smart1;
						}
//-------------------------------------------------------end of batch search---------------------------------------------
						//---------------------------------------------locality search ---------------------------------------------------
					//echo "locality---".$localityId ;exit;
					//print_r($final_tutor_id_final);exit;
					//echo $fetchSkill_id;exit;
					$final_tutor_id3 = array();
					//$localityId=10;
			//echo "jij".$localityId;exit;
						if (isset($localityId)&& $localityId!='')
						{
//							echo "innnnnn";
							if (isset($final_tutor_id_final) && sizeof($final_tutor_id_final) > 0 && isset($fetchSkill_id) && $fetchSkill_id!="")
							{
								$arrystr = "0";
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
										$arrystr .=",".$final_tutor_id_final[$j];
								}
								$condition = "tutor_id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}		
							//echo $condition;exit;
						//	print_r($final_tutor_id_final);exit;
							$addressfetchallRows = $adressObj->fetchAll($adressObj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'master_address'))
													  ->where("t.parent_address_id!='0'"));
											//print_r($addressfetchallRows);exit;		  	
							$address_id_final = array();
							$i = 0;
						foreach($addressfetchallRows as $addressrowall){
					
						$suburbsId = $addressrowall->suburbs_id;
						$explodedCategory = explode(",",$suburbsId);
						//echo $localityId."--";
						//print_r($explodedCategory);exit;
						if(isset($explodedCategory) && sizeof($explodedCategory)>0){
							
							for($g=0;$g<sizeof($explodedCategory);$g++){
								if(in_array($localityId,$explodedCategory)){
									$address_id_final[$i] = $addressrowall->address_id;
									$i=$i+1;
								}
							}
							
							}
						}
						//print_r($address_id_final);exit;
						$address_id_final = array_unique($address_id_final);
							$addressidsequence = array();
							if (isset($address_id_final) && sizeof($address_id_final)>0)
								{
									$rj=0;
									foreach($address_id_final as $v){
										$addressidsequence[$rj] =  $v;
										$rj++;
									}
								}
								$address_id_final = $addressidsequence;
								
							
							if (isset($address_id_final) && sizeof($address_id_final) > 0)
							{
								$suburbscommasep = "0";
								for($j=0;$j<sizeof($address_id_final);$j++)
								{
									$suburbscommasep .=",".$address_id_final[$j];
								}
							}		
							//echo $suburbscommasep;exit;
							$suburbscondition = "locality in ($suburbscommasep)";	
							$branchdetailObj = new Skillzot_Model_DbTable_Batch();
							$companyaddressData = $branchdetailObj->fetchAll($branchdetailObj->select()
													->from(array('c'=>DATABASE_PREFIX."tx_tutor_course_batch"))
													->where("$suburbscondition && $condition"));
							if(isset($companyaddressData) && sizeOf($companyaddressData)>0){
								$t=0;
								foreach($companyaddressData as $localityfetch){
									$final_tutor_id3[$t] = $localityfetch->tutor_id;
									$t++;
								}
							}
							$tempfinal_tutor_id_suburbs = array_unique($final_tutor_id3);
							//print_r($tempfinal_tutor_id_suburbs);exit;
							
							$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
							$travelradiousData = $travelradiousObj->fetchAll($travelradiousObj->select()
											->from(array('t'=>DATABASE_PREFIX."travel_radious"),array('tutor_id','suburbs_id'))
											->where("t.suburbs_id!='0'"));
							
						//print_r($travelradiousData);exit;//array-1
							if (isset($travelradiousData) && sizeof($travelradiousData)>0)
							{
								$w = 0;
								$temptravelradious = array();
								foreach ($travelradiousData as $travelradious)
								{
								$suburbsId = $travelradious->suburbs_id;
								$explodedCategory = explode(",",$suburbsId);
								
									if(isset($explodedCategory) && sizeof($explodedCategory)>0){
										for($g=0;$g<sizeof($explodedCategory);$g++){
											if(in_array($localityId,$explodedCategory)){
												$temptravelradious[$w] = $travelradious->tutor_id;
												$w=$w+1;
											}
										}
									}
									if (isset($temptravelradious) && sizeof($temptravelradious)>0)
									{
										$temptravelradious = $temptravelradious;
									}
									
									//$temptravelradious[$w] = $travelradious->tutor_id;
									//$w++;
								}
							}else{
								$temptravelradious = $final_tutor_id3;
							}
							//print_r($tempfinal_tutor_id_suburbs);//array-2
							
							$mergetwoarrayofsuburbs = array_merge($final_tutor_id3,$temptravelradious);
							$mergetwoarrayofsuburbs = array_unique($mergetwoarrayofsuburbs);
							//print_r($mergetwoarrayofsuburbs);exit; //array-3
							$suburbssequence = array();
							if (isset($mergetwoarrayofsuburbs) && sizeof($mergetwoarrayofsuburbs)>0)
								{
									$q=0;
									foreach($mergetwoarrayofsuburbs as $v){
										$suburbssequence[$q] =  $v;
										$q++;
									}
								}
								$final_tutor_id_final_temp_lty = array_intersect($final_tutor_id_final, $suburbssequence);
								//$final_tutor_id_final = $suburbssequence;
								
								$e=0;
								foreach($final_tutor_id_final_temp_lty as $varId1){
									if($varId1!="")
									{
										$final_tutor_id_final_ltype[$e] = $varId1;
										$e++;
									}
								}
								
								if (isset($final_tutor_id_final_ltype) && sizeof($final_tutor_id_final_ltype) > 0){
									$final_tutor_id_final = $final_tutor_id_final_ltype;
									//$final_tutor_id_final = $final_tutor_id_final; #rakesh changes
								}
								//print_r($final_tutor_id_final);exit;

						}
						//echo "-------after locality 2--------";
						//echo "<br/>";
						//print_r($final_tutor_id_final);
						//echo "<br/>";
						//exit;
						
//---------------------------------------------lesson type search ---------------------------------------------------
					/*final tutor id send to phtml*/
//						echo $fetchSkill_type;exit;
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0 && isset($fetchSkill_type) && $fetchSkill_type!=""){
							if (isset($final_tutor_id_final) && sizeof($final_tutor_id_final) > 0 && isset($fetchSkill_id) && $fetchSkill_id!="")
							{
								$arrystr = "0";
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								$condition = "t.id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}		
						//print $arrystr;exit;
						$strtutorids = implode(',',$final_tutor_id_final);
						if(isset($strtutorids) && $strtutorids!=""){
							$final_tutor_id2 = array();
							if($fetchSkill_type == 2){
								$fetchSkill_type_in = "2,3,4";
							}elseif($fetchSkill_type == 1){
								$fetchSkill_type_in = "1,3";
							}else{
								$fetchSkill_type_in = "1,2,3,4";
							}
							$tutortypesRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
														  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
														  		->where("id in ($strtutorids) && tutor_class_type in ($fetchSkill_type_in)"));
						//	print $tutorProfileobj->select()
								//						  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
								//						  		->where("id in ($strtutorids) && tutor_class_type in ($fetchSkill_type_in)");
								//echo sizeof($tutortypesRows);exit;						  		
							if(isset($tutortypesRows) && sizeOf($tutortypesRows)>0){
								$t=0;
								foreach($tutortypesRows as $typeId){
									$final_tutor_id2[$t] = $typeId->id;
									$t++;
								}
							}
							//print_r($final_tutor_id2);exit;
							$final_tutor_id_final = $final_tutor_id2;
						}
					}
						//echo $locationId;
					//	echo "-------after lesson type 3--------";
					//	echo "<br/>";
					//	print_r($final_tutor_id_final);
						//echo "<br/>";
						//exit;

//---------------------------------------------end of lesson type search ---------------------------------------------------
//---------------------------------------------location search ---------------------------------------------------
						//echo "ggg".$locationId;
						$final_tutor_id4 = array();
						if (isset($locationId)&& $locationId!="")
						{
							
							if (isset($final_tutor_id_final) && isset($fetchSkill_id) && $fetchSkill_id!="")
							{
								$arrystr = "0";
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								$condition = "tutor_id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}
							
							
							if ($locationId==3)
							{
								$fetch_location_id = "1,2,3";
							}else{
								$fetch_location_id = $locationId;
							}
							//echo $fetch_location_id;exit;
							$tutorlocationRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
																	->where("tutor_lesson_location in ($fetch_location_id) && $condition"));
									
//																	print $tutorCourseobj->select()
//																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
//																	->where("tutor_lesson_location in ($fetch_location_id) && $condition");
//																	exit;
								if(isset($tutorlocationRows) && sizeOf($tutorlocationRows)>0){
									$t=0;
									foreach($tutorlocationRows as $locationId){
										$final_tutor_id4[$t] = $locationId->tutor_id;
										$t++;
									}
								}
								//$final_tutor_id_final = $final_tutor_id4;
								
								$tempfinal_tutor_id = array_unique($final_tutor_id4);
								$smart = array();
								if (isset($tempfinal_tutor_id) && sizeof($tempfinal_tutor_id)>0)
								{
									$q=0;
									foreach($tempfinal_tutor_id as $v){
										$smart[$q] =  $v;
										$q++;
									}
								}
								$final_tutor_id_final = $smart;
						}
						//echo "aa";
					//	echo "------- after location type 4--------";
					//	echo "<br/>";
					//	print_r($final_tutor_id_final);
					//	echo "<br/>";
						//exit;

					//----------------main query-------------------
					//print_r($final_tutor_id_final);
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0){
					//echo "innnnnnnnnnn";
						$strofTutorids = implode(",",$final_tutor_id_final);
						$condition = "t.id in ($strofTutorids)";
						if(isset($tempfinal_tutor_id1) && sizeof($tempfinal_tutor_id1)>0){
							//echo "if";exit;
						if($search_tutor!="" && $locality_tutor==""){
							if($tutor_id_for_search !="")
							{
								$condition = "t.id in ($tutor_id_for_search)";	
							}else{
								$condition = "t.id in ($strofTutorids)";
							}
							
						}elseif($search_tutor!="" && $locality_tutor!=""){
							if($tutor_id_for_search !="")
							{
								$condition = "t.id in ($tutor_id_for_search_locality)";
							}else{
								$condition = "t.id in ($strofTutorids)";
							}

						}elseif($search_tutor=="" && $locality_tutor!=""){
							if($tutor_id_for_search !="")
							{
								$condition = "t.id in ($tutor_id_for_locality)";
							}else{
								$condition = "t.id in ($strofTutorids)";
							}

						}
						else{
							$condition = "t.id in ($strofTutorids)";
						}
					}else{
						$condition = "t.id in ($strofTutorids)";
					}

						
						if ($fetchSkill_id=='13' || $fetchSkill_id=='14' || $fetchSkill_id=='4' || $fetchSkill_id=='6'){
						//echo "innn";
						$tutoridsRows4 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		//->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));
											  		->order(array("t.trustmeter desc")));
											  		//print_r($tutoridsRows4);exit;
						}else{
							$tutoridsRows4 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		//->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));
													->order(array("t.trustmeter desc")));
													/*print $tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","e.qualification_count DESC","t.id ASC"));exit; */
						}
					
						$final_tutor_id_final =	$tutoridsRows4;
						
						$this->view->tutoridsRowresultforpaging = $final_tutor_id_final;
					}
					//echo $condition;exit;
					
					//-----------------------------------------------
					//echo "<br/>";
					//echo "final array";
					//echo "<br/>";
					//print_r($final_tutor_id_final);exit;
					
						$page = $this->_request->getParam('page',1);
						//$this->view->page = $page;
						$records_per_page = 10;
						$record_count = sizeof($final_tutor_id_final);
						$paginator = Zend_Paginator::factory($final_tutor_id_final);
						$paginator->setItemCountPerPage($records_per_page);
						$paginator->setCurrentPageNumber($page);
						$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
						$this->view->tutoridsRowresult = $paginator;
						$page_number  = $record_count / 1;
						$page_number_last =  floor($page_number);			
					
			}
		//---------------end of new search--------------------------------------------------
		//-------------------------------------------------	
			
	
		/*fetch category values to display in drop down*/
					
					

					
	}
													 
		$skillObj = new Skillzot_Model_DbTable_Skills();
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		$tutorprofileObj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$addresstable = new Skillzot_Model_DbTable_Address();
		$batchObj= new Skillzot_Model_DbTable_Batch();
		$skillmapobj = new Skillzot_Model_DbTable_SkillMap();
		if($this->_request->isPost()){
				
			
				$keywords1 = $this->_request->getParam("keywords");
				$keywords=trim($keywords1);
					//$keywords=mysql_real_escape_string($keywords2);
					//$authUserNamespace->search_keyword=$keywords;
					//echo $keywords;exit;
				$search_for_skill = $skilldisplayObj->fetchRow($skilldisplayObj->select()
					->setIntegrityCheck(false)
					->from(array("r"=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
					->where("skill_name LIKE '%".addslashes($keywords)."%' || skill_uniq LIKE '%".addslashes($keywords)."%' "));

				$search_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
					->setIntegrityCheck(false)
					->distinct()
					->from(array("a"=>DATABASE_PREFIX.'tx_tutor_profile'))
					->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".addslashes($keywords)."%') || a.tutor_first_name like '%".addslashes($keywords)."%' || a.tutor_last_name like '".addslashes($keywords)."%') && a.is_active='0'"));
				//echo $search_for_tutor->id;exit;
				$search_for_tutor1 = $tutorprofileObj->fetchRow($tutorprofileObj->select()
					->setIntegrityCheck(false)
					->distinct()
					->from(array("a"=>DATABASE_PREFIX.'tx_tutor_profile'))
					->where("a.company_name like '%".addslashes($keywords)."%' && a.is_active='0'"));

				//echo "if";exit;
				if($search_for_skill!="" && $search_for_tutor=="" && $search_for_tutor1=="")
				{
					$final_skill_search=$search_for_skill->skill_uniq;
					$skill_map_result=$search_for_skill->skill_uniq;
				}elseif($search_for_skill=="" && $search_for_tutor!="" && $search_for_tutor1=="")
				{
					$query_for_tutor = $tutorCourseobj->fetchRow($tutorCourseobj->select()
							   ->setIntegrityCheck(false)
							   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('distinct(GROUP_CONCAT(a.tutor_skill_id)) as tutor_skill_id'))
							   ->where("a.tutor_id='".$search_for_tutor->id."'"));
					$final_skill_id=$query_for_tutor->tutor_skill_id;
					$string = str_replace(',', ' ', $final_skill_id);
					$string_for_skill_id=trim($string);
					//echo "'".$string_for_skill_id."'";exit;
					//print_r(trim($final_skill_id, ','));exit;
					if(isset($string_for_skill_id) && $string_for_skill_id!="")
					{
					$skill_id_for_tutor = explode(" ",$string_for_skill_id);
					//print_r($skill_id_for_tutor);exit;
					//tutor_skill
					$tutor_skill=$skilldisplayObj->fetchRow($skilldisplayObj->select()
							   ->where("skill_id='".$skill_id_for_tutor[0]."' && skill_id!='0' && is_skill='1'"));
						if($tutor_skill->skill_uniq!="")
						{
							$skill_finalname=$tutor_skill->skill_uniq;
						}else{
							for($i=0;$i<=sizeof($skill_id_for_tutor);$i++) {
								$tutor_skill1=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  	 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0' && is_skill='1'"));
								if($tutor_skill1->skill_uniq!="")
								{
									$skill_finalname=$tutor_skill1->skill_uniq;
									break;
								}else{
									$tutor_skill2=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  		 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0'"));
									if($tutor_skill2->skill_uniq!="")
									{
										$tutor_skill3=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  			 ->where("skill_id='".$tutor_skill2->parent_skill_id."' && skill_id!='0' && is_skill='1'"));
										$skill_finalname=$tutor_skill3->skill_uniq;
										break;
									}
									
								}
							}
						}
					}
					//echo $tutor_skill->skill_uniq;exit;		
					$final_search_keyword1=$keywords;
					$skill_map_result="";		
				}elseif($search_for_skill=="" && $search_for_tutor=="" && $search_for_tutor1!="")
				{
						$query_for_tutor = $tutorCourseobj->fetchRow($tutorCourseobj->select()
								   ->setIntegrityCheck(false)
								   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_skill_course"),array('distinct(GROUP_CONCAT(a.tutor_skill_id)) as tutor_skill_id'))
								   ->where("a.tutor_id='".$search_for_tutor1->id."'"));
						$final_skill_id=$query_for_tutor->tutor_skill_id;
						$string = str_replace(',', ' ', $final_skill_id);
						$string_for_skill_id=trim($string);
						//echo "'".$string_for_skill_id."'";exit;
						//print_r(trim($final_skill_id, ','));exit;
						if(isset($string_for_skill_id) && $string_for_skill_id!="")
						{
						$skill_id_for_tutor = explode(" ",$string_for_skill_id);
						//print_r($skill_id_for_tutor);exit;
						//tutor_skill
						$tutor_skill=$skilldisplayObj->fetchRow($skilldisplayObj->select()
							   ->where("skill_id='".$skill_id_for_tutor[0]."' && skill_id!='0' && is_skill='1'"));
						if($tutor_skill->skill_uniq!="")
						{
							$skill_finalname=$tutor_skill->skill_uniq;
						}else{
							for($i=0;$i<=sizeof($skill_id_for_tutor);$i++) {
								$tutor_skill1=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  	 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0' && is_skill='1'"));
								if($tutor_skill1->skill_uniq!="")
								{
									$skill_finalname=$tutor_skill1->skill_uniq;
									break;
								}else{
									$tutor_skill2=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  		 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0'"));
									if($tutor_skill2->skill_uniq!="")
									{
										$tutor_skill3=$skilldisplayObj->fetchRow($skilldisplayObj->select()
								  			 ->where("skill_id='".$tutor_skill2->parent_skill_id."' && skill_id!='0' && is_skill='1'"));
										$skill_finalname=$tutor_skill3->skill_uniq;
										break;
									}
									
								}
							}
						}
						}
						//echo $tutor_skill->skill_uniq;exit;		
						$final_search_keyword1=$keywords;
						$skill_map_result="";
				}else
				{
					$keyword_unique = explode(" ", $keywords);
					//echo sizeof($keyword_unique);exit;
					$list = array("in","is","it","a","the","of","or","I","you","he","me","us","they","she","to","but","that","this","those","then","has","and");
					
					#.............skill search ..................#

					$list_arr = array();
					foreach($keyword_unique as $key){
						if (in_array($key, $list)) {
						$list_arr[] = $key;
						//print_r($list_arr);
						}
					}
					$result = array_diff($keyword_unique, $list_arr);
					$final_search_keyword=implode(" ",$result);
					$word_for_search = explode(" ", $final_search_keyword);
					//print_r($word_for_search);exit;
					$tmp_skill_name=" ";
					$tmp_skill_name1=" ";
					$tmp_skill_name_acq=" ";
					$tmp_skill_name_mat=" ";
					for($i=0;$i<sizeof($word_for_search); $i++)
					{
						$skillrowResult1 = $skilldisplayObj->fetchRow($skilldisplayObj->select()
								   ->where("skill_name LIKE '%".$word_for_search[$i]."%' || skill_uniq LIKE '%".$word_for_search[$i]."%' && skill_id!='0' "));
							
						//echo $skillrowResult1->skill_uniq;exit;
						if(isset($skillrowResult1) && sizeof($skillrowResult1)>0)
						{
							//echo "if";exit;
							$tmp_skill_name=$tmp_skill_name." ".$word_for_search[$i];
							$tmp_skill_name_acq=$tmp_skill_name_acq." ".$skillrowResult1->skill_uniq;
							$tmp_skill_name_mat=$tmp_skill_name_mat." ".$skillrowResult1->skill_name;
							$tmp_skill_serach=$tmp_skill_name1.$skillrowResult1->skill_uniq;
							
						}else{
							//echo "else";exit;
							$skillrowResult = $skillmapobj->fetchRow($skillmapobj->select()
								   ->where("skill_name like '%".$word_for_search[$i]."%' || skill_entered like '%".$word_for_search[$i]."%'"));
							if(isset($skillrowResult) && sizeof($skillrowResult)>0)
							{
								//echo "if";exit;
							$tmp_skill_name=$tmp_skill_name." ".$word_for_search[$i];
							$tmp_skill_name_acq=$tmp_skill_name_acq." ".$skillrowResult->skill_name;
							$tmp_skill_name_mat=$tmp_skill_name_mat." ".$skillrowResult1->skill_uniq;
							$tmp_skill_serach=$tmp_skill_name1.$skillrowResult->skill_name;
							}
						}	
					}
					//skill
					//$skill_search=$tmp_skill_serach;
					//$final_skill_search=trim($skill_search);
					$final_skill_name=trim($tmp_skill_name);
					//$final_skill_name_acq=$tmp_skill_name_acq;
					//echo "ist".$final_skill_name;exit;
					if(isset($final_skill_name) && $final_skill_name!="")
					{
						$skillrowResult1 = $skilldisplayObj->fetchRow($skilldisplayObj->select()
								   ->where("skill_name LIKE '%".$final_skill_name."%' || skill_uniq LIKE '%".$final_skill_name."%' && skill_id!='0' "));
						if(isset($skillrowResult1) && sizeof($skillrowResult1)>0)
						{
						$skill_search=$skillrowResult1->skill_uniq;
						$final_skill_search=trim($skill_search);
						$final_skill_name=$final_skill_name;
						$final_skill_name_mat=$skillrowResult1->skill_name;
						$final_skill_name_acq=$skillrowResult1->skill_uniq;
						//echo $final_skill_search."and".$final_skill_name."and".$final_skill_name_acq;exit;
						}else{
						$skill_search=$tmp_skill_serach;
					    $final_skill_search=trim($skill_search);
						$final_skill_name=trim($tmp_skill_name);
						$final_skill_name_mat=trim($tmp_skill_name_mat);
						$final_skill_name_acq=$tmp_skill_name_acq;
						//echo $final_skill_search."and".$final_skill_name."and".$final_skill_name_acq;exit;
						}
					}
					if(($final_skill_name==$final_skill_name_acq) || ($final_skill_name==$final_skill_name_mat))
					{
						//echo "if";exit;
						$skill_map_result=$final_skill_name;

					}else{
						$skill_map_result="";
					}
					
					//echo $authUserNamespace->tmp_skill_serach;exit;
					//echo "final='".trim($skill_search)."'";exit;
					#.............end of skill search ..................#

					#.............locality search ..................#

					if(isset($final_skill_name) && $final_skill_name!="")
					{
						$locality_diff= explode(" ",trim($final_skill_name));
					}else{
						$locality_diff= " ";
					}
					
					//print_r($locality_diff);exit;
					$list_arr2 = array();
					foreach($word_for_search as $key){
						if (in_array($key, $locality_diff)) {
						$list_arr2[] = $key;
						//print_r($list_arr1);
						}
					}
					$result2 = array_diff($word_for_search, $list_arr2);
					//print_r($result2);exit;
					$final_search_keyword2=implode(" ",$result2);
					$word_for_search2 = explode(" ",$final_search_keyword2);
					if(isset($result2) && sizeof($result2)>0)
					{
					$tmp_locality=" ";
					$tmp_locality1=" ";
					for($i=0;$i<sizeof($word_for_search2); $i++)
					{
												
						$address_row=$addresstable->fetchRow($addresstable->select()
									->setIntegrityCheck(false)
									->from(array('a'=>DATABASE_PREFIX."master_address"),array('GROUP_CONCAT(a.address_id) as address_id'))
									->where("address_value like '%".$word_for_search2[$i]."%'"));
						if(isset($address_row->address_id) && sizeof($address_row->address_id)>0)
						{
							//echo "if";exit;
							$tmp_locality=$tmp_locality.$address_row->address_id;
							$tmp_locality1=$tmp_locality1." ".$word_for_search2[$i];
						}	
					}
					$final_address=$tmp_locality;
					$final_address_id=trim($final_address);
					$final_address_value=$tmp_locality1;
					$string_for_final_address_id=trim($final_address_id);
					$string_for_final_address_value=trim($final_address_value);
					//echo "'".$final_address_id."'";exit;
					}



					#.............end of locality search ..................#

					#.............tutor search ..................#

					if(isset($string_for_final_address_id) && $string_for_final_address_id !="")
					{
						$result_for_skill_and_locality = $final_skill_name." ".$string_for_final_address_value;
					}else{
						$result_for_skill_and_locality = $final_skill_name;
					}
					if(isset($result_for_skill_and_locality) && $result_for_skill_and_locality!="")
					{
						$skill_diff= explode(" ",trim($result_for_skill_and_locality));
					}else{
						$skill_diff= " ";
					}
					

					$list_arr1 = array();
					foreach($word_for_search as $key){
						if (in_array($key, $skill_diff)) {
						$list_arr1[] = $key;
						//print_r($list_arr1);
						}
					}

					$result1 = array_diff($word_for_search, $list_arr1);
					//print_r($result1);exit;
					$final_search_keyword1=implode(" ",$result1);
					//echo $final_search_keyword1;exit;
					$tmp_skill_id=" ";
					if(isset($final_search_keyword1) && $final_search_keyword1!="")
					{
						$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
								   ->setIntegrityCheck(false)
								   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array(''))
								   ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_skill_course"),'a.id=b.tutor_id',array('distinct(GROUP_CONCAT(b.tutor_skill_id)) as tutor_skill_id'))
								   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$final_search_keyword1."%') || a.company_name like '%".$final_search_keyword1."%') && a.is_active='0'"));
						if(isset($query_for_tutor->tutor_skill_id) && sizeof($query_for_tutor->tutor_skill_id)>0)
						{
							//echo "if";exit;
							$tmp_skill_id=$tmp_skill_id.",".$query_for_tutor->tutor_skill_id;
						}
						else{
							//echo "else";exit;
						$word_for_search1 = explode(" ",$final_search_keyword1);
						for($i=0;$i<sizeof($word_for_search1); $i++)
						{
						$query_for_tutor = $tutorprofileObj->fetchRow($tutorprofileObj->select()
								   ->setIntegrityCheck(false)
								   ->from(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),array(''))
								   ->joinLeft(array('b'=>DATABASE_PREFIX."tx_tutor_skill_course"),'a.id=b.tutor_id',array('distinct(GROUP_CONCAT(b.tutor_skill_id)) as tutor_skill_id'))
								   ->where("((CONCAT(a.tutor_first_name,' ',a.tutor_last_name) like '%".$word_for_search1[$i]."%') || a.company_name like '%".$word_for_search1[$i]."%') && a.is_active='0'"));
						
							if(isset($query_for_tutor->tutor_skill_id) && sizeof($query_for_tutor->tutor_skill_id)>0)
							{
								$tmp_skill_id=$tmp_skill_id.",".$query_for_tutor->tutor_skill_id;
							}	
						}
					}
					}
					if(isset($tmp_skill_id) && $tmp_skill_id!="")
					{
						$final_skill_id=$tmp_skill_id;
						$string = str_replace(',', ' ', $final_skill_id);
						$string_for_skill_id=trim($string);
						//echo "'".$string_for_skill_id."'";exit;
						//print_r(trim($final_skill_id, ','));exit;
						if(isset($string_for_skill_id) && $string_for_skill_id!="")
						{
							$skill_id_for_tutor = explode(" ",$string_for_skill_id);
							//print_r($skill_id_for_tutor);exit;
							//tutor_skill
							$tutor_skill=$skilldisplayObj->fetchRow($skilldisplayObj->select()
							   ->where("skill_id='".$skill_id_for_tutor[0]."' && skill_id!='0' && is_skill='1'"));
							if($tutor_skill->skill_uniq!="")
							{
								$skill_finalname=$tutor_skill->skill_uniq;
							}else{
								for($i=0;$i<=sizeof($skill_id_for_tutor);$i++) {
									$tutor_skill1=$skilldisplayObj->fetchRow($skilldisplayObj->select()
									  	 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0' && is_skill='1'"));
									if($tutor_skill1->skill_uniq!="")
									{
										$skill_finalname=$tutor_skill1->skill_uniq;
										break;
									}else{
										$tutor_skill2=$skilldisplayObj->fetchRow($skilldisplayObj->select()
									  		 ->where("skill_id='".$skill_id_for_tutor[$i]."' && skill_id!='0'"));
										if($tutor_skill2->skill_uniq!="")
										{
											$tutor_skill3=$skilldisplayObj->fetchRow($skilldisplayObj->select()
									  			 ->where("skill_id='".$tutor_skill2->parent_skill_id."' && skill_id!='0' && is_skill='1'"));
											$skill_finalname=$tutor_skill3->skill_uniq;
											break;
										}
										
									}
								}
							}
							}
					}
					//echo $skill_id_for_tutor[0];exit;
				}
				   unset($authUserNamespace->maincategorySession);
				   unset($authUserNamespace->skillidnew);
				   unset($authUserNamespace->lessiontypeSession);
				   unset($authUserNamespace->locationSession);
				   unset($authUserNamespace->localitySession);
				   unset($authUserNamespace->genderSession);
				   unset($authUserNamespace->skillidSession);

					  if ($final_search_keyword1!="" && $final_skill_search!="" && $string_for_final_address_value!="")
					   {
					   		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?search=".$final_search_keyword1."&locality=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);	
					   }
				      elseif($final_search_keyword1!="" && $final_skill_search=="" && $string_for_final_address_value=="")
					    {
					    	if($tutor_skill->skill_uniq=="")
					    	{
					    		$tutor_skill->skill_uniq="music";
					    	}
					   		$tuturIdlink = "/search/".$skill_finalname."-classes-in-mumbai?search=".$final_search_keyword1."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1!="" && $final_skill_search!="" && $string_for_final_address_value=="")
					    {
					   		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?search=".$final_search_keyword1."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1!="" && $final_skill_search=="" && $string_for_final_address_value!="")
					    {
					   		$tuturIdlink = "/search/".$skill_finalname."-classes-in-mumbai?search=".$final_search_keyword1."&locality=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1=="" && $final_skill_search!="" && $string_for_final_address_value!="")
					    {
					   		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?locality=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  elseif($final_search_keyword1=="" && $final_skill_search=="" && $string_for_final_address_value!="")
					    {
					   		$tuturIdlink = "/search/music-classes-in-mumbai?locality=".$string_for_final_address_value."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					    }
					  else{
					  		$tuturIdlink = "/search/".$final_skill_search."-classes-in-mumbai?skill=".$final_skill_search."&search_keywords=".$keywords."&skill_keywords=".$skill_map_result;
							$this->_redirect($tuturIdlink);
					  }								  // print_r($skillrowResult);exit;


				//}
			}
		
	}
	
public function viewimageAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
	 	$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
	 	
	 	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
	 	$img_id=$this->_request->getParam("id");
	 	$view_img=$tutorProfileobj->fetchRow("id='$img_id'");
	 	if (isset($view_img->tutor_image_type) && $view_img->tutor_image_type!="" ){
	 		print $content = $view_img->tutor_image_content;exit;
	 	}
		
	}
	public function viewprofilethumbAction(){//displaying main imageee
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
	 	$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
	 	
	 	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
	 	$img_id=$this->_request->getParam("id");
	 	$view_img=$tutorProfileobj->fetchRow("id='$img_id'");
	 	if (isset($view_img->tutor_image_type) && $view_img->tutor_image_type!="" ){
	 		print $content = $view_img->tutor_image_content;exit;
	 	}
		
	}
	
	
public function viewthumbimageAction(){
	
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
	 	$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
	 	$img_id=$this->_request->getParam("id");
	 	$view_img=$tutorProfileobj->fetchRow("id='$img_id'");
	 	if (isset($view_img->tutor_thumb_type) && $view_img->tutor_thumb_type!="" ){
	 		print $content = $view_img->tutor_thumb_content;exit;
	 	}
		
	}
	public function tutorcontactAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();	
		$tutorcourseObj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$tutorAnalyticsobj = new Skillzot_Model_DbTable_Analytics();
		$jobtableobj = new Skillzot_Model_DbTable_Jobtable();
		$checklistobj = new Skillzot_Model_DbTable_Checklist();
		$logobj = new Skillzot_Model_DbTable_Log();
		$this->_helper->layout()->disableLayout();
		$urlValueformessage = $this->_request->getParam("url");
		if (isset($urlValueformessage) && $urlValueformessage!="")
		{
			$this->view->urlValueformessage = $urlValueformessage;
		}
		$TypeId = $this->_request->getParam("typeid");
	 	$TutorId = $this->_request->getParam("tutorid");
	 	$contact_type = $this->_request->getParam("contact");
	 	$this->view->contact=$contact_type;
	 	$date1=date('Y-m-d');
		$fetch_data = $tutorcourseObj->fetchRow("tutor_id='$TutorId'");
		$courseid=$fetch_data->id;
		if($courseid!="" && $courseid!="NULL")
		{
	 	if(isset($contact_type) && $contact_type =="email")
	 	{	
			$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
											->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
		
			if (isset($resultData) && ($resultData > 0))
			{
				$count=$resultData->message_click_count;
				$data1 = array("message_click_count"=>$count+1);
				$tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
			}
			else
			{
				$count="1";
				$data = array("date"=>$date1,"tutor_id"=>$TutorId,"course_id"=>$courseid,"message_click_count"=>$count);
				$tutorAnalyticsobj->insert($data);
			}
		}
		if(isset($contact_type) && $contact_type =="phone")
		{			
			
			$resultData = $tutorAnalyticsobj->fetchrow($tutorAnalyticsobj->select()
											->from(array('c'=>DATABASE_PREFIX."tx_tutor_analytics"))
											->where("c.tutor_id='$TutorId' && c.date='$date1' && c.course_id='$courseid'"));
			if (isset($resultData) && ($resultData > 0))
			{
				$count=$resultData->phone_click_count;
				$data1 = array("phone_click_count"=>$count+1);
				$tutorAnalyticsobj->update($data1,"tutor_id='$TutorId' && date='$date1' && course_id='$courseid'");
			}
			else
			{
				$count="1";
				$data = array("date"=>$date1,"tutor_id"=>$TutorId,"course_id"=>$courseid,"phone_click_count"=>$count);
				$tutorAnalyticsobj->insert($data);
			}
		}
		}
	 		if(isset($TutorId) && $TutorId!=""){
	 			$this->view->tutorid = $TutorId;
	 			$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
													 	 ->where("id='$TutorId'"));
				$this->view->tutorname = ucfirst($tutorProfile_row->tutor_first_name)." ".ucfirst($tutorProfile_row->tutor_last_name);
				
				if (isset($tutorProfile_row->tutor_mobile) && $tutorProfile_row->tutor_mobile){
					$this->view->mobileval = $tutorProfile_row->tutor_mobile;
				}
	 			if (isset($tutorProfile_row->tutor_landline) && $tutorProfile_row->tutor_landline){
					$this->view->landlineval = $tutorProfile_row->tutor_landline;
				}
	 			if (isset($tutorProfile_row->phone_visible) && $tutorProfile_row->phone_visible){
					$this->view->phonevisible = $tutorProfile_row->phone_visible;
				}
				if (isset($tutorProfile_row->extension) && $tutorProfile_row->extension){
					$this->view->extension = $tutorProfile_row->extension;
				}
	 		}
	 		//$authUserNamespace->studentid=="1";
	 		if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
	 			$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
				$studentData =$studentSignupObj->fetchRow("id='$authUserNamespace->studentid'");
				$this->view->phonenumber = $studentData->std_phone;
				$studentFullname = ucfirst($studentData->first_name);
				$authUserNamespace->studentfullname = $studentFullname;
	 		}
	 		
		 	$adresstypeObj = new Skillzot_Model_DbTable_Addresstype();
			$adressObj = new Skillzot_Model_DbTable_Address();
		
				$cityid = "12";
				$address_rows = $adressObj->fetchAll($adressObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_address"))
															  ->where("s.address_id!='0' && s.parent_address_id='$cityid'")
															  ->order(array("s.address_value asc")));
				//print_r($address_rows);exit;
				if (isset($address_rows) && sizeof($address_rows) > 0){
					$this->view->addressresults = $address_rows;
				}
	 			
	 		
	 		
			if($this->_request->isPost()){
				$skillsLearn = $this->_request->getParam("skillstolearn");
				$level = $this->_request->getParam("level");
				$timeDays = $this->_request->getParam("timeanddays");
				$tutorLocation = $this->_request->getParam("tutorlocation");
				$messageTutor = $this->_request->getParam("messagetutor");
				$TutorId = $this->_request->getParam("tutorId");
				$phoneNumber = $this->_request->getParam("ph_number");
				$contactBy = $this->_request->getParam("tutorcntby");
				$Locality = $this->_request->getParam("UserLocality");
				
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
						
//						if($skillsLearn == "")$response["data"]["skillstolearn"] = "null";
//						else $response["data"]["skillstolearn"] = "valid";
						
//						if($skillsLearn == "")$response["data"]["skillstolearn"] = "null";
//						else $response["data"]["skillstolearn"] = "valid";

						if($Locality == "")$response["data"]["UserLocality"] = "selectnull";
						else $response["data"]["UserLocality"] = "valid";
						
						if($messageTutor == "")$response["data"]["messagetutor"] = "null";
						else $response["data"]["messagetutor"] = "valid";
						
						if($phoneNumber == "" && $contactBy=="Phone")$response["data"]["tutorcntby"] = "null";
						elseif((!is_numeric($phoneNumber) || $phoneNumber=="0") && $contactBy=="Phone")$response["data"]["tutorcntby"] = "mobileinvalid";
						else $response["data"]["tutorcntby"] = "valid";
										
//						if($timeDays == "")$response["data"]["timeanddays"] = "null";
//						else $response["data"]["timeanddays"] = "valid";
						
//						if($tutorLocation == "")$response["data"]["tutorlocation"] = "null";
//						else $response["data"]["tutorlocation"] = "valid";
						
//						if($messageTutor == "")$response["data"]["messagetutor"] = "null";
//						else $response["data"]["messagetutor"] = "valid";
	
						if(!in_array('null',$response['data']) && !in_array('selectnull',$response['data']) && !in_array('mobileinvalid',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						echo json_encode($response);
					}
					else {
						$lastupdatedate = date("Y-m-d H:i:s");
						$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
						$fetch_data = $tutorProfileobj->fetchRow("id='$TutorId'");
						if (isset($fetch_data) && sizeof($fetch_data)>0)
						{
							$tutoremailId = $fetch_data->tutor_email;
							$tutorFullname = ucfirst($fetch_data->tutor_first_name)." ".ucfirst($fetch_data->tutor_last_name);
							$tutorFirstname = ucfirst($fetch_data->tutor_first_name);
							$authUserNamespace->tutorfullname = $tutorFullname;
							$authUserNamespace->tutorId = $tutorId;
							$companyName = $fetch_data->company_name;
							$tutorMobile = $fetch_data->tutor_mobile;
							
							$toName = $tutorFirstname;
						}
//						if (isset($companyName) && $companyName!="")
//							{
//								$toName = $companyName." "."(".$tutorFullname.")";
//							}else{
//								$toName = $tutorFirstname;
//							}
						if (isset($authUserNamespace->logintype) && $authUserNamespace->logintype=='2')
						{
							$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
							$studentsignup_row = $studentSignupObj->fetchRow($studentSignupObj->select()
												 	 	  ->where("id='$authUserNamespace->studentid'"));
							$fromFullname = ucfirst($studentsignup_row->first_name)." ".$studentsignup_row->last_name ;
							$fromFirstname = ucfirst($studentsignup_row->first_name);
							$studentEmail = $studentsignup_row->std_email;
						}
						else if(isset($authUserNamespace->logintype) && $authUserNamespace->logintype=='1')
						{
							if (isset($fetch_data) && sizeof($fetch_data)>0)
							{
								$fetch_data = $tutorProfileobj->fetchRow("id='$authUserNamespace->maintutorid'");
								$fromFullname = ucfirst($fetch_data->tutor_first_name)." ".ucfirst($fetch_data->tutor_last_name);
								$fromFirstname = ucfirst($fetch_data->tutor_first_name);
								$studentEmail = $fetch_data->tutor_email;
							}
						}
						//-----------------------internal mail functionality------------------
						if (isset($messageTutor) && $messageTutor!=""){
							$lastupdatedate = date("Y-m-d H:i:s");
							$lastupdate = date("Y-m-d h:i:s");
							$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
							if (isset($authUserNamespace->logintype) && $authUserNamespace->logintype=='2')
							{
								$data1 = array("from_id"=>$authUserNamespace->studentid,"to_id"=>$TutorId,"parent_id"=>"0",
								"send_flag"=>"s","rec_flag"=>"t","mail_content"=>$messageTutor,"subject"=>"A new inquiry for you","mail_date"=>$lastupdatedate);
								$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdate,"type"=>"STUDENT_LEAD","tutor_id"=>$TutorId,"student_id"=>$authUserNamespace->studentid);

							}else{
								$data1 = array("from_id"=>$authUserNamespace->maintutorid,"to_id"=>$TutorId,"parent_id"=>"0",
								"send_flag"=>"t","rec_flag"=>"t","mail_content"=>$messageTutor,"subject"=>"A new inquiry for you","mail_date"=>$lastupdatedate);
								$data_for_job = array("state"=>"NEW","reason"=>"NULL","date_new"=>$lastupdate,"date_first_followup"=>"NULL","date_last_followup"=>"NULL",
								"date_close"=>"NULL","date_next_followup"=>$lastupdate,"type"=>"STUDENT_LEAD","tutor_id"=>$TutorId,"student_id"=>"NULL");
							}
							//---------------sms integration code---------------------
							
							if (isset($phoneNumber) && $phoneNumber!="" && isset($contactBy) && ($contactBy=="Phone" || $contactBy=="Mail or Phone"))
							{
									
									
								//-------------------------------sms to tutor-------------------------------
									$username='madonlinepltd';
									ini_set("display_errors",1);
								    $password ='Onlinehttp';
								    $senderid = 'SKLZOT';
								    $udh = '0';
								    $dlrMask = '19';								  	
								    $mobileno =	$tutorMobile;
									$message = "Hi ".$toName.", you would be super excited to know that ".$fromFirstname.", reachable on ".$phoneNumber." , just sent us an inquiry for your class. 
									Skillzot will ensure we share your information with ".$fromFirstname."& help them enroll online. 
									Get in touch with us at 09820921404 pronto if your profile is not up to date. We wouldn’t want to miss an opportunity to change someone’s life.";
									$url = 'http://www.myvaluefirst.com/smpp/sendsms';
									$fields = array(
												'username' => urlencode($username),
												'password' => urlencode($password),
												'to' => urlencode($mobileno),
												'udh' => urlencode($udh),
												'from' => urlencode($senderid),
												'text' => urlencode($message),
												'dlr-mask' => urlencode($dlrMask)
									);
									$fields_string="";
									foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
									$fields_string = rtrim($fields_string, '&');
									$ch = curl_init();
									curl_setopt($ch,CURLOPT_URL, $url);
									curl_setopt($ch,CURLOPT_POST, count($fields));
									curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
									$result = curl_exec($ch);
									curl_close($ch);
									
															
									//---------------mail to client-----------------------------
									$message = "Hi Sandeep, a new enquiry from ".$fromFirstname.", number - ".$phoneNumber."";
								    $dlrMask = '19';
								    $mobileno =	'9820305510';
									$url = 'http://www.myvaluefirst.com/smpp/sendsms';
									$fields = array(
												'username' => urlencode($username),
												'password' => urlencode($password),
												'to' => urlencode($mobileno),
												'udh' => urlencode($udh),
												'from' => urlencode($senderid),
												'text' => urlencode($message),
												'dlr-mask' => urlencode($dlrMask)
									);
									$fields_string="";
									foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
									$fields_string = rtrim($fields_string, '&');
									$ch = curl_init();
									curl_setopt($ch,CURLOPT_URL, $url);
									curl_setopt($ch,CURLOPT_POST, count($fields));
									curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
									$result = curl_exec($ch);
									curl_close($ch);
									//-----------------------------------------------------------
									
									//-------------sms to student----------------------
									$message1 = "Hi ".$fromFirstname.", your message from skillzot.com has reached the teacher without exploding. Someone will get back to you within 1 working day. Till then, have fun.";
								    $dlrMask = '19';
								   // $dlrUrl = 'http://skillzot1.com/tutor/profile';
								   // $encodeUrl = urlencode($dlrUrl);
								    $mobileno =	$phoneNumber;
									$url = 'http://www.myvaluefirst.com/smpp/sendsms';
									$fields = array(
												'username' => urlencode($username),
												'password' => urlencode($password),
												'to' => urlencode($mobileno),
												'udh' => urlencode($udh),
												'from' => urlencode($senderid),
												'text' => urlencode($message1),
												'dlr-mask' => urlencode($dlrMask)
									);
									$fields_string="";
									foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
									$fields_string = rtrim($fields_string, '&');
									$ch = curl_init();
									curl_setopt($ch,CURLOPT_URL, $url);
									curl_setopt($ch,CURLOPT_POST, count($fields));
									curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
									$result = curl_exec($ch);
									curl_close($ch);
							}else{
								
							}
							
							//---------------end of sms integration code---------------
							$mailMessagesObj->insert($data1);
							$jobtableobj->insert($data_for_job);
							$jobSessionId = $jobtableobj->getAdapter()->lastInsertId();
							$lastupdate = date("Y-m-d h:i:s");

							$erp_step2_due_date = date('Y-m-d h:i:s', strtotime('+3 days'));
							$erp_step3_due_date = date('Y-m-d h:i:s', strtotime('+7 days'));
							$erp_step4_due_date = date('Y-m-d h:i:s', strtotime('+10 days'));
							$tutor_feedback_due_date = date('Y-m-d h:i:s', strtotime('+30 days'));
							//echo "job_id=".$jobSessionId;exit;
							
							$data_for_checklist1= array("job_id"=>$jobSessionId,"task"=>"TUTOR_RECO","date_due"=>$lastupdate,"date_created"=>$lastupdate,
							"date_done"=>"NULL","done_flag"=>"0");
							$data_for_checklist2= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_1","date_due"=>$lastupdate,"date_created"=>$lastupdate,
							"date_done"=>"NULL","done_flag"=>"0");
							$data_for_checklist3= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_2","date_due"=>$erp_step2_due_date,"date_created"=>$lastupdate,
							"date_done"=>"NULL","done_flag"=>"0");
							$data_for_checklist4= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_3","date_due"=>$erp_step3_due_date,"date_created"=>$lastupdate,
							"date_done"=>"NULL","done_flag"=>"0");
							$data_for_checklist5= array("job_id"=>$jobSessionId,"task"=>"ERP_STEP_4","date_due"=>$erp_step4_due_date,"date_created"=>$lastupdate,
							"date_done"=>"NULL","done_flag"=>"0");
							$data_for_checklist6= array("job_id"=>$jobSessionId,"task"=>"TUTOR_FEEDBACK","date_due"=>$tutor_feedback_due_date,"date_created"=>$lastupdate,
							"date_done"=>"NULL","done_flag"=>"0");

							$studentdetailsObj = new Skillzot_Model_DbTable_Studentsignup();
							$studentdetailsResult = $studentdetailsObj->fetchRow($studentdetailsObj->select()
												->from(array('a'=>DATABASE_PREFIX."tx_student_tutor"))
												->where("a.id='$authUserNamespace->studentid'"));
							$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
							$tutordetailsResult = $tutorProfileobj->fetchRow("id='$TutorId'");
							if(isset($tutordetailsResult->tutor_landline) && $tutordetailsResult->tutor_landline !="")
							{
								$land_number="/".$tutordetailsResult->tutor_landline;
							}else{
								$land_number="";
							}

							$entry_for_student="New Job created by SERPA,<br>Details:<br>Student Name: ".$studentdetailsResult->first_name." ".$studentdetailsResult->last_name."<br>Student Email: ".$studentdetailsResult->std_email."<br>Student Phone: ".$studentdetailsResult->std_phone;
							$entry_for_tutor= "<br>Tutor Name: ".$tutordetailsResult->tutor_first_name." ".$tutordetailsResult->tutor_last_name."<br>Tutor Email: ".$tutordetailsResult->tutor_email."<br>Tutor Phone: ".$tutordetailsResult->tutor_mobile.$land_number;
							$student_message = "<br>Student Message: ".$messageTutor;

							$data_for_log = array("job_id"=>$jobSessionId,"date_entry"=>$lastupdate,"t_color"=>"#000000","entry"=>$entry_for_student.$entry_for_tutor.$student_message); 
							
							$checklistobj->insert($data_for_checklist1);
							$checklistobj->insert($data_for_checklist2);
							$checklistobj->insert($data_for_checklist3);
							$checklistobj->insert($data_for_checklist4);
							$checklistobj->insert($data_for_checklist5);
							$checklistobj->insert($data_for_checklist6);

							$logobj->insert($data_for_log);

						}else{
							$messageTutor = "None";
						}
						//---------------------------end of internal  mail functionality-----
						
						if (isset($authUserNamespace->logintype) && $authUserNamespace->logintype=='2')
						{
							$tutorMessageobj = new Skillzot_Model_DbTable_Messagetutor();
							$data = array("std_id"=>$authUserNamespace->studentid,"message"=>$messageTutor,"contacted_by"=>$contactBy,"lastupdatedate"=>$lastupdatedate);
							$tutorMessageobj->insert($data);
							
							$data1 = array("std_phone"=>$phoneNumber,"std_city"=>'12',"std_locality"=>$Locality,"lastupdatedate"=>$lastupdatedate);
							$studentSignupObj->update($data1,"id=$authUserNamespace->studentid");
						}
						
						$masteraddressobj = new Skillzot_Model_DbTable_Address();
						$location_data = $masteraddressobj->fetchRow("address_id ='$Locality'");
						if (isset($location_data) && $location_data!="")
						{
							$location_value = $location_data->address_value;
						}
			
						if($phoneNumber==""){
							$phoneNumber = "Not shared";
						}
						if (isset($timeDays) && $timeDays==""){
							$timeDays = "Not mentioned";
						}
			//---------------------point no.3-------------------------------------------------
						$message1 = "<div>Hi ".$tutorFullname."...<br/></div><br/>";	
						$message1 .= "<div>We've got a new enquiry for you. Details are as below - <br/></div><br/>"; 
						$message1 .= "<table width=60% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
										<tr align='center' width=100%>
											<td  width=30%>Name:</td>
											<td  width=50%>".$fromFullname."</td>
										</tr>
											<tr align='center'>
											<td>Student's location :</td>
											<td  width=50%>".$location_value."</td>
										</tr>
										<tr align='center'>
											<td>Message :</td>
											<td width=50%>".$messageTutor."</td>
										</tr>
										<tr align='center'>
											<td>".$fromFullname."'"."phone number :</td>
											<td width=50%>".$phoneNumber."</td>
										</tr>
										<tr align='center'>
											<td>Mode of contacting the student :</td>
											<td width=30%>".$contactBy."</td>
										</tr>
										</table><br/>";
						$message1 .= "<div>$fromFullname is expecting to hear from you within 1 working day so do please follow up with your availability and take this forward.<br/></div><br/>";
						$message1 .= "<div>Cheers,<br/></div>";
						$message1 .= "<div>Sandeep<br/></div><br/>";
						$message1 .= "<div>Co-founder @ Skillzot</div>";
						$message1 .= "</div>";
						$message1 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
						
						//print_r($message1);exit;
						$sbNameTemp = "A new enquiry for your classes.";
						$message = $message1;
						$to = $tutoremailId;
						$from = "zot@skillzot.com";
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,"Sandeep @ Skillzot");
						$mail->addTo($to,$toName); //ask
						$mail->setSubject($sbNameTemp);//ask
						$mail->send();
						//$authUserNamespace->forgetpass="Mail has been sent to your EmailId";
			//------------------------------------------------------------------------------------			
						
						//----------------------backup email ---------------------------------
						$message4 = "<div>Hi ".$tutorFullname."...<br/></div><br/>";	
						$message4 .= "<div>We've got a new enquiry for you. Details are as below - <br/></div><br/>"; 
						$message4 .= "<table width=60% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
										<tr align='center' width=100%>
											<td  width=30%>Name:</td>
											<td  width=50%>".$fromFullname."</td>
										</tr>
											<tr align='center'>
											<td>Student's location :</td>
											<td  width=50%>".$location_value."</td>
										</tr>
										<tr align='center'>
											<td>Message :</td>
											<td width=50%>".$messageTutor."</td>
										</tr>
										<tr align='center'>
											<td>".$fromFullname."'"."phone number :</td>
											<td width=50%>".$phoneNumber."</td>
										</tr>
										<tr align='center'>
											<td>Mode of contacting the student :</td>
											<td width=30%>".$contactBy."</td>
										</tr>
										</table><br/>";
						$message4 .= "<div>$fromFullname is expecting to hear from you within 1 working day so do please follow up with your availability and take this forward.<br/></div><br/>";
						$message4 .= "<div>Cheers,<br/></div>";
						$message4 .= "<div>Sandeep<br/></div><br/>";
						$message4 .= "<div>Co-founder @ Skillzot</div>";
						$message4 .= "</div>";
						$message4 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
						
						
						$subTemp =  "Msg from student (".$fromFullname.") to teacher (".$toName.")";          
						//print_r($message1);exit;
						$message = $message4;
						$to = "zot@skillzot.com";
						//$to = "jinu97@gmail.com";
						$from = $studentEmail;
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,$studentEmail);
						$mail->addTo($to,$toName); //ask
						$mail->setSubject($subTemp);//ask
						$mail->send();
						//--------------------------------------------------------------------
						
		//----------------------------------------------------------------------				
						$message2 = "<div style='text-align:justify;'>";
						$message2 .= "<div>Hi...this email is to confirm that your message has been sent to $tutorFullname.<br/></div><br/>";
						$message2 .= "<div>In case you do not get a response within 1 working day or need a faster resolution feel free to mail back on this mail id or call us on +91 9820921404. We are available between 10am to 5:30pm, Mon-Fri.<br/></div><br/>";
						$message2 .= "<div>Cheers,<br/></div>";
						$message2 .= "<div>Sandeep<br/></div><br/>";
						$message2 .= "<div>Co-founder @ Skillzot</div>";
						$message2 .= "</div>";
						$message2 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
						
						$tempsubValue = "Your message has been sent to ". $toName;
						//print_r($message2);exit;
						$message = $message2;
						$to = $studentEmail;
						$from = "zot@skillzot.com";
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,"Sandeep @ Skillzot");
						$mail->addTo($to,$studentFullname);
						$mail->setSubject($tempsubValue); //company name
						$mail->send();
			//----------------------------------------------------------------------------------------	
						$url_message = urlencode($urlValueformessage);		
						$this->_redirect('/search/finalmessage/page/tutor_message?url='.$url_message);
						//$this->_redirect('/search/servey');
					}

		
			}
	}
	
	public function filtersearchAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->setLayout("lightbox");
	 		$Id = $this->_request->getParam("tutorid");
		
	}
	public function unsetpagesessionAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
	 	if($this->_request->isPost()){
			if($this->_request->isXmlHttpRequest()){
				$mdset5 = $authUserNamespace->pagevalue;
				$pageObj = new Skillzot_Model_DbTable_Pagesession();
				$pageRows = $pageObj->fetchRow("sessionnumber='$mdset5'");
				if(isset($pageRows)&& sizeof($pageRows)>0){
					
					$response = $pageRows->pagevalue;
					//$data = array("pagevalue"=>"");
					//$pageObj->update($data,"sessionnumber='$mdset5'");
					$pageObj->delete("sessionnumber='$mdset5'");
				}else{
					$response = "";
				}
	
				echo json_encode($response);
			}
	 	}
	}
	public function getskillslistAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		
	 	if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
					
					$skillobj = new Skillzot_Model_DbTable_Skills();
					$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					$adressObj = new Skillzot_Model_DbTable_Address();
					$skillListingobj = new Skillzot_Model_DbTable_Skillslisting();
					$fetchSkill_id = $this->_request->getParam("category_id");
					if (isset($fetchSkill_id) && $fetchSkill_id!="")
					{
						$authUserNamespace->maincategorySession = $fetchSkill_id;
					}
					$Skill_id_post = $this->_request->getParam("skill_id");
					if (isset($Skill_id_post) && $Skill_id_post!="")
					{
						//$authUserNamespace->skillidSession = $Skill_id_post;
					}
					
					
					
					$fetchSkill_type = $this->_request->getParam("type_id");
					if (isset($fetchSkill_type) && $fetchSkill_type!="")
					{
						$authUserNamespace->lessiontypeSession = $fetchSkill_type;
					}
					if (isset($authUserNamespace->lessiontypeSession) && $authUserNamespace->lessiontypeSession!="")
					{
						$fetchSkill_type = $authUserNamespace->lessiontypeSession;
					}
						
					
					
					$genderId = $this->_request->getParam("gender_id");
					if (isset($genderId) && $genderId!="")
					{
						//$authUserNamespace->genderSession = $genderId;
					}
					if (isset($authUserNamespace->genderSession) && $authUserNamespace->genderSession!="")
					{
						$genderId = $authUserNamespace->genderSession;
					}
					
					$locationId = $this->_request->getParam("location_id");
					if (isset($locationId) && $locationId!="")
					{
						$authUserNamespace->locationSession = $locationId;
					}
					if (isset($authUserNamespace->locationSession) && $authUserNamespace->locationSession!="")
					{
						$locationId = $authUserNamespace->locationSession ;
					}
					
					$localityId = $this->_request->getParam("locality_id");
					//echo "aaaa".$authUserNamespace->localitySession;
					if (isset($localityId) && $localityId!="")
					{
						$authUserNamespace->localitySession = $localityId;
						$tempforallmumbai= $localityId;
					}else{
						$tempforallmumbai = $localityId;
					}
					if (isset($authUserNamespace->localitySession))
					{
						$localityId = $authUserNamespace->localitySession ;
					}
						
				//	echo "bbbbb".$authUserNamespace->localitySession;
					
					if (isset($authUserNamespace->pagevalue) && $authUserNamespace->pagevalue!="")
					{
						$page = $this->_request->getParam('page');
					}else{
						$page = $this->_request->getParam('page',1);
					}
					if(isset($fetchSkill_id) && $fetchSkill_id!="")
					{
						$temp_id = $fetchSkill_id;
						$authUserNamespace->skillidnew = $fetchSkill_id;
					}else
					{
						$temp_id = $Skill_id_post;
						$authUserNamespace->skillidnew = $Skill_id_post;
					}
					//echo "fd".$temp_id;
				//category heading
					
					/* fetch all ids if category id selected*/
					
					$skillndata = $skillobj->fetchRow($skillobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
								  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1' && d.listing_flag='1'"));//21-8
								  
					if (isset($skillndata) && sizeof($skillndata)>0)
					{
						$fetchChildIdS = $skillobj->fetchAll($skillobj->select()
												  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
												  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
												  ->where("s.parent_skill_id = '$temp_id' && d.listing_flag='1'"));//21-8
												 
						/*fetch sub categories if child exsist of main categories*/
						$final_fetchids = array();
						//echo sizeof($fetchChildIdS);exit;
						if(isset($fetchChildIdS) && sizeof($fetchChildIdS)>0){
							$j = 0;
							$final_ids = array();
							$final_ids1 = array();
							foreach($fetchChildIdS as $subids){
								$final_ids[$j] = $subids->skill_id;
								$j++;
							}
						}
					}
					$m=0;
					/*fetch skills categories  of sub categories*/
					if(isset($final_ids) && sizeof($final_ids)>0){
					for($k=0;$k<sizeof($final_ids);$k++){
						$fetchsubChildIdS = $skillobj->fetchAll($skillobj->select()
												     ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
												      ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
												     ->where("s.parent_skill_id = '$final_ids[$k]' && d.listing_flag='1'"));//21-8
												     
						if(isset($fetchsubChildIdS) && sizeof($fetchsubChildIdS)){
							foreach($fetchsubChildIdS as $skillsubids){
								$final_ids1[$m] = $skillsubids->skill_id;
								$m++;
								
							}
						}
					}
					$final_fetchids = array_merge($final_ids,$final_ids1); /*All id with sub cat & skill id*/
					//print_r($final_fetchids);exit;
					}
					
					
					$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
					$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					
					/*Code for tutor couse table check if skill id exsist in this table and exists than fetch all tutor id and send to html*/
					$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));
					
					$i = 0;
					$l = 0;
					$final_tutor_id = array();
					$final_tutor_id_final =array();
					/* convert arry to string for profile table*/
					foreach($tutorCourseRows as $tutorCourserow){
					
						$Skill_id = $tutorCourserow->tutor_skill_id;
						$removeLastgradeId = substr($Skill_id, 0, strlen($Skill_id)-1);
						$explodedCategory = explode(",",$removeLastgradeId);
						//print_r($explodedCategory);
						if(isset($final_fetchids) && sizeof($final_fetchids)>0){
							
							for($g=0;$g<sizeof($final_fetchids);$g++){
								if(in_array($final_fetchids[$g],$explodedCategory)){
									$final_tutor_id[$i] = $tutorCourserow->tutor_id;
									$i=$i+1;
								}
							}
						}else{
							if(in_array($temp_id,$explodedCategory)){
							$listing_rows = $skillListingobj->fetchRow("skill_id=$fetchSkill_id && listing_flag='1'");
								if (isset($listing_rows) && sizeof($listing_rows)>0)
								{
									$final_tutor_id[$l] = $tutorCourserow->tutor_id;
									$l=$l+1;
								}
							}
						}
						$tempfinal_tutor_id = array_unique($final_tutor_id);
					//print_r($tempfinal_tutor_id);exit;
					}
					$q=0;
					foreach($tempfinal_tutor_id as $varId){
						$final_tutor_id1[$q] = $varId;
						$q++;
					}
					if (isset($final_tutor_id1) && sizeof($final_tutor_id1) > 0){
						$final_tutor_id_final = $final_tutor_id1;
					}
//					echo "-------1--------";
//					print_r($final_tutor_id_final);
	//---------------------------------------------locality search ---------------------------------------------------
					$final_tutor_id3 = array();
			//echo "jij".$localityId;
						if (isset($localityId)&& $localityId!="" && $tempforallmumbai!="")
						{
//							echo "innnnnn";
							if (isset($final_tutor_id_final) && sizeof($final_tutor_id_final) > 0 && isset($temp_id) && $temp_id!="")
							{
								$arrystr = "0";
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								$condition = "tutor_id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}		
							
							
							$addressfetchallRows = $adressObj->fetchAll($adressObj->select()
													  ->from(array('t'=>DATABASE_PREFIX.'master_address'))
													  ->where("t.parent_address_id!='0'"));
													 // print_r($addressfetchallRows);
							$address_id_final = array();
							$i = 0;
						foreach($addressfetchallRows as $addressrowall){
					
						$suburbsId = $addressrowall->suburbs_id;
						$explodedCategory = explode(",",$suburbsId);
						
						if(isset($explodedCategory) && sizeof($explodedCategory)>0){
							
							for($g=0;$g<sizeof($explodedCategory);$g++){
								if(in_array($localityId,$explodedCategory)){
									$address_id_final[$i] = $addressrowall->address_id;
									$i=$i+1;
								}
							}
							
							}
						}
							//print_r($address_id_final);exit;
						$address_id_final = array_unique($address_id_final);
							$addressidsequence = array();
							if (isset($address_id_final) && sizeof($address_id_final)>0)
								{
									$rj=0;
									foreach($address_id_final as $v){
										$addressidsequence[$rj] =  $v;
										$rj++;
									}
								}
								$address_id_final = $addressidsequence;
							
							if (isset($address_id_final) && sizeof($address_id_final) > 0)
							{
								$suburbscommasep = "0";
								for($j=0;$j<sizeof($address_id_final);$j++)
								{
									$suburbscommasep .=",".$address_id_final[$j];
								}
							}		
							$suburbscondition = "locality in ($suburbscommasep)";	
							$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
							$companyaddressData = $branchdetailObj->fetchAll($branchdetailObj->select()
													->from(array('c'=>DATABASE_PREFIX."branch_details"))
													->where("$suburbscondition && $condition"));
							if(isset($companyaddressData) && sizeOf($companyaddressData)>0){
								$t=0;
								foreach($companyaddressData as $localityfetch){
									$final_tutor_id3[$t] = $localityfetch->tutor_id;
									$t++;
								}
							}
							//$tempfinal_tutor_id_suburbs = array_unique($final_tutor_id3);
							
							$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
							$travelradiousData = $travelradiousObj->fetchAll($travelradiousObj->select()
											->from(array('t'=>DATABASE_PREFIX."travel_radious"),array('tutor_id','suburbs_id'))
											->where("t.suburbs_id!='0'"));
							
						//print_r($travelradiousData);//array-1
							if (isset($travelradiousData) && sizeof($travelradiousData)>0)
							{
								$w = 0;
								$temptravelradious = array();
								foreach ($travelradiousData as $travelradious)
								{
								$suburbsId = $travelradious->suburbs_id;
								$explodedCategory = explode(",",$suburbsId);
								
									if(isset($explodedCategory) && sizeof($explodedCategory)>0){
										for($g=0;$g<sizeof($explodedCategory);$g++){
											if(in_array($localityId,$explodedCategory)){
												$temptravelradious[$w] = $travelradious->tutor_id;
												$w=$w+1;
											}
										}
									}
									if (isset($temptravelradious) && sizeof($temptravelradious)>0)
									{
										$temptravelradious = $temptravelradious;
									}
									
									//$temptravelradious[$w] = $travelradious->tutor_id;
									//$w++;
								}
							}else{
								$temptravelradious = $final_tutor_id3;
							}
							//print_r($tempfinal_tutor_id_suburbs);//array-2
							
							$mergetwoarrayofsuburbs = array_merge($final_tutor_id3,$temptravelradious);
							$mergetwoarrayofsuburbs = array_unique($mergetwoarrayofsuburbs);
							//print_r($mergetwoarrayofsuburbs);exit; //array-3
							$suburbssequence = array();
							if (isset($mergetwoarrayofsuburbs) && sizeof($mergetwoarrayofsuburbs)>0)
								{
									$q=0;
									foreach($mergetwoarrayofsuburbs as $v){
										$suburbssequence[$q] =  $v;
										$q++;
									}
								}
								$final_tutor_id_final = array_intersect($final_tutor_id_final, $suburbssequence);
								//$final_tutor_id_final = $suburbssequence;
						}
//						echo "-------2--------";
//						echo sizeof($final_tutor_id_final);
							
//								echo $condition;		
//---------------------------------------------lesson type search ---------------------------------------------------
					/*final tutor id send to phtml*/
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0 && isset($fetchSkill_type) && $fetchSkill_type!=0){
						
						
						if (isset($final_tutor_id_final) && sizeof($final_tutor_id_final) > 0 && isset($temp_id) && $temp_id!="")
						{
							$arrystr = "0";
							for($j=0;$j<sizeof($final_tutor_id_final);$j++)
							{
								
									$arrystr .=",".$final_tutor_id_final[$j];
								
							}
							$condition = "id in ($arrystr)";
						}
						else {
							$condition = "1=1";
						}		
						
						
						$strtutorids = implode(',',$final_tutor_id_final);
						if(isset($strtutorids) && $strtutorids!=""){
							$final_tutor_id2 = array();
							if($fetchSkill_type == 2){
								$fetchSkill_type_in = "2,3,4";
							}elseif($fetchSkill_type == 1){
								$fetchSkill_type_in = "1,3";
							}else{
								$fetchSkill_type_in = "1,2,3,4";
							}
							//echo $fetchSkill_type_in;
							$tutortypesRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
														  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
														  		->where("id in ($strtutorids) && tutor_class_type in ($fetchSkill_type_in)"));
//							print $tutorProfileobj->select()
//														  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
//														  		->where("id in ($strtutorids) && tutor_class_type in ($fetchSkill_type_in)");
														  		
							if(isset($tutortypesRows) && sizeOf($tutortypesRows)>0){
								$t=0;
								foreach($tutortypesRows as $typeId){
									$final_tutor_id2[$t] = $typeId->id;
									$t++;
								}
							}
							//print_r($final_tutor_id2);
							$final_tutor_id_final = $final_tutor_id2;
						}
					}
//					if(isset($final_tutor_id1) && sizeof($final_tutor_id1)>0 && $fetchSkill_type==""){
//						$final_tutor_id_final = $final_tutor_id1;
//					}
//						echo "-------3--------";
//						print_r($final_tutor_id_final);
//---------------------------------------------location search ---------------------------------------------------
						$final_tutor_id4 = array();
						if (isset($locationId)&& $locationId!="")
						{
							
							if (isset($final_tutor_id_final) && isset($temp_id) && $temp_id!="")
							{
								$arrystr = "0";
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								$condition = "tutor_id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}
							
							
							if ($locationId==3)
							{
								$fetch_location_id = "1,2,3";
							}else{
								$fetch_location_id = $locationId;
							}
							//echo $fetch_location_id;
							$tutorlocationRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
																	->where("tutor_lesson_location in ($fetch_location_id) && $condition"));
									
//																	print $tutorCourseobj->select()
//																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course'))
//																	->where("tutor_lesson_location in ($fetch_location_id) && $condition");
//																	print_r($tutorlocationRows);								
								if(isset($tutorlocationRows) && sizeOf($tutorlocationRows)>0){
									$t=0;
									foreach($tutorlocationRows as $locationId){
										$final_tutor_id4[$t] = $locationId->tutor_id;
										$t++;
									}
								}
								//$final_tutor_id_final = $final_tutor_id4;
								
								$tempfinal_tutor_id = array_unique($final_tutor_id4);
								$smart = array();
								if (isset($tempfinal_tutor_id) && sizeof($tempfinal_tutor_id)>0)
								{
									$q=0;
									foreach($tempfinal_tutor_id as $v){
										$smart[$q] =  $v;
										$q++;
									}
								}
								$final_tutor_id_final = $smart;
						}
						//echo "aa";
//						echo "-------4--------";
//						print_r($final_tutor_id_final);
//---------------------------------------------gender search ---------------------------------------------------
						$final_tutor_id5 = array();
						if (isset($genderId)&& $genderId!="")
						{
							
							if (isset($final_tutor_id_final) && isset($temp_id) && $temp_id!="")
							{
								$arrystr = "0";
								for($j=0;$j<sizeof($final_tutor_id_final);$j++)
								{
									
										$arrystr .=",".$final_tutor_id_final[$j];
									
								}
								$condition = "id in ($arrystr)";
							}
							else {
								$condition = "1=1";
							}
							
							if ($genderId=="all")
							{
							$tutorgenderRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
																	->where("$condition"));
							}else{
								$tutorgenderRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
																	->where("tutor_gender='$genderId'&& $condition"));
							}
								if(isset($tutorgenderRows) && sizeOf($tutorgenderRows)>0){
									$t=0;
									foreach($tutorgenderRows as $genderId){
										$final_tutor_id5[$t] = $genderId->id;
										$t++;
									}
								}
								$final_tutor_id_final = $final_tutor_id5;
						}

					$response_final =array();
				//display path to skill
					
//					echo "-------3--------";
//						print_r($final_tutor_id_final);
				/*start title*/
							
					//print_r($final_tutor_id_final);exit;
					
					$fixlimit = 10;
					$limit = $fixlimit * $page;
					$temp_limit = ($page-1) * $fixlimit;
					
					/*Start Fetch all skill id than pass it this for fetch and display ajax data*/
					$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
					$skillcourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
					$masteraddressobj = new Skillzot_Model_DbTable_Address();
					$skillObj = new Skillzot_Model_DbTable_Skills();

					//-------------------------display headings of cat,subcat,skill ---------------------	
					if(isset($temp_id) && $temp_id != "404")
					{
					$skillndata = $skillobj->fetchRow("skill_id='$temp_id'");
					$response_final['category_heading']=$skillndata->skill_name;
					$skillCategory = $skillndata->skill_name;
					
						if (isset($skillCategory) && $skillCategory!="")
							{
								$response_final['skillcategory']=$skillCategory;
								$response_final['skillcategoryid'] = $skillndata->skill_id;
								$response_final['idforselected'] = $skillndata->skill_id;
							}
					$parentId = $skillndata->parent_skill_id;
					$skillDepth = $skillndata->skill_depth;
					
					if ($skillDepth=='1')
					{
						$response_final['mainsubmaincategory']="";
						$response_final['maincategory']="";
					}
					if (isset($skillDepth) && $skillDepth > 1)
					{
						$parentdata = $skillobj->fetchRow("skill_id='$parentId'");
						$submainCategory = $parentdata->skill_name;
						if (isset($submainCategory) && $submainCategory!="")
							{
								$response_final['mainsubmaincategory']=$submainCategory;
								$response_final['subcategoryid'] = $parentdata->skill_id;
								$response_final['idforselected'] = $parentdata->skill_id;
							}
						$seccondParentID = $parentdata->parent_skill_id;
						$secondskillDepth = $parentdata->skill_depth;
						if ($secondskillDepth=='1')
						{
							$response_final['maincategory']="";
						}
						if (isset($secondskillDepth) && $secondskillDepth > 1)
						{
							$finaldata = $skillobj->fetchRow("skill_id='$seccondParentID'");
							$mainCategory = $finaldata->skill_name;
							//$mainCategoryId = $finaldata->skill_id;
							if (isset($mainCategory) && $mainCategory!="")
							{
								$response_final['maincategory']=$mainCategory;
								$response_final['maincategoryid'] = $finaldata->skill_id;
								$response_final['idforselected'] = $finaldata->skill_id;
							}
						}
					}	
					}
					//-------------------------end display -------------------------------------	
//					print_r($final_tutor_id_final);
					
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0 && $temp_id!="" ){
					$strofTutorids = implode(",",$final_tutor_id_final);
					$condition = "t.id in ($strofTutorids)";
					
				if ($temp_id=='13' || $temp_id=='14' || $temp_id=='4' || $temp_id=='6'){
											  		

					$tutoridsRows1 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->where("t.is_active=0 && r.parent_skill_id='$skillndata->skill_id'"));
				foreach($tutoridsRows1 as $tids)
				{
					$temparray = $tutorProfileobj->fetchRow($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tutor_review'),array('averageofrecograde'=>'avg(t.recograde)','tutor_id'))
											  		->order(array("avg(t.recograde) desc"))
											  		->where("t.tutor_id='$tids->id'"));
					$reviewObj = new Skillzot_Model_DbTable_Review();
					$data = array("avggradeforonetutor"=>$temparray->averageofrecograde);
					$reviewObj->update($data,"tutor_id='$temparray->tutor_id'");;
				}
				//echo "avg for recograde : ";
				//echo 
				//-----same code as below-----
				
				$tutoridsRows1 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('q'=>DATABASE_PREFIX.'travel_radious'),"t.id=q.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0 && r.skill_id='$skillndata->skill_id'")
											  		->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC"))
											  		->limit($fixlimit,$temp_limit));
					$tutor1=array();
					$i=0;	
					if(isset($tutoridsRows1) && $tutoridsRows1 !="")
					{					  		
						foreach($tutoridsRows1 as $tutorrow)
						{
							$tutor1[$i]=$tutorrow->id;
							$i++;
						}
					}					
											  		
					$count_show_record1 = sizeof($tutoridsRows1); 
					//echo $count_show_record1;exit;//total records
					$tutoridsRows2 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0 && r.skill_id='$skillndata->skill_id'")
											  		->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));
					$count_record2 = sizeof($tutoridsRows2);		//paging records				  		
					$tutor2=array();
					$i=0;	
					if(isset($tutoridsRows2) && $tutoridsRows2 !="")
					{					  		
						foreach($tutoridsRows2 as $tutorrow)
						{
							$tutor2[$i]=$tutorrow->id;
							$i++;
						}
					}
					
					//---------------------------
					// added by hitesh logic of listing on the basis of recometer
					
					$reviewObj = new Skillzot_Model_DbTable_Review();
					$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");	
					if(isset($reviewdata) && $reviewdata !="")
					{							
						foreach($reviewdata as $review)
						{
							if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0 && $temp_id!="" )
							{
								if(($key = array_search($review->tutor_id, $final_tutor_id_final)) == false)
								{
		    						unset($final_tutor_id_final[$key]);
								}
								
							}
							$strofTutorids = implode(",",$final_tutor_id_final);
							$condition1 = "t.id in ($strofTutorids)";
						}
					}	
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final) > 0)
					{			
					if(isset($condition1) && $condition1 !="")
					{				  		
							$tutoridsRows3 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('q'=>DATABASE_PREFIX.'travel_radious'),"t.id=q.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition1 && t.is_active=0")
											  		->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC"))
											  		->limit($fixlimit,$temp_limit));
					}
					else 
					{
							$tutoridsRows3 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('q'=>DATABASE_PREFIX.'travel_radious'),"t.id=q.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC"))
											  		->limit($fixlimit,$temp_limit));	
					}
					$tutor3=array();
					$j=0;	
					if(isset($tutoridsRows3) && $tutoridsRows3 !="")
					{						  		
						foreach($tutoridsRows3 as $tutorrow)
						{
							$tutor3[$j]=$tutorrow->id;
							$j++;
						}
					}	
											  	
					$count_show_record3 = sizeof($tutoridsRows3); //total records
					
					if(isset($condition1) && $condition1 !="")
					{	
						
							$tutoridsRows4 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition1 && t.is_active=0")
											  		->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));
					}
					else 
					{
							$tutoridsRows4 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		->order(array("r.avggradeforonetutor desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));	
					}						  		
					$tutor4=array();
					$j=0;
					if(isset($tutoridsRows4) && $tutoridsRows4 !="")
					{							  		
						foreach($tutoridsRows4 as $tutorrow)
						{
							$tutor4[$j]=$tutorrow->id;
							$j++;
						}	
					}		
								  		
					$count_record4 = sizeof($tutoridsRows4);
					
				}
				else 
				{
					$tutor4=array();
					$tutor3=array();
				}
				
				//-----end of same code-----------
						
				}else{
					//---------------------------
					/*Start code for search */												
					$tutoridsRows1 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('q'=>DATABASE_PREFIX.'travel_radious'),"t.id=q.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0 && r.skill_id='$skillndata->skill_id'")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC"))
											  		->limit($fixlimit,$temp_limit));
					$tutor1=array();
					$i=0;	
					if(isset($tutoridsRows1) && $tutoridsRows1 !="")
					{					  		
						foreach($tutoridsRows1 as $tutorrow)
						{
							$tutor1[$i]=$tutorrow->id;
							$i++;
						}
					}					
											  		
					$count_show_record1 = sizeof($tutoridsRows1); 
					//echo $count_show_record1;exit;//total records
					$tutoridsRows2 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0 && r.skill_id='$skillndata->skill_id'")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));
					$count_record2 = sizeof($tutoridsRows2);		//paging records				  		
					$tutor2=array();
					$i=0;	
					if(isset($tutoridsRows2) && $tutoridsRows2 !="")
					{					  		
						foreach($tutoridsRows2 as $tutorrow)
						{
							$tutor2[$i]=$tutorrow->id;
							$i++;
						}
					}
					//---------------------------
					// added by hitesh logic of listing on the basis of recometer
					
					$reviewObj = new Skillzot_Model_DbTable_Review();
					$reviewdata=$reviewObj->fetchAll("skill_id='$skillndata->skill_id'");	
					if(isset($reviewdata) && $reviewdata !="")
					{							
						foreach($reviewdata as $review)
						{
							if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final)>0 && $temp_id!="" )
							{
								if(($key = array_search($review->tutor_id, $final_tutor_id_final)) == false)
								{
		    						unset($final_tutor_id_final[$key]);
								}
								
							}
							$strofTutorids = implode(",",$final_tutor_id_final);
							$condition1 = "t.id in ($strofTutorids)";
						}
					}	
					if(isset($final_tutor_id_final) && sizeof($final_tutor_id_final) > 0)
					{			
					if(isset($condition1) && $condition1 !="")
					{				  		
							$tutoridsRows3 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('q'=>DATABASE_PREFIX.'travel_radious'),"t.id=q.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition1 && t.is_active=0")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC"))
											  		->limit($fixlimit,$temp_limit));
					}
					else 
					{
							$tutoridsRows3 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('q'=>DATABASE_PREFIX.'travel_radious'),"t.id=q.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC"))
											  		->limit($fixlimit,$temp_limit));	
					}
					$tutor3=array();
					$j=0;	
					if(isset($tutoridsRows3) && $tutoridsRows3 !="")
					{						  		
						foreach($tutoridsRows3 as $tutorrow)
						{
							$tutor3[$j]=$tutorrow->id;
							$j++;
						}
					}	
											  	
					$count_show_record3 = sizeof($tutoridsRows3); //total records
					
					if(isset($condition1) && $condition1 !="")
					{	
						
							$tutoridsRows4 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition1 && t.is_active=0")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));
					}
					else 
					{
							$tutoridsRows4 = $tutorProfileobj->fetchAll($tutorProfileobj->select()
													->setIntegrityCheck(false)
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('id'=>'DISTINCT(t.id)'))
											  		->joinLeft(array('r'=>DATABASE_PREFIX.'tutor_review'),"t.id=r.tutor_id",array(''))
											  		->joinLeft(array('e'=>DATABASE_PREFIX.'tx_tutor_experience'),"t.id=e.tutor_id",array(''))
											  		->joinLeft(array('f'=>DATABASE_PREFIX.'tx_tutor_photos'),"t.id=f.tutor_id",array(''))
											  		->joinLeft(array('a'=>DATABASE_PREFIX.'tx_tutor_analytics'),"t.id=a.tutor_id",array(''))
											  		->where("$condition && t.is_active=0")
											  		->order(array("r.recograde desc","r.gradeavg desc","t.count_video desc","t.tutor_fb_link desc","t.website_link desc","t.tutor_tw_link desc","t.blog_link desc","f.photo_link desc","e.qualification_count DESC","a.enroll_count desc","a.profile_view_count desc","t.id ASC")));	
					}						  		
					$tutor4=array();
					$j=0;
					if(isset($tutoridsRows4) && $tutoridsRows4 !="")
					{							  		
						foreach($tutoridsRows4 as $tutorrow)
						{
							$tutor4[$j]=$tutorrow->id;
							$j++;
						}	
					}		
					$count_record4 = sizeof($tutoridsRows4);
				}
				else 
				{
					$tutor4=array();
					$tutor3=array();
				}
				}
//					print_r($tutor1);
//					print_r($tutor3);
//					print_r($tutor2);
//					print_r($tutor4);exit;
					$tutoridsRows=array_merge($tutor1,$tutor3);
					$tutoridsRow=array_merge($tutor2,$tutor4);
					$count_show_record=sizeof($tutoridsRows);
					$count_record=sizeof($tutoridsRow);
					// ended by hitesh
					//print_r($tutoridsRows);exit;
					
					//--------------------------------paging variables -----------------------------------		
						$response_final[0]['page'] = $page;
						$response_final[0]['recordsize1'] = $count_record;
						$response_final[0]['recordsize'] = $count_show_record;
						$page_items = 10;
						$page_count = ceil($count_record/$page_items);
						$response_final[0]['totalpage'] = $page_count;
					//--------------------------------end----------------------	
						
						
						for($i=0;$i<sizeof($tutoridsRows);$i++)
						{
							$tid = $tutoridsRows[$i];
							$fetch_data = $tutorProfileobj->fetchRow("id='$tid'");
							//print_r($fetch_data);
							//---------------------------multiple locality display-------------------
							$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
							$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
							$adressObj = new Skillzot_Model_DbTable_Address();
							$fetchlocality_data = $branchdetailObj->fetchAll($branchdetailObj->select()
													->setIntegrityCheck(false)
													->from(array('c'=>DATABASE_PREFIX."branch_details"),array('distinct(locality)'))
													->joinLeft(array('m'=>DATABASE_PREFIX."master_address"),'c.locality=m.address_id',array(''))
													->joinLeft(array('l'=>DATABASE_PREFIX."locality_suburbs"),'m.address_id=l.id',array(''))
													->where("c.tutor_id='$tid'")
													 ->order(array("m.address_value asc")));
													//echo $tid;
													//print_r($fetchlocality_data);'distinct(city_id)'
													//exit;
							if (isset($fetchlocality_data) && sizeof($fetchlocality_data)>0)
							{
								$j=0;
								$response_final[$i]['localitysizeloop'] = sizeof($fetchlocality_data);
								foreach ($fetchlocality_data as $locvalue)
								{
									$location_data = $masteraddressobj->fetchRow("address_id ='$locvalue->locality;'");
									if (isset($location_data) && sizeof($location_data)>0)
									{
										$response_final[$i][$j]['locality_value'] = $location_data->address_value;
										$j++;
									}else{
										$response_final[$i][$j]['locality_value'] = 0;
										$j++;
									}
								}
							}
							//------------------------------end multiple locality--------------------
							
							//-------------old locality,city..--------------
//							$locationId = $fetch_data->tutor_add_locality;
//							$cityId = $fetch_data->tutor_add_city;
//							$location_data = $masteraddressobj->fetchRow("address_id ='$locationId'");
//							$city_data = $masteraddressobj->fetchRow("address_id ='$cityId'");
							//------------------------------------------------------
							//$tid = $userData[$i];
							$skill_data = $skillcourseobj->fetchRow("tutor_id='$tid'");
							
							if (isset($fetch_data->id) && $fetch_data->id!=""){
								$response_final[$i]['tutor_id'] = $fetch_data->id ;
							}
							if (isset($fetch_data->tutor_class_type) && $fetch_data->tutor_class_type!=""){
								$classTypeId = $fetch_data->tutor_class_type;
								$classTypedata = $classtypeObj->fetchRow("class_type_id='$classTypeId'");
								if (isset($classTypedata->class_type_name) && $classTypedata->class_type_name!="") {
									if($classTypedata->class_type_id==4 || $classTypedata->class_type_id==2)
									{
										$response_final[$i]['class_type_name'] = "Group"."<br/>"."Classes";
									}else if($classTypedata->class_type_id==1){
										$response_final[$i]['class_type_name'] = "Personal"."<br/>"."Lessons";
									}
									else{
										$response_final[$i]['class_type_name'] = "Personal &"."<br/>"."Group Classes";
									}
								}
							}
							if (isset($fetch_data->company_name) && $fetch_data->company_name==""){
								 	if (isset($fetch_data->tutor_first_name) && $fetch_data->tutor_first_name!=""){
								 		$response_final[$i]['first_name'] = ucfirst($fetch_data->tutor_first_name)." ".ucfirst($fetch_data->tutor_last_name);
								 	}
							}else{
								if (isset($fetch_data->company_name) && $fetch_data->company_name!=""){
									$response_final[$i]['first_name'] = ucfirst($fetch_data->company_name);
								}
							}
							//----------------------old locality,city..---------------------------------------
//							if (isset($city_data->address_value) && $city_data->address_value!=""){
//								$response_final[$i]['city_value'] = $city_data->address_value;
//							}
//							if (isset($location_data->address_value) && $location_data->address_value!=""){
//								$response_final[$i]['locality_value'] = " ".$location_data->address_value;
//							}
							//-------------------------------------------------------------
							if(isset($skill_data->tutor_lesson_location)&& $skill_data->tutor_lesson_location!=""){
								$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
								$lessonlocationResult = $lessonlocationObj->fetchRow("location_id ='$skill_data->tutor_lesson_location'");
								if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0)
								{
									if ($lessonlocationResult->location_id=='3')
									{
										$response_final[$i]['tutor_lesson_location'] = "Teacher's location & open to traveling";
									}else{
										$response_final[$i]['tutor_lesson_location'] = $lessonlocationResult->location_name;
									}
								}
							}
							if(isset($skill_data->travel_radius)&& $skill_data->travel_radius!=""){
								$response_final[$i]['tutor_travel_radius'] = $skill_data->travel_radius;
							}else{
								$response_final[$i]['tutor_travel_radius'] ="";
							}
							if (isset($fetch_data->tutor_brief_desc) && $fetch_data->tutor_brief_desc!=""){
								$strCount = strlen(trim(strip_tags($fetch_data->tutor_brief_desc)));
								if (isset($strCount) && $strCount > 160) {
									$aboutCourse = trim(strip_tags($fetch_data->tutor_brief_desc));
									$showstr = substr($aboutCourse,0,130);
									$response_final[$i]['brief_flag'] = '1';
									$response_final[$i]['brief_desc'] = ucfirst($showstr)."..";
								}else{
									$response_final[$i]['brief_flag'] = '0';
									$response_final[$i]['brief_desc'] = ucfirst(trim(strip_tags($fetch_data->tutor_brief_desc."..")));
								}
							}else{
								$response_final[$i]['brief_desc'] = "";
							}
							if(isset($skill_data->tutor_rate)&& $skill_data->tutor_rate!=""){
								$response_final[$i]['tutor_rate'] = $skill_data->tutor_rate;
							}
						
						}
					}else{
						$response_final[0]['recordsize'] = 0;
						if($temp_id=="404"){
							$response_final['mainsubmaincategory']="";
							$response_final['maincategory']="";
							$response_final['category_heading']=$authUserNamespace->keyword;
					
							$response_final['skillcategory']="";
						}
						
					}
					/*End Fetch all skill id than pass it this for fetch and display ajax data*/
					echo json_encode($response_final);
				}
	 	}}
	public function addfinalskillsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$category_id = $this->_request->getParam("category_id");
				$category_rows = $skillObj->fetchAll($skillObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
															  ->where("parent_skill_id!='0' && skill_id!='0' && parent_skill_id='$category_id' && skill_depth='2' && is_skill='1'"));
				$i=0;
				$tags = "";
				foreach($category_rows as $e){
					$tags[$i]['skill_name'] = $e->skill_name;
					$tags[$i]['skill_id'] = $e->skill_id;
					//$arry[]=$tags;
					$i++;
				}
				echo json_encode($tags);
				}
	  		}
	}
	public function getskillsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$this->_helper->viewRenderer->setNoRender(true);
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$sub_category_id = $this->_request->getParam("skill_id");
				$category_rows = $skillObj->fetchAll($skillObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
															  ->where("s.parent_skill_id!='0' && s.skill_id!='0' && parent_skill_id='$sub_category_id' && skill_depth='3' && is_skill='1' && is_enabled='1'")
															  ->order(array("s.skill_name asc")));
				$i=0;
				$tags = '';
				foreach($category_rows as $e){
					$tags[$i]['skill_name'] = $e->skill_name;
					$tags[$i]['skill_id'] = $e->skill_id;
					//$arry[]=$tags;
					$i++;
				}
				if (isset($tags) && sizeof($tags)>1)
				{
					echo json_encode($tags);
				}else{
					
					echo json_encode(null);
				}
				}
	  		}
	}
public function finalmessageAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();	
		$this->_helper->layout()->disableLayout();
		$id = $this->_request->getParam("task_id");
		$this->view->task_id = $id;
		$page = $this->_request->getParam("page");
		$this->view->page_final = $page;
		$url = $this->_request->getParam("url");
		$this->view->url_finalmessage = $url;
		//echo "page = ".$page;exit;
							
		if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
	 			$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
				$studentData =$studentSignupObj->fetchRow("id='$authUserNamespace->studentid'");
				$studentFullname = ucfirst($studentData->first_name);
				$studentId = $studentData->id;
				$authUserNamespace->studentfullname = $studentFullname;
	 		}
	 	if(isset($id) && $id!=""){
	 			$taskData =$taskCompletion->fetchRow("id='$id'");
	 			$explodedata = $taskData->task_complete;
	 			$expoldetaskcomplete = explode("-",$explodedata);
	 			//echo "'".$expoldetaskcomplete[0]."'".$expoldetaskcomplete[1]."'".$expoldetaskcomplete[2]."'".$expoldetaskcomplete[3]."'".$expoldetaskcomplete[4];exit;
	 			$this->view->expoldetaskcomplete1 = $expoldetaskcomplete[0];
	 			$this->view->expoldetaskcomplete2 = $expoldetaskcomplete[1];
	 			$this->view->expoldetaskcomplete3 = $expoldetaskcomplete[2];
	 			$this->view->expoldetaskcomplete4 = $expoldetaskcomplete[3];
	 			$this->view->expoldetaskcomplete5 = $expoldetaskcomplete[4];

	 	}
	 	if($this->_request->isPost()){
			 	$statuscomplete=$this->_request->getParam("tutorcntby");
			 	$myradio1 = $this->_request->getParam("myradio1");
			 	$myradio2 = $this->_request->getParam("myradio2");
			 	$myradio3 = $this->_request->getParam("myradio3");
			 	$myradio4 = $this->_request->getParam("myradio4");
			 	$myradio5 = $this->_request->getParam("myradio5");

			 	$finalvalue = $myradio1."-".$myradio2."-".$myradio3."-".$myradio4."-".$myradio5;
				
		if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
												
						if($statuscomplete =="" &&  ($expoldetaskcomplete[0] != "" || $expoldetaskcomplete[1]!="" || $expoldetaskcomplete[2] !="" || $expoldetaskcomplete[3]!="" || $expoldetaskcomplete[4] !="")){
							$response["data"]["tutorcntby"] = "valid";
						}elseif($statuscomplete =="" &&  ($expoldetaskcomplete[0] == "" || $expoldetaskcomplete[1]=="" || $expoldetaskcomplete[2] =="" || $expoldetaskcomplete[3]=="" || $expoldetaskcomplete[4] =="")){
							$response["data"]["tutorcntby"] = "allquestion";
						}
						else $response["data"]["tutorcntby"] = "valid";

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('allquestion',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
			}
			else
			{
				$urldecode_page = urldecode($url);
				$data = array("lastupdatedate"=>date('Y-m-d h:i:s'),"student_id"=>$studentId,"task_complete"=>$finalvalue,"page"=>$page."-".$urldecode_page);
				if(isset($id) && $id!="")
				{
					$taskCompletion->update($data,"id = '$id'");
					$taskCompletionId = $id;
				
				}else{
					$taskCompletion->insert($data);
					$taskCompletionId = $taskCompletion->getAdapter()->lastInsertId();	
				}
				$this->_redirect('/search/finalmessagestep2/task_id/'.$taskCompletionId);
			}				      
		}
}
public function finalmessagestep2Action(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();
		$this->_helper->layout()->disableLayout();
		$task_id = $this->_request->getParam("task_id");
		$this->view->task_id = $task_id;

		if(isset($task_id) && $task_id!=""){
	 			$taskData =$taskCompletion->fetchRow("id='$task_id'");
	 			if(isset($taskData) && $taskData!="")
	 			{
		 			$this->view->task_description = $taskData->task_description;
		 			$this->view->task_reason = $taskData->task_reason;
		 			$this->view->task_recommend = $taskData->task_recommend;
	 			}
	 	}

		if($this->_request->isPost()){
			 	$desc=$this->_request->getParam("description");
			 	$notdesc=$this->_request->getParam("notdescription");
			 	$recommend=$this->_request->getParam("recomby");


				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
					
						if(($desc == "" || $notdesc == "" || $recommend == "") && ($taskData->task_description =="" || $taskData->task_reason == "" || $taskData->task_recommend ==""))
						{
							$response["data"]["description"] = "allquestion";
						}else{
							$response["data"]["description"] = "valid";
						}
							

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('allquestion',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
				}
				else
				{
					if($desc != "" && $notdesc != "" && $recommend != "")
					{
					$data = array("task_description"=>$desc,"task_reason"=>$notdesc,"task_recommend"=>$recommend);
					}elseif($desc != "" && $notdesc == "" && $recommend == "")
					{
						$data = array("task_description"=>$desc,"task_reason"=>$taskData->task_reason,"task_recommend"=> $taskData->task_recommend);
					}elseif($desc == "" && $notdesc != "" && $recommend == "")
					{
						$data = array("task_description"=>$taskData->task_description,"task_reason"=>$notdesc,"task_recommend"=> $taskData->task_recommend);
					}elseif($desc == "" && $notdesc == "" && $recommend != "")
					{
						$data = array("task_description"=>$taskData->task_description,"task_reason"=>$taskData->task_reason,"task_recommend"=> $recommend);
					}
					elseif($desc != "" && $notdesc != "" && $recommend == "")
					{
						$data = array("task_description"=>$desc,"task_reason"=>$notdesc,"task_recommend"=> $taskData->task_recommend);
					}
					elseif($desc != "" && $notdesc == "" && $recommend != "")
					{
						$data = array("task_description"=>$desc,"task_reason"=>$taskData->task_reason,"task_recommend"=> $recommend);
					}elseif($desc == "" && $notdesc != "" && $recommend != "")
					{
						$data = array("task_description"=>$taskData->task_description,"task_reason"=>$notdesc,"task_recommend"=> $recommend);
					}else{
						$data = array("task_description"=>$taskData->task_description,"task_reason"=>$taskData->task_reason,"task_recommend"=> $taskData->task_recommend);
					}
					if(isset($task_id) && $task_id!="")
					{
					$taskCompletion->update($data,"id = '$task_id'");	
					}
					$this->_redirect('/search/finalmessagestep3/task_id/'.$task_id);
				}				      
			}
	}

/*public function finalmessagestep3Action(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();
		$this->_helper->layout()->disableLayout();
		$task_id = $this->_request->getParam("task_id");
		$this->view->task_id = $task_id;
		if($this->_request->isPost()){
			 	$notdesc=$this->_request->getParam("notdescription");
			 	if($notdesc == "")
				{
					$status = "Not planning to enroll";
				}else{
					$status = $notdesc;
				}
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
												
						if($notdesc == "")$response["data"]["notdescription"] = "valid";
						else $response["data"]["notdescription"] = "valid";

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
				}
				else
				{
					$data = array("task_reason"=>$status);
					if($status!="")
					{
					$taskCompletion->update($data,"id = '$task_id'");	
					}
					$this->_redirect('/search/finalmessagestep4/task_id/'.$task_id);
				}				      
			}
	}

	public function finalmessagestep4Action(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();
		$this->_helper->layout()->disableLayout();
		$task_id = $this->_request->getParam("task_id");
		$this->view->task_id = $task_id;
		if($this->_request->isPost()){
			 	$recommend=$this->_request->getParam("recomby");
			 	if($recommend == "")
				{
					$status = "Online payment";
				}else{
					$status = $recommend;
				}
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
																		
						if($recommend == "")$response["data"]["recomby"] = "valid";
						else $response["data"]["recomby"] = "valid";

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
				}
				else
				{
					$data = array("task_recommend"=>$status);
					if($status!="")
					{
					$taskCompletion->update($data,"id = '$task_id'");	
					}
					$this->_redirect('/search/finalmessagestep5/task_id/'.$task_id);
				}				      
			}
	}*/
	public function finalmessagestep3Action(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();
		$this->_helper->layout()->disableLayout();
		$task_id = $this->_request->getParam("task_id");
		$this->view->task_id = $task_id;
		if(isset($task_id) && $task_id!=""){
	 			$taskData =$taskCompletion->fetchRow("id='$task_id'");
	 			if(isset($taskData) && $taskData!="")
	 			{
		 			$this->view->task_locality = $taskData->task_locality;
		 			$this->view->task_reason = $taskData->refund;
	 			}
	 	}
	 	$recommend = $taskData->task_recommend;
		if($this->_request->isPost()){
			 	$loc=$this->_request->getParam("locality");
			 	$desc=$this->_request->getParam("desc");

				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
																		
						if($loc == "" && $recommend == 'Other method' && $desc == "")
						{
							$response["data"]["locality"] = "allquestion";
						}elseif ($loc == "" && $recommend != 'Other method' && $desc != "") {
							$response["data"]["locality"] = "valid";
						}elseif ($loc != "" && $recommend == 'Other method' && $desc == "") {
							$response["data"]["locality"] = "allquestion";
						}elseif ($loc == "" && $recommend == 'Other method' && $desc != "") {
							$response["data"]["locality"] = "allquestion";
						}else{
							$response["data"]["locality"] = "valid";
						}
						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('allquestion',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
				}
				else
				{
					if($loc!= "" && $desc != "")
					{
					$data = array("task_locality"=>$loc,"refund"=>$desc);
					}elseif($loc!= "" && $desc == ""){
						$data = array("task_locality"=>$loc,"refund"=>$taskData->refund);
					}else{
						$data = array("task_locality"=>$taskData->task_locality,"refund"=>$desc);
					}
					
					if(isset($task_id) && $task_id!="")
					{
					$taskCompletion->update($data,"id = '$task_id'");	
					}
					$this->_redirect('/search/message');
				}				      
			}
	}
/*	public function finalmessagestep6Action(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();
		$this->_helper->layout()->disableLayout();
		$task_id = $this->_request->getParam("task_id");
		$this->view->task_id = $task_id;
		if($this->_request->isPost()){
			 	$desc=$this->_request->getParam("desc");
			 	if($desc == "")
				{
					$status = "Yes";
				}else{
					$status = $desc;
				}
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
												
						if($statuscomplete == "")$response["data"]["description"] = "valid";
						else $response["data"]["description"] = "valid";

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
				}
				else
				{
					$data = array("refund"=>$status);
					if($status!="")
					{
					$taskCompletion->update($data,"id = '$task_id'");	
					}
					$this->_redirect('/search/message');
				}				      
			}
	}*/
public function finalmessage1Action(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$taskCompletion = new Skillzot_Model_DbTable_Task();	
		$this->_helper->layout()->disableLayout();
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
					
		if(isset($authUserNamespace->studentid) && $authUserNamespace->studentid!=""){
	 			$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
				$studentData =$studentSignupObj->fetchRow("id='$authUserNamespace->studentid'");
				$studentFullname = ucfirst($studentData->first_name);
				$studentId = $studentData->id;
				$authUserNamespace->studentfullname = $studentFullname;
	 		}
	 	if($this->_request->isPost()){
			 	$statuscomplete=$this->_request->getParam("tutorcntby");
		 		$desc=$this->_request->getParam("description");
		 		$notdesc=$this->_request->getParam("notdescription");
		 		$recommend=$this->_request->getParam("recomby");
		 		$loc=$this->_request->getParam("locality");

		 		/*if($statuscomplete=="")
		 		{
		 			$status="Yes";
		 		}
		 		else
		 		{
		 			$status=$statuscomplete;
		 		}
		 		if($recommend=="")
		 		{
		 			$recom="1";
		 		}
		 		else
		 		{
		 			$recom=$recommend;
		 		}*/
				
		if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
												
						if($desc == "")$response["data"]["description"] = "valid";
						else $response["data"]["description"] = "";

						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						
						echo json_encode($response);
						
			}
			else
			{
			
			//$this->_redirect('/search/message');
			}	
			$data = array("lastupdatedate"=>date('Y-m-d h:i:s'),"student_id"=>$studentId,"task_complete"=>$statuscomplete,"task_description"=>$desc,"task_reason"=>$notdesc,"task_recommend"=>$recommend,"task_locality"=>$loc,"page"=>"Payment Step 1");
			if($desc!="" || $statuscomplete!="" || $notdesc!="" || $recommend!="" || $loc!="")
			{
			$taskCompletion->insert($data);	
			}
			      
	}
}
public function messageAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
	}
	public function followerdropdownAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$clientCh = $this->_request->getParam("clientCh");
		$response = array();
		if($clientCh !="" && $clientCh !=NULL && isset($clientCh)) //vimal add condition 
		  {
		  	
		  	$query_string = $skillobj->fetchAll($skillobj->select()
               														->setIntegrityCheck(false)
               														->from(array("r"=>DATABASE_PREFIX.'master_skills'))
               														->where("skill_name LIKE '$clientCh%' && is_skill='1' && is_enabled='1'"));
		 	 if(isset($query_string) && sizeof($query_string) > 0)
               	{
               		$z=0;
               		//$result_set = mysql_query($query_string)or die(mysql_error());
               		foreach($query_string as $rowquery_string)
               		{
						$response[$z] = $rowquery_string->skill_name."\n";
						$z++;
               			//$result = $rowquery_string->skill_name."\n";
						//echo $result;
					}
					//mysql_close($link);
					
               	}
               	echo json_encode($response);
		  }	
}
public function followerAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$skillmapobj = new Skillzot_Model_DbTable_SkillMap();
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		/*$adressObj = new Skillzot_Model_DbTable_Address();*/
		//$q=$this->_request->getParam("q");
		$clientCh=$_GET['q'];
		$query = trim(preg_replace("/(\s+)+/", " ", $clientCh));
		//echo "query=".$query;exit;
		$response = array();
		
		if($query !="" && $query !=NULL && isset($query)) //vimal add condition 
		  {
		
			$query_for_skill = $skilldisplayObj->fetchAll($skilldisplayObj->select()
					->setIntegrityCheck(false)
       				->from(array("r"=>DATABASE_PREFIX.'master_skills_for_diplay_listing'))
				 	->where("skill_name LIKE '".addslashes($query)."%' || skill_uniq LIKE '".addslashes($query)."%' && skill_id!='0' && is_skill='1'"));


		  	$query_for_tutor = $tutorProfile->fetchAll($tutorProfile->select()
						->setIntegrityCheck(false)
						->distinct()
						->from(array("a"=>DATABASE_PREFIX.'tx_tutor_profile'))
						->where("(a.tutor_first_name like '".addslashes($query)."%' || a.tutor_last_name like '".addslashes($query)."%') && a.is_active='0'"));
		  	$query_for_tutor1 = $tutorProfile->fetchAll($tutorProfile->select()
						->setIntegrityCheck(false)
						->distinct()
						->from(array("a"=>DATABASE_PREFIX.'tx_tutor_profile'))
						->where("a.company_name like '".addslashes($query)."%' && a.is_active='0'"));
			
			if($query_for_skill!="" && $query_for_tutor=="")
			{
					$z=0;
               		foreach($query_for_skill as $rowquery_string)
               		{
						echo $response[$z] = $rowquery_string->skill_name."\n";
						$z++;
					}
			}elseif ($query_for_skill=="" && $query_for_tutor!="") {
					$z=0;
               		foreach($query_for_tutor as $rowquery_string)
               		{
						echo $response[$z] = $rowquery_string->tutor_first_name." ".$rowquery_string->tutor_last_name."\n";
						echo $response[$z] = $rowquery_string->company_name."\n";
						$z++;
					}
			}
			else
			{
					$a=0;
               		foreach($query_for_skill as $rowquery_string)
               		{
						echo $response[$a] = $rowquery_string->skill_name."\n";
						$a++;
					}
					$z=0;
               		foreach($query_for_tutor as $rowquery_string)
               		{
						echo $response[$z] = $rowquery_string->tutor_first_name." ".$rowquery_string->tutor_last_name."\n";
						//echo $response[$z] = $rowquery_string->company_name."\n";
						$z++;
					}
					$v=0;
               		foreach($query_for_tutor1 as $rowquery_string)
               		{
						echo $response[$v] = $rowquery_string->company_name."\n";
						$v++;
					}
			}
              
		  }
	
}
public function indexoldAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("searchinnerpage");
		//unset($authUserNamespace->pagevalue);
		//unset($authUserNamespace->pagesessionforlogin);
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$skillListingobj = new Skillzot_Model_DbTable_Skillslisting();
		//echo $authUserNamespace->pagevalue;exit;
		$pageValue = $this->_request->getParam("page");
		$fetchSkill_id = $this->_request->getParam("skillid");/* search skiil id*/
		//echo $fetchSkill_id;exit;
			if (isset($pageValue) && $pageValue!="")
			{
				$pageObj = new Skillzot_Model_DbTable_Pagesession();
				$setval2 = $pageValue. time().mt_rand();
				$mdset5 =  md5($setval2);
				$data = array("sessionnumber"=>$mdset5,"pagevalue"=>$pageValue);
				$pageObj->insert($data);
				$authUserNamespace->pagevalue = $mdset5;
			}
		//-----------------------------		all session unset
		$sessionunsetVar = $this->_request->getParam("sessionunsetvar");
//		echo $sessionunsetVar;exit;
//echo $authUserNamespace->maincategorySession;exit;
		if (isset($sessionunsetVar) && $sessionunsetVar=="1")
		{
		//	echo "in";exit;
			unset($authUserNamespace->lessiontypeSession);
			unset($authUserNamespace->maincategorySession);
			unset($authUserNamespace->locationSession);
			unset($authUserNamespace->localitySession);
			unset($authUserNamespace->genderSession);
			unset($authUserNamespace->skillidSession);
			
			//unset($authUserNamespace->pagevalue);	
			
				$mdset5 = $authUserNamespace->pagevalue;
				$pageObj = new Skillzot_Model_DbTable_Pagesession();
				$pageObj->delete("sessionnumber='$mdset5'");
			
				
			$tuturIdlink = "/search/index/skillid/".$fetchSkill_id;
			$this->_redirect($tuturIdlink);
		}
		//-------------------------------------------------	
			
			
		/*fetch category values to display in drop down*/
		$categoryskillRows = $skillobj->fetchAll($skillobj->select()
											  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
											  ->where("parent_skill_id = 0 && skill_id!=0 && skill_id != 2 && is_skill='1'")
											  ->order(array("order_id asc")));
		if (isset($categoryskillRows) && sizeof($categoryskillRows)>0){
			$this->view->categoryvalues = $categoryskillRows;
		}
		
		$lessonlocationObj = new Skillzot_Model_DbTable_Tutorlessionlocation();
		$lessonlocationResult = $lessonlocationObj->fetchAll();
		if (isset($lessonlocationResult) && sizeof($lessonlocationResult)>0){
			$this->view->lessonlocation = $lessonlocationResult;
		}
		/*fetch classtype values to display in drop down*/
		
		$classtypeObj = new Skillzot_Model_DbTable_Tutorclasstype();
		$classtypeResult = $classtypeObj->fetchAll($classtypeObj->select()
										->from(array('s'=>DATABASE_PREFIX."master_tutor_class_types"))
										->order(array("class_type_id asc")));
		if (isset($classtypeResult) && sizeof($classtypeResult)>0){
			$this->view->classtype = $classtypeResult;
		}
		$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
		$suburbsData = $localitysuburbsObj->fetchAll($localitysuburbsObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."locality_suburbs"))
															  ->where("s.is_active='1'")
															  ->order(array("s.order asc")));
				if (isset($suburbsData) && sizeof($suburbsData)>0)
				{
					$this->view->suburbsdata = $suburbsData;
				}											  
//		print_r($suburbsData);exit;
//									$addressResult = $adressObj->fetchAll($adressObj->select()
//								   ->from(array('a'=>DATABASE_PREFIX."master_address"))
//								   ->where("a.address_type_id='2' && address_id!='0'"));	
//								   foreach ($addressResult as $addr)
//								   {
//								   	$gluedTogetherSpaces = implode(",",$addr->suburbs_id);
//								   	if (sizeof($gluedTogetherSpaces))
//								   	{
//								   		echo $addr->suburbs_id;
//								   		echo "</br>";
//								   	}
//								   }			
//								   exit;											  
		/*Start Search logic */
	
	$fetchSkill_type = $this->_request->getParam("skilltype");/*search skiil type*/
		if (isset($fetchSkill_type) && $fetchSkill_type!="")
		{
			$this->view->ftypeid = $fetchSkill_type;
			unset($authUserNamespace->lessiontypeSession);
			unset($authUserNamespace->maincategorySession);
			unset($authUserNamespace->locationSession);
			unset($authUserNamespace->localitySession);
			unset($authUserNamespace->genderSession);
			unset($authUserNamespace->skillidSession);
			
		}
		if (isset($authUserNamespace->maincategorySession) && $authUserNamespace->maincategorySession!="")
		{
			$fetchSkill_id = $authUserNamespace->maincategorySession;
		}	
		
		
		if (isset($fetchSkill_id) && $fetchSkill_id!="")
		{
			$this->view->fskillid = $fetchSkill_id;
			$this->view->idtoshowselected = $fetchSkill_id;
			$authUserNamespace->skillidnew = $fetchSkill_id;
		}
		
		
		/*start title*/
		
		//$skillndata = $skillobj->fetchRow("skill_id='$fetchSkill_id' && skill_id!=0 && is_skill='1'")
		
		$skillndata = $skillobj->fetchRow($skillobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
								  //->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1'"));//21-8
								  //&& d.listing_flag='1'
//							print $skillobj->select()
//								  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
//								  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
//								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1' && d.listing_flag='1'");	  
//								  print_r($skillndata);exit;
//		print $skillobj->select()
//								  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
//								  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
//								  ->where("s.skill_id='$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1' && d.listing_flag='1'");exit;

			if (isset($skillndata) && sizeof($skillndata)>0 )
				{
					$skillCategory = $skillndata->skill_name;
					$this->view->skillcategory = $skillCategory;
					$this->view->skillid = $skillndata->skill_id;//main catogory
					$this->view->idtoshowselected = $fetchSkill_id;
					$skillDepth = $skillndata->skill_depth;
					$authUserNamespace->skillcategory = $skillCategory;
					$parentId = $skillndata->parent_skill_id;
				}
		
		if (isset($skillDepth) && $skillDepth > 1)
		{//echo "in";exit;
			$parentdata = $skillobj->fetchRow("skill_id='$parentId' && skill_id!=0 && is_skill='1' && is_enabled='1'");//16-8
			
			if (isset($parentdata) && sizeof($parentdata)>0 )
			{
				$submainCategory = $parentdata->skill_name;
				if (isset($submainCategory) && $submainCategory!="")
					{
						$this->view->mainsubmaincategory = $submainCategory;
						$this->view->mainsubmainid = $parentdata->skill_id;//sub category
						$this->view->idtoshowselected = $parentdata->skill_id;
						$authUserNamespace->submaincategory = $submainCategory;
					}
				$seccondParentID = $parentdata->parent_skill_id;
				$secondskillDepth = $parentdata->skill_depth;
				if (isset($secondskillDepth) && $secondskillDepth > 1)
				{
					$finaldata = $skillobj->fetchRow("skill_id='$seccondParentID' && skill_id!=0 && is_skill='1'");//16-8
					if (isset($finaldata) && sizeof($finaldata) > 0)
					{
//						print_r($finaldata);exit;
						$mainCategory = $finaldata->skill_name;
						if (isset($mainCategory) && $mainCategory!="")
						{
							$this->view->maincategory = $mainCategory;
							$this->view->maincategoryid = $finaldata->skill_id;
							$this->view->idtoshowselected = $finaldata->skill_id;
							$authUserNamespace->maincategory = $mainCategory;
						}
					}
				}
			}
		}
		if (isset($skillndata) && sizeof($skillndata)>0 )
		{
			$skllname = $skillndata->skill_name;
			$this->view->skillname = $skllname;
		
			$fetchChildIdS = $skillobj->fetchAll($skillobj->select()
									  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									  ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
									  ->where("s.parent_skill_id = '$fetchSkill_id' && s.skill_id!=0 && s.is_skill='1' && d.listing_flag='1'"));//21-8
			//&& d.listing_flag='1'
			/*fetch sub categories if child exsist of main categories*/
			$final_fetchids = array();
			//echo sizeof($fetchChildIdS);exit;
			if(isset($fetchChildIdS) && sizeof($fetchChildIdS)>0){
				$j = 0;
				$final_ids = array();
				$final_ids1 = array();
				foreach($fetchChildIdS as $subids){
					$final_ids[$j] = $subids->skill_id;
					$j++;
				}
			}
		}
		$m=0;
		/*fetch skills categories  of sub categories*/
		if(isset($final_ids) && sizeof($final_ids)>0){
		for($k=0;$k<sizeof($final_ids);$k++){
			$fetchsubChildIdS = $skillobj->fetchAll($skillobj->select()
									     ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									      // ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
									     ->where("s.parent_skill_id = '$final_ids[$k]' && s.skill_id!=0 && s.is_skill='1'"));//21-8
			//	print_r($fetchsubChildIdS);exit;
			if(isset($fetchsubChildIdS) && sizeof($fetchsubChildIdS)){
				foreach($fetchsubChildIdS as $skillsubids){
					$final_ids1[$m] = $skillsubids->skill_id;
					$m++;
					
				}
			}
		}
		$final_fetchids = array_merge($final_ids,$final_ids1); /*All id with sub cat & skill id*/
		}
		
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		
		/*Code for tutor couse table check if skill id exsist in this table and exists than fetch all tutor id and send to html*/
		$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));
		//print_r($tutorCourseRows);exit;
		$i = 0;
		$l=0;
		$final_tutor_id = array();
		if(isset($fetchSkill_id) && $fetchSkill_id!="")
			{
				foreach($tutorCourseRows as $tutorCourserow){
				
					$Skill_id = $tutorCourserow->tutor_skill_id;
					$removeLastgradeId = substr($Skill_id, 0, strlen($Skill_id)-1);
					 
					$explodedCategory = explode(",",$removeLastgradeId);
					//print_r($explodedCategory);
					
					if(isset($final_fetchids) && sizeof($final_fetchids)>0){
						
						for($g=0;$g<sizeof($final_fetchids);$g++){
							if(in_array($final_fetchids[$g],$explodedCategory)){
								$final_tutor_id[$i] = $tutorCourserow->tutor_id;
								$i=$i+1;
							}
						}
						
					}else{
					
						if(in_array($fetchSkill_id,$explodedCategory)){
							$listing_rows = $skillListingobj->fetchRow("skill_id=$fetchSkill_id && listing_flag='1'");
							if (isset($listing_rows) && sizeof($listing_rows)>0)
							{
							
								$final_tutor_id[$l] = $tutorCourserow->tutor_id;
								$l=$l+1;
							}
						}
						$tempfinal_tutor_id = array_unique($final_tutor_id);
							
					}
				}
			}
			
			
			
			if(isset($tempfinal_tutor_id) && sizeof($tempfinal_tutor_id)>0 )
			{
						$q=0;
						foreach($tempfinal_tutor_id as $varId){
							$final_tutor_id1[$q] = $varId;
							$q++;
						}
			}
		if ($fetchSkill_id=="")
		{
			//echo "sdf";exit;
			$final_tutor_id = "";
			$final_tutor_id1 = "";
			$this->view->t_ids = "";
		}else{
			$this->view->t_ids = "jin";
		}
		/*final tutor id send to phtml*/
	//	echo "s".$fetchSkill_type;
		//print_r($final_tutor_id1);
		if(isset($final_tutor_id1) && sizeof($final_tutor_id1)>0 && $final_tutor_id1!="" && isset($fetchSkill_type) && $fetchSkill_type!=""){
			//echo "sdf";exit;
			$strtutorids = implode(',',$final_tutor_id1);
			if(isset($strtutorids)){
				$final_tutor_id2 = array();
				if($fetchSkill_type == 2){
					$fetchSkill_type_in = "2,4";
				}
				elseif($fetchSkill_type == 0){
					$fetchSkill_type_in = "1,2,3,4";
				}else{
					$fetchSkill_type_in = $fetchSkill_type;
				}
				$tutortypesRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
											  		->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'))
											  		->where("id in ($strtutorids) && tutor_class_type in ($fetchSkill_type_in)"));
				
				//echo "in";exit;							  		
				if(isset($tutortypesRows) && sizeOf($tutortypesRows)>0){
					$t=0;
					foreach($tutortypesRows as $typeId){
						$final_tutor_id2[$t] = $typeId->id;
						$t++;
					}
					//print_r($final_tutor_id2);exit;
					$strofTutorids = implode(",",$final_tutor_id2);
					//$this->view->tutorid = $final_tutor_id2;
					$condition = "id in ($strofTutorids)";
					$tutoridsRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('t.id'))
																	->where("$condition"));
					if (isset($tutoridsRows) && sizeof($tutoridsRows)>0)
					{
						$this->view->tutorid = $tutoridsRows;
						
						$page = $this->_request->getParam('page',1);
						//$this->view->page = $page;
						$records_per_page = 5;
						$record_count = sizeof($tutoridsRows);
						$paginator = Zend_Paginator::factory($tutoridsRows);
						$paginator->setItemCountPerPage($records_per_page);
						$paginator->setCurrentPageNumber($page);
						$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
						$this->view->tutorid = $paginator;
						$page_number  = $record_count / 1;
						$page_number_last =  floor($page_number);
					}
				}
			}
		}
		if(isset($final_tutor_id1) && sizeof($final_tutor_id1)>0 && $fetchSkill_type==""){
			//$this->view->tutorid = $final_tutor_id1;
			//print_r($final_tutor_id1);
			//echo "in";exit;
			$strofTutorids = implode(",",$final_tutor_id1);
			$condition = "id in ($strofTutorids)";
			$tutoridsRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
																	->from(array('t'=>DATABASE_PREFIX.'tx_tutor_profile'),array('t.id'))
																	->where("$condition"));
			//print_r($tutorgenderRows);
			if (isset($tutoridsRows) && sizeof($tutoridsRows)>0)
			{
				$this->view->tutorid = $tutoridsRows;
				
				$page = $this->_request->getParam('page',1);
				//$this->view->page = $page;
				$records_per_page = 5;
				$record_count = sizeof($tutoridsRows);
				$paginator = Zend_Paginator::factory($tutoridsRows);
				$paginator->setItemCountPerPage($records_per_page);
				$paginator->setCurrentPageNumber($page);
				$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
				$this->view->tutorid = $paginator;
				$page_number  = $record_count / 1;
				$page_number_last =  floor($page_number);
			}
		}
		
		
		$skillObj = new Skillzot_Model_DbTable_Skills();
		if($this->_request->isPost()){
				
					$keywords = $this->_request->getParam("keywords");
					$skilltype = $this->_request->getParam("skilltype");
					$authUserNamespace->keyword=$keywords;
					
						$skillrowResult = $skillObj->fetchRow($skillObj->select()
												   		  // ->setIntegrityCheck(false)
												   		 ->from(array('s'=>DATABASE_PREFIX."master_skills"))
												   		   //->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'s.occasion_id=t.tutor_class_type',array('lv.value'))
												   		    // ->joinLeft(array('d'=>DATABASE_PREFIX.'master_skills_for_diplay_listing'),"s.skill_id=d.skill_id",array(''))
														   ->where("s.skill_name like '%".$keywords."%' && s.skill_id!='0' && s.is_skill='1'"));//21-8
														   
														   unset($authUserNamespace->maincategorySession);
														   unset($authUserNamespace->skillidnew);
														   unset($authUserNamespace->lessiontypeSession);
														   unset($authUserNamespace->locationSession);
														   unset($authUserNamespace->localitySession);
														   unset($authUserNamespace->genderSession);
														   unset($authUserNamespace->skillidSession);
						if(sizeof($skillrowResult) <= 0)
						{
						$tuturIdlink = "/search/index/skillid/404/skilltype/".$skilltype;
						}
						else 
						{
						$tuturIdlink = "/search/index/skillid/".$skillrowResult->skill_id."/skilltype/".$skilltype;	
						}
						
						$this->_redirect($tuturIdlink);
				//}
			}
		
	}
}
	?>
