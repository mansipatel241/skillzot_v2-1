<?php
class ProfilepicuploadnewController extends Zend_Controller_Action
{
	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
	}
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$img_tutor_id = $this->_request->getParam("id");
	 	//$path = "docs/";
		//$this->view->path = $path;
		//$this->view->user_id_info = "1";
		$path = "/docs";
		$path = $this->_request->getParam("path");
		$this->view->path = $path;
		$path = implode("/",explode("|",$path));
		$this->view->user_id_info = "1";
		if($this->_request->isPost())
		{
			$uploadfromcourse = $this->_request->getParam("uploadfromcourse");
			
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();

				$filecheck1 = basename($uploadfromcourse);
  				$ext = substr($filecheck1, strrpos($filecheck1, '.') + 1);
				$ext = strtolower($ext);
				
				if($uploadfromcourse == "")$response["data"]["uploadfromcourse"] = "imageuploaderror";
				elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['uploadfromcourse']="valid";
				else $response["data"]["uploadfromcourse"] = "invalid";
			
				if(!in_array('null',$response['data'])&& !in_array('imageuploaderror',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('duplicate',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else
			{
				 $uploadfromtype = $_FILES["uploadfromcourse"]["type"];
				 $tmpName1 = $_FILES["uploadfromcourse"]["tmp_name"];
				 $fileName = $_FILES["uploadfromcourse"]["name"];
				 $image_size = $_FILES["uploadfromcourse"]["size"];
				if(isset($image_size) && $image_size > 5242880 ){
						$authUserNamespace->profileimageerror = "Image size is grater than 5 Mb";
						$this->_redirect('/profilepicupload/index');
//						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
//						{
//							
//							$defaultcourseId = $authUserNamespace->defultcourseid;
//							define('DEFAULTCOURSEID',$defaultcourseId);
//							//print DEFAULTCOURSEID;
//							//exit;
//							echo "<script>window.parent.location='". BASEPATH ."/editprofile/editcourse/defaultcorseid/". DEFAULTCOURSEID ."';</script>";
//						
//						}else{
//							echo "<script>window.parent.location='". BASEPATH ."/tutor/coursedetail/#jerrorimg';</script>";
//						}
					}else{	
				 $fp1 = fopen($tmpName1, 'r');
				 $addfromcomptypecontent = fread($fp1, filesize($tmpName1));
				 $content = fread($fp1, filesize($tmpName1));
				 fclose($fp1);
				
				$this->view->image_type=$uploadfromtype;
				$this->view->tmpName=$tmpName1;
				
					$fileInfo = $_FILES["uploadfromcourse"];
				
					list($imagewidth, $imageheight, $type, $attr) = getimagesize($tmpName1);
					
					if((isset($imagewidth) && $imagewidth < 240) ||(isset($imageheight) && $imageheight < 245)){
						//echo $imagewidth;echo $imageheight;exit;
						$authUserNamespace->profileimageerror = "Image size less than 240 * 245 pixels.";
						$this->_redirect('/profilepicupload/index');
//					if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
//						{
//							$defaultcourseId = $authUserNamespace->defultcourseid;
//							define('DEFAULTCOURSEID',$defaultcourseId);
//							echo "<script>window.parent.location='". BASEPATH ."/editprofile/editcourse/defaultcorseid/". DEFAULTCOURSEID ."/#rate';</script>";
//							
//						}else{
//							$this->_redirect('/uploadimage/index');
////							//echo "<script>window.parent.location='". BASEPATH ."/tutor/coursedetail/#jerrorimg';</script>";
//						}
					}else{
					list($newimagewidth, $newimageheight, $newtype, $newattr) = getimagesize($tmpName1);

					if($imagewidth > 700){
						$propimgw = 700/$imagewidth;
						$imagewidth = 700;
						$imageheight = $imageheight*$propimgw;
					}
		
					if($imageheight > 700){
						$propimgh = 700/$imageheight;
						$imageheight = 770;
						$imagewidth = $imagewidth*$propimgh;
					}
					$jpeg_quality = 90;
					$src = file_get_contents($tmpName1);
			    	$img_r = imagecreatefromstring($src);
			    	$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
			    	imagecopyresampled($dst_r,$img_r,0,0,0,0,$imagewidth,$imageheight,$newimagewidth,$newimageheight);
			
					if(sizeof($fileInfo) > 0){
				
						$type = $fileInfo["type"];
						$tmpName = $fileInfo["tmp_name"];
						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
						{
							$name = "img_pro".$authUserNamespace->admintutorid;
						}else{
							$name = "img_pro".$img_tutor_id;
						}
						//$authUserNamespace->latest_uploaded_image_no = "1";
						$imagefinaltype = explode("/",$type);
						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
						{
							
							$authUserNamespace->edit_profile_session = $name.".".$imagefinaltype[1];
							$authUserNamespace->edit_profile_session1 = $name."croppic.".$imagefinaltype[1];
						}else{
							$authUserNamespace->flow_pic_session = $name.".".$imagefinaltype[1];
							$authUserNamespace->flow_pic_session1 = $name."croppic.".$imagefinaltype[1];
						}
						imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$name.".".$imagefinaltype[1],$jpeg_quality);//main image upload
					}
					$this->_redirect('/profilepicupload/uploadpicturestep2');
				}				
				}
			}
		}
	}
	
	public function uploadpicturestep2Action(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
		{
			$img_tutor_id= $authUserNamespace->admintutorid;
			$this->view->id = $img_tutor_id;
		}else{
		$img_tutor_id=$this->_request->getParam("id");
		$this->view->id = $img_tutor_id;
		}
		//echo "<script>parent.Mediabox.close();</script>";
	}
	
	public function showcropimageAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
	}
	
	public function saveimageAction(){
		
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
//			echo $authUserNamespace->edit_profile_session1;exit;
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout()->disableLayout();
			$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
			$id = $authUserNamespace->admintutorid;
			$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
			
			if($this->_request->isPost()){
				
				if(isset($authUserNamespace->flow_pic_session) && $authUserNamespace->flow_pic_session!=""){
				
				$jpeg_quality = 90;
				$x=$this->_request->getParam("sizeX");
				$y=$this->_request->getParam("sizeY");
				$w=$this->_request->getParam("sizeW");
				$h=$this->_request->getParam("sizeH");
				
				$src=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session);
	    		$img_r = imagecreatefromstring($src);
	    		$dst_r = ImageCreateTrueColor($w,$h);
	    		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
    			imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1,$jpeg_quality);
    			
    			// hitesh added
				if (isset($authUserNamespace->flow_pic_session1) && $authUserNamespace->flow_pic_session1!="" && isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
				{
					$image_typearr = explode(".",$authUserNamespace->flow_pic_session1);
					$image_type = "image/".$image_typearr[1];
					$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1);
					
					$jpeg_quality = 90;
					$ThumbWidth =160;
					$img_r = imagecreatefromstring($getCropimagecontent);
					imagefilter($img_r, IMG_FILTER_GRAYSCALE);
				    list($width, $height) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1);
					//calculate the image ratio
					$imgratio=$width/$height;
					if ($imgratio>1){
						$newwidth = $ThumbWidth;
						$newheight = $ThumbWidth/$imgratio;
					}else{
						$newheight = $ThumbWidth;
						$newwidth = $ThumbWidth*$imgratio;
					}
					// create a new temporary image
					
					$tmp_img = imagecreatetruecolor( $newwidth, $newheight );
					imagecopyresized($tmp_img, $img_r, 0, 0, 0, 0, $newwidth,$newheight, $width, $height);
					$target_path1 = dirname(dirname(dirname(__FILE__)))."/docs/Userfc.jpeg";
					//$tagetcrop = dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1;
					
					imagejpeg($tmp_img,$target_path1,$jpeg_quality);
					
					$filecheck = basename($target_path1);
					$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
					$ext = strtolower($ext);
					if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" ||$ext == "pjpeg" || $ext == "png"){
					 		$thumb_type="image/".$ext;
					}
					
					
					$imagethumbcontent = file_get_contents($target_path1);
					
					$imagew='100';
    				$imageh='100';
    				//$src=file_get_contents($target_path1);
	    	
					$img_r = imagecreatefromstring($getCropimagecontent);
					$dst_r = ImageCreateTrueColor($imagew,$imageh);
					list($width11, $height11) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1);
	    			//list($width11, $height11) = getimagesize($target_path1);
	    			$jpeg = '.jpeg';
	    			imagecopyresized($dst_r,$img_r, 0, 0, 0, 0, $imagew, $imageh, $width11, $height11);
					imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg,$jpeg_quality);
					$var1=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg);
					
					$data = array("tutor_image_type"=>$image_type,"tutor_image_content"=>$getCropimagecontent,
										"thumb_profile_content"=>$var1,"thumb_profile_type"=>$image_type,
										"tutor_thumb_type"=>$thumb_type,"tutor_thumb_content"=>$imagethumbcontent);
		    		$tutorProfileobj->update($data,"id=$authUserNamespace->admintutorid");
		    		$authUserNamespace->changesave = "Your changes have been saved";
				//	$this->_redirect('/editprofile/profilepic');
				}
				//$this->_redirect('/editprofile/profilepic');
				}elseif(isset($authUserNamespace->edit_profile_session) && $authUserNamespace->edit_profile_session!=""){
				
				$jpeg_quality = 90;
				$x=$this->_request->getParam("sizeX");
				$y=$this->_request->getParam("sizeY");
				$w=$this->_request->getParam("sizeW");
				$h=$this->_request->getParam("sizeH");
				
				$src=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session);
				
	    		$img_r = imagecreatefromstring($src);
	    		$dst_r = ImageCreateTrueColor($w,$h);
	    		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
    			imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1,$jpeg_quality);
    			
				// hitesh added
				if (isset($authUserNamespace->edit_profile_session1) && $authUserNamespace->edit_profile_session1!="" && isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
				{
					$image_typearr = explode(".",$authUserNamespace->edit_profile_session1);
					$image_type = "image/".$image_typearr[1];
					$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1);
					
					$jpeg_quality = 90;
					$ThumbWidth =160;
					$img_r = imagecreatefromstring($getCropimagecontent);
					imagefilter($img_r, IMG_FILTER_GRAYSCALE);
				    list($width, $height) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1);
					//calculate the image ratio
					$imgratio=$width/$height;
					if ($imgratio>1){
						$newwidth = $ThumbWidth;
						$newheight = $ThumbWidth/$imgratio;
					}else{
						$newheight = $ThumbWidth;
						$newwidth = $ThumbWidth*$imgratio;
					}
					// create a new temporary image
					
					$tmp_img = imagecreatetruecolor( $newwidth, $newheight );
					imagecopyresized($tmp_img, $img_r, 0, 0, 0, 0, $newwidth,$newheight, $width, $height);
					$target_path1 = dirname(dirname(dirname(__FILE__)))."/docs/Userfc.jpeg";
					//$tagetcrop = dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->flow_pic_session1;
					
					imagejpeg($tmp_img,$target_path1,$jpeg_quality);
					
					$filecheck = basename($target_path1);
					$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
					$ext = strtolower($ext);
					if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" ||$ext == "pjpeg" || $ext == "png"){
					 		$thumb_type="image/".$ext;
					}
					
					
					$imagethumbcontent = file_get_contents($target_path1);
					
					$imagew='100';
    				$imageh='100';
    				//$src=file_get_contents($target_path1);
	    	
					$img_r = imagecreatefromstring($getCropimagecontent);
					$dst_r = ImageCreateTrueColor($imagew,$imageh);
					list($width11, $height11) = getimagesize(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->edit_profile_session1);
	    			//list($width11, $height11) = getimagesize($target_path1);
	    			$jpeg = '.jpeg';
	    			imagecopyresized($dst_r,$img_r, 0, 0, 0, 0, $imagew, $imageh, $width11, $height11);
					imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg,$jpeg_quality);
					$var1=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/"."profile".$jpeg);
					
					$data = array("tutor_image_type"=>$image_type,"tutor_image_content"=>$getCropimagecontent,
										"thumb_profile_content"=>$var1,"thumb_profile_type"=>$image_type,
										"tutor_thumb_type"=>$thumb_type,"tutor_thumb_content"=>$imagethumbcontent);
		    		$tutorProfileobj->update($data,"id=$authUserNamespace->admintutorid");
		    		$authUserNamespace->changesave = "Your changes have been saved";
		    		
					
				//	$this->_redirect('/editprofile/profilepic');
				}
				//$this->_redirect('/editprofile/profilepic');
				//  hitesh ended
		
				//unset($authUserNamespace->flow_pic_session);
				//unset($authUserNamespace->flow_pic_session1);
						
				
				}
				
	    }
	
	}


public function fileuploadAction(){
		

		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$img_tutor_id = $this->_request->getParam("id");
		$this->view->user_id_info = $img_tutor_id;
		$this->_helper->layout()->disableLayout();
		ini_set("memory_limit", "25M");
		set_time_limit(0);
		if($this->_request->isPost()){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$json['message']="";
		$uploadfromcourse = $this->_request->getParam("uploadfromcourse");
		$uploadfromtype = $_FILES["uploadfromcourse"]["type"];
		$tmpName1 = $_FILES["uploadfromcourse"]["tmp_name"];
		$fileName = $_FILES["uploadfromcourse"]["name"];
		$image_size = $_FILES["uploadfromcourse"]["size"];
		if(isset($image_size) && $image_size > 5242880){
		  $json['message'] = "size";
		}else
		{
		list($imagewidth, $imageheight, $type, $attr) = getimagesize($tmpName1);
		if((isset($imagewidth) && $imagewidth < 100) ||(isset($imageheight) && $imageheight < 100))
		{

		  $json['message'] = "widthheight";
		}
		else
		{
		      $imagefinaltype = explode("/",$uploadfromtype);
		      if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
		      {
		        $name = "img_pro".time().$authUserNamespace->admintutorid;
		      }else{
		        $name = "img_pro".time().$img_tutor_id;
		      }
		      $target_path1 = dirname(dirname(dirname(__FILE__)))."/docs/".$name.".".$imagefinaltype[1];
		      if(move_uploaded_file($tmpName1,$target_path1)){
		        $json['name'] = $name.".".$imagefinaltype[1];
		         $json['message'] = "success";
		      }else{
		        $json['message'] = "error";
		      }
			}
		}
		echo json_encode($json);
	}
		
}

  public function filecropimageAction(){
    $authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	  $filename = $this->_request->getParam("filename");
		$this->_helper->layout()->disableLayout();
		if($this->_request->isPost()){
      if($this->_request->isXmlHttpRequest()){
        $this->_helper->layout()->disableLayout();
		    $this->_helper->viewRenderer->setNoRender(true);
        $tmpName1 = dirname(dirname(dirname(__FILE__)))."/docs/".$filename;
        //print $tmpName1;exit;
        list($imagewidth, $imageheight, $type, $attr) = getimagesize($tmpName1);
			  list($newimagewidth, $newimageheight, $newtype, $newattr) = getimagesize($tmpName1);

				if($imagewidth > 700){
						$propimgw = 700/$imagewidth;
						$imagewidth = 700;
						$imageheight = $imageheight*$propimgw;
				}
			
				if($imageheight > 420){
					$propimgh = 420/$imageheight;
					$imageheight = 420;
					$imagewidth = $imagewidth*$propimgh;
				}
				$jpeg_quality = 90;
				$src = file_get_contents($tmpName1);
				$img_r = imagecreatefromstring($src);
				$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
				$white = imagecolorallocate($dst_r, 255, 255, 255);
				imagefill($dst_r, 0, 0, $white);
				imagecopyresampled($dst_r,$img_r,0,0,0,0,$imagewidth,$imageheight,$newimagewidth,$newimageheight);

						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
						{
							$name = "img_pro".time().$authUserNamespace->admintutorid;
						}else{
              				$name = "img_pro".time()."0";
						}
					
						$imagefinaltype = explode(".",$filename);
           				 $name = $imagefinaltype[0];
						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
						{		
							$authUserNamespace->edit_profile_session = $name.".".$imagefinaltype[1];
							$authUserNamespace->edit_profile_session1 = $name."croppic.".$imagefinaltype[1];
						}else{
							$authUserNamespace->flow_pic_session = $name.".".$imagefinaltype[1];
							$authUserNamespace->flow_pic_session1 = $name."croppic.".$imagefinaltype[1];
						}
						$target_path1 = dirname(dirname(dirname(__FILE__)))."/docs/".$name.".".$imagefinaltype[1];
						imagejpeg($dst_r,$target_path1,$jpeg_quality);//main image upload
            //array_map("unlink",glob(dirname(dirname(dirname(__FILE__)))."/docs/*.*" ) );
            $json['returnvalue'] = "success";
			      echo json_encode($json);
						
      }
    }
  }

  public function progressAction() {

        // check if a request is an AJAX request
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Request_Exception('Not an AJAX request detected');
        }

        $uploadId = $this->getRequest()->getParam('id');

        // this is the function that actually reads the status of uploading
        $data = uploadprogress_get_info($uploadId);

        $bytesTotal = $bytesUploaded = 0;

        if (null !== $data) {
			
            $bytesTotal = $data['bytes_total'];
            $bytesUploaded = $data['bytes_uploaded'];
        }
		//print "total byte:::::".$bytesTotal;
		/*if($bytesTotal >= 1024 && $bytesTotal <= 1048576)
		{
			//print "total byte if   ::::::::::::::".$bytesTotal;
			 $bytesTotal =  $bytesTotal * 100;
			 $bytesUploaded = $bytesUploaded * 100;
			// print "total byte if   ::::::::::::::".$bytesTotal;
			// print "uploaded if::::::::".$bytesUploaded; 
			// print "total byte after if   ::::::::::::::".$bytesTotal;
		}*/
		$bytesTotal = $bytesTotal / 1024;
        if($bytesTotal / 1024 < 1024) 
            {
				$bytesTotal = number_format($bytesTotal / 1024, 0);
            } 
			
			$bytesUploaded = $bytesUploaded / 1024;
		 if($bytesUploaded / 1024 < 1024) 
         {
				$bytesUploaded = number_format($bytesUploaded / 1024, 2);
         } 
		// print "uploaded::::".$bytesUploaded; 
		$bytesUploaded=ceil($bytesUploaded);
			//print "uploaded after set::::".$bytesUploaded; 
       // print "total byte new::::::".$bytesTotal."<br/>";
		//print "uploaded::::".$bytesUploaded; 
		
        $adapter = new Zend_ProgressBar_Adapter_JsPull();
        $progressBar = new Zend_ProgressBar($adapter, 0, $bytesTotal, 'uploadProgress');
		if($bytesTotal > 0 && $bytesUploaded > 0)
		{
			//print "in if check ::::::bytestotal:;;". $bytesTotal;
			//print "in if check ::::::bytesupload:;;".$bytesUploaded; 
			if ($bytesTotal == $bytesUploaded) 
			{
				//print "if ::::::: ".$bytesUploaded;
				$progressBar->finish();
			
			} 
			else 
			{
				//print "if else:::::: ".$bytesUploaded;
				$progressBar->update($bytesUploaded);
			}
		}
		
		else
		{
			//print "else::::::".$bytesUploaded;
			$progressBar->update($bytesUploaded);
		}
    }
	

}
?>