<?php

class Bankoffercms_Model_Custom_Miscellaneous{
	
	/**
	 * @author Parth Arora <arora.parth@gmail.com>
	 * @version 1.0
	 * @Desc
	 * The function takes an optional input specifying the length of the password to be generated, if no
	 * length is specified the default password length is taken as 7 characters long. The password generated
	 * will be alpha-numeric
	 * 
	 * @link http://www.friskwave.com
	 * @param int - length of password (optional)
	 * @return randomly generated alpha-numeric password
	 */
	public function randomPassword($password_length=7){
		
		$chars = "abcdefghijklmnopqrstuvwxyz123456789";
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '' ;
		
		while($i <= $password_length){
		
			$num = rand() % 34;
			$tmp = substr($chars, $num, 1);
			$pass = $pass . $tmp;
			$i++;
		}
		
		return $pass;
	}
	
	/**
	 * @author Parth Arora <arora.parth@gmail.com>
	 * @version 1.0
	 * @Desc
	 * to detect the user Browser and Operating System
	 * 
	 * @link http://www.friskwave.com
	 * @param string - $_SERVER user-agent string
	 * @return array - an associative Array of Client Agent Properties
	 */
	public function detectUserAgent(){
		
		$clientProps = array();
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		
		// detect Operating System
		// array of Possible Operating Systems
		$OSList = array(
			'Windows 3.11'=>'Win16','Windows 95'=>'Windows 95','Windows 95'=>'Win95','Windows 95'=>'Windows_95',
			'Windows 98'=>'Windows 98','Windows 98'=>'Win98','Windows 2000'=>'Windows NT 5.0','Windows 2000'=>'Windows 2000',
			'Windows XP'=>'Windows NT 5.1','Windows XP'=>'Windows XP','Windows Server 2003'=>'Windows NT 5.2',
			'Windows Vista'=>'Windows NT 6.0','Windows 7'=>'Windows NT 7.0','Windows NT 4.0'=>'Windows NT 4.0',
			'Windows NT 4.0'=>'WinNT4.0','Windows NT 4.0'=>'WinNT','Windows NT 4.0'=>'Windows NT','Windows ME'=>'Windows ME',
			'Open BSD'=>'OpenBSD','Sun OS'=>'SunOS','Linux'=>'Linux','Linux'=>'X11','Mac OS'=>'Mac_PowerPC','Mac OS'=>'Macintosh',
			'QNX'=>'QNX','BeOS'=>'BeOS','OS/2'=>'OS/2','Search Bot'=>'nuhk','Search Bot'=>'Googlebot','Search Bot'=>'Yammybot',
			'Search Bot'=>'Openbot','Search Bot'=>'Slurp','Search Bot'=>'MSNBot','Search Bot'=>'Ask Jeeves/Teoma','Search Bot'=>'ia_archiver'
		);
		
		foreach($OSList as $CurrOS=>$Match){
			if(stristr($userAgent,$Match)){
				$clientProps['platform'] = $CurrOS;
				break;
			}
		}
		
		// detect browser
		if(strpos($userAgent,'Gecko')){
			if(strpos($userAgent,'Netscape'))$clientProps['browser'] = 'Netscape (Gecko/Netscape)';
			elseif(strpos($userAgent,'Firefox'))$clientProps['browser'] = 'Mozilla Firefox (Gecko/Firefox)';
			else $clientProps['browser'] = 'Mozilla (Gecko/Mozilla)';
		}
		elseif(strpos($userAgent,'MSIE')){
			if(strpos($userAgent,'Opera'))$clientProps['browser'] = 'Opera (MSIE/Opera/Compatible)';
			else $clientProps['browser'] = 'Internet Explorer (MSIE/Compatible)';
		}
		else $clientProps['browser'] = 'Others browsers';
		
		// detect client Ip-Address
		if(!empty($_SERVER["HTTP_CLIENT_IP"]))$clientProps['ip'] = ''.$_SERVER["HTTP_CLIENT_IP"].' ';
		elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))$clientProps['ip'] = ''.$_SERVER["HTTP_X_FORWARDED_FOR"].' ';
		elseif(!empty($_SERVER["REMOTE_ADDR"]))$clientProps['ip'] = ''.$_SERVER["REMOTE_ADDR"].' ';
		
		return $clientProps;
	}
	
	/**
	 * @author Parth Arora <arora.parth@gmail.com>
	 * @version 1.0
	 * @Desc
	 * function to get the url of the page that the client is currently visiting
	 * 
	 * @link http://www.friskwave.com
	 * @return string - URL of the current page along with port number and the
	 * query string
	 */
	public function getCurrentUrl(){
		
		$pageURL = 'http';
		
		if($_SERVER["HTTPS"] == "on"){
			$pageURL .= "s";
		}
		
		$pageURL .= "://";
		
		if($_SERVER["SERVER_PORT"] != "80"){
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}
		else{
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		
		return $pageURL;
	}
}