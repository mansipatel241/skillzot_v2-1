function CallToAbout() 
{
	window.location.href = "About.html";
}
function CallToTeam() 
{
	window.location.href = "Team.html";
}
function CallToFAQ() 
{
	window.location.href = "FAQ.html";
}
function CallToTerms() 
{
	window.location.href = "Terms.html";
}
function CallToBlog() 
{
	window.location.href = "http://www.skillzot.com/blog/";
}
function LogOut()
{
		localStorage.removeItem("username");    
		localStorage.removeItem("StudentName");       
		localStorage.removeItem("FirstName");              
		location.reload();

}
function CallToSignUp() 
{
	//alert(page);
	window.location.href = "SignUpScreen.html";
}
function CallToLogIn() 
{
	//alert(page);
	window.location.href = "LogInScreen.html";
}
function ReturnToPage() 
{
	var Page=localStorage.getItem("Page");
	
		if(Page=="TutorDetails")
		{
			var TutorId=localStorage.getItem("TutorId");	
			window.location.href = "TutorDetails.html?TutorId="+TutorId;
		}else if(Page=="CategoryList")
		{
			var SubCategory=localStorage.getItem("SubCategory");	
			if(SubCategory=="mainpage")
			{
				window.location.href = "index.html";
			}else
			{
				window.location.href = "index.html#"+SubCategory;
			}		
		}else if(Page=="SkillTutorList")
		{
			var CategoryId=localStorage.getItem("CategoryId");
			var SkillId=localStorage.getItem("SkillId");
			window.location.href = "SkillTutorList.html?CategoryId="+CategoryId+"&SkillId="+SkillId;
			
		}else if(Page=="CourseDetails" || Page=="BatchReserve")
		{
			var TutorId=localStorage.getItem("TutorId");	
			window.location.href = "CourseDetails.html?TutorId="+TutorId;
		}else if(Page=="Reviews")
		{
			var TutorId=localStorage.getItem("TutorId");	
			var SkillId=localStorage.getItem("SkillId");
			window.location.href = "Reviews.html?TutorId="+TutorId+"&SkillId="+SkillId;
		}
		/*if(Page=="BatchReserve")
		{
			var CourseId=localStorage.getItem("CourseId");	
			var BatchId=localStorage.getItem("BatchId");
			window.location.href = "BatchReserve.html?CourseId="+CourseId+"&BatchId="+BatchId;
		}*/
}
window.onload = function () {

    $('#crossButton').click(function(e) {
		ReturnToPage();
	});
   
   	if(localStorage.getItem("username") != null)
	{
			//$('#loginlogout').html('<a onclick="LogOut()" class="ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-action">Logout</a>');
			$('.FAQ').attr('id', 'demo1');
			$('#loginlogout').html('<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-action" href="#popupExit" data-rel="popup" data-transition="slideup" data-position-to="#demo1">'+localStorage.getItem("FirstName")+'</a>');
			
	}
}