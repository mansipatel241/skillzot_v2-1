<?php

include('Conn.php');

$Tutor_Id = $_POST['TutorId'];

$sql = "SELECT * FROM `sz_tx_tutor_profile` WHERE `id`='$Tutor_Id'";
$result = $conn->query($sql);
$rowcount=mysqli_num_rows($result);

$sql_travelradius = "SELECT `travel_radius` FROM `sz_tx_tutor_course_batch` WHERE `tutor_id`='$Tutor_Id' and `travel_radius` !='' limit 1 ";
$result_travelradius = $conn->query($sql_travelradius);
          	
$sql_location = "SELECT distinct(locality) FROM `sz_tx_tutor_course_batch` AS `c` LEFT JOIN `sz_master_address` AS `m` ON c.locality=m.address_id LEFT JOIN `sz_locality_suburbs` AS `l` ON m.address_id=l.id WHERE (c.tutor_id='$Tutor_Id') ORDER BY `m`.`address_value` asc ";
$result_location = $conn->query($sql_location);
$rowcount_location = mysqli_num_rows($result_location);

$sql_skillstaught = "SELECT distinct(`tutor_skill_id`) FROM `sz_tx_tutor_skill_course` WHERE `tutor_id`='$Tutor_Id'";
$result_skillstaught = $conn->query($sql_skillstaught);
$row_skillstaught=mysqli_fetch_row($result_skillstaught);
$skillcourseObj=explode(",",$row_skillstaught[0]);
$skillnames="";
$totalcount=count($skillcourseObj);
$i=0;
if($totalcount > 0)
{
	while ($totalcount!="0") {
		# code...
	
		//for ($start=0; $start < count($skillcourseObj); $start++) {
				$skill_array=$skillcourseObj[$i];
				if($skill_array!=NULL)
				{
					$sql_skillname = "SELECT `skill_name` FROM `sz_master_skills` WHERE `skill_id`='$skill_array'";
					$result_skillname = $conn->query($sql_skillname);
					$skill_name=mysqli_fetch_row($result_skillname);
					$skillnames=$skillnames .', '. $skill_name[0];
				}
		//}     
				$i++;
				$totalcount--;
			}
}
$skillnames= ltrim ($skillnames, ', ');
$skillnames= rtrim ($skillnames, ', ');
$loc_name="";
$names="";
if($names==""){
	while ($row_location=mysqli_fetch_row($result_location))
	{
		$loc_number=$row_location[0];
		if($loc_number!=NULL)
		{

			$sql_loc = "SELECT `address_value` FROM `sz_master_address` WHERE `address_id` ='$loc_number' LIMIT 1 ";
			$result_locality = $conn->query($sql_loc);
			$loc_name=mysqli_fetch_row($result_locality);
			$names=$names .', '. $loc_name[0];
		}

	}
}
$location_names= ltrim ($names, ', ');

if ($rowcount > 0) 
{
		$row=mysqli_fetch_row($result);
		
		$row_travelradius=mysqli_fetch_row($result_travelradius);

		$json[]=array("TutorDescription"=>$row[17],"SkillsTaught"=>$skillnames,"ExtensionDisplay"=>$row[55],"CompanyName"=>$row[1], "FirstName"=>$row[2],"Travel_Radius"=>$row_travelradius[0],"Location_Display"=>$location_names,"Picture"=>base64_encode($row[37]), "LastName"=>$row[3],"Address"=>$row[9], "Mobile"=>$row[13]);		
    
}
else
{
	$json[]=array();
}

echo json_encode($json);

?>

