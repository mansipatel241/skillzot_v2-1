<?php
include('Conn.php');

$SkillId = $_REQUEST['SkillId'];
$Token = $_REQUEST['Token'];
$Count = $_REQUEST['Count'];

$Age="";
$Locality="";
$LessonType="";
$ClassLocation="";
$BatchTiming="";
$search_tutor="";
$search_locality="";

$Age = isset($_POST['Age']) ? $_POST['Age'] : '';
$Locality = isset($_POST['Locality']) ? $_POST['Locality'] : '';
$LessonType = isset($_POST['LessonType']) ? $_POST['LessonType'] : '';
$ClassLocation = isset($_POST['ClassLocation']) ? $_POST['ClassLocation'] : '';
$BatchTiming = isset($_POST['BatchTiming']) ? $_POST['BatchTiming'] : '';
$search_tutor = isset($_POST['search']) ? $_POST['search'] : '';
$search_locality = isset($_POST['locality_search']) ? $_POST['locality_search'] : '';

$Token = $Token * 5;
$rowcount_locality=0;
$travel_radius_value="";
$fetch_age_id="";
$locality_condition="";
$lesson_type_condition="";
$ClassLocation_condition="";
$Batchtiming_condition="";
$Tutor_Id_search_condition="";
$Locality_search_condition="";
if($Age==2)
{
	$fetch_age_id = "( min <=14 OR max<=14 )";
}else
{
	$fetch_age_id = "( 1=1 )";
}

if($Locality!="null" and $Locality!="undefined" and $Locality!="")
{

	$sql_locality = "SELECT `address_id` FROM `sz_master_address` WHERE `suburbs_id`='$Locality'";
	$result_locality = $conn->query($sql_locality);
	$rowcount_locality=mysqli_num_rows($result_locality); 

    $totalcount=$rowcount_locality;
    $List="";
	while($totalcount!=0)
	{
		$row_addressid=mysqli_fetch_row($result_locality);
	    $List .= $row_addressid[0] . ',';
	    
	    $totalcount--;
	}
	$List = rtrim($List, ',');

	$locality_condition="( s.locality IN ( $List ) )";

}else
{
	$locality_condition="( 1=1 )";
}

if($LessonType!="undefined" and $LessonType!="")
{
	$lesson_type_condition = "`tutor_class_type`='$LessonType'";	
}else
{
	$lesson_type_condition = "( 1=1 )";
}

if($ClassLocation!="undefined" and $ClassLocation!="")
{
	if($ClassLocation=="3")
	{
		$ClassLocation_condition = "( s.tutor_location='1' or s.tutor_location='2' )";	
	}else
	{
		$ClassLocation_condition = "( s.tutor_location='$ClassLocation' )";	
	}
	
}else
{
	$ClassLocation_condition = "( 1=1 )";
}

if($BatchTiming!="null" and $BatchTiming!="undefined" and $BatchTiming!="")
{
	if($BatchTiming=="7")
	{
		$Batchtiming_condition = "( s.tutor_batch_day like '%6%' or s.tutor_batch_day like '%7%' ) ";	
	}elseif($BatchTiming=="2")
	{
		$Batchtiming_condition = "( m.id>=11 and t.id<=19 )";
	}
	elseif($BatchTiming=="3")
	{
		$Batchtiming_condition = "( m.id>=19 and t.id<=25 )";
	}
	elseif($BatchTiming=="4")
	{
		$Batchtiming_condition = "( m.id>=25 and t.id<=35 )";
	}
	elseif($BatchTiming=="5")
	{
		$Batchtiming_condition = "( m.id>=35 and t.id<=43 )";
	}
	elseif($BatchTiming=="6")
	{
		$Batchtiming_condition = "( m.id>=43 and t.id<=67 )";
	}
}else
{
	$Batchtiming_condition = "( 1=1 )";
}


if($search_locality!="")
{
	$search_locality = str_replace('%20',' ', $search_locality);

	$sql_locality2 = "SELECT GROUP_CONCAT(address_id) AS `address_id` FROM `sz_master_address` WHERE (address_value like '%$search_locality%') LIMIT 1";
	$result_locality2 = $conn->query($sql_locality2);
	$rowcount_locality2=mysqli_num_rows($result_locality2); 

	if($rowcount_locality2 > 0)
	{
		$row_locality2=mysqli_fetch_row($result_locality2);
		$List=$row_locality2[0];
		$Locality_search_condition="( s.locality IN ( $List ) )";
	}else
	{
		$Locality_search_condition= "( 1=1 )";
	}

	
}else
{
	$Locality_search_condition= "( 1=1 )";
}

if($search_tutor!="")
{
	$search_tutor = str_replace('%20',' ', $search_tutor);
	$search_tutor = str_replace('%27',' ', $search_tutor);
	$sql_active3 = "SELECT distinct(id) FROM `sz_tx_tutor_profile` WHERE `is_active`='0' and ( (CONCAT(tutor_first_name,' ',tutor_last_name) like '%$search_tutor%') || company_name like '%$search_tutor%') ";			
    $result_active3 = $conn->query($sql_active3);
    $rowcount_active3 = mysqli_num_rows($result_active3);

    if($rowcount_active3 >0)
    {

    		$row_active3=mysqli_fetch_row($result_active3);
    		$json1[] = array("Tutor_Id"=>$row_active3[0],"SkillName"=>""); 
			$counter++;
    	
    }else
    {

		$search_tutor_array=explode(" ", $search_tutor);

		$list = array("in","is","it","a","the","of","or","I","you","he","me","us","they","she","to","but","that","this","those","then","has","and");
		
		$list_arr = array();
		foreach($search_tutor_array as $key){
			if (in_array($key, $list)) {
			$list_arr[] = $key;
			//print_r($list_arr);
			}
		}

		$result = array_diff($search_tutor_array, $list_arr);
		$final_search_keyword=implode(" ",$result);
		$search_tutor_array = explode(" ", $final_search_keyword);

		 $List="";
		for($i=0;$i<sizeof($search_tutor_array); $i++)
		{
			$key=$search_tutor_array[$i];
		
			$sql_active1 = "SELECT distinct(id) FROM `sz_tx_tutor_profile` WHERE `is_active`='0' and ( (CONCAT(tutor_first_name,' ',tutor_last_name) like '%$key%') || company_name like '%$key%') ";			
	        $result_active1 = $conn->query($sql_active1);
	        $rowcount_active1 = mysqli_num_rows($result_active1);

	        if($rowcount_active1 >0)
	        {

	        		$row_active1=mysqli_fetch_row($result_active1);
	        		$List .= $row_active1[0] . ',';
	        	
	        }
		}
		$List = rtrim($List, ',');

		$sql_tutor = "SELECT distinct(id) FROM `sz_tx_tutor_profile` WHERE ( id IN ( $List ) )";
		$result_tutor = $conn->query($sql_tutor);
		$rowcount_tutor=mysqli_num_rows($result_tutor); //1

		 if($rowcount_tutor >0)
	     {

	        		$row_tutor=mysqli_fetch_row($result_tutor);
	        		$json1[] = array("Tutor_Id"=>$row_tutor[0],"SkillName"=>""); 

					$counter++;
	        	
	     }
	}

		
}else
{
		

	$sql = "SELECT * FROM `sz_master_skills_for_diplay_listing` WHERE `skill_id`= '$SkillId' && is_skill='1'";
	$result = $conn->query($sql);
	$rowcount=mysqli_num_rows($result); //1
	$row=mysqli_fetch_row($result);
	$SkillId=$row[0];
	$Parent_Skill_Id_check=$row[5];
	$counter=0;

  	if ($rowcount > 0) 
	{	
		if($Parent_Skill_Id_check!="0")
		{

			$sql1 = "SELECT distinct(`tutor_id`) FROM `sz_tx_tutor_skill_course` WHERE ( `tutor_skill_id` LIKE '%$SkillId%' ) and $fetch_age_id "; 
			$result1 = $conn->query($sql1);
			$rowcount1 = mysqli_num_rows($result1);

	        while($rowcount1 > 0)
			{
				$row1=mysqli_fetch_row($result1);
				
				$sql_active = "SELECT id FROM `sz_tx_tutor_profile` WHERE `id`='$row1[0]' and `is_active`='0' and $lesson_type_condition ";			
		        $result_active = $conn->query($sql_active);
		        $rowcount_active = mysqli_num_rows($result_active);
		        
		        if($rowcount_active > 0)
		        {
		        		$row_active=mysqli_fetch_row($result_active);
		        		if($Batchtiming_condition!="( 1=1 )" or $ClassLocation_condition!="( 1=1 )" or $locality_condition!="( 1=1 )" or $Locality_search_condition!="( 1=1 )")
		        		{
		        			$sql_batch = "SELECT distinct(`s`.`tutor_id`) FROM `sz_tx_tutor_course_batch` AS `s` INNER JOIN `sz_master_batch_timing` AS `m` ON ( s.tutor_batch_from_timing=m.timing ) INNER JOIN `sz_master_batch_timing` AS `t` ON ( s.tutor_batch_to_timing=t.timing ) WHERE ( s.tutor_id='$row_active[0]' ) and $ClassLocation_condition and $Batchtiming_condition and $locality_condition and $Locality_search_condition";			
				        	$result_batch = $conn->query($sql_batch);
				        	$rowcount_batch = mysqli_num_rows($result_batch);
					        
					        if($rowcount_batch > 0) 
						    {
						    	$row_batch=mysqli_fetch_row($result_batch);
								$json1[] = array("Tutor_Id"=>$row_batch[0],"SkillName"=>$row[1]); 

								$counter++;
							}

		        		}else
		        		{     	
							$json1[] = array("Tutor_Id"=>$row_active[0],"SkillName"=>$row[1]); 
							$counter++;
		        		}		        
				}	

				$rowcount1--;
			}	
		}else
		{


			$sql_masterskill = "SELECT skill_id FROM `sz_master_skills_for_diplay_listing` WHERE `parent_skill_id`= '$SkillId' || `skill_id`= '$SkillId'";
			$result_masterskill = $conn->query($sql_masterskill);
			$rowcount_masterskill=mysqli_num_rows($result_masterskill);

			if ($rowcount_masterskill > 0) 
			{
				while($row_masterskill=mysqli_fetch_row($result_masterskill))
				{

					$sql1 = "SELECT distinct(`tutor_id`) FROM `sz_tx_tutor_skill_course` WHERE ( `tutor_skill_id` LIKE '%$row_masterskill[0]%' ) and $fetch_age_id "; 
					$result1 = $conn->query($sql1);
					$rowcount1 = mysqli_num_rows($result1);

			        while($rowcount1 > 0)
					{
						$row1=mysqli_fetch_row($result1);
						
						$sql_active = "SELECT id FROM `sz_tx_tutor_profile` WHERE `id`='$row1[0]' and `is_active`='0' and $lesson_type_condition ";			
				        $result_active = $conn->query($sql_active);
				        $rowcount_active = mysqli_num_rows($result_active);
				        
				        if($rowcount_active > 0)
				        {
				        		$row_active=mysqli_fetch_row($result_active);
				        		if($Batchtiming_condition!="( 1=1 )" or $ClassLocation_condition!="( 1=1 )" or $locality_condition!="( 1=1 )" or $Locality_search_condition!="( 1=1 )")
				        		{
				        			$sql_batch = "SELECT distinct(`s`.`tutor_id`) FROM `sz_tx_tutor_course_batch` AS `s` INNER JOIN `sz_master_batch_timing` AS `m` ON ( s.tutor_batch_from_timing=m.timing ) INNER JOIN `sz_master_batch_timing` AS `t` ON ( s.tutor_batch_to_timing=t.timing ) WHERE ( s.tutor_id='$row_active[0]' ) and $ClassLocation_condition and $Batchtiming_condition and $locality_condition and $Locality_search_condition";			
						        	$result_batch = $conn->query($sql_batch);
						        	$rowcount_batch = mysqli_num_rows($result_batch);
							        
							        if($rowcount_batch > 0) 
								    {
								    	$row_batch=mysqli_fetch_row($result_batch);
										$json1[] = array("Tutor_Id"=>$row_batch[0],"SkillName"=>$row[1]); 

										$counter++;
									}

				        		}else
				        		{     	
									$json1[] = array("Tutor_Id"=>$row_active[0],"SkillName"=>$row[1]); 
									$counter++;
				        		}		        
						}	

						$rowcount1--;
					}

				}

			}

		}	
		
	}


}

	
	
	
	for($i=$Token; $i<($Token+5);$i++)
	{
	   if(isset($json1[$i]['Tutor_Id']))
	   {

		if($json1[$i]['Tutor_Id'] !=null)
		{

			$TutorId = $json1[$i]['Tutor_Id'];
			$sql2 = "SELECT * FROM `sz_tx_tutor_profile` WHERE `id`='$TutorId' ";
            $result2 = $conn->query($sql2);
            $rowcount2 = mysqli_num_rows($result2);

            if ($rowcount2 > 0) 
	        {

				$sql_travelradius = "SELECT `travel_radius` FROM `sz_tx_tutor_course_batch` WHERE `tutor_id`='$TutorId' and `travel_radius` !='' limit 1 ";
	            $result_travelradius = $conn->query($sql_travelradius);
	            $rowcount_travelradius = mysqli_num_rows($result_travelradius);
	          	
	            if($rowcount_travelradius > 0)
	            {
	            	$row_travelradius=mysqli_fetch_row($result_travelradius);
	            	$travel_radius_value=$row_travelradius[0];
	            }else
	            {
	            	$travel_radius_value="";
	            }

	          	$sql_location = "SELECT distinct(c.locality) FROM `sz_tx_tutor_course_batch` AS `c` LEFT JOIN `sz_master_address` AS `m` ON c.locality=m.address_id LEFT JOIN `sz_locality_suburbs` AS `l` ON m.address_id=l.id WHERE (c.tutor_id='$TutorId') and ( c.locality IS NOT NULL ) ORDER BY `m`.`address_value` asc ";
	            $result_location = $conn->query($sql_location);
				$rowcount_location = mysqli_num_rows($result_location);

				$sql_fee = "SELECT `tutor_pay_feetype`,min(`tutor_rate`) FROM `sz_tx_tutor_skill_course` WHERE `tutor_id`='$TutorId'";

	            $result_fee = $conn->query($sql_fee);
			
					//$row_location=mysqli_fetch_row($result_location);
				$loc_name="";
				$names="";
				$location_names="";
				if($names==""){
					while ($row_location=mysqli_fetch_row($result_location))
					{
						$loc_number=$row_location[0];
						if($loc_number!=NULL)
						{
						$sql_loc = "SELECT `address_value` FROM `sz_master_address` WHERE `address_id` ='$loc_number' LIMIT 1 ";
						$result_locality = $conn->query($sql_loc);
						$loc_name=mysqli_fetch_row($result_locality);
						$names=$names .', '. $loc_name[0];
						}
					}
				}
				$location_names= ltrim ($names, ', ');
		
			
			    $row2=mysqli_fetch_row($result2);
			    
			    $row_fee=mysqli_fetch_row($result_fee);

			    $sql_feetype = "SELECT `fee_type_name` FROM `sz_master_tutor_fee_type` WHERE `fee_type_id`='$row_fee[0]';";
    			$result_feetype = $conn->query($sql_feetype);
    			$row_feetype=mysqli_fetch_row($result_feetype);
			
			    $json2[] = array("SkillNameDisplay"=>$json1[$i]['SkillName'],"Tutor_Id"=>$row2[0],"Company_Name"=>$row2[1], "First_Name"=>$row2[2], "Last_Name"=>$row2[3], "Picture"=>base64_encode($row2[37]), "Token"=>$Token, "Counter"=>$counter,"Travel_Radius"=>$travel_radius_value,"Location_Display"=>$location_names,"Fees"=>$row_fee[1],"Fees_Type"=>$row_feetype[0]); 
			    
				
			}
			

		}
	  }
	  else
		{

			//$json2[]=array();
		}
	}
		

echo json_encode($json2);
	

?>