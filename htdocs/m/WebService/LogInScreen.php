<?php

include('Conn.php');

const SKZ_INVALID_USERNAME_OR_PASSWORD = 206;
const SKZ_WRONG_PASSWORD = 207;
const SKZ_ERROR_UPDATING_RECORD = 208;

$uemail = $_POST['Email'];
$userpasswordphp = $_POST['Password'];

$upassword = sha1($userpasswordphp);

$sql = "SELECT * from sz_tx_student_tutor where std_email='$uemail'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
    	$row = $result->fetch_assoc();
  
		if(is_null($row["std_v2_password"]) )
		{
						
			if($row["std_password"] === $userpasswordphp)
			{
				$updatedpassword = $upassword;

				$sql2 = "UPDATE sz_tx_student_tutor SET std_v2_password='$updatedpassword', std_password=NULL where std_email='$uemail'";
				if ($conn->query($sql2) === TRUE) 
				{
					$json = array("status" => 0, "message" => "Success", "StudentId"=> $row["id"],"FirstName"=> $row["first_name"],"LastName"=> $row["last_name"]);
				} 
				else 
				{
					$json = array("status" => SKZ_ERROR_UPDATING_RECORD , "message" => "FailureUpdate");
				}
					
		    }
			else
			{
				$json = array("status" => SKZ_WRONG_PASSWORD , "message" => "Invalid Password");
			}		
		}
		else
		{
			if($upassword === $row["std_v2_password"])
			{
				$json = array("status" => 0, "message" => "Success", "StudentId" => $row["id"],"FirstName"=> $row["first_name"],"LastName"=> $row["last_name"]);
			}
			else
			{
				$json = array("status" => SKZ_WRONG_PASSWORD , "message" => "Invalid Password");
			}
		}		
}
else 
{
	$json = array("status" => SKZ_INVALID_USERNAME_OR_PASSWORD , "message" => "Email Id not registered");
}
	
$conn->close();

header("Content-Type: application/json; charset=UTF-8");
echo json_encode($json);
?>