<?php

include('Conn.php');

function getCommission($conn,$Tutor_Book_Id,$Tutor_Fee_Per_Unit)
{

		$sql_totalstudents = "SELECT SUM(`no_of_students`) FROM `sz_order_details` WHERE `date_of_booking` LIKE CONCAT(YEAR(NOW()), '-', MONTH(NOW()), '%') and `tutor_book_id`='$Tutor_Book_Id';";
		$result_totalstudents= $conn->query($sql_totalstudents);

		$row_totalstudents=mysqli_fetch_row($result_totalstudents);

		if($row_totalstudents==null)
		{
			$row_totalstudents[0]=0;
		}
			
		$sql_commission = "SELECT `commission` FROM `sz_commission_details` WHERE ('$row_totalstudents[0]' between `min_enroll` and `max_enroll`) and ('$Tutor_Fee_Per_Unit' between `min_fee` and `max_fee`)";
		$result_commission= $conn->query($sql_commission);

		$row_commission=mysqli_fetch_row($result_commission);
	
		return $row_commission[0];

}

$CourseId = $_REQUEST['CourseId'];
$Name = $_REQUEST ['Name'];
//$Address = $_REQUEST['Address'];
//$City = $_REQUEST['City'];
//$State = $_REQUEST['State'];
//$Zip = $_REQUEST['Zip'];
//$Mobile = $_REQUEST['Mobile'];
$email = $_REQUEST['email'];
//$StudentId = $_REQUEST['StudentId'];
$BatchId = $_REQUEST['BatchId'];
$NoOfStudents = $_REQUEST['NoOfStudents'];
$CourseName = $_REQUEST['CourseName'];
$GrandPrice = $_REQUEST['GrandPrice'];
$TutorFirstName = $_REQUEST['TutorFirstName'];
$TutorLastName = $_REQUEST['TutorLastName'];
$Tutor_Book_Id = $_REQUEST['TutorId'];
$OrderId = $_REQUEST['ReferenceNo'];
$Tutor_Fee_Per_Unit = $_REQUEST['TutorFeePerUnit'];

$class_booked = $CourseName. " by ". $TutorFirstName. " ". $TutorLastName;
	
date_default_timezone_set("Asia/Kolkata");
$date = new DateTime();
$d = $date->format('Y-m-d H:i:s');

$datedisplay= date('jS  F, Y \a\t h:i A', strtotime($d));

$totalprice =  $Tutor_Fee_Per_Unit * $NoOfStudents;
$internetcharges = 0.02*$totalprice;

$sql_studentid = "SELECT * from sz_tx_student_tutor where std_email='$email'";
$result_studentid = $conn->query($sql_studentid);

if ($result_studentid->num_rows > 0) 
{
	$row = $result_studentid->fetch_assoc();
	$StudentId=$row["id"];
}

$sql = "INSERT INTO `sz_order_details`(std_id, tutor_book_id, course_id, batch_id, order_id, date_of_booking, class_booked, no_of_students, tutor_fee_per_unit, total_price, commision_price, tutor_price, name, address, city, state, pincode, phone_number, grand_total) VALUES ($StudentId, $Tutor_Book_Id, $CourseId, $BatchId, $OrderId, '$d', '$class_booked', $NoOfStudents ,$Tutor_Fee_Per_Unit, $totalprice, 0, 0, '$Name', 'test', 'test', 'test', 'test','test', $GrandPrice )";
$result = $conn->query($sql);

$commission=getCommission($conn,$Tutor_Book_Id,$Tutor_Fee_Per_Unit);

$commission_amount= $GrandPrice * $commission;

$tutor_amount=$GrandPrice - $commission_amount;

$sql_update = "UPDATE `sz_order_details` SET `commision_price`='$commission_amount',`tutor_price`='$tutor_amount' WHERE `order_id`='$OrderId' and `tutor_book_id`='$Tutor_Book_Id' and `std_id`='$StudentId' and `batch_id`='$BatchId' and `course_id`='$CourseId'";
$result_update= $conn->query($sql_update);

if($result === TRUE) 
{
 	$json = array( 'Status'=> 1, 'DateDisplay'=>$datedisplay);

} else 
{
  	$json = array( 'Status'=> 0 ); 
}

echo json_encode($json);









?>